
package ri.serien.libecranrpg.vgtm.VGTM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM71FM_L6 extends SNPanelEcranRPG implements ioFrame {
  
  /**
   * Constructeur.
   */
  public VGTM71FM_L6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    WDIER.setVisible(lexique.isTrue("19"));
    OBJ_27_OBJ_27.setVisible(lexique.isTrue("19"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie d'écart de réglement"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    CGE01 = new XRiTextField();
    MTE01 = new XRiTextField();
    LIE01 = new XRiTextField();
    WSAN01 = new XRiTextField();
    WAFF01 = new XRiTextField();
    WNAT01 = new XRiTextField();
    label7 = new JLabel();
    CGE02 = new XRiTextField();
    MTE02 = new XRiTextField();
    LIE02 = new XRiTextField();
    WSAN02 = new XRiTextField();
    WAFF2 = new XRiTextField();
    WNAT02 = new XRiTextField();
    CGE03 = new XRiTextField();
    MTE03 = new XRiTextField();
    LIE03 = new XRiTextField();
    WSAN03 = new XRiTextField();
    WAFF03 = new XRiTextField();
    WNAT03 = new XRiTextField();
    CGE04 = new XRiTextField();
    MTE04 = new XRiTextField();
    LIE04 = new XRiTextField();
    WSAN04 = new XRiTextField();
    WAFF04 = new XRiTextField();
    WNAT04 = new XRiTextField();
    CGE05 = new XRiTextField();
    MTE05 = new XRiTextField();
    LIE05 = new XRiTextField();
    WSAN05 = new XRiTextField();
    WAFF05 = new XRiTextField();
    WNAT05 = new XRiTextField();
    C6CGT = new XRiTextField();
    C6MTT = new XRiTextField();
    C6LIT = new XRiTextField();
    label8 = new JLabel();
    OBJ_27_OBJ_27 = new JLabel();
    WDIER = new XRiTextField();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(875, 290));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");
            
            // ---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);
          
          // ======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");
            
            // ---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);
          
          // ======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");
            
            // ---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          
          // ---- label1 ----
          label1.setText("N\u00b0 Cpte");
          label1.setName("label1");
          
          // ---- label2 ----
          label2.setText("Montant");
          label2.setName("label2");
          
          // ---- label3 ----
          label3.setText("Libell\u00e9 \u00e9criture");
          label3.setName("label3");
          
          // ---- label4 ----
          label4.setText("Sect");
          label4.setName("label4");
          
          // ---- label5 ----
          label5.setText("Aff");
          label5.setName("label5");
          
          // ---- label6 ----
          label6.setText("Nat");
          label6.setName("label6");
          
          // ---- CGE01 ----
          CGE01.setName("CGE01");
          
          // ---- MTE01 ----
          MTE01.setName("MTE01");
          
          // ---- LIE01 ----
          LIE01.setName("LIE01");
          
          // ---- WSAN01 ----
          WSAN01.setName("WSAN01");
          
          // ---- WAFF01 ----
          WAFF01.setName("WAFF01");
          
          // ---- WNAT01 ----
          WNAT01.setName("WNAT01");
          
          // ---- label7 ----
          label7.setText("Charge :");
          label7.setName("label7");
          
          // ---- CGE02 ----
          CGE02.setName("CGE02");
          
          // ---- MTE02 ----
          MTE02.setName("MTE02");
          
          // ---- LIE02 ----
          LIE02.setName("LIE02");
          
          // ---- WSAN02 ----
          WSAN02.setName("WSAN02");
          
          // ---- WAFF2 ----
          WAFF2.setName("WAFF2");
          
          // ---- WNAT02 ----
          WNAT02.setName("WNAT02");
          
          // ---- CGE03 ----
          CGE03.setName("CGE03");
          
          // ---- MTE03 ----
          MTE03.setName("MTE03");
          
          // ---- LIE03 ----
          LIE03.setName("LIE03");
          
          // ---- WSAN03 ----
          WSAN03.setName("WSAN03");
          
          // ---- WAFF03 ----
          WAFF03.setName("WAFF03");
          
          // ---- WNAT03 ----
          WNAT03.setName("WNAT03");
          
          // ---- CGE04 ----
          CGE04.setName("CGE04");
          
          // ---- MTE04 ----
          MTE04.setName("MTE04");
          
          // ---- LIE04 ----
          LIE04.setName("LIE04");
          
          // ---- WSAN04 ----
          WSAN04.setName("WSAN04");
          
          // ---- WAFF04 ----
          WAFF04.setName("WAFF04");
          
          // ---- WNAT04 ----
          WNAT04.setName("WNAT04");
          
          // ---- CGE05 ----
          CGE05.setName("CGE05");
          
          // ---- MTE05 ----
          MTE05.setName("MTE05");
          
          // ---- LIE05 ----
          LIE05.setName("LIE05");
          
          // ---- WSAN05 ----
          WSAN05.setName("WSAN05");
          
          // ---- WAFF05 ----
          WAFF05.setName("WAFF05");
          
          // ---- WNAT05 ----
          WNAT05.setName("WNAT05");
          
          // ---- C6CGT ----
          C6CGT.setName("C6CGT");
          
          // ---- C6MTT ----
          C6MTT.setName("C6MTT");
          
          // ---- C6LIT ----
          C6LIT.setName("C6LIT");
          
          // ---- label8 ----
          label8.setText("T.V.A :");
          label8.setName("label8");
          
          // ---- OBJ_27_OBJ_27 ----
          OBJ_27_OBJ_27.setText("Diff\u00e9rence");
          OBJ_27_OBJ_27.setFont(OBJ_27_OBJ_27.getFont().deriveFont(OBJ_27_OBJ_27.getFont().getStyle() | Font.BOLD));
          OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");
          
          // ---- WDIER ----
          WDIER.setFont(WDIER.getFont().deriveFont(WDIER.getFont().getStyle() | Font.BOLD));
          WDIER.setName("WDIER");
          
          // ---- label9 ----
          label9.setText("\"      \"");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getSize() + 2f));
          label9.setName("label9");
          
          // ---- label10 ----
          label10.setText("\"      \"");
          label10.setFont(label10.getFont().deriveFont(label10.getFont().getSize() + 2f));
          label10.setName("label10");
          
          // ---- label11 ----
          label11.setText("\"      \"");
          label11.setFont(label11.getFont().deriveFont(label11.getFont().getSize() + 2f));
          label11.setName("label11");
          
          // ---- label12 ----
          label12.setText("\"      \"");
          label12.setFont(label12.getFont().deriveFont(label12.getFont().getSize() + 2f));
          label12.setName("label12");
          
          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup().addGap(105, 105, 105)
                  .addComponent(label1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(label3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE).addGap(145, 145, 145)
                  .addComponent(label4, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(label5, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                  .addComponent(label6))
              .addGroup(panel1Layout.createSequentialGroup().addGap(30, 30, 30)
                  .addComponent(label7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                  .addComponent(CGE01, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup().addGap(75, 75, 75).addComponent(LIE01, GroupLayout.PREFERRED_SIZE,
                          260, GroupLayout.PREFERRED_SIZE))
                      .addComponent(MTE01, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WSAN01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAFF01, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WNAT01, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(
                  panel1Layout.createSequentialGroup().addGap(30, 30, 30).addComponent(label9).addGap(36, 36, 36)
                      .addComponent(CGE02, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(MTE02, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup().addGap(75, 75, 75).addComponent(LIE02,
                              GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                      .addComponent(WSAN02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT02, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(
                  panel1Layout.createSequentialGroup().addGap(30, 30, 30).addComponent(label10).addGap(36, 36, 36)
                      .addComponent(CGE03, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(MTE03, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup().addGap(75, 75, 75).addComponent(LIE03,
                              GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                      .addComponent(WSAN03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF03, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT03, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(
                  panel1Layout.createSequentialGroup().addGap(30, 30, 30).addComponent(label11).addGap(36, 36, 36)
                      .addComponent(CGE04, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(MTE04, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup().addGap(75, 75, 75).addComponent(LIE04,
                              GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                      .addComponent(WSAN04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF04, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT04, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(
                  panel1Layout.createSequentialGroup().addGap(30, 30, 30).addComponent(label12).addGap(36, 36, 36)
                      .addComponent(CGE05, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(MTE05, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup().addGap(75, 75, 75).addComponent(LIE05,
                              GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                      .addComponent(WSAN05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF05, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT05, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup().addGap(30, 30, 30)
                  .addComponent(label8, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                  .addComponent(C6CGT, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGroup(
                      panel1Layout.createParallelGroup().addComponent(C6MTT, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup().addGap(75, 75, 75).addComponent(C6LIT,
                              GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))))
              .addGroup(panel1Layout.createSequentialGroup().addGap(30, 30, 30)
                  .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup().addGap(70, 70, 70).addComponent(WDIER, GroupLayout.PREFERRED_SIZE,
                          76, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_27_OBJ_27, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))));
          panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(panel1Layout.createParallelGroup().addComponent(label1).addComponent(label2).addComponent(label3)
                  .addComponent(label4).addComponent(label5).addComponent(label6))
              .addGap(4, 4, 4)
              .addGroup(
                  panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(label7))
                      .addComponent(CGE01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIE01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(MTE01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WSAN01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(2, 2, 2)
              .addGroup(
                  panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(label9))
                      .addComponent(CGE02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(MTE02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIE02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WSAN02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(2, 2, 2)
              .addGroup(
                  panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(label10))
                      .addComponent(CGE03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(MTE03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIE03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WSAN03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(2, 2, 2)
              .addGroup(
                  panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(label11))
                      .addComponent(CGE04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(MTE04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIE04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WSAN04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(2, 2, 2)
              .addGroup(
                  panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(label12))
                      .addComponent(CGE05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(MTE05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIE05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WSAN05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WAFF05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNAT05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(2, 2, 2)
              .addGroup(
                  panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(label8))
                      .addComponent(C6CGT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(C6MTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(C6LIT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(12, 12, 12)
              .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(WDIER, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_27_OBJ_27, GroupLayout.PREFERRED_SIZE,
                      20, GroupLayout.PREFERRED_SIZE)))));
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 685, 270);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField CGE01;
  private XRiTextField MTE01;
  private XRiTextField LIE01;
  private XRiTextField WSAN01;
  private XRiTextField WAFF01;
  private XRiTextField WNAT01;
  private JLabel label7;
  private XRiTextField CGE02;
  private XRiTextField MTE02;
  private XRiTextField LIE02;
  private XRiTextField WSAN02;
  private XRiTextField WAFF2;
  private XRiTextField WNAT02;
  private XRiTextField CGE03;
  private XRiTextField MTE03;
  private XRiTextField LIE03;
  private XRiTextField WSAN03;
  private XRiTextField WAFF03;
  private XRiTextField WNAT03;
  private XRiTextField CGE04;
  private XRiTextField MTE04;
  private XRiTextField LIE04;
  private XRiTextField WSAN04;
  private XRiTextField WAFF04;
  private XRiTextField WNAT04;
  private XRiTextField CGE05;
  private XRiTextField MTE05;
  private XRiTextField LIE05;
  private XRiTextField WSAN05;
  private XRiTextField WAFF05;
  private XRiTextField WNAT05;
  private XRiTextField C6CGT;
  private XRiTextField C6MTT;
  private XRiTextField C6LIT;
  private JLabel label8;
  private JLabel OBJ_27_OBJ_27;
  private XRiTextField WDIER;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
