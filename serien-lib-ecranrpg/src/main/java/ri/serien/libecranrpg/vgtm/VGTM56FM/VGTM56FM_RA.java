
package ri.serien.libecranrpg.vgtm.VGTM56FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM56FM_RA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _ZTW01_Top =
      { "ZTW01", "ZTW02", "ZTW03", "ZTW04", "ZTW05", "ZTW06", "ZTW07", "ZTW08", "ZTW09", "ZTW10", "ZTW11", "ZTW12", "ZTW13", "ZTW14", };
  private String[] _ZTW01_Title = { "HLDRA", };
  private String[][] _ZTW01_Data = { { "LZ01", }, { "LZ02", }, { "LZ03", }, { "LZ04", }, { "LZ05", }, { "LZ06", }, { "LZ07", }, { "LZ08", },
      { "LZ09", }, { "LZ10", }, { "LZ11", }, { "LZ12", }, { "LZ13", }, { "LZ14", } };
  private int[] _ZTW01_Width = { 541, };
  
  public VGTM56FM_RA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ZTW01.setAspectTable(_ZTW01_Top, _ZTW01_Title, _ZTW01_Data, _ZTW01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WIREF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WIREF@")).trim());
    WIDAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WIDAT@")).trim());
    RAPBRD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RAPBRD@")).trim());
    RAPECR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RAPECR@")).trim());
    RAPPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RAPPOS@")).trim());
    xTitledPanel2.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Solde au @WMOIX@")).trim());
    RAPDIF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RAPDIF@")).trim());
    RAPCPT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RAPCPT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _ZTW01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    WIDAT.setEnabled(lexique.isPresent("WIDAT"));
    WIREF.setEnabled(lexique.isPresent("WIREF"));
    RAPPOS.setEnabled(lexique.isPresent("RAPPOS"));
    RAPCPT.setEnabled(lexique.isPresent("RAPCPT"));
    RAPDIF.setEnabled(lexique.isPresent("RAPDIF"));
    RAPECR.setEnabled(lexique.isPresent("RAPECR"));
    RAPBRD.setEnabled(lexique.isPresent("RAPBRD"));
    
    OBJ_33.setVisible(lexique.HostFieldGetData("ZIRCH").trim().equalsIgnoreCase("3"));
    OBJ_32.setVisible(lexique.HostFieldGetData("ZIRCH").trim().equalsIgnoreCase("2"));
    OBJ_31.setVisible(lexique.HostFieldGetData("ZIRCH").trim().equalsIgnoreCase("1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", false);
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _ZTW01_Top, "E", "Enter");
    ZTW01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void ZTW01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _ZTW01_Top, "E", "Enter", e);
    ZTW01.setValeurTop("E");
    if (ZTW01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_51 = new JLabel();
    WISOC = new XRiTextField();
    WIJOL = new XRiTextField();
    OBJ_54 = new JLabel();
    WMOIX1 = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_32 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST7 = new JScrollPane();
    ZTW01 = new XRiTable();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_73 = new JLabel();
    WIREF = new RiZoneSortie();
    OBJ_75 = new JLabel();
    WIDAT = new RiZoneSortie();
    RAPBRD = new RiZoneSortie();
    OBJ_81 = new JLabel();
    OBJ_85 = new JLabel();
    RAPECR = new RiZoneSortie();
    RAPPOS = new RiZoneSortie();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_79 = new JLabel();
    RAPDIF = new RiZoneSortie();
    RAPCPT = new RiZoneSortie();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_25 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Rapprochement bancaire");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_51 ----
          OBJ_51.setText("Soci\u00e9t\u00e9");
          OBJ_51.setName("OBJ_51");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(BTD);
          WISOC.setName("WISOC");

          //---- WIJOL ----
          WIJOL.setName("WIJOL");

          //---- OBJ_54 ----
          OBJ_54.setText("au");
          OBJ_54.setName("OBJ_54");

          //---- WMOIX1 ----
          WMOIX1.setName("WMOIX1");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WIJOL, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WMOIX1, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIJOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WMOIX1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_31 ----
          OBJ_31.setText("Tri des \u00e9critures par date");
          OBJ_31.setName("OBJ_31");
          p_tete_droite.add(OBJ_31);

          //---- OBJ_33 ----
          OBJ_33.setText("Tri des \u00e9critures par pi\u00e8ces");
          OBJ_33.setName("OBJ_33");
          p_tete_droite.add(OBJ_33);

          //---- OBJ_32 ----
          OBJ_32.setText("Tri des \u00e9critures par montants");
          OBJ_32.setName("OBJ_32");
          p_tete_droite.add(OBJ_32);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recherche d'\u00e9critures");
              riSousMenu_bt6.setToolTipText("Recherche d'\u00e9critures");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Edition \u00e9tat rappro.");
              riSousMenu_bt7.setToolTipText("Edition de l'\u00e9tat de rapprochement");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Affichage");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Ecritures par dates");
              riSousMenu_bt18.setToolTipText("Affichage des \u00e9critures ( en bas) par dates");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Ecritures par montants");
              riSousMenu_bt19.setToolTipText("Affichage des \u00e9critures par montants");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);

            //======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");

              //---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Ecritures par pi\u00e8ces");
              riSousMenu_bt20.setToolTipText("Affichage des \u00e9critures  par pi\u00e8ces");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //======== SCROLLPANE_LIST7 ========
            {
              SCROLLPANE_LIST7.setName("SCROLLPANE_LIST7");

              //---- ZTW01 ----
              ZTW01.setComponentPopupMenu(BTD);
              ZTW01.setName("ZTW01");
              ZTW01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  ZTW01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST7.setViewportView(ZTW01);
            }

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("Solde total");
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

              //---- OBJ_73 ----
              OBJ_73.setText("Bordereau");
              OBJ_73.setName("OBJ_73");

              //---- WIREF ----
              WIREF.setBorder(new BevelBorder(BevelBorder.LOWERED));
              WIREF.setText("@WIREF@");
              WIREF.setName("WIREF");

              //---- OBJ_75 ----
              OBJ_75.setText("du");
              OBJ_75.setName("OBJ_75");

              //---- WIDAT ----
              WIDAT.setText("@WIDAT@");
              WIDAT.setName("WIDAT");

              //---- RAPBRD ----
              RAPBRD.setBorder(new BevelBorder(BevelBorder.LOWERED));
              RAPBRD.setText("@RAPBRD@");
              RAPBRD.setHorizontalAlignment(SwingConstants.RIGHT);
              RAPBRD.setName("RAPBRD");

              //---- OBJ_81 ----
              OBJ_81.setText("Ecritures non rapproch\u00e9es");
              OBJ_81.setName("OBJ_81");

              //---- OBJ_85 ----
              OBJ_85.setText("Position en banque calcul\u00e9e");
              OBJ_85.setName("OBJ_85");

              //---- RAPECR ----
              RAPECR.setBorder(new BevelBorder(BevelBorder.LOWERED));
              RAPECR.setText("@RAPECR@");
              RAPECR.setHorizontalAlignment(SwingConstants.RIGHT);
              RAPECR.setName("RAPECR");

              //---- RAPPOS ----
              RAPPOS.setBorder(new BevelBorder(BevelBorder.LOWERED));
              RAPPOS.setText("@RAPPOS@");
              RAPPOS.setHorizontalAlignment(SwingConstants.RIGHT);
              RAPPOS.setName("RAPPOS");

              GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
              xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
              xTitledPanel1ContentContainerLayout.setHorizontalGroup(
                xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(WIREF, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(WIDAT, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(RAPBRD, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                        .addGap(115, 115, 115)
                        .addComponent(RAPECR, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addComponent(OBJ_85)
                        .addGap(119, 119, 119)
                        .addComponent(RAPPOS, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))))
              );
              xTitledPanel1ContentContainerLayout.setVerticalGroup(
                xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                      .addComponent(WIREF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WIDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(RAPBRD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                    .addGap(9, 9, 9)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(RAPECR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(8, 8, 8)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(RAPPOS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setTitle("Solde au @WMOIX@");
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

              //---- OBJ_79 ----
              OBJ_79.setText("Position comptable");
              OBJ_79.setName("OBJ_79");

              //---- RAPDIF ----
              RAPDIF.setBorder(new BevelBorder(BevelBorder.LOWERED));
              RAPDIF.setText("@RAPDIF@");
              RAPDIF.setHorizontalAlignment(SwingConstants.RIGHT);
              RAPDIF.setForeground(Color.red);
              RAPDIF.setName("RAPDIF");

              //---- RAPCPT ----
              RAPCPT.setBorder(new BevelBorder(BevelBorder.LOWERED));
              RAPCPT.setText("@RAPCPT@");
              RAPCPT.setHorizontalAlignment(SwingConstants.RIGHT);
              RAPCPT.setName("RAPCPT");

              GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
              xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
              xTitledPanel2ContentContainerLayout.setHorizontalGroup(
                xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addComponent(RAPDIF, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(RAPCPT, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))))
              );
              xTitledPanel2ContentContainerLayout.setVerticalGroup(
                xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(RAPDIF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(RAPCPT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(SCROLLPANE_LIST7, GroupLayout.PREFERRED_SIZE, 687, GroupLayout.PREFERRED_SIZE)
                      .addGap(8, 8, 8)
                      .addGroup(panel2Layout.createParallelGroup()
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(SCROLLPANE_LIST7, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                      .addGap(14, 14, 14)
                      .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))
                  .addGap(26, 26, 26)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Saisie \u00e9critures pour ajustement au premier rapprochement");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      BTD.addSeparator();

      //---- OBJ_26 ----
      OBJ_26.setText("Choix possibles");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_51;
  private XRiTextField WISOC;
  private XRiTextField WIJOL;
  private JLabel OBJ_54;
  private XRiTextField WMOIX1;
  private JPanel p_tete_droite;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_32;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST7;
  private XRiTable ZTW01;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_73;
  private RiZoneSortie WIREF;
  private JLabel OBJ_75;
  private RiZoneSortie WIDAT;
  private RiZoneSortie RAPBRD;
  private JLabel OBJ_81;
  private JLabel OBJ_85;
  private RiZoneSortie RAPECR;
  private RiZoneSortie RAPPOS;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_79;
  private RiZoneSortie RAPDIF;
  private RiZoneSortie RAPCPT;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_25;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
