
package ri.serien.libecranrpg.vgtm.VGTM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] AFTFA_Value = { "", "F", "A", };
  private String[] AFTRG_Value = { "", "1", "2", "3", "4", "5", "6", };
  
  public VGTM05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    AFTRG.setValeurs(AFTRG_Value, null);
    AFTFA.setValeurs(AFTFA_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    AFSRE.setEnabled(lexique.isPresent("AFSRE"));
    AFNRE.setEnabled(lexique.isPresent("AFNRE"));
    AFBRE.setEnabled(lexique.isPresent("AFBRE"));
    AFAFA.setEnabled(lexique.isPresent("AFAFA"));
    AFNC3X.setEnabled(lexique.isPresent("AFNC3X"));
    AFNC2X.setEnabled(lexique.isPresent("AFNC2X"));
    AFQUI.setEnabled(lexique.isPresent("AFQUI"));
    AFNCA.setEnabled(lexique.isPresent("AFNCA"));
    AFNCG.setVisible(lexique.isPresent("AFNCG"));
    AFFAR.setEnabled(lexique.isPresent("AFFAR"));
    AFRLV.setEnabled(lexique.isPresent("AFRLV"));
    AFDREX.setEnabled(lexique.isPresent("AFDREX"));
    AFDECX.setEnabled(lexique.isPresent("AFDECX"));
    AFFACX.setEnabled(lexique.isPresent("AFFACX"));
    AFNCC.setEnabled(lexique.isPresent("AFNCC"));
    AFMTT.setEnabled(lexique.isPresent("AFMTT"));
    AFCLA.setEnabled(lexique.isPresent("AFCLA"));
    AFRCC.setEnabled(lexique.isPresent("AFRCC"));
    AFAD3.setEnabled(lexique.isPresent("AFAD3"));
    AFAD2.setEnabled(lexique.isPresent("AFAD2"));
    AFAD1.setEnabled(lexique.isPresent("AFAD1"));
    AFNOM.setEnabled(lexique.isPresent("AFNOM"));
    // AFTFA.setEnabled( lexique.isPresent("AFTFA"));
    // AFTRG.setEnabled( lexique.isPresent("AFTRG"));
    // AFTFA.setSelectedIndex(getIndice("AFTFA", AFTFA_Value));
    // AFTRG.setSelectedIndex(getIndice("AFTRG", AFTRG_Value));
    OBJ_88.setVisible(lexique.isTrue("N31"));
    OBJ_89.setVisible(lexique.isTrue("31"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("AFTFA", 0, AFTFA_Value[AFTFA.getSelectedIndex()]);
    // lexique.HostFieldPutData("AFTRG", 0, AFTRG_Value[AFTRG.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_29 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_80 = new JLabel();
    INDNUM = new XRiTextField();
    INDSUF = new XRiTextField();
    OBJ_106 = new JLabel();
    AFTFA = new XRiComboBox();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    AFMTT = new XRiTextField();
    AFFACX = new XRiTextField();
    AFDECX = new XRiTextField();
    AFTRG = new XRiComboBox();
    AFAFA = new XRiTextField();
    AFNCC = new XRiTextField();
    AFRCC = new XRiTextField();
    AFRLV = new XRiTextField();
    AFFAR = new XRiTextField();
    AFDREX = new XRiTextField();
    AFBRE = new XRiTextField();
    AFNRE = new XRiTextField();
    AFSRE = new XRiTextField();
    AFNC2X = new XRiTextField();
    AFQUI = new XRiTextField();
    AFNC3X = new XRiTextField();
    OBJ_72 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_74 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    AFNCG = new XRiTextField();
    AFNCA = new XRiTextField();
    AFCLA = new XRiTextField();
    AFNOM = new XRiTextField();
    AFAD1 = new XRiTextField();
    AFAD2 = new XRiTextField();
    AFAD3 = new XRiTextField();
    OBJ_91 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_89 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion de l'affacturage");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_29 ----
          OBJ_29.setText("Etablissement");
          OBJ_29.setName("OBJ_29");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_80 ----
          OBJ_80.setText("Facture");
          OBJ_80.setName("OBJ_80");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");

          //---- OBJ_106 ----
          OBJ_106.setText("Type de document");
          OBJ_106.setName("OBJ_106");

          //---- AFTFA ----
          AFTFA.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Facture",
            "Avoir"
          }));
          AFTFA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AFTFA.setName("AFTFA");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_106, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AFTFA, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(250, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_106, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AFTFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Cessions factures");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Cessions avoirs");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(760, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Facture");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- AFMTT ----
            AFMTT.setComponentPopupMenu(BTD);
            AFMTT.setName("AFMTT");

            //---- AFFACX ----
            AFFACX.setComponentPopupMenu(BTD);
            AFFACX.setName("AFFACX");

            //---- AFDECX ----
            AFDECX.setComponentPopupMenu(BTD);
            AFDECX.setName("AFDECX");

            //---- AFTRG ----
            AFTRG.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Virement postal CCP",
              "Ch\u00e8que bancaire",
              "Virement interbancaire",
              "Billet \u00e0 ordre",
              "Effet accept\u00e9",
              "LCR"
            }));
            AFTRG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AFTRG.setName("AFTRG");

            //---- AFAFA ----
            AFAFA.setComponentPopupMenu(BTD);
            AFAFA.setName("AFAFA");

            //---- AFNCC ----
            AFNCC.setComponentPopupMenu(BTD);
            AFNCC.setName("AFNCC");

            //---- AFRCC ----
            AFRCC.setComponentPopupMenu(BTD);
            AFRCC.setName("AFRCC");

            //---- AFRLV ----
            AFRLV.setComponentPopupMenu(BTD);
            AFRLV.setName("AFRLV");

            //---- AFFAR ----
            AFFAR.setComponentPopupMenu(BTD);
            AFFAR.setName("AFFAR");

            //---- AFDREX ----
            AFDREX.setComponentPopupMenu(BTD);
            AFDREX.setName("AFDREX");

            //---- AFBRE ----
            AFBRE.setComponentPopupMenu(BTD);
            AFBRE.setName("AFBRE");

            //---- AFNRE ----
            AFNRE.setComponentPopupMenu(BTD);
            AFNRE.setName("AFNRE");

            //---- AFSRE ----
            AFSRE.setName("AFSRE");

            //---- AFNC2X ----
            AFNC2X.setComponentPopupMenu(BTD);
            AFNC2X.setName("AFNC2X");

            //---- AFQUI ----
            AFQUI.setComponentPopupMenu(BTD);
            AFQUI.setName("AFQUI");

            //---- AFNC3X ----
            AFNC3X.setName("AFNC3X");

            //---- OBJ_72 ----
            OBJ_72.setText("Num\u00e9ro facture rattach\u00e9e");
            OBJ_72.setName("OBJ_72");

            //---- OBJ_86 ----
            OBJ_86.setText("R\u00e8glement \u00e0 \u00e9ch\u00e9ance");
            OBJ_86.setName("OBJ_86");

            //---- OBJ_98 ----
            OBJ_98.setText("Type de r\u00e8glement");
            OBJ_98.setName("OBJ_98");

            //---- OBJ_70 ----
            OBJ_70.setText("Num\u00e9ro relev\u00e9");
            OBJ_70.setName("OBJ_70");

            //---- OBJ_68 ----
            OBJ_68.setText("Code Factor");
            OBJ_68.setName("OBJ_68");

            //---- OBJ_69 ----
            OBJ_69.setText("R\u00e9f\u00e9rences");
            OBJ_69.setName("OBJ_69");

            //---- OBJ_67 ----
            OBJ_67.setText("Ech\u00e9ance");
            OBJ_67.setName("OBJ_67");

            //---- OBJ_79 ----
            OBJ_79.setText("Cession");
            OBJ_79.setName("OBJ_79");

            //---- OBJ_77 ----
            OBJ_77.setText("Compta");
            OBJ_77.setName("OBJ_77");

            //---- OBJ_66 ----
            OBJ_66.setText("Dat.Fact");
            OBJ_66.setName("OBJ_66");

            //---- OBJ_65 ----
            OBJ_65.setText("Montant");
            OBJ_65.setName("OBJ_65");

            //---- OBJ_78 ----
            OBJ_78.setText("N\u00b0quit");
            OBJ_78.setName("OBJ_78");

            //---- OBJ_75 ----
            OBJ_75.setText("Type");
            OBJ_75.setName("OBJ_75");

            //---- OBJ_73 ----
            OBJ_73.setText("Date");
            OBJ_73.setName("OBJ_73");

            //---- OBJ_76 ----
            OBJ_76.setText("Suf");
            OBJ_76.setName("OBJ_76");

            //---- OBJ_74 ----
            OBJ_74.setText("Bq");
            OBJ_74.setName("OBJ_74");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                      .addGap(104, 104, 104)
                      .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                      .addGap(66, 66, 66)
                      .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(AFMTT, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                      .addGap(47, 47, 47)
                      .addComponent(AFFACX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(AFDECX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(54, 54, 54)
                      .addComponent(AFTRG, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(AFAFA, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFFAR, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFRLV, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFNCC, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                      .addGap(27, 27, 27)
                      .addComponent(AFRCC, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                      .addGap(103, 103, 103)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFDREX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                      .addGap(9, 9, 9)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(AFBRE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                      .addGap(14, 14, 14)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFNRE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFSRE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                      .addGap(14, 14, 14)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFNC2X, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                      .addGap(61, 61, 61)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFQUI, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
                      .addGap(224, 224, 224)
                      .addComponent(AFNC3X, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(162, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(AFMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFFACX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(AFDECX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(8, 8, 8)
                      .addComponent(AFTRG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(AFAFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(26, 26, 26)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(27, 27, 27)
                          .addComponent(AFFAR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(AFRLV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(27, 27, 27)
                      .addComponent(AFNCC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(27, 27, 27)
                      .addComponent(AFRCC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(AFDREX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(21, 21, 21)
                      .addComponent(AFBRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(AFNRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(AFSRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(AFNC2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(AFQUI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFNC3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(13, Short.MAX_VALUE))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Tiers");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- AFNCG ----
            AFNCG.setComponentPopupMenu(BTD);
            AFNCG.setName("AFNCG");
            xTitledPanel2ContentContainer.add(AFNCG);
            AFNCG.setBounds(29, 71, 60, AFNCG.getPreferredSize().height);

            //---- AFNCA ----
            AFNCA.setComponentPopupMenu(BTD);
            AFNCA.setName("AFNCA");
            xTitledPanel2ContentContainer.add(AFNCA);
            AFNCA.setBounds(96, 71, 60, AFNCA.getPreferredSize().height);

            //---- AFCLA ----
            AFCLA.setComponentPopupMenu(BTD);
            AFCLA.setName("AFCLA");
            xTitledPanel2ContentContainer.add(AFCLA);
            AFCLA.setBounds(174, 71, 160, AFCLA.getPreferredSize().height);

            //---- AFNOM ----
            AFNOM.setComponentPopupMenu(BTD);
            AFNOM.setName("AFNOM");
            xTitledPanel2ContentContainer.add(AFNOM);
            AFNOM.setBounds(352, 36, 310, AFNOM.getPreferredSize().height);

            //---- AFAD1 ----
            AFAD1.setName("AFAD1");
            xTitledPanel2ContentContainer.add(AFAD1);
            AFAD1.setBounds(352, 71, 310, AFAD1.getPreferredSize().height);

            //---- AFAD2 ----
            AFAD2.setName("AFAD2");
            xTitledPanel2ContentContainer.add(AFAD2);
            AFAD2.setBounds(352, 106, 310, AFAD2.getPreferredSize().height);

            //---- AFAD3 ----
            AFAD3.setName("AFAD3");
            xTitledPanel2ContentContainer.add(AFAD3);
            AFAD3.setBounds(352, 141, 310, AFAD3.getPreferredSize().height);

            //---- OBJ_91 ----
            OBJ_91.setText("Nom ou raison sociale");
            OBJ_91.setName("OBJ_91");
            xTitledPanel2ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(352, 5, 163, 18);

            //---- OBJ_88 ----
            OBJ_88.setText("Num\u00e9ro de compte");
            OBJ_88.setName("OBJ_88");
            xTitledPanel2ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(29, 40, 129, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("Classement");
            OBJ_90.setName("OBJ_90");
            xTitledPanel2ContentContainer.add(OBJ_90);
            OBJ_90.setBounds(177, 40, 93, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("Num\u00e9ro");
            OBJ_89.setName("OBJ_89");
            xTitledPanel2ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(96, 40, 60, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(xTitledPanel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE)
                  .addComponent(xTitledPanel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_29;
  private XRiTextField INDETB;
  private JLabel OBJ_80;
  private XRiTextField INDNUM;
  private XRiTextField INDSUF;
  private JLabel OBJ_106;
  private XRiComboBox AFTFA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField AFMTT;
  private XRiTextField AFFACX;
  private XRiTextField AFDECX;
  private XRiComboBox AFTRG;
  private XRiTextField AFAFA;
  private XRiTextField AFNCC;
  private XRiTextField AFRCC;
  private XRiTextField AFRLV;
  private XRiTextField AFFAR;
  private XRiTextField AFDREX;
  private XRiTextField AFBRE;
  private XRiTextField AFNRE;
  private XRiTextField AFSRE;
  private XRiTextField AFNC2X;
  private XRiTextField AFQUI;
  private XRiTextField AFNC3X;
  private JLabel OBJ_72;
  private JLabel OBJ_86;
  private JLabel OBJ_98;
  private JLabel OBJ_70;
  private JLabel OBJ_68;
  private JLabel OBJ_69;
  private JLabel OBJ_67;
  private JLabel OBJ_79;
  private JLabel OBJ_77;
  private JLabel OBJ_66;
  private JLabel OBJ_65;
  private JLabel OBJ_78;
  private JLabel OBJ_75;
  private JLabel OBJ_73;
  private JLabel OBJ_76;
  private JLabel OBJ_74;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField AFNCG;
  private XRiTextField AFNCA;
  private XRiTextField AFCLA;
  private XRiTextField AFNOM;
  private XRiTextField AFAD1;
  private XRiTextField AFAD2;
  private XRiTextField AFAD3;
  private JLabel OBJ_91;
  private JLabel OBJ_88;
  private JLabel OBJ_90;
  private JLabel OBJ_89;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
