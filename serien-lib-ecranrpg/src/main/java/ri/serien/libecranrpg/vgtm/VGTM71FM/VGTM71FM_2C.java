
package ri.serien.libecranrpg.vgtm.VGTM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM71FM_2C extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] L1SNS_Value = { "", "D", "C", };
  
  /**
   * Constructeur.
   */
  public VGTM71FM_2C(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    L1SNS.setValeurs(L1SNS_Value, null);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    W1SNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1SNS@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    TTVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTVA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    WCHGX.setVisible(lexique.isTrue("75"));
    L1DEV.setVisible(lexique.isTrue("75"));
    L1MTD.setVisible(lexique.isTrue("75"));
    OBJ_28.setVisible(lexique.isPresent("LIBMTT"));
    L1QTE.setVisible(lexique.isTrue("76"));
    OBJ_62.setVisible(lexique.isTrue("75"));
    OBJ_63.setVisible(lexique.isTrue("76"));
    OBJ_64.setVisible(lexique.isTrue("75"));
    
    panel1.setVisible(lexique.isTrue("74"));
    
    OBJ_68.setVisible(lexique.isTrue("77"));
    if (lexique.isTrue("79")) {
      OBJ_68.setForeground(Color.RED);
    }
    else {
      OBJ_68.setForeground(Color.BLACK);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@V01F@"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 13);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 11);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", true);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WCOD = new XRiTextField();
    L1NLI = new XRiTextField();
    L1NCG = new XRiTextField();
    L1NCA = new XRiTextField();
    LIBCPT = new XRiTextField();
    USOLDE = new XRiTextField();
    OBJ_67 = new JLabel();
    W1SNS = new RiZoneSortie();
    OBJ_68 = new JLabel();
    panel3 = new JPanel();
    L1DTJ = new XRiTextField();
    L1SNS = new XRiComboBox();
    L1CLB = new XRiTextField();
    L1MTT = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    L1PCE = new XRiTextField();
    L1DEV = new XRiTextField();
    WCHGX = new XRiTextField();
    L1MTD = new XRiTextField();
    L1QTE = new XRiTextField();
    OBJ_31 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_63 = new JLabel();
    panel1 = new JPanel();
    L1SAN = new XRiTextField();
    L1ACT = new XRiTextField();
    L1NAT = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_32 = new JLabel();
    L1LIB8 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    L1LIB9 = new XRiTextField();
    label6 = new JLabel();
    label5 = new JLabel();
    panel4 = new JPanel();
    L1IN6 = new XRiTextField();
    OBJ_53 = new JLabel();
    TTVA = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1185, 255));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 180));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Vue des comptes");
              riSousMenu_bt6.setToolTipText("Vue des comptes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Plan comptable");
              riSousMenu_bt7.setToolTipText("Plan comptable");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Curseur sur le jour");
              riSousMenu_bt8.setToolTipText("Mise En/Hors fonction curseur positionn\u00e9 sur le jour");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Saisie zones libell\u00e9s");
              riSousMenu_bt9.setToolTipText("Saisie des zones : code libell\u00e9  et  libell\u00e9");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Affectations r\u00e8glements");
              riSousMenu_bt10.setToolTipText("Mise En/Hors fonction des affectations de r\u00e8glements");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("For\u00e7age en devises");
              riSousMenu_bt11.setToolTipText("For\u00e7age en saisie sur comptes en devises");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Documents li\u00e9s");
              riSousMenu_bt14.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes");
              riSousMenu_bt15.setToolTipText("Bloc-notes");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel2 ========
        {
          panel2.setBorder(new DropShadowBorder());
          panel2.setOpaque(false);
          panel2.setName("panel2");
          
          // ---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setName("WCOD");
          
          // ---- L1NLI ----
          L1NLI.setComponentPopupMenu(BTD);
          L1NLI.setName("L1NLI");
          
          // ---- L1NCG ----
          L1NCG.setComponentPopupMenu(BTD);
          L1NCG.setName("L1NCG");
          
          // ---- L1NCA ----
          L1NCA.setComponentPopupMenu(BTD);
          L1NCA.setName("L1NCA");
          
          // ---- LIBCPT ----
          LIBCPT.setEditable(false);
          LIBCPT.setName("LIBCPT");
          
          // ---- USOLDE ----
          USOLDE.setComponentPopupMenu(BTD);
          USOLDE.setName("USOLDE");
          
          // ---- OBJ_67 ----
          OBJ_67.setText("Solde");
          OBJ_67.setName("OBJ_67");
          
          // ---- W1SNS ----
          W1SNS.setText("@W1SNS@");
          W1SNS.setName("W1SNS");
          
          // ---- OBJ_68 ----
          OBJ_68.setText("@W1ATT@");
          OBJ_68.setForeground(Color.red);
          OBJ_68.setName("OBJ_68");
          
          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                  .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15)
                          .addComponent(WCOD, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                          .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                          .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                          .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15).addComponent(LIBCPT, GroupLayout.PREFERRED_SIZE,
                          310, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup().addGap(17, 17, 17)
                          .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(19, 19, 19)
                          .addComponent(USOLDE, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE).addGap(14, 14, 14)
                          .addComponent(W1SNS, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15).addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE,
                          310, GroupLayout.PREFERRED_SIZE)))
                  .addGap(10, 10, 10)));
          panel2Layout.setVerticalGroup(panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15)
                  .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WCOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                          .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2).addComponent(LIBCPT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(93, 93, 93)
                  .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20,
                          GroupLayout.PREFERRED_SIZE))
                      .addComponent(USOLDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(W1SNS, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(6, 6, 6).addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)));
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 340, 235);
        
        // ======== panel3 ========
        {
          panel3.setBorder(new DropShadowBorder());
          panel3.setOpaque(false);
          panel3.setName("panel3");
          
          // ---- L1DTJ ----
          L1DTJ.setComponentPopupMenu(BTD);
          L1DTJ.setName("L1DTJ");
          
          // ---- L1SNS ----
          L1SNS.setModel(new DefaultComboBoxModel(new String[] { "  ", "D\u00e9bit", "Cr\u00e9dit" }));
          L1SNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1SNS.setName("L1SNS");
          
          // ---- L1CLB ----
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");
          
          // ---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setName("L1MTT");
          
          // ---- OBJ_28 ----
          OBJ_28.setText("@LIBMTT@");
          OBJ_28.setName("OBJ_28");
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Sens");
          OBJ_29.setName("OBJ_29");
          
          // ---- OBJ_30 ----
          OBJ_30.setText("C");
          OBJ_30.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_30.setName("OBJ_30");
          
          // ---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setName("L1PCE");
          
          // ---- L1DEV ----
          L1DEV.setName("L1DEV");
          
          // ---- WCHGX ----
          WCHGX.setName("WCHGX");
          
          // ---- L1MTD ----
          L1MTD.setComponentPopupMenu(BTD);
          L1MTD.setName("L1MTD");
          
          // ---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setName("L1QTE");
          
          // ---- OBJ_31 ----
          OBJ_31.setText("N\u00b0pi\u00e8ce");
          OBJ_31.setName("OBJ_31");
          
          // ---- OBJ_64 ----
          OBJ_64.setText("Devise");
          OBJ_64.setName("OBJ_64");
          
          // ---- OBJ_62 ----
          OBJ_62.setText("Taux");
          OBJ_62.setName("OBJ_62");
          
          // ---- OBJ_35 ----
          OBJ_35.setText("le");
          OBJ_35.setName("OBJ_35");
          
          // ---- OBJ_63 ----
          OBJ_63.setText("Quantit\u00e9");
          OBJ_63.setName("OBJ_63");
          
          // ======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            
            // ---- L1SAN ----
            L1SAN.setComponentPopupMenu(BTD);
            L1SAN.setName("L1SAN");
            
            // ---- L1ACT ----
            L1ACT.setComponentPopupMenu(BTD);
            L1ACT.setName("L1ACT");
            
            // ---- L1NAT ----
            L1NAT.setComponentPopupMenu(BTD);
            L1NAT.setName("L1NAT");
            
            // ---- OBJ_34 ----
            OBJ_34.setText("Nature");
            OBJ_34.setName("OBJ_34");
            
            // ---- OBJ_33 ----
            OBJ_33.setText("Affaire");
            OBJ_33.setName("OBJ_33");
            
            // ---- OBJ_32 ----
            OBJ_32.setText("Section");
            OBJ_32.setName("OBJ_32");
            
            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup().addGap(20, 20, 20)
                    .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                            .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE).addGap(38, 38, 38)
                            .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(L1SAN, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                            .addComponent(L1ACT, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                            .addComponent(L1NAT, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))));
            panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5)
                    .addGroup(panel1Layout.createParallelGroup().addComponent(OBJ_32).addComponent(OBJ_33).addComponent(OBJ_34))
                    .addGap(9, 9, 9)
                    .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(L1SAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(L1ACT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(L1NAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
          }
          
          // ---- L1LIB8 ----
          L1LIB8.setName("L1LIB8");
          
          // ---- L1LIB2 ----
          L1LIB2.setName("L1LIB2");
          
          // ---- L1LIB9 ----
          L1LIB9.setName("L1LIB9");
          
          // ---- label6 ----
          label6.setText("Banque");
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          
          // ---- label5 ----
          label5.setText("Nom");
          label5.setName("label5");
          
          // ======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("TVA"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);
            
            // ---- L1IN6 ----
            L1IN6.setName("L1IN6");
            panel4.add(L1IN6);
            L1IN6.setBounds(20, 30, 24, L1IN6.getPreferredSize().height);
            
            // ---- OBJ_53 ----
            OBJ_53.setText("Taux");
            OBJ_53.setName("OBJ_53");
            panel4.add(OBJ_53);
            OBJ_53.setBounds(55, 35, 45, 19);
            
            // ---- TTVA ----
            TTVA.setText("@TTVA@");
            TTVA.setHorizontalAlignment(SwingConstants.RIGHT);
            TTVA.setName("TTVA");
            panel4.add(TTVA);
            TTVA.setBounds(100, 32, 44, TTVA.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout panel3Layout = new GroupLayout(panel3);
          panel3.setLayout(panel3Layout);
          panel3Layout.setHorizontalGroup(panel3Layout.createParallelGroup()
              .addGroup(panel3Layout.createSequentialGroup().addGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup().addGap(10, 10, 10).addGroup(panel3Layout.createParallelGroup()
                      .addGroup(panel3Layout.createSequentialGroup()
                          .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE).addGap(24, 24, 24)
                          .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE).addGap(54, 54, 54)
                          .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                          .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel3Layout
                          .createSequentialGroup().addComponent(L1DTJ, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                          .addGap(11, 11, 11).addComponent(L1SNS, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                          .addGap(13, 13, 13).addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGap(10, 10, 10).addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)))
                      .addGap(12, 12, 12)
                      .addGroup(panel3Layout.createParallelGroup()
                          .addGroup(panel3Layout.createSequentialGroup()
                              .addComponent(L1LIB8, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                              .addComponent(L1LIB2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                              .addComponent(L1LIB9, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(23, 23, 23)
                              .addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                          .addComponent(label5, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel3Layout.createSequentialGroup().addGap(385, 385, 385).addComponent(label6, GroupLayout.PREFERRED_SIZE,
                      125, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel3Layout.createSequentialGroup().addGap(538, 538, 538).addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE,
                      52, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel3Layout.createSequentialGroup().addGap(10, 10, 10)
                      .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                          .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel3Layout.createSequentialGroup()
                              .addGroup(panel3Layout.createParallelGroup()
                                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                  .addGroup(panel3Layout.createSequentialGroup().addGap(43, 43, 43).addComponent(OBJ_62,
                                      GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                                  .addGroup(panel3Layout.createSequentialGroup()
                                      .addComponent(L1DEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                                      .addComponent(WCHGX, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                              .addGap(1, 1, 1).addComponent(L1MTD, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                              .addGap(1, 1, 1)
                              .addGroup(panel3Layout.createParallelGroup()
                                  .addGroup(panel3Layout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_63,
                                      GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                                  .addComponent(L1QTE, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                              .addGap(32, 32, 32)
                              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
                  .addGap(15, 15, 15)));
          panel3Layout
              .setVerticalGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup().addGap(20, 20, 20)
                      .addGroup(panel3Layout.createParallelGroup()
                          .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel3Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_29))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_30))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_28))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(5, 5, 5)
                              .addGroup(panel3Layout.createParallelGroup().addComponent(label6).addComponent(label5)))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_31)))
                      .addGap(4, 4, 4)
                      .addGroup(panel3Layout.createParallelGroup()
                          .addComponent(L1DTJ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1SNS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1LIB8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1LIB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1LIB9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(7, 7, 7)
                      .addGroup(
                          panel3Layout.createParallelGroup()
                              .addGroup(panel3Layout
                                  .createSequentialGroup().addGap(5, 5, 5)
                                  .addGroup(panel3Layout.createParallelGroup()
                                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                      .addGroup(panel3Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_62)))
                                  .addGap(5, 5, 5)
                                  .addGroup(panel3Layout.createParallelGroup()
                                      .addComponent(L1DEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addComponent(WCHGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)))
                              .addGroup(panel3Layout.createSequentialGroup().addGap(30, 30, 30).addComponent(L1MTD,
                                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel3Layout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_63).addGap(7, 7, 7)
                                  .addComponent(L1QTE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addContainerGap()));
        }
        p_contenu.add(panel3);
        panel3.setBounds(355, 10, 650, 235);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Duplication");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField WCOD;
  private XRiTextField L1NLI;
  private XRiTextField L1NCG;
  private XRiTextField L1NCA;
  private XRiTextField LIBCPT;
  private XRiTextField USOLDE;
  private JLabel OBJ_67;
  private RiZoneSortie W1SNS;
  private JLabel OBJ_68;
  private JPanel panel3;
  private XRiTextField L1DTJ;
  private XRiComboBox L1SNS;
  private XRiTextField L1CLB;
  private XRiTextField L1MTT;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private XRiTextField L1PCE;
  private XRiTextField L1DEV;
  private XRiTextField WCHGX;
  private XRiTextField L1MTD;
  private XRiTextField L1QTE;
  private JLabel OBJ_31;
  private JLabel OBJ_64;
  private JLabel OBJ_62;
  private JLabel OBJ_35;
  private JLabel OBJ_63;
  private JPanel panel1;
  private XRiTextField L1SAN;
  private XRiTextField L1ACT;
  private XRiTextField L1NAT;
  private JLabel OBJ_34;
  private JLabel OBJ_33;
  private JLabel OBJ_32;
  private XRiTextField L1LIB8;
  private XRiTextField L1LIB2;
  private XRiTextField L1LIB9;
  private JLabel label6;
  private JLabel label5;
  private JPanel panel4;
  private XRiTextField L1IN6;
  private JLabel OBJ_53;
  private RiZoneSortie TTVA;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
