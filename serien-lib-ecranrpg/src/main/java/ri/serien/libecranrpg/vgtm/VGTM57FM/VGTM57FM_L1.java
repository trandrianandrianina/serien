
package ri.serien.libecranrpg.vgtm.VGTM57FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM57FM_L1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTX01_Top = { "WTX01", "WTX02", "WTX03", "WTX04", "WTX05", "WTX06", "WTX07", "WTX08", "WTX09", "WTX10", "WTX11",
      "WTX12", "WTX13", "WTX14", "WTX15", "WTX16", };
  private String[] _WTX01_Title = { "N° du Compte  Intitulé du Compte", };
  private String[][] _WTX01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, };
  private int[] _WTX01_Width = { 541, };
  
  public VGTM57FM_L1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTX01.setAspectTable(_WTX01_Top, _WTX01_Title, _WTX01_Data, _WTX01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTIT@ @JOLIB@")).trim());
    JOLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTX01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("78");
    


    
    
    OBJ_84.setVisible(lexique.isPresent("WCOD"));
    OBJ_67.setVisible(lexique.isPresent("L1NLI"));
    OBJ_69.setVisible(lexique.isPresent("LIBMTT"));
    labelAffectation.setVisible(lexique.isTrue("38"));
    OBJ_80.setVisible(lexique.isTrue("40"));
    WEDIF.setForeground(new Color(204, 0, 0));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @LOCTIT@ @JOLIB@"));
    
    

    
    p_bpresentation.setCodeEtablissement(E1SOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(E1SOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTX01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    WTX01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTX01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTX01MouseClicked(MouseEvent e) {
    if (WTX01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_37 = new JLabel();
    E1SOC = new XRiTextField();
    OBJ_43 = new JLabel();
    E1DTEX = new XRiCalendrier();
    OBJ_41 = new JLabel();
    E1CFO = new XRiTextField();
    OBJ_39 = new JLabel();
    E1CJO = new XRiTextField();
    JOLIB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    labelAffectation = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTX01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_84 = new JLabel();
    WCOD = new XRiTextField();
    OBJ_67 = new JLabel();
    L1NLI = new XRiTextField();
    OBJ_73 = new JLabel();
    WC1NFA = new XRiTextField();
    OBJ_69 = new JLabel();
    WC1MTT = new XRiTextField();
    OBJ_70 = new JLabel();
    L1NCGX = new XRiTextField();
    L1NCA = new XRiTextField();
    WRCHA = new XRiTextField();
    OBJ_71 = new JLabel();
    OBJ_79 = new JLabel();
    WA2MTT = new XRiTextField();
    WC1TOT = new XRiTextField();
    OBJ_72 = new JLabel();
    OBJ_80 = new JLabel();
    WEDIF = new XRiTextField();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_26 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@LOCTIT@ @JOLIB@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_37 ----
          OBJ_37.setText("Soci\u00e9t\u00e9");
          OBJ_37.setName("OBJ_37");

          //---- E1SOC ----
          E1SOC.setComponentPopupMenu(BTD);
          E1SOC.setName("E1SOC");

          //---- OBJ_43 ----
          OBJ_43.setText("Date");
          OBJ_43.setName("OBJ_43");

          //---- E1DTEX ----
          E1DTEX.setComponentPopupMenu(BTD);
          E1DTEX.setName("E1DTEX");

          //---- OBJ_41 ----
          OBJ_41.setText("Folio");
          OBJ_41.setName("OBJ_41");

          //---- E1CFO ----
          E1CFO.setComponentPopupMenu(BTD);
          E1CFO.setName("E1CFO");

          //---- OBJ_39 ----
          OBJ_39.setText("Journal");
          OBJ_39.setName("OBJ_39");

          //---- E1CJO ----
          E1CJO.setComponentPopupMenu(BTD);
          E1CJO.setName("E1CJO");

          //---- JOLIB ----
          JOLIB.setText("@JOLIB@");
          JOLIB.setName("JOLIB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JOLIB, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JOLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- labelAffectation ----
          labelAffectation.setText("Affectation");
          labelAffectation.setFont(labelAffectation.getFont().deriveFont(labelAffectation.getFont().getStyle() | Font.BOLD, labelAffectation.getFont().getSize() + 3f));
          labelAffectation.setName("labelAffectation");
          p_tete_droite.add(labelAffectation);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Affectations r\u00e9glements");
              riSousMenu_bt6.setToolTipText("Mise en/hors fonction des affectations de r\u00e9glements");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Saisie nouvelle \u00e9criture");
              riSousMenu_bt9.setToolTipText("Saisie d'une nouvelle \u00e9criture");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Factures rattach\u00e9es");
              riSousMenu_bt11.setToolTipText("Recherche des factures rattach\u00e9es sur n\u00b0 de centrale");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Factures / r\u00e9f\u00e9rence ");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setToolTipText("Bloc-notes sur \u00e9criture");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(740, 490));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTX01 ----
              WTX01.setComponentPopupMenu(BTD);
              WTX01.setName("WTX01");
              WTX01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTX01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTX01);
            }
            xTitledPanel1ContentContainer.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(10, 15, 665, 285);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            xTitledPanel1ContentContainer.add(BT_PGUP);
            BT_PGUP.setBounds(680, 15, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            xTitledPanel1ContentContainer.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(680, 175, 25, 125);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_84 ----
            OBJ_84.setText("C");
            OBJ_84.setName("OBJ_84");
            xTitledPanel2ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(10, 34, 12, 20);

            //---- WCOD ----
            WCOD.setComponentPopupMenu(BTD);
            WCOD.setName("WCOD");
            xTitledPanel2ContentContainer.add(WCOD);
            WCOD.setBounds(25, 30, 20, WCOD.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("L");
            OBJ_67.setName("OBJ_67");
            xTitledPanel2ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(50, 34, 12, 20);

            //---- L1NLI ----
            L1NLI.setComponentPopupMenu(BTD);
            L1NLI.setName("L1NLI");
            xTitledPanel2ContentContainer.add(L1NLI);
            L1NLI.setBounds(65, 30, 44, L1NLI.getPreferredSize().height);

            //---- OBJ_73 ----
            OBJ_73.setText("Facture");
            OBJ_73.setName("OBJ_73");
            xTitledPanel2ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(115, 4, 48, 20);

            //---- WC1NFA ----
            WC1NFA.setComponentPopupMenu(BTD);
            WC1NFA.setName("WC1NFA");
            xTitledPanel2ContentContainer.add(WC1NFA);
            WC1NFA.setBounds(115, 30, 68, WC1NFA.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("@LIBMTT@");
            OBJ_69.setName("OBJ_69");
            xTitledPanel2ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(185, 4, 97, 20);

            //---- WC1MTT ----
            WC1MTT.setComponentPopupMenu(BTD);
            WC1MTT.setName("WC1MTT");
            xTitledPanel2ContentContainer.add(WC1MTT);
            WC1MTT.setBounds(185, 30, 116, WC1MTT.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("Compte");
            OBJ_70.setName("OBJ_70");
            xTitledPanel2ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(300, 4, 115, 20);

            //---- L1NCGX ----
            L1NCGX.setComponentPopupMenu(BTD);
            L1NCGX.setName("L1NCGX");
            xTitledPanel2ContentContainer.add(L1NCGX);
            L1NCGX.setBounds(300, 30, 60, L1NCGX.getPreferredSize().height);

            //---- L1NCA ----
            L1NCA.setComponentPopupMenu(BTD);
            L1NCA.setName("L1NCA");
            xTitledPanel2ContentContainer.add(L1NCA);
            L1NCA.setBounds(360, 30, 60, L1NCA.getPreferredSize().height);

            //---- WRCHA ----
            WRCHA.setComponentPopupMenu(BTD);
            WRCHA.setName("WRCHA");
            xTitledPanel2ContentContainer.add(WRCHA);
            WRCHA.setBounds(420, 30, 116, WRCHA.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("M.r\u00e9gl\u00e9");
            OBJ_71.setName("OBJ_71");
            xTitledPanel2ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(545, 4, 54, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("Affect\u00e9");
            OBJ_79.setName("OBJ_79");
            xTitledPanel2ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(545, 35, 45, 20);

            //---- WA2MTT ----
            WA2MTT.setName("WA2MTT");
            xTitledPanel2ContentContainer.add(WA2MTT);
            WA2MTT.setBounds(595, 0, 116, WA2MTT.getPreferredSize().height);

            //---- WC1TOT ----
            WC1TOT.setName("WC1TOT");
            xTitledPanel2ContentContainer.add(WC1TOT);
            WC1TOT.setBounds(595, 30, 116, WC1TOT.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setText(" ou recherche");
            OBJ_72.setName("OBJ_72");
            xTitledPanel2ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(420, 4, 115, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("Ecart");
            OBJ_80.setForeground(new Color(204, 0, 0));
            OBJ_80.setName("OBJ_80");
            xTitledPanel2ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(545, 65, 45, 20);

            //---- WEDIF ----
            WEDIF.setForeground(new Color(204, 0, 0));
            WEDIF.setName("WEDIF");
            xTitledPanel2ContentContainer.add(WEDIF);
            WEDIF.setBounds(595, 60, 116, WEDIF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(6, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 336, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_21 ----
      OBJ_21.setText("Modifier");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Annuler");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
      BTD.addSeparator();

      //---- OBJ_27 ----
      OBJ_27.setText("Choix possibles");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_26 ----
      OBJ_26.setText("Aide en ligne");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_37;
  private XRiTextField E1SOC;
  private JLabel OBJ_43;
  private XRiCalendrier E1DTEX;
  private JLabel OBJ_41;
  private XRiTextField E1CFO;
  private JLabel OBJ_39;
  private XRiTextField E1CJO;
  private RiZoneSortie JOLIB;
  private JPanel p_tete_droite;
  private JLabel labelAffectation;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTX01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_84;
  private XRiTextField WCOD;
  private JLabel OBJ_67;
  private XRiTextField L1NLI;
  private JLabel OBJ_73;
  private XRiTextField WC1NFA;
  private JLabel OBJ_69;
  private XRiTextField WC1MTT;
  private JLabel OBJ_70;
  private XRiTextField L1NCGX;
  private XRiTextField L1NCA;
  private XRiTextField WRCHA;
  private JLabel OBJ_71;
  private JLabel OBJ_79;
  private XRiTextField WA2MTT;
  private XRiTextField WC1TOT;
  private JLabel OBJ_72;
  private JLabel OBJ_80;
  private XRiTextField WEDIF;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_26;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
