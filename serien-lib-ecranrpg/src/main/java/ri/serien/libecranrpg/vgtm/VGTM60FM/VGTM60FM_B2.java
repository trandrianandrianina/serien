
package ri.serien.libecranrpg.vgtm.VGTM60FM;
// Nom Fichier: i_VGTM60FM_FMTB2_FMTF1_68.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGTM60FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WPT1_Top = { "WPT1", "WPT2", "WPT3", "WPT4", "WPT5", "WPT6", "WPT7", "WPT8", "WPT9", "WPT10", "WPT11", "WPT12", "WPT13",
      "WPT14", "WPT15", "WPT16", };
  private String[] _WPT1_Title = { "Numéro", "Libellé", "Date", };
  private String[][] _WPT1_Data =
      { { "01", "LIB01", "DTG01", }, { "02", "LIB02", "DTG02", }, { "03", "LIB03", "DTG03", }, { "04", "LIB04", "DTG04", },
          { "05", "LIB05", "DTG05", }, { "06", "LIB06", "DTG06", }, { "07", "LIB07", "DTG07", }, { "08", "LIB08", "DTG08", },
          { "09", "LIB09", "DTG09", }, { "10", "LIB10", "DTG10", }, { "11", "LIB11", "DTG11", }, { "12", "LIB12", "DTG12", },
          { "13", "LIB13", "DTG13", }, { "14", "LIB14", "DTG14", }, { "15", "LIB15", "DTG15", }, { "16", "LIB16", "DTG16", }, };
  private int[] _WPT1_Width = { 40, 142, 60, };
  
  public VGTM60FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Ajout
    initDiverses();
    WPTTOU.setValeursSelection("**", "  ");
    WPT1.setAspectTable(_WPT1_Top, _WPT1_Title, _WPT1_Data, _WPT1_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WPT1_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // WPTTOU.setEnabled( lexique.isPresent("WPTTOU"));
    // WPTTOU.setSelected(lexique.HostFieldGetData("WPTTOU").equalsIgnoreCase("**"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("TABLEAUX DE TRESORERIE"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (WPTTOU.isSelected())
    // lexique.HostFieldPutData("WPTTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WPTTOU", 0, " ");
  }
  
  private void WPT1MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WPT1_Top, "1", "ENTER", e);
    if (WPT1.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WPT1 = new XRiTable();
    WPTTOU = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(570, 390));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Demande de r\u00e9g\u00e9n\u00e9ration de tableau"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WPT1 ----
            WPT1.setName("WPT1");
            WPT1.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WPT1MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WPT1);
          }
          panel1.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(15, 35, 350, 285);

          //---- WPTTOU ----
          WPTTOU.setText("G\u00e9n\u00e9ration globale");
          WPTTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPTTOU.setName("WPTTOU");
          panel1.add(WPTTOU);
          WPTTOU.setBounds(224, 325, 141, 22);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 380, 370);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WPT1;
  private XRiCheckBox WPTTOU;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
