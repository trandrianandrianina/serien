
package ri.serien.libecranrpg.vgtm.VGTM57FM;
// Nom Fichier: pop_VGTM57FM_FMTL5_FMTF1_29.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM57FM_L5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGTM57FM_L5(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT1@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Ventilation analytique"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WVM01 = new XRiTextField();
    WVS01 = new XRiTextField();
    WVA01 = new XRiTextField();
    WVM02 = new XRiTextField();
    WVS02 = new XRiTextField();
    WVA02 = new XRiTextField();
    WVM03 = new XRiTextField();
    WVS03 = new XRiTextField();
    WVA03 = new XRiTextField();
    WVM04 = new XRiTextField();
    WVS04 = new XRiTextField();
    WVA04 = new XRiTextField();
    WVM05 = new XRiTextField();
    WVS05 = new XRiTextField();
    WVA05 = new XRiTextField();
    WVM06 = new XRiTextField();
    WVS06 = new XRiTextField();
    WVA06 = new XRiTextField();
    WVM07 = new XRiTextField();
    WVS07 = new XRiTextField();
    WVA07 = new XRiTextField();
    WVM08 = new XRiTextField();
    WVS08 = new XRiTextField();
    WVA08 = new XRiTextField();
    WVM09 = new XRiTextField();
    WVS09 = new XRiTextField();
    WVA09 = new XRiTextField();
    WVM10 = new XRiTextField();
    WVS10 = new XRiTextField();
    WVA10 = new XRiTextField();
    WVM11 = new XRiTextField();
    WVS11 = new XRiTextField();
    WVA11 = new XRiTextField();
    WVM12 = new XRiTextField();
    WVS12 = new XRiTextField();
    WVA12 = new XRiTextField();
    WVM13 = new XRiTextField();
    WVS13 = new XRiTextField();
    WVA13 = new XRiTextField();
    WVM14 = new XRiTextField();
    WVS14 = new XRiTextField();
    WVA14 = new XRiTextField();
    WVM15 = new XRiTextField();
    WVS15 = new XRiTextField();
    WVA15 = new XRiTextField();
    WVM16 = new XRiTextField();
    WVS16 = new XRiTextField();
    WVA16 = new XRiTextField();
    WVM17 = new XRiTextField();
    WVS17 = new XRiTextField();
    WVA17 = new XRiTextField();
    WVM18 = new XRiTextField();
    WVS18 = new XRiTextField();
    WVA18 = new XRiTextField();
    WVM19 = new XRiTextField();
    WVS19 = new XRiTextField();
    WVA19 = new XRiTextField();
    WVM20 = new XRiTextField();
    WVS20 = new XRiTextField();
    WVA20 = new XRiTextField();
    WVM21 = new XRiTextField();
    WVS21 = new XRiTextField();
    WVA21 = new XRiTextField();
    WVM22 = new XRiTextField();
    WVS22 = new XRiTextField();
    WVA22 = new XRiTextField();
    WVM23 = new XRiTextField();
    WVS23 = new XRiTextField();
    WVA23 = new XRiTextField();
    WVM24 = new XRiTextField();
    WVS24 = new XRiTextField();
    WVA24 = new XRiTextField();
    WVM25 = new XRiTextField();
    WVS25 = new XRiTextField();
    WVA25 = new XRiTextField();
    WVM26 = new XRiTextField();
    WVS26 = new XRiTextField();
    WVA26 = new XRiTextField();
    WVM27 = new XRiTextField();
    WVS27 = new XRiTextField();
    WVA27 = new XRiTextField();
    WVM28 = new XRiTextField();
    WVS28 = new XRiTextField();
    WVA28 = new XRiTextField();
    WVM29 = new XRiTextField();
    WVS29 = new XRiTextField();
    WVA29 = new XRiTextField();
    WVM30 = new XRiTextField();
    WVS30 = new XRiTextField();
    WVA30 = new XRiTextField();
    WVM31 = new XRiTextField();
    WVS31 = new XRiTextField();
    WVA31 = new XRiTextField();
    WVM32 = new XRiTextField();
    WVS32 = new XRiTextField();
    WVA32 = new XRiTextField();
    WVM33 = new XRiTextField();
    WVS33 = new XRiTextField();
    WVA33 = new XRiTextField();
    WVM34 = new XRiTextField();
    WVS34 = new XRiTextField();
    WVA34 = new XRiTextField();
    WVM35 = new XRiTextField();
    WVS35 = new XRiTextField();
    WVA35 = new XRiTextField();
    WVM36 = new XRiTextField();
    WVS36 = new XRiTextField();
    WVA36 = new XRiTextField();
    WVM37 = new XRiTextField();
    WVS37 = new XRiTextField();
    WVA37 = new XRiTextField();
    WVM38 = new XRiTextField();
    WVS38 = new XRiTextField();
    WVA38 = new XRiTextField();
    WVM39 = new XRiTextField();
    WVS39 = new XRiTextField();
    WVA39 = new XRiTextField();
    WVM40 = new XRiTextField();
    WVS40 = new XRiTextField();
    WVA40 = new XRiTextField();
    WVM41 = new XRiTextField();
    WVS41 = new XRiTextField();
    WVA41 = new XRiTextField();
    WVM42 = new XRiTextField();
    WVS42 = new XRiTextField();
    WVA42 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    panel2 = new JPanel();
    WMTVTL = new XRiTextField();
    WECVTL = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_44 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Recalcul ventilations");
            riSousMenu_bt6.setToolTipText("Recalcul des ventilations analytiques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WVM01 ----
          WVM01.setName("WVM01");
          panel1.add(WVM01);
          WVM01.setBounds(15, 35, 130, WVM01.getPreferredSize().height);

          //---- WVS01 ----
          WVS01.setName("WVS01");
          panel1.add(WVS01);
          WVS01.setBounds(145, 35, 54, WVS01.getPreferredSize().height);

          //---- WVA01 ----
          WVA01.setName("WVA01");
          panel1.add(WVA01);
          WVA01.setBounds(200, 35, 64, WVA01.getPreferredSize().height);

          //---- WVM02 ----
          WVM02.setName("WVM02");
          panel1.add(WVM02);
          WVM02.setBounds(290, 35, 130, WVM02.getPreferredSize().height);

          //---- WVS02 ----
          WVS02.setName("WVS02");
          panel1.add(WVS02);
          WVS02.setBounds(420, 35, 54, WVS02.getPreferredSize().height);

          //---- WVA02 ----
          WVA02.setName("WVA02");
          panel1.add(WVA02);
          WVA02.setBounds(475, 35, 64, WVA02.getPreferredSize().height);

          //---- WVM03 ----
          WVM03.setName("WVM03");
          panel1.add(WVM03);
          WVM03.setBounds(560, 35, 130, 28);

          //---- WVS03 ----
          WVS03.setName("WVS03");
          panel1.add(WVS03);
          WVS03.setBounds(690, 35, 54, 28);

          //---- WVA03 ----
          WVA03.setName("WVA03");
          panel1.add(WVA03);
          WVA03.setBounds(745, 35, 64, 28);

          //---- WVM04 ----
          WVM04.setName("WVM04");
          panel1.add(WVM04);
          WVM04.setBounds(15, 60, 130, WVM04.getPreferredSize().height);

          //---- WVS04 ----
          WVS04.setName("WVS04");
          panel1.add(WVS04);
          WVS04.setBounds(145, 60, 54, WVS04.getPreferredSize().height);

          //---- WVA04 ----
          WVA04.setName("WVA04");
          panel1.add(WVA04);
          WVA04.setBounds(200, 60, 64, WVA04.getPreferredSize().height);

          //---- WVM05 ----
          WVM05.setName("WVM05");
          panel1.add(WVM05);
          WVM05.setBounds(290, 60, 130, WVM05.getPreferredSize().height);

          //---- WVS05 ----
          WVS05.setName("WVS05");
          panel1.add(WVS05);
          WVS05.setBounds(420, 60, 54, WVS05.getPreferredSize().height);

          //---- WVA05 ----
          WVA05.setName("WVA05");
          panel1.add(WVA05);
          WVA05.setBounds(475, 60, 64, WVA05.getPreferredSize().height);

          //---- WVM06 ----
          WVM06.setName("WVM06");
          panel1.add(WVM06);
          WVM06.setBounds(560, 60, 130, WVM06.getPreferredSize().height);

          //---- WVS06 ----
          WVS06.setName("WVS06");
          panel1.add(WVS06);
          WVS06.setBounds(690, 60, 54, WVS06.getPreferredSize().height);

          //---- WVA06 ----
          WVA06.setName("WVA06");
          panel1.add(WVA06);
          WVA06.setBounds(745, 60, 64, WVA06.getPreferredSize().height);

          //---- WVM07 ----
          WVM07.setName("WVM07");
          panel1.add(WVM07);
          WVM07.setBounds(15, 85, 130, 28);

          //---- WVS07 ----
          WVS07.setName("WVS07");
          panel1.add(WVS07);
          WVS07.setBounds(145, 85, 54, 28);

          //---- WVA07 ----
          WVA07.setName("WVA07");
          panel1.add(WVA07);
          WVA07.setBounds(200, 85, 64, 28);

          //---- WVM08 ----
          WVM08.setName("WVM08");
          panel1.add(WVM08);
          WVM08.setBounds(290, 85, 130, 28);

          //---- WVS08 ----
          WVS08.setName("WVS08");
          panel1.add(WVS08);
          WVS08.setBounds(420, 85, 54, 28);

          //---- WVA08 ----
          WVA08.setName("WVA08");
          panel1.add(WVA08);
          WVA08.setBounds(475, 85, 64, 28);

          //---- WVM09 ----
          WVM09.setName("WVM09");
          panel1.add(WVM09);
          WVM09.setBounds(560, 85, 130, 28);

          //---- WVS09 ----
          WVS09.setName("WVS09");
          panel1.add(WVS09);
          WVS09.setBounds(690, 85, 54, 28);

          //---- WVA09 ----
          WVA09.setName("WVA09");
          panel1.add(WVA09);
          WVA09.setBounds(745, 85, 64, 28);

          //---- WVM10 ----
          WVM10.setName("WVM10");
          panel1.add(WVM10);
          WVM10.setBounds(15, 110, 130, WVM10.getPreferredSize().height);

          //---- WVS10 ----
          WVS10.setName("WVS10");
          panel1.add(WVS10);
          WVS10.setBounds(145, 110, 54, WVS10.getPreferredSize().height);

          //---- WVA10 ----
          WVA10.setName("WVA10");
          panel1.add(WVA10);
          WVA10.setBounds(200, 110, 64, WVA10.getPreferredSize().height);

          //---- WVM11 ----
          WVM11.setName("WVM11");
          panel1.add(WVM11);
          WVM11.setBounds(290, 110, 130, WVM11.getPreferredSize().height);

          //---- WVS11 ----
          WVS11.setName("WVS11");
          panel1.add(WVS11);
          WVS11.setBounds(420, 110, 54, WVS11.getPreferredSize().height);

          //---- WVA11 ----
          WVA11.setName("WVA11");
          panel1.add(WVA11);
          WVA11.setBounds(475, 110, 64, WVA11.getPreferredSize().height);

          //---- WVM12 ----
          WVM12.setName("WVM12");
          panel1.add(WVM12);
          WVM12.setBounds(560, 110, 130, WVM12.getPreferredSize().height);

          //---- WVS12 ----
          WVS12.setName("WVS12");
          panel1.add(WVS12);
          WVS12.setBounds(690, 110, 54, WVS12.getPreferredSize().height);

          //---- WVA12 ----
          WVA12.setName("WVA12");
          panel1.add(WVA12);
          WVA12.setBounds(745, 110, 64, WVA12.getPreferredSize().height);

          //---- WVM13 ----
          WVM13.setName("WVM13");
          panel1.add(WVM13);
          WVM13.setBounds(15, 135, 130, WVM13.getPreferredSize().height);

          //---- WVS13 ----
          WVS13.setName("WVS13");
          panel1.add(WVS13);
          WVS13.setBounds(145, 135, 54, WVS13.getPreferredSize().height);

          //---- WVA13 ----
          WVA13.setName("WVA13");
          panel1.add(WVA13);
          WVA13.setBounds(200, 135, 64, WVA13.getPreferredSize().height);

          //---- WVM14 ----
          WVM14.setName("WVM14");
          panel1.add(WVM14);
          WVM14.setBounds(290, 135, 130, WVM14.getPreferredSize().height);

          //---- WVS14 ----
          WVS14.setName("WVS14");
          panel1.add(WVS14);
          WVS14.setBounds(420, 135, 54, WVS14.getPreferredSize().height);

          //---- WVA14 ----
          WVA14.setName("WVA14");
          panel1.add(WVA14);
          WVA14.setBounds(475, 135, 64, WVA14.getPreferredSize().height);

          //---- WVM15 ----
          WVM15.setName("WVM15");
          panel1.add(WVM15);
          WVM15.setBounds(560, 135, 130, WVM15.getPreferredSize().height);

          //---- WVS15 ----
          WVS15.setName("WVS15");
          panel1.add(WVS15);
          WVS15.setBounds(690, 135, 54, WVS15.getPreferredSize().height);

          //---- WVA15 ----
          WVA15.setName("WVA15");
          panel1.add(WVA15);
          WVA15.setBounds(745, 135, 64, WVA15.getPreferredSize().height);

          //---- WVM16 ----
          WVM16.setName("WVM16");
          panel1.add(WVM16);
          WVM16.setBounds(15, 160, 130, WVM16.getPreferredSize().height);

          //---- WVS16 ----
          WVS16.setName("WVS16");
          panel1.add(WVS16);
          WVS16.setBounds(145, 160, 54, WVS16.getPreferredSize().height);

          //---- WVA16 ----
          WVA16.setName("WVA16");
          panel1.add(WVA16);
          WVA16.setBounds(200, 160, 64, WVA16.getPreferredSize().height);

          //---- WVM17 ----
          WVM17.setName("WVM17");
          panel1.add(WVM17);
          WVM17.setBounds(290, 160, 130, WVM17.getPreferredSize().height);

          //---- WVS17 ----
          WVS17.setName("WVS17");
          panel1.add(WVS17);
          WVS17.setBounds(420, 160, 54, WVS17.getPreferredSize().height);

          //---- WVA17 ----
          WVA17.setName("WVA17");
          panel1.add(WVA17);
          WVA17.setBounds(475, 160, 64, WVA17.getPreferredSize().height);

          //---- WVM18 ----
          WVM18.setName("WVM18");
          panel1.add(WVM18);
          WVM18.setBounds(560, 160, 130, WVM18.getPreferredSize().height);

          //---- WVS18 ----
          WVS18.setName("WVS18");
          panel1.add(WVS18);
          WVS18.setBounds(690, 160, 54, WVS18.getPreferredSize().height);

          //---- WVA18 ----
          WVA18.setName("WVA18");
          panel1.add(WVA18);
          WVA18.setBounds(745, 160, 64, WVA18.getPreferredSize().height);

          //---- WVM19 ----
          WVM19.setName("WVM19");
          panel1.add(WVM19);
          WVM19.setBounds(15, 185, 130, 28);

          //---- WVS19 ----
          WVS19.setName("WVS19");
          panel1.add(WVS19);
          WVS19.setBounds(145, 185, 54, 28);

          //---- WVA19 ----
          WVA19.setName("WVA19");
          panel1.add(WVA19);
          WVA19.setBounds(200, 185, 64, 28);

          //---- WVM20 ----
          WVM20.setName("WVM20");
          panel1.add(WVM20);
          WVM20.setBounds(290, 185, 130, 28);

          //---- WVS20 ----
          WVS20.setName("WVS20");
          panel1.add(WVS20);
          WVS20.setBounds(420, 185, 54, 28);

          //---- WVA20 ----
          WVA20.setName("WVA20");
          panel1.add(WVA20);
          WVA20.setBounds(475, 185, 64, 28);

          //---- WVM21 ----
          WVM21.setName("WVM21");
          panel1.add(WVM21);
          WVM21.setBounds(560, 185, 130, 28);

          //---- WVS21 ----
          WVS21.setName("WVS21");
          panel1.add(WVS21);
          WVS21.setBounds(690, 185, 54, 28);

          //---- WVA21 ----
          WVA21.setName("WVA21");
          panel1.add(WVA21);
          WVA21.setBounds(745, 185, 64, 28);

          //---- WVM22 ----
          WVM22.setName("WVM22");
          panel1.add(WVM22);
          WVM22.setBounds(15, 210, 130, WVM22.getPreferredSize().height);

          //---- WVS22 ----
          WVS22.setName("WVS22");
          panel1.add(WVS22);
          WVS22.setBounds(145, 210, 54, WVS22.getPreferredSize().height);

          //---- WVA22 ----
          WVA22.setName("WVA22");
          panel1.add(WVA22);
          WVA22.setBounds(200, 210, 64, WVA22.getPreferredSize().height);

          //---- WVM23 ----
          WVM23.setName("WVM23");
          panel1.add(WVM23);
          WVM23.setBounds(290, 210, 130, WVM23.getPreferredSize().height);

          //---- WVS23 ----
          WVS23.setName("WVS23");
          panel1.add(WVS23);
          WVS23.setBounds(420, 210, 54, WVS23.getPreferredSize().height);

          //---- WVA23 ----
          WVA23.setName("WVA23");
          panel1.add(WVA23);
          WVA23.setBounds(475, 210, 64, WVA23.getPreferredSize().height);

          //---- WVM24 ----
          WVM24.setName("WVM24");
          panel1.add(WVM24);
          WVM24.setBounds(560, 210, 130, WVM24.getPreferredSize().height);

          //---- WVS24 ----
          WVS24.setName("WVS24");
          panel1.add(WVS24);
          WVS24.setBounds(690, 210, 54, WVS24.getPreferredSize().height);

          //---- WVA24 ----
          WVA24.setName("WVA24");
          panel1.add(WVA24);
          WVA24.setBounds(745, 210, 64, WVA24.getPreferredSize().height);

          //---- WVM25 ----
          WVM25.setName("WVM25");
          panel1.add(WVM25);
          WVM25.setBounds(15, 235, 130, WVM25.getPreferredSize().height);

          //---- WVS25 ----
          WVS25.setName("WVS25");
          panel1.add(WVS25);
          WVS25.setBounds(145, 235, 54, WVS25.getPreferredSize().height);

          //---- WVA25 ----
          WVA25.setName("WVA25");
          panel1.add(WVA25);
          WVA25.setBounds(200, 235, 64, WVA25.getPreferredSize().height);

          //---- WVM26 ----
          WVM26.setName("WVM26");
          panel1.add(WVM26);
          WVM26.setBounds(290, 235, 130, WVM26.getPreferredSize().height);

          //---- WVS26 ----
          WVS26.setName("WVS26");
          panel1.add(WVS26);
          WVS26.setBounds(420, 235, 54, WVS26.getPreferredSize().height);

          //---- WVA26 ----
          WVA26.setName("WVA26");
          panel1.add(WVA26);
          WVA26.setBounds(475, 235, 64, WVA26.getPreferredSize().height);

          //---- WVM27 ----
          WVM27.setName("WVM27");
          panel1.add(WVM27);
          WVM27.setBounds(560, 235, 130, WVM27.getPreferredSize().height);

          //---- WVS27 ----
          WVS27.setName("WVS27");
          panel1.add(WVS27);
          WVS27.setBounds(690, 235, 54, WVS27.getPreferredSize().height);

          //---- WVA27 ----
          WVA27.setName("WVA27");
          panel1.add(WVA27);
          WVA27.setBounds(745, 235, 64, WVA27.getPreferredSize().height);

          //---- WVM28 ----
          WVM28.setName("WVM28");
          panel1.add(WVM28);
          WVM28.setBounds(15, 260, 130, WVM28.getPreferredSize().height);

          //---- WVS28 ----
          WVS28.setName("WVS28");
          panel1.add(WVS28);
          WVS28.setBounds(145, 260, 54, WVS28.getPreferredSize().height);

          //---- WVA28 ----
          WVA28.setName("WVA28");
          panel1.add(WVA28);
          WVA28.setBounds(200, 260, 64, WVA28.getPreferredSize().height);

          //---- WVM29 ----
          WVM29.setName("WVM29");
          panel1.add(WVM29);
          WVM29.setBounds(290, 260, 130, WVM29.getPreferredSize().height);

          //---- WVS29 ----
          WVS29.setName("WVS29");
          panel1.add(WVS29);
          WVS29.setBounds(420, 260, 54, WVS29.getPreferredSize().height);

          //---- WVA29 ----
          WVA29.setName("WVA29");
          panel1.add(WVA29);
          WVA29.setBounds(475, 260, 64, WVA29.getPreferredSize().height);

          //---- WVM30 ----
          WVM30.setName("WVM30");
          panel1.add(WVM30);
          WVM30.setBounds(560, 260, 130, WVM30.getPreferredSize().height);

          //---- WVS30 ----
          WVS30.setName("WVS30");
          panel1.add(WVS30);
          WVS30.setBounds(690, 260, 54, WVS30.getPreferredSize().height);

          //---- WVA30 ----
          WVA30.setName("WVA30");
          panel1.add(WVA30);
          WVA30.setBounds(745, 260, 64, WVA30.getPreferredSize().height);

          //---- WVM31 ----
          WVM31.setName("WVM31");
          panel1.add(WVM31);
          WVM31.setBounds(15, 285, 130, WVM31.getPreferredSize().height);

          //---- WVS31 ----
          WVS31.setName("WVS31");
          panel1.add(WVS31);
          WVS31.setBounds(145, 285, 54, WVS31.getPreferredSize().height);

          //---- WVA31 ----
          WVA31.setName("WVA31");
          panel1.add(WVA31);
          WVA31.setBounds(200, 285, 64, WVA31.getPreferredSize().height);

          //---- WVM32 ----
          WVM32.setName("WVM32");
          panel1.add(WVM32);
          WVM32.setBounds(290, 285, 130, WVM32.getPreferredSize().height);

          //---- WVS32 ----
          WVS32.setName("WVS32");
          panel1.add(WVS32);
          WVS32.setBounds(420, 285, 54, WVS32.getPreferredSize().height);

          //---- WVA32 ----
          WVA32.setName("WVA32");
          panel1.add(WVA32);
          WVA32.setBounds(475, 285, 64, WVA32.getPreferredSize().height);

          //---- WVM33 ----
          WVM33.setName("WVM33");
          panel1.add(WVM33);
          WVM33.setBounds(560, 285, 130, WVM33.getPreferredSize().height);

          //---- WVS33 ----
          WVS33.setName("WVS33");
          panel1.add(WVS33);
          WVS33.setBounds(690, 285, 54, WVS33.getPreferredSize().height);

          //---- WVA33 ----
          WVA33.setName("WVA33");
          panel1.add(WVA33);
          WVA33.setBounds(745, 285, 64, WVA33.getPreferredSize().height);

          //---- WVM34 ----
          WVM34.setName("WVM34");
          panel1.add(WVM34);
          WVM34.setBounds(15, 310, 130, WVM34.getPreferredSize().height);

          //---- WVS34 ----
          WVS34.setName("WVS34");
          panel1.add(WVS34);
          WVS34.setBounds(145, 310, 54, WVS34.getPreferredSize().height);

          //---- WVA34 ----
          WVA34.setName("WVA34");
          panel1.add(WVA34);
          WVA34.setBounds(200, 310, 64, WVA34.getPreferredSize().height);

          //---- WVM35 ----
          WVM35.setName("WVM35");
          panel1.add(WVM35);
          WVM35.setBounds(290, 310, 130, WVM35.getPreferredSize().height);

          //---- WVS35 ----
          WVS35.setName("WVS35");
          panel1.add(WVS35);
          WVS35.setBounds(420, 310, 54, WVS35.getPreferredSize().height);

          //---- WVA35 ----
          WVA35.setName("WVA35");
          panel1.add(WVA35);
          WVA35.setBounds(475, 310, 64, WVA35.getPreferredSize().height);

          //---- WVM36 ----
          WVM36.setName("WVM36");
          panel1.add(WVM36);
          WVM36.setBounds(560, 310, 130, WVM36.getPreferredSize().height);

          //---- WVS36 ----
          WVS36.setName("WVS36");
          panel1.add(WVS36);
          WVS36.setBounds(690, 310, 54, WVS36.getPreferredSize().height);

          //---- WVA36 ----
          WVA36.setName("WVA36");
          panel1.add(WVA36);
          WVA36.setBounds(745, 310, 64, WVA36.getPreferredSize().height);

          //---- WVM37 ----
          WVM37.setName("WVM37");
          panel1.add(WVM37);
          WVM37.setBounds(15, 335, 130, WVM37.getPreferredSize().height);

          //---- WVS37 ----
          WVS37.setName("WVS37");
          panel1.add(WVS37);
          WVS37.setBounds(145, 335, 54, WVS37.getPreferredSize().height);

          //---- WVA37 ----
          WVA37.setName("WVA37");
          panel1.add(WVA37);
          WVA37.setBounds(200, 335, 64, WVA37.getPreferredSize().height);

          //---- WVM38 ----
          WVM38.setName("WVM38");
          panel1.add(WVM38);
          WVM38.setBounds(290, 335, 130, WVM38.getPreferredSize().height);

          //---- WVS38 ----
          WVS38.setName("WVS38");
          panel1.add(WVS38);
          WVS38.setBounds(420, 335, 54, WVS38.getPreferredSize().height);

          //---- WVA38 ----
          WVA38.setName("WVA38");
          panel1.add(WVA38);
          WVA38.setBounds(475, 335, 64, WVA38.getPreferredSize().height);

          //---- WVM39 ----
          WVM39.setName("WVM39");
          panel1.add(WVM39);
          WVM39.setBounds(560, 335, 130, WVM39.getPreferredSize().height);

          //---- WVS39 ----
          WVS39.setName("WVS39");
          panel1.add(WVS39);
          WVS39.setBounds(690, 335, 54, WVS39.getPreferredSize().height);

          //---- WVA39 ----
          WVA39.setName("WVA39");
          panel1.add(WVA39);
          WVA39.setBounds(745, 335, 64, WVA39.getPreferredSize().height);

          //---- WVM40 ----
          WVM40.setName("WVM40");
          panel1.add(WVM40);
          WVM40.setBounds(15, 360, 130, WVM40.getPreferredSize().height);

          //---- WVS40 ----
          WVS40.setName("WVS40");
          panel1.add(WVS40);
          WVS40.setBounds(145, 360, 54, WVS40.getPreferredSize().height);

          //---- WVA40 ----
          WVA40.setName("WVA40");
          panel1.add(WVA40);
          WVA40.setBounds(200, 360, 64, WVA40.getPreferredSize().height);

          //---- WVM41 ----
          WVM41.setName("WVM41");
          panel1.add(WVM41);
          WVM41.setBounds(290, 360, 130, WVM41.getPreferredSize().height);

          //---- WVS41 ----
          WVS41.setName("WVS41");
          panel1.add(WVS41);
          WVS41.setBounds(420, 360, 54, WVS41.getPreferredSize().height);

          //---- WVA41 ----
          WVA41.setName("WVA41");
          panel1.add(WVA41);
          WVA41.setBounds(475, 360, 64, WVA41.getPreferredSize().height);

          //---- WVM42 ----
          WVM42.setName("WVM42");
          panel1.add(WVM42);
          WVM42.setBounds(560, 360, 130, WVM42.getPreferredSize().height);

          //---- WVS42 ----
          WVS42.setName("WVS42");
          panel1.add(WVS42);
          WVS42.setBounds(690, 360, 54, WVS42.getPreferredSize().height);

          //---- WVA42 ----
          WVA42.setName("WVA42");
          panel1.add(WVA42);
          WVA42.setBounds(745, 360, 64, WVA42.getPreferredSize().height);

          //---- label1 ----
          label1.setText("@LIBMTT@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(15, 10, 130, 25);

          //---- label2 ----
          label2.setText("section");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(145, 10, 54, 25);

          //---- label3 ----
          label3.setText("affaire");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(200, 10, 64, 25);

          //---- label4 ----
          label4.setText("@LIBMT1@");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(290, 10, 130, 25);

          //---- label5 ----
          label5.setText("section");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(420, 10, 54, 25);

          //---- label6 ----
          label6.setText("affaire");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(475, 10, 64, 25);

          //---- label7 ----
          label7.setText("@LIBMT2@");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(560, 10, 130, 25);

          //---- label8 ----
          label8.setText("section");
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(690, 10, 54, 25);

          //---- label9 ----
          label9.setText("affaire");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(745, 10, 64, 25);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WMTVTL ----
            WMTVTL.setName("WMTVTL");
            panel2.add(WMTVTL);
            WMTVTL.setBounds(150, 10, 130, WMTVTL.getPreferredSize().height);

            //---- WECVTL ----
            WECVTL.setName("WECVTL");
            panel2.add(WECVTL);
            WECVTL.setBounds(150, 40, 130, WECVTL.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Montant ventilation");
            OBJ_42.setName("OBJ_42");
            panel2.add(OBJ_42);
            OBJ_42.setBounds(20, 10, 130, 28);

            //---- OBJ_44 ----
            OBJ_44.setText("Ecart");
            OBJ_44.setName("OBJ_44");
            panel2.add(OBJ_44);
            OBJ_44.setBounds(20, 40, 130, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(520, 400, 290, 80);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 815, 485);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WVM01;
  private XRiTextField WVS01;
  private XRiTextField WVA01;
  private XRiTextField WVM02;
  private XRiTextField WVS02;
  private XRiTextField WVA02;
  private XRiTextField WVM03;
  private XRiTextField WVS03;
  private XRiTextField WVA03;
  private XRiTextField WVM04;
  private XRiTextField WVS04;
  private XRiTextField WVA04;
  private XRiTextField WVM05;
  private XRiTextField WVS05;
  private XRiTextField WVA05;
  private XRiTextField WVM06;
  private XRiTextField WVS06;
  private XRiTextField WVA06;
  private XRiTextField WVM07;
  private XRiTextField WVS07;
  private XRiTextField WVA07;
  private XRiTextField WVM08;
  private XRiTextField WVS08;
  private XRiTextField WVA08;
  private XRiTextField WVM09;
  private XRiTextField WVS09;
  private XRiTextField WVA09;
  private XRiTextField WVM10;
  private XRiTextField WVS10;
  private XRiTextField WVA10;
  private XRiTextField WVM11;
  private XRiTextField WVS11;
  private XRiTextField WVA11;
  private XRiTextField WVM12;
  private XRiTextField WVS12;
  private XRiTextField WVA12;
  private XRiTextField WVM13;
  private XRiTextField WVS13;
  private XRiTextField WVA13;
  private XRiTextField WVM14;
  private XRiTextField WVS14;
  private XRiTextField WVA14;
  private XRiTextField WVM15;
  private XRiTextField WVS15;
  private XRiTextField WVA15;
  private XRiTextField WVM16;
  private XRiTextField WVS16;
  private XRiTextField WVA16;
  private XRiTextField WVM17;
  private XRiTextField WVS17;
  private XRiTextField WVA17;
  private XRiTextField WVM18;
  private XRiTextField WVS18;
  private XRiTextField WVA18;
  private XRiTextField WVM19;
  private XRiTextField WVS19;
  private XRiTextField WVA19;
  private XRiTextField WVM20;
  private XRiTextField WVS20;
  private XRiTextField WVA20;
  private XRiTextField WVM21;
  private XRiTextField WVS21;
  private XRiTextField WVA21;
  private XRiTextField WVM22;
  private XRiTextField WVS22;
  private XRiTextField WVA22;
  private XRiTextField WVM23;
  private XRiTextField WVS23;
  private XRiTextField WVA23;
  private XRiTextField WVM24;
  private XRiTextField WVS24;
  private XRiTextField WVA24;
  private XRiTextField WVM25;
  private XRiTextField WVS25;
  private XRiTextField WVA25;
  private XRiTextField WVM26;
  private XRiTextField WVS26;
  private XRiTextField WVA26;
  private XRiTextField WVM27;
  private XRiTextField WVS27;
  private XRiTextField WVA27;
  private XRiTextField WVM28;
  private XRiTextField WVS28;
  private XRiTextField WVA28;
  private XRiTextField WVM29;
  private XRiTextField WVS29;
  private XRiTextField WVA29;
  private XRiTextField WVM30;
  private XRiTextField WVS30;
  private XRiTextField WVA30;
  private XRiTextField WVM31;
  private XRiTextField WVS31;
  private XRiTextField WVA31;
  private XRiTextField WVM32;
  private XRiTextField WVS32;
  private XRiTextField WVA32;
  private XRiTextField WVM33;
  private XRiTextField WVS33;
  private XRiTextField WVA33;
  private XRiTextField WVM34;
  private XRiTextField WVS34;
  private XRiTextField WVA34;
  private XRiTextField WVM35;
  private XRiTextField WVS35;
  private XRiTextField WVA35;
  private XRiTextField WVM36;
  private XRiTextField WVS36;
  private XRiTextField WVA36;
  private XRiTextField WVM37;
  private XRiTextField WVS37;
  private XRiTextField WVA37;
  private XRiTextField WVM38;
  private XRiTextField WVS38;
  private XRiTextField WVA38;
  private XRiTextField WVM39;
  private XRiTextField WVS39;
  private XRiTextField WVA39;
  private XRiTextField WVM40;
  private XRiTextField WVS40;
  private XRiTextField WVA40;
  private XRiTextField WVM41;
  private XRiTextField WVS41;
  private XRiTextField WVA41;
  private XRiTextField WVM42;
  private XRiTextField WVS42;
  private XRiTextField WVA42;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JPanel panel2;
  private XRiTextField WMTVTL;
  private XRiTextField WECVTL;
  private JLabel OBJ_42;
  private JLabel OBJ_44;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
