
package ri.serien.libecranrpg.vgtm.VGTM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM71FM_E extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, };
  private int[] _LD01_Width = { 22, };
  public ODialog dialog_JO = null;
  
  /**
   * Constructeur.
   */
  public VGTM71FM_E(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation
        .setText(lexique.TranslationTable(interpreteurD.analyseExpression("Saisie des écritures de trésorerie : @LOCTIT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOAGB@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIBR@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT1@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    WMTCP.setVisible(lexique.isTrue("85"));
    WMTCR.setVisible(lexique.isTrue("84"));
    OBJ_66.setVisible(lexique.isTrue("85"));
    OBJ_63.setVisible(lexique.isTrue("84"));
    OBJ_44.setVisible(lexique.isPresent("JOLIBR"));
    OBJ_46.setVisible(lexique.isPresent("JOAGB"));
    OBJ_45.setVisible(lexique.isPresent("JOLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@LOCTIT@"));
    p_bpresentation.setCodeEtablissement(WISOC.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "DSH");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void BTN_JOActionPerformed(ActionEvent e) {
    lexique.addVariableGlobale("CODE_SOC", WISOC.getText());
    lexique.addVariableGlobale("ZONE_JO", "WICJO");
    if (dialog_JO == null) {
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_53 = new JLabel();
    WISOC = new XRiTextField();
    WICJO = new XRiTextField();
    OBJ_55 = new JLabel();
    WICFO = new XRiTextField();
    OBJ_56 = new JLabel();
    WIDTEX = new XRiTextField();
    BTN_JO2 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JPanel();
    SCROLLPANE_LIST6 = new JScrollPane();
    LD01 = new XRiTable();
    OBJ_45 = new RiZoneSortie();
    OBJ_46 = new RiZoneSortie();
    OBJ_44 = new RiZoneSortie();
    OBJ_63 = new JLabel();
    OBJ_66 = new JLabel();
    WMTCR = new XRiTextField();
    WMTCP = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Saisie des \u00e9critures de tr\u00e9sorerie : @LOCTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche2.setName("p_tete_gauche2");
          
          // ---- OBJ_53 ----
          OBJ_53.setText("Soci\u00e9t\u00e9");
          OBJ_53.setName("OBJ_53");
          
          // ---- WISOC ----
          WISOC.setComponentPopupMenu(null);
          WISOC.setName("WISOC");
          
          // ---- WICJO ----
          WICJO.setComponentPopupMenu(null);
          WICJO.setName("WICJO");
          
          // ---- OBJ_55 ----
          OBJ_55.setText("Folio");
          OBJ_55.setName("OBJ_55");
          
          // ---- WICFO ----
          WICFO.setComponentPopupMenu(null);
          WICFO.setName("WICFO");
          
          // ---- OBJ_56 ----
          OBJ_56.setText("Date");
          OBJ_56.setName("OBJ_56");
          
          // ---- WIDTEX ----
          WIDTEX.setComponentPopupMenu(null);
          WIDTEX.setName("WIDTEX");
          
          // ---- BTN_JO2 ----
          BTN_JO2.setText("Journal");
          BTN_JO2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BTN_JO2.setToolTipText("Recherche d'un journal");
          BTN_JO2.setName("BTN_JO2");
          
          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(23, 23, 23)
                  .addComponent(BTN_JO2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(40, 40, 40)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(40, 40, 40)
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE).addGap(204, 204, 204)));
          p_tete_gauche2Layout.setVerticalGroup(p_tete_gauche2Layout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(BTN_JO2))
              .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup().addGap(4, 4, 4)
                  .addGroup(p_tete_gauche2Layout.createParallelGroup()
                      .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche2);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Sortir le folio de la tr\u00e9so.");
              riSousMenu_bt11.setToolTipText("Sortir le folio de la comptabilit\u00e9");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(660, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== P_Centre ========
          {
            P_Centre.setOpaque(false);
            P_Centre.setName("P_Centre");
            
            // ======== SCROLLPANE_LIST6 ========
            {
              SCROLLPANE_LIST6.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST6.setName("SCROLLPANE_LIST6");
              
              // ---- LD01 ----
              LD01.setComponentPopupMenu(BTD);
              LD01.setName("LD01");
              SCROLLPANE_LIST6.setViewportView(LD01);
            }
            
            // ---- OBJ_45 ----
            OBJ_45.setText("@JOLIB@");
            OBJ_45.setName("OBJ_45");
            
            // ---- OBJ_46 ----
            OBJ_46.setText("@JOAGB@");
            OBJ_46.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_46.setName("OBJ_46");
            
            // ---- OBJ_44 ----
            OBJ_44.setText("@JOLIBR@");
            OBJ_44.setName("OBJ_44");
            
            // ---- OBJ_63 ----
            OBJ_63.setText("@TIT1@");
            OBJ_63.setFont(OBJ_63.getFont().deriveFont(OBJ_63.getFont().getStyle() | Font.BOLD));
            OBJ_63.setName("OBJ_63");
            
            // ---- OBJ_66 ----
            OBJ_66.setText("@TIT2@");
            OBJ_66.setFont(OBJ_66.getFont().deriveFont(OBJ_66.getFont().getStyle() | Font.BOLD));
            OBJ_66.setName("OBJ_66");
            
            // ---- WMTCR ----
            WMTCR.setComponentPopupMenu(null);
            WMTCR.setFont(WMTCR.getFont().deriveFont(WMTCR.getFont().getStyle() | Font.BOLD));
            WMTCR.setName("WMTCR");
            
            // ---- WMTCP ----
            WMTCP.setComponentPopupMenu(null);
            WMTCP.setFont(WMTCP.getFont().deriveFont(WMTCP.getFont().getStyle() | Font.BOLD));
            WMTCP.setName("WMTCP");
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            
            GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
            P_Centre.setLayout(P_CentreLayout);
            P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
                .addGroup(P_CentreLayout.createSequentialGroup().addGap(20, 20, 20)
                    .addGroup(P_CentreLayout.createParallelGroup()
                        .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE))
                    .addGap(101, 101, 101).addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_CentreLayout.createSequentialGroup().addGap(20, 20, 20)
                    .addComponent(SCROLLPANE_LIST6, GroupLayout.PREFERRED_SIZE, 565, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                    .addGroup(P_CentreLayout.createParallelGroup()
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
                .addGroup(P_CentreLayout.createSequentialGroup().addGap(285, 285, 285)
                    .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                    .addComponent(WMTCR, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_CentreLayout.createSequentialGroup().addGap(285, 285, 285)
                    .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                    .addComponent(WMTCP, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)));
            P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
                .addGroup(P_CentreLayout.createSequentialGroup().addGap(10, 10, 10)
                    .addGroup(P_CentreLayout.createParallelGroup()
                        .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addGroup(P_CentreLayout.createParallelGroup()
                        .addComponent(SCROLLPANE_LIST6, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
                        .addGroup(P_CentreLayout
                            .createSequentialGroup().addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                            .addGap(30, 30, 30).addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)))
                    .addGap(10, 10, 10)
                    .addGroup(P_CentreLayout.createParallelGroup()
                        .addGroup(P_CentreLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE,
                            20, GroupLayout.PREFERRED_SIZE))
                        .addComponent(WMTCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(P_CentreLayout.createParallelGroup()
                        .addGroup(P_CentreLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE,
                            20, GroupLayout.PREFERRED_SIZE))
                        .addComponent(WMTCP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(30, 30, 30)));
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(31, Short.MAX_VALUE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(17, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_53;
  private XRiTextField WISOC;
  private XRiTextField WICJO;
  private JLabel OBJ_55;
  private XRiTextField WICFO;
  private JLabel OBJ_56;
  private XRiTextField WIDTEX;
  private JLabel BTN_JO2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Centre;
  private JScrollPane SCROLLPANE_LIST6;
  private XRiTable LD01;
  private RiZoneSortie OBJ_45;
  private RiZoneSortie OBJ_46;
  private RiZoneSortie OBJ_44;
  private JLabel OBJ_63;
  private JLabel OBJ_66;
  private XRiTextField WMTCR;
  private XRiTextField WMTCP;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
