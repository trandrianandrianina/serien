
package ri.serien.libecranrpg.vgtm.VGTM55FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM55FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] EBSNF_Value = { "", "C", "D", };
  private String[] EBSND_Value = { "", "C", "D", };
  public ODialog dialog_JO = null;
  
  public VGTM55FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EBSNF.setValeurs(EBSNF_Value, null);
    EBSND.setValeurs(EBSND_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Saisie de relevé de compte @LIBPG@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@WJOLIB@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    // EBSNF.setSelectedIndex(getIndice("EBSNF", EBSNF_Value));
    // EBSND.setSelectedIndex(getIndice("EBSND", EBSND_Value));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - SAISIE RELEVE DE COMPTE @LIBPG@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("EBSNF", 0, EBSNF_Value[EBSNF.getSelectedIndex()]);
    // lexique.HostFieldPutData("EBSND", 0, EBSND_Value[EBSND.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BTN_JOActionPerformed(ActionEvent e) {
    lexique.addVariableGlobale("CODE_SOC", WISOC.getText());
    lexique.addVariableGlobale("ZONE_JO", "WICJO");
    if (dialog_JO == null) {
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WISOC = new XRiTextField();
    WICJO = new XRiTextField();
    WIDTEX = new XRiCalendrier();
    OBJ_54 = new JLabel();
    OBJ_72 = new JLabel();
    BTN_JO = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    WCO1 = new XRiTextField();
    WLO1 = new XRiTextField();
    WCO2 = new XRiTextField();
    WLO2 = new XRiTextField();
    WCO3 = new XRiTextField();
    WLO3 = new XRiTextField();
    WCO4 = new XRiTextField();
    WLO4 = new XRiTextField();
    WCO5 = new XRiTextField();
    WLO5 = new XRiTextField();
    WCO6 = new XRiTextField();
    WLO6 = new XRiTextField();
    WCO7 = new XRiTextField();
    WLO7 = new XRiTextField();
    WCO8 = new XRiTextField();
    WLO8 = new XRiTextField();
    EBREB = new XRiTextField();
    EBSOD = new XRiTextField();
    EBSND = new XRiComboBox();
    EBSOF = new XRiTextField();
    EBSNF = new XRiComboBox();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_26 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Saisie de relev\u00e9 de compte @LIBPG@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(BTD);
          WISOC.setName("WISOC");

          //---- WICJO ----
          WICJO.setComponentPopupMenu(BTD);
          WICJO.setName("WICJO");

          //---- WIDTEX ----
          WIDTEX.setComponentPopupMenu(null);
          WIDTEX.setName("WIDTEX");

          //---- OBJ_54 ----
          OBJ_54.setText("Soci\u00e9t\u00e9");
          OBJ_54.setName("OBJ_54");

          //---- OBJ_72 ----
          OBJ_72.setText("Date");
          OBJ_72.setName("OBJ_72");

          //---- BTN_JO ----
          BTN_JO.setText("Journal");
          BTN_JO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BTN_JO.setToolTipText("Recherche d'un journal");
          BTN_JO.setName("BTN_JO");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BTN_JO, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(BTN_JO))
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(500, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("@WJOLIB@"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("Libell\u00e9 de l'op\u00e9ration");
            xTitledSeparator1.setName("xTitledSeparator1");
            panel1.add(xTitledSeparator1);
            xTitledSeparator1.setBounds(135, 55, 240, xTitledSeparator1.getPreferredSize().height);

            //---- xTitledSeparator2 ----
            xTitledSeparator2.setTitle("Code");
            xTitledSeparator2.setName("xTitledSeparator2");
            panel1.add(xTitledSeparator2);
            xTitledSeparator2.setBounds(90, 55, 40, xTitledSeparator2.getPreferredSize().height);

            //---- label1 ----
            label1.setText("1");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(60, 85, 15, 20);

            //---- label2 ----
            label2.setText("2");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(60, 112, 15, 20);

            //---- label3 ----
            label3.setText("3");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(60, 139, 15, 20);

            //---- label4 ----
            label4.setText("4");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(60, 166, 15, 20);

            //---- label5 ----
            label5.setText("5");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(60, 193, 15, 20);

            //---- label6 ----
            label6.setText("6");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(60, 220, 15, 20);

            //---- label7 ----
            label7.setText("7");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setName("label7");
            panel1.add(label7);
            label7.setBounds(60, 247, 15, 20);

            //---- label8 ----
            label8.setText("8");
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setName("label8");
            panel1.add(label8);
            label8.setBounds(60, 274, 15, 20);

            //---- WCO1 ----
            WCO1.setName("WCO1");
            panel1.add(WCO1);
            WCO1.setBounds(90, 81, 20, WCO1.getPreferredSize().height);

            //---- WLO1 ----
            WLO1.setName("WLO1");
            panel1.add(WLO1);
            WLO1.setBounds(135, 81, 240, WLO1.getPreferredSize().height);

            //---- WCO2 ----
            WCO2.setName("WCO2");
            panel1.add(WCO2);
            WCO2.setBounds(90, 108, 20, WCO2.getPreferredSize().height);

            //---- WLO2 ----
            WLO2.setName("WLO2");
            panel1.add(WLO2);
            WLO2.setBounds(135, 108, 240, WLO2.getPreferredSize().height);

            //---- WCO3 ----
            WCO3.setName("WCO3");
            panel1.add(WCO3);
            WCO3.setBounds(90, 135, 20, WCO3.getPreferredSize().height);

            //---- WLO3 ----
            WLO3.setName("WLO3");
            panel1.add(WLO3);
            WLO3.setBounds(135, 135, 240, WLO3.getPreferredSize().height);

            //---- WCO4 ----
            WCO4.setName("WCO4");
            panel1.add(WCO4);
            WCO4.setBounds(90, 162, 20, WCO4.getPreferredSize().height);

            //---- WLO4 ----
            WLO4.setName("WLO4");
            panel1.add(WLO4);
            WLO4.setBounds(135, 162, 240, WLO4.getPreferredSize().height);

            //---- WCO5 ----
            WCO5.setName("WCO5");
            panel1.add(WCO5);
            WCO5.setBounds(90, 189, 20, WCO5.getPreferredSize().height);

            //---- WLO5 ----
            WLO5.setName("WLO5");
            panel1.add(WLO5);
            WLO5.setBounds(135, 189, 240, WLO5.getPreferredSize().height);

            //---- WCO6 ----
            WCO6.setName("WCO6");
            panel1.add(WCO6);
            WCO6.setBounds(90, 216, 20, WCO6.getPreferredSize().height);

            //---- WLO6 ----
            WLO6.setName("WLO6");
            panel1.add(WLO6);
            WLO6.setBounds(135, 216, 240, WLO6.getPreferredSize().height);

            //---- WCO7 ----
            WCO7.setName("WCO7");
            panel1.add(WCO7);
            WCO7.setBounds(90, 243, 20, WCO7.getPreferredSize().height);

            //---- WLO7 ----
            WLO7.setName("WLO7");
            panel1.add(WLO7);
            WLO7.setBounds(135, 243, 240, WLO7.getPreferredSize().height);

            //---- WCO8 ----
            WCO8.setName("WCO8");
            panel1.add(WCO8);
            WCO8.setBounds(90, 270, 20, WCO8.getPreferredSize().height);

            //---- WLO8 ----
            WLO8.setName("WLO8");
            panel1.add(WLO8);
            WLO8.setBounds(135, 270, 240, WLO8.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- EBREB ----
          EBREB.setComponentPopupMenu(BTD);
          EBREB.setName("EBREB");

          //---- EBSOD ----
          EBSOD.setComponentPopupMenu(BTD);
          EBSOD.setName("EBSOD");

          //---- EBSND ----
          EBSND.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Cr\u00e9dit",
            "D\u00e9bit"
          }));
          EBSND.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBSND.setName("EBSND");

          //---- EBSOF ----
          EBSOF.setComponentPopupMenu(BTD);
          EBSOF.setName("EBSOF");

          //---- EBSNF ----
          EBSNF.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Cr\u00e9dit",
            "D\u00e9bit"
          }));
          EBSNF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBSNF.setName("EBSNF");

          //---- OBJ_27 ----
          OBJ_27.setText("Solde de d\u00e9but du relev\u00e9 de compte");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_28 ----
          OBJ_28.setText("Solde de fin du relev\u00e9 de compte");
          OBJ_28.setName("OBJ_28");

          //---- OBJ_26 ----
          OBJ_26.setText("R\u00e9f\u00e9rence du relev\u00e9 de compte");
          OBJ_26.setName("OBJ_26");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(EBREB, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(EBSOD, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(EBSND, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(EBSOF, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(EBSNF, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)))
                    .addGap(61, 61, 61))))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(EBREB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(EBSOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EBSND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(EBSOF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EBSNF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(44, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField WISOC;
  private XRiTextField WICJO;
  private XRiCalendrier WIDTEX;
  private JLabel OBJ_54;
  private JLabel OBJ_72;
  private JLabel BTN_JO;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private XRiTextField WCO1;
  private XRiTextField WLO1;
  private XRiTextField WCO2;
  private XRiTextField WLO2;
  private XRiTextField WCO3;
  private XRiTextField WLO3;
  private XRiTextField WCO4;
  private XRiTextField WLO4;
  private XRiTextField WCO5;
  private XRiTextField WLO5;
  private XRiTextField WCO6;
  private XRiTextField WLO6;
  private XRiTextField WCO7;
  private XRiTextField WLO7;
  private XRiTextField WCO8;
  private XRiTextField WLO8;
  private XRiTextField EBREB;
  private XRiTextField EBSOD;
  private XRiComboBox EBSND;
  private XRiTextField EBSOF;
  private XRiComboBox EBSNF;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JLabel OBJ_26;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
