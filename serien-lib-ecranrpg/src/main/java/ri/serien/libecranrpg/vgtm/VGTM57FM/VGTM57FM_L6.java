
package ri.serien.libecranrpg.vgtm.VGTM57FM;
// Nom Fichier: pop_VGTM57FM_FMTL6_20.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM57FM_L6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGTM57FM_L6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererAffichageMenus(null);
    gererLesErreurs("19");
    

    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie écart de réglement"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    CGE01 = new XRiTextField();
    MTE01 = new XRiTextField();
    LIE01 = new XRiTextField();
    WSAN01 = new XRiTextField();
    WAFF01 = new XRiTextField();
    WNAT01 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    CGE02 = new XRiTextField();
    MTE02 = new XRiTextField();
    LIE02 = new XRiTextField();
    WSAN02 = new XRiTextField();
    WAFF02 = new XRiTextField();
    WNAT02 = new XRiTextField();
    CGE03 = new XRiTextField();
    MTE03 = new XRiTextField();
    LIE03 = new XRiTextField();
    WSAN03 = new XRiTextField();
    WAFF03 = new XRiTextField();
    WNAT03 = new XRiTextField();
    CGE04 = new XRiTextField();
    MTE04 = new XRiTextField();
    LIE04 = new XRiTextField();
    WSAN04 = new XRiTextField();
    WAFF04 = new XRiTextField();
    WNAT04 = new XRiTextField();
    CGE05 = new XRiTextField();
    MTE05 = new XRiTextField();
    LIE05 = new XRiTextField();
    WSAN05 = new XRiTextField();
    WAFF05 = new XRiTextField();
    WNAT05 = new XRiTextField();
    C6CGT = new XRiTextField();
    C6MTT = new XRiTextField();
    C6LIT = new XRiTextField();
    label10 = new JLabel();
    WDIER = new XRiTextField();
    separator1 = compFactory.createSeparator("Charge");
    separator2 = compFactory.createSeparator("TVA");
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 340));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setName("panel1");
          panel1.setLayout(null);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(new Rectangle(new Point(15, 10), panel1.getPreferredSize()));

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- CGE01 ----
          CGE01.setName("CGE01");
          panel2.add(CGE01);
          CGE01.setBounds(25, 75, 60, CGE01.getPreferredSize().height);

          //---- MTE01 ----
          MTE01.setName("MTE01");
          panel2.add(MTE01);
          MTE01.setBounds(90, 75, 70, MTE01.getPreferredSize().height);

          //---- LIE01 ----
          LIE01.setName("LIE01");
          panel2.add(LIE01);
          LIE01.setBounds(165, 75, 234, LIE01.getPreferredSize().height);

          //---- WSAN01 ----
          WSAN01.setName("WSAN01");
          panel2.add(WSAN01);
          WSAN01.setBounds(400, 75, 44, WSAN01.getPreferredSize().height);

          //---- WAFF01 ----
          WAFF01.setName("WAFF01");
          panel2.add(WAFF01);
          WAFF01.setBounds(445, 75, 64, WAFF01.getPreferredSize().height);

          //---- WNAT01 ----
          WNAT01.setName("WNAT01");
          panel2.add(WNAT01);
          WNAT01.setBounds(510, 75, 60, WNAT01.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Compte");
          label1.setName("label1");
          panel2.add(label1);
          label1.setBounds(25, 55, 60, 20);

          //---- label2 ----
          label2.setText("Montant");
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(90, 55, 60, 20);

          //---- label3 ----
          label3.setText("Libell\u00e9 de l'\u00e9criture");
          label3.setName("label3");
          panel2.add(label3);
          label3.setBounds(165, 55, 234, 20);

          //---- label4 ----
          label4.setText("Section");
          label4.setName("label4");
          panel2.add(label4);
          label4.setBounds(400, 55, 60, 20);

          //---- label5 ----
          label5.setText("Affaire");
          label5.setName("label5");
          panel2.add(label5);
          label5.setBounds(445, 55, 60, 20);

          //---- label6 ----
          label6.setText("Nature");
          label6.setName("label6");
          panel2.add(label6);
          label6.setBounds(510, 55, 60, 20);

          //---- CGE02 ----
          CGE02.setName("CGE02");
          panel2.add(CGE02);
          CGE02.setBounds(25, 100, 60, CGE02.getPreferredSize().height);

          //---- MTE02 ----
          MTE02.setName("MTE02");
          panel2.add(MTE02);
          MTE02.setBounds(90, 100, 70, MTE02.getPreferredSize().height);

          //---- LIE02 ----
          LIE02.setName("LIE02");
          panel2.add(LIE02);
          LIE02.setBounds(165, 100, 234, LIE02.getPreferredSize().height);

          //---- WSAN02 ----
          WSAN02.setName("WSAN02");
          panel2.add(WSAN02);
          WSAN02.setBounds(400, 100, 44, WSAN02.getPreferredSize().height);

          //---- WAFF02 ----
          WAFF02.setName("WAFF02");
          panel2.add(WAFF02);
          WAFF02.setBounds(445, 100, 64, WAFF02.getPreferredSize().height);

          //---- WNAT02 ----
          WNAT02.setName("WNAT02");
          panel2.add(WNAT02);
          WNAT02.setBounds(510, 100, 60, WNAT02.getPreferredSize().height);

          //---- CGE03 ----
          CGE03.setName("CGE03");
          panel2.add(CGE03);
          CGE03.setBounds(25, 125, 60, CGE03.getPreferredSize().height);

          //---- MTE03 ----
          MTE03.setName("MTE03");
          panel2.add(MTE03);
          MTE03.setBounds(90, 125, 70, MTE03.getPreferredSize().height);

          //---- LIE03 ----
          LIE03.setName("LIE03");
          panel2.add(LIE03);
          LIE03.setBounds(165, 125, 234, LIE03.getPreferredSize().height);

          //---- WSAN03 ----
          WSAN03.setName("WSAN03");
          panel2.add(WSAN03);
          WSAN03.setBounds(400, 125, 44, WSAN03.getPreferredSize().height);

          //---- WAFF03 ----
          WAFF03.setName("WAFF03");
          panel2.add(WAFF03);
          WAFF03.setBounds(445, 125, 64, WAFF03.getPreferredSize().height);

          //---- WNAT03 ----
          WNAT03.setName("WNAT03");
          panel2.add(WNAT03);
          WNAT03.setBounds(510, 125, 60, WNAT03.getPreferredSize().height);

          //---- CGE04 ----
          CGE04.setName("CGE04");
          panel2.add(CGE04);
          CGE04.setBounds(25, 150, 60, CGE04.getPreferredSize().height);

          //---- MTE04 ----
          MTE04.setName("MTE04");
          panel2.add(MTE04);
          MTE04.setBounds(90, 150, 70, MTE04.getPreferredSize().height);

          //---- LIE04 ----
          LIE04.setName("LIE04");
          panel2.add(LIE04);
          LIE04.setBounds(165, 150, 234, LIE04.getPreferredSize().height);

          //---- WSAN04 ----
          WSAN04.setName("WSAN04");
          panel2.add(WSAN04);
          WSAN04.setBounds(400, 150, 44, WSAN04.getPreferredSize().height);

          //---- WAFF04 ----
          WAFF04.setName("WAFF04");
          panel2.add(WAFF04);
          WAFF04.setBounds(445, 150, 64, WAFF04.getPreferredSize().height);

          //---- WNAT04 ----
          WNAT04.setName("WNAT04");
          panel2.add(WNAT04);
          WNAT04.setBounds(510, 150, 60, WNAT04.getPreferredSize().height);

          //---- CGE05 ----
          CGE05.setName("CGE05");
          panel2.add(CGE05);
          CGE05.setBounds(25, 175, 60, CGE05.getPreferredSize().height);

          //---- MTE05 ----
          MTE05.setName("MTE05");
          panel2.add(MTE05);
          MTE05.setBounds(90, 175, 70, MTE05.getPreferredSize().height);

          //---- LIE05 ----
          LIE05.setName("LIE05");
          panel2.add(LIE05);
          LIE05.setBounds(165, 175, 234, LIE05.getPreferredSize().height);

          //---- WSAN05 ----
          WSAN05.setName("WSAN05");
          panel2.add(WSAN05);
          WSAN05.setBounds(400, 175, 44, WSAN05.getPreferredSize().height);

          //---- WAFF05 ----
          WAFF05.setName("WAFF05");
          panel2.add(WAFF05);
          WAFF05.setBounds(445, 175, 64, WAFF05.getPreferredSize().height);

          //---- WNAT05 ----
          WNAT05.setName("WNAT05");
          panel2.add(WNAT05);
          WNAT05.setBounds(510, 175, 60, WNAT05.getPreferredSize().height);

          //---- C6CGT ----
          C6CGT.setName("C6CGT");
          panel2.add(C6CGT);
          C6CGT.setBounds(25, 230, 60, C6CGT.getPreferredSize().height);

          //---- C6MTT ----
          C6MTT.setName("C6MTT");
          panel2.add(C6MTT);
          C6MTT.setBounds(90, 230, 70, C6MTT.getPreferredSize().height);

          //---- C6LIT ----
          C6LIT.setName("C6LIT");
          panel2.add(C6LIT);
          C6LIT.setBounds(165, 230, 234, C6LIT.getPreferredSize().height);

          //---- label10 ----
          label10.setText("Diff\u00e9rence");
          label10.setName("label10");
          panel2.add(label10);
          label10.setBounds(25, 265, 60, 20);

          //---- WDIER ----
          WDIER.setName("WDIER");
          panel2.add(WDIER);
          WDIER.setBounds(90, 260, 70, WDIER.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel2.add(separator1);
          separator1.setBounds(15, 25, 565, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          panel2.add(separator2);
          separator2.setBounds(15, 210, 565, separator2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 605, 320);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel2;
  private XRiTextField CGE01;
  private XRiTextField MTE01;
  private XRiTextField LIE01;
  private XRiTextField WSAN01;
  private XRiTextField WAFF01;
  private XRiTextField WNAT01;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField CGE02;
  private XRiTextField MTE02;
  private XRiTextField LIE02;
  private XRiTextField WSAN02;
  private XRiTextField WAFF02;
  private XRiTextField WNAT02;
  private XRiTextField CGE03;
  private XRiTextField MTE03;
  private XRiTextField LIE03;
  private XRiTextField WSAN03;
  private XRiTextField WAFF03;
  private XRiTextField WNAT03;
  private XRiTextField CGE04;
  private XRiTextField MTE04;
  private XRiTextField LIE04;
  private XRiTextField WSAN04;
  private XRiTextField WAFF04;
  private XRiTextField WNAT04;
  private XRiTextField CGE05;
  private XRiTextField MTE05;
  private XRiTextField LIE05;
  private XRiTextField WSAN05;
  private XRiTextField WAFF05;
  private XRiTextField WNAT05;
  private XRiTextField C6CGT;
  private XRiTextField C6MTT;
  private XRiTextField C6LIT;
  private JLabel label10;
  private XRiTextField WDIER;
  private JComponent separator1;
  private JComponent separator2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
