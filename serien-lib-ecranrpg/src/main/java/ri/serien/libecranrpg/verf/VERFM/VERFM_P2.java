
package ri.serien.libecranrpg.verf.VERFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VERFM_P2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VERFM_P2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  // -- Méthodes publiques
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OS400.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OS400@")).trim());
    SRLNBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SRLNBR@")).trim());
    MODEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MODEL@")).trim());
    VER220.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VER220@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MES220@")).trim());
    WCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLI@")).trim());
    WLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIV@")).trim());
    WNBUTI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBUTI@")).trim());
    MOD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD01@")).trim());
    MLI01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI01@")).trim());
    MOD06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD06@")).trim());
    MLI06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI06@")).trim());
    MOD11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD11@")).trim());
    MLI11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI11@")).trim());
    MOD16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD16@")).trim());
    MLI16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI16@")).trim());
    MOD21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD21@")).trim());
    MLI21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI21@")).trim());
    MOD26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD26@")).trim());
    MLI26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI26@")).trim());
    MOD31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD31@")).trim());
    MLI31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI31@")).trim());
    MOD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD02@")).trim());
    MLI02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI02@")).trim());
    MOD07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD07@")).trim());
    MLI07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI07@")).trim());
    MOD12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD12@")).trim());
    MLI12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI12@")).trim());
    MOD17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD17@")).trim());
    MLI17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI17@")).trim());
    MOD22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD22@")).trim());
    MLI22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI22@")).trim());
    MOD27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD27@")).trim());
    MLI27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI27@")).trim());
    MOD32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD32@")).trim());
    MLI32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI32@")).trim());
    MOD03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD03@")).trim());
    MLI03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI03@")).trim());
    MOD08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD08@")).trim());
    MLI08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI08@")).trim());
    MOD13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD13@")).trim());
    MLI13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI13@")).trim());
    MOD18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD18@")).trim());
    MLI18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI18@")).trim());
    MOD23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD23@")).trim());
    MLI23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI23@")).trim());
    MOD28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD28@")).trim());
    MLI28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI28@")).trim());
    MOD33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD33@")).trim());
    MLI33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI33@")).trim());
    MOD04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD04@")).trim());
    MLI04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI04@")).trim());
    MOD09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD09@")).trim());
    MLI09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI09@")).trim());
    MOD14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD14@")).trim());
    MLI14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI14@")).trim());
    MOD19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD19@")).trim());
    MLI19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI19@")).trim());
    MOD24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD24@")).trim());
    MLI24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI24@")).trim());
    MOD29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD29@")).trim());
    MLI29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI29@")).trim());
    MOD34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD34@")).trim());
    MLI34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI34@")).trim());
    MOD05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD05@")).trim());
    MLI05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI05@")).trim());
    MOD10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD10@")).trim());
    MLI10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI10@")).trim());
    MOD15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD15@")).trim());
    MLI15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI15@")).trim());
    MOD20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD20@")).trim());
    MLI20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI20@")).trim());
    MOD25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD25@")).trim());
    MLI25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI25@")).trim());
    MOD30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD30@")).trim());
    MLI30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI30@")).trim());
    MOD35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD35@")).trim());
    MLI35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MLI35@")).trim());
    lbLibelle1Java.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBJV1@")).trim());
    lbLibelle2Java.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBJV2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Mise à jour de la date
    bpPresentation.setSousTitre("Informations à la date du " + DateHeure.getFormateDateHeure(DateHeure.JJ_MM_AAAA_A_HH_MM_SS));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlServeurOS400 = new SNPanelTitre();
    lbVersionOs400 = new SNLabelChamp();
    OS400 = new RiZoneSortie();
    lbNumeroSerie = new SNLabelChamp();
    SRLNBR = new RiZoneSortie();
    lbModeleServeur = new SNLabelChamp();
    MODEL = new RiZoneSortie();
    pnlNiveauProgrammes = new SNPanelTitre();
    lbVersionProgramme = new SNLabelChamp();
    VER220 = new RiZoneSortie();
    label1 = new SNMessage();
    pnlClient = new SNPanelTitre();
    lbNumeroClient = new SNLabelChamp();
    WCLI = new RiZoneSortie();
    WLIV = new RiZoneSortie();
    lbNombreUtilisateurs = new SNLabelChamp();
    WNBUTI = new RiZoneSortie();
    pnlModules = new SNPanelTitre();
    pnlColonne1 = new SNPanel();
    MOD01 = new RiZoneSortie();
    MLI01 = new RiZoneSortie();
    MOD06 = new RiZoneSortie();
    MLI06 = new RiZoneSortie();
    MOD11 = new RiZoneSortie();
    MLI11 = new RiZoneSortie();
    MOD16 = new RiZoneSortie();
    MLI16 = new RiZoneSortie();
    MOD21 = new RiZoneSortie();
    MLI21 = new RiZoneSortie();
    MOD26 = new RiZoneSortie();
    MLI26 = new RiZoneSortie();
    MOD31 = new RiZoneSortie();
    MLI31 = new RiZoneSortie();
    pnlColonne2 = new SNPanel();
    MOD02 = new RiZoneSortie();
    MLI02 = new RiZoneSortie();
    MOD07 = new RiZoneSortie();
    MLI07 = new RiZoneSortie();
    MOD12 = new RiZoneSortie();
    MLI12 = new RiZoneSortie();
    MOD17 = new RiZoneSortie();
    MLI17 = new RiZoneSortie();
    MOD22 = new RiZoneSortie();
    MLI22 = new RiZoneSortie();
    MOD27 = new RiZoneSortie();
    MLI27 = new RiZoneSortie();
    MOD32 = new RiZoneSortie();
    MLI32 = new RiZoneSortie();
    pnlColonne4 = new SNPanel();
    MOD03 = new RiZoneSortie();
    MLI03 = new RiZoneSortie();
    MOD08 = new RiZoneSortie();
    MLI08 = new RiZoneSortie();
    MOD13 = new RiZoneSortie();
    MLI13 = new RiZoneSortie();
    MOD18 = new RiZoneSortie();
    MLI18 = new RiZoneSortie();
    MOD23 = new RiZoneSortie();
    MLI23 = new RiZoneSortie();
    MOD28 = new RiZoneSortie();
    MLI28 = new RiZoneSortie();
    MOD33 = new RiZoneSortie();
    MLI33 = new RiZoneSortie();
    pnlColonne5 = new SNPanel();
    MOD04 = new RiZoneSortie();
    MLI04 = new RiZoneSortie();
    MOD09 = new RiZoneSortie();
    MLI09 = new RiZoneSortie();
    MOD14 = new RiZoneSortie();
    MLI14 = new RiZoneSortie();
    MOD19 = new RiZoneSortie();
    MLI19 = new RiZoneSortie();
    MOD24 = new RiZoneSortie();
    MLI24 = new RiZoneSortie();
    MOD29 = new RiZoneSortie();
    MLI29 = new RiZoneSortie();
    MOD34 = new RiZoneSortie();
    MLI34 = new RiZoneSortie();
    pnlColonne3 = new SNPanel();
    MOD05 = new RiZoneSortie();
    MLI05 = new RiZoneSortie();
    MOD10 = new RiZoneSortie();
    MLI10 = new RiZoneSortie();
    MOD15 = new RiZoneSortie();
    MLI15 = new RiZoneSortie();
    MOD20 = new RiZoneSortie();
    MLI20 = new RiZoneSortie();
    MOD25 = new RiZoneSortie();
    MLI25 = new RiZoneSortie();
    MOD30 = new RiZoneSortie();
    MLI30 = new RiZoneSortie();
    MOD35 = new RiZoneSortie();
    MLI35 = new RiZoneSortie();
    pnlJava = new SNPanelTitre();
    lbLibelle1Java = new SNMessage();
    lbLibelle2Java = new SNMessage();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Informations sur votre syst\u00e8me et vos niveaux de versions");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(950, 590));
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlServeurOS400 ========
      {
        pnlServeurOS400.setOpaque(false);
        pnlServeurOS400.setTitre("Serveur et OS400");
        pnlServeurOS400.setName("pnlServeurOS400");
        pnlServeurOS400.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlServeurOS400.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlServeurOS400.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlServeurOS400.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlServeurOS400.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbVersionOs400 ----
        lbVersionOs400.setText("Version de l'OS/400");
        lbVersionOs400.setPreferredSize(new Dimension(200, 30));
        lbVersionOs400.setMinimumSize(new Dimension(200, 30));
        lbVersionOs400.setMaximumSize(new Dimension(200, 30));
        lbVersionOs400.setName("lbVersionOs400");
        pnlServeurOS400.add(lbVersionOs400, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- OS400 ----
        OS400.setText("@OS400@");
        OS400.setFont(OS400.getFont().deriveFont(OS400.getFont().getSize() + 3f));
        OS400.setMaximumSize(new Dimension(50, 30));
        OS400.setMinimumSize(new Dimension(50, 30));
        OS400.setPreferredSize(new Dimension(50, 30));
        OS400.setHorizontalAlignment(SwingConstants.CENTER);
        OS400.setName("OS400");
        pnlServeurOS400.add(OS400, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbNumeroSerie ----
        lbNumeroSerie.setText("Num\u00e9ro de s\u00e9rie du serveur");
        lbNumeroSerie.setPreferredSize(new Dimension(250, 30));
        lbNumeroSerie.setMinimumSize(new Dimension(250, 30));
        lbNumeroSerie.setMaximumSize(new Dimension(250, 30));
        lbNumeroSerie.setName("lbNumeroSerie");
        pnlServeurOS400.add(lbNumeroSerie, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- SRLNBR ----
        SRLNBR.setText("@SRLNBR@");
        SRLNBR.setFont(SRLNBR.getFont().deriveFont(SRLNBR.getFont().getSize() + 3f));
        SRLNBR.setMaximumSize(new Dimension(100, 30));
        SRLNBR.setMinimumSize(new Dimension(100, 30));
        SRLNBR.setPreferredSize(new Dimension(100, 30));
        SRLNBR.setName("SRLNBR");
        pnlServeurOS400.add(SRLNBR, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbModeleServeur ----
        lbModeleServeur.setText("Mod\u00e8le du serveur");
        lbModeleServeur.setName("lbModeleServeur");
        pnlServeurOS400.add(lbModeleServeur, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- MODEL ----
        MODEL.setText("@MODEL@");
        MODEL.setFont(MODEL.getFont().deriveFont(MODEL.getFont().getSize() + 3f));
        MODEL.setPreferredSize(new Dimension(75, 30));
        MODEL.setMinimumSize(new Dimension(75, 30));
        MODEL.setMaximumSize(new Dimension(75, 30));
        MODEL.setHorizontalAlignment(SwingConstants.CENTER);
        MODEL.setName("MODEL");
        pnlServeurOS400.add(MODEL, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlServeurOS400,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlNiveauProgrammes ========
      {
        pnlNiveauProgrammes.setOpaque(false);
        pnlNiveauProgrammes.setTitre("Informations sur le niveau des programmes et fichiers");
        pnlNiveauProgrammes.setName("pnlNiveauProgrammes");
        pnlNiveauProgrammes.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlNiveauProgrammes.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlNiveauProgrammes.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlNiveauProgrammes.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlNiveauProgrammes.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbVersionProgramme ----
        lbVersionProgramme.setText("Version des programmes");
        lbVersionProgramme.setPreferredSize(new Dimension(200, 30));
        lbVersionProgramme.setMaximumSize(new Dimension(200, 30));
        lbVersionProgramme.setMinimumSize(new Dimension(200, 30));
        lbVersionProgramme.setName("lbVersionProgramme");
        pnlNiveauProgrammes.add(lbVersionProgramme, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- VER220 ----
        VER220.setText("@VER220@");
        VER220.setFont(VER220.getFont().deriveFont(VER220.getFont().getStyle() | Font.BOLD, VER220.getFont().getSize() + 3f));
        VER220.setName("VER220");
        pnlNiveauProgrammes.add(VER220, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- label1 ----
        label1.setText("@MES220@");
        label1.setForeground(Color.red);
        label1.setName("label1");
        pnlNiveauProgrammes.add(label1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlNiveauProgrammes,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlClient ========
      {
        pnlClient.setOpaque(false);
        pnlClient.setTitre("Vos informations client chez R\u00e9solution Informatique");
        pnlClient.setName("pnlClient");
        pnlClient.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlClient.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbNumeroClient ----
        lbNumeroClient.setText("Num\u00e9ro client");
        lbNumeroClient.setPreferredSize(new Dimension(200, 30));
        lbNumeroClient.setMinimumSize(new Dimension(200, 30));
        lbNumeroClient.setMaximumSize(new Dimension(200, 30));
        lbNumeroClient.setName("lbNumeroClient");
        pnlClient.add(lbNumeroClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WCLI ----
        WCLI.setText("@WCLI@");
        WCLI.setFont(WCLI.getFont().deriveFont(WCLI.getFont().getSize() + 3f));
        WCLI.setHorizontalAlignment(SwingConstants.RIGHT);
        WCLI.setMinimumSize(new Dimension(100, 30));
        WCLI.setMaximumSize(new Dimension(100, 30));
        WCLI.setPreferredSize(new Dimension(100, 30));
        WCLI.setName("WCLI");
        pnlClient.add(WCLI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WLIV ----
        WLIV.setText("@WLIV@");
        WLIV.setFont(WLIV.getFont().deriveFont(WLIV.getFont().getSize() + 3f));
        WLIV.setHorizontalAlignment(SwingConstants.RIGHT);
        WLIV.setPreferredSize(new Dimension(50, 30));
        WLIV.setMinimumSize(new Dimension(50, 30));
        WLIV.setMaximumSize(new Dimension(50, 30));
        WLIV.setName("WLIV");
        pnlClient.add(WLIV, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbNombreUtilisateurs ----
        lbNombreUtilisateurs.setText("Nombre d'utilisateurs");
        lbNombreUtilisateurs.setName("lbNombreUtilisateurs");
        pnlClient.add(lbNombreUtilisateurs, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WNBUTI ----
        WNBUTI.setText("@WNBUTI@");
        WNBUTI.setFont(WNBUTI.getFont().deriveFont(WNBUTI.getFont().getSize() + 3f));
        WNBUTI.setHorizontalAlignment(SwingConstants.RIGHT);
        WNBUTI.setPreferredSize(new Dimension(75, 30));
        WNBUTI.setMinimumSize(new Dimension(75, 30));
        WNBUTI.setMaximumSize(new Dimension(75, 30));
        WNBUTI.setName("WNBUTI");
        pnlClient.add(WNBUTI, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlClient,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlModules ========
      {
        pnlModules.setOpaque(false);
        pnlModules.setTitre("Modules actifs");
        pnlModules.setPreferredSize(new Dimension(1048, 245));
        pnlModules.setName("pnlModules");
        pnlModules.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlModules.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlModules.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlModules.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlModules.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlColonne1 ========
        {
          pnlColonne1.setPreferredSize(new Dimension(200, 200));
          pnlColonne1.setMinimumSize(new Dimension(200, 200));
          pnlColonne1.setMaximumSize(new Dimension(200, 200));
          pnlColonne1.setName("pnlColonne1");
          pnlColonne1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlColonne1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlColonne1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlColonne1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlColonne1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- MOD01 ----
          MOD01.setText("@MOD01@");
          MOD01.setPreferredSize(new Dimension(60, 24));
          MOD01.setMinimumSize(new Dimension(60, 24));
          MOD01.setMaximumSize(new Dimension(60, 24));
          MOD01.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD01.setName("MOD01");
          pnlColonne1.add(MOD01, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI01 ----
          MLI01.setText("@MLI01@");
          MLI01.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI01.setMinimumSize(new Dimension(100, 24));
          MLI01.setMaximumSize(new Dimension(100, 24));
          MLI01.setName("MLI01");
          pnlColonne1.add(MLI01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD06 ----
          MOD06.setText("@MOD06@");
          MOD06.setPreferredSize(new Dimension(60, 24));
          MOD06.setMinimumSize(new Dimension(60, 24));
          MOD06.setMaximumSize(new Dimension(60, 24));
          MOD06.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD06.setName("MOD06");
          pnlColonne1.add(MOD06, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI06 ----
          MLI06.setText("@MLI06@");
          MLI06.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI06.setMinimumSize(new Dimension(100, 24));
          MLI06.setMaximumSize(new Dimension(100, 24));
          MLI06.setName("MLI06");
          pnlColonne1.add(MLI06, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD11 ----
          MOD11.setText("@MOD11@");
          MOD11.setMaximumSize(new Dimension(60, 24));
          MOD11.setMinimumSize(new Dimension(60, 24));
          MOD11.setPreferredSize(new Dimension(60, 24));
          MOD11.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD11.setName("MOD11");
          pnlColonne1.add(MOD11, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI11 ----
          MLI11.setText("@MLI11@");
          MLI11.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI11.setMinimumSize(new Dimension(100, 24));
          MLI11.setMaximumSize(new Dimension(100, 24));
          MLI11.setName("MLI11");
          pnlColonne1.add(MLI11, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD16 ----
          MOD16.setText("@MOD16@");
          MOD16.setPreferredSize(new Dimension(60, 24));
          MOD16.setMinimumSize(new Dimension(60, 24));
          MOD16.setMaximumSize(new Dimension(60, 24));
          MOD16.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD16.setName("MOD16");
          pnlColonne1.add(MOD16, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI16 ----
          MLI16.setText("@MLI16@");
          MLI16.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI16.setMinimumSize(new Dimension(100, 24));
          MLI16.setMaximumSize(new Dimension(100, 24));
          MLI16.setName("MLI16");
          pnlColonne1.add(MLI16, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD21 ----
          MOD21.setText("@MOD21@");
          MOD21.setMaximumSize(new Dimension(60, 24));
          MOD21.setMinimumSize(new Dimension(60, 24));
          MOD21.setPreferredSize(new Dimension(60, 24));
          MOD21.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD21.setName("MOD21");
          pnlColonne1.add(MOD21, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI21 ----
          MLI21.setText("@MLI21@");
          MLI21.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI21.setMinimumSize(new Dimension(100, 24));
          MLI21.setMaximumSize(new Dimension(100, 24));
          MLI21.setName("MLI21");
          pnlColonne1.add(MLI21, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD26 ----
          MOD26.setText("@MOD26@");
          MOD26.setMaximumSize(new Dimension(60, 24));
          MOD26.setMinimumSize(new Dimension(60, 24));
          MOD26.setPreferredSize(new Dimension(60, 24));
          MOD26.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD26.setName("MOD26");
          pnlColonne1.add(MOD26, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI26 ----
          MLI26.setText("@MLI26@");
          MLI26.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI26.setMinimumSize(new Dimension(100, 24));
          MLI26.setMaximumSize(new Dimension(100, 24));
          MLI26.setName("MLI26");
          pnlColonne1.add(MLI26, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD31 ----
          MOD31.setText("@MOD31@");
          MOD31.setMaximumSize(new Dimension(60, 24));
          MOD31.setMinimumSize(new Dimension(60, 24));
          MOD31.setPreferredSize(new Dimension(60, 24));
          MOD31.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD31.setName("MOD31");
          pnlColonne1.add(MOD31, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- MLI31 ----
          MLI31.setText("@MLI31@");
          MLI31.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI31.setMinimumSize(new Dimension(100, 24));
          MLI31.setMaximumSize(new Dimension(100, 24));
          MLI31.setName("MLI31");
          pnlColonne1.add(MLI31, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlModules.add(pnlColonne1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlColonne2 ========
        {
          pnlColonne2.setPreferredSize(new Dimension(200, 200));
          pnlColonne2.setMinimumSize(new Dimension(200, 200));
          pnlColonne2.setMaximumSize(new Dimension(200, 200));
          pnlColonne2.setName("pnlColonne2");
          pnlColonne2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlColonne2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlColonne2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlColonne2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlColonne2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- MOD02 ----
          MOD02.setText("@MOD02@");
          MOD02.setPreferredSize(new Dimension(60, 24));
          MOD02.setMaximumSize(new Dimension(60, 24));
          MOD02.setMinimumSize(new Dimension(60, 24));
          MOD02.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD02.setName("MOD02");
          pnlColonne2.add(MOD02, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI02 ----
          MLI02.setText("@MLI02@");
          MLI02.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI02.setMinimumSize(new Dimension(100, 24));
          MLI02.setMaximumSize(new Dimension(100, 24));
          MLI02.setName("MLI02");
          pnlColonne2.add(MLI02, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD07 ----
          MOD07.setText("@MOD07@");
          MOD07.setPreferredSize(new Dimension(60, 24));
          MOD07.setMaximumSize(new Dimension(60, 24));
          MOD07.setMinimumSize(new Dimension(60, 24));
          MOD07.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD07.setName("MOD07");
          pnlColonne2.add(MOD07, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI07 ----
          MLI07.setText("@MLI07@");
          MLI07.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI07.setMinimumSize(new Dimension(100, 24));
          MLI07.setMaximumSize(new Dimension(100, 24));
          MLI07.setName("MLI07");
          pnlColonne2.add(MLI07, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD12 ----
          MOD12.setText("@MOD12@");
          MOD12.setPreferredSize(new Dimension(60, 24));
          MOD12.setMaximumSize(new Dimension(60, 24));
          MOD12.setMinimumSize(new Dimension(60, 24));
          MOD12.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD12.setName("MOD12");
          pnlColonne2.add(MOD12, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI12 ----
          MLI12.setText("@MLI12@");
          MLI12.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI12.setMinimumSize(new Dimension(100, 24));
          MLI12.setMaximumSize(new Dimension(100, 24));
          MLI12.setName("MLI12");
          pnlColonne2.add(MLI12, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD17 ----
          MOD17.setText("@MOD17@");
          MOD17.setPreferredSize(new Dimension(60, 24));
          MOD17.setMaximumSize(new Dimension(60, 24));
          MOD17.setMinimumSize(new Dimension(60, 24));
          MOD17.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD17.setName("MOD17");
          pnlColonne2.add(MOD17, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI17 ----
          MLI17.setText("@MLI17@");
          MLI17.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI17.setMinimumSize(new Dimension(100, 24));
          MLI17.setMaximumSize(new Dimension(100, 24));
          MLI17.setName("MLI17");
          pnlColonne2.add(MLI17, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD22 ----
          MOD22.setText("@MOD22@");
          MOD22.setPreferredSize(new Dimension(60, 24));
          MOD22.setMaximumSize(new Dimension(60, 24));
          MOD22.setMinimumSize(new Dimension(60, 24));
          MOD22.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD22.setName("MOD22");
          pnlColonne2.add(MOD22, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI22 ----
          MLI22.setText("@MLI22@");
          MLI22.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI22.setMinimumSize(new Dimension(100, 24));
          MLI22.setMaximumSize(new Dimension(100, 24));
          MLI22.setName("MLI22");
          pnlColonne2.add(MLI22, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD27 ----
          MOD27.setText("@MOD27@");
          MOD27.setPreferredSize(new Dimension(60, 24));
          MOD27.setMaximumSize(new Dimension(60, 24));
          MOD27.setMinimumSize(new Dimension(60, 24));
          MOD27.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD27.setName("MOD27");
          pnlColonne2.add(MOD27, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI27 ----
          MLI27.setText("@MLI27@");
          MLI27.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI27.setMinimumSize(new Dimension(100, 24));
          MLI27.setMaximumSize(new Dimension(100, 24));
          MLI27.setName("MLI27");
          pnlColonne2.add(MLI27, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD32 ----
          MOD32.setText("@MOD32@");
          MOD32.setPreferredSize(new Dimension(60, 24));
          MOD32.setMaximumSize(new Dimension(60, 24));
          MOD32.setMinimumSize(new Dimension(60, 24));
          MOD32.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD32.setName("MOD32");
          pnlColonne2.add(MOD32, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- MLI32 ----
          MLI32.setText("@MLI32@");
          MLI32.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI32.setMinimumSize(new Dimension(100, 24));
          MLI32.setMaximumSize(new Dimension(100, 24));
          MLI32.setName("MLI32");
          pnlColonne2.add(MLI32, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlModules.add(pnlColonne2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlColonne4 ========
        {
          pnlColonne4.setPreferredSize(new Dimension(200, 200));
          pnlColonne4.setMinimumSize(new Dimension(200, 200));
          pnlColonne4.setMaximumSize(new Dimension(200, 200));
          pnlColonne4.setName("pnlColonne4");
          pnlColonne4.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlColonne4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlColonne4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlColonne4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlColonne4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- MOD03 ----
          MOD03.setText("@MOD03@");
          MOD03.setPreferredSize(new Dimension(60, 24));
          MOD03.setMaximumSize(new Dimension(60, 24));
          MOD03.setMinimumSize(new Dimension(60, 24));
          MOD03.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD03.setName("MOD03");
          pnlColonne4.add(MOD03, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI03 ----
          MLI03.setText("@MLI03@");
          MLI03.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI03.setMinimumSize(new Dimension(100, 24));
          MLI03.setMaximumSize(new Dimension(100, 24));
          MLI03.setName("MLI03");
          pnlColonne4.add(MLI03, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD08 ----
          MOD08.setText("@MOD08@");
          MOD08.setPreferredSize(new Dimension(60, 24));
          MOD08.setMaximumSize(new Dimension(60, 24));
          MOD08.setMinimumSize(new Dimension(60, 24));
          MOD08.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD08.setName("MOD08");
          pnlColonne4.add(MOD08, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI08 ----
          MLI08.setText("@MLI08@");
          MLI08.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI08.setMinimumSize(new Dimension(100, 24));
          MLI08.setMaximumSize(new Dimension(100, 24));
          MLI08.setName("MLI08");
          pnlColonne4.add(MLI08, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD13 ----
          MOD13.setText("@MOD13@");
          MOD13.setPreferredSize(new Dimension(60, 24));
          MOD13.setMaximumSize(new Dimension(60, 24));
          MOD13.setMinimumSize(new Dimension(60, 24));
          MOD13.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD13.setName("MOD13");
          pnlColonne4.add(MOD13, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI13 ----
          MLI13.setText("@MLI13@");
          MLI13.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI13.setMinimumSize(new Dimension(100, 24));
          MLI13.setMaximumSize(new Dimension(100, 24));
          MLI13.setName("MLI13");
          pnlColonne4.add(MLI13, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD18 ----
          MOD18.setText("@MOD18@");
          MOD18.setPreferredSize(new Dimension(60, 24));
          MOD18.setMaximumSize(new Dimension(60, 24));
          MOD18.setMinimumSize(new Dimension(60, 24));
          MOD18.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD18.setName("MOD18");
          pnlColonne4.add(MOD18, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI18 ----
          MLI18.setText("@MLI18@");
          MLI18.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI18.setMinimumSize(new Dimension(100, 24));
          MLI18.setMaximumSize(new Dimension(100, 24));
          MLI18.setName("MLI18");
          pnlColonne4.add(MLI18, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD23 ----
          MOD23.setText("@MOD23@");
          MOD23.setPreferredSize(new Dimension(60, 24));
          MOD23.setMaximumSize(new Dimension(60, 24));
          MOD23.setMinimumSize(new Dimension(60, 24));
          MOD23.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD23.setName("MOD23");
          pnlColonne4.add(MOD23, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI23 ----
          MLI23.setText("@MLI23@");
          MLI23.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI23.setMinimumSize(new Dimension(100, 24));
          MLI23.setMaximumSize(new Dimension(100, 24));
          MLI23.setName("MLI23");
          pnlColonne4.add(MLI23, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD28 ----
          MOD28.setText("@MOD28@");
          MOD28.setPreferredSize(new Dimension(60, 24));
          MOD28.setMaximumSize(new Dimension(60, 24));
          MOD28.setMinimumSize(new Dimension(60, 24));
          MOD28.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD28.setName("MOD28");
          pnlColonne4.add(MOD28, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI28 ----
          MLI28.setText("@MLI28@");
          MLI28.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI28.setMinimumSize(new Dimension(100, 24));
          MLI28.setMaximumSize(new Dimension(100, 24));
          MLI28.setName("MLI28");
          pnlColonne4.add(MLI28, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD33 ----
          MOD33.setText("@MOD33@");
          MOD33.setPreferredSize(new Dimension(60, 24));
          MOD33.setMaximumSize(new Dimension(60, 24));
          MOD33.setMinimumSize(new Dimension(60, 24));
          MOD33.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD33.setName("MOD33");
          pnlColonne4.add(MOD33, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- MLI33 ----
          MLI33.setText("@MLI33@");
          MLI33.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI33.setMinimumSize(new Dimension(100, 24));
          MLI33.setMaximumSize(new Dimension(100, 24));
          MLI33.setName("MLI33");
          pnlColonne4.add(MLI33, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlModules.add(pnlColonne4, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlColonne5 ========
        {
          pnlColonne5.setPreferredSize(new Dimension(200, 200));
          pnlColonne5.setMinimumSize(new Dimension(200, 200));
          pnlColonne5.setMaximumSize(new Dimension(200, 200));
          pnlColonne5.setName("pnlColonne5");
          pnlColonne5.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlColonne5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlColonne5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlColonne5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlColonne5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- MOD04 ----
          MOD04.setText("@MOD04@");
          MOD04.setPreferredSize(new Dimension(60, 24));
          MOD04.setMaximumSize(new Dimension(60, 24));
          MOD04.setMinimumSize(new Dimension(60, 24));
          MOD04.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD04.setName("MOD04");
          pnlColonne5.add(MOD04, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI04 ----
          MLI04.setText("@MLI04@");
          MLI04.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI04.setMinimumSize(new Dimension(100, 24));
          MLI04.setMaximumSize(new Dimension(100, 24));
          MLI04.setName("MLI04");
          pnlColonne5.add(MLI04, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD09 ----
          MOD09.setText("@MOD09@");
          MOD09.setPreferredSize(new Dimension(60, 24));
          MOD09.setMaximumSize(new Dimension(60, 24));
          MOD09.setMinimumSize(new Dimension(60, 24));
          MOD09.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD09.setName("MOD09");
          pnlColonne5.add(MOD09, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI09 ----
          MLI09.setText("@MLI09@");
          MLI09.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI09.setMinimumSize(new Dimension(100, 24));
          MLI09.setMaximumSize(new Dimension(100, 24));
          MLI09.setName("MLI09");
          pnlColonne5.add(MLI09, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD14 ----
          MOD14.setText("@MOD14@");
          MOD14.setPreferredSize(new Dimension(60, 24));
          MOD14.setMaximumSize(new Dimension(60, 24));
          MOD14.setMinimumSize(new Dimension(60, 24));
          MOD14.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD14.setName("MOD14");
          pnlColonne5.add(MOD14, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI14 ----
          MLI14.setText("@MLI14@");
          MLI14.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI14.setMinimumSize(new Dimension(100, 24));
          MLI14.setMaximumSize(new Dimension(100, 24));
          MLI14.setName("MLI14");
          pnlColonne5.add(MLI14, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD19 ----
          MOD19.setText("@MOD19@");
          MOD19.setPreferredSize(new Dimension(60, 24));
          MOD19.setMaximumSize(new Dimension(60, 24));
          MOD19.setMinimumSize(new Dimension(60, 24));
          MOD19.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD19.setName("MOD19");
          pnlColonne5.add(MOD19, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI19 ----
          MLI19.setText("@MLI19@");
          MLI19.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI19.setMinimumSize(new Dimension(100, 24));
          MLI19.setMaximumSize(new Dimension(100, 24));
          MLI19.setName("MLI19");
          pnlColonne5.add(MLI19, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD24 ----
          MOD24.setText("@MOD24@");
          MOD24.setPreferredSize(new Dimension(60, 24));
          MOD24.setMaximumSize(new Dimension(60, 24));
          MOD24.setMinimumSize(new Dimension(60, 24));
          MOD24.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD24.setName("MOD24");
          pnlColonne5.add(MOD24, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI24 ----
          MLI24.setText("@MLI24@");
          MLI24.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI24.setMinimumSize(new Dimension(100, 24));
          MLI24.setMaximumSize(new Dimension(100, 24));
          MLI24.setName("MLI24");
          pnlColonne5.add(MLI24, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD29 ----
          MOD29.setText("@MOD29@");
          MOD29.setPreferredSize(new Dimension(60, 24));
          MOD29.setMaximumSize(new Dimension(60, 24));
          MOD29.setMinimumSize(new Dimension(60, 24));
          MOD29.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD29.setName("MOD29");
          pnlColonne5.add(MOD29, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI29 ----
          MLI29.setText("@MLI29@");
          MLI29.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI29.setMinimumSize(new Dimension(100, 24));
          MLI29.setMaximumSize(new Dimension(100, 24));
          MLI29.setName("MLI29");
          pnlColonne5.add(MLI29, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD34 ----
          MOD34.setText("@MOD34@");
          MOD34.setPreferredSize(new Dimension(60, 24));
          MOD34.setMaximumSize(new Dimension(60, 24));
          MOD34.setMinimumSize(new Dimension(60, 24));
          MOD34.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD34.setName("MOD34");
          pnlColonne5.add(MOD34, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- MLI34 ----
          MLI34.setText("@MLI34@");
          MLI34.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI34.setMinimumSize(new Dimension(100, 24));
          MLI34.setMaximumSize(new Dimension(100, 24));
          MLI34.setName("MLI34");
          pnlColonne5.add(MLI34, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlModules.add(pnlColonne5, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlColonne3 ========
        {
          pnlColonne3.setPreferredSize(new Dimension(200, 200));
          pnlColonne3.setMinimumSize(new Dimension(200, 200));
          pnlColonne3.setMaximumSize(new Dimension(200, 200));
          pnlColonne3.setName("pnlColonne3");
          pnlColonne3.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlColonne3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlColonne3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlColonne3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlColonne3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- MOD05 ----
          MOD05.setText("@MOD05@");
          MOD05.setPreferredSize(new Dimension(60, 24));
          MOD05.setMaximumSize(new Dimension(60, 24));
          MOD05.setMinimumSize(new Dimension(60, 24));
          MOD05.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD05.setName("MOD05");
          pnlColonne3.add(MOD05, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI05 ----
          MLI05.setText("@MLI05@");
          MLI05.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI05.setMinimumSize(new Dimension(100, 24));
          MLI05.setMaximumSize(new Dimension(100, 24));
          MLI05.setName("MLI05");
          pnlColonne3.add(MLI05, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD10 ----
          MOD10.setText("@MOD10@");
          MOD10.setPreferredSize(new Dimension(60, 24));
          MOD10.setMaximumSize(new Dimension(60, 24));
          MOD10.setMinimumSize(new Dimension(60, 24));
          MOD10.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD10.setName("MOD10");
          pnlColonne3.add(MOD10, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI10 ----
          MLI10.setText("@MLI10@");
          MLI10.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI10.setMinimumSize(new Dimension(100, 24));
          MLI10.setMaximumSize(new Dimension(100, 24));
          MLI10.setName("MLI10");
          pnlColonne3.add(MLI10, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD15 ----
          MOD15.setText("@MOD15@");
          MOD15.setPreferredSize(new Dimension(60, 24));
          MOD15.setMaximumSize(new Dimension(60, 24));
          MOD15.setMinimumSize(new Dimension(60, 24));
          MOD15.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD15.setName("MOD15");
          pnlColonne3.add(MOD15, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI15 ----
          MLI15.setText("@MLI15@");
          MLI15.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI15.setMinimumSize(new Dimension(100, 24));
          MLI15.setMaximumSize(new Dimension(100, 24));
          MLI15.setName("MLI15");
          pnlColonne3.add(MLI15, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD20 ----
          MOD20.setText("@MOD20@");
          MOD20.setPreferredSize(new Dimension(60, 24));
          MOD20.setMaximumSize(new Dimension(60, 24));
          MOD20.setMinimumSize(new Dimension(60, 24));
          MOD20.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD20.setName("MOD20");
          pnlColonne3.add(MOD20, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI20 ----
          MLI20.setText("@MLI20@");
          MLI20.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI20.setMinimumSize(new Dimension(100, 24));
          MLI20.setMaximumSize(new Dimension(100, 24));
          MLI20.setName("MLI20");
          pnlColonne3.add(MLI20, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD25 ----
          MOD25.setText("@MOD25@");
          MOD25.setPreferredSize(new Dimension(60, 24));
          MOD25.setMaximumSize(new Dimension(60, 24));
          MOD25.setMinimumSize(new Dimension(60, 24));
          MOD25.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD25.setName("MOD25");
          pnlColonne3.add(MOD25, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI25 ----
          MLI25.setText("@MLI25@");
          MLI25.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI25.setMinimumSize(new Dimension(100, 24));
          MLI25.setMaximumSize(new Dimension(100, 24));
          MLI25.setName("MLI25");
          pnlColonne3.add(MLI25, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD30 ----
          MOD30.setText("@MOD30@");
          MOD30.setPreferredSize(new Dimension(60, 24));
          MOD30.setMaximumSize(new Dimension(60, 24));
          MOD30.setMinimumSize(new Dimension(60, 24));
          MOD30.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD30.setName("MOD30");
          pnlColonne3.add(MOD30, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- MLI30 ----
          MLI30.setText("@MLI30@");
          MLI30.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI30.setMinimumSize(new Dimension(100, 24));
          MLI30.setMaximumSize(new Dimension(100, 24));
          MLI30.setName("MLI30");
          pnlColonne3.add(MLI30, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MOD35 ----
          MOD35.setText("@MOD35@");
          MOD35.setPreferredSize(new Dimension(60, 24));
          MOD35.setMaximumSize(new Dimension(60, 24));
          MOD35.setMinimumSize(new Dimension(60, 24));
          MOD35.setFont(new Font("sansserif", Font.PLAIN, 14));
          MOD35.setName("MOD35");
          pnlColonne3.add(MOD35, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- MLI35 ----
          MLI35.setText("@MLI35@");
          MLI35.setFont(new Font("sansserif", Font.PLAIN, 14));
          MLI35.setMinimumSize(new Dimension(100, 24));
          MLI35.setMaximumSize(new Dimension(100, 24));
          MLI35.setName("MLI35");
          pnlColonne3.add(MLI35, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlModules.add(pnlColonne3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlModules, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlJava ========
      {
        pnlJava.setOpaque(false);
        pnlJava.setTitre("Informations sur Java");
        pnlJava.setName("pnlJava");
        pnlJava.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlJava.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlJava.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlJava.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlJava.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbLibelle1Java ----
        lbLibelle1Java.setText("@LIBJV1@");
        lbLibelle1Java.setMaximumSize(new Dimension(150, 25));
        lbLibelle1Java.setMinimumSize(new Dimension(150, 25));
        lbLibelle1Java.setPreferredSize(new Dimension(150, 25));
        lbLibelle1Java.setName("lbLibelle1Java");
        pnlJava.add(lbLibelle1Java, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLibelle2Java ----
        lbLibelle2Java.setText("@LIBJV2@");
        lbLibelle2Java.setPreferredSize(new Dimension(150, 25));
        lbLibelle2Java.setMinimumSize(new Dimension(150, 25));
        lbLibelle2Java.setMaximumSize(new Dimension(150, 25));
        lbLibelle2Java.setName("lbLibelle2Java");
        pnlJava.add(lbLibelle2Java, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlJava,
          new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlServeurOS400;
  private SNLabelChamp lbVersionOs400;
  private RiZoneSortie OS400;
  private SNLabelChamp lbNumeroSerie;
  private RiZoneSortie SRLNBR;
  private SNLabelChamp lbModeleServeur;
  private RiZoneSortie MODEL;
  private SNPanelTitre pnlNiveauProgrammes;
  private SNLabelChamp lbVersionProgramme;
  private RiZoneSortie VER220;
  private SNMessage label1;
  private SNPanelTitre pnlClient;
  private SNLabelChamp lbNumeroClient;
  private RiZoneSortie WCLI;
  private RiZoneSortie WLIV;
  private SNLabelChamp lbNombreUtilisateurs;
  private RiZoneSortie WNBUTI;
  private SNPanelTitre pnlModules;
  private SNPanel pnlColonne1;
  private RiZoneSortie MOD01;
  private RiZoneSortie MLI01;
  private RiZoneSortie MOD06;
  private RiZoneSortie MLI06;
  private RiZoneSortie MOD11;
  private RiZoneSortie MLI11;
  private RiZoneSortie MOD16;
  private RiZoneSortie MLI16;
  private RiZoneSortie MOD21;
  private RiZoneSortie MLI21;
  private RiZoneSortie MOD26;
  private RiZoneSortie MLI26;
  private RiZoneSortie MOD31;
  private RiZoneSortie MLI31;
  private SNPanel pnlColonne2;
  private RiZoneSortie MOD02;
  private RiZoneSortie MLI02;
  private RiZoneSortie MOD07;
  private RiZoneSortie MLI07;
  private RiZoneSortie MOD12;
  private RiZoneSortie MLI12;
  private RiZoneSortie MOD17;
  private RiZoneSortie MLI17;
  private RiZoneSortie MOD22;
  private RiZoneSortie MLI22;
  private RiZoneSortie MOD27;
  private RiZoneSortie MLI27;
  private RiZoneSortie MOD32;
  private RiZoneSortie MLI32;
  private SNPanel pnlColonne4;
  private RiZoneSortie MOD03;
  private RiZoneSortie MLI03;
  private RiZoneSortie MOD08;
  private RiZoneSortie MLI08;
  private RiZoneSortie MOD13;
  private RiZoneSortie MLI13;
  private RiZoneSortie MOD18;
  private RiZoneSortie MLI18;
  private RiZoneSortie MOD23;
  private RiZoneSortie MLI23;
  private RiZoneSortie MOD28;
  private RiZoneSortie MLI28;
  private RiZoneSortie MOD33;
  private RiZoneSortie MLI33;
  private SNPanel pnlColonne5;
  private RiZoneSortie MOD04;
  private RiZoneSortie MLI04;
  private RiZoneSortie MOD09;
  private RiZoneSortie MLI09;
  private RiZoneSortie MOD14;
  private RiZoneSortie MLI14;
  private RiZoneSortie MOD19;
  private RiZoneSortie MLI19;
  private RiZoneSortie MOD24;
  private RiZoneSortie MLI24;
  private RiZoneSortie MOD29;
  private RiZoneSortie MLI29;
  private RiZoneSortie MOD34;
  private RiZoneSortie MLI34;
  private SNPanel pnlColonne3;
  private RiZoneSortie MOD05;
  private RiZoneSortie MLI05;
  private RiZoneSortie MOD10;
  private RiZoneSortie MLI10;
  private RiZoneSortie MOD15;
  private RiZoneSortie MLI15;
  private RiZoneSortie MOD20;
  private RiZoneSortie MLI20;
  private RiZoneSortie MOD25;
  private RiZoneSortie MLI25;
  private RiZoneSortie MOD30;
  private RiZoneSortie MLI30;
  private RiZoneSortie MOD35;
  private RiZoneSortie MLI35;
  private SNPanelTitre pnlJava;
  private SNMessage lbLibelle1Java;
  private SNMessage lbLibelle2Java;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
