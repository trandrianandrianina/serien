
package ri.serien.libecranrpg.sgvm.SGVM55FM;
// Nom Fichier: pop_SGVM55FM_FMTP6_829.java

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM55FM_P6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM55FM_P6(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITLE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void BT_ENTERActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_4 = new JLabel();
    P_PnlOpts = new JPanel();
    BT_ENTER = new JButton();

    //======== this ========
    setName("this");

    //---- OBJ_4 ----
    OBJ_4.setText("@TITLE@");
    OBJ_4.setName("OBJ_4");

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }

    //---- BT_ENTER ----
    BT_ENTER.setToolTipText("Ok");
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ENTERActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(116, 116, 116)
              .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(315, 315, 315)
              .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)))
          .addGap(665, 665, 665)
          .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(40, 40, 40)
          .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
          .addGap(10, 10, 10)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 516, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_4;
  private JPanel P_PnlOpts;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
