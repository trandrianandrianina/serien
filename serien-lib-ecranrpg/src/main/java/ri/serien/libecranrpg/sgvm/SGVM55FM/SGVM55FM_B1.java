
package ri.serien.libecranrpg.sgvm.SGVM55FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM55FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM55FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    OPT1.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT3.setValeursSelection("X", " ");
    WNOMCF.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    snPlageDatesBons.setDefilement(EnumDefilementPlageDate.SANS_DEFILEMENT);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    pnlSelectionClient.setVisible(!WTOU.isSelected());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    snPlageDatesBons.setDateDebutParChampRPG(lexique, "PERDEB");
    snPlageDatesBons.setDateFinParChampRPG(lexique, "PERFIN");
    
    chargerComposantClient();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Composant snPlageDate
    snPlageDatesBons.renseignerChampRPGDebut(lexique, "PERDEB");
    snPlageDatesBons.renseignerChampRPGFin(lexique, "PERFIN");
    
    // Composant snClient
    snClient.renseignerChampRPG(lexique, "NUMCLX", "NUMSX");
  }
  
  private void chargerComposantClient() {
    snClient.setSession(getSession());
    snClient.setIdEtablissement(snEtablissement.getIdSelection());
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "NUMCLX", "NUMSX");
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    pnlSelectionClient.setVisible(!pnlSelectionClient.isVisible());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantClient();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlPlageDeDates = new SNPanelTitre();
    lbPlage = new SNLabelChamp();
    snPlageDatesBons = new SNPlageDate();
    pnlNumeroClient = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    pnlSelectionClient = new SNPanel();
    lbClient = new SNLabelChamp();
    snClient = new SNClient();
    pnlSelectionBons = new SNPanelTitre();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    WNOMCF = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlPlageDeDates ========
        {
          pnlPlageDeDates.setTitre("Plage de dates \u00e0 traiter");
          pnlPlageDeDates.setName("pnlPlageDeDates");
          pnlPlageDeDates.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageDeDates.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageDeDates.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPlageDeDates.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageDeDates.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbPlage ----
          lbPlage.setText("Edition des bons");
          lbPlage.setName("lbPlage");
          pnlPlageDeDates.add(lbPlage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snPlageDatesBons ----
          snPlageDatesBons.setName("snPlageDatesBons");
          pnlPlageDeDates.add(snPlageDatesBons, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageDeDates, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlNumeroClient ========
        {
          pnlNumeroClient.setTitre("S\u00e9lection du client \u00e0 traiter");
          pnlNumeroClient.setName("pnlNumeroClient");
          pnlNumeroClient.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlNumeroClient.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlNumeroClient.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlNumeroClient.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlNumeroClient.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMaximumSize(new Dimension(45, 30));
          WTOU.setMinimumSize(new Dimension(45, 30));
          WTOU.setPreferredSize(new Dimension(45, 30));
          WTOU.setName("WTOU");
          WTOU.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTOUItemStateChanged(e);
            }
          });
          pnlNumeroClient.add(WTOU, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionClient ========
          {
            pnlSelectionClient.setName("pnlSelectionClient");
            pnlSelectionClient.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionClient.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionClient.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlSelectionClient.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionClient.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- lbClient ----
            lbClient.setText("Code client");
            lbClient.setName("lbClient");
            pnlSelectionClient.add(lbClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snClient ----
            snClient.setName("snClient");
            pnlSelectionClient.add(snClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNumeroClient.add(pnlSelectionClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlNumeroClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlSelectionBons ========
        {
          pnlSelectionBons.setTitre("S\u00e9lection des bons \u00e0 traiter");
          pnlSelectionBons.setName("pnlSelectionBons");
          pnlSelectionBons.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlSelectionBons.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlSelectionBons.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlSelectionBons.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlSelectionBons.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- OPT1 ----
          OPT1.setText("Bons en attente (ATT)");
          OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT1.setMaximumSize(new Dimension(239, 30));
          OPT1.setMinimumSize(new Dimension(239, 30));
          OPT1.setName("OPT1");
          OPT1.setPreferredSize(new Dimension(239, 30));
          pnlSelectionBons.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OPT2 ----
          OPT2.setText("Bons valid\u00e9s non exp\u00e9di\u00e9s (VAL)");
          OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT2.setMaximumSize(new Dimension(239, 30));
          OPT2.setMinimumSize(new Dimension(239, 30));
          OPT2.setName("OPT2");
          OPT2.setPreferredSize(new Dimension(239, 30));
          pnlSelectionBons.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OPT3 ----
          OPT3.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
          OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT3.setMaximumSize(new Dimension(239, 30));
          OPT3.setMinimumSize(new Dimension(239, 30));
          OPT3.setPreferredSize(new Dimension(239, 30));
          OPT3.setName("OPT3");
          pnlSelectionBons.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlSelectionBons, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptionEdition ========
        {
          pnlOptionEdition.setTitre("Options d'\u00e9dition");
          pnlOptionEdition.setName("pnlOptionEdition");
          pnlOptionEdition.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptionEdition.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlOptionEdition.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlOptionEdition.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlOptionEdition.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- WNOMCF ----
          WNOMCF.setText("Edition du nom du client factur\u00e9");
          WNOMCF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WNOMCF.setFont(new Font("sansserif", Font.PLAIN, 14));
          WNOMCF.setMaximumSize(new Dimension(222, 30));
          WNOMCF.setMinimumSize(new Dimension(222, 30));
          WNOMCF.setPreferredSize(new Dimension(222, 30));
          WNOMCF.setName("WNOMCF");
          pnlOptionEdition.add(WNOMCF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPlageDeDates;
  private SNLabelChamp lbPlage;
  private SNPlageDate snPlageDatesBons;
  private SNPanelTitre pnlNumeroClient;
  private XRiCheckBox WTOU;
  private SNPanel pnlSelectionClient;
  private SNLabelChamp lbClient;
  private SNClient snClient;
  private SNPanelTitre pnlSelectionBons;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox WNOMCF;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
