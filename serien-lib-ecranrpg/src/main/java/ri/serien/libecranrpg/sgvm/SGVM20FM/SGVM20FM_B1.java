
package ri.serien.libecranrpg.sgvm.SGVM20FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComboBox;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM261] Gestion des ventes -> Documents de ventes -> Factures périodiques -> Facturation périodique
 * Indicateur:01000001
 * Titre:Facturation périodique
 */
public class SGVM20FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private int indexSelectionne = 0;
  
  public SGVM20FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverse après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Indicateur
    boolean isParametragePlanification = lexique.isTrue("97");
    
    // Visibilité
    lbDateFacturation.setVisible(!isParametragePlanification);
    
    // Récupération des type de facturation
    CODTF.removeAllItems();
    String[] CODTF_value = { lexique.HostFieldGetData("TF11"), lexique.HostFieldGetData("TF12"), lexique.HostFieldGetData("TF13"),
        lexique.HostFieldGetData("TF14"), lexique.HostFieldGetData("TF15"), lexique.HostFieldGetData("TF16"),
        lexique.HostFieldGetData("TF17") };
    String[] CODTF_titre = { lexique.HostFieldGetData("TFL1"), lexique.HostFieldGetData("TFL2"), lexique.HostFieldGetData("TFL3"),
        lexique.HostFieldGetData("TFL4"), lexique.HostFieldGetData("TFL5"), lexique.HostFieldGetData("TFL6"),
        lexique.HostFieldGetData("TFL7") };
    for (int i = 0; i < CODTF_value.length; i++) {
      if (!CODTF_titre[i].trim().isEmpty() && !CODTF_value[i].trim().isEmpty()) {
        CODTF.addItem(new TypeFac(CODTF_titre[i], CODTF_value[i], i));
      }
    }
    CODTF.setSelectedIndex(indexSelectionne);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // charge les composants
    chargerComposantMagasin();
    chargerComposantRepresentant();
    chargerComposantTransporteur();
    chargerComposantCategorieClient();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Gestion de la sélection complete
    if (snMagasin.getIdSelection() == null) {
      lexique.HostFieldPutData("WMAG", 0, "**");
    }
    else {
      snMagasin.renseignerChampRPG(lexique, "WMAG");
    }
    
    if (snRepresentant.getIdSelection() == null) {
      lexique.HostFieldPutData("CODREP", 0, "**");
    }
    else {
      snRepresentant.renseignerChampRPG(lexique, "CODREP");
    }
    
    if (snCategorieClient.getIdSelection() == null) {
      lexique.HostFieldPutData("CODCC", 0, "**");
    }
    else {
      snCategorieClient.renseignerChampRPG(lexique, "CODCC");
    }
    
    if (snTransporteur.getIdSelection() == null) {
      lexique.HostFieldPutData("CODTR", 0, "**");
    }
    else {
      snTransporteur.renseignerChampRPG(lexique, "CODTR");
    }
    
    lexique.HostFieldPutData("CODTF", 0, ((TypeFac) CODTF.getSelectedItem()).valeur);
    indexSelectionne = CODTF.getSelectedIndex();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composant magasins
   */
  private void chargerComposantMagasin() {
    // Renseigner le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Charge le composant transporteurs
   */
  private void chargerComposantTransporteur() {
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(snEtablissement.getIdSelection());
    snTransporteur.setTousAutorise(true);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "CODTR");
  }
  
  /**
   * Charge le composant catégorie clients
   */
  private void chargerComposantCategorieClient() {
    snCategorieClient.setSession(getSession());
    snCategorieClient.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClient.setTousAutorise(true);
    snCategorieClient.charger(false);
    snCategorieClient.setSelectionParChampRPG(lexique, "CODCC");
  }
  
  /**
   * Charge le composant representants
   */
  private void chargerComposantRepresentant() {
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "CODREP");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantMagasin();
      chargerComposantRepresentant();
      chargerComposantTransporteur();
      chargerComposantCategorieClient();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlDroite = new SNPanel();
    pnlInformationFacturation = new SNPanelTitre();
    lbDateFacturation = new SNLabelChamp();
    lbTypeFacturation = new SNLabelChamp();
    DATFAC = new XRiCalendrier();
    CODTF = new JComboBox();
    pnlCritereSelection = new SNPanelTitre();
    lbRepresentant = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    lbCategorie = new SNLabelChamp();
    snCategorieClient = new SNCategorieClient();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlGauche = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfPeriodeEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriodeEnCours = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Facturation p\u00e9riodique");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlInformationFacturation ========
        {
          pnlInformationFacturation.setTitre("Informations de facturation");
          pnlInformationFacturation.setName("pnlInformationFacturation");
          pnlInformationFacturation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlInformationFacturation.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlInformationFacturation.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlInformationFacturation.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlInformationFacturation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbDateFacturation ----
          lbDateFacturation.setText("Date de facturation");
          lbDateFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateFacturation.setName("lbDateFacturation");
          pnlInformationFacturation.add(lbDateFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbTypeFacturation ----
          lbTypeFacturation.setText("Type de facturation");
          lbTypeFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTypeFacturation.setPreferredSize(new Dimension(142, 30));
          lbTypeFacturation.setMinimumSize(new Dimension(142, 30));
          lbTypeFacturation.setName("lbTypeFacturation");
          pnlInformationFacturation.add(lbTypeFacturation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- DATFAC ----
          DATFAC.setPreferredSize(new Dimension(110, 30));
          DATFAC.setMinimumSize(new Dimension(110, 30));
          DATFAC.setFont(new Font("sansserif", Font.PLAIN, 14));
          DATFAC.setName("DATFAC");
          pnlInformationFacturation.add(DATFAC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CODTF ----
          CODTF.setFont(new Font("sansserif", Font.PLAIN, 14));
          CODTF.setPreferredSize(new Dimension(75, 30));
          CODTF.setMinimumSize(new Dimension(75, 30));
          CODTF.setMaximumSize(new Dimension(75, 30));
          CODTF.setName("CODTF");
          pnlInformationFacturation.add(CODTF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlInformationFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbRepresentant ----
          lbRepresentant.setText("Repr\u00e9sentant");
          lbRepresentant.setName("lbRepresentant");
          pnlCritereSelection.add(lbRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snRepresentant ----
          snRepresentant.setName("snRepresentant");
          pnlCritereSelection.add(snRepresentant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbCategorie ----
          lbCategorie.setText("Cat\u00e9gorie client");
          lbCategorie.setName("lbCategorie");
          pnlCritereSelection.add(lbCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snCategorieClient ----
          snCategorieClient.setFont(new Font("sansserif", Font.PLAIN, 14));
          snCategorieClient.setName("snCategorieClient");
          pnlCritereSelection.add(snCategorieClient, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbTransporteur ----
          lbTransporteur.setText("Transporteur");
          lbTransporteur.setName("lbTransporteur");
          pnlCritereSelection.add(lbTransporteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snTransporteur ----
          snTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
          snTransporteur.setName("snTransporteur");
          pnlCritereSelection.add(snTransporteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlCritereSelection.add(lbMagasin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasin ----
          snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin.setName("snMagasin");
          pnlCritereSelection.add(snMagasin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlCritereSelection, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
          tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
          tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlGauche.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlInformationFacturation;
  private SNLabelChamp lbDateFacturation;
  private SNLabelChamp lbTypeFacturation;
  private XRiCalendrier DATFAC;
  private JComboBox CODTF;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbCategorie;
  private SNCategorieClient snCategorieClient;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfPeriodeEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}

class TypeFac {
  String nom;
  String valeur;
  int index;
  
  public TypeFac(String n, String v, int i) {
    nom = n;
    valeur = v;
    index = i;
  }
  
  @Override
  public String toString() {
    return nom;
  }
  
}
