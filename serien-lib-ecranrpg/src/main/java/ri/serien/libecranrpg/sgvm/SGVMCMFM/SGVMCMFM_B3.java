
package ri.serien.libecranrpg.sgvm.SGVMCMFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;

/**
 * @author Stéphane Vénéri
 */
public class SGVMCMFM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMCMFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@TRIP@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // TRI6.setEnabled( lexique.isPresent("TRI6"));
    // TRI5.setEnabled( lexique.isPresent("TRI5"));
    // TRI4.setEnabled( lexique.isPresent("TRI4"));
    // TRI3.setEnabled( lexique.isPresent("TRI3"));
    // TRI2.setEnabled( lexique.isPresent("TRI2"));
    // TRI1.setEnabled( lexique.isPresent("TRI1"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_16 = new JLabel();
    TRI1 = new XRiComboBox();
    TRI2 = new XRiComboBox();
    TRI3 = new XRiComboBox();
    TRI4 = new XRiComboBox();
    TRI5 = new XRiComboBox();
    TRI6 = new XRiComboBox();
    OBJ_10 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_14 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_18 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(470, 270));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(480, 285));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("@TRIP@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_16 ----
          OBJ_16.setText("Zone G\u00e9ographique");
          OBJ_16.setName("OBJ_16");
          panel1.add(OBJ_16);
          OBJ_16.setBounds(30, 130, 142, 20);

          //---- TRI1 ----
          TRI1.setModel(new DefaultComboBoxModel(new String[] {
            "1",
            "2",
            "3"
          }));
          TRI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI1.setName("TRI1");
          panel1.add(TRI1);
          TRI1.setBounds(200, 40, 45, TRI1.getPreferredSize().height);

          //---- TRI2 ----
          TRI2.setModel(new DefaultComboBoxModel(new String[] {
            "1",
            "2",
            "3"
          }));
          TRI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI2.setName("TRI2");
          panel1.add(TRI2);
          TRI2.setBounds(200, 69, 45, TRI2.getPreferredSize().height);

          //---- TRI3 ----
          TRI3.setModel(new DefaultComboBoxModel(new String[] {
            "1",
            "2",
            "3"
          }));
          TRI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI3.setName("TRI3");
          panel1.add(TRI3);
          TRI3.setBounds(200, 98, 45, TRI3.getPreferredSize().height);

          //---- TRI4 ----
          TRI4.setModel(new DefaultComboBoxModel(new String[] {
            "1",
            "2",
            "3"
          }));
          TRI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI4.setName("TRI4");
          panel1.add(TRI4);
          TRI4.setBounds(200, 127, 45, TRI4.getPreferredSize().height);

          //---- TRI5 ----
          TRI5.setModel(new DefaultComboBoxModel(new String[] {
            "1",
            "2",
            "3"
          }));
          TRI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI5.setName("TRI5");
          panel1.add(TRI5);
          TRI5.setBounds(200, 156, 45, TRI5.getPreferredSize().height);

          //---- TRI6 ----
          TRI6.setModel(new DefaultComboBoxModel(new String[] {
            "1",
            "2",
            "3"
          }));
          TRI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI6.setName("TRI6");
          panel1.add(TRI6);
          TRI6.setBounds(200, 185, 45, TRI6.getPreferredSize().height);

          //---- OBJ_10 ----
          OBJ_10.setText("Nom du Client");
          OBJ_10.setName("OBJ_10");
          panel1.add(OBJ_10);
          OBJ_10.setBounds(30, 43, 104, 20);

          //---- OBJ_12 ----
          OBJ_12.setText("Code Postal");
          OBJ_12.setName("OBJ_12");
          panel1.add(OBJ_12);
          OBJ_12.setBounds(30, 72, 95, 20);

          //---- OBJ_14 ----
          OBJ_14.setText("Code Pays");
          OBJ_14.setName("OBJ_14");
          panel1.add(OBJ_14);
          OBJ_14.setBounds(30, 101, 88, 20);

          //---- OBJ_20 ----
          OBJ_20.setText("Code Ape");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(30, 188, 82, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("Cat\u00e9gorie");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(30, 159, 80, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_16;
  private XRiComboBox TRI1;
  private XRiComboBox TRI2;
  private XRiComboBox TRI3;
  private XRiComboBox TRI4;
  private XRiComboBox TRI5;
  private XRiComboBox TRI6;
  private JLabel OBJ_10;
  private JLabel OBJ_12;
  private JLabel OBJ_14;
  private JLabel OBJ_20;
  private JLabel OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
