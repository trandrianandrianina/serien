
package ri.serien.libecranrpg.sgvm.SGVMDAFM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMDAFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public SGVMDAFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    OPT1.setValeursSelection("G", " ");
    OPT2.setValeursSelection("F", " ");
    OPT3.setValeursSelection("A", " ");
    OPT4.setValeursSelection("T", " ");
    OPT5.setValeursSelection("R", " ");
    OPT6.setValeursSelection("1", " ");
    OPT7.setValeursSelection("2", " ");
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Composant établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Composant client début
    snClientDebut.setSession(getSession());
    snClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snClientDebut.charger(false);
    snClientDebut.setSelectionParChampRPG(lexique, "DEBCLI");
    
    // Composant client fin
    snClientFin.setSession(getSession());
    snClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snClientFin.charger(false);
    snClientFin.setSelectionParChampRPG(lexique, "FINCLI");
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snClientDebut.renseignerChampRPG(lexique, "DEBCLI");
    snClientFin.renseignerChampRPG(lexique, "FINCLI");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbClientDebut = new SNLabelChamp();
    snClientDebut = new SNClientPrincipal();
    lbClientFin = new SNLabelChamp();
    snClientFin = new SNClientPrincipal();
    pnlEtatAvoir = new SNPanelTitre();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    OPT4 = new XRiCheckBox();
    OPT5 = new XRiCheckBox();
    OPT6 = new XRiCheckBox();
    OPT7 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlCritereSelection = new SNPanelTitre();
    lbDatesDebut = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    WDADD = new XRiCalendrier();
    label3 = new JLabel();
    WDADF = new XRiCalendrier();
    lbDatesFin = new SNLabelChamp();
    sNPanel2 = new SNPanel();
    WDAFD = new XRiCalendrier();
    label4 = new JLabel();
    WDAFF = new XRiCalendrier();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0 };
            
            // ---- lbClientDebut ----
            lbClientDebut.setText("Client de d\u00e9but");
            lbClientDebut.setPreferredSize(new Dimension(170, 30));
            lbClientDebut.setMinimumSize(new Dimension(170, 30));
            lbClientDebut.setMaximumSize(new Dimension(170, 30));
            lbClientDebut.setName("lbClientDebut");
            pnlCritereDeSelection.add(lbClientDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClientDebut ----
            snClientDebut.setName("snClientDebut");
            pnlCritereDeSelection.add(snClientDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClientFin ----
            lbClientFin.setText("Client de fin");
            lbClientFin.setName("lbClientFin");
            pnlCritereDeSelection.add(lbClientFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snClientFin ----
            snClientFin.setName("snClientFin");
            pnlCritereDeSelection.add(snClientFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlEtatAvoir ========
          {
            pnlEtatAvoir.setTitre("Types de rattachements \u00e0 traiter");
            pnlEtatAvoir.setName("pnlEtatAvoir");
            pnlEtatAvoir.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtatAvoir.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlEtatAvoir.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlEtatAvoir.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtatAvoir.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OPT1 ----
            OPT1.setText("Rattachement \u00e0 un groupe");
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setMinimumSize(new Dimension(124, 30));
            OPT1.setPreferredSize(new Dimension(124, 30));
            OPT1.setName("OPT1");
            pnlEtatAvoir.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Rattachement \u00e0 une famille");
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setMinimumSize(new Dimension(124, 30));
            OPT2.setPreferredSize(new Dimension(124, 30));
            OPT2.setName("OPT2");
            pnlEtatAvoir.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Rattachement \u00e0 un article");
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setMinimumSize(new Dimension(124, 30));
            OPT3.setPreferredSize(new Dimension(124, 30));
            OPT3.setName("OPT3");
            pnlEtatAvoir.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT4 ----
            OPT4.setText("Rattachement \u00e0 un tarif");
            OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT4.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT4.setMinimumSize(new Dimension(124, 30));
            OPT4.setPreferredSize(new Dimension(124, 30));
            OPT4.setName("OPT4");
            pnlEtatAvoir.add(OPT4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT5 ----
            OPT5.setText("Rattachement \u00e0 un ensemble article");
            OPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT5.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT5.setMinimumSize(new Dimension(124, 30));
            OPT5.setPreferredSize(new Dimension(124, 30));
            OPT5.setName("OPT5");
            pnlEtatAvoir.add(OPT5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT6 ----
            OPT6.setText("Rattachement \u00e0 un regroupement d'achat");
            OPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT6.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT6.setMinimumSize(new Dimension(124, 30));
            OPT6.setPreferredSize(new Dimension(124, 30));
            OPT6.setName("OPT6");
            pnlEtatAvoir.add(OPT6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT7 ----
            OPT7.setText("Rattachement \u00e0 un fournisseur");
            OPT7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT7.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT7.setMinimumSize(new Dimension(124, 30));
            OPT7.setPreferredSize(new Dimension(124, 30));
            OPT7.setName("OPT7");
            pnlEtatAvoir.add(OPT7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlEtatAvoir, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Dates de validit\u00e9");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDatesDebut ----
            lbDatesDebut.setText("Dates de d\u00e9but du");
            lbDatesDebut.setName("lbDatesDebut");
            pnlCritereSelection.add(lbDatesDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDADD ----
              WDADD.setMaximumSize(new Dimension(120, 30));
              WDADD.setMinimumSize(new Dimension(120, 30));
              WDADD.setPreferredSize(new Dimension(120, 30));
              WDADD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDADD.setName("WDADD");
              sNPanel1.add(WDADD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- label3 ----
              label3.setText("au");
              label3.setHorizontalAlignment(SwingConstants.CENTER);
              label3.setFont(new Font("sansserif", Font.PLAIN, 14));
              label3.setName("label3");
              sNPanel1.add(label3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDADF ----
              WDADF.setMaximumSize(new Dimension(120, 30));
              WDADF.setMinimumSize(new Dimension(120, 30));
              WDADF.setPreferredSize(new Dimension(120, 30));
              WDADF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDADF.setName("WDADF");
              sNPanel1.add(WDADF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDatesFin ----
            lbDatesFin.setText("Dates de fin du");
            lbDatesFin.setName("lbDatesFin");
            pnlCritereSelection.add(lbDatesFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== sNPanel2 ========
            {
              sNPanel2.setName("sNPanel2");
              sNPanel2.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDAFD ----
              WDAFD.setMaximumSize(new Dimension(120, 30));
              WDAFD.setMinimumSize(new Dimension(120, 30));
              WDAFD.setPreferredSize(new Dimension(120, 30));
              WDAFD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDAFD.setName("WDAFD");
              sNPanel2.add(WDAFD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- label4 ----
              label4.setText("au");
              label4.setHorizontalAlignment(SwingConstants.CENTER);
              label4.setFont(new Font("sansserif", Font.PLAIN, 14));
              label4.setName("label4");
              sNPanel2.add(label4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDAFF ----
              WDAFF.setMaximumSize(new Dimension(120, 30));
              WDAFF.setMinimumSize(new Dimension(120, 30));
              WDAFF.setPreferredSize(new Dimension(120, 30));
              WDAFF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDAFF.setName("WDAFF");
              sNPanel2.add(WDAFF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(sNPanel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlCritereSelection, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbClientDebut;
  private SNClientPrincipal snClientDebut;
  private SNLabelChamp lbClientFin;
  private SNClientPrincipal snClientFin;
  private SNPanelTitre pnlEtatAvoir;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private XRiCheckBox OPT4;
  private XRiCheckBox OPT5;
  private XRiCheckBox OPT6;
  private XRiCheckBox OPT7;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbDatesDebut;
  private SNPanel sNPanel1;
  private XRiCalendrier WDADD;
  private JLabel label3;
  private XRiCalendrier WDADF;
  private SNLabelChamp lbDatesFin;
  private SNPanel sNPanel2;
  private XRiCalendrier WDAFD;
  private JLabel label4;
  private XRiCalendrier WDAFF;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
