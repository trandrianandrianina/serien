
package ri.serien.libecranrpg.sgvm.SGVMIVEF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMIVEF_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] TYPDOC_Value = { "C", "D" };
  
  public SGVMIVEF_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TYPDOC.setValeurs(TYPDOC_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    FICENT = new XRiTextField();
    label6 = new JLabel();
    label8 = new JLabel();
    WETB = new XRiTextField();
    label11 = new JLabel();
    label12 = new JLabel();
    label15 = new JLabel();
    WCLI = new XRiTextField();
    WLIV = new XRiTextField();
    label16 = new JLabel();
    WNOM = new XRiTextField();
    TYPDOC = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- label1 ----
          label1.setText("Les fichiers tableurs doivent \u00eatre enregistr\u00e9s avec une extension \".CSV\" et plac\u00e9s dans un ");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 3f));
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setName("label1");
          p_contenu.add(label1);
          label1.setBounds(40, 94, 765, 30);

          //---- label2 ----
          label2.setText("dossier nomm\u00e9 \"INJECTEUR\"   ");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 3f));
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setName("label2");
          p_contenu.add(label2);
          label2.setBounds(40, 124, 765, 30);

          //---- label3 ----
          label3.setText("Ce dossier \"INJECTEUR\" doit \u00eatre cr\u00e9e dans le r\u00e9pertoire du param\u00e8tre \"NT\" ");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() + 3f));
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setName("label3");
          p_contenu.add(label3);
          label3.setBounds(40, 184, 765, 30);

          //---- label4 ----
          label4.setText("(Menu EXP8.1 -  Exemple \"DOCUMENTS\")   ");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getSize() + 3f));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          p_contenu.add(label4);
          label4.setBounds(40, 214, 765, 30);

          //---- label5 ----
          label5.setText("ATTENTION");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getSize() + 3f));
          label5.setForeground(new Color(204, 0, 0));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          p_contenu.add(label5);
          label5.setBounds(40, 44, 765, 30);

          //---- FICENT ----
          FICENT.setName("FICENT");
          p_contenu.add(FICENT);
          FICENT.setBounds(315, 279, 380, FICENT.getPreferredSize().height);

          //---- label6 ----
          label6.setText("Nom du fichier \u00e0 injecter");
          label6.setName("label6");
          p_contenu.add(label6);
          label6.setBounds(40, 279, 275, 28);

          //---- label8 ----
          label8.setText(".CSV");
          label8.setName("label8");
          p_contenu.add(label8);
          label8.setBounds(700, 279, label8.getPreferredSize().width, 28);

          //---- WETB ----
          WETB.setName("WETB");
          p_contenu.add(WETB);
          WETB.setBounds(315, 340, 40, WETB.getPreferredSize().height);

          //---- label11 ----
          label11.setText("Num\u00e9ro client");
          label11.setName("label11");
          p_contenu.add(label11);
          label11.setBounds(40, 340, 275, 28);

          //---- label12 ----
          label12.setText("Type de document");
          label12.setName("label12");
          p_contenu.add(label12);
          label12.setBounds(40, 434, 275, 28);

          //---- label15 ----
          label15.setText("SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER");
          label15.setHorizontalAlignment(SwingConstants.CENTER);
          label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
          label15.setForeground(new Color(153, 0, 0));
          label15.setName("label15");
          p_contenu.add(label15);
          label15.setBounds(40, 14, 765, 23);

          //---- WCLI ----
          WCLI.setName("WCLI");
          p_contenu.add(WCLI);
          WCLI.setBounds(370, 340, 65, WCLI.getPreferredSize().height);

          //---- WLIV ----
          WLIV.setName("WLIV");
          p_contenu.add(WLIV);
          WLIV.setBounds(435, 340, 40, WLIV.getPreferredSize().height);

          //---- label16 ----
          label16.setText("ou recherche");
          label16.setName("label16");
          p_contenu.add(label16);
          label16.setBounds(40, 375, 275, 28);

          //---- WNOM ----
          WNOM.setName("WNOM");
          p_contenu.add(WNOM);
          WNOM.setBounds(315, 375, 230, WNOM.getPreferredSize().height);

          //---- TYPDOC ----
          TYPDOC.setModel(new DefaultComboBoxModel(new String[] {
            "Commande",
            "Devis"
          }));
          TYPDOC.setName("TYPDOC");
          p_contenu.add(TYPDOC);
          TYPDOC.setBounds(315, 435, 110, TYPDOC.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private XRiTextField FICENT;
  private JLabel label6;
  private JLabel label8;
  private XRiTextField WETB;
  private JLabel label11;
  private JLabel label12;
  private JLabel label15;
  private XRiTextField WCLI;
  private XRiTextField WLIV;
  private JLabel label16;
  private XRiTextField WNOM;
  private XRiComboBox TYPDOC;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
