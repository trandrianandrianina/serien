
package ri.serien.libecranrpg.sgvm.SGVM33FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM33FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] WCOD2_Value = { "", "2", };
  private String[] TDVFIN_Value = { "", "0", "1", "2", "3", "4", "5", "6", };
  private String[] TDVDEB_Value = { "", "0", "1", "2", "3", "4", "5", "6", };
  
  public SGVM33FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    NOMCLF.setValeursSelection("OUI", "NON");
    AGSTAT.setValeursSelection("OUI", "NON");
    LIGVAL.setValeursSelection("OUI", "NON");
    EMARGE.setValeursSelection("OUI", "NON");
    OPTED3.setValeursSelection("O", "N");
    OPTED2.setValeursSelection("O", "N");
    OPTED1.setValeursSelection("O", "N");
    OPT3.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT1.setValeursSelection("X", " ");
    WTOU.setValeursSelection("**", "  ");
    DETART.setValeursSelection("OUI", "NON");
    WCOD2.setValeurs(WCOD2_Value, null);
    TDVDEB.setValeurs(TDVDEB_Value, null);
    TDVFIN.setValeurs(TDVFIN_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    PERDEB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERDEB@")).trim());
    PERFIN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERFIN@")).trim());
    WRPLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRPLIB@")).trim());
    REP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@REP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    REP.setVisible(lexique.isPresent("REP"));
    WETB.setEnabled(lexique.isPresent("WETB"));
    // NATMTT.setVisible( lexique.isTrue("N91"));
    OBJ_71.setVisible(lexique.isTrue("N91"));
    panel7.setVisible(lexique.isTrue("N91"));
    NATMTT.setEnabled(lexique.HostFieldGetData("WCUMUL").equalsIgnoreCase("NON"));
    OBJ_71.setVisible(lexique.isPresent("WTOU"));
    // WTOU.setVisible( lexique.isPresent("WTOU"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // DETART.setVisible( lexique.isPresent("DETART"));
    // DETART.setSelected(lexique.HostFieldGetData("DETART").equalsIgnoreCase("OUI"));
    // OPT3.setVisible( lexique.isPresent("OPT3"));
    // OPT3.setSelected(lexique.HostFieldGetData("OPT3").equalsIgnoreCase("X"));
    WRPLIB.setVisible(lexique.isPresent("WTOU"));
    // OPT2.setVisible( lexique.isPresent("OPT2"));
    // OPT2.setSelected(lexique.HostFieldGetData("OPT2").equalsIgnoreCase("X"));
    // EMARGE.setVisible( lexique.isPresent("EMARGE"));
    // EMARGE.setSelected(lexique.HostFieldGetData("EMARGE").equalsIgnoreCase("OUI"));
    // NOMCLF.setVisible( lexique.isPresent("NOMCLF"));
    // NOMCLF.setSelected(lexique.HostFieldGetData("NOMCLF").equalsIgnoreCase("OUI"));
    // LIGVAL.setVisible( lexique.isPresent("LIGVAL"));
    // LIGVAL.setSelected(lexique.HostFieldGetData("LIGVAL").equalsIgnoreCase("OUI"));
    // AGSTAT.setVisible( lexique.isPresent("AGSTAT"));
    // AGSTAT.setSelected(lexique.HostFieldGetData("AGSTAT").equalsIgnoreCase("OUI"));
    // WCOD2.setVisible( lexique.isPresent("WCOD2"));
    // OPTED1.setVisible( lexique.isPresent("OPTED1"));
    // OPTED1.setSelected(lexique.HostFieldGetData("OPTED1").equalsIgnoreCase("O"));
    // OPTED3.setVisible( lexique.isPresent("OPTED3"));
    // OPTED3.setSelected(lexique.HostFieldGetData("OPTED3").equalsIgnoreCase("O"));
    // OPTED2.setVisible( lexique.isPresent("OPTED2"));
    // OPTED2.setSelected(lexique.HostFieldGetData("OPTED2").equalsIgnoreCase("O"));
    // OPT1.setVisible( lexique.isPresent("OPT1"));
    // OPT1.setSelected(lexique.HostFieldGetData("OPT1").equalsIgnoreCase("X"));
    // TDVFIN.setSelectedIndex(getIndice("TDVFIN", TDVFIN_Value));
    // TDVDEB.setSelectedIndex(getIndice("TDVDEB", TDVDEB_Value));
    // TDVFIN.setVisible( lexique.isTrue("92"));
    // TDVDEB.setVisible( lexique.isTrue("92"));
    OBJ_67.setVisible(lexique.isTrue("92"));
    panel2.setVisible(lexique.isTrue("92"));
    P_SEL0.setVisible(!WTOU.isSelected());
    OBJ_68.setVisible(lexique.isPresent("WTOU"));
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (DETART.isSelected())
    // lexique.HostFieldPutData("DETART", 0, "OUI");
    // else
    // lexique.HostFieldPutData("DETART", 0, "NON");
    // if (OPT3.isSelected())
    // lexique.HostFieldPutData("OPT3", 0, "X");
    // else
    // lexique.HostFieldPutData("OPT3", 0, " ");
    // if (OPT2.isSelected())
    // lexique.HostFieldPutData("OPT2", 0, "X");
    // else
    // lexique.HostFieldPutData("OPT2", 0, " ");
    // if (EMARGE.isSelected())
    // lexique.HostFieldPutData("EMARGE", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EMARGE", 0, "NON");
    // if (NOMCLF.isSelected())
    // lexique.HostFieldPutData("NOMCLF", 0, "OUI");
    // else
    // lexique.HostFieldPutData("NOMCLF", 0, "NON");
    // if (LIGVAL.isSelected())
    // lexique.HostFieldPutData("LIGVAL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("LIGVAL", 0, "NON");
    // if (AGSTAT.isSelected())
    // lexique.HostFieldPutData("AGSTAT", 0, "OUI");
    // else
    // lexique.HostFieldPutData("AGSTAT", 0, "NON");
    // lexique.HostFieldPutData("WCOD2", 0, WCOD2_Value[WCOD2.getSelectedIndex()]);
    // if (OPTED1.isSelected())
    // lexique.HostFieldPutData("OPTED1", 0, "O");
    // else
    // lexique.HostFieldPutData("OPTED1", 0, "N");
    // if (OPTED3.isSelected())
    // lexique.HostFieldPutData("OPTED3", 0, "O");
    // else
    // lexique.HostFieldPutData("OPTED3", 0, "N");
    // if (OPTED2.isSelected())
    // lexique.HostFieldPutData("OPTED2", 0, "O");
    // else
    // lexique.HostFieldPutData("OPTED2", 0, "N");
    // if (OPT1.isSelected())
    // lexique.HostFieldPutData("OPT1", 0, "X");
    // else
    // lexique.HostFieldPutData("OPT1", 0, " ");
    // lexique.HostFieldPutData("TDVFIN", 0, TDVFIN_Value[TDVFIN.getSelectedIndex()]);
    // lexique.HostFieldPutData("TDVDEB", 0, TDVDEB_Value[TDVDEB.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_27 = new JXTitledSeparator();
    OBJ_31 = new RiZoneSortie();
    OBJ_33 = new RiZoneSortie();
    WETB = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_39 = new JXTitledSeparator();
    OBJ_47 = new JXTitledSeparator();
    OBJ_62 = new JXTitledSeparator();
    DETART = new XRiCheckBox();
    panel4 = new JPanel();
    OBJ_60 = new JLabel();
    PERDEB = new RiZoneSortie();
    OBJ_61 = new JLabel();
    PERFIN = new RiZoneSortie();
    panel5 = new JPanel();
    WTOU = new XRiCheckBox();
    P_SEL0 = new JPanel();
    WCOD2 = new XRiComboBox();
    WRPLIB = new RiZoneSortie();
    OBJ_71 = new JLabel();
    REP = new RiZoneSortie();
    OBJ_68 = new JXTitledSeparator();
    panel6 = new JPanel();
    panel3 = new JPanel();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    OPTED1 = new XRiCheckBox();
    OPTED2 = new XRiCheckBox();
    OPTED3 = new XRiCheckBox();
    panel1 = new JPanel();
    EMARGE = new XRiCheckBox();
    LIGVAL = new XRiCheckBox();
    AGSTAT = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_67 = new JLabel();
    TDVDEB = new XRiComboBox();
    TDVFIN = new XRiComboBox();
    panel7 = new JPanel();
    OBJ_65 = new JLabel();
    NATMTT = new XRiComboBox();
    NOMCLF = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label1 ----
          label1.setText("@LOCTP@");
          label1.setMinimumSize(new Dimension(120, 22));
          label1.setPreferredSize(new Dimension(120, 22));
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setHorizontalTextPosition(SwingConstants.RIGHT);
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 80));
            menus_haut.setPreferredSize(new Dimension(160, 80));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Exportation tableur");
              riSousMenu_bt6.setToolTipText("Exportation vers tableur");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_27 ----
          OBJ_27.setTitle("Etablissement s\u00e9lectionn\u00e9");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_31 ----
          OBJ_31.setText("@DGNOM@");
          OBJ_31.setName("OBJ_31");

          //---- OBJ_33 ----
          OBJ_33.setText("@WENCX@");
          OBJ_33.setName("OBJ_33");

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setText("@WETB@");
          WETB.setName("WETB");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_39 ----
          OBJ_39.setTitle("Options possibles");
          OBJ_39.setName("OBJ_39");

          //---- OBJ_47 ----
          OBJ_47.setTitle("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_62 ----
          OBJ_62.setTitle("");
          OBJ_62.setName("OBJ_62");

          //---- DETART ----
          DETART.setText("D\u00e9tails des lignes articles");
          DETART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DETART.setName("DETART");

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_60 ----
            OBJ_60.setText("Du");
            OBJ_60.setName("OBJ_60");
            panel4.add(OBJ_60);
            OBJ_60.setBounds(10, 15, 21, 18);

            //---- PERDEB ----
            PERDEB.setText("@PERDEB@");
            PERDEB.setName("PERDEB");
            panel4.add(PERDEB);
            PERDEB.setBounds(43, 12, 76, PERDEB.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("au");
            OBJ_61.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_61.setName("OBJ_61");
            panel4.add(OBJ_61);
            OBJ_61.setBounds(145, 15, 30, 18);

            //---- PERFIN ----
            PERFIN.setText("@PERFIN@");
            PERFIN.setName("PERFIN");
            panel4.add(PERFIN);
            PERFIN.setBounds(203, 12, 76, PERFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel5.add(WTOU);
            WTOU.setBounds(25, 53, 141, WTOU.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- WCOD2 ----
              WCOD2.setModel(new DefaultComboBoxModel(new String[] {
                "1er",
                "2\u00e9me"
              }));
              WCOD2.setComponentPopupMenu(BTD);
              WCOD2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WCOD2.setName("WCOD2");
              P_SEL0.add(WCOD2);
              WCOD2.setBounds(170, 14, 70, WCOD2.getPreferredSize().height);

              //---- WRPLIB ----
              WRPLIB.setText("@WRPLIB@");
              WRPLIB.setName("WRPLIB");
              P_SEL0.add(WRPLIB);
              WRPLIB.setBounds(15, 45, 225, WRPLIB.getPreferredSize().height);

              //---- OBJ_71 ----
              OBJ_71.setText("Code repr\u00e9sentant");
              OBJ_71.setName("OBJ_71");
              P_SEL0.add(OBJ_71);
              OBJ_71.setBounds(15, 18, 114, 18);

              //---- REP ----
              REP.setText("@REP@");
              REP.setName("REP");
              P_SEL0.add(REP);
              REP.setBounds(130, 15, 34, REP.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL0.setMinimumSize(preferredSize);
                P_SEL0.setPreferredSize(preferredSize);
              }
            }
            panel5.add(P_SEL0);
            P_SEL0.setBounds(175, 35, 255, 85);

            //---- OBJ_68 ----
            OBJ_68.setTitle("Repr\u00e9sentant");
            OBJ_68.setName("OBJ_68");
            panel5.add(OBJ_68);
            OBJ_68.setBounds(10, 5, 445, OBJ_68.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel6 ========
          {
            panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OPT1 ----
              OPT1.setText("Bons homologu\u00e9s non exp\u00e9di\u00e9s (HOM)");
              OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT1.setName("OPT1");
              panel3.add(OPT1);
              OPT1.setBounds(10, 10, 255, 18);

              //---- OPT2 ----
              OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
              OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT2.setName("OPT2");
              panel3.add(OPT2);
              OPT2.setBounds(10, 35, 255, 18);

              //---- OPT3 ----
              OPT3.setText("Bons factur\u00e9s (FAC)");
              OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT3.setName("OPT3");
              panel3.add(OPT3);
              OPT3.setBounds(10, 60, 255, 18);

              //---- OPTED1 ----
              OPTED1.setText("Edition de l'adresse compl\u00e8te du client");
              OPTED1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTED1.setName("OPTED1");
              panel3.add(OPTED1);
              OPTED1.setBounds(10, 10, 253, OPTED1.getPreferredSize().height);

              //---- OPTED2 ----
              OPTED2.setText("Edition du bloc-notes client");
              OPTED2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTED2.setName("OPTED2");
              panel3.add(OPTED2);
              OPTED2.setBounds(10, 35, 255, OPTED2.getPreferredSize().height);

              //---- OPTED3 ----
              OPTED3.setText("Saut de page par bon");
              OPTED3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTED3.setName("OPTED3");
              panel3.add(OPTED3);
              OPTED3.setBounds(10, 60, 255, OPTED3.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel6.add(panel3);
            panel3.setBounds(15, 10, 275, 90);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- EMARGE ----
              EMARGE.setText("Edition de la marge");
              EMARGE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EMARGE.setName("EMARGE");
              panel1.add(EMARGE);
              EMARGE.setBounds(15, 64, 250, EMARGE.getPreferredSize().height);

              //---- LIGVAL ----
              LIGVAL.setText("S\u00e9lection des lignes en valeur");
              LIGVAL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LIGVAL.setName("LIGVAL");
              panel1.add(LIGVAL);
              LIGVAL.setBounds(15, 37, 250, LIGVAL.getPreferredSize().height);

              //---- AGSTAT ----
              AGSTAT.setText("Edition des articles non g\u00e9r\u00e9s en stats");
              AGSTAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              AGSTAT.setName("AGSTAT");
              panel1.add(AGSTAT);
              AGSTAT.setBounds(15, 10, 250, AGSTAT.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel6.add(panel1);
            panel1.setBounds(10, 209, 275, 90);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_67 ----
              OBJ_67.setText("Etat des devis");
              OBJ_67.setName("OBJ_67");
              panel2.add(OBJ_67);
              OBJ_67.setBounds(20, 13, 88, 20);

              //---- TDVDEB ----
              TDVDEB.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "Attente",
                "Valid\u00e9",
                "Envoy\u00e9",
                "Sign\u00e9",
                "D\u00e9pass\u00e9",
                "Perdu",
                "Clotur\u00e9"
              }));
              TDVDEB.setComponentPopupMenu(BTD);
              TDVDEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TDVDEB.setName("TDVDEB");
              panel2.add(TDVDEB);
              TDVDEB.setBounds(115, 10, 95, TDVDEB.getPreferredSize().height);

              //---- TDVFIN ----
              TDVFIN.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "Attente",
                "Valid\u00e9",
                "Envoy\u00e9",
                "Sign\u00e9",
                "D\u00e9pass\u00e9",
                "Perdu",
                "Clotur\u00e9"
              }));
              TDVFIN.setComponentPopupMenu(BTD);
              TDVFIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TDVFIN.setName("TDVFIN");
              panel2.add(TDVFIN);
              TDVFIN.setBounds(215, 10, 95, TDVFIN.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel6.add(panel2);
            panel2.setBounds(10, 160, 315, 45);

            //======== panel7 ========
            {
              panel7.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);

              //---- OBJ_65 ----
              OBJ_65.setText("Nature du montant");
              OBJ_65.setName("OBJ_65");
              panel7.add(OBJ_65);
              OBJ_65.setBounds(15, 15, 111, OBJ_65.getPreferredSize().height);

              //---- NATMTT ----
              NATMTT.setModel(new DefaultComboBoxModel(new String[] {
                "HT ",
                "TTC"
              }));
              NATMTT.setComponentPopupMenu(BTD);
              NATMTT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              NATMTT.setName("NATMTT");
              panel7.add(NATMTT);
              NATMTT.setBounds(210, 10, 70, NATMTT.getPreferredSize().height);
            }
            panel6.add(panel7);
            panel7.setBounds(15, 105, 305, 45);

            //---- NOMCLF ----
            NOMCLF.setText("Edition du nom du client factur\u00e9");
            NOMCLF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NOMCLF.setName("NOMCLF");
            panel6.add(NOMCLF);
            NOMCLF.setBounds(25, 300, 250, NOMCLF.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 845, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(DETART, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(48, 48, 48)
                    .addComponent(DETART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(22, 22, 22)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_27;
  private RiZoneSortie OBJ_31;
  private RiZoneSortie OBJ_33;
  private RiZoneSortie WETB;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_47;
  private JXTitledSeparator OBJ_62;
  private XRiCheckBox DETART;
  private JPanel panel4;
  private JLabel OBJ_60;
  private RiZoneSortie PERDEB;
  private JLabel OBJ_61;
  private RiZoneSortie PERFIN;
  private JPanel panel5;
  private XRiCheckBox WTOU;
  private JPanel P_SEL0;
  private XRiComboBox WCOD2;
  private RiZoneSortie WRPLIB;
  private JLabel OBJ_71;
  private RiZoneSortie REP;
  private JXTitledSeparator OBJ_68;
  private JPanel panel6;
  private JPanel panel3;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private XRiCheckBox OPTED1;
  private XRiCheckBox OPTED2;
  private XRiCheckBox OPTED3;
  private JPanel panel1;
  private XRiCheckBox EMARGE;
  private XRiCheckBox LIGVAL;
  private XRiCheckBox AGSTAT;
  private JPanel panel2;
  private JLabel OBJ_67;
  private XRiComboBox TDVDEB;
  private XRiComboBox TDVFIN;
  private JPanel panel7;
  private JLabel OBJ_65;
  private XRiComboBox NATMTT;
  private XRiCheckBox NOMCLF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
