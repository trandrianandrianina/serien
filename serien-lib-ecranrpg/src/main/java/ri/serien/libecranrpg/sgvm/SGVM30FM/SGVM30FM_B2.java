
package ri.serien.libecranrpg.sgvm.SGVM30FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM542] Gestion des ventes -> Opérations périodiques -> Journal fiscal des ventes -> Edition sur période arrêtée
 * Indicateur : 00100001 (93)
 * Titre : Edition des journaux fiscaux de vente sur période arrêtée
 * 
 * [GVM552] Gestion des ventes -> Opérations périodiques -> Journal fiscal par sections -> Edition sur période arrêtée
 * Indicateur : 10100001 (91 et 93)
 * Titre : Edition des journaux fiscaux de vente sur période arrêtée par section
 */
public class SGVM30FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORTER = "Exporter";
  private Message LOCTP = null;
  
  public SGVM30FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    UETVA.setValeursSelection("1", " ");
    REPON2.setValeursSelection("OUI", "NON");
    REPON3.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    AVOIR.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfperiodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Indicateurs
    Boolean isParSection = lexique.isTrue("91 and 93");
    Boolean isMagasin = lexique.isTrue("31");
    Boolean isSectionAnalytique = lexique.isTrue("91");
    
    // Visibilité des composants
    lbMagasin.setVisible(isMagasin);
    snMagasin.setVisible(isMagasin);
    lbPeriodeFacture.setVisible(UPER1.isVisible());
    pnlMiseEnForme.setVisible(UPER1.isVisible());
    snSectionAnalytique.setVisible(isSectionAnalytique);
    lbSectionAnalytique.setVisible(isSectionAnalytique);
    
    // Titre
    if (isParSection) {
      bpPresentation.setText("Edition des journaux fiscaux de vente sur période arrêtée par section");
    }
    else {
      bpPresentation.setText("Edition des journaux fiscaux de vente sur période arrêtée");
    }
    
    // Titre panel etablissement
    if (isMagasin) {
      pnlEtablissement.setTitre("Etablissement et magasin");
    }
    else {
      pnlEtablissement.setTitre("Etablissement");
    }
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion de LOCTP
    pnlBandeau.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Initialisation de l'etablissmenet
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charge les composants
    chargerListeMagasin();
    chargerListeSectionAnalytique();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snMagasin.isVisible()) {
      snMagasin.renseignerChampRPG(lexique, "XPARG6");
    }
    if (snSectionAnalytique.isVisible()) {
      snSectionAnalytique.renseignerChampRPG(lexique, "SANA");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * initialise le composant magasin
   */
  private void chargerListeMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "XPARG6");
  }
  
  /**
   * Initialise le composants section analytique suivant l'etablissement
   */
  private void chargerListeSectionAnalytique() {
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "SANA");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanelContenu();
    pnlBandeau = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbSectionAnalytique = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    lbPeriodeFacture = new SNLabelChamp();
    pnlMiseEnForme = new SNPanel();
    UPER1 = new XRiTextField();
    lbAnnee = new SNLabelChamp();
    UPER2 = new XRiTextField();
    UETVA = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbEnCours = new SNLabelChamp();
    tfperiodeEnCours = new SNTexte();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlOption = new SNPanelTitre();
    REPON2 = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    REPON3 = new XRiCheckBox();
    AVOIR = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des journaux fiscaux de vente sur p\u00e9riode arr\u00eat\u00e9e");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlBandeau ========
      {
        pnlBandeau.setName("pnlBandeau");
        pnlBandeau.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlBandeau.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlBandeau.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlBandeau.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlBandeau.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlBandeau.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlBandeau, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setOpaque(false);
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbSectionAnalytique ----
            lbSectionAnalytique.setText("Section analytique");
            lbSectionAnalytique.setMinimumSize(new Dimension(200, 30));
            lbSectionAnalytique.setPreferredSize(new Dimension(200, 30));
            lbSectionAnalytique.setMaximumSize(new Dimension(200, 30));
            lbSectionAnalytique.setName("lbSectionAnalytique");
            pnlCritereSelection.add(lbSectionAnalytique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSectionAnalytique ----
            snSectionAnalytique.setName("snSectionAnalytique");
            pnlCritereSelection.add(snSectionAnalytique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeFacture ----
            lbPeriodeFacture.setText("P\u00e9riode factur\u00e9e \u00e0 \u00e9diter   Mois");
            lbPeriodeFacture.setPreferredSize(new Dimension(200, 30));
            lbPeriodeFacture.setMinimumSize(new Dimension(200, 30));
            lbPeriodeFacture.setMaximumSize(new Dimension(200, 30));
            lbPeriodeFacture.setName("lbPeriodeFacture");
            pnlCritereSelection.add(lbPeriodeFacture, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlMiseEnForme ========
            {
              pnlMiseEnForme.setName("pnlMiseEnForme");
              pnlMiseEnForme.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMiseEnForme.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlMiseEnForme.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMiseEnForme.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMiseEnForme.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- UPER1 ----
              UPER1.setToolTipText("Mois");
              UPER1.setMinimumSize(new Dimension(30, 30));
              UPER1.setPreferredSize(new Dimension(30, 30));
              UPER1.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER1.setName("UPER1");
              pnlMiseEnForme.add(UPER1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAnnee ----
              lbAnnee.setText("Ann\u00e9e");
              lbAnnee.setHorizontalAlignment(SwingConstants.CENTER);
              lbAnnee.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbAnnee.setPreferredSize(new Dimension(45, 30));
              lbAnnee.setMinimumSize(new Dimension(45, 30));
              lbAnnee.setName("lbAnnee");
              pnlMiseEnForme.add(lbAnnee, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- UPER2 ----
              UPER2.setToolTipText("Ann\u00e9e");
              UPER2.setMinimumSize(new Dimension(30, 30));
              UPER2.setPreferredSize(new Dimension(30, 30));
              UPER2.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER2.setName("UPER2");
              pnlMiseEnForme.add(UPER2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlMiseEnForme, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- UETVA ----
            UETVA.setText("Edition des ventes UE avec TVA");
            UETVA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UETVA.setPreferredSize(new Dimension(175, 30));
            UETVA.setMinimumSize(new Dimension(175, 30));
            UETVA.setFont(new Font("sansserif", Font.PLAIN, 14));
            UETVA.setName("UETVA");
            pnlCritereSelection.add(UETVA, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement et magasin");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEnCours ----
            lbEnCours.setText("P\u00e9riode en cours");
            lbEnCours.setName("lbEnCours");
            pnlEtablissement.add(lbEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfperiodeEnCours ----
            tfperiodeEnCours.setText("@WENCX@");
            tfperiodeEnCours.setEnabled(false);
            tfperiodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfperiodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfperiodeEnCours.setName("tfperiodeEnCours");
            pnlEtablissement.add(tfperiodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setMinimumSize(new Dimension(175, 30));
            lbMagasin.setPreferredSize(new Dimension(175, 30));
            lbMagasin.setName("lbMagasin");
            pnlEtablissement.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setEnabled(false);
            snMagasin.setName("snMagasin");
            pnlEtablissement.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOption ========
          {
            pnlOption.setTitre("Option d'\u00e9dition");
            pnlOption.setName("pnlOption");
            pnlOption.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOption.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOption.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOption.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOption.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- REPON2 ----
            REPON2.setText("Saut de page par journ\u00e9e");
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setPreferredSize(new Dimension(185, 30));
            REPON2.setMinimumSize(new Dimension(185, 30));
            REPON2.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON2.setName("REPON2");
            pnlOption.add(REPON2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON1 ----
            REPON1.setText("R\u00e9capitulatif seulement");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setPreferredSize(new Dimension(175, 30));
            REPON1.setMinimumSize(new Dimension(175, 30));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setName("REPON1");
            pnlOption.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON3 ----
            REPON3.setText("Edition avec interlignes");
            REPON3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON3.setPreferredSize(new Dimension(175, 30));
            REPON3.setMinimumSize(new Dimension(175, 30));
            REPON3.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON3.setMaximumSize(new Dimension(175, 30));
            REPON3.setName("REPON3");
            pnlOption.add(REPON3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- AVOIR ----
            AVOIR.setText("Edition des avoirs \u00e0 part");
            AVOIR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AVOIR.setMinimumSize(new Dimension(175, 30));
            AVOIR.setPreferredSize(new Dimension(175, 30));
            AVOIR.setFont(new Font("sansserif", Font.PLAIN, 14));
            AVOIR.setMaximumSize(new Dimension(175, 30));
            AVOIR.setName("AVOIR");
            pnlOption.add(AVOIR, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOption, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlPrincipal.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlBandeau;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbPeriodeFacture;
  private SNPanel pnlMiseEnForme;
  private XRiTextField UPER1;
  private SNLabelChamp lbAnnee;
  private XRiTextField UPER2;
  private XRiCheckBox UETVA;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEnCours;
  private SNTexte tfperiodeEnCours;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlOption;
  private XRiCheckBox REPON2;
  private XRiCheckBox REPON1;
  private XRiCheckBox REPON3;
  private XRiCheckBox AVOIR;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
