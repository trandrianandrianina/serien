
package ri.serien.libecranrpg.sgvm.SGVMQ2FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMQ2FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMQ2FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU3.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBKVD@")).trim());
    OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBKVF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    
    
    
    // WTOU3.setEnabled( lexique.isPresent("WTOU3"));
    // WTOU3.setSelected(lexique.HostFieldGetData("WTOU3").equalsIgnoreCase("**"));
    P_SEL0.setVisible((!WTOU3.isSelected()) & (lexique.isPresent("WTOU3")));
    
    MA01_ck.setVisible(lexique.isPresent("MA01"));
    MA01_ck.setEnabled(lexique.isPresent("MA01"));
    MA01_ck.setSelected(lexique.HostFieldGetData("MA01").equalsIgnoreCase("**"));
    panel6.setVisible((!MA01_ck.isSelected()) & (lexique.isPresent("MA01")));
    
    // WTOU2.setEnabled( lexique.isPresent("WTOU2"));
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    P_SEL2.setVisible((!WTOU2.isSelected()) & (lexique.isPresent("WTOU2")));
    
    // WTOU1.setEnabled( lexique.isPresent("WTOU1"));
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    P_SEL1.setVisible((!WTOU1.isSelected()) & (lexique.isPresent("WTOU1")));
    
    panel1.setVisible(lexique.isPresent("BLODEB"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU3.isSelected())
    // lexique.HostFieldPutData("WTOU3", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU3", 0, " ");
    if (MA01_ck.isSelected()) {
      lexique.HostFieldPutData("MA01", 0, "**");
      // else
      // lexique.HostFieldPutData("MA01", 0, " ");
      // if (WTOU2.isSelected())
      // lexique.HostFieldPutData("WTOU2", 0, "**");
      // else
      // lexique.HostFieldPutData("WTOU2", 0, " ");
      // if (WTOU1.isSelected())
      // lexique.HostFieldPutData("WTOU1", 0, "**");
      // else
      // lexique.HostFieldPutData("WTOU1", 0, " ");
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOU3ActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void MA01ActionPerformed(ActionEvent e) {
    panel6.setVisible(!panel6.isVisible());
    if (!MA01_ck.isSelected()) {
      MA01.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_21 = new JXTitledSeparator();
    OBJ_25 = new JXTitledSeparator();
    OBJ_29 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    panel1 = new JPanel();
    OBJ_28 = new JXTitledSeparator();
    OBJ_38 = new JLabel();
    BLODEB = new XRiCalendrier();
    OBJ_39 = new JLabel();
    BLOFIN = new XRiCalendrier();
    panel2 = new JPanel();
    OBJ_35 = new JXTitledSeparator();
    OBJ_37 = new JLabel();
    WANNEE = new XRiTextField();
    OBJ_36 = new JLabel();
    WPER = new XRiTextField();
    panel3 = new JPanel();
    WTOU2 = new XRiCheckBox();
    P_SEL2 = new JPanel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_40 = new JLabel();
    panel4 = new JPanel();
    WTOU1 = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_74 = new JLabel();
    OBJ_75 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    panel5 = new JPanel();
    P_SEL0 = new JPanel();
    OBJ_71 = new RiZoneSortie();
    OBJ_72 = new RiZoneSortie();
    OBJ_23 = new JLabel();
    OBJ_70 = new JLabel();
    KVDEB = new XRiTextField();
    KVFIN = new XRiTextField();
    WTOU3 = new XRiCheckBox();
    panel7 = new JPanel();
    MA01_ck = new JCheckBox();
    panel6 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(790, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 30, 720, 19);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //---- OBJ_21 ----
          OBJ_21.setTitle("Canal de vente");
          OBJ_21.setName("OBJ_21");
          p_contenu.add(OBJ_21);
          OBJ_21.setBounds(35, 195, 720, OBJ_21.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setTitle("Plages de codes groupe-famille");
          OBJ_25.setName("OBJ_25");
          p_contenu.add(OBJ_25);
          OBJ_25.setBounds(35, 290, 720, OBJ_25.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setTitle("Plages de codes articles");
          OBJ_29.setName("OBJ_29");
          p_contenu.add(OBJ_29);
          OBJ_29.setBounds(35, 385, 720, OBJ_29.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setTitle("Code(s) magasin(s) \u00e0 traiter");
          OBJ_43.setName("OBJ_43");
          p_contenu.add(OBJ_43);
          OBJ_43.setBounds(35, 115, 720, OBJ_43.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_28 ----
            OBJ_28.setTitle("Plage de date de bloquage");
            OBJ_28.setName("OBJ_28");
            panel1.add(OBJ_28);
            OBJ_28.setBounds(5, 5, 365, OBJ_28.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("Du");
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(15, 34, 21, 20);

            //---- BLODEB ----
            BLODEB.setName("BLODEB");
            panel1.add(BLODEB);
            BLODEB.setBounds(60, 30, 105, BLODEB.getPreferredSize().height);

            //---- OBJ_39 ----
            OBJ_39.setText("au");
            OBJ_39.setName("OBJ_39");
            panel1.add(OBJ_39);
            OBJ_39.setBounds(195, 34, 18, 20);

            //---- BLOFIN ----
            BLOFIN.setName("BLOFIN");
            panel1.add(BLOFIN);
            BLOFIN.setBounds(235, 30, 105, BLOFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(385, 485, 380, 70);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_35 ----
            OBJ_35.setTitle("Edition au");
            OBJ_35.setName("OBJ_35");
            panel2.add(OBJ_35);
            OBJ_35.setBounds(5, 5, 320, OBJ_35.getPreferredSize().height);

            //---- OBJ_37 ----
            OBJ_37.setText("Ann\u00e9e de fin");
            OBJ_37.setName("OBJ_37");
            panel2.add(OBJ_37);
            OBJ_37.setBounds(20, 34, 77, 20);

            //---- WANNEE ----
            WANNEE.setComponentPopupMenu(BTD);
            WANNEE.setName("WANNEE");
            panel2.add(WANNEE);
            WANNEE.setBounds(115, 30, 44, WANNEE.getPreferredSize().height);

            //---- OBJ_36 ----
            OBJ_36.setText("N\u00b0 p\u00e9riode (RQ)");
            OBJ_36.setName("OBJ_36");
            panel2.add(OBJ_36);
            OBJ_36.setBounds(165, 34, 94, 20);

            //---- WPER ----
            WPER.setComponentPopupMenu(BTD);
            WPER.setName("WPER");
            panel2.add(WPER);
            WPER.setBounds(280, 30, 28, WPER.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(30, 485, 340, 85);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WTOU2 ----
            WTOU2.setText("S\u00e9lection compl\u00e8te");
            WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU2.setName("WTOU2");
            WTOU2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU2ActionPerformed(e);
              }
            });
            panel3.add(WTOU2);
            WTOU2.setBounds(5, 23, 151, 20);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- ARTDEB ----
              ARTDEB.setComponentPopupMenu(BTD);
              ARTDEB.setName("ARTDEB");
              P_SEL2.add(ARTDEB);
              ARTDEB.setBounds(160, 6, 214, ARTDEB.getPreferredSize().height);

              //---- ARTFIN ----
              ARTFIN.setComponentPopupMenu(BTD);
              ARTFIN.setName("ARTFIN");
              P_SEL2.add(ARTFIN);
              ARTFIN.setBounds(160, 32, 214, ARTFIN.getPreferredSize().height);

              //---- OBJ_42 ----
              OBJ_42.setText("Article de d\u00e9but");
              OBJ_42.setName("OBJ_42");
              P_SEL2.add(OBJ_42);
              OBJ_42.setBounds(15, 12, 96, 16);

              //---- OBJ_40 ----
              OBJ_40.setText("Article de fin");
              OBJ_40.setName("OBJ_40");
              P_SEL2.add(OBJ_40);
              OBJ_40.setBounds(15, 38, 75, 16);
            }
            panel3.add(P_SEL2);
            P_SEL2.setBounds(160, 5, 530, 65);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(45, 405, 700, 75);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- WTOU1 ----
            WTOU1.setText("S\u00e9lection compl\u00e8te");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });
            panel4.add(WTOU1);
            WTOU1.setBounds(5, 25, 151, 20);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_74 ----
              OBJ_74.setText("Groupe-famille de d\u00e9but");
              OBJ_74.setName("OBJ_74");
              P_SEL1.add(OBJ_74);
              OBJ_74.setBounds(15, 11, 140, 16);

              //---- OBJ_75 ----
              OBJ_75.setText("Groupe-famille de fin");
              OBJ_75.setName("OBJ_75");
              P_SEL1.add(OBJ_75);
              OBJ_75.setBounds(15, 39, 132, 16);

              //---- FAMDEB ----
              FAMDEB.setComponentPopupMenu(BTD);
              FAMDEB.setName("FAMDEB");
              P_SEL1.add(FAMDEB);
              FAMDEB.setBounds(160, 5, 44, FAMDEB.getPreferredSize().height);

              //---- FAMFIN ----
              FAMFIN.setComponentPopupMenu(BTD);
              FAMFIN.setName("FAMFIN");
              P_SEL1.add(FAMFIN);
              FAMFIN.setBounds(160, 33, 44, FAMFIN.getPreferredSize().height);
            }
            panel4.add(P_SEL1);
            P_SEL1.setBounds(160, 5, 530, 65);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(45, 305, 700, 75);

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_71 ----
              OBJ_71.setText("@LIBKVD@");
              OBJ_71.setName("OBJ_71");
              P_SEL0.add(OBJ_71);
              OBJ_71.setBounds(210, 6, 310, OBJ_71.getPreferredSize().height);

              //---- OBJ_72 ----
              OBJ_72.setText("@LIBKVF@");
              OBJ_72.setName("OBJ_72");
              P_SEL0.add(OBJ_72);
              OBJ_72.setBounds(210, 35, 310, OBJ_72.getPreferredSize().height);

              //---- OBJ_23 ----
              OBJ_23.setText("Canal de vente d\u00e9but");
              OBJ_23.setName("OBJ_23");
              P_SEL0.add(OBJ_23);
              OBJ_23.setBounds(15, 10, 125, 16);

              //---- OBJ_70 ----
              OBJ_70.setText("Canal de vente fin");
              OBJ_70.setName("OBJ_70");
              P_SEL0.add(OBJ_70);
              OBJ_70.setBounds(15, 39, 109, 16);

              //---- KVDEB ----
              KVDEB.setComponentPopupMenu(BTD);
              KVDEB.setName("KVDEB");
              P_SEL0.add(KVDEB);
              KVDEB.setBounds(160, 4, 44, KVDEB.getPreferredSize().height);

              //---- KVFIN ----
              KVFIN.setComponentPopupMenu(BTD);
              KVFIN.setName("KVFIN");
              P_SEL0.add(KVFIN);
              KVFIN.setBounds(160, 33, 44, KVFIN.getPreferredSize().height);
            }
            panel5.add(P_SEL0);
            P_SEL0.setBounds(160, 5, 530, 65);

            //---- WTOU3 ----
            WTOU3.setText("S\u00e9lection compl\u00e8te");
            WTOU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU3.setName("WTOU3");
            WTOU3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU3ActionPerformed(e);
              }
            });
            panel5.add(WTOU3);
            WTOU3.setBounds(5, 25, 146, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel5);
          panel5.setBounds(45, 210, 700, 75);

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- MA01_ck ----
            MA01_ck.setText("S\u00e9lection compl\u00e8te");
            MA01_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MA01_ck.setName("MA01_ck");
            MA01_ck.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                MA01ActionPerformed(e);
              }
            });
            panel7.add(MA01_ck);
            MA01_ck.setBounds(5, 20, 134, 20);

            //======== panel6 ========
            {
              panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              panel6.add(MA01);
              MA01.setBounds(15, 10, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              panel6.add(MA02);
              MA02.setBounds(57, 10, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              panel6.add(MA03);
              MA03.setBounds(99, 10, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              panel6.add(MA04);
              MA04.setBounds(141, 10, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              panel6.add(MA05);
              MA05.setBounds(183, 10, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              panel6.add(MA06);
              MA06.setBounds(225, 10, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              panel6.add(MA07);
              MA07.setBounds(267, 10, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              panel6.add(MA08);
              MA08.setBounds(309, 10, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              panel6.add(MA09);
              MA09.setBounds(351, 10, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              panel6.add(MA10);
              MA10.setBounds(393, 10, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              panel6.add(MA11);
              MA11.setBounds(435, 10, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              panel6.add(MA12);
              MA12.setBounds(477, 10, 34, MA12.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }
            panel7.add(panel6);
            panel6.setBounds(160, 5, 530, 50);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel7);
          panel7.setBounds(45, 130, 700, 60);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_29;
  private JXTitledSeparator OBJ_43;
  private JPanel panel1;
  private JXTitledSeparator OBJ_28;
  private JLabel OBJ_38;
  private XRiCalendrier BLODEB;
  private JLabel OBJ_39;
  private XRiCalendrier BLOFIN;
  private JPanel panel2;
  private JXTitledSeparator OBJ_35;
  private JLabel OBJ_37;
  private XRiTextField WANNEE;
  private JLabel OBJ_36;
  private XRiTextField WPER;
  private JPanel panel3;
  private XRiCheckBox WTOU2;
  private JPanel P_SEL2;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_42;
  private JLabel OBJ_40;
  private JPanel panel4;
  private XRiCheckBox WTOU1;
  private JPanel P_SEL1;
  private JLabel OBJ_74;
  private JLabel OBJ_75;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JPanel panel5;
  private JPanel P_SEL0;
  private RiZoneSortie OBJ_71;
  private RiZoneSortie OBJ_72;
  private JLabel OBJ_23;
  private JLabel OBJ_70;
  private XRiTextField KVDEB;
  private XRiTextField KVFIN;
  private XRiCheckBox WTOU3;
  private JPanel panel7;
  private JCheckBox MA01_ck;
  private JPanel panel6;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
