
package ri.serien.libecranrpg.sgvm.SGVMA6FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMA6FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMA6FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    OPT6.setValeurs("6", "RB");
    OPT5.setValeurs("5", "RB");
    OPT4.setValeurs("4", "RB");
    OPT3.setValeurs("3", "RB");
    OPT2.setValeurs("2", "RB");
    OPT1.setValeurs("1", "RB");
    EXBUY.setValeursSelection("O", "N");
    EDT12.setValeursSelection("O", "N");
    SPTAR.setValeursSelection("O", "N");
    SPFAM.setValeursSelection("O", "N");
    ARTHIS.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    EDT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDIT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    
    
    OBJ_54.setVisible(lexique.isTrue("N93"));
    OBJ_53.setVisible(lexique.isTrue("93"));
    RACNV.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("5") & lexique.isPresent("RACNV"));
    OBJ_59.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    OBJ_55.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    OBJ_50.setVisible(lexique.isTrue("33"));
    OBJ_49.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    OBJ_51.setVisible(lexique.HostFieldGetData("MOTCLA").equalsIgnoreCase("Mot Classement 2") & lexique.isPresent("MCLA"));
    OBJ_51.setVisible(lexique.isTrue("34"));
    OBJ_48.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    OBJ_67.setVisible(!lexique.isTrue("93"));
    OBJ_68.setVisible(lexique.isTrue("93"));
    
    if (lexique.isPresent("EDT12")) {
      EDT12.setText("Edit° format 12 pouces");
    }
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@&&G_TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_530 = new JXTitledSeparator();
    OBJ_52 = new JXTitledSeparator();
    OPT1 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    OPT4 = new XRiRadioButton();
    OPT5 = new XRiRadioButton();
    OPT6 = new XRiRadioButton();
    OBJ_53 = new JLabel();
    OBJ_54 = new JLabel();
    CNV = new XRiTextField();
    NUMCLI = new XRiTextField();
    OBJ_630 = new JLabel();
    OPTABC = new XRiComboBox();
    OBJ_63 = new JLabel();
    OPTAB1 = new XRiComboBox();
    ARTHIS = new XRiCheckBox();
    OBJ_50 = new JXTitledSeparator();
    OBJ_48 = new JXTitledSeparator();
    OBJ_49 = new JXTitledSeparator();
    OBJ_51 = new JXTitledSeparator();
    MCLA = new XRiTextField();
    RACNV = new XRiTextField();
    OBJ_55 = new JLabel();
    OBJ_59 = new JLabel();
    FAM02 = new XRiTextField();
    FAM01 = new XRiTextField();
    OBJ_62 = new JXTitledSeparator();
    OBJ_64 = new JLabel();
    REFTAR = new XRiTextField();
    OBJ_68 = new JLabel();
    NPRIX = new XRiTextField();
    OBJ_71 = new JLabel();
    CODDEV = new XRiTextField();
    OBJ_73 = new JLabel();
    SPFAM = new XRiCheckBox();
    EDT12 = new XRiCheckBox();
    EXBUY = new XRiCheckBox();
    SPTAR = new XRiCheckBox();
    OBJ_67 = new JLabel();
    ZDATE = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_530 ----
          OBJ_530.setTitle("Options possibles dans un tarif");
          OBJ_530.setName("OBJ_530");

          //---- OBJ_52 ----
          OBJ_52.setTitle("");
          OBJ_52.setName("OBJ_52");

          //---- OPT1 ----
          OPT1.setText("Tous les articles");
          OPT1.setComponentPopupMenu(BTD);
          OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT1.setName("OPT1");

          //---- OPT2 ----
          OPT2.setText("Articles d'une famille");
          OPT2.setComponentPopupMenu(BTD);
          OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT2.setName("OPT2");

          //---- OPT3 ----
          OPT3.setText("Articles sur mot de classement 1");
          OPT3.setComponentPopupMenu(BTD);
          OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT3.setName("OPT3");

          //---- OPT4 ----
          OPT4.setText("Articles de m\u00eame mot classement 2");
          OPT4.setComponentPopupMenu(BTD);
          OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT4.setName("OPT4");

          //---- OPT5 ----
          OPT5.setText("Articles de m\u00eame rattachement CNV");
          OPT5.setComponentPopupMenu(BTD);
          OPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT5.setName("OPT5");

          //---- OPT6 ----
          OPT6.setText("Articles de la CNV");
          OPT6.setComponentPopupMenu(BTD);
          OPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT6.setName("OPT6");

          //---- OBJ_53 ----
          OBJ_53.setText("Code condition de vente \"CN\"");
          OBJ_53.setName("OBJ_53");

          //---- OBJ_54 ----
          OBJ_54.setText("Num\u00e9ro client \u00e0 traiter");
          OBJ_54.setName("OBJ_54");

          //---- CNV ----
          CNV.setComponentPopupMenu(BTD);
          CNV.setName("CNV");

          //---- NUMCLI ----
          NUMCLI.setComponentPopupMenu(BTD);
          NUMCLI.setName("NUMCLI");

          //---- OBJ_630 ----
          OBJ_630.setText("Filtre sur code ABC de l'article");
          OBJ_630.setName("OBJ_630");

          //---- OPTABC ----
          OPTABC.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTABC.setComponentPopupMenu(BTD);
          OPTABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTABC.setName("OPTABC");

          //---- OBJ_63 ----
          OBJ_63.setText("\u00e0");
          OBJ_63.setName("OBJ_63");

          //---- OPTAB1 ----
          OPTAB1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTAB1.setComponentPopupMenu(BTD);
          OPTAB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTAB1.setName("OPTAB1");

          //---- ARTHIS ----
          ARTHIS.setText("Seulement Articles ayant d\u00e9j\u00e0 tourn\u00e9s");
          ARTHIS.setComponentPopupMenu(BTD);
          ARTHIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARTHIS.setName("ARTHIS");

          //---- OBJ_50 ----
          OBJ_50.setTitle("Mot de classement 1");
          OBJ_50.setName("OBJ_50");

          //---- OBJ_48 ----
          OBJ_48.setTitle("Rattachement CNV (facultatif)");
          OBJ_48.setName("OBJ_48");

          //---- OBJ_49 ----
          OBJ_49.setTitle("Plage de famille \u00e0 traiter");
          OBJ_49.setName("OBJ_49");

          //---- OBJ_51 ----
          OBJ_51.setTitle("Mot de classement 2");
          OBJ_51.setName("OBJ_51");

          //---- MCLA ----
          MCLA.setComponentPopupMenu(BTD);
          MCLA.setName("MCLA");

          //---- RACNV ----
          RACNV.setComponentPopupMenu(BTD);
          RACNV.setName("RACNV");

          //---- OBJ_55 ----
          OBJ_55.setText("Famille d\u00e9but");
          OBJ_55.setName("OBJ_55");

          //---- OBJ_59 ----
          OBJ_59.setText("Famille fin");
          OBJ_59.setName("OBJ_59");

          //---- FAM02 ----
          FAM02.setComponentPopupMenu(BTD);
          FAM02.setName("FAM02");

          //---- FAM01 ----
          FAM01.setComponentPopupMenu(BTD);
          FAM01.setName("FAM01");

          //---- OBJ_62 ----
          OBJ_62.setTitle("Options suppl\u00e9mentaires");
          OBJ_62.setName("OBJ_62");

          //---- OBJ_64 ----
          OBJ_64.setText("R\u00e9f\u00e9rence tarif \u00e0 \u00e9diter  (ou **)");
          OBJ_64.setName("OBJ_64");

          //---- REFTAR ----
          REFTAR.setComponentPopupMenu(BTD);
          REFTAR.setName("REFTAR");

          //---- OBJ_68 ----
          OBJ_68.setText("Num\u00e9ro tarif  \u00e0 \u00e9diter (01 \u00e0 10)");
          OBJ_68.setName("OBJ_68");

          //---- NPRIX ----
          NPRIX.setComponentPopupMenu(BTD);
          NPRIX.setName("NPRIX");

          //---- OBJ_71 ----
          OBJ_71.setText("Date de validit\u00e9 des prix");
          OBJ_71.setName("OBJ_71");

          //---- CODDEV ----
          CODDEV.setComponentPopupMenu(BTD);
          CODDEV.setName("CODDEV");

          //---- OBJ_73 ----
          OBJ_73.setText("Code devise \u00e0 traiter");
          OBJ_73.setName("OBJ_73");

          //---- SPFAM ----
          SPFAM.setText("Saut de page par Famille");
          SPFAM.setComponentPopupMenu(BTD);
          SPFAM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SPFAM.setName("SPFAM");

          //---- EDT12 ----
          EDT12.setText("@EDIT@");
          EDT12.setComponentPopupMenu(BTD);
          EDT12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDT12.setName("EDT12");

          //---- EXBUY ----
          EXBUY.setText("Sortie Excel BUYING PACK");
          EXBUY.setComponentPopupMenu(BTD);
          EXBUY.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EXBUY.setName("EXBUY");

          //---- SPTAR ----
          SPTAR.setText("Saut de page par Tarif");
          SPTAR.setComponentPopupMenu(BTD);
          SPTAR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SPTAR.setName("SPTAR");

          //---- OBJ_67 ----
          OBJ_67.setText("Num\u00e9ro tarif  (01 \u00e0 10)");
          OBJ_67.setName("OBJ_67");

          //---- ZDATE ----
          ZDATE.setName("ZDATE");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(CNV, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NUMCLI, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(110, 110, 110)
                .addComponent(OBJ_630, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OPTABC, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OPTAB1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(479, 479, 479)
                .addComponent(ARTHIS, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_530, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OPT1, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OPT2, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OPT3, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OPT4, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
                .addGap(181, 181, 181)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(REFTAR, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(ZDATE, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CODDEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NPRIX, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OPT5, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
                .addGap(181, 181, 181)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(SPTAR, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SPFAM, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OPT6, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
                .addGap(181, 181, 181)
                .addComponent(EDT12, GroupLayout.PREFERRED_SIZE, 219, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(479, 479, 479)
                .addComponent(EXBUY, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(FAM01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(85, 85, 85)
                    .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(RACNV, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(170, 170, 170)
                    .addComponent(FAM02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(MCLA, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                  .addComponent(CNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NUMCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_630, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(OPTABC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(OPTAB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(ARTHIS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_530, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OPT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OPT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OPT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OPT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(REFTAR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(22, 22, 22)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(ZDATE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(CODDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(NPRIX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OPT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(SPTAR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(SPFAM, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OPT6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(EDT12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(EXBUY, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(FAM01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(RACNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(FAM02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MCLA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(OPT1);
    RB_GRP.add(OPT2);
    RB_GRP.add(OPT3);
    RB_GRP.add(OPT4);
    RB_GRP.add(OPT5);
    RB_GRP.add(OPT6);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_530;
  private JXTitledSeparator OBJ_52;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT3;
  private XRiRadioButton OPT4;
  private XRiRadioButton OPT5;
  private XRiRadioButton OPT6;
  private JLabel OBJ_53;
  private JLabel OBJ_54;
  private XRiTextField CNV;
  private XRiTextField NUMCLI;
  private JLabel OBJ_630;
  private XRiComboBox OPTABC;
  private JLabel OBJ_63;
  private XRiComboBox OPTAB1;
  private XRiCheckBox ARTHIS;
  private JXTitledSeparator OBJ_50;
  private JXTitledSeparator OBJ_48;
  private JXTitledSeparator OBJ_49;
  private JXTitledSeparator OBJ_51;
  private XRiTextField MCLA;
  private XRiTextField RACNV;
  private JLabel OBJ_55;
  private JLabel OBJ_59;
  private XRiTextField FAM02;
  private XRiTextField FAM01;
  private JXTitledSeparator OBJ_62;
  private JLabel OBJ_64;
  private XRiTextField REFTAR;
  private JLabel OBJ_68;
  private XRiTextField NPRIX;
  private JLabel OBJ_71;
  private XRiTextField CODDEV;
  private JLabel OBJ_73;
  private XRiCheckBox SPFAM;
  private XRiCheckBox EDT12;
  private XRiCheckBox EXBUY;
  private XRiCheckBox SPTAR;
  private JLabel OBJ_67;
  private XRiCalendrier ZDATE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
