
package ri.serien.libecranrpg.sgvm.SGVM44FM;
// Nom Fichier: pop_SGVM44FM_FMTP5_FMTF1_740.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM44FM_P5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM44FM_P5(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    P_PnlOpts = new JPanel();
    BT_ENTER = new JButton();
    panel1 = new JPanel();
    OBJ_4 = new JLabel();
    OBJ_6 = new JLabel();
    OBJ_5 = new JButton();

    //======== this ========
    setPreferredSize(new Dimension(270, 117));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel2 ========
    {
      panel2.setPreferredSize(new Dimension(340, 48));
      panel2.setName("panel2");
      panel2.setLayout(null);

      //======== P_PnlOpts ========
      {
        P_PnlOpts.setName("P_PnlOpts");
        P_PnlOpts.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("ok");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_7ActionPerformed(e);
          }
        });
        P_PnlOpts.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);
      }
      panel2.add(P_PnlOpts);
      P_PnlOpts.setBounds(205, 0, 65, 48);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel2.getComponentCount(); i++) {
          Rectangle bounds = panel2.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel2.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel2.setMinimumSize(preferredSize);
        panel2.setPreferredSize(preferredSize);
      }
    }
    add(panel2, BorderLayout.SOUTH);

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_4 ----
      OBJ_4.setText("Traitement sans objet.");
      OBJ_4.setName("OBJ_4");
      panel1.add(OBJ_4);
      OBJ_4.setBounds(65, 10, 154, 20);

      //---- OBJ_6 ----
      OBJ_6.setText("Option non pr\u00e9vue dans \"PS\"");
      OBJ_6.setName("OBJ_6");
      panel1.add(OBJ_6);
      OBJ_6.setBounds(50, 40, 177, 20);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    add(panel1, BorderLayout.CENTER);

    //---- OBJ_5 ----
    OBJ_5.setText("");
    OBJ_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_5.setName("OBJ_5");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel2;
  private JPanel P_PnlOpts;
  private JButton BT_ENTER;
  private JPanel panel1;
  private JLabel OBJ_4;
  private JLabel OBJ_6;
  private JButton OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
