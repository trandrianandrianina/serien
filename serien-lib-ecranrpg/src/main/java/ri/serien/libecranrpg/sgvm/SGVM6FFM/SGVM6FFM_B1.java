
package ri.serien.libecranrpg.sgvm.SGVM6FFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM6FFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM6FFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EEDT2.setValeurs("2", "RB3");
    EEDT1.setValeurs("1", "RB3");
    ESTA2.setValeurs("2", "RB2");
    ESTA1.setValeurs("1", "RB2");
    EOPT2.setValeurs("2", "RB1");
    EOPT1.setValeurs("1", "RB1");
    WTOU.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // ESTA2.setSelected(lexique.HostFieldGetData("RB2").equalsIgnoreCase("2"));
    // ESTA1.setSelected(lexique.HostFieldGetData("RB2").equalsIgnoreCase("1"));
    OBJ_21.setVisible(lexique.isPresent("EOPT1"));
    // EOPT2.setSelected(lexique.HostFieldGetData("RB1").equalsIgnoreCase("2"));
    // EOPT1.setSelected(lexique.HostFieldGetData("RB1").equalsIgnoreCase("1"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTOU.isSelected() & WTOU.isVisible());
    // EEDT2.setSelected(lexique.HostFieldGetData("RB3").equalsIgnoreCase("2"));
    // EEDT1.setSelected(lexique.HostFieldGetData("RB3").equalsIgnoreCase("1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    
    

    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (EEDT2.isSelected())
    // lexique.HostFieldPutData("RB3", 0, "2");
    // if (EEDT1.isSelected())
    // lexique.HostFieldPutData("RB3", 0, "1");
    // if (ESTA2.isSelected())
    // lexique.HostFieldPutData("RB2", 0, "2");
    // if (ESTA1.isSelected())
    // lexique.HostFieldPutData("RB2", 0, "1");
    // if (EOPT2.isSelected())
    // lexique.HostFieldPutData("RB1", 0, "2");
    // if (EOPT1.isSelected())
    // lexique.HostFieldPutData("RB1", 0, "1");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_29 = new JXTitledSeparator();
    OBJ_39 = new JXTitledSeparator();
    OBJ_30 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    EDEBF = new XRiTextField();
    EFINF = new XRiTextField();
    EOPT1 = new XRiRadioButton();
    EOPT2 = new XRiRadioButton();
    ESTA1 = new XRiRadioButton();
    ESTA2 = new XRiRadioButton();
    EEDT1 = new XRiRadioButton();
    EEDT2 = new XRiRadioButton();
    REPON1 = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(525, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_19 ----
          OBJ_19.setTitle("Plage familles \u00e0 traiter");
          OBJ_19.setName("OBJ_19");

          //---- OBJ_21 ----
          OBJ_21.setTitle("Option d\u00e9sir\u00e9e");
          OBJ_21.setName("OBJ_21");

          //---- OBJ_29 ----
          OBJ_29.setTitle("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_29.setName("OBJ_29");

          //---- OBJ_39 ----
          OBJ_39.setTitle("Type d'\u00e9tat");
          OBJ_39.setName("OBJ_39");

          //---- OBJ_30 ----
          OBJ_30.setTitle("");
          OBJ_30.setName("OBJ_30");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_27 ----
            OBJ_27.setText("Code famille de d\u00e9but");
            OBJ_27.setName("OBJ_27");
            P_SEL0.add(OBJ_27);
            OBJ_27.setBounds(15, 17, 130, 18);

            //---- OBJ_28 ----
            OBJ_28.setText("Code famille de fin");
            OBJ_28.setName("OBJ_28");
            P_SEL0.add(OBJ_28);
            OBJ_28.setBounds(15, 45, 117, 18);

            //---- EDEBF ----
            EDEBF.setComponentPopupMenu(BTD);
            EDEBF.setName("EDEBF");
            P_SEL0.add(EDEBF);
            EDEBF.setBounds(198, 12, 44, EDEBF.getPreferredSize().height);

            //---- EFINF ----
            EFINF.setComponentPopupMenu(BTD);
            EFINF.setName("EFINF");
            P_SEL0.add(EFINF);
            EFINF.setBounds(198, 40, 44, EFINF.getPreferredSize().height);
          }

          //---- EOPT1 ----
          EOPT1.setText("Statistiques sur le command\u00e9");
          EOPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EOPT1.setName("EOPT1");

          //---- EOPT2 ----
          EOPT2.setText("Statistiques sur le factur\u00e9");
          EOPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EOPT2.setName("EOPT2");

          //---- ESTA1 ----
          ESTA1.setText("Statistiques mois en cours");
          ESTA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESTA1.setName("ESTA1");

          //---- ESTA2 ----
          ESTA2.setText("Statistiques mois suivant");
          ESTA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESTA2.setName("ESTA2");

          //---- EEDT1 ----
          EEDT1.setText("Multi-lignes avec marges");
          EEDT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EEDT1.setName("EEDT1");

          //---- EEDT2 ----
          EEDT2.setText("Mono-ligne avec remises");
          EEDT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EEDT2.setName("EEDT2");

          //---- REPON1 ----
          REPON1.setText("D\u00e9tail des familles");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ESTA1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(EOPT1, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ESTA2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(EOPT2, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 268, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(EEDT1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(EEDT2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(ESTA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EOPT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(ESTA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EOPT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(EEDT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(EEDT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- RB1_GRP ----
    ButtonGroup RB1_GRP = new ButtonGroup();
    RB1_GRP.add(EOPT1);
    RB1_GRP.add(EOPT2);

    //---- RB2_GRP ----
    ButtonGroup RB2_GRP = new ButtonGroup();
    RB2_GRP.add(ESTA1);
    RB2_GRP.add(ESTA2);

    //---- RB3_GRP ----
    ButtonGroup RB3_GRP = new ButtonGroup();
    RB3_GRP.add(EEDT1);
    RB3_GRP.add(EEDT2);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_29;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_30;
  private JPanel P_SEL0;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private XRiTextField EDEBF;
  private XRiTextField EFINF;
  private XRiRadioButton EOPT1;
  private XRiRadioButton EOPT2;
  private XRiRadioButton ESTA1;
  private XRiRadioButton ESTA2;
  private XRiRadioButton EEDT1;
  private XRiRadioButton EEDT2;
  private XRiCheckBox REPON1;
  private XRiCheckBox WTOU;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
