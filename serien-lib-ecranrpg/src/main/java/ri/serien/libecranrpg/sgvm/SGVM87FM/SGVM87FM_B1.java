/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM87FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.EnumCategorieDate;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.affaire.snaffaire.SNAffaire;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2B2] Gestion des ventes -> Documents de ventes -> Traitements commandes -> Préparation des expéditions
 */
public class SGVM87FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private ArrayList<EnumCategorieDate> listeEnumCategorieDate = new ArrayList<EnumCategorieDate>();
  
  public SGVM87FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    // Lier les composants catégorie clients
    snCategorieClientDebut.lierComposantFin(snCategorieClientFin);
    
    // Ajouter la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Chargement de la liste des valeurs de l'EnumDatePreEnregister
    for (int i = 0; i < EnumCategorieDate.values().length; i++) {
      listeEnumCategorieDate.add(EnumCategorieDate.values()[i]);
    }
    
    for (EnumCategorieDate categorieTri : listeEnumCategorieDate) {
      cbCategorieDate.addItem(categorieTri);
    }
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    lbWLIB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB1@")).trim());
    lbWLIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB2@")).trim());
    lbWLIB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB3@")).trim());
    lbWLIB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB4@")).trim());
    lbWLIB5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB5@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++
    // Visibilité des composants
    lbAuWLIB5.setVisible(lexique.isPresent("WLIB5"));
    lbDuWLIB5.setVisible(lexique.isPresent("WLIB5"));
    lbAuWLIB4.setVisible(lexique.isPresent("WLIB4"));
    lbDuWLIB4.setVisible(lexique.isPresent("WLIB4"));
    lbAuWLIB3.setVisible(lexique.isPresent("WLIB3"));
    lbDuWLIB3.setVisible(lexique.isPresent("WLIB3"));
    lbAuWLIB2.setVisible(lexique.isPresent("WLIB2"));
    lbDuWLIB2.setVisible(lexique.isPresent("WLIB2"));
    lbDuWLIB1.setVisible(lexique.isPresent("WLIB1"));
    lbAuWLIB1.setVisible(lexique.isPresent("WLIB1"));
    WZP5F.setVisible(lexique.isPresent("WZP5F"));
    WZP5D.setVisible(lexique.isPresent("WZP5D"));
    WZP4F.setVisible(lexique.isPresent("WZP4F"));
    WZP4D.setVisible(lexique.isPresent("WZP4D"));
    WZP3F.setVisible(lexique.isPresent("WZP3F"));
    WZP3D.setVisible(lexique.isPresent("WZP3D"));
    WZP2F.setVisible(lexique.isPresent("WZP2F"));
    WZP2D.setVisible(lexique.isPresent("WZP2D"));
    WZP1F.setVisible(lexique.isPresent("WZP1F"));
    WZP1D.setVisible(lexique.isPresent("WZP1D"));
    lbWLIB5.setVisible(lexique.isPresent("WLIB5"));
    lbWLIB4.setVisible(lexique.isPresent("WLIB4"));
    lbWLIB3.setVisible(lexique.isPresent("WLIB3"));
    lbWLIB2.setVisible(lexique.isPresent("WLIB2"));
    lbWLIB1.setVisible(lexique.isPresent("WLIB1"));
    pnlZonePersonalise
        .setVisible(lbWLIB1.isVisible() || lbWLIB2.isVisible() || lbWLIB3.isVisible() || lbWLIB4.isVisible() || lbWLIB5.isVisible());
    lbSelectionZonePersonnalise.setVisible(pnlZonePersonalise.isVisible());
    
    // Disponibilité des composants
    snAffaire.setEnabled(lexique.isPresent("WACT"));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charger l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charger le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    
    // Charger le CategorieClient
    snCategorieClientDebut.setSession(getSession());
    snCategorieClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientDebut.setTousAutorise(true);
    snCategorieClientDebut.charger(true);
    snCategorieClientDebut.renseignerChampRPG(lexique, "WCATD");
    
    snCategorieClientFin.setSession(getSession());
    snCategorieClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientFin.setTousAutorise(true);
    snCategorieClientFin.charger(true);
    snCategorieClientFin.renseignerChampRPG(lexique, "WCATF");
    
    // Charger les affaires
    snAffaire.setSession(getSession());
    snAffaire.setIdEtablissement(snEtablissement.getIdSelection());
    snAffaire.setTousAutorise(true);
    snAffaire.charger(true);
    snAffaire.renseignerChampRPG(lexique, "WACT");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snCategorieClientDebut.renseignerChampRPG(lexique, "WCATD");
    snCategorieClientFin.renseignerChampRPG(lexique, "WCATF");
    snAffaire.renseignerChampRPG(lexique, "WACT");
    // Sélection de la date de livraison
    lexique.HostFieldPutData("WTDL", 0, ((EnumCategorieDate) cbCategorieDate.getSelectedItem()).getCode());
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbDateLivraisonDe = new SNLabelChamp();
    pnlDateLivraison = new SNPanel();
    WDATD = new XRiCalendrier();
    lbDateLivraisonAu = new SNLabelChamp();
    WDATF = new XRiCalendrier();
    lbCategorieDateLivraison = new SNLabelChamp();
    cbCategorieDate = new SNComboBox();
    lbSelectionDeBon = new SNLabelChamp();
    pnlSelectionSurBon = new SNPanel();
    WCODD = new XRiTextField();
    lbA = new SNLabelChamp();
    WCODF = new XRiTextField();
    lbNumeroCommandeDebut = new SNLabelChamp();
    pnlCommandeATraiter = new SNPanel();
    WNUMD = new XRiTextField();
    lbNumeroCommandeFin = new SNLabelChamp();
    WNUMF = new XRiTextField();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbAffaire = new SNLabelChamp();
    snAffaire = new SNAffaire();
    lbCategorieClientDeDebut = new SNLabelChamp();
    snCategorieClientDebut = new SNCategorieClient();
    lbCategorieClientDeFin = new SNLabelChamp();
    snCategorieClientFin = new SNCategorieClient();
    lbSelectionZonePersonnalise = new SNLabelChamp();
    pnlZonePersonalise = new JPanel();
    lbWLIB1 = new SNLabelChamp();
    lbDuWLIB1 = new SNLabelChamp();
    WZP1D = new XRiTextField();
    lbAuWLIB1 = new SNLabelChamp();
    WZP1F = new XRiTextField();
    WZP2F = new XRiTextField();
    lbAuWLIB2 = new SNLabelChamp();
    WZP2D = new XRiTextField();
    lbDuWLIB2 = new SNLabelChamp();
    lbWLIB2 = new SNLabelChamp();
    lbWLIB3 = new SNLabelChamp();
    lbDuWLIB3 = new SNLabelChamp();
    WZP3D = new XRiTextField();
    lbAuWLIB3 = new SNLabelChamp();
    WZP3F = new XRiTextField();
    WZP4F = new XRiTextField();
    lbAuWLIB4 = new SNLabelChamp();
    WZP4D = new XRiTextField();
    lbDuWLIB4 = new SNLabelChamp();
    lbWLIB4 = new SNLabelChamp();
    lbWLIB5 = new SNLabelChamp();
    lbDuWLIB5 = new SNLabelChamp();
    WZP5D = new XRiTextField();
    lbAuWLIB5 = new SNLabelChamp();
    WZP5F = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(970, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0 };
            
            // ---- lbDateLivraisonDe ----
            lbDateLivraisonDe.setText("Date de livraison du");
            lbDateLivraisonDe.setPreferredSize(new Dimension(170, 30));
            lbDateLivraisonDe.setMinimumSize(new Dimension(170, 30));
            lbDateLivraisonDe.setMaximumSize(new Dimension(170, 30));
            lbDateLivraisonDe.setName("lbDateLivraisonDe");
            pnlCritereDeSelection.add(lbDateLivraisonDe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateLivraison ========
            {
              pnlDateLivraison.setName("pnlDateLivraison");
              pnlDateLivraison.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDATD ----
              WDATD.setPreferredSize(new Dimension(110, 30));
              WDATD.setMinimumSize(new Dimension(110, 30));
              WDATD.setMaximumSize(new Dimension(110, 30));
              WDATD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATD.setName("WDATD");
              pnlDateLivraison.add(WDATD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDateLivraisonAu ----
              lbDateLivraisonAu.setText("au");
              lbDateLivraisonAu.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbDateLivraisonAu.setPreferredSize(new Dimension(16, 30));
              lbDateLivraisonAu.setMinimumSize(new Dimension(16, 30));
              lbDateLivraisonAu.setMaximumSize(new Dimension(16, 30));
              lbDateLivraisonAu.setName("lbDateLivraisonAu");
              pnlDateLivraison.add(lbDateLivraisonAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDATF ----
              WDATF.setPreferredSize(new Dimension(110, 30));
              WDATF.setMinimumSize(new Dimension(110, 30));
              WDATF.setMaximumSize(new Dimension(110, 30));
              WDATF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATF.setName("WDATF");
              pnlDateLivraison.add(WDATF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlDateLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieDateLivraison ----
            lbCategorieDateLivraison.setText("Cat\u00e9gorie date de livraison");
            lbCategorieDateLivraison.setMinimumSize(new Dimension(170, 30));
            lbCategorieDateLivraison.setMaximumSize(new Dimension(170, 30));
            lbCategorieDateLivraison.setPreferredSize(new Dimension(170, 30));
            lbCategorieDateLivraison.setName("lbCategorieDateLivraison");
            pnlCritereDeSelection.add(lbCategorieDateLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbCategorieDate ----
            cbCategorieDate.setName("cbCategorieDate");
            pnlCritereDeSelection.add(cbCategorieDate, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSelectionDeBon ----
            lbSelectionDeBon.setText("S\u00e9lection de");
            lbSelectionDeBon.setPreferredSize(new Dimension(170, 30));
            lbSelectionDeBon.setMinimumSize(new Dimension(170, 30));
            lbSelectionDeBon.setMaximumSize(new Dimension(170, 30));
            lbSelectionDeBon.setName("lbSelectionDeBon");
            pnlCritereDeSelection.add(lbSelectionDeBon, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlSelectionSurBon ========
            {
              pnlSelectionSurBon.setName("pnlSelectionSurBon");
              pnlSelectionSurBon.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSelectionSurBon.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlSelectionSurBon.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionSurBon.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSelectionSurBon.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WCODD ----
              WCODD.setComponentPopupMenu(null);
              WCODD.setPreferredSize(new Dimension(24, 30));
              WCODD.setMinimumSize(new Dimension(24, 30));
              WCODD.setMaximumSize(new Dimension(24, 30));
              WCODD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WCODD.setName("WCODD");
              pnlSelectionSurBon.add(WCODD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbA ----
              lbA.setText("\u00e0");
              lbA.setPreferredSize(new Dimension(8, 30));
              lbA.setMaximumSize(new Dimension(8, 30));
              lbA.setMinimumSize(new Dimension(8, 30));
              lbA.setName("lbA");
              pnlSelectionSurBon.add(lbA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WCODF ----
              WCODF.setComponentPopupMenu(null);
              WCODF.setMinimumSize(new Dimension(24, 30));
              WCODF.setPreferredSize(new Dimension(24, 30));
              WCODF.setMaximumSize(new Dimension(24, 30));
              WCODF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WCODF.setName("WCODF");
              pnlSelectionSurBon.add(WCODF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlSelectionSurBon, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNumeroCommandeDebut ----
            lbNumeroCommandeDebut.setText("Commande \u00e0 traiter de");
            lbNumeroCommandeDebut.setPreferredSize(new Dimension(170, 30));
            lbNumeroCommandeDebut.setMinimumSize(new Dimension(170, 30));
            lbNumeroCommandeDebut.setMaximumSize(new Dimension(170, 30));
            lbNumeroCommandeDebut.setName("lbNumeroCommandeDebut");
            pnlCritereDeSelection.add(lbNumeroCommandeDebut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlCommandeATraiter ========
            {
              pnlCommandeATraiter.setName("pnlCommandeATraiter");
              pnlCommandeATraiter.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WNUMD ----
              WNUMD.setComponentPopupMenu(null);
              WNUMD.setMinimumSize(new Dimension(70, 30));
              WNUMD.setPreferredSize(new Dimension(70, 30));
              WNUMD.setMaximumSize(new Dimension(70, 30));
              WNUMD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNUMD.setName("WNUMD");
              pnlCommandeATraiter.add(WNUMD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbNumeroCommandeFin ----
              lbNumeroCommandeFin.setText("\u00e0");
              lbNumeroCommandeFin.setMinimumSize(new Dimension(8, 30));
              lbNumeroCommandeFin.setMaximumSize(new Dimension(8, 30));
              lbNumeroCommandeFin.setPreferredSize(new Dimension(8, 30));
              lbNumeroCommandeFin.setName("lbNumeroCommandeFin");
              pnlCommandeATraiter.add(lbNumeroCommandeFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WNUMF ----
              WNUMF.setComponentPopupMenu(null);
              WNUMF.setMinimumSize(new Dimension(70, 30));
              WNUMF.setMaximumSize(new Dimension(70, 30));
              WNUMF.setPreferredSize(new Dimension(70, 30));
              WNUMF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNUMF.setName("WNUMF");
              pnlCommandeATraiter.add(WNUMF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlCommandeATraiter, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setPreferredSize(new Dimension(170, 30));
            lbMagasin.setMinimumSize(new Dimension(170, 30));
            lbMagasin.setMaximumSize(new Dimension(170, 30));
            lbMagasin.setName("lbMagasin");
            pnlCritereDeSelection.add(lbMagasin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAffaire ----
            lbAffaire.setText("Affaire");
            lbAffaire.setPreferredSize(new Dimension(170, 30));
            lbAffaire.setMinimumSize(new Dimension(170, 30));
            lbAffaire.setMaximumSize(new Dimension(170, 30));
            lbAffaire.setName("lbAffaire");
            pnlCritereDeSelection.add(lbAffaire, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snAffaire ----
            snAffaire.setName("snAffaire");
            pnlCritereDeSelection.add(snAffaire, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieClientDeDebut ----
            lbCategorieClientDeDebut.setText("Cat\u00e9gorie client de d\u00e9but");
            lbCategorieClientDeDebut.setPreferredSize(new Dimension(170, 30));
            lbCategorieClientDeDebut.setMinimumSize(new Dimension(170, 30));
            lbCategorieClientDeDebut.setMaximumSize(new Dimension(170, 30));
            lbCategorieClientDeDebut.setName("lbCategorieClientDeDebut");
            pnlCritereDeSelection.add(lbCategorieClientDeDebut, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCategorieClientDebut ----
            snCategorieClientDebut.setName("snCategorieClientDebut");
            pnlCritereDeSelection.add(snCategorieClientDebut, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieClientDeFin ----
            lbCategorieClientDeFin.setText("Cat\u00e9gorie client de fin");
            lbCategorieClientDeFin.setPreferredSize(new Dimension(170, 30));
            lbCategorieClientDeFin.setMinimumSize(new Dimension(170, 30));
            lbCategorieClientDeFin.setMaximumSize(new Dimension(170, 30));
            lbCategorieClientDeFin.setName("lbCategorieClientDeFin");
            pnlCritereDeSelection.add(lbCategorieClientDeFin, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCategorieClientFin ----
            snCategorieClientFin.setName("snCategorieClientFin");
            pnlCritereDeSelection.add(snCategorieClientFin, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSelectionZonePersonnalise ----
            lbSelectionZonePersonnalise.setText("S\u00e9lection sur zones personnalis\u00e9es");
            lbSelectionZonePersonnalise.setPreferredSize(new Dimension(175, 30));
            lbSelectionZonePersonnalise.setMinimumSize(new Dimension(175, 30));
            lbSelectionZonePersonnalise.setMaximumSize(new Dimension(175, 30));
            lbSelectionZonePersonnalise.setHorizontalAlignment(SwingConstants.LEFT);
            lbSelectionZonePersonnalise.setName("lbSelectionZonePersonnalise");
            pnlCritereDeSelection.add(lbSelectionZonePersonnalise, new GridBagConstraints(0, 8, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlZonePersonalise ========
            {
              pnlZonePersonalise.setOpaque(false);
              pnlZonePersonalise.setName("pnlZonePersonalise");
              pnlZonePersonalise.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlZonePersonalise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlZonePersonalise.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlZonePersonalise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlZonePersonalise.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbWLIB1 ----
              lbWLIB1.setText("@WLIB1@");
              lbWLIB1.setPreferredSize(new Dimension(284, 30));
              lbWLIB1.setMinimumSize(new Dimension(284, 30));
              lbWLIB1.setMaximumSize(new Dimension(284, 30));
              lbWLIB1.setName("lbWLIB1");
              pnlZonePersonalise.add(lbWLIB1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbDuWLIB1 ----
              lbDuWLIB1.setText("du");
              lbDuWLIB1.setPreferredSize(new Dimension(20, 30));
              lbDuWLIB1.setMinimumSize(new Dimension(20, 30));
              lbDuWLIB1.setMaximumSize(new Dimension(20, 30));
              lbDuWLIB1.setName("lbDuWLIB1");
              pnlZonePersonalise.add(lbDuWLIB1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- WZP1D ----
              WZP1D.setComponentPopupMenu(null);
              WZP1D.setPreferredSize(new Dimension(36, 30));
              WZP1D.setMinimumSize(new Dimension(36, 30));
              WZP1D.setMaximumSize(new Dimension(36, 30));
              WZP1D.setName("WZP1D");
              pnlZonePersonalise.add(WZP1D, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbAuWLIB1 ----
              lbAuWLIB1.setText("au");
              lbAuWLIB1.setHorizontalAlignment(SwingConstants.CENTER);
              lbAuWLIB1.setHorizontalTextPosition(SwingConstants.CENTER);
              lbAuWLIB1.setPreferredSize(new Dimension(20, 30));
              lbAuWLIB1.setMinimumSize(new Dimension(20, 30));
              lbAuWLIB1.setMaximumSize(new Dimension(20, 30));
              lbAuWLIB1.setName("lbAuWLIB1");
              pnlZonePersonalise.add(lbAuWLIB1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- WZP1F ----
              WZP1F.setComponentPopupMenu(null);
              WZP1F.setPreferredSize(new Dimension(36, 30));
              WZP1F.setMinimumSize(new Dimension(36, 30));
              WZP1F.setMaximumSize(new Dimension(36, 30));
              WZP1F.setName("WZP1F");
              pnlZonePersonalise.add(WZP1F, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- WZP2F ----
              WZP2F.setComponentPopupMenu(null);
              WZP2F.setPreferredSize(new Dimension(36, 30));
              WZP2F.setMinimumSize(new Dimension(36, 30));
              WZP2F.setMaximumSize(new Dimension(36, 30));
              WZP2F.setName("WZP2F");
              pnlZonePersonalise.add(WZP2F, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbAuWLIB2 ----
              lbAuWLIB2.setText("au");
              lbAuWLIB2.setHorizontalTextPosition(SwingConstants.CENTER);
              lbAuWLIB2.setHorizontalAlignment(SwingConstants.CENTER);
              lbAuWLIB2.setPreferredSize(new Dimension(20, 30));
              lbAuWLIB2.setMinimumSize(new Dimension(20, 30));
              lbAuWLIB2.setMaximumSize(new Dimension(20, 30));
              lbAuWLIB2.setName("lbAuWLIB2");
              pnlZonePersonalise.add(lbAuWLIB2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- WZP2D ----
              WZP2D.setComponentPopupMenu(null);
              WZP2D.setPreferredSize(new Dimension(36, 30));
              WZP2D.setMinimumSize(new Dimension(36, 30));
              WZP2D.setMaximumSize(new Dimension(36, 30));
              WZP2D.setName("WZP2D");
              pnlZonePersonalise.add(WZP2D, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbDuWLIB2 ----
              lbDuWLIB2.setText("du");
              lbDuWLIB2.setPreferredSize(new Dimension(20, 30));
              lbDuWLIB2.setMinimumSize(new Dimension(20, 30));
              lbDuWLIB2.setMaximumSize(new Dimension(20, 30));
              lbDuWLIB2.setName("lbDuWLIB2");
              pnlZonePersonalise.add(lbDuWLIB2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbWLIB2 ----
              lbWLIB2.setText("@WLIB2@");
              lbWLIB2.setPreferredSize(new Dimension(284, 30));
              lbWLIB2.setMinimumSize(new Dimension(284, 30));
              lbWLIB2.setMaximumSize(new Dimension(284, 30));
              lbWLIB2.setName("lbWLIB2");
              pnlZonePersonalise.add(lbWLIB2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbWLIB3 ----
              lbWLIB3.setText("@WLIB3@");
              lbWLIB3.setPreferredSize(new Dimension(284, 30));
              lbWLIB3.setMinimumSize(new Dimension(284, 30));
              lbWLIB3.setMaximumSize(new Dimension(284, 30));
              lbWLIB3.setName("lbWLIB3");
              pnlZonePersonalise.add(lbWLIB3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbDuWLIB3 ----
              lbDuWLIB3.setText("du");
              lbDuWLIB3.setPreferredSize(new Dimension(20, 30));
              lbDuWLIB3.setMinimumSize(new Dimension(20, 30));
              lbDuWLIB3.setMaximumSize(new Dimension(20, 30));
              lbDuWLIB3.setName("lbDuWLIB3");
              pnlZonePersonalise.add(lbDuWLIB3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- WZP3D ----
              WZP3D.setComponentPopupMenu(null);
              WZP3D.setPreferredSize(new Dimension(36, 30));
              WZP3D.setMinimumSize(new Dimension(36, 30));
              WZP3D.setMaximumSize(new Dimension(36, 30));
              WZP3D.setName("WZP3D");
              pnlZonePersonalise.add(WZP3D, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbAuWLIB3 ----
              lbAuWLIB3.setText("au");
              lbAuWLIB3.setHorizontalTextPosition(SwingConstants.CENTER);
              lbAuWLIB3.setHorizontalAlignment(SwingConstants.CENTER);
              lbAuWLIB3.setPreferredSize(new Dimension(20, 30));
              lbAuWLIB3.setMinimumSize(new Dimension(20, 30));
              lbAuWLIB3.setMaximumSize(new Dimension(20, 30));
              lbAuWLIB3.setName("lbAuWLIB3");
              pnlZonePersonalise.add(lbAuWLIB3, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- WZP3F ----
              WZP3F.setComponentPopupMenu(null);
              WZP3F.setPreferredSize(new Dimension(36, 30));
              WZP3F.setMinimumSize(new Dimension(36, 30));
              WZP3F.setMaximumSize(new Dimension(36, 30));
              WZP3F.setName("WZP3F");
              pnlZonePersonalise.add(WZP3F, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- WZP4F ----
              WZP4F.setComponentPopupMenu(null);
              WZP4F.setPreferredSize(new Dimension(36, 30));
              WZP4F.setMinimumSize(new Dimension(36, 30));
              WZP4F.setMaximumSize(new Dimension(36, 30));
              WZP4F.setName("WZP4F");
              pnlZonePersonalise.add(WZP4F, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbAuWLIB4 ----
              lbAuWLIB4.setText("au");
              lbAuWLIB4.setHorizontalTextPosition(SwingConstants.CENTER);
              lbAuWLIB4.setHorizontalAlignment(SwingConstants.CENTER);
              lbAuWLIB4.setPreferredSize(new Dimension(20, 30));
              lbAuWLIB4.setMinimumSize(new Dimension(20, 30));
              lbAuWLIB4.setMaximumSize(new Dimension(20, 30));
              lbAuWLIB4.setName("lbAuWLIB4");
              pnlZonePersonalise.add(lbAuWLIB4, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- WZP4D ----
              WZP4D.setComponentPopupMenu(null);
              WZP4D.setPreferredSize(new Dimension(36, 30));
              WZP4D.setMinimumSize(new Dimension(36, 30));
              WZP4D.setMaximumSize(new Dimension(36, 30));
              WZP4D.setName("WZP4D");
              pnlZonePersonalise.add(WZP4D, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbDuWLIB4 ----
              lbDuWLIB4.setText("du");
              lbDuWLIB4.setPreferredSize(new Dimension(20, 30));
              lbDuWLIB4.setMinimumSize(new Dimension(20, 30));
              lbDuWLIB4.setMaximumSize(new Dimension(20, 30));
              lbDuWLIB4.setName("lbDuWLIB4");
              pnlZonePersonalise.add(lbDuWLIB4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbWLIB4 ----
              lbWLIB4.setText("@WLIB4@");
              lbWLIB4.setPreferredSize(new Dimension(284, 30));
              lbWLIB4.setMinimumSize(new Dimension(284, 30));
              lbWLIB4.setMaximumSize(new Dimension(284, 30));
              lbWLIB4.setName("lbWLIB4");
              pnlZonePersonalise.add(lbWLIB4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbWLIB5 ----
              lbWLIB5.setText("@WLIB5@");
              lbWLIB5.setPreferredSize(new Dimension(284, 30));
              lbWLIB5.setMinimumSize(new Dimension(284, 30));
              lbWLIB5.setMaximumSize(new Dimension(284, 30));
              lbWLIB5.setName("lbWLIB5");
              pnlZonePersonalise.add(lbWLIB5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDuWLIB5 ----
              lbDuWLIB5.setText("du");
              lbDuWLIB5.setPreferredSize(new Dimension(20, 30));
              lbDuWLIB5.setMinimumSize(new Dimension(20, 30));
              lbDuWLIB5.setMaximumSize(new Dimension(20, 30));
              lbDuWLIB5.setName("lbDuWLIB5");
              pnlZonePersonalise.add(lbDuWLIB5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WZP5D ----
              WZP5D.setComponentPopupMenu(null);
              WZP5D.setPreferredSize(new Dimension(36, 30));
              WZP5D.setMinimumSize(new Dimension(36, 30));
              WZP5D.setMaximumSize(new Dimension(36, 30));
              WZP5D.setName("WZP5D");
              pnlZonePersonalise.add(WZP5D, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAuWLIB5 ----
              lbAuWLIB5.setText("au");
              lbAuWLIB5.setHorizontalTextPosition(SwingConstants.CENTER);
              lbAuWLIB5.setHorizontalAlignment(SwingConstants.CENTER);
              lbAuWLIB5.setPreferredSize(new Dimension(20, 30));
              lbAuWLIB5.setMinimumSize(new Dimension(20, 30));
              lbAuWLIB5.setMaximumSize(new Dimension(20, 30));
              lbAuWLIB5.setName("lbAuWLIB5");
              pnlZonePersonalise.add(lbAuWLIB5, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WZP5F ----
              WZP5F.setComponentPopupMenu(null);
              WZP5F.setPreferredSize(new Dimension(36, 30));
              WZP5F.setMinimumSize(new Dimension(36, 30));
              WZP5F.setMaximumSize(new Dimension(36, 30));
              WZP5F.setName("WZP5F");
              pnlZonePersonalise.add(WZP5F, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlZonePersonalise, new GridBagConstraints(0, 9, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setEnabled(false);
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbDateLivraisonDe;
  private SNPanel pnlDateLivraison;
  private XRiCalendrier WDATD;
  private SNLabelChamp lbDateLivraisonAu;
  private XRiCalendrier WDATF;
  private SNLabelChamp lbCategorieDateLivraison;
  private SNComboBox cbCategorieDate;
  private SNLabelChamp lbSelectionDeBon;
  private SNPanel pnlSelectionSurBon;
  private XRiTextField WCODD;
  private SNLabelChamp lbA;
  private XRiTextField WCODF;
  private SNLabelChamp lbNumeroCommandeDebut;
  private SNPanel pnlCommandeATraiter;
  private XRiTextField WNUMD;
  private SNLabelChamp lbNumeroCommandeFin;
  private XRiTextField WNUMF;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbAffaire;
  private SNAffaire snAffaire;
  private SNLabelChamp lbCategorieClientDeDebut;
  private SNCategorieClient snCategorieClientDebut;
  private SNLabelChamp lbCategorieClientDeFin;
  private SNCategorieClient snCategorieClientFin;
  private SNLabelChamp lbSelectionZonePersonnalise;
  private JPanel pnlZonePersonalise;
  private SNLabelChamp lbWLIB1;
  private SNLabelChamp lbDuWLIB1;
  private XRiTextField WZP1D;
  private SNLabelChamp lbAuWLIB1;
  private XRiTextField WZP1F;
  private XRiTextField WZP2F;
  private SNLabelChamp lbAuWLIB2;
  private XRiTextField WZP2D;
  private SNLabelChamp lbDuWLIB2;
  private SNLabelChamp lbWLIB2;
  private SNLabelChamp lbWLIB3;
  private SNLabelChamp lbDuWLIB3;
  private XRiTextField WZP3D;
  private SNLabelChamp lbAuWLIB3;
  private XRiTextField WZP3F;
  private XRiTextField WZP4F;
  private SNLabelChamp lbAuWLIB4;
  private XRiTextField WZP4D;
  private SNLabelChamp lbDuWLIB4;
  private SNLabelChamp lbWLIB4;
  private SNLabelChamp lbWLIB5;
  private SNLabelChamp lbDuWLIB5;
  private XRiTextField WZP5D;
  private SNLabelChamp lbAuWLIB5;
  private XRiTextField WZP5F;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
