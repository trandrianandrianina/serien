
package ri.serien.libecranrpg.sgvm.SGVM9BFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM9BFM_07 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM9BFM_07(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    NAT5.setValeurs("5", "R2");
    NAT4.setValeurs("4", "R2");
    NAT3.setValeurs("3", "R2");
    NAT2.setValeurs("2", "R2");
    NAT1.setValeurs("1", "R2");
    WTOU3.setValeursSelection("**", "  ");
    SAUT2.setValeursSelection("O", "N");
    WTOU2.setValeursSelection("**", "  ");
    SAUT1.setValeursSelection("O", "N");
    WTOU1.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // WTOU3.setSelected(lexique.HostFieldGetData("WTOU3").equalsIgnoreCase("**"));
    // SAUT2.setSelected(lexique.HostFieldGetData("SAUT2").equalsIgnoreCase("O"));
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    // SAUT1.setSelected(lexique.HostFieldGetData("SAUT1").equalsIgnoreCase("O"));
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    // NAT4.setSelected(lexique.HostFieldGetData("R2").equalsIgnoreCase("4"));
    // NAT3.setSelected(lexique.HostFieldGetData("R2").equalsIgnoreCase("3"));
    // NAT2.setSelected(lexique.HostFieldGetData("R2").equalsIgnoreCase("2"));
    // NAT1.setSelected(lexique.HostFieldGetData("R2").equalsIgnoreCase("1"));
    // NAT5.setSelected(lexique.HostFieldGetData("R2").equalsIgnoreCase("5"));
    
    panel3.setVisible(!WTOU1.isSelected());
    panel2.setVisible(!WTOU2.isSelected());
    panel1.setVisible(!WTOU3.isSelected());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU3.isSelected())
    // lexique.HostFieldPutData("WTOU3", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU3", 0, " ");
    // if (SAUT2.isSelected())
    // lexique.HostFieldPutData("SAUT2", 0, "O");
    // else
    // lexique.HostFieldPutData("SAUT2", 0, "N");
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU2", 0, " ");
    // if (SAUT1.isSelected())
    // lexique.HostFieldPutData("SAUT1", 0, "O");
    // else
    // lexique.HostFieldPutData("SAUT1", 0, "N");
    // if (WTOU1.isSelected())
    // lexique.HostFieldPutData("WTOU1", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU1", 0, " ");
    // if (NAT4.isSelected())
    // lexique.HostFieldPutData("R2", 0, "4");
    // if (NAT3.isSelected())
    // lexique.HostFieldPutData("R2", 0, "3");
    // if (NAT2.isSelected())
    // lexique.HostFieldPutData("R2", 0, "2");
    // if (NAT1.isSelected())
    // lexique.HostFieldPutData("R2", 0, "1");
    // if (NAT5.isSelected())
    // lexique.HostFieldPutData("R2", 0, "5");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    panel3.setVisible(!panel3.isVisible());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    panel2.setVisible(!panel2.isVisible());
  }
  
  private void WTOU3ActionPerformed(ActionEvent e) {
    panel1.setVisible(!panel1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_42 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    OBJ_46 = new JXTitledSeparator();
    OBJ_52 = new JXTitledSeparator();
    OBJ_58 = new RiZoneSortie();
    WTOU1 = new XRiCheckBox();
    SAUT1 = new XRiCheckBox();
    WTOU2 = new XRiCheckBox();
    SAUT2 = new XRiCheckBox();
    WTOU3 = new XRiCheckBox();
    panel4 = new JPanel();
    NAT1 = new XRiRadioButton();
    NAT2 = new XRiRadioButton();
    NAT3 = new XRiRadioButton();
    NAT4 = new XRiRadioButton();
    NAT5 = new XRiRadioButton();
    panel3 = new JPanel();
    OBJ_59 = new JLabel();
    OBJ_74 = new JLabel();
    ECLID2 = new XRiTextField();
    ECLIF2 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_57 = new JLabel();
    OBJ_60 = new JLabel();
    EFAMD = new XRiTextField();
    EFAMF = new XRiTextField();
    panel1 = new JPanel();
    OBJ_53 = new JLabel();
    OBJ_54 = new JLabel();
    EMOT2F = new XRiTextField();
    EMOT2D = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_42 ----
          OBJ_42.setTitle("Plages de crit\u00e8res");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_43 ----
          OBJ_43.setTitle("");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_46 ----
          OBJ_46.setTitle("");
          OBJ_46.setName("OBJ_46");

          //---- OBJ_52 ----
          OBJ_52.setTitle("Type de croisement s\u00e9lectionn\u00e9");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_58 ----
          OBJ_58.setText("Client / Groupe famille d'article / Mot de classement 2");
          OBJ_58.setFont(OBJ_58.getFont().deriveFont(OBJ_58.getFont().getStyle() | Font.BOLD, OBJ_58.getFont().getSize() + 3f));
          OBJ_58.setName("OBJ_58");

          //---- WTOU1 ----
          WTOU1.setText("S\u00e9lection compl\u00e8te");
          WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU1.setName("WTOU1");
          WTOU1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU1ActionPerformed(e);
            }
          });

          //---- SAUT1 ----
          SAUT1.setText("Saut de page");
          SAUT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SAUT1.setName("SAUT1");

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU2.setName("WTOU2");
          WTOU2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU2ActionPerformed(e);
            }
          });

          //---- SAUT2 ----
          SAUT2.setText("Saut de page");
          SAUT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SAUT2.setName("SAUT2");

          //---- WTOU3 ----
          WTOU3.setText("S\u00e9lection compl\u00e8te");
          WTOU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU3.setName("WTOU3");
          WTOU3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU3ActionPerformed(e);
            }
          });

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Options possibles pour code nature"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- NAT1 ----
            NAT1.setText("Chiffre d'affaire H.T");
            NAT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT1.setName("NAT1");
            panel4.add(NAT1);
            NAT1.setBounds(20, 35, 241, NAT1.getPreferredSize().height);

            //---- NAT2 ----
            NAT2.setText("Chiffre d'affaire H.T + marge");
            NAT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT2.setName("NAT2");
            panel4.add(NAT2);
            NAT2.setBounds(20, 60, 241, NAT2.getPreferredSize().height);

            //---- NAT3 ----
            NAT3.setText("Quantit\u00e9");
            NAT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT3.setName("NAT3");
            panel4.add(NAT3);
            NAT3.setBounds(280, 35, 241, NAT3.getPreferredSize().height);

            //---- NAT4 ----
            NAT4.setText("Chiffre d'affaire H.T + quantit\u00e9");
            NAT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT4.setName("NAT4");
            panel4.add(NAT4);
            NAT4.setBounds(280, 60, 241, NAT4.getPreferredSize().height);

            //---- NAT5 ----
            NAT5.setText("Chiffre d'affaire H.T + marge + quantit\u00e9");
            NAT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT5.setName("NAT5");
            panel4.add(NAT5);
            NAT5.setBounds(540, 35, 244, NAT5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_59 ----
            OBJ_59.setText("Client de d\u00e9but");
            OBJ_59.setName("OBJ_59");
            panel3.add(OBJ_59);
            OBJ_59.setBounds(15, 20, 180, OBJ_59.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("Client de fin");
            OBJ_74.setName("OBJ_74");
            panel3.add(OBJ_74);
            OBJ_74.setBounds(300, 20, 180, OBJ_74.getPreferredSize().height);

            //---- ECLID2 ----
            ECLID2.setComponentPopupMenu(BTD);
            ECLID2.setName("ECLID2");
            panel3.add(ECLID2);
            ECLID2.setBounds(205, 15, 70, ECLID2.getPreferredSize().height);

            //---- ECLIF2 ----
            ECLIF2.setComponentPopupMenu(BTD);
            ECLIF2.setName("ECLIF2");
            panel3.add(ECLIF2);
            ECLIF2.setBounds(490, 15, 70, ECLIF2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_57 ----
            OBJ_57.setText("Groupe/Famille de d\u00e9but");
            OBJ_57.setName("OBJ_57");
            panel2.add(OBJ_57);
            OBJ_57.setBounds(10, 16, 180, 16);

            //---- OBJ_60 ----
            OBJ_60.setText("Groupe/Famille de fin");
            OBJ_60.setName("OBJ_60");
            panel2.add(OBJ_60);
            OBJ_60.setBounds(10, 45, 180, 16);

            //---- EFAMD ----
            EFAMD.setComponentPopupMenu(BTD);
            EFAMD.setName("EFAMD");
            panel2.add(EFAMD);
            EFAMD.setBounds(205, 10, 45, EFAMD.getPreferredSize().height);

            //---- EFAMF ----
            EFAMF.setComponentPopupMenu(BTD);
            EFAMF.setName("EFAMF");
            panel2.add(EFAMF);
            EFAMF.setBounds(205, 40, 45, EFAMF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel1 ========
          {
            panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_53 ----
            OBJ_53.setText("Mot de classement 2 de d\u00e9but");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(10, 16, 180, 16);

            //---- OBJ_54 ----
            OBJ_54.setText("Mot de classement 2 de fin");
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(10, 46, 180, 16);

            //---- EMOT2F ----
            EMOT2F.setComponentPopupMenu(BTD);
            EMOT2F.setName("EMOT2F");
            panel1.add(EMOT2F);
            EMOT2F.setBounds(205, 40, 214, EMOT2F.getPreferredSize().height);

            //---- EMOT2D ----
            EMOT2D.setComponentPopupMenu(BTD);
            EMOT2D.setName("EMOT2D");
            panel1.add(EMOT2D);
            EMOT2D.setBounds(205, 10, 214, EMOT2D.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 815, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 815, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 815, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SAUT1, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
                .addGap(74, 74, 74)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 815, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SAUT2, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
                .addGap(74, 74, 74)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 815, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU3, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 815, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(17, 17, 17)
                    .addComponent(SAUT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(17, 17, 17)
                    .addComponent(SAUT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WTOU3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- R2_GRP ----
    ButtonGroup R2_GRP = new ButtonGroup();
    R2_GRP.add(NAT1);
    R2_GRP.add(NAT2);
    R2_GRP.add(NAT3);
    R2_GRP.add(NAT4);
    R2_GRP.add(NAT5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_43;
  private JXTitledSeparator OBJ_46;
  private JXTitledSeparator OBJ_52;
  private RiZoneSortie OBJ_58;
  private XRiCheckBox WTOU1;
  private XRiCheckBox SAUT1;
  private XRiCheckBox WTOU2;
  private XRiCheckBox SAUT2;
  private XRiCheckBox WTOU3;
  private JPanel panel4;
  private XRiRadioButton NAT1;
  private XRiRadioButton NAT2;
  private XRiRadioButton NAT3;
  private XRiRadioButton NAT4;
  private XRiRadioButton NAT5;
  private JPanel panel3;
  private JLabel OBJ_59;
  private JLabel OBJ_74;
  private XRiTextField ECLID2;
  private XRiTextField ECLIF2;
  private JPanel panel2;
  private JLabel OBJ_57;
  private JLabel OBJ_60;
  private XRiTextField EFAMD;
  private XRiTextField EFAMF;
  private JPanel panel1;
  private JLabel OBJ_53;
  private JLabel OBJ_54;
  private XRiTextField EMOT2F;
  private XRiTextField EMOT2D;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
