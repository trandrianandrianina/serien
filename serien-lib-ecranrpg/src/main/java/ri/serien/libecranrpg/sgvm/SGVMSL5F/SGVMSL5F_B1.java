
package ri.serien.libecranrpg.sgvm.SGVMSL5F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVMSL5F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMSL5F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WARTU.setValeursSelection("1", "");
    WCL1U.setValeursSelection("1", "");
    WCL2U.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CLCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCLI@")).trim());
    CLLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLLIV@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Restriction de recherche pour lignes d'avoir");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    label1 = new JLabel();
    CLCLI = new RiZoneSortie();
    CLLIV = new RiZoneSortie();
    riZoneSortie1 = new RiZoneSortie();
    label2 = new JLabel();
    panel3 = new JPanel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label8 = new JLabel();
    WBON = new XRiTextField();
    WNFA = new XRiTextField();
    WART = new XRiTextField();
    WCL1 = new XRiTextField();
    WCL2 = new XRiTextField();
    WARTU = new XRiCheckBox();
    WCL1U = new XRiCheckBox();
    WCL2U = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(710, 360));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Client concern\u00e9"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label1 ----
            label1.setText("Num\u00e9ro");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(25, 30, 60, 24);

            //---- CLCLI ----
            CLCLI.setText("@CLCLI@");
            CLCLI.setHorizontalAlignment(SwingConstants.RIGHT);
            CLCLI.setName("CLCLI");
            panel2.add(CLCLI);
            CLCLI.setBounds(145, 30, 60, CLCLI.getPreferredSize().height);

            //---- CLLIV ----
            CLLIV.setText("@CLLIV@");
            CLLIV.setHorizontalAlignment(SwingConstants.RIGHT);
            CLLIV.setName("CLLIV");
            panel2.add(CLLIV);
            CLLIV.setBounds(210, 30, 30, CLLIV.getPreferredSize().height);

            //---- riZoneSortie1 ----
            riZoneSortie1.setText("@CLNOM@");
            riZoneSortie1.setName("riZoneSortie1");
            panel2.add(riZoneSortie1);
            riZoneSortie1.setBounds(145, 65, 310, riZoneSortie1.getPreferredSize().height);

            //---- label2 ----
            label2.setText("Nom");
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(25, 65, 60, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(5, 5, 505, 110);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Restrictions"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label3 ----
            label3.setText("Num\u00e9ro de bon");
            label3.setName("label3");
            panel3.add(label3);
            label3.setBounds(25, 40, 95, 24);

            //---- label4 ----
            label4.setText("Num\u00e9ro de facture");
            label4.setName("label4");
            panel3.add(label4);
            label4.setBounds(265, 40, 115, 24);

            //---- label5 ----
            label5.setText("Article");
            label5.setName("label5");
            panel3.add(label5);
            label5.setBounds(25, 78, 95, 24);

            //---- label6 ----
            label6.setText("Classement 1");
            label6.setName("label6");
            panel3.add(label6);
            label6.setBounds(25, 116, 95, 24);

            //---- label8 ----
            label8.setText("Classement 2");
            label8.setName("label8");
            panel3.add(label8);
            label8.setBounds(25, 154, 95, 24);

            //---- WBON ----
            WBON.setHorizontalAlignment(SwingConstants.RIGHT);
            WBON.setName("WBON");
            panel3.add(WBON);
            WBON.setBounds(145, 38, 60, WBON.getPreferredSize().height);

            //---- WNFA ----
            WNFA.setHorizontalAlignment(SwingConstants.RIGHT);
            WNFA.setName("WNFA");
            panel3.add(WNFA);
            WNFA.setBounds(385, 38, 70, WNFA.getPreferredSize().height);

            //---- WART ----
            WART.setName("WART");
            panel3.add(WART);
            WART.setBounds(145, 76, 210, WART.getPreferredSize().height);

            //---- WCL1 ----
            WCL1.setName("WCL1");
            panel3.add(WCL1);
            WCL1.setBounds(145, 114, 210, WCL1.getPreferredSize().height);

            //---- WCL2 ----
            WCL2.setName("WCL2");
            panel3.add(WCL2);
            WCL2.setBounds(145, 152, 210, WCL2.getPreferredSize().height);

            //---- WARTU ----
            WARTU.setText("Scan");
            WARTU.setName("WARTU");
            panel3.add(WARTU);
            WARTU.setBounds(385, 76, 65, 28);

            //---- WCL1U ----
            WCL1U.setText("Scan");
            WCL1U.setName("WCL1U");
            panel3.add(WCL1U);
            WCL1U.setBounds(385, 114, 65, 28);

            //---- WCL2U ----
            WCL2U.setText("Scan");
            WCL2U.setName("WCL2U");
            panel3.add(WCL2U);
            WCL2U.setBounds(385, 152, 65, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(5, 125, 505, 210);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel2;
  private JLabel label1;
  private RiZoneSortie CLCLI;
  private RiZoneSortie CLLIV;
  private RiZoneSortie riZoneSortie1;
  private JLabel label2;
  private JPanel panel3;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label8;
  private XRiTextField WBON;
  private XRiTextField WNFA;
  private XRiTextField WART;
  private XRiTextField WCL1;
  private XRiTextField WCL2;
  private XRiCheckBox WARTU;
  private XRiCheckBox WCL1U;
  private XRiCheckBox WCL2U;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
