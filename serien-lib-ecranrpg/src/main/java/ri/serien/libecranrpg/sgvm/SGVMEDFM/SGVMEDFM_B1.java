
package ri.serien.libecranrpg.sgvm.SGVMEDFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMEDFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] WETA_Value = { "", "HOM", "EXP", "FAC", };
  
  public SGVMEDFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WETA.setValeurs(WETA_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    boolean is96 = lexique.isTrue("96");
    lbPlanning.setVisible(is96);
    
    boolean is91 = lexique.isTrue("91");
    pChemin.setVisible(is91);

    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    KCGRPF.setEnabled(lexique.isPresent("KCGRPF"));
    KCGRPD.setEnabled(lexique.isPresent("KCGRPD"));
    KCSFAF.setEnabled(lexique.isPresent("KCSFAF"));
    KCFAMF.setEnabled(lexique.isPresent("KCFAMF"));
    KCSFAD.setEnabled(lexique.isPresent("KCSFAD"));
    KCFAMD.setEnabled(lexique.isPresent("KCFAMD"));
    WSUF.setVisible(lexique.isPresent("WSUF"));
    WNUM.setVisible(lexique.isPresent("WNUM"));
    RGSTAT.setEnabled(lexique.isPresent("RGSTAT"));
    OBJ_60.setVisible(lexique.isPresent("WCHED"));
    WCHED.setVisible(lexique.isPresent("WCHED"));
    WCHES.setVisible(lexique.isPresent("WCHES"));
    OBJ_58.setVisible(lexique.isPresent("WCHES"));
    CLNOM.setVisible(lexique.isPresent("CLNOM"));
    // WETA.setVisible( lexique.isPresent("WETA"));
    // OBJ_57.setVisible(lexique.isPresent("WCHES"));
    
    

    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WETA", 0, WETA_Value[WETA.getSelectedIndex()]);
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F6"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvmed"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_contenu = new JPanel();
    lbPlanning = new JLabel();
    pEtablissement = new JPanel();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    pClient = new JPanel();
    OBJ_62 = new JLabel();
    OBJ_55 = new JLabel();
    RGSTAT = new XRiTextField();
    WNUM = new XRiTextField();
    OBJ_43 = new JLabel();
    WSUF = new XRiTextField();
    CLNOM = new RiZoneSortie();
    KCFAMD = new XRiTextField();
    KCSFAD = new XRiTextField();
    KCFAMF = new XRiTextField();
    KCSFAF = new XRiTextField();
    KCGRPD = new XRiTextField();
    KCGRPF = new XRiTextField();
    OBJ_63 = new JLabel();
    pBons = new JPanel();
    OBJ_56 = new JLabel();
    WETA = new XRiComboBox();
    pChemin = new JPanel();
    OBJ_58 = new JLabel();
    WCHES = new XRiTextField();
    OBJ_60 = new JLabel();
    WCHED = new XRiTextField();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_contenu ========
    {
      p_contenu.setPreferredSize(new Dimension(700, 420));
      p_contenu.setBorder(new LineBorder(Color.darkGray));
      p_contenu.setBackground(new Color(239, 239, 222));
      p_contenu.setName("p_contenu");
      p_contenu.setLayout(new GridBagLayout());
      ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {990, 0};
      ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {30, 133, 176, 98, 119, 0};
      ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
      ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0, 1.0, 1.0, 1.0E-4};

      //---- lbPlanning ----
      lbPlanning.setText("\"*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***\"");
      lbPlanning.setForeground(new Color(255, 0, 51));
      lbPlanning.setFont(new Font("sansserif", Font.PLAIN, 14));
      lbPlanning.setName("lbPlanning");
      p_contenu.add(lbPlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pEtablissement ========
      {
        pEtablissement.setBorder(new CompoundBorder(
          new TitledBorder("Etablissement s\u00e9lectionn\u00e9"),
          new EmptyBorder(10, 10, 10, 10)));
        pEtablissement.setOpaque(false);
        pEtablissement.setName("pEtablissement");
        pEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout)pEtablissement.getLayout()).columnWidths = new int[] {0, 0, 524, 0};
        ((GridBagLayout)pEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pEtablissement.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- z_etablissement_ ----
        z_etablissement_.setComponentPopupMenu(null);
        z_etablissement_.setText("@WETB@");
        z_etablissement_.setFont(new Font("sansserif", Font.PLAIN, 14));
        z_etablissement_.setMaximumSize(new Dimension(50, 30));
        z_etablissement_.setMinimumSize(new Dimension(50, 30));
        z_etablissement_.setPreferredSize(new Dimension(50, 30));
        z_etablissement_.setName("z_etablissement_");
        pEtablissement.add(z_etablissement_, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- bouton_etablissement ----
        bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
        bouton_etablissement.setName("bouton_etablissement");
        bouton_etablissement.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bouton_etablissementActionPerformed(e);
          }
        });
        pEtablissement.add(bouton_etablissement, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- z_dgnom_ ----
        z_dgnom_.setText("@DGNOM@");
        z_dgnom_.setFont(new Font("sansserif", Font.PLAIN, 14));
        z_dgnom_.setMaximumSize(new Dimension(250, 30));
        z_dgnom_.setMinimumSize(new Dimension(250, 30));
        z_dgnom_.setPreferredSize(new Dimension(250, 30));
        z_dgnom_.setName("z_dgnom_");
        pEtablissement.add(z_dgnom_, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- z_wencx_ ----
        z_wencx_.setText("@WENCX@");
        z_wencx_.setPreferredSize(new Dimension(250, 30));
        z_wencx_.setMinimumSize(new Dimension(250, 30));
        z_wencx_.setMaximumSize(new Dimension(250, 30));
        z_wencx_.setFont(new Font("sansserif", Font.PLAIN, 14));
        z_wencx_.setName("z_wencx_");
        pEtablissement.add(z_wencx_, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_contenu.add(pEtablissement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(10, 10, 15, 10), 0, 0));

      //======== pClient ========
      {
        pClient.setBorder(new CompoundBorder(
          new TitledBorder("Client"),
          new EmptyBorder(10, 10, 10, 10)));
        pClient.setOpaque(false);
        pClient.setName("pClient");
        pClient.setLayout(new GridBagLayout());
        ((GridBagLayout)pClient.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pClient.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pClient.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pClient.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- OBJ_62 ----
        OBJ_62.setText("Groupe/Famille/Sous-famille");
        OBJ_62.setFont(new Font("sansserif", Font.PLAIN, 14));
        OBJ_62.setName("OBJ_62");
        pClient.add(OBJ_62, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- OBJ_55 ----
        OBJ_55.setText("Regroupement statistiques");
        OBJ_55.setFont(new Font("sansserif", Font.PLAIN, 14));
        OBJ_55.setName("OBJ_55");
        pClient.add(OBJ_55, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- RGSTAT ----
        RGSTAT.setComponentPopupMenu(BTD);
        RGSTAT.setFont(new Font("sansserif", Font.PLAIN, 14));
        RGSTAT.setMaximumSize(new Dimension(100, 30));
        RGSTAT.setMinimumSize(new Dimension(100, 30));
        RGSTAT.setPreferredSize(new Dimension(100, 30));
        RGSTAT.setName("RGSTAT");
        pClient.add(RGSTAT, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNUM ----
        WNUM.setComponentPopupMenu(BTD);
        WNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNUM.setMinimumSize(new Dimension(100, 30));
        WNUM.setMaximumSize(new Dimension(100, 30));
        WNUM.setPreferredSize(new Dimension(100, 30));
        WNUM.setName("WNUM");
        pClient.add(WNUM, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- OBJ_43 ----
        OBJ_43.setText("Code");
        OBJ_43.setFont(new Font("sansserif", Font.PLAIN, 14));
        OBJ_43.setName("OBJ_43");
        pClient.add(OBJ_43, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WSUF ----
        WSUF.setComponentPopupMenu(BTD);
        WSUF.setPreferredSize(new Dimension(50, 30));
        WSUF.setMinimumSize(new Dimension(50, 30));
        WSUF.setMaximumSize(new Dimension(50, 30));
        WSUF.setFont(new Font("sansserif", Font.PLAIN, 14));
        WSUF.setName("WSUF");
        pClient.add(WSUF, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- CLNOM ----
        CLNOM.setText("@CLNOM@");
        CLNOM.setFont(new Font("sansserif", Font.PLAIN, 14));
        CLNOM.setMaximumSize(new Dimension(350, 30));
        CLNOM.setMinimumSize(new Dimension(350, 30));
        CLNOM.setPreferredSize(new Dimension(350, 30));
        CLNOM.setName("CLNOM");
        pClient.add(CLNOM, new GridBagConstraints(5, 0, 5, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- KCFAMD ----
        KCFAMD.setComponentPopupMenu(BTD);
        KCFAMD.setFont(new Font("sansserif", Font.PLAIN, 14));
        KCFAMD.setMinimumSize(new Dimension(50, 30));
        KCFAMD.setMaximumSize(new Dimension(50, 30));
        KCFAMD.setPreferredSize(new Dimension(50, 30));
        KCFAMD.setName("KCFAMD");
        pClient.add(KCFAMD, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- KCSFAD ----
        KCSFAD.setComponentPopupMenu(BTD);
        KCSFAD.setFont(new Font("sansserif", Font.PLAIN, 14));
        KCSFAD.setMinimumSize(new Dimension(50, 30));
        KCSFAD.setMaximumSize(new Dimension(50, 30));
        KCSFAD.setPreferredSize(new Dimension(50, 30));
        KCSFAD.setName("KCSFAD");
        pClient.add(KCSFAD, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- KCFAMF ----
        KCFAMF.setComponentPopupMenu(BTD);
        KCFAMF.setFont(new Font("sansserif", Font.PLAIN, 14));
        KCFAMF.setMaximumSize(new Dimension(50, 30));
        KCFAMF.setMinimumSize(new Dimension(50, 30));
        KCFAMF.setPreferredSize(new Dimension(50, 30));
        KCFAMF.setName("KCFAMF");
        pClient.add(KCFAMF, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- KCSFAF ----
        KCSFAF.setComponentPopupMenu(BTD);
        KCSFAF.setMaximumSize(new Dimension(50, 30));
        KCSFAF.setMinimumSize(new Dimension(50, 30));
        KCSFAF.setPreferredSize(new Dimension(50, 30));
        KCSFAF.setName("KCSFAF");
        pClient.add(KCSFAF, new GridBagConstraints(7, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- KCGRPD ----
        KCGRPD.setComponentPopupMenu(BTD);
        KCGRPD.setFont(new Font("sansserif", Font.PLAIN, 14));
        KCGRPD.setMinimumSize(new Dimension(50, 30));
        KCGRPD.setMaximumSize(new Dimension(50, 30));
        KCGRPD.setPreferredSize(new Dimension(50, 30));
        KCGRPD.setName("KCGRPD");
        pClient.add(KCGRPD, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- KCGRPF ----
        KCGRPF.setComponentPopupMenu(BTD);
        KCGRPF.setFont(new Font("sansserif", Font.PLAIN, 14));
        KCGRPF.setMaximumSize(new Dimension(50, 30));
        KCGRPF.setMinimumSize(new Dimension(50, 30));
        KCGRPF.setPreferredSize(new Dimension(50, 30));
        KCGRPF.setName("KCGRPF");
        pClient.add(KCGRPF, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- OBJ_63 ----
        OBJ_63.setText("\u00e0");
        OBJ_63.setFont(new Font("sansserif", Font.PLAIN, 14));
        OBJ_63.setName("OBJ_63");
        pClient.add(OBJ_63, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));
      }
      p_contenu.add(pClient, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(10, 10, 15, 10), 0, 0));

      //======== pBons ========
      {
        pBons.setBorder(new CompoundBorder(
          new TitledBorder("Bons"),
          new EmptyBorder(10, 10, 10, 10)));
        pBons.setOpaque(false);
        pBons.setName("pBons");
        pBons.setLayout(new GridBagLayout());
        ((GridBagLayout)pBons.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pBons.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pBons.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pBons.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- OBJ_56 ----
        OBJ_56.setText("Etat des bons \u00e0 traiter");
        OBJ_56.setFont(new Font("sansserif", Font.PLAIN, 14));
        OBJ_56.setName("OBJ_56");
        pBons.add(OBJ_56, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WETA ----
        WETA.setModel(new DefaultComboBoxModel(new String[] {
          " ",
          "Homologu\u00e9s",
          "Exp\u00e9di\u00e9s",
          "Factur\u00e9s"
        }));
        WETA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WETA.setName("WETA");
        pBons.add(WETA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_contenu.add(pBons, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(10, 10, 15, 10), 0, 0));

      //======== pChemin ========
      {
        pChemin.setBorder(new CompoundBorder(
          new TitledBorder("Chemin fichier"),
          new EmptyBorder(10, 10, 10, 10)));
        pChemin.setOpaque(false);
        pChemin.setName("pChemin");
        pChemin.setLayout(new GridBagLayout());
        ((GridBagLayout)pChemin.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pChemin.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pChemin.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pChemin.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- OBJ_58 ----
        OBJ_58.setText("Chemin fichier SERIE M  (source)");
        OBJ_58.setFont(new Font("sansserif", Font.PLAIN, 14));
        OBJ_58.setName("OBJ_58");
        pChemin.add(OBJ_58, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WCHES ----
        WCHES.setComponentPopupMenu(BTD);
        WCHES.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCHES.setMaximumSize(new Dimension(400, 30));
        WCHES.setMinimumSize(new Dimension(400, 30));
        WCHES.setPreferredSize(new Dimension(400, 30));
        WCHES.setName("WCHES");
        pChemin.add(WCHES, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- OBJ_60 ----
        OBJ_60.setText("Chemin fichier central  (destination)");
        OBJ_60.setFont(new Font("sansserif", Font.PLAIN, 14));
        OBJ_60.setName("OBJ_60");
        pChemin.add(OBJ_60, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WCHED ----
        WCHED.setComponentPopupMenu(BTD);
        WCHED.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCHED.setMaximumSize(new Dimension(400, 30));
        WCHED.setMinimumSize(new Dimension(400, 30));
        WCHED.setPreferredSize(new Dimension(400, 30));
        WCHED.setName("WCHED");
        pChemin.add(WCHED, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_contenu.add(pChemin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(10, 10, 10, 10), 0, 0));
    }
    add(p_contenu, BorderLayout.CENTER);

    //======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());

      //======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());

        //======== navig_erreurs ========
        {
          navig_erreurs.setName("navig_erreurs");

          //---- bouton_erreurs ----
          bouton_erreurs.setText("Erreurs");
          bouton_erreurs.setToolTipText("Erreurs");
          bouton_erreurs.setName("bouton_erreurs");
          navig_erreurs.add(bouton_erreurs);
        }
        menus_bas.add(navig_erreurs);

        //======== navig_valid ========
        {
          navig_valid.setName("navig_valid");

          //---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setToolTipText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);

        //======== navig_retour ========
        {
          navig_retour.setName("navig_retour");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setToolTipText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);

      //======== scroll_droite ========
      {
        scroll_droite.setBackground(new Color(238, 239, 241));
        scroll_droite.setPreferredSize(new Dimension(16, 520));
        scroll_droite.setBorder(null);
        scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll_droite.setName("scroll_droite");

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 200));
          menus_haut.setPreferredSize(new Dimension(160, 200));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt_export ----
            riSousMenu_bt_export.setText("Exportation tableur");
            riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
            riSousMenu_bt_export.setName("riSousMenu_bt_export");
            riSousMenu_bt_export.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt_exportActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt_export);
          }
          menus_haut.add(riSousMenu6);
        }
        scroll_droite.setViewportView(menus_haut);
      }
      p_menus.add(scroll_droite, BorderLayout.NORTH);
    }
    add(p_menus, BorderLayout.LINE_END);

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.PAGE_START);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Traitement par lots");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_contenu;
  private JLabel lbPlanning;
  private JPanel pEtablissement;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private JPanel pClient;
  private JLabel OBJ_62;
  private JLabel OBJ_55;
  private XRiTextField RGSTAT;
  private XRiTextField WNUM;
  private JLabel OBJ_43;
  private XRiTextField WSUF;
  private RiZoneSortie CLNOM;
  private XRiTextField KCFAMD;
  private XRiTextField KCSFAD;
  private XRiTextField KCFAMF;
  private XRiTextField KCSFAF;
  private XRiTextField KCGRPD;
  private XRiTextField KCGRPF;
  private JLabel OBJ_63;
  private JPanel pBons;
  private JLabel OBJ_56;
  private XRiComboBox WETA;
  private JPanel pChemin;
  private JLabel OBJ_58;
  private XRiTextField WCHES;
  private JLabel OBJ_60;
  private XRiTextField WCHED;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
