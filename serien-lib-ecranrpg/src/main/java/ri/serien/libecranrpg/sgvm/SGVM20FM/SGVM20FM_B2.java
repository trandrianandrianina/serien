
package ri.serien.libecranrpg.sgvm.SGVM20FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM261] Gestion des ventes -> Documents de ventes -> Factures périodiques -> Facturation périodique
 * Indicateur:01000001
 * Titre:Facturation périodique
 */
public class SGVM20FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORTER = "Exporter";
  
  public SGVM20FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    NSOLD.setValeursSelection("1", " ");
    REPON1.setValeursSelection("OUI", "NON");
    CFAC1.setValeurs("1", "RB");
    CFAC2.setValeurs("2", "RB");
    CFAC3.setValeurs("3", "RB");
    CFAC4.setValeurs("4", "RB");
    CFAC5.setValeurs("5", "RB");
    CFAC6.setValeurs("6", "RB");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    CFAC1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD1@")).trim());
    CFAC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD2@")).trim());
    CFAC3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD3@")).trim());
    CFAC4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD4@")).trim());
    CFAC5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD5@")).trim());
    CFAC6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverse après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // visibilité
    lbDATFAC.setVisible(!lexique.isTrue("97"));
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation des magasins
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    TFLIB1.removeAllItems();
    // Le RPG envoyant rien lors de la sélection de tous, on le rentre manuellement
    if (lexique.HostFieldGetData("CODTF").trim().contentEquals("*")) {
      TFLIB1.addItem("Tous TF");
    }
    else {
      TFLIB1.addItem(lexique.HostFieldGetData("TFLIB1").trim());
    }
    
    // initialisation des composants
    chargerComposantMagasin();
    chargerComposantRepresentant();
    chargerComposantCategorieClient();
    chargerComposantTransporteur();
  }
  
  @Override
  public void getData() {
    super.getData();
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snRepresentant.renseignerChampRPG(lexique, "CODREP");
    snCategorieClient.renseignerChampRPG(lexique, "CODCC");
    snTransporteur.renseignerChampRPG(lexique, "CODTR");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composant magasins
   */
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Charge le composant representant
   */
  private void chargerComposantRepresentant() {
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "CODREP");
  }
  
  /**
   * Charge le composant des catégorie clients
   */
  private void chargerComposantCategorieClient() {
    snCategorieClient.setSession(getSession());
    snCategorieClient.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClient.setTousAutorise(true);
    snCategorieClient.charger(false);
    snCategorieClient.setSelectionParChampRPG(lexique, "CODCC");
  }
  
  /**
   * Charge le composant des transporteurs
   */
  private void chargerComposantTransporteur() {
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(snEtablissement.getIdSelection());
    snTransporteur.setTousAutorise(true);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "CODTR");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlInformations = new SNPanelTitre();
    lbDATFAC = new SNLabelChamp();
    DATFAC = new XRiCalendrier();
    lbTypeFacturation = new SNLabelChamp();
    TFLIB1 = new JComboBox();
    pnlCritereSelection = new SNPanelTitre();
    lbRepresentant = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    lbCategorieClient = new SNLabelChamp();
    snCategorieClient = new SNCategorieClient();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlOptions = new SNPanelTitre();
    lbSelectionBL = new SNLabelChamp();
    MTTC = new XRiTextField();
    lbEuros = new SNLabelChamp();
    REPON1 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlFacturation = new SNPanelTitre();
    CFAC1 = new XRiRadioButton();
    CFAC2 = new XRiRadioButton();
    CFAC3 = new XRiRadioButton();
    CFAC4 = new XRiRadioButton();
    CFAC5 = new XRiRadioButton();
    CFAC6 = new XRiRadioButton();
    NSOLD = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    RB_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Facturation p\u00e9riodique");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlInformations ========
        {
          pnlInformations.setTitre("Informations de facturation");
          pnlInformations.setName("pnlInformations");
          pnlInformations.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlInformations.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlInformations.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlInformations.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlInformations.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbDATFAC ----
          lbDATFAC.setText("Date de facturation");
          lbDATFAC.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDATFAC.setName("lbDATFAC");
          pnlInformations.add(lbDATFAC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- DATFAC ----
          DATFAC.setFont(new Font("sansserif", Font.PLAIN, 14));
          DATFAC.setPreferredSize(new Dimension(110, 30));
          DATFAC.setMinimumSize(new Dimension(110, 30));
          DATFAC.setMaximumSize(new Dimension(110, 30));
          DATFAC.setName("DATFAC");
          pnlInformations.add(DATFAC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbTypeFacturation ----
          lbTypeFacturation.setText("Type de facturation");
          lbTypeFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTypeFacturation.setName("lbTypeFacturation");
          pnlInformations.add(lbTypeFacturation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- TFLIB1 ----
          TFLIB1.setFont(new Font("sansserif", Font.PLAIN, 14));
          TFLIB1.setPreferredSize(new Dimension(75, 30));
          TFLIB1.setMinimumSize(new Dimension(75, 30));
          TFLIB1.setEnabled(false);
          TFLIB1.setMaximumSize(new Dimension(75, 30));
          TFLIB1.setName("TFLIB1");
          pnlInformations.add(TFLIB1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbRepresentant ----
          lbRepresentant.setText("Repr\u00e9sentant");
          lbRepresentant.setName("lbRepresentant");
          pnlCritereSelection.add(lbRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snRepresentant ----
          snRepresentant.setEnabled(false);
          snRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
          snRepresentant.setName("snRepresentant");
          pnlCritereSelection.add(snRepresentant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbCategorieClient ----
          lbCategorieClient.setText("Cat\u00e9gorie client");
          lbCategorieClient.setName("lbCategorieClient");
          pnlCritereSelection.add(lbCategorieClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snCategorieClient ----
          snCategorieClient.setEnabled(false);
          snCategorieClient.setFont(new Font("sansserif", Font.PLAIN, 14));
          snCategorieClient.setName("snCategorieClient");
          pnlCritereSelection.add(snCategorieClient, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbTransporteur ----
          lbTransporteur.setText("Transporteur");
          lbTransporteur.setName("lbTransporteur");
          pnlCritereSelection.add(lbTransporteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snTransporteur ----
          snTransporteur.setEnabled(false);
          snTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
          snTransporteur.setName("snTransporteur");
          pnlCritereSelection.add(snTransporteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlCritereSelection.add(lbMagasin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasin ----
          snMagasin.setEnabled(false);
          snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin.setName("snMagasin");
          pnlCritereSelection.add(snMagasin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptions ========
        {
          pnlOptions.setTitre("Options de livraisons et d'\u00e9tat");
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbSelectionBL ----
          lbSelectionBL.setText("S\u00e9lection des livraisons sup\u00e9rieures \u00e0");
          lbSelectionBL.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbSelectionBL.setPreferredSize(new Dimension(240, 30));
          lbSelectionBL.setMinimumSize(new Dimension(240, 30));
          lbSelectionBL.setName("lbSelectionBL");
          pnlOptions.add(lbSelectionBL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- MTTC ----
          MTTC.setFont(new Font("sansserif", Font.PLAIN, 14));
          MTTC.setPreferredSize(new Dimension(80, 30));
          MTTC.setMinimumSize(new Dimension(80, 30));
          MTTC.setName("MTTC");
          pnlOptions.add(MTTC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbEuros ----
          lbEuros.setText("euros");
          lbEuros.setMinimumSize(new Dimension(36, 30));
          lbEuros.setMaximumSize(new Dimension(36, 30));
          lbEuros.setPreferredSize(new Dimension(36, 30));
          lbEuros.setName("lbEuros");
          pnlOptions.add(lbEuros, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- REPON1 ----
          REPON1.setText("Etat r\u00e9capitulatif");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
          REPON1.setMinimumSize(new Dimension(124, 30));
          REPON1.setPreferredSize(new Dimension(124, 30));
          REPON1.setName("REPON1");
          pnlOptions.add(REPON1, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlGauche.add(pnlOptions, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- tfEnCours ----
          tfEnCours.setText("@WENCX@");
          tfEnCours.setEnabled(false);
          tfEnCours.setPreferredSize(new Dimension(260, 30));
          tfEnCours.setMinimumSize(new Dimension(260, 30));
          tfEnCours.setMaximumSize(new Dimension(260, 30));
          tfEnCours.setName("tfEnCours");
          pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setEnabled(false);
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlFacturation ========
        {
          pnlFacturation.setTitre("Codes facturation");
          pnlFacturation.setName("pnlFacturation");
          pnlFacturation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlFacturation.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlFacturation.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlFacturation.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)pnlFacturation.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- CFAC1 ----
          CFAC1.setText("@COD1@");
          CFAC1.setMaximumSize(new Dimension(400, 30));
          CFAC1.setMinimumSize(new Dimension(400, 30));
          CFAC1.setPreferredSize(new Dimension(400, 30));
          CFAC1.setFont(new Font("sansserif", Font.PLAIN, 14));
          CFAC1.setName("CFAC1");
          pnlFacturation.add(CFAC1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CFAC2 ----
          CFAC2.setText("@COD2@");
          CFAC2.setMaximumSize(new Dimension(400, 30));
          CFAC2.setMinimumSize(new Dimension(400, 30));
          CFAC2.setPreferredSize(new Dimension(400, 30));
          CFAC2.setFont(new Font("sansserif", Font.PLAIN, 14));
          CFAC2.setName("CFAC2");
          pnlFacturation.add(CFAC2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CFAC3 ----
          CFAC3.setText("@COD3@");
          CFAC3.setMaximumSize(new Dimension(400, 30));
          CFAC3.setMinimumSize(new Dimension(400, 30));
          CFAC3.setPreferredSize(new Dimension(400, 30));
          CFAC3.setFont(new Font("sansserif", Font.PLAIN, 14));
          CFAC3.setName("CFAC3");
          pnlFacturation.add(CFAC3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CFAC4 ----
          CFAC4.setText("@COD4@");
          CFAC4.setMinimumSize(new Dimension(400, 30));
          CFAC4.setPreferredSize(new Dimension(400, 30));
          CFAC4.setFont(new Font("sansserif", Font.PLAIN, 14));
          CFAC4.setMaximumSize(new Dimension(400, 30));
          CFAC4.setName("CFAC4");
          pnlFacturation.add(CFAC4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CFAC5 ----
          CFAC5.setText("@COD5@");
          CFAC5.setMinimumSize(new Dimension(400, 30));
          CFAC5.setPreferredSize(new Dimension(400, 30));
          CFAC5.setFont(new Font("sansserif", Font.PLAIN, 14));
          CFAC5.setMaximumSize(new Dimension(400, 30));
          CFAC5.setName("CFAC5");
          pnlFacturation.add(CFAC5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CFAC6 ----
          CFAC6.setText("@COD6@");
          CFAC6.setMinimumSize(new Dimension(400, 30));
          CFAC6.setPreferredSize(new Dimension(400, 30));
          CFAC6.setFont(new Font("sansserif", Font.PLAIN, 14));
          CFAC6.setMaximumSize(new Dimension(400, 30));
          CFAC6.setName("CFAC6");
          pnlFacturation.add(CFAC6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- NSOLD ----
          NSOLD.setText("For\u00e7age commandes non sold\u00e9es");
          NSOLD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NSOLD.setFont(new Font("sansserif", Font.PLAIN, 14));
          NSOLD.setPreferredSize(new Dimension(238, 30));
          NSOLD.setMinimumSize(new Dimension(238, 30));
          NSOLD.setName("NSOLD");
          pnlFacturation.add(NSOLD, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlFacturation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //---- RB_GRP ----
    RB_GRP.add(CFAC1);
    RB_GRP.add(CFAC2);
    RB_GRP.add(CFAC3);
    RB_GRP.add(CFAC4);
    RB_GRP.add(CFAC5);
    RB_GRP.add(CFAC6);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlInformations;
  private SNLabelChamp lbDATFAC;
  private XRiCalendrier DATFAC;
  private SNLabelChamp lbTypeFacturation;
  private JComboBox TFLIB1;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbCategorieClient;
  private SNCategorieClient snCategorieClient;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlOptions;
  private SNLabelChamp lbSelectionBL;
  private XRiTextField MTTC;
  private SNLabelChamp lbEuros;
  private XRiCheckBox REPON1;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlFacturation;
  private XRiRadioButton CFAC1;
  private XRiRadioButton CFAC2;
  private XRiRadioButton CFAC3;
  private XRiRadioButton CFAC4;
  private XRiRadioButton CFAC5;
  private XRiRadioButton CFAC6;
  private XRiCheckBox NSOLD;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup RB_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
