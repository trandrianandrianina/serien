
package ri.serien.libecranrpg.sgvm.SGVM66FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2924] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Bons suspects
 * Indicateur : 00010001
 * Titre : Edition des bons de ventes suspects
 * 
 * [GVM2925] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Bons suspects / section
 * analytique
 * Indicateur : 10010001
 * Titre : Edition des bons de ventes suspects par section analytique
 * 
 * [GVM2928] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Ventes articles dépréciés /
 * vendeur
 * Indicateur : 01010001 (80 = 0)
 * Titre : Edition des ventes dépreciés par vendeur
 * 
 * [GVM2929] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Ventes d'articles fin de série
 * Indicateur : 01010001 (80 = 1)
 * Titre : Edition des ventes de fin de série par vendeur
 */
public class SGVM66FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private boolean isPlanning = false;
  private String[] cbCodeDate_Value = { "*DAT", "*ENC", "*PRE", };
  private String[] cbCodeDate_Text = { "Date du jour", "Mois en cours", "Mois précédant", };
  private Message LOCTP = null;
  
  public SGVM66FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    cbCodeDate.removeAllItems();
    for (String texte : cbCodeDate_Text) {
      cbCodeDate.addItem(texte);
    }
    
    OPT3.setValeursSelection("1", " ");
    OPT2.setValeursSelection("1", " ");
    OPT1.setValeursSelection("1", " ");
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverse après initialisation des données
    setDiverses();
    
    // Indicateur
    Boolean is80 = lexique.isTrue("80");
    Boolean is91 = lexique.isTrue("91");
    Boolean is92 = lexique.isTrue("92");
    Boolean is94 = lexique.isTrue("94");
    
    isPlanning = lexique.isTrue("90");
    
    cbCodeDate.setVisible(isPlanning);
    lbPlanning.setVisible(isPlanning);
    PERDEB.setVisible(!isPlanning);
    PERFIN.setVisible(!isPlanning);
    lbAu.setVisible(!isPlanning);
    if (isPlanning) {
      cbCodeDate.setSelectedItem(lexique.HostFieldGetData("WCPER"));
    }
    
    // Mets le composant suivant la situation
    if (is91 && is94) {
      lbVendeur.setText("Section analytique");
      snSectionAnalytique.setVisible(true);
      snVendeur.setVisible(false);
    }
    else {
      if (is92 && is94) {
        lbVendeur.setText("Vendeur");
        snSectionAnalytique.setVisible(false);
        snVendeur.setVisible(true);
      }
      else {
        snSectionAnalytique.setVisible(false);
        snVendeur.setVisible(false);
        lbVendeur.setVisible(false);
      }
    }
    
    // Mets le bon titre suivant le pts de menu
    if (is94) {
      bpPresentation.setText("Edition des bons de ventes suspects");
    }
    if (is91) {
      bpPresentation.setText("Edition des bons de ventes suspects par section analytique");
    }
    if (is80 && is92) {
      bpPresentation.setText("Edition des ventes des articles en fin de série par vendeur");
    }
    if (!is80 && is92) {
      bpPresentation.setText("Edition des ventes des articles dépreciés par vendeur");
    }
    
    // Sélection d'une option d'édition par défaut
    if (!OPT1.isSelected() || !OPT2.isSelected() || !OPT3.isSelected()) {
      OPT1.setSelected(true);
    }
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // charger les composants
    chargerListeVendeurs();
    chargerListeSectionAnalytique();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    if (isPlanning) {
      lexique.HostFieldPutData("WCPER", 0, cbCodeDate_Value[cbCodeDate.getSelectedIndex()]);
    }
    
    if (snSectionAnalytique.isVisible()) {
      if (snSectionAnalytique.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUS", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUS", 0, "  ");
        snSectionAnalytique.renseignerChampRPG(lexique, "SAN");
      }
    }
    if (snVendeur.isVisible()) {
      if (snVendeur.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUS", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUS", 0, "  ");
        snVendeur.renseignerChampRPG(lexique, "WVDE");
      }
    }
  }
  
  /**
   * Charger les vendeurs
   */
  private void chargerListeVendeurs() {
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "WVDE");
  }
  
  /**
   * Charger les section analytique
   */
  private void chargerListeSectionAnalytique() {
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "SAN");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeVendeurs();
      chargerListeSectionAnalytique();
      tfEnCours.setText(lexique.HostFieldGetData("WENCX"));
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbPeriodeAEditer = new SNLabelChamp();
    pnlPeriodeEditer = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    cbCodeDate = new SNComboBox();
    lbVendeur = new SNLabelChamp();
    pnlMiseEnformeVendeurOuAnalytique = new SNPanel();
    snVendeur = new SNVendeur();
    snSectionAnalytique = new SNSectionAnalytique();
    pnlBonEditer = new SNPanelTitre();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPlanning ----
        lbPlanning.setText("Vous \u00eates en cours de param\u00e9trage en vue de la mise au planning");
        lbPlanning.setMinimumSize(new Dimension(120, 30));
        lbPlanning.setPreferredSize(new Dimension(120, 30));
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriodeAEditer ----
            lbPeriodeAEditer.setText("P\u00e9riode \u00e0 \u00e9diter du");
            lbPeriodeAEditer.setMinimumSize(new Dimension(200, 30));
            lbPeriodeAEditer.setPreferredSize(new Dimension(200, 30));
            lbPeriodeAEditer.setMaximumSize(new Dimension(200, 30));
            lbPeriodeAEditer.setName("lbPeriodeAEditer");
            pnlCritereDeSelection.add(lbPeriodeAEditer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeEditer ========
            {
              pnlPeriodeEditer.setName("pnlPeriodeEditer");
              pnlPeriodeEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- PERDEB ----
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setMinimumSize(new Dimension(110, 30));
              PERDEB.setPreferredSize(new Dimension(110, 30));
              PERDEB.setName("PERDEB");
              pnlPeriodeEditer.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setHorizontalAlignment(SwingConstants.LEFT);
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlPeriodeEditer.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERFIN ----
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setMinimumSize(new Dimension(110, 30));
              PERFIN.setPreferredSize(new Dimension(110, 30));
              PERFIN.setName("PERFIN");
              pnlPeriodeEditer.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbCodeDate ----
              cbCodeDate.setModel(new DefaultComboBoxModel(new String[] { "Date du jour", "D\u00e9but du mois en cours",
                  "Fin du mois en cours", "D\u00e9but du mois pr\u00e9c\u00e9dent", "Fin du mois pr\u00e9c\u00e9dent" }));
              cbCodeDate.setBackground(Color.white);
              cbCodeDate.setFont(new Font("sansserif", Font.PLAIN, 14));
              cbCodeDate.setName("cbCodeDate");
              pnlPeriodeEditer.add(cbCodeDate, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlPeriodeEditer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVendeur ----
            lbVendeur.setText("texte dans le code");
            lbVendeur.setMinimumSize(new Dimension(200, 30));
            lbVendeur.setPreferredSize(new Dimension(200, 30));
            lbVendeur.setMaximumSize(new Dimension(200, 30));
            lbVendeur.setName("lbVendeur");
            pnlCritereDeSelection.add(lbVendeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlMiseEnformeVendeurOuAnalytique ========
            {
              pnlMiseEnformeVendeurOuAnalytique.setName("pnlMiseEnformeVendeurOuAnalytique");
              pnlMiseEnformeVendeurOuAnalytique.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMiseEnformeVendeurOuAnalytique.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlMiseEnformeVendeurOuAnalytique.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMiseEnformeVendeurOuAnalytique.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlMiseEnformeVendeurOuAnalytique.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- snVendeur ----
              snVendeur.setFont(new Font("sansserif", Font.PLAIN, 14));
              snVendeur.setName("snVendeur");
              pnlMiseEnformeVendeurOuAnalytique.add(snVendeur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- snSectionAnalytique ----
              snSectionAnalytique.setFont(new Font("sansserif", Font.PLAIN, 14));
              snSectionAnalytique.setName("snSectionAnalytique");
              pnlMiseEnformeVendeurOuAnalytique.add(snSectionAnalytique, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlMiseEnformeVendeurOuAnalytique, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlBonEditer ========
          {
            pnlBonEditer.setTitre("Bons \u00e0 \u00e9diter");
            pnlBonEditer.setName("pnlBonEditer");
            pnlBonEditer.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlBonEditer.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlBonEditer.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlBonEditer.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlBonEditer.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OPT1 ----
            OPT1.setText("Bons valid\u00e9s non exp\u00e9di\u00e9s (VAL)");
            OPT1.setComponentPopupMenu(null);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setForeground(Color.black);
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setMinimumSize(new Dimension(250, 30));
            OPT1.setPreferredSize(new Dimension(250, 30));
            OPT1.setMaximumSize(new Dimension(250, 30));
            OPT1.setName("OPT1");
            pnlBonEditer.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
            OPT2.setComponentPopupMenu(null);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setMinimumSize(new Dimension(250, 30));
            OPT2.setPreferredSize(new Dimension(250, 30));
            OPT2.setMaximumSize(new Dimension(250, 30));
            OPT2.setName("OPT2");
            pnlBonEditer.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Bons factur\u00e9s (FAC)");
            OPT3.setComponentPopupMenu(null);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setMinimumSize(new Dimension(250, 30));
            OPT3.setPreferredSize(new Dimension(250, 30));
            OPT3.setMaximumSize(new Dimension(250, 30));
            OPT3.setName("OPT3");
            pnlBonEditer.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlBonEditer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEditable(false);
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setEnabled(false);
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNMessage lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbPeriodeAEditer;
  private SNPanel pnlPeriodeEditer;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNComboBox cbCodeDate;
  private SNLabelChamp lbVendeur;
  private SNPanel pnlMiseEnformeVendeurOuAnalytique;
  private SNVendeur snVendeur;
  private SNSectionAnalytique snSectionAnalytique;
  private SNPanelTitre pnlBonEditer;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
