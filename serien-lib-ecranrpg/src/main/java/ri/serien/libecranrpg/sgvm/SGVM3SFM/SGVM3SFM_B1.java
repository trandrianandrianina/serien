
package ri.serien.libecranrpg.sgvm.SGVM3SFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM3SFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] WTNS_Value = { "0", "5", "4", "3", "2", "9", "8", "7", "6", "1", };
  
  public SGVM3SFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTNS.setValeurs(WTNS_Value, null);
    WTOU1.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCCD@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCCF@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSCD@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSCF@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMAG@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!WTOU.isSelected() & WTOU.isVisible());
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTOU1.isSelected() & WTOU1.isVisible());
    OBJ_50.setVisible(lexique.isPresent("LIBDEV"));
    OBJ_48.setVisible(lexique.isPresent("LIBMAG"));
    
    // Combobox WTNS
    // WTNS.setSelectedIndex(getIndice("WTNS",WTNS_Value));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU1.isSelected())
    // lexique.HostFieldPutData("WTOU1", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU1", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // lexique.HostFieldPutData("WTNS", 0, WTNS_Value[WTNS.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_21 = new JXTitledSeparator();
    OBJ_25 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_45 = new RiZoneSortie();
    OBJ_46 = new RiZoneSortie();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    DEBCC = new XRiTextField();
    FINCC = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_42 = new RiZoneSortie();
    OBJ_44 = new RiZoneSortie();
    OBJ_32 = new JLabel();
    OBJ_43 = new JLabel();
    DEBSC = new XRiTextField();
    FINSC = new XRiTextField();
    WTOU = new XRiCheckBox();
    WTOU1 = new XRiCheckBox();
    panel1 = new JPanel();
    OBJ_47 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_48 = new RiZoneSortie();
    WTNS = new XRiComboBox();
    WDEV = new XRiTextField();
    OBJ_50 = new RiZoneSortie();
    WMAG = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(695, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_21 ----
          OBJ_21.setTitle("Autres s\u00e9lections");
          OBJ_21.setName("OBJ_21");

          //---- OBJ_25 ----
          OBJ_25.setTitle("Plage de codes scoring");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Cat\u00e9gorie client");
          OBJ_22.setName("OBJ_22");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_45 ----
            OBJ_45.setText("@LIBCCD@");
            OBJ_45.setName("OBJ_45");
            P_SEL0.add(OBJ_45);
            OBJ_45.setBounds(170, 10, 260, OBJ_45.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("@LIBCCF@");
            OBJ_46.setName("OBJ_46");
            P_SEL0.add(OBJ_46);
            OBJ_46.setBounds(170, 39, 260, OBJ_46.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("Code de d\u00e9but");
            OBJ_51.setName("OBJ_51");
            P_SEL0.add(OBJ_51);
            OBJ_51.setBounds(15, 12, 85, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("Code de fin");
            OBJ_52.setName("OBJ_52");
            P_SEL0.add(OBJ_52);
            OBJ_52.setBounds(15, 41, 85, 20);

            //---- DEBCC ----
            DEBCC.setComponentPopupMenu(BTD);
            DEBCC.setName("DEBCC");
            P_SEL0.add(DEBCC);
            DEBCC.setBounds(105, 8, 44, DEBCC.getPreferredSize().height);

            //---- FINCC ----
            FINCC.setComponentPopupMenu(BTD);
            FINCC.setName("FINCC");
            P_SEL0.add(FINCC);
            FINCC.setBounds(105, 37, 44, FINCC.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_42 ----
            OBJ_42.setText("@LIBSCD@");
            OBJ_42.setName("OBJ_42");
            P_SEL1.add(OBJ_42);
            OBJ_42.setBounds(170, 10, 260, OBJ_42.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("@LIBSCF@");
            OBJ_44.setName("OBJ_44");
            P_SEL1.add(OBJ_44);
            OBJ_44.setBounds(170, 39, 260, OBJ_44.getPreferredSize().height);

            //---- OBJ_32 ----
            OBJ_32.setText("Code de d\u00e9but");
            OBJ_32.setName("OBJ_32");
            P_SEL1.add(OBJ_32);
            OBJ_32.setBounds(15, 12, 85, 20);

            //---- OBJ_43 ----
            OBJ_43.setText("Code de fin");
            OBJ_43.setName("OBJ_43");
            P_SEL1.add(OBJ_43);
            OBJ_43.setBounds(15, 41, 85, 20);

            //---- DEBSC ----
            DEBSC.setComponentPopupMenu(BTD);
            DEBSC.setName("DEBSC");
            P_SEL1.add(DEBSC);
            DEBSC.setBounds(105, 8, 64, DEBSC.getPreferredSize().height);

            //---- FINSC ----
            FINSC.setComponentPopupMenu(BTD);
            FINSC.setName("FINSC");
            P_SEL1.add(FINSC);
            FINSC.setBounds(105, 37, 64, FINSC.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL1.setMinimumSize(preferredSize);
              P_SEL1.setPreferredSize(preferredSize);
            }
          }

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- WTOU1 ----
          WTOU1.setText("S\u00e9lection compl\u00e8te");
          WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU1.setName("WTOU1");
          WTOU1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU1ActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_47 ----
            OBJ_47.setText("Code magasin");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(10, 9, 91, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Top d'attention");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(10, 41, 148, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("S\u00e9lection devise");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(10, 73, 105, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("@LIBMAG@");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(210, 7, 260, OBJ_48.getPreferredSize().height);

            //---- WTNS ----
            WTNS.setModel(new DefaultComboBoxModel(new String[] {
              "0 - Zone personnalis\u00e9e",
              "5 - Zone personnalis\u00e9e",
              "4 - For\u00e7age bon en attente",
              "3 - Livraison interdite",
              "2 - Client interdit",
              "9 - Client d\u00e9sactiv\u00e9",
              "8 - Zone personnalis\u00e9e",
              "7 - Zone personnalis\u00e9e",
              "6 - Zone personnalis\u00e9e",
              "1 - Rouge clignotant"
            }));
            WTNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTNS.setName("WTNS");
            panel1.add(WTNS);
            WTNS.setBounds(165, 38, 178, WTNS.getPreferredSize().height);

            //---- WDEV ----
            WDEV.setComponentPopupMenu(BTD);
            WDEV.setName("WDEV");
            panel1.add(WDEV);
            WDEV.setBounds(165, 69, 44, WDEV.getPreferredSize().height);

            //---- OBJ_50 ----
            OBJ_50.setText("@LIBDEV@");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(210, 71, 210, OBJ_50.getPreferredSize().height);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            panel1.add(WMAG);
            WMAG.setBounds(165, 5, 34, WMAG.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_22;
  private JPanel P_SEL0;
  private RiZoneSortie OBJ_45;
  private RiZoneSortie OBJ_46;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private XRiTextField DEBCC;
  private XRiTextField FINCC;
  private JPanel P_SEL1;
  private RiZoneSortie OBJ_42;
  private RiZoneSortie OBJ_44;
  private JLabel OBJ_32;
  private JLabel OBJ_43;
  private XRiTextField DEBSC;
  private XRiTextField FINSC;
  private XRiCheckBox WTOU;
  private XRiCheckBox WTOU1;
  private JPanel panel1;
  private JLabel OBJ_47;
  private JLabel OBJ_53;
  private JLabel OBJ_49;
  private RiZoneSortie OBJ_48;
  private XRiComboBox WTNS;
  private XRiTextField WDEV;
  private RiZoneSortie OBJ_50;
  private XRiTextField WMAG;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
