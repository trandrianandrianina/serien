
package ri.serien.libecranrpg.sgvm.SGVM37FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2921] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Détail et marge
 * Indicateur : 00000001
 * Titre : Edition des bons de ventes détaillés
 */
public class SGVM37FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private Message LOCTP = null;
  
  public SGVM37FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    SAUCLI.setValeursSelection("OUI", "NON");
    NONSTA.setValeursSelection("OUI", "NON");
    DETART.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfEnCours.setVisible(lexique.isPresent("WENCX"));
    pnlTypeDocument.setVisible(OPT1.isVisible());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Renseigne l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Charge les composants de sélection
    chargerComposantMagasin();
    chargerComposantClient();
    chargerComposantSectionAnalytique();
    
    if (!OPT1.isSelected() && !OPT2.isSelected() && !OPT3.isSelected() && !OPT4.isSelected()) {
      OPT1.setSelected(true);
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Renseigne les champ qui on été sélectionné
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snClientDebut.renseignerChampRPG(lexique, "DEBCLI");
    snClientFin.renseignerChampRPG(lexique, "FINCLI");
    snSectionAnalytique.renseignerChampRPG(lexique, "SECAN");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    if (snClientDebut.getIdSelection() == null && snClientFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOU2", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU2", 0, "");
    }
    
    if (OPT1.isSelected()) {
      lexique.HostFieldPutData("OPT1", 0, "1");
      lexique.HostFieldPutData("OPT2", 0, "");
      lexique.HostFieldPutData("OPT3", 0, "");
      lexique.HostFieldPutData("OPT4", 0, "");
    }
    if (OPT2.isSelected()) {
      lexique.HostFieldPutData("OPT1", 0, "");
      lexique.HostFieldPutData("OPT2", 0, "1");
      lexique.HostFieldPutData("OPT3", 0, "");
      lexique.HostFieldPutData("OPT4", 0, "");
    }
    if (OPT3.isSelected()) {
      lexique.HostFieldPutData("OPT1", 0, "");
      lexique.HostFieldPutData("OPT2", 0, "");
      lexique.HostFieldPutData("OPT3", 0, "1");
      lexique.HostFieldPutData("OPT4", 0, "");
    }
    if (OPT4.isSelected()) {
      lexique.HostFieldPutData("OPT1", 0, "");
      lexique.HostFieldPutData("OPT2", 0, "");
      lexique.HostFieldPutData("OPT3", 0, "");
      lexique.HostFieldPutData("OPT4", 0, "1");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composant des magasins suivant l'établissement
   **/
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Charge le composant des sections analytique suivant l'établissement
   **/
  private void chargerComposantSectionAnalytique() {
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(true);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "SECAN");
  }
  
  /**
   * Charge le composant des clients suivant l'établissement
   **/
  private void chargerComposantClient() {
    snClientDebut.setSession(getSession());
    snClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snClientDebut.charger(false);
    snClientDebut.setSelectionParChampRPG(lexique, "DEBCLI");
    
    snClientFin.setSession(getSession());
    snClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snClientFin.charger(false);
    snClientFin.setSelectionParChampRPG(lexique, "FINCLI");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantMagasin();
      chargerComposantClient();
      chargerComposantSectionAnalytique();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbPeriodeAediter = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbSectionAnalytique = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    lbClienDeDebut = new SNLabelChamp();
    snClientDebut = new SNClientPrincipal();
    lbClientDeFin = new SNLabelChamp();
    snClientFin = new SNClientPrincipal();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    DETART = new XRiCheckBox();
    NONSTA = new XRiCheckBox();
    SAUCLI = new XRiCheckBox();
    pnlTypeDocument = new SNPanelTitre();
    OPT1 = new JRadioButton();
    OPT2 = new JRadioButton();
    OPT3 = new JRadioButton();
    OPT4 = new JRadioButton();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des bons de ventes d\u00e9taill\u00e9s");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.LEADING);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriodeAediter ----
            lbPeriodeAediter.setText("P\u00e9riode \u00e0 \u00e9diter du");
            lbPeriodeAediter.setMinimumSize(new Dimension(125, 30));
            lbPeriodeAediter.setPreferredSize(new Dimension(125, 30));
            lbPeriodeAediter.setMaximumSize(new Dimension(125, 30));
            lbPeriodeAediter.setName("lbPeriodeAediter");
            pnlCritereDeSelection.add(lbPeriodeAediter, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- PERDEB ----
              PERDEB.setNextFocusableComponent(null);
              PERDEB.setMinimumSize(new Dimension(110, 30));
              PERDEB.setPreferredSize(new Dimension(110, 30));
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setName("PERDEB");
              sNPanel1.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              sNPanel1.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERFIN ----
              PERFIN.setNextFocusableComponent(null);
              PERFIN.setPreferredSize(new Dimension(110, 30));
              PERFIN.setMinimumSize(new Dimension(110, 30));
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setName("PERFIN");
              sNPanel1.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setPreferredSize(new Dimension(125, 30));
            lbMagasin.setMinimumSize(new Dimension(125, 30));
            lbMagasin.setMaximumSize(new Dimension(125, 30));
            lbMagasin.setName("lbMagasin");
            pnlCritereDeSelection.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setName("snMagasin");
            pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSectionAnalytique ----
            lbSectionAnalytique.setText("Section analytique");
            lbSectionAnalytique.setPreferredSize(new Dimension(125, 30));
            lbSectionAnalytique.setMinimumSize(new Dimension(125, 30));
            lbSectionAnalytique.setMaximumSize(new Dimension(125, 30));
            lbSectionAnalytique.setName("lbSectionAnalytique");
            pnlCritereDeSelection.add(lbSectionAnalytique, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSectionAnalytique ----
            snSectionAnalytique.setName("snSectionAnalytique");
            pnlCritereDeSelection.add(snSectionAnalytique, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClienDeDebut ----
            lbClienDeDebut.setText("Client de d\u00e9but");
            lbClienDeDebut.setPreferredSize(new Dimension(125, 30));
            lbClienDeDebut.setMinimumSize(new Dimension(125, 30));
            lbClienDeDebut.setMaximumSize(new Dimension(125, 30));
            lbClienDeDebut.setName("lbClienDeDebut");
            pnlCritereDeSelection.add(lbClienDeDebut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClientDebut ----
            snClientDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snClientDebut.setPreferredSize(new Dimension(350, 30));
            snClientDebut.setMinimumSize(new Dimension(350, 30));
            snClientDebut.setMaximumSize(new Dimension(350, 30));
            snClientDebut.setName("snClientDebut");
            pnlCritereDeSelection.add(snClientDebut, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClientDeFin ----
            lbClientDeFin.setText("Client de fin");
            lbClientDeFin.setMinimumSize(new Dimension(125, 30));
            lbClientDeFin.setMaximumSize(new Dimension(125, 30));
            lbClientDeFin.setPreferredSize(new Dimension(125, 30));
            lbClientDeFin.setName("lbClientDeFin");
            pnlCritereDeSelection.add(lbClientDeFin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snClientFin ----
            snClientFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snClientFin.setMaximumSize(new Dimension(350, 30));
            snClientFin.setMinimumSize(new Dimension(350, 30));
            snClientFin.setPreferredSize(new Dimension(350, 30));
            snClientFin.setName("snClientFin");
            pnlCritereDeSelection.add(snClientFin, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setEditable(false);
            tfEnCours.setEnabled(false);
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setOpaque(false);
            pnlOptionEdition.setTitre("Option d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- DETART ----
            DETART.setText("D\u00e9tail lignes articles");
            DETART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DETART.setNextFocusableComponent(null);
            DETART.setFont(new Font("sansserif", Font.PLAIN, 14));
            DETART.setPreferredSize(new Dimension(148, 30));
            DETART.setMinimumSize(new Dimension(148, 30));
            DETART.setName("DETART");
            pnlOptionEdition.add(DETART, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- NONSTA ----
            NONSTA.setText("Articles non g\u00e9r\u00e9s en statistique");
            NONSTA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NONSTA.setNextFocusableComponent(null);
            NONSTA.setFont(new Font("sansserif", Font.PLAIN, 14));
            NONSTA.setMinimumSize(new Dimension(190, 30));
            NONSTA.setPreferredSize(new Dimension(190, 30));
            NONSTA.setName("NONSTA");
            pnlOptionEdition.add(NONSTA, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- SAUCLI ----
            SAUCLI.setText("Saut de page par client");
            SAUCLI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SAUCLI.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAUCLI.setPreferredSize(new Dimension(169, 30));
            SAUCLI.setMinimumSize(new Dimension(169, 30));
            SAUCLI.setName("SAUCLI");
            pnlOptionEdition.add(SAUCLI, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTypeDocument ========
          {
            pnlTypeDocument.setNextFocusableComponent(null);
            pnlTypeDocument.setOpaque(false);
            pnlTypeDocument.setTitre("Type de document");
            pnlTypeDocument.setPreferredSize(new Dimension(280, 185));
            pnlTypeDocument.setMinimumSize(new Dimension(280, 167));
            pnlTypeDocument.setName("pnlTypeDocument");
            pnlTypeDocument.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTypeDocument.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTypeDocument.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTypeDocument.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTypeDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OPT1 ----
            OPT1.setText("Bons homologu\u00e9s non exp\u00e9di\u00e9s (HOM)");
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setPreferredSize(new Dimension(45, 30));
            OPT1.setMinimumSize(new Dimension(45, 30));
            OPT1.setName("OPT1");
            pnlTypeDocument.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setPreferredSize(new Dimension(45, 30));
            OPT2.setMinimumSize(new Dimension(45, 30));
            OPT2.setName("OPT2");
            pnlTypeDocument.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Bons factur\u00e9s (FAC)");
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setPreferredSize(new Dimension(45, 30));
            OPT3.setMinimumSize(new Dimension(45, 30));
            OPT3.setName("OPT3");
            pnlTypeDocument.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT4 ----
            OPT4.setText("Devis (DEV)");
            OPT4.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT4.setPreferredSize(new Dimension(45, 30));
            OPT4.setMinimumSize(new Dimension(45, 30));
            OPT4.setName("OPT4");
            pnlTypeDocument.add(OPT4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlTypeDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- btgOptionEdition ----
    ButtonGroup btgOptionEdition = new ButtonGroup();
    btgOptionEdition.add(OPT1);
    btgOptionEdition.add(OPT2);
    btgOptionEdition.add(OPT3);
    btgOptionEdition.add(OPT4);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbPeriodeAediter;
  private SNPanel sNPanel1;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbClienDeDebut;
  private SNClientPrincipal snClientDebut;
  private SNLabelChamp lbClientDeFin;
  private SNClientPrincipal snClientFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox DETART;
  private XRiCheckBox NONSTA;
  private XRiCheckBox SAUCLI;
  private SNPanelTitre pnlTypeDocument;
  private JRadioButton OPT1;
  private JRadioButton OPT2;
  private JRadioButton OPT3;
  private JRadioButton OPT4;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
