
package ri.serien.libecranrpg.sgvm.SGVM66FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2928] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Ventes articles dépréciés /
 * vendeur
 * Indicateur : 01010001 (80 = 0)
 * Titre : Etat des ventes dépreciés par vendeur
 * 
 * [GVM2929] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Ventes d'articles fin de série
 * Indicateur : 01010001 (80 = 1)
 * Titre : Etat des ventes de fin de série par vendeur
 */
public class SGVM66FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private boolean isPlanning = false;
  private String[] cbCodeDate_Value = { "*DAT", "*ENC", "*PRE", };
  private String[] cbCodeDate_Text = { "Date du jour", "Mois en cours", "Mois précédant", };
  private Message LOCTP;
  
  public SGVM66FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    cbCodeDate.removeAllItems();
    for (String texte : cbCodeDate_Text) {
      cbCodeDate.addItem(texte);
    }
    
    // Liée les composants pour les plages de sélection
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snMagasinDebut.lierComposantFin(snMagasinFin);
    
    OPT3.setValeursSelection("1", " ");
    OPT2.setValeursSelection("1", " ");
    OPT1.setValeursSelection("1", " ");
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Indicateur
    Boolean is92 = lexique.isTrue("92");
    Boolean is80 = lexique.isTrue("80");
    
    isPlanning = lexique.isTrue("90");
    
    cbCodeDate.setVisible(isPlanning);
    lbPlanning.setVisible(isPlanning);
    PERDEB.setVisible(!isPlanning);
    PERFIN.setVisible(!isPlanning);
    lbAu.setVisible(!isPlanning);
    if (isPlanning) {
      cbCodeDate.setSelectedItem(lexique.HostFieldGetData("WCPER"));
    }
    
    // Mets le bon titre suivant le pts de menu
    if (is92 && is80) {
      bpPresentation.setText("Etat des ventes de fin de série par vendeur");
    }
    else {
      bpPresentation.setText("Etat des ventes dépreciés par vendeur");
    }
    
    // Visibilité des composants
    PMARS.setVisible(lexique.isPresent("PMARS"));
    PMAR.setVisible(lexique.isPresent("PMAR"));
    lbTauxDepreciationInf.setVisible(lexique.isPresent("PMARS"));
    lbTauxDepreciationSup.setVisible(lexique.isPresent("PMAR"));
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Charger les composants
    chargerMagasin();
    chargerFamille();
    chargerVendeur();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snVendeur.renseignerChampRPG(lexique, "WVDE");
    
    if (isPlanning) {
      lexique.HostFieldPutData("WCPER", 0, cbCodeDate_Value[cbCodeDate.getSelectedIndex()]);
    }
    
    if (snMagasinDebut.getIdSelection() == null && snMagasinFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTMAG", 0, "**");
    }
    else {
      snMagasinDebut.renseignerChampRPG(lexique, "MAGDEB");
      snMagasinFin.renseignerChampRPG(lexique, "MAGFIN");
      lexique.HostFieldPutData("WTMAG", 0, "  ");
    }
    if (snFamilleDebut.getIdSelection() == null && snFamilleFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTFAM", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTFAM", 0, "  ");
      snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
      snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge la liste des vendeurs lors du changement d'etablissement
   */
  private void chargerVendeur() {
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "WVDE");
  }
  
  /**
   * Charge la liste des magasins lors du changement d'etablissement
   */
  private void chargerMagasin() {
    snMagasinDebut.setSession(getSession());
    snMagasinDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinDebut.setTousAutorise(true);
    snMagasinDebut.charger(false);
    snMagasinDebut.setSelectionParChampRPG(lexique, "MAGDEB");
    
    snMagasinFin.setSession(getSession());
    snMagasinFin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinFin.setTousAutorise(true);
    snMagasinFin.charger(false);
    snMagasinFin.setSelectionParChampRPG(lexique, "MAGFIN");
  }
  
  /**
   * Charge la liste des familles lors du changement d'etablissement
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbPeriodeAEditer = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    cbCodeDate = new SNComboBox();
    lbCodeVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbTauxDepreciationSup = new SNLabelChamp();
    PMAR = new XRiTextField();
    lbTauxDepreciationInf = new SNLabelChamp();
    PMARS = new XRiTextField();
    lbSelectionFamille = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbSelectionFamilleFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbSelectionMagasinDebut = new SNLabelChamp();
    snMagasinDebut = new SNMagasin();
    lbSelectionMagasinFin = new SNLabelChamp();
    snMagasinFin = new SNMagasin();
    pnlOption = new SNPanelTitre();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPlanning ----
        lbPlanning.setText("Vous \u00eates en cours de param\u00e9trage en vue de la mise au planning");
        lbPlanning.setMinimumSize(new Dimension(120, 30));
        lbPlanning.setPreferredSize(new Dimension(120, 30));
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriodeAEditer ----
            lbPeriodeAEditer.setText("P\u00e9riode \u00e0 \u00e9diter");
            lbPeriodeAEditer.setMaximumSize(new Dimension(210, 30));
            lbPeriodeAEditer.setPreferredSize(new Dimension(210, 30));
            lbPeriodeAEditer.setMinimumSize(new Dimension(210, 30));
            lbPeriodeAEditer.setName("lbPeriodeAEditer");
            pnlCritereSelection.add(lbPeriodeAEditer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- PERDEB ----
              PERDEB.setPreferredSize(new Dimension(110, 30));
              PERDEB.setMinimumSize(new Dimension(110, 30));
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setName("PERDEB");
              pnlPeriodeAEditer.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlPeriodeAEditer.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERFIN ----
              PERFIN.setMinimumSize(new Dimension(110, 30));
              PERFIN.setPreferredSize(new Dimension(110, 30));
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setName("PERFIN");
              pnlPeriodeAEditer.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbCodeDate ----
              cbCodeDate.setModel(new DefaultComboBoxModel(new String[] { "Date du jour", "D\u00e9but du mois en cours",
                  "Fin du mois en cours", "D\u00e9but du mois pr\u00e9c\u00e9dent", "Fin du mois pr\u00e9c\u00e9dent" }));
              cbCodeDate.setBackground(Color.white);
              cbCodeDate.setFont(new Font("sansserif", Font.PLAIN, 14));
              cbCodeDate.setName("cbCodeDate");
              pnlPeriodeAEditer.add(cbCodeDate, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeVendeur ----
            lbCodeVendeur.setText("Vendeur");
            lbCodeVendeur.setMaximumSize(new Dimension(210, 30));
            lbCodeVendeur.setPreferredSize(new Dimension(210, 30));
            lbCodeVendeur.setMinimumSize(new Dimension(210, 30));
            lbCodeVendeur.setName("lbCodeVendeur");
            pnlCritereSelection.add(lbCodeVendeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snVendeur ----
            snVendeur.setEnabled(false);
            snVendeur.setFont(new Font("sansserif", Font.PLAIN, 14));
            snVendeur.setName("snVendeur");
            pnlCritereSelection.add(snVendeur, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTauxDepreciationSup ----
            lbTauxDepreciationSup.setText("Taux de d\u00e9pr\u00e9ciation sup\u00e9rieur \u00e0");
            lbTauxDepreciationSup.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTauxDepreciationSup.setMaximumSize(new Dimension(210, 30));
            lbTauxDepreciationSup.setPreferredSize(new Dimension(210, 30));
            lbTauxDepreciationSup.setMinimumSize(new Dimension(210, 30));
            lbTauxDepreciationSup.setName("lbTauxDepreciationSup");
            pnlCritereSelection.add(lbTauxDepreciationSup, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- PMAR ----
            PMAR.setFont(new Font("sansserif", Font.PLAIN, 14));
            PMAR.setPreferredSize(new Dimension(50, 30));
            PMAR.setMinimumSize(new Dimension(50, 30));
            PMAR.setName("PMAR");
            pnlCritereSelection.add(PMAR, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTauxDepreciationInf ----
            lbTauxDepreciationInf.setText("inf\u00e9rieur \u00e0");
            lbTauxDepreciationInf.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTauxDepreciationInf.setPreferredSize(new Dimension(64, 30));
            lbTauxDepreciationInf.setMinimumSize(new Dimension(64, 30));
            lbTauxDepreciationInf.setMaximumSize(new Dimension(64, 30));
            lbTauxDepreciationInf.setName("lbTauxDepreciationInf");
            pnlCritereSelection.add(lbTauxDepreciationInf, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- PMARS ----
            PMARS.setFont(new Font("sansserif", Font.PLAIN, 14));
            PMARS.setMinimumSize(new Dimension(50, 30));
            PMARS.setPreferredSize(new Dimension(50, 30));
            PMARS.setName("PMARS");
            pnlCritereSelection.add(PMARS, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSelectionFamille ----
            lbSelectionFamille.setText("Famille de d\u00e9but");
            lbSelectionFamille.setMaximumSize(new Dimension(210, 30));
            lbSelectionFamille.setPreferredSize(new Dimension(210, 30));
            lbSelectionFamille.setMinimumSize(new Dimension(210, 30));
            lbSelectionFamille.setName("lbSelectionFamille");
            pnlCritereSelection.add(lbSelectionFamille, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereSelection.add(snFamilleDebut, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSelectionFamilleFin ----
            lbSelectionFamilleFin.setText("Famille de fin");
            lbSelectionFamilleFin.setPreferredSize(new Dimension(210, 30));
            lbSelectionFamilleFin.setMinimumSize(new Dimension(210, 30));
            lbSelectionFamilleFin.setMaximumSize(new Dimension(210, 30));
            lbSelectionFamilleFin.setName("lbSelectionFamilleFin");
            pnlCritereSelection.add(lbSelectionFamilleFin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleFin ----
            snFamilleFin.setName("snFamilleFin");
            pnlCritereSelection.add(snFamilleFin, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSelectionMagasinDebut ----
            lbSelectionMagasinDebut.setText("Magasin de d\u00e9but");
            lbSelectionMagasinDebut.setMaximumSize(new Dimension(210, 30));
            lbSelectionMagasinDebut.setPreferredSize(new Dimension(210, 30));
            lbSelectionMagasinDebut.setMinimumSize(new Dimension(210, 30));
            lbSelectionMagasinDebut.setName("lbSelectionMagasinDebut");
            pnlCritereSelection.add(lbSelectionMagasinDebut, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasinDebut ----
            snMagasinDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasinDebut.setName("snMagasinDebut");
            pnlCritereSelection.add(snMagasinDebut, new GridBagConstraints(1, 5, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSelectionMagasinFin ----
            lbSelectionMagasinFin.setText("Magasin de fin");
            lbSelectionMagasinFin.setMaximumSize(new Dimension(210, 30));
            lbSelectionMagasinFin.setPreferredSize(new Dimension(210, 30));
            lbSelectionMagasinFin.setMinimumSize(new Dimension(210, 30));
            lbSelectionMagasinFin.setName("lbSelectionMagasinFin");
            pnlCritereSelection.add(lbSelectionMagasinFin, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasinFin ----
            snMagasinFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasinFin.setName("snMagasinFin");
            pnlCritereSelection.add(snMagasinFin, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOption ========
          {
            pnlOption.setOpaque(false);
            pnlOption.setTitre("Bons \u00e0 \u00e9diter");
            pnlOption.setName("pnlOption");
            pnlOption.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOption.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOption.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOption.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOption.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OPT1 ----
            OPT1.setText("Bon valid\u00e9s non exp\u00e9di\u00e9s (VAL)");
            OPT1.setComponentPopupMenu(null);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setPreferredSize(new Dimension(224, 30));
            OPT1.setMinimumSize(new Dimension(224, 30));
            OPT1.setName("OPT1");
            pnlOption.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
            OPT2.setComponentPopupMenu(null);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setMinimumSize(new Dimension(239, 30));
            OPT2.setPreferredSize(new Dimension(239, 30));
            OPT2.setName("OPT2");
            pnlOption.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Bons factur\u00e9s (FAC)");
            OPT3.setComponentPopupMenu(null);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setPreferredSize(new Dimension(151, 30));
            OPT3.setMinimumSize(new Dimension(151, 30));
            OPT3.setName("OPT3");
            pnlOption.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlOption, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNMessage lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbPeriodeAEditer;
  private SNPanel pnlPeriodeAEditer;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNComboBox cbCodeDate;
  private SNLabelChamp lbCodeVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbTauxDepreciationSup;
  private XRiTextField PMAR;
  private SNLabelChamp lbTauxDepreciationInf;
  private XRiTextField PMARS;
  private SNLabelChamp lbSelectionFamille;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbSelectionFamilleFin;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSelectionMagasinDebut;
  private SNMagasin snMagasinDebut;
  private SNLabelChamp lbSelectionMagasinFin;
  private SNMagasin snMagasinFin;
  private SNPanelTitre pnlOption;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
