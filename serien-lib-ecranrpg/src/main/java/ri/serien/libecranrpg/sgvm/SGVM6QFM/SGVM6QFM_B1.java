
package ri.serien.libecranrpg.sgvm.SGVM6QFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM6QFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM6QFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    WMAG_ck.setVisible(lexique.isPresent("WMAG"));
    WMAG_ck.setEnabled(WMAG_ck.isVisible());
    WMAG_ck.setSelected(lexique.HostFieldGetData("WMAG").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!WMAG_ck.isSelected() & WMAG_ck.isVisible());
    
    WVDE_ck.setVisible(lexique.isPresent("WVDE"));
    WVDE_ck.setEnabled(WVDE_ck.isVisible());
    WVDE_ck.setSelected(lexique.HostFieldGetData("WVDE").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WVDE_ck.isSelected() & WVDE_ck.isVisible());
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (WMAG_ck.isSelected()) {
      lexique.HostFieldPutData("WMAG", 0, "**");
    }
    if (WVDE_ck.isSelected()) {
      lexique.HostFieldPutData("WVDE", 0, "**");
    }
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WMAGActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
    if (!WMAG_ck.isSelected() & WMAG.getText().equalsIgnoreCase("**")) {
      WMAG.setText("");
    }
  }
  
  private void WVDEActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!WVDE_ck.isSelected() & WVDE.getText().trim().equalsIgnoreCase("**")) {
      WVDE.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_37 = new JXTitledSeparator();
    OBJ_38 = new JXTitledSeparator();
    OBJ_41 = new JXTitledSeparator();
    OBJ_42 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    OBJ_46 = new JXTitledSeparator();
    OBJ_68 = new JLabel();
    WAV01 = new XRiTextField();
    WAV02 = new XRiTextField();
    WAV03 = new XRiTextField();
    WAV04 = new XRiTextField();
    WAV05 = new XRiTextField();
    WAV06 = new XRiTextField();
    WAV07 = new XRiTextField();
    WAV08 = new XRiTextField();
    WAV09 = new XRiTextField();
    panel1 = new JPanel();
    OBJ_67 = new JLabel();
    OBJ_66 = new JLabel();
    DATD = new XRiCalendrier();
    DATF = new XRiCalendrier();
    panel2 = new JPanel();
    WMAG_ck = new JCheckBox();
    P_SEL1 = new JPanel();
    OBJ_69 = new JLabel();
    WMAG = new XRiTextField();
    panel3 = new JPanel();
    WVDE_ck = new JCheckBox();
    P_SEL0 = new JPanel();
    OBJ_70 = new JLabel();
    WVDE = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(520, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_37 ----
          OBJ_37.setTitle("Type d'avoir \u00e0 s\u00e9lectionner");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_38 ----
          OBJ_38.setTitle("");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_41 ----
          OBJ_41.setTitle("");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_42 ----
          OBJ_42.setTitle("");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_43 ----
          OBJ_43.setTitle("");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_46 ----
          OBJ_46.setTitle("P\u00e9riode \u00e0 traiter");
          OBJ_46.setName("OBJ_46");

          //---- OBJ_68 ----
          OBJ_68.setText("Code avoir");
          OBJ_68.setName("OBJ_68");

          //---- WAV01 ----
          WAV01.setComponentPopupMenu(BTD);
          WAV01.setName("WAV01");

          //---- WAV02 ----
          WAV02.setComponentPopupMenu(BTD);
          WAV02.setName("WAV02");

          //---- WAV03 ----
          WAV03.setComponentPopupMenu(BTD);
          WAV03.setName("WAV03");

          //---- WAV04 ----
          WAV04.setComponentPopupMenu(BTD);
          WAV04.setName("WAV04");

          //---- WAV05 ----
          WAV05.setComponentPopupMenu(BTD);
          WAV05.setName("WAV05");

          //---- WAV06 ----
          WAV06.setComponentPopupMenu(BTD);
          WAV06.setName("WAV06");

          //---- WAV07 ----
          WAV07.setComponentPopupMenu(BTD);
          WAV07.setName("WAV07");

          //---- WAV08 ----
          WAV08.setComponentPopupMenu(BTD);
          WAV08.setName("WAV08");

          //---- WAV09 ----
          WAV09.setComponentPopupMenu(BTD);
          WAV09.setName("WAV09");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_67 ----
            OBJ_67.setText("\u00e0");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(270, 10, 11, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("de");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(25, 14, 19, 20);

            //---- DATD ----
            DATD.setName("DATD");
            panel1.add(DATD);
            DATD.setBounds(70, 10, 105, DATD.getPreferredSize().height);

            //---- DATF ----
            DATF.setName("DATF");
            panel1.add(DATF);
            DATF.setBounds(325, 10, 105, DATF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WMAG_ck ----
            WMAG_ck.setText("s\u00e9lection compl\u00e8te");
            WMAG_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WMAG_ck.setName("WMAG_ck");
            WMAG_ck.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WMAGActionPerformed(e);
              }
            });
            panel2.add(WMAG_ck);
            WMAG_ck.setBounds(5, 15, 130, 20);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_69 ----
              OBJ_69.setText("Code magasin");
              OBJ_69.setComponentPopupMenu(BTD);
              OBJ_69.setName("OBJ_69");
              P_SEL1.add(OBJ_69);
              OBJ_69.setBounds(15, 9, 94, 20);

              //---- WMAG ----
              WMAG.setComponentPopupMenu(BTD);
              WMAG.setName("WMAG");
              P_SEL1.add(WMAG);
              WMAG.setBounds(140, 5, 34, WMAG.getPreferredSize().height);
            }
            panel2.add(P_SEL1);
            P_SEL1.setBounds(165, 5, 260, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WVDE_ck ----
            WVDE_ck.setText("s\u00e9lection compl\u00e8te");
            WVDE_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WVDE_ck.setName("WVDE_ck");
            WVDE_ck.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WVDEActionPerformed(e);
              }
            });
            panel3.add(WVDE_ck);
            WVDE_ck.setBounds(5, 15, 130, 20);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_70 ----
              OBJ_70.setText("Code vendeur");
              OBJ_70.setComponentPopupMenu(BTD);
              OBJ_70.setName("OBJ_70");
              P_SEL0.add(OBJ_70);
              OBJ_70.setBounds(15, 9, 88, 20);

              //---- WVDE ----
              WVDE.setComponentPopupMenu(BTD);
              WVDE.setName("WVDE");
              P_SEL0.add(WVDE);
              WVDE.setBounds(140, 5, 44, WVDE.getPreferredSize().height);
            }
            panel3.add(P_SEL0);
            P_SEL0.setBounds(165, 5, 260, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(WAV01, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV02, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV03, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV04, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV05, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV06, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV07, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV08, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WAV09, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WAV01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WAV09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_37;
  private JXTitledSeparator OBJ_38;
  private JXTitledSeparator OBJ_41;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_43;
  private JXTitledSeparator OBJ_46;
  private JLabel OBJ_68;
  private XRiTextField WAV01;
  private XRiTextField WAV02;
  private XRiTextField WAV03;
  private XRiTextField WAV04;
  private XRiTextField WAV05;
  private XRiTextField WAV06;
  private XRiTextField WAV07;
  private XRiTextField WAV08;
  private XRiTextField WAV09;
  private JPanel panel1;
  private JLabel OBJ_67;
  private JLabel OBJ_66;
  private XRiCalendrier DATD;
  private XRiCalendrier DATF;
  private JPanel panel2;
  private JCheckBox WMAG_ck;
  private JPanel P_SEL1;
  private JLabel OBJ_69;
  private XRiTextField WMAG;
  private JPanel panel3;
  private JCheckBox WVDE_ck;
  private JPanel P_SEL0;
  private JLabel OBJ_70;
  private XRiTextField WVDE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
