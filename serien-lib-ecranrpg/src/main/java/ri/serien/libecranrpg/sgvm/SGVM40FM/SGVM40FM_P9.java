
package ri.serien.libecranrpg.sgvm.SGVM40FM;
// Nom Fichier: pop_SGVM40FM_FMTP9_FMTF1_554.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM40FM_P9 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM40FM_P9(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NFACP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_6.setVisible(lexique.isPresent("NFACP"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JLabel();
    OBJ_7 = new JLabel();
    OBJ_4 = new JButton();
    OBJ_6 = new JLabel();
    P_PnlOpts = new JPanel();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_5 ----
    OBJ_5.setText("Traitement termin\u00e9");
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(141, 28, 154, 20);

    //---- OBJ_7 ----
    OBJ_7.setText("Nombre de Factures");
    OBJ_7.setName("OBJ_7");
    add(OBJ_7);
    OBJ_7.setBounds(122, 62, 126, 20);

    //---- OBJ_4 ----
    OBJ_4.setText("");
    OBJ_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_4.setName("OBJ_4");
    add(OBJ_4);
    OBJ_4.setBounds(30, 19, 54, 44);

    //---- OBJ_6 ----
    OBJ_6.setText("@NFACP@");
    OBJ_6.setName("OBJ_6");
    add(OBJ_6);
    OBJ_6.setBounds(268, 63, 48, 18);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }
    add(P_PnlOpts);
    P_PnlOpts.setBounds(400, 15, 55, 516);

    setPreferredSize(new Dimension(364, 108));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_5;
  private JLabel OBJ_7;
  private JButton OBJ_4;
  private JLabel OBJ_6;
  private JPanel P_PnlOpts;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
