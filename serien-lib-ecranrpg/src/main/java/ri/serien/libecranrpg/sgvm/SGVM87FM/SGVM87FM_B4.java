
package ri.serien.libecranrpg.sgvm.SGVM87FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

public class SGVM87FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM87FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    EXCLU3.setValeursSelection("X", " ");
    EXCLU2.setValeursSelection("X", " ");
    EXCLU1.setValeursSelection("X", " ");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Exclusions"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlOptionsExclusion = new SNPanelTitre();
    EXCLU1 = new XRiCheckBox();
    EXCLU2 = new XRiCheckBox();
    EXCLU3 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(450, 170));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

      //======== pnlOptionsExclusion ========
      {
        pnlOptionsExclusion.setOpaque(false);
        pnlOptionsExclusion.setTitre("Option d'exclusion");
        pnlOptionsExclusion.setName("pnlOptionsExclusion");
        pnlOptionsExclusion.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlOptionsExclusion.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlOptionsExclusion.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlOptionsExclusion.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
        ((GridBagLayout)pnlOptionsExclusion.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- EXCLU1 ----
        EXCLU1.setText("D\u00e9partements d'Outre-Mer");
        EXCLU1.setComponentPopupMenu(null);
        EXCLU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXCLU1.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXCLU1.setMaximumSize(new Dimension(200, 30));
        EXCLU1.setMinimumSize(new Dimension(200, 30));
        EXCLU1.setPreferredSize(new Dimension(200, 30));
        EXCLU1.setName("EXCLU1");
        pnlOptionsExclusion.add(EXCLU1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- EXCLU2 ----
        EXCLU2.setText("Des bons de type export");
        EXCLU2.setComponentPopupMenu(null);
        EXCLU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXCLU2.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXCLU2.setMaximumSize(new Dimension(200, 30));
        EXCLU2.setMinimumSize(new Dimension(200, 30));
        EXCLU2.setPreferredSize(new Dimension(200, 30));
        EXCLU2.setName("EXCLU2");
        pnlOptionsExclusion.add(EXCLU2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- EXCLU3 ----
        EXCLU3.setText("Des bons de type CEE");
        EXCLU3.setComponentPopupMenu(null);
        EXCLU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXCLU3.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXCLU3.setMaximumSize(new Dimension(200, 30));
        EXCLU3.setMinimumSize(new Dimension(200, 30));
        EXCLU3.setPreferredSize(new Dimension(200, 30));
        EXCLU3.setName("EXCLU3");
        pnlOptionsExclusion.add(EXCLU3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlOptionsExclusion, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlOptionsExclusion;
  private XRiCheckBox EXCLU1;
  private XRiCheckBox EXCLU2;
  private XRiCheckBox EXCLU3;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
