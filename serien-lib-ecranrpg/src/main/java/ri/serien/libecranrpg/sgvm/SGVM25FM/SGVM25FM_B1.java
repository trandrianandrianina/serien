
package ri.serien.libecranrpg.sgvm.SGVM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**

 */
public class SGVM25FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String BOUTON_TRI = "Trier l'édition";
  private Message WTYPI = null;
  
  public SGVM25FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    WSER.setValeursSelection("OUI", "NON");
    EDTCTM.setValeursSelection("OUI", "NON");
    WFOR.setValeursSelection("OUI", "NON");
    WRED.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_TRI, 't', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbWTYPI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTYPI@")).trim());
    tfDateEtablissement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Fonctions diverses après initialisation des données
    setDiverses();
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Initialsie WCHOIX afin de pas etre en mode tri à l'entrée du programme
    lexique.HostFieldPutData("WCHOIX", 0, " ");
    
    // Indicateurs
    Boolean isPlageDevis = lexique.isTrue("36");
    Boolean isDateLivraison = lexique.isTrue("37");
    
    // Visibilité des composants
    tfDateEtablissement.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfDateEtablissement.isVisible());
    lbNbrExemplaire.setVisible(WNEX.isVisible());
    
    // Gestion de WTYPI
    pnlMission.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    WTYPI = WTYPI.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbWTYPI.setMessage(WTYPI);
    
    // Indique quand afficher les libelle des date de livraison
    if (isDateLivraison) {
      lbDateLivraison.setVisible(true);
      lbAu.setVisible(true);
    }
    else {
      lbDateLivraison.setVisible(false);
      lbAu.setVisible(false);
    }
    
    // Indique le bon libellé suivant le point de menu
    if (isPlageDevis) {
      lbPlageDe.setText("Plage de devis de");
    }
    else {
      lbPlageDe.setText("Plage de documents de");
    }
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Logo
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    chargerComposantMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    if (WNDEB.getText().isEmpty() && WNFIN.getText().isEmpty()) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
    }
  }
  
  // charge le composant suivant l'etablissement
  public void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostFieldPutData("WCHOIX", 0, " ");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_TRI)) {
        lexique.HostFieldPutData("WCHOIX", 0, "X");
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void sNEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantMagasin();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMission = new SNPanel();
    lbWTYPI = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbDateLivraison = new SNLabelChamp();
    pnlDateDeLivraison = new SNPanel();
    WDATDX = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    WDATFX = new XRiCalendrier();
    lbPlageDe = new SNLabelChamp();
    pnlPlageDe = new SNPanel();
    pnlPLageDeInfo = new JPanel();
    WNDEB = new XRiTextField();
    WNDEBS = new XRiTextField();
    lbA = new SNLabelChamp();
    WNFIN = new XRiTextField();
    WNFINS = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionner = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEtablissement = new SNTexte();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlOptions = new SNPanelTitre();
    lbNbrExemplaire = new SNLabelChamp();
    WNEX = new XRiTextField();
    WRED = new XRiCheckBox();
    WFOR = new XRiCheckBox();
    WSER = new XRiCheckBox();
    EDTCTM = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMission ========
      {
        pnlMission.setPreferredSize(new Dimension(120, 30));
        pnlMission.setName("pnlMission");
        pnlMission.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMission.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMission.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMission.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMission.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbWTYPI ----
        lbWTYPI.setText("@WTYPI@");
        lbWTYPI.setPreferredSize(new Dimension(120, 30));
        lbWTYPI.setHorizontalTextPosition(SwingConstants.LEADING);
        lbWTYPI.setName("lbWTYPI");
        pnlMission.add(lbWTYPI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMission, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setPreferredSize(new Dimension(700, 520));
        pnlColonne.setBackground(new Color(239, 239, 222));
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateLivraison ----
            lbDateLivraison.setText("Date de livraison pr\u00e9vue du");
            lbDateLivraison.setPreferredSize(new Dimension(175, 30));
            lbDateLivraison.setMinimumSize(new Dimension(175, 30));
            lbDateLivraison.setName("lbDateLivraison");
            pnlCritereSelection.add(lbDateLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateDeLivraison ========
            {
              pnlDateDeLivraison.setName("pnlDateDeLivraison");
              pnlDateDeLivraison.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDATDX ----
              WDATDX.setPreferredSize(new Dimension(110, 30));
              WDATDX.setMinimumSize(new Dimension(110, 30));
              WDATDX.setMaximumSize(new Dimension(120, 28));
              WDATDX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATDX.setName("WDATDX");
              pnlDateDeLivraison.add(WDATDX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlDateDeLivraison.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDATFX ----
              WDATFX.setPreferredSize(new Dimension(110, 30));
              WDATFX.setMinimumSize(new Dimension(110, 30));
              WDATFX.setMaximumSize(new Dimension(120, 28));
              WDATFX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATFX.setName("WDATFX");
              pnlDateDeLivraison.add(WDATFX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlDateDeLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPlageDe ----
            lbPlageDe.setText("Plage de [voir code]");
            lbPlageDe.setMaximumSize(new Dimension(225, 30));
            lbPlageDe.setPreferredSize(new Dimension(175, 30));
            lbPlageDe.setMinimumSize(new Dimension(175, 30));
            lbPlageDe.setName("lbPlageDe");
            pnlCritereSelection.add(lbPlageDe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlPlageDe ========
            {
              pnlPlageDe.setName("pnlPlageDe");
              pnlPlageDe.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPlageDe.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlPlageDe.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPlageDe.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPlageDe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ======== pnlPLageDeInfo ========
              {
                pnlPLageDeInfo.setOpaque(false);
                pnlPLageDeInfo.setName("pnlPLageDeInfo");
                pnlPLageDeInfo.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).columnWeights = new double[] { 1.0, 1.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- WNDEB ----
                WNDEB.setComponentPopupMenu(pmBTD);
                WNDEB.setMinimumSize(new Dimension(70, 30));
                WNDEB.setPreferredSize(new Dimension(70, 30));
                WNDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNDEB.setMaximumSize(new Dimension(70, 30));
                WNDEB.setName("WNDEB");
                pnlPLageDeInfo.add(WNDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WNDEBS ----
                WNDEBS.setComponentPopupMenu(pmBTD);
                WNDEBS.setPreferredSize(new Dimension(24, 30));
                WNDEBS.setMinimumSize(new Dimension(24, 30));
                WNDEBS.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNDEBS.setName("WNDEBS");
                pnlPLageDeInfo.add(WNDEBS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbA ----
                lbA.setText("\u00e0");
                lbA.setPreferredSize(new Dimension(8, 30));
                lbA.setMinimumSize(new Dimension(8, 30));
                lbA.setMaximumSize(new Dimension(8, 30));
                lbA.setName("lbA");
                pnlPLageDeInfo.add(lbA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WNFIN ----
                WNFIN.setComponentPopupMenu(pmBTD);
                WNFIN.setMinimumSize(new Dimension(70, 30));
                WNFIN.setPreferredSize(new Dimension(70, 30));
                WNFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNFIN.setMaximumSize(new Dimension(70, 30));
                WNFIN.setName("WNFIN");
                pnlPLageDeInfo.add(WNFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WNFINS ----
                WNFINS.setComponentPopupMenu(pmBTD);
                WNFINS.setMinimumSize(new Dimension(24, 30));
                WNFINS.setPreferredSize(new Dimension(24, 30));
                WNFINS.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNFINS.setName("WNFINS");
                pnlPLageDeInfo.add(WNFINS, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPlageDe.add(pnlPLageDeInfo, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlPlageDe, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionner ========
          {
            pnlEtablissementSelectionner.setTitre("Etablissement et magasin");
            pnlEtablissementSelectionner.setName("pnlEtablissementSelectionner");
            pnlEtablissementSelectionner.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionner.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                sNEtablissementValueChanged(e);
              }
            });
            pnlEtablissementSelectionner.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionner.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfDateEtablissement ----
            tfDateEtablissement.setText("@WENCX@");
            tfDateEtablissement.setEditable(false);
            tfDateEtablissement.setPreferredSize(new Dimension(264, 30));
            tfDateEtablissement.setMinimumSize(new Dimension(264, 30));
            tfDateEtablissement.setEnabled(false);
            tfDateEtablissement.setName("tfDateEtablissement");
            pnlEtablissementSelectionner.add(tfDateEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlEtablissementSelectionner.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setEnabled(false);
            snMagasin.setName("snMagasin");
            pnlEtablissementSelectionner.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionner, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptions ========
          {
            pnlOptions.setOpaque(false);
            pnlOptions.setTitre("Options d'\u00e9dition");
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNbrExemplaire ----
            lbNbrExemplaire.setText("Nombre d'exemplaires");
            lbNbrExemplaire.setName("lbNbrExemplaire");
            pnlOptions.add(lbNbrExemplaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WNEX ----
            WNEX.setComponentPopupMenu(pmBTD);
            WNEX.setPreferredSize(new Dimension(24, 30));
            WNEX.setMinimumSize(new Dimension(24, 30));
            WNEX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNEX.setName("WNEX");
            pnlOptions.add(WNEX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WRED ----
            WRED.setText("R\u00e9\u00e9dition");
            WRED.setComponentPopupMenu(pmBTD);
            WRED.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WRED.setFont(new Font("sansserif", Font.PLAIN, 14));
            WRED.setPreferredSize(new Dimension(82, 30));
            WRED.setMinimumSize(new Dimension(82, 30));
            WRED.setName("WRED");
            pnlOptions.add(WRED, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WFOR ----
            WFOR.setText("For\u00e7age");
            WFOR.setComponentPopupMenu(pmBTD);
            WFOR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WFOR.setFont(new Font("sansserif", Font.PLAIN, 14));
            WFOR.setMinimumSize(new Dimension(75, 30));
            WFOR.setPreferredSize(new Dimension(75, 30));
            WFOR.setName("WFOR");
            pnlOptions.add(WFOR, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WSER ----
            WSER.setText("Edition des num\u00e9ros de s\u00e9rie");
            WSER.setComponentPopupMenu(pmBTD);
            WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSER.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSER.setPreferredSize(new Dimension(207, 30));
            WSER.setMinimumSize(new Dimension(207, 30));
            WSER.setName("WSER");
            pnlOptions.add(WSER, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EDTCTM ----
            EDTCTM.setText("Edition contr\u00f4le de marge");
            EDTCTM.setComponentPopupMenu(pmBTD);
            EDTCTM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTCTM.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTCTM.setMinimumSize(new Dimension(185, 30));
            EDTCTM.setPreferredSize(new Dimension(185, 30));
            EDTCTM.setName("EDTCTM");
            pnlOptions.add(EDTCTM, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMission;
  private SNLabelTitre lbWTYPI;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbDateLivraison;
  private SNPanel pnlDateDeLivraison;
  private XRiCalendrier WDATDX;
  private SNLabelChamp lbAu;
  private XRiCalendrier WDATFX;
  private SNLabelChamp lbPlageDe;
  private SNPanel pnlPlageDe;
  private JPanel pnlPLageDeInfo;
  private XRiTextField WNDEB;
  private XRiTextField WNDEBS;
  private SNLabelChamp lbA;
  private XRiTextField WNFIN;
  private XRiTextField WNFINS;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionner;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlOptions;
  private SNLabelChamp lbNbrExemplaire;
  private XRiTextField WNEX;
  private XRiCheckBox WRED;
  private XRiCheckBox WFOR;
  private XRiCheckBox WSER;
  private XRiCheckBox EDTCTM;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
