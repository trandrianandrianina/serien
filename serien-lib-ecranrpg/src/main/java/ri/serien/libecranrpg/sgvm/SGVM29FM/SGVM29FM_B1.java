
package ri.serien.libecranrpg.sgvm.SGVM29FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM29FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORT = "Exportation tableur";
  
  public SGVM29FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORT, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Ajout
    initDiverses();
    OPT4.setValeursSelection("X", " ");
    OPT3.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT1.setValeursSelection("X", " ");
    TOUBON.setValeursSelection("**", "  ");
    OPT5.setValeursSelection("X", " ");
    WTRI.setValeursSelection("O", "N");
    WTOU.setValeursSelection("**", "  ");
    WTOP.setValeursSelection("O", "N");
    WDU.setValeursSelection("O", "N");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    pnlSelectionPlageBons.setVisible(!WTOU.isSelected());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    chargerComposantDevise();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snDevise.renseignerChampRPG(lexique, "BONDEV");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORT)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // Charger composant devise
  private void chargerComposantDevise() {
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(snEtablissement.getIdSelection());
    snDevise.setTousAutorise(true);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "BONDEV");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantDevise();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    pnlSelectionPlageBons.setVisible(!pnlSelectionPlageBons.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlPlageBonsTraiter = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    pnlSelectionPlageBons = new SNPanel();
    lbPremierBon = new SNLabelChamp();
    NUMDEB = new XRiTextField();
    lbDernierBon = new SNLabelChamp();
    NUMFIN = new XRiTextField();
    pnlDevise = new SNPanelTitre();
    pnlSelectionDevise = new SNPanel();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    pnlNatureMontant = new SNPanel();
    lbNatureMontant = new SNLabelChamp();
    NATLOG = new XRiComboBox();
    pnlOptionsSelection = new SNPanelTitre();
    TOUBON = new XRiCheckBox();
    OPT5 = new XRiCheckBox();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    OPT4 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionsEdition = new SNPanelTitre();
    WTOP = new XRiCheckBox();
    WTRI = new XRiCheckBox();
    WDU = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1070, 660));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout(1, 2));
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlPlageBonsTraiter ========
        {
          pnlPlageBonsTraiter.setTitre("Plage des bons \u00e0 traiter");
          pnlPlageBonsTraiter.setName("pnlPlageBonsTraiter");
          pnlPlageBonsTraiter.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPlageBonsTraiter.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlPlageBonsTraiter.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPlageBonsTraiter.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPlageBonsTraiter.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMaximumSize(new Dimension(142, 30));
          WTOU.setMinimumSize(new Dimension(142, 30));
          WTOU.setPreferredSize(new Dimension(142, 30));
          WTOU.setName("WTOU");
          WTOU.addItemListener(e -> WTOUItemStateChanged(e));
          pnlPlageBonsTraiter.add(WTOU, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlSelectionPlageBons ========
          {
            pnlSelectionPlageBons.setName("pnlSelectionPlageBons");
            pnlSelectionPlageBons.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSelectionPlageBons.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlSelectionPlageBons.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlSelectionPlageBons.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlSelectionPlageBons.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPremierBon ----
            lbPremierBon.setText("Num\u00e9ro du premier bon");
            lbPremierBon.setMaximumSize(new Dimension(200, 30));
            lbPremierBon.setMinimumSize(new Dimension(200, 30));
            lbPremierBon.setPreferredSize(new Dimension(200, 30));
            lbPremierBon.setName("lbPremierBon");
            pnlSelectionPlageBons.add(lbPremierBon, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- NUMDEB ----
            NUMDEB.setMinimumSize(new Dimension(70, 30));
            NUMDEB.setPreferredSize(new Dimension(70, 30));
            NUMDEB.setName("NUMDEB");
            pnlSelectionPlageBons.add(NUMDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDernierBon ----
            lbDernierBon.setText("Num\u00e9ro du dernier bon");
            lbDernierBon.setName("lbDernierBon");
            pnlSelectionPlageBons.add(lbDernierBon, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- NUMFIN ----
            NUMFIN.setName("NUMFIN");
            pnlSelectionPlageBons.add(NUMFIN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageBonsTraiter.add(pnlSelectionPlageBons, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageBonsTraiter, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDevise ========
        {
          pnlDevise.setTitre("Devise \u00e0 traiter");
          pnlDevise.setName("pnlDevise");
          pnlDevise.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDevise.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDevise.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDevise.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDevise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlSelectionDevise ========
          {
            pnlSelectionDevise.setName("pnlSelectionDevise");
            pnlSelectionDevise.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSelectionDevise.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlSelectionDevise.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlSelectionDevise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlSelectionDevise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbDevise ----
            lbDevise.setText("Code devise");
            lbDevise.setName("lbDevise");
            pnlSelectionDevise.add(lbDevise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snDevise ----
            snDevise.setName("snDevise");
            pnlSelectionDevise.add(snDevise, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDevise.add(pnlSelectionDevise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlDevise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlNatureMontant ========
        {
          pnlNatureMontant.setName("pnlNatureMontant");
          pnlNatureMontant.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlNatureMontant.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlNatureMontant.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlNatureMontant.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlNatureMontant.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbNatureMontant ----
          lbNatureMontant.setText("Nature du montant");
          lbNatureMontant.setName("lbNatureMontant");
          pnlNatureMontant.add(lbNatureMontant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- NATLOG ----
          NATLOG.setModel(new DefaultComboBoxModel<>(new String[] { "HT ", "TTC" }));
          NATLOG.setName("NATLOG");
          pnlNatureMontant.add(NATLOG, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlNatureMontant, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOptionsSelection ========
        {
          pnlOptionsSelection.setTitre("S\u00e9lection de l'\u00e9tat des bons \u00e0 traiter");
          pnlOptionsSelection.setName("pnlOptionsSelection");
          pnlOptionsSelection.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptionsSelection.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlOptionsSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlOptionsSelection.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOptionsSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- TOUBON ----
          TOUBON.setText("Bons en attente, valid\u00e9s, exp\u00e9di\u00e9s et factur\u00e9s (ATT + VAL + EXP + FAC)");
          TOUBON.setFont(new Font("sansserif", Font.PLAIN, 14));
          TOUBON.setMaximumSize(new Dimension(267, 30));
          TOUBON.setMinimumSize(new Dimension(267, 30));
          TOUBON.setPreferredSize(new Dimension(267, 30));
          TOUBON.setName("TOUBON");
          pnlOptionsSelection.add(TOUBON, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OPT5 ----
          OPT5.setText("Bons en attente de confirmation (POS)");
          OPT5.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT5.setMaximumSize(new Dimension(267, 30));
          OPT5.setMinimumSize(new Dimension(267, 30));
          OPT5.setPreferredSize(new Dimension(267, 30));
          OPT5.setName("OPT5");
          pnlOptionsSelection.add(OPT5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OPT1 ----
          OPT1.setText("Bons en attente (ATT)");
          OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT1.setMaximumSize(new Dimension(267, 30));
          OPT1.setMinimumSize(new Dimension(267, 30));
          OPT1.setPreferredSize(new Dimension(267, 30));
          OPT1.setName("OPT1");
          pnlOptionsSelection.add(OPT1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OPT2 ----
          OPT2.setText("Bons valid\u00e9s non exp\u00e9di\u00e9s (VAL)");
          OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT2.setMaximumSize(new Dimension(267, 30));
          OPT2.setMinimumSize(new Dimension(267, 30));
          OPT2.setPreferredSize(new Dimension(267, 30));
          OPT2.setName("OPT2");
          pnlOptionsSelection.add(OPT2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OPT3 ----
          OPT3.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
          OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT3.setMaximumSize(new Dimension(267, 30));
          OPT3.setMinimumSize(new Dimension(267, 30));
          OPT3.setPreferredSize(new Dimension(267, 30));
          OPT3.setName("OPT3");
          pnlOptionsSelection.add(OPT3, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OPT4 ----
          OPT4.setText("Bons valid\u00e9s non confirm\u00e9s (HNC)");
          OPT4.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT4.setMaximumSize(new Dimension(267, 30));
          OPT4.setMinimumSize(new Dimension(267, 30));
          OPT4.setPreferredSize(new Dimension(267, 30));
          OPT4.setName("OPT4");
          pnlOptionsSelection.add(OPT4, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlOptionsSelection, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setEditable(false);
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOptionsEdition ========
        {
          pnlOptionsEdition.setTitre("Option d'\u00e9dition");
          pnlOptionsEdition.setName("pnlOptionsEdition");
          pnlOptionsEdition.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlOptionsEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOptionsEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- WTOP ----
          WTOP.setText("Edition des bons verrouill\u00e9s");
          WTOP.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOP.setMaximumSize(new Dimension(199, 30));
          WTOP.setMinimumSize(new Dimension(199, 30));
          WTOP.setPreferredSize(new Dimension(199, 30));
          WTOP.setName("WTOP");
          pnlOptionsEdition.add(WTOP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- WTRI ----
          WTRI.setText("Edition tri\u00e9e par date de livraison");
          WTRI.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTRI.setMaximumSize(new Dimension(199, 30));
          WTRI.setMinimumSize(new Dimension(199, 30));
          WTRI.setPreferredSize(new Dimension(199, 30));
          WTRI.setName("WTRI");
          pnlOptionsEdition.add(WTRI, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- WDU ----
          WDU.setText("Direct usine uniquement");
          WDU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDU.setMaximumSize(new Dimension(199, 30));
          WDU.setMinimumSize(new Dimension(199, 30));
          WDU.setPreferredSize(new Dimension(199, 30));
          WDU.setName("WDU");
          pnlOptionsEdition.add(WDU, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptionsEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPlageBonsTraiter;
  private XRiCheckBox WTOU;
  private SNPanel pnlSelectionPlageBons;
  private SNLabelChamp lbPremierBon;
  private XRiTextField NUMDEB;
  private SNLabelChamp lbDernierBon;
  private XRiTextField NUMFIN;
  private SNPanelTitre pnlDevise;
  private SNPanel pnlSelectionDevise;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNPanel pnlNatureMontant;
  private SNLabelChamp lbNatureMontant;
  private XRiComboBox NATLOG;
  private SNPanelTitre pnlOptionsSelection;
  private XRiCheckBox TOUBON;
  private XRiCheckBox OPT5;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private XRiCheckBox OPT4;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionsEdition;
  private XRiCheckBox WTOP;
  private XRiCheckBox WTRI;
  private XRiCheckBox WDU;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
