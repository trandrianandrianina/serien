
package ri.serien.libecranrpg.sgvm.SGVM30FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM541] Gestion des ventes -> Opérations périodiques -> Journal fiscal des ventes -> Edition journal fiscal des ventes
 * Indicateur : 00000001
 * Titre : Edition des journaux fiscaux de vente
 * 
 * [GVM551] Gestion des ventes -> Opérations périodiques -> Journal fiscal par sections -> Edition
 * Indicateur : 10000001 (91)
 * Titre : Edition des journaux fiscaux de vente par section
 */
public class SGVM30FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORTER = "Exporter";
  private Message LOCTP = null;
  private Message periodeUn = null;
  private Message periodeDeux = null;
  private Message periodeTrois = null;
  
  public SGVM30FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    UETVA.setValeursSelection("1", " ");
    REPON2.setValeursSelection("OUI", "NON");
    REPON3.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    AVOIR.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    lbPeriode1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO1@")).trim());
    lbPeriode2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO2@")).trim());
    lbPeriode3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO3@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Indicateurs
    Boolean isEditionSection = lexique.isTrue("91");
    Boolean isMagasin = lexique.isTrue("31");
    Boolean isGroupeCanalVente = lexique.isTrue("32");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Visibilité des composants ainsi que changement de titre suivant le pts de menu
    if (isEditionSection) {
      snSectionAnalytique.setVisible(true);
      lbSectionAnalytique.setVisible(true);
      bpPresentation.setText("Edition des journaux fiscaux de vente par section");
    }
    else {
      snSectionAnalytique.setVisible(false);
      lbSectionAnalytique.setVisible(false);
      bpPresentation.setText("Edition des journaux fiscaux de vente");
    }
    
    pnlPeriode1.setVisible(!lexique.HostFieldGetData("PERIO1").trim().isEmpty());
    pnlPeriode3.setVisible(!lexique.HostFieldGetData("PERIO3").trim().isEmpty());
    pnlPeriode2.setVisible(!lexique.HostFieldGetData("PERIO2").trim().isEmpty());
    
    lbMagasin.setVisible(isMagasin);
    snMagasin.setVisible(isMagasin);
    snCanalDeVente.setVisible(isGroupeCanalVente);
    lbGroupeCanauxVente.setVisible(isGroupeCanalVente);
    
    // Titre panel etablissement
    if (isMagasin) {
      pnlEtablissement.setTitre("Etablissement et magasin");
    }
    else {
      pnlEtablissement.setTitre("Etablissement");
    }
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Message période facturé
    periodeUn = periodeUn.getMessageNormal(lexique.HostFieldGetData("PERIO1").trim().replaceAll("\\s+", " ") + " "
        + lexique.HostFieldGetData("LIB1").trim().replaceAll("\\s+", " "));
    lbPeriode1.setMessage(periodeUn);
    
    periodeDeux = periodeDeux.getMessageNormal(lexique.HostFieldGetData("PERIO2").trim().replaceAll("\\s+", " ") + " "
        + lexique.HostFieldGetData("LIB2").trim().replaceAll("\\s+", " "));
    lbPeriode2.setMessage(periodeDeux);
    
    periodeTrois = periodeTrois.getMessageNormal(lexique.HostFieldGetData("PERIO3").trim().replaceAll("\\s+", " ") + " "
        + lexique.HostFieldGetData("LIB3").trim().replaceAll("\\s+", " "));
    lbPeriode3.setMessage(periodeTrois);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Charge de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charge les composants
    chargerListeMagasin();
    chargerListeSectionAnalytique();
    chargerListeCanalVente();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snMagasin.isVisible()) {
      snMagasin.renseignerChampRPG(lexique, "XPARG6");
    }
    if (snCanalDeVente.isVisible()) {
      snCanalDeVente.renseignerChampRPG(lexique, "XPARG6");
    }
    snSectionAnalytique.renseignerChampRPG(lexique, "SANA");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composants magasin suivant l'etablissement
   */
  private void chargerListeMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "XPARG6");
  }
  
  /**
   * Charge le composants canal de vente suivant l'etablissement
   */
  private void chargerListeCanalVente() {
    snCanalDeVente.setSession(getSession());
    snCanalDeVente.setIdEtablissement(snEtablissement.getIdSelection());
    snCanalDeVente.setTousAutorise(true);
    snCanalDeVente.charger(false);
    snCanalDeVente.setSelectionParChampRPG(lexique, "XPARG6");
  }
  
  /**
   * Charge le composants section analytique suivant l'etablissement
   */
  private void chargerListeSectionAnalytique() {
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "SANA");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeMagasin();
      chargerListeSectionAnalytique();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    pnlPeriode1 = new SNPanel();
    lbPeriodeFactureUn = new SNLabelChamp();
    UPER1 = new XRiTextField();
    UPER2 = new XRiTextField();
    lbAu = new SNLabelChamp();
    lbPeriode1 = new SNLabelUnite();
    pnlPeriode2 = new SNPanel();
    lbPeriodeFactureDeux = new SNLabelChamp();
    UPER3 = new XRiTextField();
    UPER4 = new XRiTextField();
    lbAu2 = new SNLabelChamp();
    lbPeriode2 = new SNLabelUnite();
    pnlPeriode3 = new SNPanel();
    lbPeriodeFactureTrois = new SNLabelChamp();
    UPER5 = new XRiTextField();
    UPER6 = new XRiTextField();
    lbSlash3 = new SNLabelChamp();
    lbPeriode3 = new SNLabelUnite();
    lbSectionAnalytique = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    lbGroupeCanauxVente = new SNLabelChamp();
    snCanalDeVente = new SNCanalDeVente();
    UETVA = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlEdition = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    REPON2 = new XRiCheckBox();
    REPON3 = new XRiCheckBox();
    AVOIR = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des journaux fiscaux de vente");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlPeriode1 ========
            {
              pnlPeriode1.setOpaque(false);
              pnlPeriode1.setName("pnlPeriode1");
              pnlPeriode1.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriode1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriode1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriode1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriode1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbPeriodeFactureUn ----
              lbPeriodeFactureUn.setText("P\u00e9riode factur\u00e9e du");
              lbPeriodeFactureUn.setMinimumSize(new Dimension(185, 30));
              lbPeriodeFactureUn.setMaximumSize(new Dimension(185, 30));
              lbPeriodeFactureUn.setPreferredSize(new Dimension(185, 30));
              lbPeriodeFactureUn.setName("lbPeriodeFactureUn");
              pnlPeriode1.add(lbPeriodeFactureUn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- UPER1 ----
              UPER1.setToolTipText("Jour");
              UPER1.setMinimumSize(new Dimension(36, 30));
              UPER1.setPreferredSize(new Dimension(36, 30));
              UPER1.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER1.setName("UPER1");
              pnlPeriode1.add(UPER1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- UPER2 ----
              UPER2.setToolTipText("Jour");
              UPER2.setPreferredSize(new Dimension(36, 30));
              UPER2.setMinimumSize(new Dimension(36, 30));
              UPER2.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER2.setName("UPER2");
              pnlPeriode1.add(UPER2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setHorizontalAlignment(SwingConstants.CENTER);
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlPeriode1.add(lbAu, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbPeriode1 ----
              lbPeriode1.setText("@PERIO1@");
              lbPeriode1.setMinimumSize(new Dimension(150, 30));
              lbPeriode1.setPreferredSize(new Dimension(150, 30));
              lbPeriode1.setMaximumSize(new Dimension(150, 30));
              lbPeriode1.setName("lbPeriode1");
              pnlPeriode1.add(lbPeriode1, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlPeriode1, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlPeriode2 ========
            {
              pnlPeriode2.setOpaque(false);
              pnlPeriode2.setName("pnlPeriode2");
              pnlPeriode2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriode2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriode2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriode2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriode2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbPeriodeFactureDeux ----
              lbPeriodeFactureDeux.setText("P\u00e9riode factur\u00e9e du");
              lbPeriodeFactureDeux.setMinimumSize(new Dimension(185, 30));
              lbPeriodeFactureDeux.setMaximumSize(new Dimension(185, 30));
              lbPeriodeFactureDeux.setPreferredSize(new Dimension(185, 30));
              lbPeriodeFactureDeux.setName("lbPeriodeFactureDeux");
              pnlPeriode2.add(lbPeriodeFactureDeux, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- UPER3 ----
              UPER3.setToolTipText("Jour");
              UPER3.setPreferredSize(new Dimension(36, 30));
              UPER3.setMinimumSize(new Dimension(36, 30));
              UPER3.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER3.setName("UPER3");
              pnlPeriode2.add(UPER3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- UPER4 ----
              UPER4.setToolTipText("Jour");
              UPER4.setPreferredSize(new Dimension(36, 30));
              UPER4.setMinimumSize(new Dimension(36, 30));
              UPER4.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER4.setName("UPER4");
              pnlPeriode2.add(UPER4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu2 ----
              lbAu2.setText("au");
              lbAu2.setHorizontalAlignment(SwingConstants.CENTER);
              lbAu2.setPreferredSize(new Dimension(16, 30));
              lbAu2.setMinimumSize(new Dimension(16, 30));
              lbAu2.setMaximumSize(new Dimension(16, 30));
              lbAu2.setName("lbAu2");
              pnlPeriode2.add(lbAu2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbPeriode2 ----
              lbPeriode2.setText("@PERIO2@");
              lbPeriode2.setMaximumSize(new Dimension(150, 30));
              lbPeriode2.setName("lbPeriode2");
              pnlPeriode2.add(lbPeriode2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlPeriode2, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlPeriode3 ========
            {
              pnlPeriode3.setOpaque(false);
              pnlPeriode3.setName("pnlPeriode3");
              pnlPeriode3.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriode3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriode3.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriode3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriode3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbPeriodeFactureTrois ----
              lbPeriodeFactureTrois.setText("P\u00e9riode factur\u00e9e du");
              lbPeriodeFactureTrois.setMinimumSize(new Dimension(185, 30));
              lbPeriodeFactureTrois.setMaximumSize(new Dimension(185, 30));
              lbPeriodeFactureTrois.setPreferredSize(new Dimension(185, 30));
              lbPeriodeFactureTrois.setName("lbPeriodeFactureTrois");
              pnlPeriode3.add(lbPeriodeFactureTrois, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- UPER5 ----
              UPER5.setToolTipText("Jour");
              UPER5.setMinimumSize(new Dimension(36, 30));
              UPER5.setPreferredSize(new Dimension(36, 30));
              UPER5.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER5.setName("UPER5");
              pnlPeriode3.add(UPER5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- UPER6 ----
              UPER6.setToolTipText("Jour");
              UPER6.setMinimumSize(new Dimension(36, 30));
              UPER6.setPreferredSize(new Dimension(36, 30));
              UPER6.setFont(new Font("sansserif", Font.PLAIN, 14));
              UPER6.setName("UPER6");
              pnlPeriode3.add(UPER6, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSlash3 ----
              lbSlash3.setText("au");
              lbSlash3.setHorizontalAlignment(SwingConstants.CENTER);
              lbSlash3.setPreferredSize(new Dimension(16, 30));
              lbSlash3.setMinimumSize(new Dimension(16, 30));
              lbSlash3.setMaximumSize(new Dimension(16, 30));
              lbSlash3.setName("lbSlash3");
              pnlPeriode3.add(lbSlash3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbPeriode3 ----
              lbPeriode3.setText("@PERIO3@");
              lbPeriode3.setMinimumSize(new Dimension(150, 30));
              lbPeriode3.setPreferredSize(new Dimension(150, 30));
              lbPeriode3.setMaximumSize(new Dimension(150, 30));
              lbPeriode3.setName("lbPeriode3");
              pnlPeriode3.add(lbPeriode3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlPeriode3, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSectionAnalytique ----
            lbSectionAnalytique.setText("Section analytique");
            lbSectionAnalytique.setName("lbSectionAnalytique");
            pnlCritereDeSelection.add(lbSectionAnalytique, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSectionAnalytique ----
            snSectionAnalytique.setName("snSectionAnalytique");
            pnlCritereDeSelection.add(snSectionAnalytique, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbGroupeCanauxVente ----
            lbGroupeCanauxVente.setText("Groupe de canaux de vente");
            lbGroupeCanauxVente.setPreferredSize(new Dimension(185, 30));
            lbGroupeCanauxVente.setMinimumSize(new Dimension(185, 30));
            lbGroupeCanauxVente.setName("lbGroupeCanauxVente");
            pnlCritereDeSelection.add(lbGroupeCanauxVente, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCanalDeVente ----
            snCanalDeVente.setName("snCanalDeVente");
            pnlCritereDeSelection.add(snCanalDeVente, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- UETVA ----
            UETVA.setText("Edition des ventes UE avec TVA");
            UETVA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UETVA.setPreferredSize(new Dimension(175, 30));
            UETVA.setMinimumSize(new Dimension(175, 30));
            UETVA.setFont(new Font("sansserif", Font.PLAIN, 14));
            UETVA.setName("UETVA");
            pnlCritereDeSelection.add(UETVA, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement et magasin");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setEnabled(false);
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setMinimumSize(new Dimension(185, 30));
            lbEtablissement.setPreferredSize(new Dimension(185, 30));
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setPreferredSize(new Dimension(185, 30));
            lbPeriode.setMinimumSize(new Dimension(185, 30));
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setPreferredSize(new Dimension(185, 30));
            lbMagasin.setMinimumSize(new Dimension(185, 30));
            lbMagasin.setName("lbMagasin");
            pnlEtablissement.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setEnabled(false);
            snMagasin.setName("snMagasin");
            pnlEtablissement.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlEdition ========
          {
            pnlEdition.setOpaque(false);
            pnlEdition.setTitre("Options d'\u00e9dition");
            pnlEdition.setName("pnlEdition");
            pnlEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- REPON1 ----
            REPON1.setText("R\u00e9capitulatif seulement");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setMinimumSize(new Dimension(190, 30));
            REPON1.setPreferredSize(new Dimension(190, 30));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setName("REPON1");
            pnlEdition.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON2 ----
            REPON2.setText("Saut de page par journ\u00e9e");
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setPreferredSize(new Dimension(190, 30));
            REPON2.setMinimumSize(new Dimension(190, 30));
            REPON2.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON2.setName("REPON2");
            pnlEdition.add(REPON2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON3 ----
            REPON3.setText("Edition avec interlignes");
            REPON3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON3.setMinimumSize(new Dimension(190, 30));
            REPON3.setPreferredSize(new Dimension(190, 30));
            REPON3.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON3.setName("REPON3");
            pnlEdition.add(REPON3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- AVOIR ----
            AVOIR.setText("Edition des avoirs \u00e0 part");
            AVOIR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AVOIR.setPreferredSize(new Dimension(190, 30));
            AVOIR.setMinimumSize(new Dimension(190, 30));
            AVOIR.setFont(new Font("sansserif", Font.PLAIN, 14));
            AVOIR.setName("AVOIR");
            pnlEdition.add(AVOIR, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNPanel pnlPeriode1;
  private SNLabelChamp lbPeriodeFactureUn;
  private XRiTextField UPER1;
  private XRiTextField UPER2;
  private SNLabelChamp lbAu;
  private SNLabelUnite lbPeriode1;
  private SNPanel pnlPeriode2;
  private SNLabelChamp lbPeriodeFactureDeux;
  private XRiTextField UPER3;
  private XRiTextField UPER4;
  private SNLabelChamp lbAu2;
  private SNLabelUnite lbPeriode2;
  private SNPanel pnlPeriode3;
  private SNLabelChamp lbPeriodeFactureTrois;
  private XRiTextField UPER5;
  private XRiTextField UPER6;
  private SNLabelChamp lbSlash3;
  private SNLabelUnite lbPeriode3;
  private SNLabelChamp lbSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbGroupeCanauxVente;
  private SNCanalDeVente snCanalDeVente;
  private XRiCheckBox UETVA;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlEdition;
  private XRiCheckBox REPON1;
  private XRiCheckBox REPON2;
  private XRiCheckBox REPON3;
  private XRiCheckBox AVOIR;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
