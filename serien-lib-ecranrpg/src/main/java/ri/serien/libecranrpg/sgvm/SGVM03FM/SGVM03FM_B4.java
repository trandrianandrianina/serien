
package ri.serien.libecranrpg.sgvm.SGVM03FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.client.EnumCodeAttentionClient;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM1714] Gestion des ventes -> Fiches permanentes -> Editions clients -> Listes -> Par représentant / numéro client
 * Indicateur : 00010001
 * Titre : Edition du fichier des clients par Représentant/ Numéro client
 * 
 * [GVM1715] Gestion des ventes -> Fiches permanentes -> Editions clients -> Listes -> Par représentant / catégorie client
 * Indicateur : 00010101
 * Titre : Edition du fichier des clients par Représentant/ Catégorie client
 * 
 * [GVM1716] Gestion des ventes -> Fiches permanentes -> Editions clients -> Listes -> Par représentant / code postal
 * Indicateur : 00000101
 * Titre : Edition du fichier des clients par Représentant/ Code postal
 */
public class SGVM03FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private Message LOCTP = null;
  
  public SGVM03FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    snRepresentantDebut.lierComposantFin(snRepresentantFin);
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Indicateur
    Boolean isFichierNumeroClient = lexique.isTrue("94");
    Boolean isFichierCodePostal = lexique.isTrue("96");
    Boolean isFichierCategorieClient = lexique.isTrue("94 and 96");
    
    // Titre
    if (isFichierNumeroClient) {
      bpPresentation.setText("Edition du fichier des clients par Représentant/ Numéro client");
    }
    else if (isFichierCodePostal) {
      bpPresentation.setText("Edition du fichier des clients par Représentant/ Code postal");
    }
    if (isFichierCategorieClient) {
      bpPresentation.setText("Edition du fichier des clients par Représentant/ Catégorie client");
    }
    
    // Ajout d'élements dans la combobox
    if (cbSelectionOptions.getItemCount() == 0) {
      cbSelectionOptions.removeAllItems();
      cbSelectionOptions.addItem("Tous");
      cbSelectionOptions.setSelectedItem(lexique.HostFieldGetData("WTNS"));
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_CLIENT_ACTIF);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_ZONE_EN_ROUGE);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_CLIENT_INTERDIT);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_LIVRAISON_INTERDITE);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_ATTENTE_DEPASSEMENT);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_PAIEMENT_A_LA_COMMANDE);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_CLIENT_DESACTIVE);
    }
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Iinitialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Chargement des composants des composants
    chargerListeMagasin();
    chargerListeDevise();
    chargerListeRepresentant();
  }
  
  @Override
  public void getData() {
    super.getData();
    recupererValeurCombobox();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snDevise.renseignerChampRPG(lexique, "WDEV");
    snRepresentantDebut.renseignerChampRPG(lexique, "DEBICR");
    snRepresentantFin.renseignerChampRPG(lexique, "FINICR");
    if (snRepresentantDebut.getIdSelection() == null && snRepresentantFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
    }
  }
  
  /**
   * Charge la liste des magasin suivant l'etablissemenet
   */
  private void chargerListeMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Charge la liste des representant suivant l'etablissemenet
   */
  private void chargerListeRepresentant() {
    snRepresentantDebut.setSession(getSession());
    snRepresentantDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantDebut.setTousAutorise(true);
    snRepresentantDebut.charger(false);
    snRepresentantDebut.setSelectionParChampRPG(lexique, "DEBICR");
    
    snRepresentantFin.setSession(getSession());
    snRepresentantFin.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantFin.setTousAutorise(true);
    snRepresentantFin.charger(false);
    snRepresentantFin.setSelectionParChampRPG(lexique, "FINICR");
  }
  
  /**
   * Charge la liste des devise suivant l'etablissemenet
   */
  private void chargerListeDevise() {
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(snEtablissement.getIdSelection());
    snDevise.setTousAutorise(true);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "WDEV");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Permet de récuperer quelle est l'option séléctionner dans le combobox
   */
  private void recupererValeurCombobox() {
    if (cbSelectionOptions.getSelectedItem().equals(null)) {
      return;
    }
    else {
      if (cbSelectionOptions.getSelectedItem() instanceof String) {
        lexique.HostFieldPutData("WTOU2", 0, "**");
      }
      else {
        switch ((EnumCodeAttentionClient) cbSelectionOptions.getSelectedItem()) {
          case ATTENTION_CLIENT_ACTIF:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_CLIENT_ACTIF.getCode().toString());
            break;
          case ATTENTION_ZONE_EN_ROUGE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_ZONE_EN_ROUGE.getCode().toString());
            break;
          case ATTENTION_CLIENT_INTERDIT:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_CLIENT_INTERDIT.getCode().toString());
            break;
          case ATTENTION_LIVRAISON_INTERDITE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_LIVRAISON_INTERDITE.getCode().toString());
            break;
          case ATTENTION_ATTENTE_DEPASSEMENT:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_ATTENTE_DEPASSEMENT.getCode().toString());
            break;
          case ATTENTION_PAIEMENT_A_LA_COMMANDE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_PAIEMENT_A_LA_COMMANDE.getCode().toString());
            break;
          case ATTENTION_CLIENT_DESACTIVE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_CLIENT_DESACTIVE.getCode().toString());
            break;
          default:
            break;
        }
      }
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeMagasin();
      chargerListeDevise();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCriteresSelection = new SNPanelTitre();
    lbRepresentantDebut = new SNLabelChamp();
    snRepresentantDebut = new SNRepresentant();
    lbRepresentantFin = new SNLabelChamp();
    snRepresentantFin = new SNRepresentant();
    lbTopAttention = new SNLabelChamp();
    cbSelectionOptions = new SNComboBox();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    lbCodeMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCriteresSelection ========
          {
            pnlCriteresSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresSelection.setName("pnlCriteresSelection");
            pnlCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbRepresentantDebut ----
            lbRepresentantDebut.setText("Repr\u00e9sentant d\u00e9but ");
            lbRepresentantDebut.setName("lbRepresentantDebut");
            pnlCriteresSelection.add(lbRepresentantDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snRepresentantDebut ----
            snRepresentantDebut.setName("snRepresentantDebut");
            pnlCriteresSelection.add(snRepresentantDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbRepresentantFin ----
            lbRepresentantFin.setText("Repr\u00e9sentant fin");
            lbRepresentantFin.setName("lbRepresentantFin");
            pnlCriteresSelection.add(lbRepresentantFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snRepresentantFin ----
            snRepresentantFin.setName("snRepresentantFin");
            pnlCriteresSelection.add(snRepresentantFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTopAttention ----
            lbTopAttention.setText("Type de client");
            lbTopAttention.setMinimumSize(new Dimension(95, 30));
            lbTopAttention.setPreferredSize(new Dimension(95, 30));
            lbTopAttention.setName("lbTopAttention");
            pnlCriteresSelection.add(lbTopAttention, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbSelectionOptions ----
            cbSelectionOptions.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbSelectionOptions.setPreferredSize(new Dimension(260, 30));
            cbSelectionOptions.setMinimumSize(new Dimension(260, 30));
            cbSelectionOptions.setName("cbSelectionOptions");
            pnlCriteresSelection.add(cbSelectionOptions, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDevise ----
            lbDevise.setText("Devise");
            lbDevise.setName("lbDevise");
            pnlCriteresSelection.add(lbDevise, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snDevise ----
            snDevise.setName("snDevise");
            pnlCriteresSelection.add(snDevise, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeMagasin ----
            lbCodeMagasin.setText("Magasin");
            lbCodeMagasin.setName("lbCodeMagasin");
            pnlCriteresSelection.add(lbCodeMagasin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setName("snMagasin");
            pnlCriteresSelection.add(snMagasin, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setPreferredSize(new Dimension(308, 30));
            tfEnCours.setMinimumSize(new Dimension(308, 30));
            tfEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfEnCours.setEnabled(false);
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("Periode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresSelection;
  private SNLabelChamp lbRepresentantDebut;
  private SNRepresentant snRepresentantDebut;
  private SNLabelChamp lbRepresentantFin;
  private SNRepresentant snRepresentantFin;
  private SNLabelChamp lbTopAttention;
  private SNComboBox cbSelectionOptions;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNLabelChamp lbCodeMagasin;
  private SNMagasin snMagasin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
