
package ri.serien.libecranrpg.sgvm.SGVM6KFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM6KFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM6KFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CLINUL.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    // TODO Init spécifiques..
    // CLINUL.setSelected(lexique.HostFieldGetData("CLINUL").equalsIgnoreCase("OUI"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CLINUL.isSelected())
    // lexique.HostFieldPutData("CLINUL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CLINUL", 0, "NON");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_24 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_23 = new JXTitledSeparator();
    CLINUL = new XRiCheckBox();
    OBJ_89 = new JLabel();
    OBJ_88 = new JLabel();
    PAYDEB = new XRiTextField();
    PAYFIN = new XRiTextField();
    CATDEB = new XRiTextField();
    CATFIN = new XRiTextField();
    panel1 = new JPanel();
    OBJ_92 = new JLabel();
    PDEB1 = new XRiCalendrier();
    PFIN1 = new XRiCalendrier();
    OBJ_93 = new JLabel();
    PDEB2 = new XRiCalendrier();
    PDEB3 = new XRiCalendrier();
    OBJ_94 = new JLabel();
    PFIN2 = new XRiCalendrier();
    PFIN3 = new XRiCalendrier();
    panel2 = new JPanel();
    OBJ_91 = new JLabel();
    TOPCLI = new XRiTextField();
    CLIDEB = new XRiTextField();
    CLIFIN = new XRiTextField();
    panel3 = new JPanel();
    GP101 = new XRiTextField();
    GP102 = new XRiTextField();
    OBJ_90 = new JLabel();
    panel4 = new JPanel();
    ZP201 = new XRiTextField();
    ZP202 = new XRiTextField();
    OBJ_95 = new JLabel();
    ZP203 = new XRiTextField();
    ZP204 = new XRiTextField();
    ZP205 = new XRiTextField();
    ZP206 = new XRiTextField();
    ZP207 = new XRiTextField();
    NZPART = new XRiTextField();
    WZPART = new XRiTextField();
    ZP208 = new XRiTextField();
    ZP209 = new XRiTextField();
    ZP210 = new XRiTextField();
    ZP211 = new XRiTextField();
    ZP212 = new XRiTextField();
    panel5 = new JPanel();
    DEP01 = new XRiTextField();
    DEP02 = new XRiTextField();
    DEP03 = new XRiTextField();
    DEP04 = new XRiTextField();
    DEP05 = new XRiTextField();
    DEP06 = new XRiTextField();
    DEP07 = new XRiTextField();
    DEP08 = new XRiTextField();
    DEP09 = new XRiTextField();
    DEP10 = new XRiTextField();
    DEP11 = new XRiTextField();
    DEP12 = new XRiTextField();
    DEP13 = new XRiTextField();
    DEP26 = new XRiTextField();
    DEP25 = new XRiTextField();
    DEP24 = new XRiTextField();
    DEP23 = new XRiTextField();
    DEP22 = new XRiTextField();
    DEP21 = new XRiTextField();
    DEP20 = new XRiTextField();
    DEP19 = new XRiTextField();
    DEP18 = new XRiTextField();
    DEP17 = new XRiTextField();
    DEP16 = new XRiTextField();
    DEP15 = new XRiTextField();
    DEP14 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_85 = new JPanel();
    OBJ_86 = new JLabel();
    OBJ_87 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(580, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_24 ----
          OBJ_24.setTitle("Zones personnalis\u00e9es 2 article");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Code(s) d\u00e9partement(s) \u00e0 traiter");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_21 ----
          OBJ_21.setTitle("P\u00e9riodes \u00e0 s\u00e9lectionner");
          OBJ_21.setName("OBJ_21");

          //---- OBJ_23 ----
          OBJ_23.setTitle("");
          OBJ_23.setName("OBJ_23");

          //---- CLINUL ----
          CLINUL.setText("Edition des clients avec montants nuls");
          CLINUL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLINUL.setName("CLINUL");

          //---- OBJ_89 ----
          OBJ_89.setText("Cat\u00e9gorie(s) client");
          OBJ_89.setName("OBJ_89");

          //---- OBJ_88 ----
          OBJ_88.setText("Code(s) pays");
          OBJ_88.setName("OBJ_88");

          //---- PAYDEB ----
          PAYDEB.setComponentPopupMenu(BTD);
          PAYDEB.setName("PAYDEB");

          //---- PAYFIN ----
          PAYFIN.setComponentPopupMenu(BTD);
          PAYFIN.setName("PAYFIN");

          //---- CATDEB ----
          CATDEB.setComponentPopupMenu(BTD);
          CATDEB.setName("CATDEB");

          //---- CATFIN ----
          CATFIN.setComponentPopupMenu(BTD);
          CATFIN.setName("CATFIN");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_92 ----
            OBJ_92.setText("P\u00e9riode 1");
            OBJ_92.setName("OBJ_92");
            panel1.add(OBJ_92);
            OBJ_92.setBounds(10, 14, 61, 20);

            //---- PDEB1 ----
            PDEB1.setName("PDEB1");
            panel1.add(PDEB1);
            PDEB1.setBounds(195, 10, 105, PDEB1.getPreferredSize().height);

            //---- PFIN1 ----
            PFIN1.setName("PFIN1");
            panel1.add(PFIN1);
            PFIN1.setBounds(385, 10, 105, PFIN1.getPreferredSize().height);

            //---- OBJ_93 ----
            OBJ_93.setText("P\u00e9riode 2");
            OBJ_93.setName("OBJ_93");
            panel1.add(OBJ_93);
            OBJ_93.setBounds(10, 44, 61, 20);

            //---- PDEB2 ----
            PDEB2.setName("PDEB2");
            panel1.add(PDEB2);
            PDEB2.setBounds(195, 40, 105, PDEB2.getPreferredSize().height);

            //---- PDEB3 ----
            PDEB3.setName("PDEB3");
            panel1.add(PDEB3);
            PDEB3.setBounds(195, 70, 105, PDEB3.getPreferredSize().height);

            //---- OBJ_94 ----
            OBJ_94.setText("P\u00e9riode 3");
            OBJ_94.setName("OBJ_94");
            panel1.add(OBJ_94);
            OBJ_94.setBounds(10, 74, 58, 20);

            //---- PFIN2 ----
            PFIN2.setName("PFIN2");
            panel1.add(PFIN2);
            PFIN2.setBounds(385, 40, 105, PFIN2.getPreferredSize().height);

            //---- PFIN3 ----
            PFIN3.setName("PFIN3");
            panel1.add(PFIN3);
            PFIN3.setBounds(385, 70, 105, PFIN3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_91 ----
            OBJ_91.setText("Num\u00e9ro(s) client(s)");
            OBJ_91.setName("OBJ_91");
            panel2.add(OBJ_91);
            OBJ_91.setBounds(5, 9, 117, 20);

            //---- TOPCLI ----
            TOPCLI.setComponentPopupMenu(BTD);
            TOPCLI.setName("TOPCLI");
            panel2.add(TOPCLI);
            TOPCLI.setBounds(222, 5, 24, TOPCLI.getPreferredSize().height);

            //---- CLIDEB ----
            CLIDEB.setComponentPopupMenu(BTD);
            CLIDEB.setName("CLIDEB");
            panel2.add(CLIDEB);
            CLIDEB.setBounds(260, 5, 60, CLIDEB.getPreferredSize().height);

            //---- CLIFIN ----
            CLIFIN.setComponentPopupMenu(BTD);
            CLIFIN.setName("CLIFIN");
            panel2.add(CLIFIN);
            CLIFIN.setBounds(335, 5, 60, CLIFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- GP101 ----
            GP101.setComponentPopupMenu(BTD);
            GP101.setName("GP101");
            panel3.add(GP101);
            GP101.setBounds(232, 5, 24, GP101.getPreferredSize().height);

            //---- GP102 ----
            GP102.setComponentPopupMenu(BTD);
            GP102.setName("GP102");
            panel3.add(GP102);
            GP102.setBounds(269, 5, 24, GP102.getPreferredSize().height);

            //---- OBJ_90 ----
            OBJ_90.setText("Groupe zones personnalis\u00e9es 1 art");
            OBJ_90.setName("OBJ_90");
            panel3.add(OBJ_90);
            OBJ_90.setBounds(10, 9, 211, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- ZP201 ----
            ZP201.setComponentPopupMenu(BTD);
            ZP201.setName("ZP201");
            panel4.add(ZP201);
            ZP201.setBounds(15, 5, 34, ZP201.getPreferredSize().height);

            //---- ZP202 ----
            ZP202.setComponentPopupMenu(BTD);
            ZP202.setName("ZP202");
            panel4.add(ZP202);
            ZP202.setBounds(52, 5, 34, ZP202.getPreferredSize().height);

            //---- OBJ_95 ----
            OBJ_95.setText("Autre ZP art.(3,4 ou 5)(n\u00b0/code)");
            OBJ_95.setName("OBJ_95");
            panel4.add(OBJ_95);
            OBJ_95.setBounds(15, 39, 201, 20);

            //---- ZP203 ----
            ZP203.setComponentPopupMenu(BTD);
            ZP203.setName("ZP203");
            panel4.add(ZP203);
            ZP203.setBounds(89, 5, 34, ZP203.getPreferredSize().height);

            //---- ZP204 ----
            ZP204.setComponentPopupMenu(BTD);
            ZP204.setName("ZP204");
            panel4.add(ZP204);
            ZP204.setBounds(126, 5, 34, ZP204.getPreferredSize().height);

            //---- ZP205 ----
            ZP205.setComponentPopupMenu(BTD);
            ZP205.setName("ZP205");
            panel4.add(ZP205);
            ZP205.setBounds(163, 5, 34, ZP205.getPreferredSize().height);

            //---- ZP206 ----
            ZP206.setComponentPopupMenu(BTD);
            ZP206.setName("ZP206");
            panel4.add(ZP206);
            ZP206.setBounds(200, 5, 34, ZP206.getPreferredSize().height);

            //---- ZP207 ----
            ZP207.setComponentPopupMenu(BTD);
            ZP207.setName("ZP207");
            panel4.add(ZP207);
            ZP207.setBounds(237, 5, 34, ZP207.getPreferredSize().height);

            //---- NZPART ----
            NZPART.setComponentPopupMenu(BTD);
            NZPART.setName("NZPART");
            panel4.add(NZPART);
            NZPART.setBounds(237, 35, 20, NZPART.getPreferredSize().height);

            //---- WZPART ----
            WZPART.setComponentPopupMenu(BTD);
            WZPART.setName("WZPART");
            panel4.add(WZPART);
            WZPART.setBounds(274, 35, 34, WZPART.getPreferredSize().height);

            //---- ZP208 ----
            ZP208.setComponentPopupMenu(BTD);
            ZP208.setName("ZP208");
            panel4.add(ZP208);
            ZP208.setBounds(274, 5, 34, ZP208.getPreferredSize().height);

            //---- ZP209 ----
            ZP209.setComponentPopupMenu(BTD);
            ZP209.setName("ZP209");
            panel4.add(ZP209);
            ZP209.setBounds(311, 5, 34, ZP209.getPreferredSize().height);

            //---- ZP210 ----
            ZP210.setComponentPopupMenu(BTD);
            ZP210.setName("ZP210");
            panel4.add(ZP210);
            ZP210.setBounds(348, 5, 34, ZP210.getPreferredSize().height);

            //---- ZP211 ----
            ZP211.setComponentPopupMenu(BTD);
            ZP211.setName("ZP211");
            panel4.add(ZP211);
            ZP211.setBounds(385, 5, 34, ZP211.getPreferredSize().height);

            //---- ZP212 ----
            ZP212.setComponentPopupMenu(BTD);
            ZP212.setName("ZP212");
            panel4.add(ZP212);
            ZP212.setBounds(422, 5, 34, ZP212.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- DEP01 ----
            DEP01.setComponentPopupMenu(BTD);
            DEP01.setName("DEP01");
            panel5.add(DEP01);
            DEP01.setBounds(10, 5, 34, DEP01.getPreferredSize().height);

            //---- DEP02 ----
            DEP02.setComponentPopupMenu(BTD);
            DEP02.setName("DEP02");
            panel5.add(DEP02);
            DEP02.setBounds(47, 5, 34, DEP02.getPreferredSize().height);

            //---- DEP03 ----
            DEP03.setComponentPopupMenu(BTD);
            DEP03.setName("DEP03");
            panel5.add(DEP03);
            DEP03.setBounds(84, 5, 34, DEP03.getPreferredSize().height);

            //---- DEP04 ----
            DEP04.setComponentPopupMenu(BTD);
            DEP04.setName("DEP04");
            panel5.add(DEP04);
            DEP04.setBounds(121, 5, 34, DEP04.getPreferredSize().height);

            //---- DEP05 ----
            DEP05.setComponentPopupMenu(BTD);
            DEP05.setName("DEP05");
            panel5.add(DEP05);
            DEP05.setBounds(158, 5, 34, DEP05.getPreferredSize().height);

            //---- DEP06 ----
            DEP06.setComponentPopupMenu(BTD);
            DEP06.setName("DEP06");
            panel5.add(DEP06);
            DEP06.setBounds(195, 5, 34, DEP06.getPreferredSize().height);

            //---- DEP07 ----
            DEP07.setComponentPopupMenu(BTD);
            DEP07.setName("DEP07");
            panel5.add(DEP07);
            DEP07.setBounds(232, 5, 34, DEP07.getPreferredSize().height);

            //---- DEP08 ----
            DEP08.setComponentPopupMenu(BTD);
            DEP08.setName("DEP08");
            panel5.add(DEP08);
            DEP08.setBounds(269, 5, 34, DEP08.getPreferredSize().height);

            //---- DEP09 ----
            DEP09.setComponentPopupMenu(BTD);
            DEP09.setName("DEP09");
            panel5.add(DEP09);
            DEP09.setBounds(306, 5, 34, DEP09.getPreferredSize().height);

            //---- DEP10 ----
            DEP10.setComponentPopupMenu(BTD);
            DEP10.setName("DEP10");
            panel5.add(DEP10);
            DEP10.setBounds(343, 5, 34, DEP10.getPreferredSize().height);

            //---- DEP11 ----
            DEP11.setComponentPopupMenu(BTD);
            DEP11.setName("DEP11");
            panel5.add(DEP11);
            DEP11.setBounds(380, 5, 34, DEP11.getPreferredSize().height);

            //---- DEP12 ----
            DEP12.setComponentPopupMenu(BTD);
            DEP12.setName("DEP12");
            panel5.add(DEP12);
            DEP12.setBounds(417, 5, 34, DEP12.getPreferredSize().height);

            //---- DEP13 ----
            DEP13.setComponentPopupMenu(BTD);
            DEP13.setName("DEP13");
            panel5.add(DEP13);
            DEP13.setBounds(454, 5, 34, DEP13.getPreferredSize().height);

            //---- DEP26 ----
            DEP26.setComponentPopupMenu(BTD);
            DEP26.setName("DEP26");
            panel5.add(DEP26);
            DEP26.setBounds(454, 35, 34, DEP26.getPreferredSize().height);

            //---- DEP25 ----
            DEP25.setComponentPopupMenu(BTD);
            DEP25.setName("DEP25");
            panel5.add(DEP25);
            DEP25.setBounds(417, 35, 34, DEP25.getPreferredSize().height);

            //---- DEP24 ----
            DEP24.setComponentPopupMenu(BTD);
            DEP24.setName("DEP24");
            panel5.add(DEP24);
            DEP24.setBounds(380, 35, 34, DEP24.getPreferredSize().height);

            //---- DEP23 ----
            DEP23.setComponentPopupMenu(BTD);
            DEP23.setName("DEP23");
            panel5.add(DEP23);
            DEP23.setBounds(343, 35, 34, DEP23.getPreferredSize().height);

            //---- DEP22 ----
            DEP22.setComponentPopupMenu(BTD);
            DEP22.setName("DEP22");
            panel5.add(DEP22);
            DEP22.setBounds(306, 35, 34, DEP22.getPreferredSize().height);

            //---- DEP21 ----
            DEP21.setComponentPopupMenu(BTD);
            DEP21.setName("DEP21");
            panel5.add(DEP21);
            DEP21.setBounds(269, 35, 34, DEP21.getPreferredSize().height);

            //---- DEP20 ----
            DEP20.setComponentPopupMenu(BTD);
            DEP20.setName("DEP20");
            panel5.add(DEP20);
            DEP20.setBounds(232, 35, 34, DEP20.getPreferredSize().height);

            //---- DEP19 ----
            DEP19.setComponentPopupMenu(BTD);
            DEP19.setName("DEP19");
            panel5.add(DEP19);
            DEP19.setBounds(195, 35, 34, DEP19.getPreferredSize().height);

            //---- DEP18 ----
            DEP18.setComponentPopupMenu(BTD);
            DEP18.setName("DEP18");
            panel5.add(DEP18);
            DEP18.setBounds(158, 35, 34, DEP18.getPreferredSize().height);

            //---- DEP17 ----
            DEP17.setComponentPopupMenu(BTD);
            DEP17.setName("DEP17");
            panel5.add(DEP17);
            DEP17.setBounds(121, 35, 34, DEP17.getPreferredSize().height);

            //---- DEP16 ----
            DEP16.setComponentPopupMenu(BTD);
            DEP16.setName("DEP16");
            panel5.add(DEP16);
            DEP16.setBounds(84, 35, 34, DEP16.getPreferredSize().height);

            //---- DEP15 ----
            DEP15.setComponentPopupMenu(BTD);
            DEP15.setName("DEP15");
            panel5.add(DEP15);
            DEP15.setBounds(47, 35, 34, DEP15.getPreferredSize().height);

            //---- DEP14 ----
            DEP14.setComponentPopupMenu(BTD);
            DEP14.setName("DEP14");
            panel5.add(DEP14);
            DEP14.setBounds(10, 35, 34, DEP14.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(PAYDEB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(PAYFIN, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61)
                .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(CATDEB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(CATFIN, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 470, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(CLINUL, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE))))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PAYDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PAYFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(CLINUL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== OBJ_85 ========
    {
      OBJ_85.setName("OBJ_85");
      OBJ_85.setLayout(null);

      //---- OBJ_86 ----
      OBJ_86.setText("Cette \u00e9dition large est pr\u00e9vue sur papier zon\u00e9. Or, vous souhaitez du 12\" : imposez le CPI 20 \u00e0 votre imprimante.");
      OBJ_86.setName("OBJ_86");
      OBJ_85.add(OBJ_86);
      OBJ_86.setBounds(60, 5, 640, 58);

      //---- OBJ_87 ----
      OBJ_87.setIcon(new ImageIcon("images/msgbox04.gif"));
      OBJ_87.setName("OBJ_87");
      OBJ_85.add(OBJ_87);
      OBJ_87.setBounds(19, 4, 54, 36);

      OBJ_85.setPreferredSize(new Dimension(55, 65));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_23;
  private XRiCheckBox CLINUL;
  private JLabel OBJ_89;
  private JLabel OBJ_88;
  private XRiTextField PAYDEB;
  private XRiTextField PAYFIN;
  private XRiTextField CATDEB;
  private XRiTextField CATFIN;
  private JPanel panel1;
  private JLabel OBJ_92;
  private XRiCalendrier PDEB1;
  private XRiCalendrier PFIN1;
  private JLabel OBJ_93;
  private XRiCalendrier PDEB2;
  private XRiCalendrier PDEB3;
  private JLabel OBJ_94;
  private XRiCalendrier PFIN2;
  private XRiCalendrier PFIN3;
  private JPanel panel2;
  private JLabel OBJ_91;
  private XRiTextField TOPCLI;
  private XRiTextField CLIDEB;
  private XRiTextField CLIFIN;
  private JPanel panel3;
  private XRiTextField GP101;
  private XRiTextField GP102;
  private JLabel OBJ_90;
  private JPanel panel4;
  private XRiTextField ZP201;
  private XRiTextField ZP202;
  private JLabel OBJ_95;
  private XRiTextField ZP203;
  private XRiTextField ZP204;
  private XRiTextField ZP205;
  private XRiTextField ZP206;
  private XRiTextField ZP207;
  private XRiTextField NZPART;
  private XRiTextField WZPART;
  private XRiTextField ZP208;
  private XRiTextField ZP209;
  private XRiTextField ZP210;
  private XRiTextField ZP211;
  private XRiTextField ZP212;
  private JPanel panel5;
  private XRiTextField DEP01;
  private XRiTextField DEP02;
  private XRiTextField DEP03;
  private XRiTextField DEP04;
  private XRiTextField DEP05;
  private XRiTextField DEP06;
  private XRiTextField DEP07;
  private XRiTextField DEP08;
  private XRiTextField DEP09;
  private XRiTextField DEP10;
  private XRiTextField DEP11;
  private XRiTextField DEP12;
  private XRiTextField DEP13;
  private XRiTextField DEP26;
  private XRiTextField DEP25;
  private XRiTextField DEP24;
  private XRiTextField DEP23;
  private XRiTextField DEP22;
  private XRiTextField DEP21;
  private XRiTextField DEP20;
  private XRiTextField DEP19;
  private XRiTextField DEP18;
  private XRiTextField DEP17;
  private XRiTextField DEP16;
  private XRiTextField DEP15;
  private XRiTextField DEP14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private JPanel OBJ_85;
  private JLabel OBJ_86;
  private JLabel OBJ_87;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
