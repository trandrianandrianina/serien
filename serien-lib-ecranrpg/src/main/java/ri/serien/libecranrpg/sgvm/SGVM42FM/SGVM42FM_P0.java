
package ri.serien.libecranrpg.sgvm.SGVM42FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class SGVM42FM_P0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM42FM_P0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    FOLIO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FOLIO@")).trim());
    LIGNE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIGNE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    FOLIO = new RiZoneSortie();
    LIGNE = new RiZoneSortie();
    OBJ_4 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(475, 165));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");

          //---- FOLIO ----
          FOLIO.setText("@FOLIO@");
          FOLIO.setName("FOLIO");

          //---- LIGNE ----
          LIGNE.setText("@LIGNE@");
          LIGNE.setName("LIGNE");

          //---- OBJ_4 ----
          OBJ_4.setText("Traitement en cours");
          OBJ_4.setFont(OBJ_4.getFont().deriveFont(OBJ_4.getFont().getStyle() | Font.BOLD));
          OBJ_4.setName("OBJ_4");

          GroupLayout p_recupLayout = new GroupLayout(p_recup);
          p_recup.setLayout(p_recupLayout);
          p_recupLayout.setHorizontalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(p_recupLayout.createParallelGroup()
                  .addComponent(FOLIO, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
                  .addComponent(LIGNE, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
                .addGap(686, 686, 686))
          );
          p_recupLayout.setVerticalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FOLIO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LIGNE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie FOLIO;
  private RiZoneSortie LIGNE;
  private JLabel OBJ_4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
