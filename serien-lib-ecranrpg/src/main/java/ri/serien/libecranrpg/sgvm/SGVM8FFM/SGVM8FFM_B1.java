
package ri.serien.libecranrpg.sgvm.SGVM8FFM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.EnumCategorieDate;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.affaire.snaffaire.SNAffaire;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2B8] Gestion des ventes -> Documents de ventes -> Traitements commandes -> Etat des ruptures à venir
 * Indicateur : 00000001
 * Titre : Edition des stocks en ruptures à venir
 */
public class SGVM8FFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] WTRI_values = { "1", "2" };
  
  private Message LOCTP = null;
  private String BOUTON_CHOIX_PAPIER = "Choisir papier";
  private ArrayList<EnumCategorieDate> listeEnumCategorieDate = new ArrayList<EnumCategorieDate>();
  
  public SGVM8FFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    WTRI.setValeurs(WTRI_values, null);
    
    // Lié les plages
    snCategorieClientDebut.lierComposantFin(snCategorieClientFin);
    snFamilleDebut.lierComposantFin(snFamilleFin);
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_CHOIX_PAPIER, 'c', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
    // Chargement de la liste des valeurs de l'EnumDatePreEnregister
    for (int i = 0; i < EnumCategorieDate.values().length; i++) {
      listeEnumCategorieDate.add(EnumCategorieDate.values()[i]);
    }
    
    for (EnumCategorieDate categorieTri : listeEnumCategorieDate) {
      cbCategorieDate.addItem(categorieTri);
    }
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibiltié zone personnalisée
    WLIB5.setVisible(!lexique.HostFieldGetData("WLIB5").toString().trim().isEmpty());
    WZP5D.setVisible(WLIB5.isVisible());
    WZP5F.setVisible(WLIB5.isVisible());
    lbAuCinq.setVisible(WLIB5.isVisible());
    lbDuCinq.setVisible(WLIB5.isVisible());
    lbCinq.setVisible(WLIB5.isVisible());
    
    WLIB4.setVisible(!lexique.HostFieldGetData("WLIB4").toString().trim().isEmpty());
    WZP4D.setVisible(WLIB4.isVisible());
    WZP4F.setVisible(WLIB4.isVisible());
    lbAuQuatre.setVisible(WLIB4.isVisible());
    lbDuQuatre.setVisible(WLIB4.isVisible());
    lbQuatre.setVisible(WLIB4.isVisible());
    
    WLIB3.setVisible(!lexique.HostFieldGetData("WLIB3").toString().trim().isEmpty());
    WZP3D.setVisible(WLIB3.isVisible());
    WZP3F.setVisible(WLIB3.isVisible());
    lbAuTrois.setVisible(WLIB3.isVisible());
    lbDuTrois.setVisible(WLIB3.isVisible());
    lbTrois.setVisible(WLIB3.isVisible());
    
    WLIB2.setVisible(!lexique.HostFieldGetData("WLIB2").toString().trim().isEmpty());
    WZP2D.setVisible(WLIB2.isVisible());
    WZP2F.setVisible(WLIB2.isVisible());
    lbAuDeux.setVisible(WLIB2.isVisible());
    lbDuDeux.setVisible(WLIB2.isVisible());
    lbDeux.setVisible(WLIB2.isVisible());
    
    WLIB1.setVisible(!lexique.HostFieldGetData("WLIB1").toString().trim().isEmpty());
    WZP1D.setVisible(WLIB1.isVisible());
    WZP1F.setVisible(WLIB1.isVisible());
    lbAuUn.setVisible(WLIB1.isVisible());
    lbDuUn.setVisible(WLIB1.isVisible());
    lbUn.setVisible(WLIB1.isVisible());
    
    pnlSelectionZonePersonnalise
        .setVisible(WLIB1.isVisible() || WLIB2.isVisible() || WLIB3.isVisible() || WLIB4.isVisible() || WLIB5.isVisible());
    
    // Disponibiltié des composant
    WCODF.setEnabled(lexique.isPresent("WCODF"));
    WCODD.setEnabled(lexique.isPresent("WCODD"));
    WNUMF.setEnabled(lexique.isPresent("WNUMF"));
    WNUMD.setEnabled(lexique.isPresent("WNUMD"));
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Charge l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charge le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // Charge l'affaires
    snAffaire.setSession(getSession());
    snAffaire.setIdEtablissement(snEtablissement.getIdSelection());
    snAffaire.setTousAutorise(true);
    snAffaire.charger(false);
    snAffaire.setSelectionParChampRPG(lexique, "WACT");
    
    // Charge les catégorie client
    snCategorieClientDebut.setSession(getSession());
    snCategorieClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientDebut.setTousAutorise(true);
    snCategorieClientDebut.charger(false);
    snCategorieClientDebut.setSelectionParChampRPG(lexique, "WCATD");
    
    snCategorieClientFin.setSession(getSession());
    snCategorieClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientFin.setTousAutorise(true);
    snCategorieClientFin.charger(false);
    snCategorieClientFin.setSelectionParChampRPG(lexique, "WCATF");
    
    // Charge les familles
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snAffaire.renseignerChampRPG(lexique, "WACT");
    snCategorieClientDebut.renseignerChampRPG(lexique, "WCATD");
    snCategorieClientFin.renseignerChampRPG(lexique, "WCATF");
    snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
    snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    // Sélection de la date de livraison
    lexique.HostFieldPutData("WTDL", 0, ((EnumCategorieDate) cbCategorieDate.getSelectedItem()).getCode());
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CHOIX_PAPIER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbDateLivraison = new SNLabelChamp();
    pnlDateLivraison = new SNPanel();
    WDATD = new XRiCalendrier();
    lbAuDateLivraison = new SNLabelChamp();
    WDATF = new XRiCalendrier();
    lbCategorieLivraison = new SNLabelChamp();
    cbCategorieDate = new SNComboBox();
    lbSelectionBon = new SNLabelChamp();
    pnlSelectionBon = new SNPanel();
    WCODD = new XRiTextField();
    lbASelectionBon = new SNLabelChamp();
    WCODF = new XRiTextField();
    lbCommandeATraiter = new SNLabelChamp();
    pnlCommandeATraiter = new SNPanel();
    WNUMD = new XRiTextField();
    lbACommandeTraiter = new SNLabelChamp();
    WNUMF = new XRiTextField();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbAffaire = new SNLabelChamp();
    snAffaire = new SNAffaire();
    lbCategorieClientDebut = new SNLabelChamp();
    snCategorieClientDebut = new SNCategorieClient();
    lbCategorieClientFin = new SNLabelChamp();
    snCategorieClientFin = new SNCategorieClient();
    lbFamilleArticle = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    sNLabelChamp1 = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlSelectionZonePersonnalise = new SNPanelTitre();
    WLIB1 = new XRiTextField();
    WZP1D = new XRiTextField();
    WZP1F = new XRiTextField();
    WLIB2 = new XRiTextField();
    WZP2D = new XRiTextField();
    WZP2F = new XRiTextField();
    WLIB3 = new XRiTextField();
    WZP3D = new XRiTextField();
    WZP3F = new XRiTextField();
    WLIB4 = new XRiTextField();
    WZP4D = new XRiTextField();
    WZP4F = new XRiTextField();
    WLIB5 = new XRiTextField();
    WZP5D = new XRiTextField();
    WZP5F = new XRiTextField();
    lbUn = new SNLabelChamp();
    lbDeux = new SNLabelChamp();
    lbAuTrois = new SNLabelChamp();
    lbQuatre = new SNLabelChamp();
    lbCinq = new SNLabelChamp();
    lbDuUn = new SNLabelChamp();
    lbAuUn = new SNLabelChamp();
    lbDuDeux = new SNLabelChamp();
    lbAuDeux = new SNLabelChamp();
    lbTrois = new SNLabelChamp();
    lbDuTrois = new SNLabelChamp();
    lbDuQuatre = new SNLabelChamp();
    lbAuQuatre = new SNLabelChamp();
    lbDuCinq = new SNLabelChamp();
    lbAuCinq = new SNLabelChamp();
    pnlOptionTri = new SNPanelTitre();
    WRES = new XRiCheckBox();
    lbCommandesRéservées2 = new SNLabelChamp();
    WTRI = new XRiComboBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des stocks en ruptures \u00e0 venir");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout(1, 2));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateLivraison ----
            lbDateLivraison.setText("Date de livraison du");
            lbDateLivraison.setName("lbDateLivraison");
            pnlCritereDeSelection.add(lbDateLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateLivraison ========
            {
              pnlDateLivraison.setName("pnlDateLivraison");
              pnlDateLivraison.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDATD ----
              WDATD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATD.setPreferredSize(new Dimension(110, 30));
              WDATD.setMinimumSize(new Dimension(110, 30));
              WDATD.setMaximumSize(new Dimension(110, 30));
              WDATD.setName("WDATD");
              pnlDateLivraison.add(WDATD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAuDateLivraison ----
              lbAuDateLivraison.setText("au");
              lbAuDateLivraison.setPreferredSize(new Dimension(16, 30));
              lbAuDateLivraison.setMinimumSize(new Dimension(16, 30));
              lbAuDateLivraison.setMaximumSize(new Dimension(16, 30));
              lbAuDateLivraison.setName("lbAuDateLivraison");
              pnlDateLivraison.add(lbAuDateLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDATF ----
              WDATF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATF.setPreferredSize(new Dimension(110, 30));
              WDATF.setMinimumSize(new Dimension(110, 30));
              WDATF.setMaximumSize(new Dimension(110, 30));
              WDATF.setName("WDATF");
              pnlDateLivraison.add(WDATF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlDateLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieLivraison ----
            lbCategorieLivraison.setText("Cat\u00e9gorie date de livraison");
            lbCategorieLivraison.setPreferredSize(new Dimension(170, 30));
            lbCategorieLivraison.setMinimumSize(new Dimension(170, 30));
            lbCategorieLivraison.setMaximumSize(new Dimension(170, 30));
            lbCategorieLivraison.setName("lbCategorieLivraison");
            pnlCritereDeSelection.add(lbCategorieLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbCategorieDate ----
            cbCategorieDate.setName("cbCategorieDate");
            pnlCritereDeSelection.add(cbCategorieDate, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSelectionBon ----
            lbSelectionBon.setText("S\u00e9lection sur bon de");
            lbSelectionBon.setName("lbSelectionBon");
            pnlCritereDeSelection.add(lbSelectionBon, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlSelectionBon ========
            {
              pnlSelectionBon.setName("pnlSelectionBon");
              pnlSelectionBon.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSelectionBon.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlSelectionBon.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionBon.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlSelectionBon.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WCODD ----
              WCODD.setComponentPopupMenu(null);
              WCODD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WCODD.setPreferredSize(new Dimension(24, 30));
              WCODD.setMinimumSize(new Dimension(24, 30));
              WCODD.setMaximumSize(new Dimension(24, 30));
              WCODD.setName("WCODD");
              pnlSelectionBon.add(WCODD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbASelectionBon ----
              lbASelectionBon.setText("\u00e0");
              lbASelectionBon.setPreferredSize(new Dimension(8, 30));
              lbASelectionBon.setMinimumSize(new Dimension(8, 30));
              lbASelectionBon.setMaximumSize(new Dimension(8, 30));
              lbASelectionBon.setName("lbASelectionBon");
              pnlSelectionBon.add(lbASelectionBon, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WCODF ----
              WCODF.setComponentPopupMenu(null);
              WCODF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WCODF.setPreferredSize(new Dimension(24, 30));
              WCODF.setMinimumSize(new Dimension(24, 30));
              WCODF.setMaximumSize(new Dimension(24, 30));
              WCODF.setName("WCODF");
              pnlSelectionBon.add(WCODF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlSelectionBon, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCommandeATraiter ----
            lbCommandeATraiter.setText("Commande \u00e0 traiter de");
            lbCommandeATraiter.setPreferredSize(new Dimension(167, 30));
            lbCommandeATraiter.setMinimumSize(new Dimension(167, 30));
            lbCommandeATraiter.setMaximumSize(new Dimension(167, 30));
            lbCommandeATraiter.setName("lbCommandeATraiter");
            pnlCritereDeSelection.add(lbCommandeATraiter, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlCommandeATraiter ========
            {
              pnlCommandeATraiter.setName("pnlCommandeATraiter");
              pnlCommandeATraiter.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlCommandeATraiter.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WNUMD ----
              WNUMD.setComponentPopupMenu(null);
              WNUMD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNUMD.setPreferredSize(new Dimension(70, 30));
              WNUMD.setMinimumSize(new Dimension(70, 30));
              WNUMD.setMaximumSize(new Dimension(70, 30));
              WNUMD.setName("WNUMD");
              pnlCommandeATraiter.add(WNUMD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbACommandeTraiter ----
              lbACommandeTraiter.setText("\u00e0");
              lbACommandeTraiter.setMinimumSize(new Dimension(8, 30));
              lbACommandeTraiter.setMaximumSize(new Dimension(8, 30));
              lbACommandeTraiter.setPreferredSize(new Dimension(8, 30));
              lbACommandeTraiter.setName("lbACommandeTraiter");
              pnlCommandeATraiter.add(lbACommandeTraiter, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WNUMF ----
              WNUMF.setComponentPopupMenu(null);
              WNUMF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNUMF.setPreferredSize(new Dimension(70, 30));
              WNUMF.setMinimumSize(new Dimension(70, 30));
              WNUMF.setMaximumSize(new Dimension(70, 30));
              WNUMF.setName("WNUMF");
              pnlCommandeATraiter.add(WNUMF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlCommandeATraiter, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlCritereDeSelection.add(lbMagasin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setName("snMagasin");
            pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAffaire ----
            lbAffaire.setText("Affaire");
            lbAffaire.setName("lbAffaire");
            pnlCritereDeSelection.add(lbAffaire, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snAffaire ----
            snAffaire.setFont(new Font("sansserif", Font.PLAIN, 14));
            snAffaire.setName("snAffaire");
            pnlCritereDeSelection.add(snAffaire, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieClientDebut ----
            lbCategorieClientDebut.setText("Cat\u00e9gorie client de d\u00e9but");
            lbCategorieClientDebut.setName("lbCategorieClientDebut");
            pnlCritereDeSelection.add(lbCategorieClientDebut, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCategorieClientDebut ----
            snCategorieClientDebut.setName("snCategorieClientDebut");
            pnlCritereDeSelection.add(snCategorieClientDebut, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieClientFin ----
            lbCategorieClientFin.setText("Cat\u00e9gorie client de fin");
            lbCategorieClientFin.setName("lbCategorieClientFin");
            pnlCritereDeSelection.add(lbCategorieClientFin, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCategorieClientFin ----
            snCategorieClientFin.setName("snCategorieClientFin");
            pnlCritereDeSelection.add(snCategorieClientFin, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleArticle ----
            lbFamilleArticle.setText("Famille article de d\u00e9but");
            lbFamilleArticle.setName("lbFamilleArticle");
            pnlCritereDeSelection.add(lbFamilleArticle, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereDeSelection.add(snFamilleDebut, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp1 ----
            sNLabelChamp1.setText("Famille article de fin");
            sNLabelChamp1.setName("sNLabelChamp1");
            pnlCritereDeSelection.add(sNLabelChamp1, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snFamilleFin ----
            snFamilleFin.setName("snFamilleFin");
            pnlCritereDeSelection.add(snFamilleFin, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlSelectionZonePersonnalise ========
          {
            pnlSelectionZonePersonnalise.setOpaque(false);
            pnlSelectionZonePersonnalise.setTitre("S\u00e9lection sur zone personnalis\u00e9e");
            pnlSelectionZonePersonnalise.setName("pnlSelectionZonePersonnalise");
            pnlSelectionZonePersonnalise.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSelectionZonePersonnalise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlSelectionZonePersonnalise.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlSelectionZonePersonnalise.getLayout()).columnWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlSelectionZonePersonnalise.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- WLIB1 ----
            WLIB1.setComponentPopupMenu(null);
            WLIB1.setFont(new Font("sansserif", Font.PLAIN, 14));
            WLIB1.setPreferredSize(new Dimension(284, 30));
            WLIB1.setMinimumSize(new Dimension(284, 30));
            WLIB1.setMaximumSize(new Dimension(284, 30));
            WLIB1.setName("WLIB1");
            pnlSelectionZonePersonnalise.add(WLIB1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP1D ----
            WZP1D.setComponentPopupMenu(null);
            WZP1D.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZP1D.setPreferredSize(new Dimension(36, 30));
            WZP1D.setMinimumSize(new Dimension(36, 30));
            WZP1D.setMaximumSize(new Dimension(36, 30));
            WZP1D.setName("WZP1D");
            pnlSelectionZonePersonnalise.add(WZP1D, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP1F ----
            WZP1F.setComponentPopupMenu(null);
            WZP1F.setPreferredSize(new Dimension(36, 30));
            WZP1F.setMinimumSize(new Dimension(36, 30));
            WZP1F.setMaximumSize(new Dimension(36, 30));
            WZP1F.setName("WZP1F");
            pnlSelectionZonePersonnalise.add(WZP1F, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WLIB2 ----
            WLIB2.setComponentPopupMenu(null);
            WLIB2.setFont(new Font("sansserif", Font.PLAIN, 14));
            WLIB2.setPreferredSize(new Dimension(284, 30));
            WLIB2.setMinimumSize(new Dimension(284, 30));
            WLIB2.setMaximumSize(new Dimension(284, 30));
            WLIB2.setName("WLIB2");
            pnlSelectionZonePersonnalise.add(WLIB2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP2D ----
            WZP2D.setComponentPopupMenu(null);
            WZP2D.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZP2D.setPreferredSize(new Dimension(36, 30));
            WZP2D.setMinimumSize(new Dimension(36, 30));
            WZP2D.setMaximumSize(new Dimension(36, 30));
            WZP2D.setName("WZP2D");
            pnlSelectionZonePersonnalise.add(WZP2D, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP2F ----
            WZP2F.setComponentPopupMenu(null);
            WZP2F.setPreferredSize(new Dimension(36, 30));
            WZP2F.setMinimumSize(new Dimension(36, 30));
            WZP2F.setMaximumSize(new Dimension(36, 30));
            WZP2F.setName("WZP2F");
            pnlSelectionZonePersonnalise.add(WZP2F, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WLIB3 ----
            WLIB3.setComponentPopupMenu(null);
            WLIB3.setFont(new Font("sansserif", Font.PLAIN, 14));
            WLIB3.setPreferredSize(new Dimension(284, 30));
            WLIB3.setMinimumSize(new Dimension(284, 30));
            WLIB3.setMaximumSize(new Dimension(284, 30));
            WLIB3.setName("WLIB3");
            pnlSelectionZonePersonnalise.add(WLIB3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP3D ----
            WZP3D.setComponentPopupMenu(null);
            WZP3D.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZP3D.setPreferredSize(new Dimension(36, 30));
            WZP3D.setMinimumSize(new Dimension(36, 30));
            WZP3D.setMaximumSize(new Dimension(36, 30));
            WZP3D.setName("WZP3D");
            pnlSelectionZonePersonnalise.add(WZP3D, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP3F ----
            WZP3F.setComponentPopupMenu(null);
            WZP3F.setPreferredSize(new Dimension(36, 30));
            WZP3F.setMinimumSize(new Dimension(36, 30));
            WZP3F.setMaximumSize(new Dimension(36, 30));
            WZP3F.setName("WZP3F");
            pnlSelectionZonePersonnalise.add(WZP3F, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WLIB4 ----
            WLIB4.setComponentPopupMenu(null);
            WLIB4.setFont(new Font("sansserif", Font.PLAIN, 14));
            WLIB4.setPreferredSize(new Dimension(284, 30));
            WLIB4.setMinimumSize(new Dimension(284, 30));
            WLIB4.setMaximumSize(new Dimension(284, 30));
            WLIB4.setName("WLIB4");
            pnlSelectionZonePersonnalise.add(WLIB4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP4D ----
            WZP4D.setComponentPopupMenu(null);
            WZP4D.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZP4D.setPreferredSize(new Dimension(36, 30));
            WZP4D.setMinimumSize(new Dimension(36, 30));
            WZP4D.setMaximumSize(new Dimension(36, 30));
            WZP4D.setName("WZP4D");
            pnlSelectionZonePersonnalise.add(WZP4D, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZP4F ----
            WZP4F.setComponentPopupMenu(null);
            WZP4F.setPreferredSize(new Dimension(36, 30));
            WZP4F.setMinimumSize(new Dimension(36, 30));
            WZP4F.setMaximumSize(new Dimension(36, 30));
            WZP4F.setName("WZP4F");
            pnlSelectionZonePersonnalise.add(WZP4F, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WLIB5 ----
            WLIB5.setComponentPopupMenu(null);
            WLIB5.setFont(new Font("sansserif", Font.PLAIN, 14));
            WLIB5.setPreferredSize(new Dimension(284, 30));
            WLIB5.setMinimumSize(new Dimension(284, 30));
            WLIB5.setMaximumSize(new Dimension(284, 30));
            WLIB5.setName("WLIB5");
            pnlSelectionZonePersonnalise.add(WLIB5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WZP5D ----
            WZP5D.setComponentPopupMenu(null);
            WZP5D.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZP5D.setPreferredSize(new Dimension(36, 30));
            WZP5D.setMinimumSize(new Dimension(36, 30));
            WZP5D.setMaximumSize(new Dimension(36, 30));
            WZP5D.setName("WZP5D");
            pnlSelectionZonePersonnalise.add(WZP5D, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WZP5F ----
            WZP5F.setComponentPopupMenu(null);
            WZP5F.setPreferredSize(new Dimension(36, 30));
            WZP5F.setMinimumSize(new Dimension(36, 30));
            WZP5F.setMaximumSize(new Dimension(36, 30));
            WZP5F.setName("WZP5F");
            pnlSelectionZonePersonnalise.add(WZP5F, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbUn ----
            lbUn.setText("1-");
            lbUn.setPreferredSize(new Dimension(13, 30));
            lbUn.setMinimumSize(new Dimension(13, 30));
            lbUn.setMaximumSize(new Dimension(13, 30));
            lbUn.setName("lbUn");
            pnlSelectionZonePersonnalise.add(lbUn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDeux ----
            lbDeux.setText("2-");
            lbDeux.setMaximumSize(new Dimension(13, 30));
            lbDeux.setMinimumSize(new Dimension(13, 30));
            lbDeux.setPreferredSize(new Dimension(13, 30));
            lbDeux.setName("lbDeux");
            pnlSelectionZonePersonnalise.add(lbDeux, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbAuTrois ----
            lbAuTrois.setText("3-");
            lbAuTrois.setMaximumSize(new Dimension(13, 30));
            lbAuTrois.setMinimumSize(new Dimension(13, 30));
            lbAuTrois.setPreferredSize(new Dimension(13, 30));
            lbAuTrois.setName("lbAuTrois");
            pnlSelectionZonePersonnalise.add(lbAuTrois, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbQuatre ----
            lbQuatre.setText("4-");
            lbQuatre.setMaximumSize(new Dimension(13, 30));
            lbQuatre.setMinimumSize(new Dimension(13, 30));
            lbQuatre.setPreferredSize(new Dimension(13, 30));
            lbQuatre.setName("lbQuatre");
            pnlSelectionZonePersonnalise.add(lbQuatre, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbCinq ----
            lbCinq.setText("5-");
            lbCinq.setMaximumSize(new Dimension(13, 30));
            lbCinq.setMinimumSize(new Dimension(13, 30));
            lbCinq.setPreferredSize(new Dimension(13, 30));
            lbCinq.setName("lbCinq");
            pnlSelectionZonePersonnalise.add(lbCinq, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbDuUn ----
            lbDuUn.setText("du");
            lbDuUn.setPreferredSize(new Dimension(16, 30));
            lbDuUn.setMinimumSize(new Dimension(16, 30));
            lbDuUn.setMaximumSize(new Dimension(16, 30));
            lbDuUn.setName("lbDuUn");
            pnlSelectionZonePersonnalise.add(lbDuUn, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbAuUn ----
            lbAuUn.setText("au");
            lbAuUn.setPreferredSize(new Dimension(16, 30));
            lbAuUn.setMinimumSize(new Dimension(16, 30));
            lbAuUn.setMaximumSize(new Dimension(16, 30));
            lbAuUn.setName("lbAuUn");
            pnlSelectionZonePersonnalise.add(lbAuUn, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDuDeux ----
            lbDuDeux.setText("du");
            lbDuDeux.setPreferredSize(new Dimension(16, 30));
            lbDuDeux.setMinimumSize(new Dimension(16, 30));
            lbDuDeux.setMaximumSize(new Dimension(16, 30));
            lbDuDeux.setName("lbDuDeux");
            pnlSelectionZonePersonnalise.add(lbDuDeux, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbAuDeux ----
            lbAuDeux.setText("au");
            lbAuDeux.setPreferredSize(new Dimension(16, 30));
            lbAuDeux.setMinimumSize(new Dimension(16, 30));
            lbAuDeux.setMaximumSize(new Dimension(16, 30));
            lbAuDeux.setName("lbAuDeux");
            pnlSelectionZonePersonnalise.add(lbAuDeux, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTrois ----
            lbTrois.setText("du");
            lbTrois.setPreferredSize(new Dimension(16, 30));
            lbTrois.setMinimumSize(new Dimension(16, 30));
            lbTrois.setMaximumSize(new Dimension(16, 30));
            lbTrois.setName("lbTrois");
            pnlSelectionZonePersonnalise.add(lbTrois, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDuTrois ----
            lbDuTrois.setText("au");
            lbDuTrois.setPreferredSize(new Dimension(16, 30));
            lbDuTrois.setMinimumSize(new Dimension(16, 30));
            lbDuTrois.setMaximumSize(new Dimension(16, 30));
            lbDuTrois.setName("lbDuTrois");
            pnlSelectionZonePersonnalise.add(lbDuTrois, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDuQuatre ----
            lbDuQuatre.setText("du");
            lbDuQuatre.setPreferredSize(new Dimension(16, 30));
            lbDuQuatre.setMinimumSize(new Dimension(16, 30));
            lbDuQuatre.setMaximumSize(new Dimension(16, 30));
            lbDuQuatre.setName("lbDuQuatre");
            pnlSelectionZonePersonnalise.add(lbDuQuatre, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbAuQuatre ----
            lbAuQuatre.setText("au");
            lbAuQuatre.setPreferredSize(new Dimension(16, 30));
            lbAuQuatre.setMinimumSize(new Dimension(16, 30));
            lbAuQuatre.setMaximumSize(new Dimension(16, 30));
            lbAuQuatre.setName("lbAuQuatre");
            pnlSelectionZonePersonnalise.add(lbAuQuatre, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDuCinq ----
            lbDuCinq.setText("du");
            lbDuCinq.setPreferredSize(new Dimension(16, 30));
            lbDuCinq.setMinimumSize(new Dimension(16, 30));
            lbDuCinq.setMaximumSize(new Dimension(16, 30));
            lbDuCinq.setName("lbDuCinq");
            pnlSelectionZonePersonnalise.add(lbDuCinq, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbAuCinq ----
            lbAuCinq.setText("au");
            lbAuCinq.setPreferredSize(new Dimension(16, 30));
            lbAuCinq.setMinimumSize(new Dimension(16, 30));
            lbAuCinq.setMaximumSize(new Dimension(16, 30));
            lbAuCinq.setName("lbAuCinq");
            pnlSelectionZonePersonnalise.add(lbAuCinq, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlSelectionZonePersonnalise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionTri ========
          {
            pnlOptionTri.setOpaque(false);
            pnlOptionTri.setTitre("Option de tri");
            pnlOptionTri.setName("pnlOptionTri");
            pnlOptionTri.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionTri.getLayout()).columnWidths = new int[] { 288, 0, 0 };
            ((GridBagLayout) pnlOptionTri.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionTri.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionTri.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- WRES ----
            WRES.setText("Commandes r\u00e9serv\u00e9es uniquement");
            WRES.setComponentPopupMenu(null);
            WRES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WRES.setFont(new Font("sansserif", Font.PLAIN, 14));
            WRES.setName("WRES");
            pnlOptionTri.add(WRES, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbCommandesRéservées2 ----
            lbCommandesRéservées2.setText("Option de tri");
            lbCommandesRéservées2.setPreferredSize(new Dimension(170, 30));
            lbCommandesRéservées2.setMinimumSize(new Dimension(170, 30));
            lbCommandesRéservées2.setMaximumSize(new Dimension(170, 30));
            lbCommandesRéservées2.setName("lbCommandesR\u00e9serv\u00e9es2");
            pnlOptionTri.add(lbCommandesRéservées2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WTRI ----
            WTRI.setModel(
                new DefaultComboBoxModel<>(new String[] { "Edition tri\u00e9e par famille", "Edition tri\u00e9e par article" }));
            WTRI.setName("WTRI");
            pnlOptionTri.add(WTRI, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionTri, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbDateLivraison;
  private SNPanel pnlDateLivraison;
  private XRiCalendrier WDATD;
  private SNLabelChamp lbAuDateLivraison;
  private XRiCalendrier WDATF;
  private SNLabelChamp lbCategorieLivraison;
  private SNComboBox cbCategorieDate;
  private SNLabelChamp lbSelectionBon;
  private SNPanel pnlSelectionBon;
  private XRiTextField WCODD;
  private SNLabelChamp lbASelectionBon;
  private XRiTextField WCODF;
  private SNLabelChamp lbCommandeATraiter;
  private SNPanel pnlCommandeATraiter;
  private XRiTextField WNUMD;
  private SNLabelChamp lbACommandeTraiter;
  private XRiTextField WNUMF;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbAffaire;
  private SNAffaire snAffaire;
  private SNLabelChamp lbCategorieClientDebut;
  private SNCategorieClient snCategorieClientDebut;
  private SNLabelChamp lbCategorieClientFin;
  private SNCategorieClient snCategorieClientFin;
  private SNLabelChamp lbFamilleArticle;
  private SNFamille snFamilleDebut;
  private SNLabelChamp sNLabelChamp1;
  private SNFamille snFamilleFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlSelectionZonePersonnalise;
  private XRiTextField WLIB1;
  private XRiTextField WZP1D;
  private XRiTextField WZP1F;
  private XRiTextField WLIB2;
  private XRiTextField WZP2D;
  private XRiTextField WZP2F;
  private XRiTextField WLIB3;
  private XRiTextField WZP3D;
  private XRiTextField WZP3F;
  private XRiTextField WLIB4;
  private XRiTextField WZP4D;
  private XRiTextField WZP4F;
  private XRiTextField WLIB5;
  private XRiTextField WZP5D;
  private XRiTextField WZP5F;
  private SNLabelChamp lbUn;
  private SNLabelChamp lbDeux;
  private SNLabelChamp lbAuTrois;
  private SNLabelChamp lbQuatre;
  private SNLabelChamp lbCinq;
  private SNLabelChamp lbDuUn;
  private SNLabelChamp lbAuUn;
  private SNLabelChamp lbDuDeux;
  private SNLabelChamp lbAuDeux;
  private SNLabelChamp lbTrois;
  private SNLabelChamp lbDuTrois;
  private SNLabelChamp lbDuQuatre;
  private SNLabelChamp lbAuQuatre;
  private SNLabelChamp lbDuCinq;
  private SNLabelChamp lbAuCinq;
  private SNPanelTitre pnlOptionTri;
  private XRiCheckBox WRES;
  private SNLabelChamp lbCommandesRéservées2;
  private XRiComboBox WTRI;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
