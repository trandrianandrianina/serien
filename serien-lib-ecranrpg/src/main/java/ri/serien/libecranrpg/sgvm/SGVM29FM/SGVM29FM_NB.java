
package ri.serien.libecranrpg.sgvm.SGVM29FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM29FM_NB extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM29FM_NB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfNombreDePages.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NPAGE@")).trim());
    tfNombreDeBons.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBBON@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    setTitle("@TITLE@");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    sNPanelTitre1 = new SNPanelTitre();
    lbNombreDePages = new SNLabelChamp();
    tfNombreDePages = new SNTexte();
    lbNombreDeBons = new SNLabelChamp();
    tfNombreDeBons = new SNTexte();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(350, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout(1, 1));

      //======== sNPanelTitre1 ========
      {
        sNPanelTitre1.setName("sNPanelTitre1");
        sNPanelTitre1.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanelTitre1.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)sNPanelTitre1.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)sNPanelTitre1.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)sNPanelTitre1.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbNombreDePages ----
        lbNombreDePages.setText("Nombre de pages");
        lbNombreDePages.setName("lbNombreDePages");
        sNPanelTitre1.add(lbNombreDePages, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- tfNombreDePages ----
        tfNombreDePages.setMinimumSize(new Dimension(40, 30));
        tfNombreDePages.setPreferredSize(new Dimension(40, 30));
        tfNombreDePages.setMaximumSize(new Dimension(40, 30));
        tfNombreDePages.setEditable(false);
        tfNombreDePages.setEnabled(false);
        tfNombreDePages.setText("@NPAGE@");
        tfNombreDePages.setName("tfNombreDePages");
        sNPanelTitre1.add(tfNombreDePages, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNombreDeBons ----
        lbNombreDeBons.setText("Nombre de bons");
        lbNombreDeBons.setName("lbNombreDeBons");
        sNPanelTitre1.add(lbNombreDeBons, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- tfNombreDeBons ----
        tfNombreDeBons.setEditable(false);
        tfNombreDeBons.setEnabled(false);
        tfNombreDeBons.setText("@NBBON@");
        tfNombreDeBons.setName("tfNombreDeBons");
        sNPanelTitre1.add(tfNombreDeBons, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(sNPanelTitre1);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp lbNombreDePages;
  private SNTexte tfNombreDePages;
  private SNLabelChamp lbNombreDeBons;
  private SNTexte tfNombreDeBons;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
