
package ri.serien.libecranrpg.sgvm.SGVM66FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2924] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Bons suspects
 * Indicateur : 00100001
 * Titre : Edition des bons de ventes suspects
 * 
 * [GVM2925] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Bons suspects / section
 * analytique
 * Indicateur : 10100001
 * Titre : Edition des bons de ventes suspects par section analytique
 */
public class SGVM66FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private boolean isPlanning = false;
  private String[] cbCodeDate_Value = { "*DAT", "*ENC", "*PRE", };
  private String[] cbCodeDate_Text = { "Date du jour", "Mois en cours", "Mois précédant", };
  private Message LOCTP;
  
  public SGVM66FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    cbCodeDate.removeAllItems();
    for (String texte : cbCodeDate_Text) {
      cbCodeDate.addItem(texte);
    }
    
    // Liée les composants pour les plages de sélection
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snMagasinDebut.lierComposantFin(snMagasinFin);
    
    CRI1.setValeursSelection("1", " ");
    CRI2.setValeursSelection("1", " ");
    CRI3.setValeursSelection("1", " ");
    CRI4.setValeursSelection("1", " ");
    CRI5.setValeursSelection("1", " ");
    CRI6.setValeursSelection("1", " ");
    CRI7.setValeursSelection("1", " ");
    CRI8.setValeursSelection("1", " ");
    CRI9.setValeursSelection("1", " ");
    CRI10.setValeursSelection("1", " ");
    CRI11.setValeursSelection("1", " ");
    CRI12.setValeursSelection("1", " ");
    OPT3.setValeursSelection("1", " ");
    OPT2.setValeursSelection("1", " ");
    OPT1.setValeursSelection("1", " ");
    ERED.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Indicateur
    Boolean is91 = lexique.isTrue("91");
    Boolean is94 = lexique.isTrue("94");
    
    isPlanning = lexique.isTrue("90");
    
    cbCodeDate.setVisible(isPlanning);
    lbPlanning.setVisible(isPlanning);
    PERDEB.setVisible(!isPlanning);
    PERFIN.setVisible(!isPlanning);
    lbAu.setVisible(!isPlanning);
    if (isPlanning) {
      cbCodeDate.setSelectedItem(lexique.HostFieldGetData("WCPER"));
    }
    
    // Mets le bon titre suivant le pts de menu
    if (is91) {
      bpPresentation.setText("Edition des bons de ventes suspects par section analytique");
    }
    else {
      bpPresentation.setText("Edition des bons de ventes suspects");
    }
    MTINF.setEnabled(lexique.isPresent("MTINF"));
    MTSUP.setEnabled(lexique.isPresent("MTSUP"));
    
    // Mets le composant suivant la situation
    lbSection.setText("Section analytique");
    if (is91 && is94) {
      lbSection.setVisible(true);
      snSectionAnalytique.setVisible(true);
    }
    else {
      lbSection.setVisible(false);
      snSectionAnalytique.setVisible(false);
    }
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Charger les composants
    chargerFamille();
    chargerMagasin();
    chargerSectionAnalytique();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    if (isPlanning) {
      lexique.HostFieldPutData("WCPER", 0, cbCodeDate_Value[cbCodeDate.getSelectedIndex()]);
    }
    
    if (snMagasinDebut.getIdSelection() == null && snMagasinFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTMAG", 0, "**");
    }
    else {
      snMagasinDebut.renseignerChampRPG(lexique, "MAGDEB");
      snMagasinFin.renseignerChampRPG(lexique, "MAGFIN");
      lexique.HostFieldPutData("WTMAG", 0, "  ");
    }
    if (snFamilleDebut.getIdSelection() == null && snFamilleFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTFAM", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTFAM", 0, "  ");
      snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
      snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    }
    if (snSectionAnalytique.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOUS", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOUS", 0, "  ");
      snSectionAnalytique.renseignerChampRPG(lexique, "SAN");
    }
  }
  
  /**
   * Charger les magasins
   */
  private void chargerMagasin() {
    snMagasinDebut.setSession(getSession());
    snMagasinDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinDebut.setTousAutorise(true);
    snMagasinDebut.charger(false);
    snMagasinDebut.setSelectionParChampRPG(lexique, "MAGDEB");
    
    snMagasinFin.setSession(getSession());
    snMagasinFin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinFin.setTousAutorise(true);
    snMagasinFin.charger(false);
    snMagasinFin.setSelectionParChampRPG(lexique, "MAGFIN");
  }
  
  /**
   * Charger les familles
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  /**
   * Charger les section analytique
   */
  private void chargerSectionAnalytique() {
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "SAN");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbPeriode = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    cbCodeDate = new SNComboBox();
    lbSection = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    lbCodeFamilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbCodeFamilleFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbCodeMagasinDebut = new SNLabelChamp();
    snMagasinDebut = new SNMagasin();
    lbCodeMagasinFin = new SNLabelChamp();
    snMagasinFin = new SNMagasin();
    pnlBonEditer = new SNPanelTitre();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    pnlOptionEdition = new SNPanelTitre();
    ERED = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlControlesEffectuer = new SNPanelTitre();
    CRI12 = new XRiCheckBox();
    pnlMargeInférieur = new SNPanel();
    CRI3 = new XRiCheckBox();
    PMAR = new XRiTextField();
    lbPourcentInferieur = new SNLabelUnite();
    pnlMargeSuperieur = new SNPanel();
    CRI11 = new XRiCheckBox();
    PMARS = new XRiTextField();
    lbPourcentSuperieur = new SNLabelUnite();
    CRI1 = new XRiCheckBox();
    CRI2 = new XRiCheckBox();
    CRI10 = new XRiCheckBox();
    pnlMontantSuperieur = new SNPanel();
    CRI4 = new XRiCheckBox();
    MTSUP = new XRiTextField();
    pnlMontantInferieur = new SNPanel();
    CRI5 = new XRiCheckBox();
    MTINF = new XRiTextField();
    CRI6 = new XRiCheckBox();
    CRI9 = new XRiCheckBox();
    CRI8 = new XRiCheckBox();
    CRI7 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setPreferredSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPlanning ----
        lbPlanning.setText("Vous \u00eates en cours de param\u00e9trage en vue de la mise au planning");
        lbPlanning.setMinimumSize(new Dimension(120, 30));
        lbPlanning.setPreferredSize(new Dimension(120, 30));
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode \u00e0 \u00e9diter");
            lbPeriode.setPreferredSize(new Dimension(200, 30));
            lbPeriode.setMinimumSize(new Dimension(200, 30));
            lbPeriode.setMaximumSize(new Dimension(200, 30));
            lbPeriode.setName("lbPeriode");
            pnlCritereDeSelection.add(lbPeriode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- PERDEB ----
              PERDEB.setPreferredSize(new Dimension(110, 30));
              PERDEB.setMinimumSize(new Dimension(110, 30));
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setName("PERDEB");
              pnlPeriodeAEditer.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMaximumSize(new Dimension(20, 30));
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlPeriodeAEditer.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERFIN ----
              PERFIN.setMinimumSize(new Dimension(110, 30));
              PERFIN.setPreferredSize(new Dimension(110, 30));
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setName("PERFIN");
              pnlPeriodeAEditer.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbCodeDate ----
              cbCodeDate.setModel(new DefaultComboBoxModel(new String[] { "Date du jour", "D\u00e9but du mois en cours",
                  "Fin du mois en cours", "D\u00e9but du mois pr\u00e9c\u00e9dent", "Fin du mois pr\u00e9c\u00e9dent" }));
              cbCodeDate.setBackground(Color.white);
              cbCodeDate.setFont(new Font("sansserif", Font.PLAIN, 14));
              cbCodeDate.setName("cbCodeDate");
              pnlPeriodeAEditer.add(cbCodeDate, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSection ----
            lbSection.setText("Section analytique");
            lbSection.setPreferredSize(new Dimension(200, 30));
            lbSection.setMinimumSize(new Dimension(200, 30));
            lbSection.setMaximumSize(new Dimension(200, 30));
            lbSection.setName("lbSection");
            pnlCritereDeSelection.add(lbSection, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSectionAnalytique ----
            snSectionAnalytique.setFont(new Font("sansserif", Font.PLAIN, 14));
            snSectionAnalytique.setEnabled(false);
            snSectionAnalytique.setName("snSectionAnalytique");
            pnlCritereDeSelection.add(snSectionAnalytique, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeFamilleDebut ----
            lbCodeFamilleDebut.setText("Famille de d\u00e9but");
            lbCodeFamilleDebut.setPreferredSize(new Dimension(200, 30));
            lbCodeFamilleDebut.setMinimumSize(new Dimension(200, 30));
            lbCodeFamilleDebut.setMaximumSize(new Dimension(200, 30));
            lbCodeFamilleDebut.setName("lbCodeFamilleDebut");
            pnlCritereDeSelection.add(lbCodeFamilleDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereDeSelection.add(snFamilleDebut, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeFamilleFin ----
            lbCodeFamilleFin.setText("Famille de fin");
            lbCodeFamilleFin.setPreferredSize(new Dimension(200, 30));
            lbCodeFamilleFin.setMinimumSize(new Dimension(200, 30));
            lbCodeFamilleFin.setMaximumSize(new Dimension(200, 30));
            lbCodeFamilleFin.setName("lbCodeFamilleFin");
            pnlCritereDeSelection.add(lbCodeFamilleFin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleFin ----
            snFamilleFin.setName("snFamilleFin");
            pnlCritereDeSelection.add(snFamilleFin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeMagasinDebut ----
            lbCodeMagasinDebut.setText("Magasin de d\u00e9but");
            lbCodeMagasinDebut.setPreferredSize(new Dimension(200, 30));
            lbCodeMagasinDebut.setMinimumSize(new Dimension(200, 30));
            lbCodeMagasinDebut.setMaximumSize(new Dimension(200, 30));
            lbCodeMagasinDebut.setName("lbCodeMagasinDebut");
            pnlCritereDeSelection.add(lbCodeMagasinDebut, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasinDebut ----
            snMagasinDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasinDebut.setName("snMagasinDebut");
            pnlCritereDeSelection.add(snMagasinDebut, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeMagasinFin ----
            lbCodeMagasinFin.setText("Magasin de fin");
            lbCodeMagasinFin.setMinimumSize(new Dimension(200, 30));
            lbCodeMagasinFin.setPreferredSize(new Dimension(200, 30));
            lbCodeMagasinFin.setMaximumSize(new Dimension(200, 30));
            lbCodeMagasinFin.setName("lbCodeMagasinFin");
            pnlCritereDeSelection.add(lbCodeMagasinFin, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasinFin ----
            snMagasinFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasinFin.setName("snMagasinFin");
            pnlCritereDeSelection.add(snMagasinFin, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlBonEditer ========
          {
            pnlBonEditer.setOpaque(false);
            pnlBonEditer.setTitre("Bons \u00e0 \u00e9diter");
            pnlBonEditer.setName("pnlBonEditer");
            pnlBonEditer.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlBonEditer.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlBonEditer.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlBonEditer.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlBonEditer.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OPT1 ----
            OPT1.setText("Bons valid\u00e9s non exp\u00e9di\u00e9s (VAL)");
            OPT1.setComponentPopupMenu(null);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setMaximumSize(new Dimension(272, 30));
            OPT1.setMinimumSize(new Dimension(272, 30));
            OPT1.setPreferredSize(new Dimension(272, 30));
            OPT1.setName("OPT1");
            pnlBonEditer.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
            OPT2.setComponentPopupMenu(null);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setPreferredSize(new Dimension(239, 30));
            OPT2.setMinimumSize(new Dimension(239, 30));
            OPT2.setName("OPT2");
            pnlBonEditer.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Bons factur\u00e9s (FAC)");
            OPT3.setComponentPopupMenu(null);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setMinimumSize(new Dimension(151, 30));
            OPT3.setPreferredSize(new Dimension(151, 30));
            OPT3.setName("OPT3");
            pnlBonEditer.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlBonEditer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Options d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- ERED ----
            ERED.setText("Edition r\u00e9duite");
            ERED.setComponentPopupMenu(null);
            ERED.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ERED.setFont(new Font("sansserif", Font.PLAIN, 14));
            ERED.setMinimumSize(new Dimension(125, 30));
            ERED.setPreferredSize(new Dimension(125, 30));
            ERED.setHorizontalAlignment(SwingConstants.RIGHT);
            ERED.setName("ERED");
            pnlOptionEdition.add(ERED, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlOptionEdition, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setMinimumSize(new Dimension(536, 80));
            pnlEtablissement.setPreferredSize(new Dimension(536, 80));
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlControlesEffectuer ========
          {
            pnlControlesEffectuer.setTitre("Contr\u00f4les \u00e0 effectuer");
            pnlControlesEffectuer.setName("pnlControlesEffectuer");
            pnlControlesEffectuer.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlControlesEffectuer.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlControlesEffectuer.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlControlesEffectuer.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlControlesEffectuer.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- CRI12 ----
            CRI12.setText("Quantit\u00e9 tr\u00e8s importante");
            CRI12.setMaximumSize(new Dimension(200, 30));
            CRI12.setMinimumSize(new Dimension(200, 30));
            CRI12.setPreferredSize(new Dimension(200, 30));
            CRI12.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI12.setName("CRI12");
            pnlControlesEffectuer.add(CRI12, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlMargeInférieur ========
            {
              pnlMargeInférieur.setName("pnlMargeInf\u00e9rieur");
              pnlMargeInférieur.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMargeInférieur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlMargeInférieur.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMargeInférieur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMargeInférieur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CRI3 ----
              CRI3.setText("Marge inf\u00e9rieure \u00e0");
              CRI3.setComponentPopupMenu(null);
              CRI3.setFont(new Font("sansserif", Font.PLAIN, 14));
              CRI3.setMinimumSize(new Dimension(139, 30));
              CRI3.setPreferredSize(new Dimension(139, 30));
              CRI3.setName("CRI3");
              pnlMargeInférieur.add(CRI3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PMAR ----
              PMAR.setFont(new Font("sansserif", Font.PLAIN, 14));
              PMAR.setMinimumSize(new Dimension(50, 30));
              PMAR.setPreferredSize(new Dimension(50, 30));
              PMAR.setName("PMAR");
              pnlMargeInférieur.add(PMAR, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbPourcentInferieur ----
              lbPourcentInferieur.setText("%");
              lbPourcentInferieur.setHorizontalAlignment(SwingConstants.LEFT);
              lbPourcentInferieur.setMinimumSize(new Dimension(25, 30));
              lbPourcentInferieur.setPreferredSize(new Dimension(25, 30));
              lbPourcentInferieur.setMaximumSize(new Dimension(25, 30));
              lbPourcentInferieur.setName("lbPourcentInferieur");
              pnlMargeInférieur.add(lbPourcentInferieur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlControlesEffectuer.add(pnlMargeInférieur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlMargeSuperieur ========
            {
              pnlMargeSuperieur.setName("pnlMargeSuperieur");
              pnlMargeSuperieur.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMargeSuperieur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlMargeSuperieur.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMargeSuperieur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMargeSuperieur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CRI11 ----
              CRI11.setText("Marge sup\u00e9rieure \u00e0");
              CRI11.setComponentPopupMenu(null);
              CRI11.setFont(new Font("sansserif", Font.PLAIN, 14));
              CRI11.setPreferredSize(new Dimension(147, 30));
              CRI11.setMinimumSize(new Dimension(147, 30));
              CRI11.setName("CRI11");
              pnlMargeSuperieur.add(CRI11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PMARS ----
              PMARS.setFont(new Font("sansserif", Font.PLAIN, 14));
              PMARS.setMinimumSize(new Dimension(50, 30));
              PMARS.setPreferredSize(new Dimension(50, 30));
              PMARS.setName("PMARS");
              pnlMargeSuperieur.add(PMARS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbPourcentSuperieur ----
              lbPourcentSuperieur.setText("%");
              lbPourcentSuperieur.setHorizontalAlignment(SwingConstants.LEFT);
              lbPourcentSuperieur.setMinimumSize(new Dimension(25, 30));
              lbPourcentSuperieur.setPreferredSize(new Dimension(25, 30));
              lbPourcentSuperieur.setMaximumSize(new Dimension(25, 30));
              lbPourcentSuperieur.setName("lbPourcentSuperieur");
              pnlMargeSuperieur.add(lbPourcentSuperieur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlControlesEffectuer.add(pnlMargeSuperieur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CRI1 ----
            CRI1.setText("Marge n\u00e9gative");
            CRI1.setComponentPopupMenu(null);
            CRI1.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI1.setPreferredSize(new Dimension(219, 30));
            CRI1.setMinimumSize(new Dimension(219, 30));
            CRI1.setName("CRI1");
            pnlControlesEffectuer.add(CRI1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CRI2 ----
            CRI2.setText("Marge nulle");
            CRI2.setComponentPopupMenu(null);
            CRI2.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI2.setMinimumSize(new Dimension(97, 30));
            CRI2.setPreferredSize(new Dimension(97, 30));
            CRI2.setName("CRI2");
            pnlControlesEffectuer.add(CRI2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CRI10 ----
            CRI10.setText("Marge minimale de la cat\u00e9gorie client non atteinte");
            CRI10.setComponentPopupMenu(null);
            CRI10.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI10.setMinimumSize(new Dimension(350, 30));
            CRI10.setMaximumSize(new Dimension(350, 30));
            CRI10.setPreferredSize(new Dimension(350, 30));
            CRI10.setName("CRI10");
            pnlControlesEffectuer.add(CRI10, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlMontantSuperieur ========
            {
              pnlMontantSuperieur.setName("pnlMontantSuperieur");
              pnlMontantSuperieur.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMontantSuperieur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMontantSuperieur.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMontantSuperieur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMontantSuperieur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CRI4 ----
              CRI4.setText("Montant d'une ligne sup\u00e9rieur \u00e0");
              CRI4.setComponentPopupMenu(null);
              CRI4.setFont(new Font("sansserif", Font.PLAIN, 14));
              CRI4.setMinimumSize(new Dimension(225, 30));
              CRI4.setMaximumSize(new Dimension(225, 30));
              CRI4.setPreferredSize(new Dimension(225, 30));
              CRI4.setName("CRI4");
              pnlMontantSuperieur.add(CRI4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MTSUP ----
              MTSUP.setFont(new Font("sansserif", Font.PLAIN, 14));
              MTSUP.setPreferredSize(new Dimension(100, 30));
              MTSUP.setMinimumSize(new Dimension(100, 30));
              MTSUP.setName("MTSUP");
              pnlMontantSuperieur.add(MTSUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlControlesEffectuer.add(pnlMontantSuperieur, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlMontantInferieur ========
            {
              pnlMontantInferieur.setName("pnlMontantInferieur");
              pnlMontantInferieur.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMontantInferieur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMontantInferieur.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMontantInferieur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMontantInferieur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CRI5 ----
              CRI5.setText("Montant d'une ligne inf\u00e9rieur \u00e0");
              CRI5.setComponentPopupMenu(null);
              CRI5.setFont(new Font("sansserif", Font.PLAIN, 14));
              CRI5.setMinimumSize(new Dimension(215, 30));
              CRI5.setPreferredSize(new Dimension(215, 30));
              CRI5.setMaximumSize(new Dimension(215, 30));
              CRI5.setName("CRI5");
              pnlMontantInferieur.add(CRI5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MTINF ----
              MTINF.setFont(new Font("sansserif", Font.PLAIN, 14));
              MTINF.setMinimumSize(new Dimension(100, 30));
              MTINF.setPreferredSize(new Dimension(100, 30));
              MTINF.setName("MTINF");
              pnlMontantInferieur.add(MTINF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlControlesEffectuer.add(pnlMontantInferieur, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CRI6 ----
            CRI6.setText("Montant total n\u00e9gatif (avoir)");
            CRI6.setComponentPopupMenu(null);
            CRI6.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI6.setMinimumSize(new Dimension(103, 30));
            CRI6.setPreferredSize(new Dimension(103, 30));
            CRI6.setName("CRI6");
            pnlControlesEffectuer.add(CRI6, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CRI9 ----
            CRI9.setText("Pr\u00e9sence d'un article gratuit");
            CRI9.setComponentPopupMenu(null);
            CRI9.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI9.setPreferredSize(new Dimension(105, 30));
            CRI9.setMinimumSize(new Dimension(105, 30));
            CRI9.setName("CRI9");
            pnlControlesEffectuer.add(CRI9, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CRI8 ----
            CRI8.setText("Commissionnement forc\u00e9");
            CRI8.setComponentPopupMenu(null);
            CRI8.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI8.setMinimumSize(new Dimension(226, 30));
            CRI8.setPreferredSize(new Dimension(226, 30));
            CRI8.setName("CRI8");
            pnlControlesEffectuer.add(CRI8, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CRI7 ----
            CRI7.setText("Prix de vente forc\u00e9");
            CRI7.setComponentPopupMenu(null);
            CRI7.setFont(new Font("sansserif", Font.PLAIN, 14));
            CRI7.setPreferredSize(new Dimension(185, 30));
            CRI7.setMinimumSize(new Dimension(185, 30));
            CRI7.setName("CRI7");
            pnlControlesEffectuer.add(CRI7, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlControlesEffectuer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlPrincipal.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNMessage lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbPeriode;
  private SNPanel pnlPeriodeAEditer;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNComboBox cbCodeDate;
  private SNLabelChamp lbSection;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbCodeFamilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbCodeFamilleFin;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbCodeMagasinDebut;
  private SNMagasin snMagasinDebut;
  private SNLabelChamp lbCodeMagasinFin;
  private SNMagasin snMagasinFin;
  private SNPanelTitre pnlBonEditer;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox ERED;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlControlesEffectuer;
  private XRiCheckBox CRI12;
  private SNPanel pnlMargeInférieur;
  private XRiCheckBox CRI3;
  private XRiTextField PMAR;
  private SNLabelUnite lbPourcentInferieur;
  private SNPanel pnlMargeSuperieur;
  private XRiCheckBox CRI11;
  private XRiTextField PMARS;
  private SNLabelUnite lbPourcentSuperieur;
  private XRiCheckBox CRI1;
  private XRiCheckBox CRI2;
  private XRiCheckBox CRI10;
  private SNPanel pnlMontantSuperieur;
  private XRiCheckBox CRI4;
  private XRiTextField MTSUP;
  private SNPanel pnlMontantInferieur;
  private XRiCheckBox CRI5;
  private XRiTextField MTINF;
  private XRiCheckBox CRI6;
  private XRiCheckBox CRI9;
  private XRiCheckBox CRI8;
  private XRiCheckBox CRI7;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
