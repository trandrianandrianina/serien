
package ri.serien.libecranrpg.sgvm.SGVM66FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.sntypegratuit.SNTypeGratuit;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2951] Gestion des ventes -> Documents de ventes -> Etats des bons -> Gratuits -> Par client
 * Indicateur : 00100001
 * Titre : Edition des gratuits par client
 * 
 * [GVM2952] Gestion des ventes -> Documents de ventes -> Etats des bons -> Gratuits -> Par section analytique
 * Indicateur : 10100001
 * Titre : Edition des gratuits par section analytique
 * 
 * [GVM2953] Gestion des ventes -> Documents de ventes -> Etats des bons -> Gratuits -> Par représentant
 * Indicateur : 00101001
 * Titre : Edition des gratuits par représentant
 * 
 * [GVM2954] Gestion des ventes -> Documents de ventes -> Etats des bons -> Gratuits -> Par fournisseur et famille
 * Indicateur : 00100011
 * Titre : Edition des gratuits par fournisseur et famille
 */
public class SGVM66FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private boolean isPlanning = false;
  private String[] cbCodeDate_Value = { "*DAT", "*ENC", "*PRE", };
  private String[] cbCodeDate_Text = { "Date du jour", "Mois en cours", "Mois précédant", };
  private Message LOCTP;
  
  public SGVM66FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    cbCodeDate.removeAllItems();
    for (String texte : cbCodeDate_Text) {
      cbCodeDate.addItem(texte);
    }
    
    // Liée les composants pour les plages de sélection
    snFamilleDebut.lierComposantFin(snFamilleFin);
    
    OPT3.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT1.setValeursSelection("X", " ");
    REPONS.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Visibilité des composants
    snArticleFin.setVisible(lexique.isPresent("EARTF"));
    snArticleDebut.setVisible(lexique.isPresent("EARTD"));
    pnlOptionEdition.setVisible(REPONS.isVisible());
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    
    // Indicateur
    Boolean is91 = lexique.isTrue("91");
    Boolean is95 = lexique.isTrue("95");
    Boolean is97 = lexique.isTrue("97");
    
    isPlanning = lexique.isTrue("90");
    
    cbCodeDate.setVisible(isPlanning);
    lbPlanning.setVisible(isPlanning);
    PERDEB.setVisible(!isPlanning);
    PERFIN.setVisible(!isPlanning);
    lbAu.setVisible(!isPlanning);
    if (isPlanning) {
      cbCodeDate.setSelectedItem(lexique.HostFieldGetData("WCPER"));
    }
    
    // Mets le bon titre suivant le pts de menu
    if (is91) {
      bpPresentation.setText("Edition des gratuits par section analytique");
    }
    if (is95) {
      bpPresentation.setText("Edition des gratuits par représentant");
    }
    if (is97) {
      bpPresentation.setText("Edition des gratuits par fournisseur et famille");
    }
    if (!is91 && !is95 && !is97) {
      bpPresentation.setText("Edition des gratuits par client");
    }
    
    if (is91) {
      lbCodeSection.setVisible(true);
      snSectionAnalytique.setVisible(true);
    }
    else {
      lbCodeSection.setVisible(false);
      snSectionAnalytique.setVisible(false);
    }
    
    if (is95) {
      lbCodeRepresentant.setVisible(true);
      snRepresentant.setVisible(true);
    }
    else {
      lbCodeRepresentant.setVisible(false);
      snRepresentant.setVisible(false);
    }
    
    if (is97) {
      snFournisseur.setVisible(true);
      lbCodeFournisseur.setVisible(true);
    }
    else {
      snFournisseur.setVisible(false);
      lbCodeFournisseur.setVisible(false);
    }
    
    // Sélecrtionne un état de bon obligatoire si aucun n'est sélectionner au démarrage
    if (!OPT1.isSelected() || !OPT2.isSelected() || !OPT3.isSelected()) {
      if (OPT1.isVisible()) {
        OPT1.setSelected(true);
      }
      else if (OPT2.isVisible()) {
        OPT2.setSelected(true);
      }
      else if (OPT3.isVisible()) {
        OPT3.setSelected(true);
      }
      else {
        OPT1.setSelected(false);
        OPT2.setSelected(false);
        OPT3.setSelected(false);
      }
    }
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Charger les composant
    chargerArticle();
    chargerFournisseur();
    chargerRepresentant();
    chargerFamille();
    chargerSectionAnalytique();
    chargerTypeGratuit();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snArticleDebut.renseignerChampRPG(lexique, "EARTD");
    snArticleFin.renseignerChampRPG(lexique, "EARTF");
    
    if (isPlanning) {
      lexique.HostFieldPutData("WCPER", 0, cbCodeDate_Value[cbCodeDate.getSelectedIndex()]);
    }
    
    // Gestion sélection tous famille
    if (snFamilleDebut.getIdSelection() == null && snFamilleFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTFAM", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTFAM", 0, "  ");
      snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
      snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    }
    
    // Gestion sélection tous section analytique
    if (snSectionAnalytique.isVisible()) {
      if (snSectionAnalytique.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUS", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUS", 0, "  ");
        snSectionAnalytique.renseignerChampRPG(lexique, "SAN");
      }
    }
    
    // Gestion sélection tous type gratuit
    if (snTypeGratuit.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOUG", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOUG", 0, " ");
      snTypeGratuit.renseignerChampRPG(lexique, "TGRAT");
    }
    
    // Gestion sélection tous representant
    if (snRepresentant.isVisible()) {
      if (snRepresentant.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUR", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUR", 0, "  ");
        snRepresentant.renseignerChampRPG(lexique, "REP");
      }
    }
    
    // Gestion sélection tous fournisseur
    if (snFournisseur.isVisible()) {
      if (snFournisseur.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOU01", 0, "**");
        lexique.HostFieldPutData("CODFRS", 0, "");
      }
      else {
        lexique.HostFieldPutData("WTOU01", 0, " ");
        snFournisseur.renseignerChampRPG(lexique, "CODFRS");
      }
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge les articles suivant l'etablissement
   */
  private void chargerArticle() {
    // Renseigner l'article de débutWMAG
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "EARTD");
    // Renseigner l'article de fin
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "EARTF");
  }
  
  /**
   * Charge la liste des fournisseurs suivant l'établissement
   */
  private void chargerFournisseur() {
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "CODFRS");
  }
  
  /**
   * Charge la liste des famille suivant l'établissement
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(true);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  /**
   * Charge la liste des représentant suivant l'établissement
   */
  private void chargerRepresentant() {
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "REP");
  }
  
  /**
   * Charge la liste des représentant suivant l'établissement
   */
  private void chargerSectionAnalytique() {
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "SAN");
  }
  
  /**
   * Charge la liste des type de gratuit suivant l'établissement
   */
  private void chargerTypeGratuit() {
    snTypeGratuit.setSession(getSession());
    snTypeGratuit.setIdEtablissement(snEtablissement.getIdSelection());
    snTypeGratuit.setTousAutorise(true);
    snTypeGratuit.charger(false);
    snTypeGratuit.setSelectionParChampRPG(lexique, "TGRAT");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerArticle();
      chargerFournisseur();
      chargerRepresentant();
      chargerFamille();
      chargerSectionAnalytique();
      chargerTypeGratuit();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanel();
    pnlDroite = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lnPeriodeAEditer = new SNLabelChamp();
    pnlPeriodeEditer = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    cbCodeDate = new SNComboBox();
    lbTypeDeGratuit = new SNLabelChamp();
    snTypeGratuit = new SNTypeGratuit();
    lbCodeFameilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbCodeArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbCodeArticleFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    lbCodeFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbCodeSection = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    lbCodeRepresentant = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    pnlGauche = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    tfEnCours = new SNTexte();
    pnlBonEditer = new SNPanelTitre();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    pnlOptionEdition = new SNPanelTitre();
    REPONS = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1050, 640));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des gratuits");
    bpPresentation.setPreferredSize(new Dimension(150, 55));
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPlanning ----
        lbPlanning.setText("Vous \u00eates en cours de param\u00e9trage en vue de la mise au planning");
        lbPlanning.setMinimumSize(new Dimension(120, 30));
        lbPlanning.setPreferredSize(new Dimension(120, 30));
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lnPeriodeAEditer ----
            lnPeriodeAEditer.setText("P\u00e9riode \u00e0 \u00e9diter");
            lnPeriodeAEditer.setPreferredSize(new Dimension(140, 30));
            lnPeriodeAEditer.setMinimumSize(new Dimension(140, 30));
            lnPeriodeAEditer.setMaximumSize(new Dimension(140, 30));
            lnPeriodeAEditer.setName("lnPeriodeAEditer");
            pnlCritereSelection.add(lnPeriodeAEditer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeEditer ========
            {
              pnlPeriodeEditer.setName("pnlPeriodeEditer");
              pnlPeriodeEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- PERDEB ----
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setPreferredSize(new Dimension(110, 30));
              PERDEB.setMinimumSize(new Dimension(110, 30));
              PERDEB.setName("PERDEB");
              pnlPeriodeEditer.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlPeriodeEditer.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERFIN ----
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setPreferredSize(new Dimension(110, 30));
              PERFIN.setMinimumSize(new Dimension(110, 30));
              PERFIN.setName("PERFIN");
              pnlPeriodeEditer.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbCodeDate ----
              cbCodeDate.setModel(new DefaultComboBoxModel(new String[] { "Date du jour", "D\u00e9but du mois en cours",
                  "Fin du mois en cours", "D\u00e9but du mois pr\u00e9c\u00e9dent", "Fin du mois pr\u00e9c\u00e9dent" }));
              cbCodeDate.setBackground(Color.white);
              cbCodeDate.setFont(new Font("sansserif", Font.PLAIN, 14));
              cbCodeDate.setName("cbCodeDate");
              pnlPeriodeEditer.add(cbCodeDate, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlPeriodeEditer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTypeDeGratuit ----
            lbTypeDeGratuit.setText("Type de gratuit");
            lbTypeDeGratuit.setPreferredSize(new Dimension(140, 30));
            lbTypeDeGratuit.setMinimumSize(new Dimension(140, 30));
            lbTypeDeGratuit.setMaximumSize(new Dimension(140, 30));
            lbTypeDeGratuit.setName("lbTypeDeGratuit");
            pnlCritereSelection.add(lbTypeDeGratuit, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snTypeGratuit ----
            snTypeGratuit.setName("snTypeGratuit");
            pnlCritereSelection.add(snTypeGratuit, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeFameilleDebut ----
            lbCodeFameilleDebut.setText("Famille de d\u00e9but");
            lbCodeFameilleDebut.setPreferredSize(new Dimension(140, 30));
            lbCodeFameilleDebut.setMinimumSize(new Dimension(140, 30));
            lbCodeFameilleDebut.setMaximumSize(new Dimension(140, 30));
            lbCodeFameilleDebut.setName("lbCodeFameilleDebut");
            pnlCritereSelection.add(lbCodeFameilleDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereSelection.add(snFamilleDebut, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFin ----
            lbFin.setText("Famille de fin");
            lbFin.setPreferredSize(new Dimension(35, 30));
            lbFin.setMinimumSize(new Dimension(55, 30));
            lbFin.setMaximumSize(new Dimension(55, 30));
            lbFin.setName("lbFin");
            pnlCritereSelection.add(lbFin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleFin ----
            snFamilleFin.setName("snFamilleFin");
            pnlCritereSelection.add(snFamilleFin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeArticleDebut ----
            lbCodeArticleDebut.setText("Article de d\u00e9but");
            lbCodeArticleDebut.setPreferredSize(new Dimension(140, 30));
            lbCodeArticleDebut.setMinimumSize(new Dimension(140, 30));
            lbCodeArticleDebut.setMaximumSize(new Dimension(140, 30));
            lbCodeArticleDebut.setName("lbCodeArticleDebut");
            pnlCritereSelection.add(lbCodeArticleDebut, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleDebut ----
            snArticleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snArticleDebut.setMaximumSize(new Dimension(350, 30));
            snArticleDebut.setMinimumSize(new Dimension(350, 30));
            snArticleDebut.setPreferredSize(new Dimension(350, 30));
            snArticleDebut.setName("snArticleDebut");
            pnlCritereSelection.add(snArticleDebut, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeArticleFin ----
            lbCodeArticleFin.setText("Article de fin");
            lbCodeArticleFin.setPreferredSize(new Dimension(140, 30));
            lbCodeArticleFin.setMinimumSize(new Dimension(140, 30));
            lbCodeArticleFin.setMaximumSize(new Dimension(140, 30));
            lbCodeArticleFin.setName("lbCodeArticleFin");
            pnlCritereSelection.add(lbCodeArticleFin, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleFin ----
            snArticleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snArticleFin.setPreferredSize(new Dimension(350, 30));
            snArticleFin.setMinimumSize(new Dimension(350, 30));
            snArticleFin.setMaximumSize(new Dimension(350, 30));
            snArticleFin.setName("snArticleFin");
            pnlCritereSelection.add(snArticleFin, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeFournisseur ----
            lbCodeFournisseur.setText("Fournisseur");
            lbCodeFournisseur.setPreferredSize(new Dimension(140, 30));
            lbCodeFournisseur.setMinimumSize(new Dimension(140, 30));
            lbCodeFournisseur.setMaximumSize(new Dimension(140, 30));
            lbCodeFournisseur.setName("lbCodeFournisseur");
            pnlCritereSelection.add(lbCodeFournisseur, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseur ----
            snFournisseur.setName("snFournisseur");
            pnlCritereSelection.add(snFournisseur, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeSection ----
            lbCodeSection.setText("Code section");
            lbCodeSection.setPreferredSize(new Dimension(140, 30));
            lbCodeSection.setMinimumSize(new Dimension(140, 30));
            lbCodeSection.setMaximumSize(new Dimension(140, 30));
            lbCodeSection.setName("lbCodeSection");
            pnlCritereSelection.add(lbCodeSection, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSectionAnalytique ----
            snSectionAnalytique.setFont(new Font("sansserif", Font.PLAIN, 14));
            snSectionAnalytique.setName("snSectionAnalytique");
            pnlCritereSelection.add(snSectionAnalytique, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeRepresentant ----
            lbCodeRepresentant.setText("Repr\u00e9sentant");
            lbCodeRepresentant.setPreferredSize(new Dimension(140, 30));
            lbCodeRepresentant.setMinimumSize(new Dimension(140, 30));
            lbCodeRepresentant.setMaximumSize(new Dimension(140, 30));
            lbCodeRepresentant.setName("lbCodeRepresentant");
            pnlCritereSelection.add(lbCodeRepresentant, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snRepresentant ----
            snRepresentant.setPreferredSize(new Dimension(300, 30));
            snRepresentant.setMinimumSize(new Dimension(300, 30));
            snRepresentant.setMaximumSize(new Dimension(300, 30));
            snRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
            snRepresentant.setName("snRepresentant");
            pnlCritereSelection.add(snRepresentant, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setPreferredSize(new Dimension(250, 115));
            pnlEtablissement.setMinimumSize(new Dimension(250, 97));
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlBonEditer ========
          {
            pnlBonEditer.setTitre("Bons \u00e0 \u00e9diter");
            pnlBonEditer.setPreferredSize(new Dimension(50, 150));
            pnlBonEditer.setMinimumSize(new Dimension(50, 132));
            pnlBonEditer.setName("pnlBonEditer");
            pnlBonEditer.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlBonEditer.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlBonEditer.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlBonEditer.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlBonEditer.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OPT1 ----
            OPT1.setText("Bons valid\u00e9s non exp\u00e9di\u00e9s (VAL)");
            OPT1.setComponentPopupMenu(null);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setPreferredSize(new Dimension(231, 30));
            OPT1.setMinimumSize(new Dimension(231, 30));
            OPT1.setName("OPT1");
            pnlBonEditer.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
            OPT2.setComponentPopupMenu(null);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setMinimumSize(new Dimension(239, 30));
            OPT2.setPreferredSize(new Dimension(239, 30));
            OPT2.setName("OPT2");
            pnlBonEditer.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Bons factur\u00e9s (FAC)");
            OPT3.setComponentPopupMenu(null);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setPreferredSize(new Dimension(151, 30));
            OPT3.setMinimumSize(new Dimension(151, 30));
            OPT3.setName("OPT3");
            pnlBonEditer.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlBonEditer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Option d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- REPONS ----
            REPONS.setText("Saut de page par repr\u00e9sentant");
            REPONS.setComponentPopupMenu(null);
            REPONS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPONS.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPONS.setPreferredSize(new Dimension(217, 30));
            REPONS.setMinimumSize(new Dimension(217, 30));
            REPONS.setName("REPONS");
            pnlOptionEdition.add(REPONS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlOptionEdition, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNMessage lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lnPeriodeAEditer;
  private SNPanel pnlPeriodeEditer;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNComboBox cbCodeDate;
  private SNLabelChamp lbTypeDeGratuit;
  private SNTypeGratuit snTypeGratuit;
  private SNLabelChamp lbCodeFameilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFin;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbCodeArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbCodeArticleFin;
  private SNArticle snArticleFin;
  private SNLabelChamp lbCodeFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbCodeSection;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbCodeRepresentant;
  private SNRepresentant snRepresentant;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNEtablissement snEtablissement;
  private SNTexte tfEnCours;
  private SNPanelTitre pnlBonEditer;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox REPONS;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
