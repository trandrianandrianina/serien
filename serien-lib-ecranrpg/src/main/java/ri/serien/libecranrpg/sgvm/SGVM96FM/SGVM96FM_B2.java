
package ri.serien.libecranrpg.sgvm.SGVM96FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM96FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM96FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    NAT5.setValeurs("5", "R3");
    NAT4.setValeurs("4", "R3");
    NAT3.setValeurs("3", "R3");
    NAT2.setValeurs("2", "R3");
    NAT1.setValeurs("1", "R3");
    CRO4.setValeurs("4", "R2");
    CRO3.setValeurs("3", "R2");
    CRO1.setValeurs("2", "R2");
    CRO2.setValeurs("1", "R2");
    STA3.setValeurs("3", "R1");
    STA2.setValeurs("2", "R1");
    STA1.setValeurs("1", "R1");
    WTOU1.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    STA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Statistiques jusqu'au @SDAT1@")).trim());
    STA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Statistiques jusqu'au @SDAT2@")).trim());
    STA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Statistiques jusqu'au @SDAT3@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPLAD2@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPLAF2@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPLAD1@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPLAF1@")).trim());
    CRO2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCRO2@")).trim());
    CRO1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCRO1@")).trim());
    CRO3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCRO3@")).trim());
    CRO4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCRO4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    // STA2.setSelected(lexique.HostFieldGetData("STA2").equalsIgnoreCase("X"));
    // STA3.setVisible( lexique.isPresent("STA3"));
    // STA3.setSelected(lexique.HostFieldGetData("STA3").equalsIgnoreCase("X"));
    // STA1.setSelected(lexique.HostFieldGetData("STA1").equalsIgnoreCase("X"));
    // STA2.setEnabled(false);
    // STA3.setEnabled(false);
    // STA1.setEnabled(false);
    // OBJ_36.setVisible(true);
    // OBJ_37.setVisible(true);
    // OBJ_38.setVisible(true);
    // OBJ_39.setVisible(true);
    // OBJ_40.setVisible(true);
    // OBJ_41.setVisible(true);
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("O"));
    // CRO4.setSelected(lexique.HostFieldGetData("CRO4").equalsIgnoreCase("X"));
    // CRO3.setSelected(lexique.HostFieldGetData("CRO3").equalsIgnoreCase("X"));
    // CRO2.setSelected(lexique.HostFieldGetData("CRO2").equalsIgnoreCase("X"));
    // CRO1.setSelected(lexique.HostFieldGetData("CRO1").equalsIgnoreCase("X"));
    // CRO4.setEnabled(false);
    // CRO3.setEnabled(false);
    // CRO2.setEnabled(false);
    // CRO1.setEnabled(false);
    // NAT5.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("5"));
    // NAT4.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("4"));
    // NAT3.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("3"));
    // NAT2.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("2"));
    // NAT1.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("1"));
    OBJ_21.setVisible(lexique.isPresent("REPON1"));
    panel6.setVisible(!lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    panel7.setVisible(!lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU2", 0, " ");
    // if (WTOU1.isSelected())
    // lexique.HostFieldPutData("WTOU1", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU1", 0, " ");
    // if (STA2.isSelected())
    // lexique.HostFieldPutData("STA2", 0, "X");
    // if (STA3.isSelected())
    // lexique.HostFieldPutData("STA3", 0, "X");
    // if (STA1.isSelected())
    // lexique.HostFieldPutData("STA1", 0, "X");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "O");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "N");
    // if (CRO4.isSelected())
    // lexique.HostFieldPutData("CRO4", 0, "X");
    // if (CRO3.isSelected())
    // lexique.HostFieldPutData("CRO3", 0, "X");
    // if (CRO2.isSelected())
    // lexique.HostFieldPutData("CRO2", 0, "X");
    // if (CRO1.isSelected())
    // lexique.HostFieldPutData("CRO1", 0, "X");
    // if (NAT5.isSelected())
    // lexique.HostFieldPutData("R3", 0, "5");
    // if (NAT4.isSelected())
    // lexique.HostFieldPutData("R3", 0, "4");
    // if (NAT3.isSelected())
    // lexique.HostFieldPutData("R3", 0, "3");
    // if (NAT2.isSelected())
    // lexique.HostFieldPutData("R3", 0, "2");
    // if (NAT1.isSelected())
    // lexique.HostFieldPutData("R3", 0, "1");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    panel7.setVisible(!panel7.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    panel6.setVisible(!panel6.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_35 = new JXTitledSeparator();
    OBJ_19 = new JXTitledSeparator();
    OBJ_20 = new JXTitledSeparator();
    OBJ_30 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    REPON1 = new XRiCheckBox();
    panel1 = new JPanel();
    STA1 = new XRiRadioButton();
    STA2 = new XRiRadioButton();
    STA3 = new XRiRadioButton();
    panel3 = new JPanel();
    WTOU2 = new XRiCheckBox();
    OBJ_41 = new JLabel();
    panel7 = new JPanel();
    AR20D2 = new XRiTextField();
    AR20F2 = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    ECLID2 = new XRiTextField();
    ECLIF2 = new XRiTextField();
    ECATD2 = new XRiTextField();
    ECATF2 = new XRiTextField();
    EREPD2 = new XRiTextField();
    EREPF2 = new XRiTextField();
    EGRPD2 = new XRiTextField();
    EGRPF2 = new XRiTextField();
    panel4 = new JPanel();
    WTOU1 = new XRiCheckBox();
    OBJ_40 = new JLabel();
    panel6 = new JPanel();
    AR20D1 = new XRiTextField();
    AR20F1 = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    ECLID1 = new XRiTextField();
    ECLIF1 = new XRiTextField();
    ECATD1 = new XRiTextField();
    ECATF1 = new XRiTextField();
    EREPD1 = new XRiTextField();
    EREPF1 = new XRiTextField();
    EGRPD1 = new XRiTextField();
    EGRPF1 = new XRiTextField();
    panel2 = new JPanel();
    CRO2 = new XRiRadioButton();
    CRO1 = new XRiRadioButton();
    CRO3 = new XRiRadioButton();
    CRO4 = new XRiRadioButton();
    panel5 = new JPanel();
    NAT1 = new XRiRadioButton();
    NAT2 = new XRiRadioButton();
    NAT3 = new XRiRadioButton();
    NAT4 = new XRiRadioButton();
    NAT5 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(710, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_35 ----
          OBJ_35.setTitle("Options possibles pour code nature");
          OBJ_35.setName("OBJ_35");

          //---- OBJ_19 ----
          OBJ_19.setTitle("");
          OBJ_19.setName("OBJ_19");

          //---- OBJ_20 ----
          OBJ_20.setTitle("");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_30 ----
          OBJ_30.setTitle("Type de croisement");
          OBJ_30.setName("OBJ_30");

          //---- OBJ_22 ----
          OBJ_22.setTitle("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_21 ----
          OBJ_21.setTitle("");
          OBJ_21.setName("OBJ_21");

          //---- REPON1 ----
          REPON1.setText("Regroupement article / produit");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- STA1 ----
            STA1.setText("Statistiques jusqu'au @SDAT1@");
            STA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STA1.setName("STA1");

            //---- STA2 ----
            STA2.setText("Statistiques jusqu'au @SDAT2@");
            STA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STA2.setName("STA2");

            //---- STA3 ----
            STA3.setText("Statistiques jusqu'au @SDAT3@");
            STA3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STA3.setName("STA3");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(STA1, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)
                    .addComponent(STA2, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)
                    .addComponent(STA3, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(STA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(STA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(STA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- WTOU2 ----
            WTOU2.setText("S\u00e9lection");
            WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU2.setName("WTOU2");
            WTOU2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU2ActionPerformed(e);
              }
            });

            //---- OBJ_41 ----
            OBJ_41.setText("compl\u00e8te");
            OBJ_41.setName("OBJ_41");

            //======== panel7 ========
            {
              panel7.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel7.setOpaque(false);
              panel7.setName("panel7");

              //---- AR20D2 ----
              AR20D2.setComponentPopupMenu(BTD);
              AR20D2.setName("AR20D2");

              //---- AR20F2 ----
              AR20F2.setComponentPopupMenu(BTD);
              AR20F2.setName("AR20F2");

              //---- OBJ_38 ----
              OBJ_38.setText("@OPLAD2@");
              OBJ_38.setName("OBJ_38");

              //---- OBJ_39 ----
              OBJ_39.setText("@OPLAF2@");
              OBJ_39.setName("OBJ_39");

              //---- ECLID2 ----
              ECLID2.setComponentPopupMenu(BTD);
              ECLID2.setName("ECLID2");

              //---- ECLIF2 ----
              ECLIF2.setComponentPopupMenu(BTD);
              ECLIF2.setName("ECLIF2");

              //---- ECATD2 ----
              ECATD2.setComponentPopupMenu(BTD);
              ECATD2.setName("ECATD2");

              //---- ECATF2 ----
              ECATF2.setComponentPopupMenu(BTD);
              ECATF2.setName("ECATF2");

              //---- EREPD2 ----
              EREPD2.setComponentPopupMenu(BTD);
              EREPD2.setName("EREPD2");

              //---- EREPF2 ----
              EREPF2.setComponentPopupMenu(BTD);
              EREPF2.setName("EREPF2");

              //---- EGRPD2 ----
              EGRPD2.setComponentPopupMenu(BTD);
              EGRPD2.setName("EGRPD2");

              //---- EGRPF2 ----
              EGRPF2.setComponentPopupMenu(BTD);
              EGRPF2.setName("EGRPF2");

              GroupLayout panel7Layout = new GroupLayout(panel7);
              panel7.setLayout(panel7Layout);
              panel7Layout.setHorizontalGroup(
                panel7Layout.createParallelGroup()
                  .addGroup(panel7Layout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addGroup(panel7Layout.createParallelGroup()
                      .addGroup(panel7Layout.createSequentialGroup()
                        .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addGroup(panel7Layout.createParallelGroup()
                          .addComponent(ECATD2, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                          .addComponent(EGRPD2, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(EREPD2, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ECLID2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(AR20D2, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(panel7Layout.createSequentialGroup()
                        .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addGroup(panel7Layout.createParallelGroup()
                          .addComponent(EGRPF2, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ECATF2, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                          .addComponent(AR20F2, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                          .addComponent(EREPF2, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ECLIF2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))))
              );
              panel7Layout.setVerticalGroup(
                panel7Layout.createParallelGroup()
                  .addGroup(panel7Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addGroup(panel7Layout.createParallelGroup()
                      .addGroup(panel7Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_38))
                      .addComponent(ECATD2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EGRPD2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EREPD2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ECLID2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(AR20D2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(panel7Layout.createParallelGroup()
                      .addGroup(panel7Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_39))
                      .addComponent(EGRPF2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ECATF2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(AR20F2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EREPF2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ECLIF2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(31, 31, 31)
                  .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(32, 32, 32)
                  .addComponent(OBJ_41))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");

            //---- WTOU1 ----
            WTOU1.setText("S\u00e9lection");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });

            //---- OBJ_40 ----
            OBJ_40.setText("compl\u00e8te");
            OBJ_40.setName("OBJ_40");

            //======== panel6 ========
            {
              panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel6.setOpaque(false);
              panel6.setName("panel6");

              //---- AR20D1 ----
              AR20D1.setComponentPopupMenu(BTD);
              AR20D1.setName("AR20D1");

              //---- AR20F1 ----
              AR20F1.setComponentPopupMenu(BTD);
              AR20F1.setName("AR20F1");

              //---- OBJ_36 ----
              OBJ_36.setText("@OPLAD1@");
              OBJ_36.setName("OBJ_36");

              //---- OBJ_37 ----
              OBJ_37.setText("@OPLAF1@");
              OBJ_37.setName("OBJ_37");

              //---- ECLID1 ----
              ECLID1.setComponentPopupMenu(BTD);
              ECLID1.setName("ECLID1");

              //---- ECLIF1 ----
              ECLIF1.setComponentPopupMenu(BTD);
              ECLIF1.setName("ECLIF1");

              //---- ECATD1 ----
              ECATD1.setComponentPopupMenu(BTD);
              ECATD1.setName("ECATD1");

              //---- ECATF1 ----
              ECATF1.setComponentPopupMenu(BTD);
              ECATF1.setName("ECATF1");

              //---- EREPD1 ----
              EREPD1.setComponentPopupMenu(BTD);
              EREPD1.setName("EREPD1");

              //---- EREPF1 ----
              EREPF1.setComponentPopupMenu(BTD);
              EREPF1.setName("EREPF1");

              //---- EGRPD1 ----
              EGRPD1.setComponentPopupMenu(BTD);
              EGRPD1.setName("EGRPD1");

              //---- EGRPF1 ----
              EGRPF1.setComponentPopupMenu(BTD);
              EGRPF1.setName("EGRPF1");

              GroupLayout panel6Layout = new GroupLayout(panel6);
              panel6.setLayout(panel6Layout);
              panel6Layout.setHorizontalGroup(
                panel6Layout.createParallelGroup()
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addGroup(panel6Layout.createParallelGroup()
                      .addGroup(panel6Layout.createSequentialGroup()
                        .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addGroup(panel6Layout.createParallelGroup()
                          .addComponent(AR20D1, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ECATD1, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                          .addComponent(EGRPD1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ECLID1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(EREPD1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(panel6Layout.createSequentialGroup()
                        .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addGroup(panel6Layout.createParallelGroup()
                          .addComponent(EREPF1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ECLIF1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ECATF1, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                          .addComponent(AR20F1, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                          .addComponent(EGRPF1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))))
              );
              panel6Layout.setVerticalGroup(
                panel6Layout.createParallelGroup()
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addGroup(panel6Layout.createParallelGroup()
                      .addGroup(panel6Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_36))
                      .addComponent(AR20D1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ECATD1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EGRPD1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ECLID1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EREPD1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(panel6Layout.createParallelGroup()
                      .addGroup(panel6Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_37))
                      .addComponent(EREPF1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ECLIF1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ECATF1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(AR20F1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EGRPF1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }

            GroupLayout panel4Layout = new GroupLayout(panel4);
            panel4.setLayout(panel4Layout);
            panel4Layout.setHorizontalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
            );
            panel4Layout.setVerticalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(31, 31, 31)
                  .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(32, 32, 32)
                  .addComponent(OBJ_40))
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- CRO2 ----
            CRO2.setText("@OCRO2@");
            CRO2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CRO2.setName("CRO2");

            //---- CRO1 ----
            CRO1.setText("@OCRO1@");
            CRO1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CRO1.setName("CRO1");

            //---- CRO3 ----
            CRO3.setText("@OCRO3@");
            CRO3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CRO3.setName("CRO3");

            //---- CRO4 ----
            CRO4.setText("@OCRO4@");
            CRO4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CRO4.setName("CRO4");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(CRO1, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CRO2, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CRO3, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CRO4, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(CRO1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(CRO2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(CRO3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(CRO4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");

            //---- NAT1 ----
            NAT1.setText("Chiffre d'affaire H.T");
            NAT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT1.setName("NAT1");

            //---- NAT2 ----
            NAT2.setText("Chiffre d'affaire H.T + marge");
            NAT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT2.setName("NAT2");

            //---- NAT3 ----
            NAT3.setText("Quantit\u00e9");
            NAT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT3.setName("NAT3");

            //---- NAT4 ----
            NAT4.setText("Chiffre d'affaire H.T + quantit\u00e9");
            NAT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT4.setName("NAT4");

            //---- NAT5 ----
            NAT5.setText("Chiffre d'affaire H.T + marge + quantit\u00e9");
            NAT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NAT5.setName("NAT5");

            GroupLayout panel5Layout = new GroupLayout(panel5);
            panel5.setLayout(panel5Layout);
            panel5Layout.setHorizontalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addComponent(NAT1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NAT2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NAT3, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NAT4, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NAT5, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)))
            );
            panel5Layout.setVerticalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(NAT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(3, 3, 3)
                  .addComponent(NAT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(3, 3, 3)
                  .addComponent(NAT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(3, 3, 3)
                  .addComponent(NAT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(3, 3, 3)
                  .addComponent(NAT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                .addGap(35, 35, 35)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- R1_GRP ----
    ButtonGroup R1_GRP = new ButtonGroup();
    R1_GRP.add(STA1);
    R1_GRP.add(STA2);
    R1_GRP.add(STA3);

    //---- R2_GRP ----
    ButtonGroup R2_GRP = new ButtonGroup();
    R2_GRP.add(CRO2);
    R2_GRP.add(CRO1);
    R2_GRP.add(CRO3);
    R2_GRP.add(CRO4);

    //---- R3_GRP ----
    ButtonGroup R3_GRP = new ButtonGroup();
    R3_GRP.add(NAT1);
    R3_GRP.add(NAT2);
    R3_GRP.add(NAT3);
    R3_GRP.add(NAT4);
    R3_GRP.add(NAT5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_35;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_20;
  private JXTitledSeparator OBJ_30;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_21;
  private XRiCheckBox REPON1;
  private JPanel panel1;
  private XRiRadioButton STA1;
  private XRiRadioButton STA2;
  private XRiRadioButton STA3;
  private JPanel panel3;
  private XRiCheckBox WTOU2;
  private JLabel OBJ_41;
  private JPanel panel7;
  private XRiTextField AR20D2;
  private XRiTextField AR20F2;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private XRiTextField ECLID2;
  private XRiTextField ECLIF2;
  private XRiTextField ECATD2;
  private XRiTextField ECATF2;
  private XRiTextField EREPD2;
  private XRiTextField EREPF2;
  private XRiTextField EGRPD2;
  private XRiTextField EGRPF2;
  private JPanel panel4;
  private XRiCheckBox WTOU1;
  private JLabel OBJ_40;
  private JPanel panel6;
  private XRiTextField AR20D1;
  private XRiTextField AR20F1;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private XRiTextField ECLID1;
  private XRiTextField ECLIF1;
  private XRiTextField ECATD1;
  private XRiTextField ECATF1;
  private XRiTextField EREPD1;
  private XRiTextField EREPF1;
  private XRiTextField EGRPD1;
  private XRiTextField EGRPF1;
  private JPanel panel2;
  private XRiRadioButton CRO2;
  private XRiRadioButton CRO1;
  private XRiRadioButton CRO3;
  private XRiRadioButton CRO4;
  private JPanel panel5;
  private XRiRadioButton NAT1;
  private XRiRadioButton NAT2;
  private XRiRadioButton NAT3;
  private XRiRadioButton NAT4;
  private XRiRadioButton NAT5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
