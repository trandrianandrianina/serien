
package ri.serien.libecranrpg.sgvm.SGVM43FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM43FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private static final String EXPORTER = "Exporter";
  private String[] NATLOG_Value = { "", "H.T", "TTC", };
  
  public SGVM43FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    NATLOG.setValeurs(NATLOG_Value, null);
    REPONS.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EXPORTER, 'e', true);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_wencx_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    z_wencx_2 = new RiZoneSortie();
    pnlDroitAuteur = new JPanel();
    lbDateDebut = new SNLabelChamp();
    PERDEB = new XRiCalendrier();
    lbDateFin = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    pnlDroitAuteur2 = new JPanel();
    REPONS = new XRiCheckBox();
    lbNatureMontant = new SNLabelChamp();
    NATLOG = new XRiComboBox();
    pnlDroitAuteur3 = new JPanel();
    lbSectionDebut = new SNLabelChamp();
    SECT1 = new XRiTextField();
    lbSectionFin = new SNLabelChamp();
    SECT2 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setBackground(new Color(239, 239, 222));
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setOpaque(false);
      snBarreBouton.setName("snBarreBouton");
      p_sud.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setBorder(new TitledBorder(null, "Etablissement", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
              new Font("sansserif", Font.BOLD, 14)));
          pnlEtablissement.setOpaque(false);
          pnlEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- z_wencx_2 ----
          z_wencx_2.setText("@WENCX@");
          z_wencx_2.setPreferredSize(new Dimension(220, 30));
          z_wencx_2.setMinimumSize(new Dimension(220, 30));
          z_wencx_2.setFont(new Font("sansserif", Font.PLAIN, 14));
          z_wencx_2.setName("z_wencx_2");
          pnlEtablissement.add(z_wencx_2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDroitAuteur ========
        {
          pnlDroitAuteur.setBorder(new TitledBorder(null, "P\u00e9riode \u00e0 traiter", TitledBorder.LEADING,
              TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
          pnlDroitAuteur.setOpaque(false);
          pnlDroitAuteur.setName("pnlDroitAuteur");
          pnlDroitAuteur.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroitAuteur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroitAuteur.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroitAuteur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroitAuteur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbDateDebut ----
          lbDateDebut.setText("Date de d\u00e9but");
          lbDateDebut.setName("lbDateDebut");
          pnlDroitAuteur.add(lbDateDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- PERDEB ----
          PERDEB.setComponentPopupMenu(null);
          PERDEB.setMaximumSize(new Dimension(105, 30));
          PERDEB.setMinimumSize(new Dimension(105, 30));
          PERDEB.setPreferredSize(new Dimension(105, 30));
          PERDEB.setName("PERDEB");
          pnlDroitAuteur.add(PERDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- lbDateFin ----
          lbDateFin.setText("Date de fin");
          lbDateFin.setName("lbDateFin");
          pnlDroitAuteur.add(lbDateFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- PERFIN ----
          PERFIN.setComponentPopupMenu(null);
          PERFIN.setPreferredSize(new Dimension(105, 30));
          PERFIN.setMinimumSize(new Dimension(105, 30));
          PERFIN.setMaximumSize(new Dimension(105, 30));
          PERFIN.setName("PERFIN");
          pnlDroitAuteur.add(PERFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlDroitAuteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDroitAuteur2 ========
        {
          pnlDroitAuteur2.setBorder(
              new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
          pnlDroitAuteur2.setOpaque(false);
          pnlDroitAuteur2.setName("pnlDroitAuteur2");
          pnlDroitAuteur2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroitAuteur2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroitAuteur2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroitAuteur2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroitAuteur2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- REPONS ----
          REPONS.setText("Uniquement bons factur\u00e9s");
          REPONS.setComponentPopupMenu(null);
          REPONS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPONS.setFont(new Font("sansserif", Font.PLAIN, 14));
          REPONS.setName("REPONS");
          pnlDroitAuteur2.add(REPONS, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbNatureMontant ----
          lbNatureMontant.setText("Nature du montant");
          lbNatureMontant.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNatureMontant.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNatureMontant.setName("lbNatureMontant");
          pnlDroitAuteur2.add(lbNatureMontant, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- NATLOG ----
          NATLOG.setModel(new DefaultComboBoxModel<>(new String[] { "", "H.T", "TTC" }));
          NATLOG.setComponentPopupMenu(null);
          NATLOG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NATLOG.setFont(new Font("sansserif", Font.PLAIN, 14));
          NATLOG.setMinimumSize(new Dimension(70, 30));
          NATLOG.setPreferredSize(new Dimension(70, 30));
          NATLOG.setMaximumSize(new Dimension(70, 30));
          NATLOG.setName("NATLOG");
          pnlDroitAuteur2.add(NATLOG, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 10), 0, 0));
        }
        pnlContenu.add(pnlDroitAuteur2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDroitAuteur3 ========
        {
          pnlDroitAuteur3.setBorder(new TitledBorder(null, "Section analytique", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
              new Font("sansserif", Font.BOLD, 14)));
          pnlDroitAuteur3.setOpaque(false);
          pnlDroitAuteur3.setName("pnlDroitAuteur3");
          pnlDroitAuteur3.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroitAuteur3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroitAuteur3.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroitAuteur3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroitAuteur3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbSectionDebut ----
          lbSectionDebut.setText("Section de d\u00e9but");
          lbSectionDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbSectionDebut.setName("lbSectionDebut");
          pnlDroitAuteur3.add(lbSectionDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- SECT1 ----
          SECT1.setComponentPopupMenu(BTD);
          SECT1.setPreferredSize(new Dimension(50, 30));
          SECT1.setMinimumSize(new Dimension(50, 30));
          SECT1.setMaximumSize(new Dimension(50, 30));
          SECT1.setName("SECT1");
          pnlDroitAuteur3.add(SECT1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- lbSectionFin ----
          lbSectionFin.setText("Section de fin");
          lbSectionFin.setHorizontalAlignment(SwingConstants.RIGHT);
          lbSectionFin.setName("lbSectionFin");
          pnlDroitAuteur3.add(lbSectionFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- SECT2 ----
          SECT2.setComponentPopupMenu(BTD);
          SECT2.setPreferredSize(new Dimension(50, 30));
          SECT2.setMinimumSize(new Dimension(50, 30));
          SECT2.setMaximumSize(new Dimension(50, 30));
          SECT2.setName("SECT2");
          pnlDroitAuteur3.add(SECT2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlDroitAuteur3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(e -> OBJ_11ActionPerformed(e));
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private JPanel pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private RiZoneSortie z_wencx_2;
  private JPanel pnlDroitAuteur;
  private SNLabelChamp lbDateDebut;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbDateFin;
  private XRiCalendrier PERFIN;
  private JPanel pnlDroitAuteur2;
  private XRiCheckBox REPONS;
  private SNLabelChamp lbNatureMontant;
  private XRiComboBox NATLOG;
  private JPanel pnlDroitAuteur3;
  private SNLabelChamp lbSectionDebut;
  private XRiTextField SECT1;
  private SNLabelChamp lbSectionFin;
  private XRiTextField SECT2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
