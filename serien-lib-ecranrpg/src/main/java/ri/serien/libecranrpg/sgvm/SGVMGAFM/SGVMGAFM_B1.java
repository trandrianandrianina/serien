
package ri.serien.libecranrpg.sgvm.SGVMGAFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMGAFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] DEPART_Value = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17",
      "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
      "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61",
      "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83",
      "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "97", "99", };
  
  public SGVMGAFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    DEPART.setValeurs(DEPART_Value, null);
    ZPLONG.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPRO@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCCT@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBACT@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZOG@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBART@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBREP@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GENLIB@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LZP1@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LZP2@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LZP3@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LZP4@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LZP5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_61.setVisible(lexique.isPresent("LZP5"));
    OBJ_60.setVisible(lexique.isPresent("LZP4"));
    OBJ_59.setVisible(lexique.isPresent("LZP3"));
    OBJ_58.setVisible(lexique.isPresent("LZP2"));
    OBJ_57.setVisible(lexique.isPresent("LZP1"));
    OBJ_55.setVisible(lexique.isPresent("LIBZOG"));
    // ZPLONG.setSelected(lexique.HostFieldGetData("ZPLONG").equalsIgnoreCase("1"));
    OBJ_65.setVisible(lexique.isPresent("LIBACT"));
    OBJ_64.setVisible(lexique.isPresent("LIBCCT"));
    OBJ_52.setVisible(lexique.isPresent("LIBPRO"));
    OBJ_50.setVisible(lexique.isPresent("LIBART"));
    OBJ_46.setVisible(lexique.isPresent("LIBREP"));
    OBJ_44.setVisible(lexique.isPresent("GENLIB"));
    // DEPART.setSelectedIndex(getIndice("DEPART", DEPART_Value));
    
    

    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (ZPLONG.isSelected())
    // lexique.HostFieldPutData("ZPLONG", 0, "1");
    // else
    // lexique.HostFieldPutData("ZPLONG", 0, " ");
    // lexique.HostFieldPutData("DEPART", 0, DEPART_Value[DEPART.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void ZPLONGActionPerformed(ActionEvent e) {
    // P_SEL0.setVisible(!P_SEL0.isVisible()));
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    DEPART = new XRiComboBox();
    OBJ_52 = new RiZoneSortie();
    OBJ_64 = new RiZoneSortie();
    OBJ_65 = new RiZoneSortie();
    ZPLONG = new XRiCheckBox();
    OBJ_56 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_55 = new RiZoneSortie();
    OBJ_51 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_53 = new JLabel();
    CODZOG = new XRiTextField();
    CATPRO = new XRiTextField();
    CODCCT = new XRiTextField();
    CODACT = new XRiTextField();
    OBJ_50 = new RiZoneSortie();
    OBJ_47 = new JLabel();
    CODART = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_45 = new JLabel();
    CODREP = new XRiTextField();
    OBJ_46 = new RiZoneSortie();
    OBJ_43 = new JLabel();
    GENACT = new XRiTextField();
    OBJ_44 = new RiZoneSortie();
    label1 = new JLabel();
    DATTOU = new XRiCalendrier();
    panel1 = new JPanel();
    OBJ_57 = new RiZoneSortie();
    CODZP1 = new XRiTextField();
    OBJ_58 = new RiZoneSortie();
    CODZP2 = new XRiTextField();
    OBJ_59 = new RiZoneSortie();
    CODZP3 = new XRiTextField();
    OBJ_60 = new RiZoneSortie();
    CODZP4 = new XRiTextField();
    OBJ_61 = new RiZoneSortie();
    CODZP5 = new XRiTextField();
    DATAR1 = new XRiCalendrier();
    DATAR2 = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 780, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //---- OBJ_19 ----
          OBJ_19.setTitle("");
          OBJ_19.setName("OBJ_19");
          p_contenu.add(OBJ_19);
          OBJ_19.setBounds(35, 125, 780, OBJ_19.getPreferredSize().height);

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(null);
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- DEPART ----
            DEPART.setModel(new DefaultComboBoxModel(new String[] {
              "01 - AIN",
              "02 - AISNE",
              "03 - ALLIER",
              "04 - ALPES DE HAUTE PROVENCE",
              "05 - ALPES (HAUTES)",
              "06 - ALPES-MARITIMES",
              "07 - ARDECHE",
              "08 - ARDENNES",
              "09 - ARIEGE",
              "10 - AUBE",
              "11 - AUDE",
              "12 - AVEYRON",
              "13 - BOUCHES-DU-RHONE",
              "14 - CALVADOS",
              "15 - CANTAL",
              "16 - CHARENTE",
              "17 - CHARENTE MARITIME",
              "18 - CHER",
              "19 - CORREZE",
              "20 - CORSE",
              "21 - COTE D'OR",
              "22 - COTES-D'ARMOR",
              "23 - CREUSE",
              "24 - DORDOGNE",
              "25 - DOUBS",
              "26 - DROME",
              "27 - EURE",
              "28 - EURE ET LOIR",
              "29 - FINISTERE",
              "30 - GARD",
              "31 - HAUTE GARONNE",
              "32 - GERS",
              "33 - GIRONDE",
              "34 - HERAULT",
              "35 - ILLE ET VILAINE",
              "36 - INDRE",
              "37 - INDRE ET LOIRE",
              "38 - ISERE",
              "39 - JURA",
              "40 - LANDES",
              "41 - LOIR ET CHER",
              "42 - LOIRE",
              "43 - HAUTE LOIRE",
              "44 - LOIRE ATLANTIQUE",
              "45 - LOIRET",
              "46 - LOT",
              "47 - LOT ET GARONNE",
              "48 - LOZERE",
              "49 - MAINE ET LOIRE",
              "50 - MANCHE",
              "51 - MARNE",
              "52 - HAUTE MARNE",
              "53 - MAYENNE",
              "54 - MEURTHE ET MOSELLE",
              "55 - MEUSE",
              "56 - MORBIHAN",
              "57 - MOSELLE",
              "58 - NIEVRE",
              "59 - NORD",
              "60 - OISE",
              "61 - ORNE",
              "62 - PAS DE CALAIS",
              "63 - PUY DE DOME",
              "64 - PYRENEES ATLANTIQUES",
              "65 - HAUTES PYRENEES",
              "66 - PYRENEES ORIENTALES",
              "67 - BAS RHIN",
              "68 - HAUT RHIN",
              "69 - RHONE",
              "70 - HAUTE SAONE",
              "71 - SAONE ET LOIRE",
              "72 - SARTHE",
              "73 - SAVOIE",
              "74 - HAUTE SAVOIE",
              "75 - PARIS",
              "76 - SEINE MARITIME",
              "77 - SEINE ET MARNE",
              "78 - YVELINES",
              "79 - DEUX SEVRES",
              "80 - SOMME",
              "81 - TARN",
              "82 - TARN ET GARONNE",
              "83 - VAR",
              "84 - VAUCLUSE",
              "85 - VENDEE",
              "86 - VIENNE",
              "87 - HAUTE VIENNE",
              "88 - VOSGES",
              "89 - YONNE",
              "90 - TERRITOIRE DE BELFORT",
              "91 - ESSONNE",
              "92 - HAUTS DE SEINE",
              "93 - SEINE SAINT DENIS",
              "94 - VAL DE MARNE",
              "95 - VAL D'OISE",
              "97 - DOM-TOM",
              "99 - ETRANGER"
            }));
            DEPART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DEPART.setName("DEPART");
            P_SEL0.add(DEPART);
            DEPART.setBounds(235, 210, 222, DEPART.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("@LIBPRO@");
            OBJ_52.setName("OBJ_52");
            P_SEL0.add(OBJ_52);
            OBJ_52.setBounds(325, 177, 310, OBJ_52.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("@LIBCCT@");
            OBJ_64.setName("OBJ_64");
            P_SEL0.add(OBJ_64);
            OBJ_64.setBounds(325, 347, 231, OBJ_64.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("@LIBACT@");
            OBJ_65.setName("OBJ_65");
            P_SEL0.add(OBJ_65);
            OBJ_65.setBounds(325, 382, 231, OBJ_65.getPreferredSize().height);

            //---- ZPLONG ----
            ZPLONG.setText("Zones personnalisables longues");
            ZPLONG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZPLONG.setName("ZPLONG");
            ZPLONG.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ZPLONGActionPerformed(e);
              }
            });
            P_SEL0.add(ZPLONG);
            ZPLONG.setBounds(235, 315, 221, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Zones personnalisables courtes");
            OBJ_56.setName("OBJ_56");
            P_SEL0.add(OBJ_56);
            OBJ_56.setBounds(12, 285, 218, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("Type d'action exist. sur prospect");
            OBJ_63.setName("OBJ_63");
            P_SEL0.add(OBJ_63);
            OBJ_63.setBounds(12, 384, 213, 20);

            //---- OBJ_55 ----
            OBJ_55.setText("@LIBZOG@");
            OBJ_55.setName("OBJ_55");
            P_SEL0.add(OBJ_55);
            OBJ_55.setBounds(325, 247, 260, OBJ_55.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("Cat\u00e9gorie de prospects");
            OBJ_51.setName("OBJ_51");
            P_SEL0.add(OBJ_51);
            OBJ_51.setBounds(12, 179, 162, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("Zone g\u00e9ographique");
            OBJ_54.setName("OBJ_54");
            P_SEL0.add(OBJ_54);
            OBJ_54.setBounds(12, 249, 140, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("Fonction du contact");
            OBJ_62.setName("OBJ_62");
            P_SEL0.add(OBJ_62);
            OBJ_62.setBounds(12, 349, 136, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("D\u00e9partement");
            OBJ_53.setName("OBJ_53");
            P_SEL0.add(OBJ_53);
            OBJ_53.setBounds(12, 213, 99, 20);

            //---- CODZOG ----
            CODZOG.setComponentPopupMenu(BTD);
            CODZOG.setName("CODZOG");
            P_SEL0.add(CODZOG);
            CODZOG.setBounds(235, 245, 64, CODZOG.getPreferredSize().height);

            //---- CATPRO ----
            CATPRO.setComponentPopupMenu(BTD);
            CATPRO.setName("CATPRO");
            P_SEL0.add(CATPRO);
            CATPRO.setBounds(235, 175, 44, CATPRO.getPreferredSize().height);

            //---- CODCCT ----
            CODCCT.setComponentPopupMenu(BTD);
            CODCCT.setName("CODCCT");
            P_SEL0.add(CODCCT);
            CODCCT.setBounds(235, 345, 40, CODCCT.getPreferredSize().height);

            //---- CODACT ----
            CODACT.setComponentPopupMenu(BTD);
            CODACT.setName("CODACT");
            P_SEL0.add(CODACT);
            CODACT.setBounds(235, 380, 40, CODACT.getPreferredSize().height);

            //---- OBJ_50 ----
            OBJ_50.setText("@LIBART@");
            OBJ_50.setName("OBJ_50");
            P_SEL0.add(OBJ_50);
            OBJ_50.setBounds(235, 140, 310, OBJ_50.getPreferredSize().height);

            //---- OBJ_47 ----
            OBJ_47.setText("Ayant achet\u00e9 un article");
            OBJ_47.setName("OBJ_47");
            P_SEL0.add(OBJ_47);
            OBJ_47.setBounds(12, 110, 155, 20);

            //---- CODART ----
            CODART.setComponentPopupMenu(BTD);
            CODART.setName("CODART");
            P_SEL0.add(CODART);
            CODART.setBounds(235, 105, 214, CODART.getPreferredSize().height);

            //---- OBJ_48 ----
            OBJ_48.setText("entre le");
            OBJ_48.setName("OBJ_48");
            P_SEL0.add(OBJ_48);
            OBJ_48.setBounds(465, 110, 48, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("et le");
            OBJ_49.setName("OBJ_49");
            P_SEL0.add(OBJ_49);
            OBJ_49.setBounds(630, 110, 28, 20);

            //---- OBJ_45 ----
            OBJ_45.setText("Repr\u00e9sentant");
            OBJ_45.setName("OBJ_45");
            P_SEL0.add(OBJ_45);
            OBJ_45.setBounds(12, 75, 102, 20);

            //---- CODREP ----
            CODREP.setComponentPopupMenu(BTD);
            CODREP.setName("CODREP");
            P_SEL0.add(CODREP);
            CODREP.setBounds(235, 70, 34, CODREP.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("@LIBREP@");
            OBJ_46.setName("OBJ_46");
            P_SEL0.add(OBJ_46);
            OBJ_46.setBounds(325, 70, 310, OBJ_46.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Type d'action \u00e0 g\u00e9n\u00e9rer");
            OBJ_43.setName("OBJ_43");
            P_SEL0.add(OBJ_43);
            OBJ_43.setBounds(12, 40, 164, 20);

            //---- GENACT ----
            GENACT.setComponentPopupMenu(BTD);
            GENACT.setName("GENACT");
            P_SEL0.add(GENACT);
            GENACT.setBounds(235, 35, 44, GENACT.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("@GENLIB@");
            OBJ_44.setName("OBJ_44");
            P_SEL0.add(OBJ_44);
            OBJ_44.setBounds(325, 35, 310, OBJ_44.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Date tourn\u00e9e");
            label1.setName("label1");
            P_SEL0.add(label1);
            label1.setBounds(12, 2, 130, 25);

            //---- DATTOU ----
            DATTOU.setName("DATTOU");
            P_SEL0.add(DATTOU);
            DATTOU.setBounds(235, 0, 105, DATTOU.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_57 ----
              OBJ_57.setText("@LZP1@");
              OBJ_57.setName("OBJ_57");
              panel1.add(OBJ_57);
              OBJ_57.setBounds(5, 7, 34, OBJ_57.getPreferredSize().height);

              //---- CODZP1 ----
              CODZP1.setComponentPopupMenu(BTD);
              CODZP1.setName("CODZP1");
              panel1.add(CODZP1);
              CODZP1.setBounds(41, 5, 34, CODZP1.getPreferredSize().height);

              //---- OBJ_58 ----
              OBJ_58.setText("@LZP2@");
              OBJ_58.setName("OBJ_58");
              panel1.add(OBJ_58);
              OBJ_58.setBounds(77, 7, 34, OBJ_58.getPreferredSize().height);

              //---- CODZP2 ----
              CODZP2.setComponentPopupMenu(BTD);
              CODZP2.setName("CODZP2");
              panel1.add(CODZP2);
              CODZP2.setBounds(113, 5, 34, CODZP2.getPreferredSize().height);

              //---- OBJ_59 ----
              OBJ_59.setText("@LZP3@");
              OBJ_59.setName("OBJ_59");
              panel1.add(OBJ_59);
              OBJ_59.setBounds(149, 7, 34, OBJ_59.getPreferredSize().height);

              //---- CODZP3 ----
              CODZP3.setComponentPopupMenu(BTD);
              CODZP3.setName("CODZP3");
              panel1.add(CODZP3);
              CODZP3.setBounds(185, 5, 34, CODZP3.getPreferredSize().height);

              //---- OBJ_60 ----
              OBJ_60.setText("@LZP4@");
              OBJ_60.setName("OBJ_60");
              panel1.add(OBJ_60);
              OBJ_60.setBounds(221, 7, 34, OBJ_60.getPreferredSize().height);

              //---- CODZP4 ----
              CODZP4.setComponentPopupMenu(BTD);
              CODZP4.setName("CODZP4");
              panel1.add(CODZP4);
              CODZP4.setBounds(257, 5, 34, CODZP4.getPreferredSize().height);

              //---- OBJ_61 ----
              OBJ_61.setText("@LZP5@");
              OBJ_61.setName("OBJ_61");
              panel1.add(OBJ_61);
              OBJ_61.setBounds(293, 7, 34, OBJ_61.getPreferredSize().height);

              //---- CODZP5 ----
              CODZP5.setComponentPopupMenu(BTD);
              CODZP5.setName("CODZP5");
              panel1.add(CODZP5);
              CODZP5.setBounds(329, 5, 34, CODZP5.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            P_SEL0.add(panel1);
            panel1.setBounds(235, 275, 380, 40);

            //---- DATAR1 ----
            DATAR1.setTypeSaisie(6);
            DATAR1.setName("DATAR1");
            P_SEL0.add(DATAR1);
            DATAR1.setBounds(525, 105, 90, DATAR1.getPreferredSize().height);

            //---- DATAR2 ----
            DATAR2.setTypeSaisie(6);
            DATAR2.setName("DATAR2");
            P_SEL0.add(DATAR2);
            DATAR2.setBounds(670, 105, 90, DATAR2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL0);
          P_SEL0.setBounds(40, 140, P_SEL0.getPreferredSize().width, 435);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JPanel P_SEL0;
  private XRiComboBox DEPART;
  private RiZoneSortie OBJ_52;
  private RiZoneSortie OBJ_64;
  private RiZoneSortie OBJ_65;
  private XRiCheckBox ZPLONG;
  private JLabel OBJ_56;
  private JLabel OBJ_63;
  private RiZoneSortie OBJ_55;
  private JLabel OBJ_51;
  private JLabel OBJ_54;
  private JLabel OBJ_62;
  private JLabel OBJ_53;
  private XRiTextField CODZOG;
  private XRiTextField CATPRO;
  private XRiTextField CODCCT;
  private XRiTextField CODACT;
  private RiZoneSortie OBJ_50;
  private JLabel OBJ_47;
  private XRiTextField CODART;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private JLabel OBJ_45;
  private XRiTextField CODREP;
  private RiZoneSortie OBJ_46;
  private JLabel OBJ_43;
  private XRiTextField GENACT;
  private RiZoneSortie OBJ_44;
  private JLabel label1;
  private XRiCalendrier DATTOU;
  private JPanel panel1;
  private RiZoneSortie OBJ_57;
  private XRiTextField CODZP1;
  private RiZoneSortie OBJ_58;
  private XRiTextField CODZP2;
  private RiZoneSortie OBJ_59;
  private XRiTextField CODZP3;
  private RiZoneSortie OBJ_60;
  private XRiTextField CODZP4;
  private RiZoneSortie OBJ_61;
  private XRiTextField CODZP5;
  private XRiCalendrier DATAR1;
  private XRiCalendrier DATAR2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
