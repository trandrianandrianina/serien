
package ri.serien.libecranrpg.sgvm.SGVM59FM;
// Nom Fichier: pop_SGVM59FM_FMTB6_FMTF1_500.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVM59FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM59FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    CODARR.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    THHMN.setVisible(lexique.isPresent("THHMN"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Traitement par lots"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    lbHeureTraitement = new SNLabelChamp();
    THHMN = new XRiTextField();
    CODARR = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(375, 185));
    setPreferredSize(new Dimension(375, 185));
    setMaximumSize(new Dimension(375, 185));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //---- lbHeureTraitement ----
      lbHeureTraitement.setText("Heure de traitement (hhmm):");
      lbHeureTraitement.setFont(new Font("sansserif", Font.PLAIN, 14));
      lbHeureTraitement.setPreferredSize(new Dimension(185, 30));
      lbHeureTraitement.setMinimumSize(new Dimension(185, 30));
      lbHeureTraitement.setName("lbHeureTraitement");
      pnlContenu.add(lbHeureTraitement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- THHMN ----
      THHMN.setPreferredSize(new Dimension(56, 30));
      THHMN.setMinimumSize(new Dimension(56, 30));
      THHMN.setMaximumSize(new Dimension(56, 30));
      THHMN.setFont(new Font("sansserif", Font.PLAIN, 14));
      THHMN.setName("THHMN");
      pnlContenu.add(THHMN, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- CODARR ----
      CODARR.setText("Arr\u00eat de la machine");
      CODARR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      CODARR.setFont(new Font("sansserif", Font.PLAIN, 14));
      CODARR.setMinimumSize(new Dimension(146, 30));
      CODARR.setPreferredSize(new Dimension(146, 30));
      CODARR.setName("CODARR");
      pnlContenu.add(CODARR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbHeureTraitement;
  private XRiTextField THHMN;
  private XRiCheckBox CODARR;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
