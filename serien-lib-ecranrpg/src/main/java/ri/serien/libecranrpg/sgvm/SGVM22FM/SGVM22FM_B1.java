
package ri.serien.libecranrpg.sgvm.SGVM22FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2811] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Factures
 * Indicateur : 00000001
 * Titre : Edition des factures
 * 
 * [GVM2812] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Duplicatas de factures
 * Indicateur : 00010001 (94)
 * Titre : Reedition des factures (duplicata)
 * 
 * [GVM2813] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Factures/traites
 * Indicateur : 00001001 (95)
 * Titre : Edition des factures avec traites
 * 
 * [GVM2814] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Duplicatas de factures/traites
 * Indicateur : 00011001 (94,95)
 * Titre : Réédition des factures (duplicata de factures avec traites)
 * 
 * [GVM2816] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Dématérialisation des factures
 * Indicateur : 00010101 (94,96)
 * Titre : Dématérialisation des factures
 */
public class SGVM22FM_B1 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_TRIER = "Trier l'édition";
  private static final String BOUTON_AFFICHER = "Afficher le document généré";
  
  // Variables
  private Message messagePlanning = null;
  private Message information = null;
  
  public SGVM22FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    WSER.setValeursSelection("OUI", "NON");
    WTOU.setValeursSelection("**", "  ");
    EPOTRV.setValeursSelection("1", "  ");
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_TRIER, 't', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER, 'a', false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbWTYPI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTYPI@")).trim());
    tfDateEtablissement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverse après initialisation des données
    setDiverses();
    
    // Indicateurs
    Boolean isMagasin = lexique.isTrue("35");
    Boolean isGroupeCanalVente = lexique.isTrue("36");
    Boolean isEditionMicroArchivage = lexique.isTrue("91");
    Boolean isPeriodeEditer = lexique.isTrue("(N91) and (N94)");
    Boolean isPeriodeReediter = lexique.isTrue("(N91) and 94");
    Boolean isParametrage = lexique.isTrue("97");
    Boolean isEditionFactures = lexique.isTrue("(N94) and (N95) and (N96)");
    Boolean isReeditionFactureAvecTraite = lexique.isTrue("(94) and (N95)");
    Boolean isEditionFactureTraite = lexique.isTrue("(N94) and (95)");
    Boolean isReeditionFactureFactureSurTraite = lexique.isTrue("(94) and (95)");
    Boolean isDemateralisationFacture = lexique.isTrue("(94) and (96) and (N95)");
    
    // Active le bouton éditer et trier l'édition si une date est saisie
    if (isReeditionFactureAvecTraite || isReeditionFactureFactureSurTraite || isDemateralisationFacture) {
      if (WDATDX.getDate() == null) {
        snBarreBouton.activerBouton(EnumBouton.EDITER, false);
        snBarreBouton.activerBouton(BOUTON_TRIER, false);
      }
      else {
        snBarreBouton.activerBouton(EnumBouton.EDITER, true);
        snBarreBouton.activerBouton(BOUTON_TRIER, true);
      }
    }
    
    // Active le bouton d'affichage du document PDF généré
    // S'il s'agit d'une édition à partir du poste de travail et si la variable contenant le chemin est renseignée
    String documentPdf = Constantes.normerTexte(lexique.HostFieldGetData("WDPDF"));
    if (EPOTRV.isSelected() && !documentPdf.isEmpty()) {
      snBarreBouton.activerBouton(BOUTON_AFFICHER, true);
    }
    else {
      snBarreBouton.activerBouton(BOUTON_AFFICHER, false);
    }
    
    // Met le libellé pour l'édition de la période
    if (isEditionMicroArchivage) {
      lbPeriode.setText("Edition pour l'archivage micro-fiches du");
    }
    else {
      if (isPeriodeReediter) {
        lbPeriode.setText("Période à rééditer du");
      }
      else {
        if (isPeriodeEditer) {
          lbPeriode.setText("Période à éditer du");
        }
      }
    }
    
    // Mets le bon titre
    if (isEditionFactures) {
      pbPresentation.setText("Edition des factures");
      WTOU.setText("Edition des factures non éditées (boîte aux lettres)");
      lbInformation.setVisible(false);
    }
    if (isReeditionFactureAvecTraite) {
      pbPresentation.setText("Réédition des factures (duplicata)");
      lbInformation.setVisible(true);
    }
    if (isEditionFactureTraite) {
      pbPresentation.setText("Edition des factures avec traites");
      lbInformation.setVisible(false);
    }
    if (isReeditionFactureFactureSurTraite) {
      pbPresentation.setText("Réédition des factures (duplicata de factures sur traites)");
      lbInformation.setVisible(true);
    }
    if (isDemateralisationFacture) {
      pbPresentation.setText("Dématérialisation des factures");
      lbInformation.setVisible(true);
    }
    
    // Message d'informations indiquant si la période est obligatoire
    information = information.getMessageNormal("Merci de choisir une date de début afin de pouvoir éditer");
    lbInformation.setMessage(information);
    
    // Message planning
    lbPlanning.setVisible(false);
    if (isParametrage) {
      messagePlanning = messagePlanning.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(messagePlanning);
      lbPlanning.setVisible(true);
    }
    
    // Recupere la valeur de WTYPI3
    lbWTYPI.setText(lexique.HostFieldGetData("WTYPI"));
    
    // Visibilité des composants
    lbAuPeriode.setVisible(lexique.isPresent("WDATDX"));
    lbWTYPI.setVisible(!lexique.isPresent("WTYPI"));
    
    // Gere la visibilité des composants magasin et canal de vente
    snMagasin.setVisible(false);
    snCanalDeVente.setVisible(false);
    lbMagasinOuCanalVente.setVisible(false);
    
    if (isMagasin) {
      lbMagasinOuCanalVente.setText("Magasin");
      lbMagasinOuCanalVente.setVisible(true);
      snMagasin.setVisible(true);
      snCanalDeVente.setVisible(false);
    }
    else if (isGroupeCanalVente) {
      lbMagasinOuCanalVente.setText("Groupe canal de vente");
      lbMagasinOuCanalVente.setVisible(true);
      snMagasin.setVisible(false);
      snCanalDeVente.setVisible(true);
    }
    pnlTypeVente.setVisible(snMagasin.isVisible() || snCanalDeVente.isVisible());
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Disponibilité des composant
    WNDEB.setEnabled(!WTOU.isSelected());
    WNFIN.setEnabled(!WTOU.isSelected());
    if (WTOU.isSelected()) {
      WNDEB.setText("");
      WNFIN.setText("");
    }
    
    // Charge l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Logo
    pbPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charge les composants
    chargerComposantMagasin();
    chargerComposantCanalDeVente();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snMagasin.isVisible()) {
      snMagasin.renseignerChampRPG(lexique, "UMAG");
    }
    if (snCanalDeVente.isVisible()) {
      snCanalDeVente.renseignerChampRPG(lexique, "UMAG");
    }
  }
  
  private void selectionCompleteActionPerformed(ActionEvent e) {
    WNDEB.setEnabled(!WTOU.isSelected());
    WNFIN.setEnabled(!WTOU.isSelected());
    if (WTOU.isSelected()) {
      WNDEB.setText("");
      WNFIN.setText("");
    }
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostFieldPutData("WCHOIX", 0, "");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_TRIER)) {
        lexique.HostFieldPutData("WCHOIX", 0, "X");
        snEtablissement.renseignerChampRPG(lexique, "WETB");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER)) {
        afficherDocumentPdf();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Affiche le fichier PDF qui vient d'être générédans le cas d'une édition à partir du poste de travail.
   */
  private void afficherDocumentPdf() {
    String documentIfs = Constantes.normerTexte(lexique.HostFieldGetData("WDPDF"));
    if (documentIfs.isEmpty()) {
      return;
    }
    String documentPdfsurPc = lexique.RecuperationFichier(documentIfs);
    lexique.AfficheDocument(documentPdfsurPc);
  }
  
  /**
   * Charge le composant magasin
   */
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "UMAG");
  }
  
  /**
   * Charge le composant canal de vente
   */
  private void chargerComposantCanalDeVente() {
    snCanalDeVente.setSession(getSession());
    snCanalDeVente.setIdEtablissement(snEtablissement.getIdSelection());
    snCanalDeVente.setTousAutorise(true);
    snCanalDeVente.charger(true);
    snCanalDeVente.setSelectionParChampRPG(lexique, "UMAG");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantMagasin();
      chargerComposantCanalDeVente();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void calendrierActionPerformed(ActionEvent e) {
    Boolean isReeditionFactureAvecTraite = lexique.isTrue("(94) and (N95)");
    Boolean isReeditionFactureFactureSurTraite = lexique.isTrue("(94) and (95)");
    Boolean isDemateralisationFacture = lexique.isTrue("(94) and (96) and (N95)");
    if (isReeditionFactureAvecTraite || isReeditionFactureFactureSurTraite || isDemateralisationFacture) {
      if (WDATDX.getDate() == null && WNDEB.getText().isEmpty()) {
        snBarreBouton.activerBouton(EnumBouton.EDITER, false);
        snBarreBouton.activerBouton(BOUTON_TRIER, false);
      }
      else {
        snBarreBouton.activerBouton(EnumBouton.EDITER, true);
        snBarreBouton.activerBouton(BOUTON_TRIER, true);
      }
    }
  }
  
  private void WNDEBFocusLost(FocusEvent e) {
    Boolean isReeditionFactureAvecTraite = lexique.isTrue("(94) and (N95)");
    Boolean isReeditionFactureFactureSurTraite = lexique.isTrue("(94) and (95)");
    Boolean isDemateralisationFacture = lexique.isTrue("(94) and (96) and (N95)");
    try {
      if (isReeditionFactureAvecTraite || isReeditionFactureFactureSurTraite || isDemateralisationFacture) {
        if (WDATDX.getDate() == null && WNDEB.getText().isEmpty()) {
          snBarreBouton.activerBouton(EnumBouton.EDITER, false);
          snBarreBouton.activerBouton(BOUTON_TRIER, false);
        }
        else {
          snBarreBouton.activerBouton(EnumBouton.EDITER, true);
          snBarreBouton.activerBouton(BOUTON_TRIER, true);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pbPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbPlanning = new SNMessage();
    lbWTYPI = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCriteresSelection = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    lbPlageDePeriode = new SNLabelChamp();
    pnlNumeroFacture = new SNPanel();
    WNDEB = new XRiTextField();
    lbAPlage = new SNLabelChamp();
    WNFIN = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEtablissement = new SNTexte();
    pnlSousCriteresSelection = new SNPanelTitre();
    lbPeriode = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    WDATDX = new XRiCalendrier();
    lbAuPeriode = new SNLabelChamp();
    WDATFX = new XRiCalendrier();
    lbInformation = new SNLabelTitre();
    lbMagasinOuCanalVente = new SNLabelChamp();
    pnlTypeVente = new SNPanel();
    snCanalDeVente = new SNCanalDeVente();
    snMagasin = new SNMagasin();
    lbPlageDeMontantsMinimum = new SNLabelChamp();
    pnlMontant = new SNPanel();
    MTTMIN = new XRiTextField();
    lbPlageDeMontantsMaximum = new SNLabelChamp();
    MTTMAX = new XRiTextField();
    pnlOptionEdition = new SNPanelTitre();
    WSER = new XRiCheckBox();
    EPOTRV = new XRiCheckBox();
    lbNombreExemplaires = new SNLabelChamp();
    WNEX = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- pbPresentation ----
    pbPresentation.setText("Reedition des factures");
    pbPresentation.setFont(new Font("sansserif", Font.PLAIN, 14));
    pbPresentation.setName("pbPresentation");
    add(pbPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbPlanning ----
        lbPlanning.setText("Label Planning");
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbWTYPI ----
        lbWTYPI.setText("@WTYPI@");
        lbWTYPI.setHorizontalAlignment(SwingConstants.RIGHT);
        lbWTYPI.setPreferredSize(new Dimension(135, 30));
        lbWTYPI.setMinimumSize(new Dimension(135, 30));
        lbWTYPI.setMaximumSize(new Dimension(135, 20));
        lbWTYPI.setName("lbWTYPI");
        pnlMessage.add(lbWTYPI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCriteresSelection ========
          {
            pnlCriteresSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresSelection.setName("pnlCriteresSelection");
            pnlCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- WTOU ----
            WTOU.setText("Toutes");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTOU.setMinimumSize(new Dimension(90, 30));
            WTOU.setPreferredSize(new Dimension(90, 30));
            WTOU.setMaximumSize(new Dimension(90, 30));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                selectionCompleteActionPerformed(e);
              }
            });
            pnlCriteresSelection.add(WTOU, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPlageDePeriode ----
            lbPlageDePeriode.setText("Plage de factures de");
            lbPlageDePeriode.setMaximumSize(new Dimension(200, 30));
            lbPlageDePeriode.setPreferredSize(new Dimension(150, 19));
            lbPlageDePeriode.setMinimumSize(new Dimension(140, 19));
            lbPlageDePeriode.setName("lbPlageDePeriode");
            pnlCriteresSelection.add(lbPlageDePeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlNumeroFacture ========
            {
              pnlNumeroFacture.setOpaque(false);
              pnlNumeroFacture.setName("pnlNumeroFacture");
              pnlNumeroFacture.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNumeroFacture.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WNDEB ----
              WNDEB.setMinimumSize(new Dimension(80, 30));
              WNDEB.setPreferredSize(new Dimension(80, 30));
              WNDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNDEB.setName("WNDEB");
              WNDEB.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                  WNDEBFocusLost(e);
                }
              });
              pnlNumeroFacture.add(WNDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAPlage ----
              lbAPlage.setText("\u00e0");
              lbAPlage.setMinimumSize(new Dimension(8, 30));
              lbAPlage.setMaximumSize(new Dimension(8, 30));
              lbAPlage.setPreferredSize(new Dimension(8, 30));
              lbAPlage.setName("lbAPlage");
              pnlNumeroFacture.add(lbAPlage, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WNFIN ----
              WNFIN.setPreferredSize(new Dimension(80, 30));
              WNFIN.setMinimumSize(new Dimension(80, 30));
              WNFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNFIN.setName("WNFIN");
              pnlNumeroFacture.add(WNFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresSelection.add(pnlNumeroFacture, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionne ========
          {
            pnlEtablissementSelectionne.setTitre("Etablissement");
            pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
            pnlEtablissementSelectionne.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionne.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfDateEtablissement ----
            tfDateEtablissement.setText("@WENCX@");
            tfDateEtablissement.setMaximumSize(new Dimension(272, 20));
            tfDateEtablissement.setEditable(false);
            tfDateEtablissement.setPreferredSize(new Dimension(272, 30));
            tfDateEtablissement.setMinimumSize(new Dimension(272, 30));
            tfDateEtablissement.setEnabled(false);
            tfDateEtablissement.setName("tfDateEtablissement");
            pnlEtablissementSelectionne.add(tfDateEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlSousCriteresSelection ========
          {
            pnlSousCriteresSelection.setTitre("Crit\u00e8res de filtrage");
            pnlSousCriteresSelection.setName("pnlSousCriteresSelection");
            pnlSousCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSousCriteresSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlSousCriteresSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlSousCriteresSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlSousCriteresSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode \u00e0 \u00e9diter de[voir code]");
            lbPeriode.setMaximumSize(new Dimension(200, 30));
            lbPeriode.setPreferredSize(new Dimension(190, 30));
            lbPeriode.setMinimumSize(new Dimension(190, 30));
            lbPeriode.setName("lbPeriode");
            pnlSousCriteresSelection.add(lbPeriode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDATDX ----
              WDATDX.setPreferredSize(new Dimension(110, 30));
              WDATDX.setMinimumSize(new Dimension(110, 30));
              WDATDX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATDX.setName("WDATDX");
              WDATDX.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  calendrierActionPerformed(e);
                }
              });
              pnlPeriodeAEditer.add(WDATDX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAuPeriode ----
              lbAuPeriode.setText("au");
              lbAuPeriode.setPreferredSize(new Dimension(16, 30));
              lbAuPeriode.setMaximumSize(new Dimension(16, 30));
              lbAuPeriode.setMinimumSize(new Dimension(16, 30));
              lbAuPeriode.setName("lbAuPeriode");
              pnlPeriodeAEditer.add(lbAuPeriode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDATFX ----
              WDATFX.setMinimumSize(new Dimension(110, 30));
              WDATFX.setPreferredSize(new Dimension(110, 30));
              WDATFX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATFX.setName("WDATFX");
              WDATFX.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  calendrierActionPerformed(e);
                }
              });
              pnlPeriodeAEditer.add(WDATFX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlSousCriteresSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbInformation ----
            lbInformation.setText("Merci de choisir une date de d\u00e9but afin de pouvoir \u00e9diter");
            lbInformation.setName("lbInformation");
            pnlSousCriteresSelection.add(lbInformation, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasinOuCanalVente ----
            lbMagasinOuCanalVente.setText("Groupe canal de vente");
            lbMagasinOuCanalVente.setPreferredSize(new Dimension(190, 30));
            lbMagasinOuCanalVente.setMinimumSize(new Dimension(190, 30));
            lbMagasinOuCanalVente.setMaximumSize(new Dimension(200, 30));
            lbMagasinOuCanalVente.setName("lbMagasinOuCanalVente");
            pnlSousCriteresSelection.add(lbMagasinOuCanalVente, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlTypeVente ========
            {
              pnlTypeVente.setOpaque(false);
              pnlTypeVente.setName("pnlTypeVente");
              pnlTypeVente.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlTypeVente.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlTypeVente.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlTypeVente.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlTypeVente.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- snCanalDeVente ----
              snCanalDeVente.setName("snCanalDeVente");
              pnlTypeVente.add(snCanalDeVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snMagasin ----
              snMagasin.setName("snMagasin");
              pnlTypeVente.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlSousCriteresSelection.add(pnlTypeVente, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPlageDeMontantsMinimum ----
            lbPlageDeMontantsMinimum.setText("Montants TTC minimum");
            lbPlageDeMontantsMinimum.setMaximumSize(new Dimension(200, 30));
            lbPlageDeMontantsMinimum.setPreferredSize(new Dimension(190, 30));
            lbPlageDeMontantsMinimum.setMinimumSize(new Dimension(190, 30));
            lbPlageDeMontantsMinimum.setName("lbPlageDeMontantsMinimum");
            pnlSousCriteresSelection.add(lbPlageDeMontantsMinimum, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlMontant ========
            {
              pnlMontant.setName("pnlMontant");
              pnlMontant.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMontant.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlMontant.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMontant.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMontant.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- MTTMIN ----
              MTTMIN.setPreferredSize(new Dimension(90, 30));
              MTTMIN.setMinimumSize(new Dimension(90, 30));
              MTTMIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              MTTMIN.setName("MTTMIN");
              pnlMontant.add(MTTMIN, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbPlageDeMontantsMaximum ----
              lbPlageDeMontantsMaximum.setText("Maximum");
              lbPlageDeMontantsMaximum.setMaximumSize(new Dimension(65, 30));
              lbPlageDeMontantsMaximum.setPreferredSize(new Dimension(65, 30));
              lbPlageDeMontantsMaximum.setMinimumSize(new Dimension(65, 30));
              lbPlageDeMontantsMaximum.setName("lbPlageDeMontantsMaximum");
              pnlMontant.add(lbPlageDeMontantsMaximum, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MTTMAX ----
              MTTMAX.setMinimumSize(new Dimension(90, 30));
              MTTMAX.setPreferredSize(new Dimension(90, 30));
              MTTMAX.setFont(new Font("sansserif", Font.PLAIN, 14));
              MTTMAX.setMaximumSize(new Dimension(90, 30));
              MTTMAX.setName("MTTMAX");
              pnlMontant.add(MTTMAX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlSousCriteresSelection.add(pnlMontant, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlSousCriteresSelection, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Options d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- WSER ----
            WSER.setText("Edition des num\u00e9ros de s\u00e9rie");
            WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSER.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSER.setMinimumSize(new Dimension(207, 30));
            WSER.setPreferredSize(new Dimension(207, 30));
            WSER.setName("WSER");
            pnlOptionEdition.add(WSER, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPOTRV ----
            EPOTRV.setText("Edition \u00e0 partir du poste de travail");
            EPOTRV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOTRV.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPOTRV.setMinimumSize(new Dimension(207, 30));
            EPOTRV.setPreferredSize(new Dimension(207, 30));
            EPOTRV.setName("EPOTRV");
            pnlOptionEdition.add(EPOTRV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbNombreExemplaires ----
            lbNombreExemplaires.setText("Nombre d'exemplaires");
            lbNombreExemplaires.setMaximumSize(new Dimension(250, 30));
            lbNombreExemplaires.setMinimumSize(new Dimension(250, 30));
            lbNombreExemplaires.setPreferredSize(new Dimension(250, 30));
            lbNombreExemplaires.setName("lbNombreExemplaires");
            pnlOptionEdition.add(lbNombreExemplaires, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WNEX ----
            WNEX.setMinimumSize(new Dimension(24, 28));
            WNEX.setPreferredSize(new Dimension(24, 28));
            WNEX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNEX.setName("WNEX");
            pnlOptionEdition.add(WNEX, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre pbPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNMessage lbPlanning;
  private SNLabelTitre lbWTYPI;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresSelection;
  private XRiCheckBox WTOU;
  private SNLabelChamp lbPlageDePeriode;
  private SNPanel pnlNumeroFacture;
  private XRiTextField WNDEB;
  private SNLabelChamp lbAPlage;
  private XRiTextField WNFIN;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEtablissement;
  private SNPanelTitre pnlSousCriteresSelection;
  private SNLabelChamp lbPeriode;
  private SNPanel pnlPeriodeAEditer;
  private XRiCalendrier WDATDX;
  private SNLabelChamp lbAuPeriode;
  private XRiCalendrier WDATFX;
  private SNLabelTitre lbInformation;
  private SNLabelChamp lbMagasinOuCanalVente;
  private SNPanel pnlTypeVente;
  private SNCanalDeVente snCanalDeVente;
  private SNMagasin snMagasin;
  private SNLabelChamp lbPlageDeMontantsMinimum;
  private SNPanel pnlMontant;
  private XRiTextField MTTMIN;
  private SNLabelChamp lbPlageDeMontantsMaximum;
  private XRiTextField MTTMAX;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox WSER;
  private XRiCheckBox EPOTRV;
  private SNLabelChamp lbNombreExemplaires;
  private XRiTextField WNEX;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
