
package ri.serien.libecranrpg.sgvm.SGVM1BFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM1BFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM1BFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    LIB4.setValeursSelection("X", " ");
    LIB3.setValeursSelection("X", " ");
    LIB2.setValeursSelection("X", " ");
    LIB1.setValeursSelection("X", " ");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_44.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG@")).trim());
    FALIBD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBD@")).trim());
    FALIBF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBF@")).trim());
    A1LIBD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBD@")).trim());
    A1LIBF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBF@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCNV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    NTAR.setEnabled(lexique.isPresent("NTAR"));
    // OBJ_52.setVisible(!interpreteurD.analyseExpression("@WTOU@").trim().equalsIgnoreCase("**"));
    WMAG.setEnabled(lexique.isPresent("WMAG"));
    // LIB4.setEnabled( lexique.isPresent("LIB4"));
    // LIB4.setSelected(lexique.HostFieldGetData("LIB4").equalsIgnoreCase("X"));
    // LIB3.setEnabled( lexique.isPresent("LIB3"));
    // LIB3.setSelected(lexique.HostFieldGetData("LIB3").equalsIgnoreCase("X"));
    // LIB2.setEnabled( lexique.isPresent("LIB2"));
    // LIB2.setSelected(lexique.HostFieldGetData("LIB2").equalsIgnoreCase("X"));
    // LIB1.setEnabled( lexique.isPresent("LIB1"));
    // LIB1.setSelected(lexique.HostFieldGetData("LIB1").equalsIgnoreCase("X"));
    NBETQ.setEnabled(lexique.isPresent("NBETQ"));
    WCNV.setEnabled(lexique.isPresent("WCNV"));
    // WTOU.setEnabled( lexique.isPresent("WTOU"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    OBJ_62.setVisible(lexique.isPresent("LIBCNV"));
    
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    
    // mot classement1
    FINIAC.setVisible(lexique.isTrue("91"));
    DEBIAC.setVisible(lexique.isTrue("91"));
    // articles
    DEBLO2.setVisible(lexique.isTrue("92"));
    FINLO2.setVisible(lexique.isTrue("92"));
    A1LIBD.setVisible(lexique.isTrue("92"));
    A1LIBF.setVisible(lexique.isTrue("92"));
    // groupe famille
    FINIAF.setVisible(lexique.isTrue("93"));
    DEBIAF.setVisible(lexique.isTrue("93"));
    FALIBD.setVisible(lexique.isTrue("93"));
    FALIBF.setVisible(lexique.isTrue("93"));
    // mot classement 2
    DEBIAK.setVisible(lexique.isTrue("94"));
    FINIAK.setVisible(lexique.isTrue("94"));
    
    if (lexique.isTrue("91")) {
      OBJ_44.setTitle("Plage de mots de classement 1");
    }
    if (lexique.isTrue("92")) {
      OBJ_44.setTitle("Plage de codes articles");
    }
    if (lexique.isTrue("93")) {
      OBJ_44.setTitle("Plage de codes groupe-famille");
    }
    if (lexique.isTrue("94")) {
      OBJ_44.setTitle("Plage de mots de classement 2");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@&&G_TITRE@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (LIB4.isSelected())
    // lexique.HostFieldPutData("LIB4", 0, "X");
    // else
    // lexique.HostFieldPutData("LIB4", 0, " ");
    // if (LIB3.isSelected())
    // lexique.HostFieldPutData("LIB3", 0, "X");
    // else
    // lexique.HostFieldPutData("LIB3", 0, " ");
    // if (LIB2.isSelected())
    // lexique.HostFieldPutData("LIB2", 0, "X");
    // else
    // lexique.HostFieldPutData("LIB2", 0, " ");
    // if (LIB1.isSelected())
    // lexique.HostFieldPutData("LIB1", 0, "X");
    // else
    // lexique.HostFieldPutData("LIB1", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_44 = new JXTitledSeparator();
    WTOU = new XRiCheckBox();
    P_SEL0 = new JPanel();
    OBJ_55 = new JLabel();
    OBJ_50 = new JLabel();
    DEBIAF = new XRiTextField();
    FINIAF = new XRiTextField();
    FALIBD = new RiZoneSortie();
    FALIBF = new RiZoneSortie();
    DEBIAC = new XRiTextField();
    DEBIAK = new XRiTextField();
    DEBLO2 = new XRiTextField();
    FINIAC = new XRiTextField();
    FINLO2 = new XRiTextField();
    FINIAK = new XRiTextField();
    A1LIBD = new RiZoneSortie();
    A1LIBF = new RiZoneSortie();
    OBJ_56 = new JXTitledSeparator();
    OBJ_57 = new JLabel();
    WMAG = new XRiTextField();
    OBJ_60 = new JLabel();
    OBJ_59 = new JXTitledSeparator();
    WCNV = new XRiTextField();
    OBJ_62 = new RiZoneSortie();
    OBJ_63 = new JXTitledSeparator();
    OBJ_64 = new JLabel();
    NTAR = new XRiTextField();
    OBJ_66 = new JXTitledSeparator();
    OBJ_67 = new JLabel();
    LIB1 = new XRiCheckBox();
    LIB2 = new XRiCheckBox();
    LIB3 = new XRiCheckBox();
    LIB4 = new XRiCheckBox();
    OBJ_72 = new JXTitledSeparator();
    OBJ_73 = new JLabel();
    NBETQ = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_44 ----
          OBJ_44.setTitle("@PLAG@");
          OBJ_44.setName("OBJ_44");

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setComponentPopupMenu(BTD);
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_55 ----
            OBJ_55.setText("Fin");
            OBJ_55.setName("OBJ_55");
            P_SEL0.add(OBJ_55);
            OBJ_55.setBounds(25, 44, 85, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("D\u00e9but");
            OBJ_50.setName("OBJ_50");
            P_SEL0.add(OBJ_50);
            OBJ_50.setBounds(25, 14, 85, 20);

            //---- DEBIAF ----
            DEBIAF.setComponentPopupMenu(BTD);
            DEBIAF.setName("DEBIAF");
            P_SEL0.add(DEBIAF);
            DEBIAF.setBounds(115, 10, 40, DEBIAF.getPreferredSize().height);

            //---- FINIAF ----
            FINIAF.setComponentPopupMenu(BTD);
            FINIAF.setName("FINIAF");
            P_SEL0.add(FINIAF);
            FINIAF.setBounds(115, 40, 40, FINIAF.getPreferredSize().height);

            //---- FALIBD ----
            FALIBD.setText("@FALIBD@");
            FALIBD.setName("FALIBD");
            P_SEL0.add(FALIBD);
            FALIBD.setBounds(190, 12, 295, 24);

            //---- FALIBF ----
            FALIBF.setText("@FALIBF@");
            FALIBF.setName("FALIBF");
            P_SEL0.add(FALIBF);
            FALIBF.setBounds(190, 42, 295, 24);

            //---- DEBIAC ----
            DEBIAC.setComponentPopupMenu(BTD);
            DEBIAC.setName("DEBIAC");
            P_SEL0.add(DEBIAC);
            DEBIAC.setBounds(115, 10, 210, DEBIAC.getPreferredSize().height);

            //---- DEBIAK ----
            DEBIAK.setComponentPopupMenu(BTD);
            DEBIAK.setName("DEBIAK");
            P_SEL0.add(DEBIAK);
            DEBIAK.setBounds(115, 10, 210, DEBIAK.getPreferredSize().height);

            //---- DEBLO2 ----
            DEBLO2.setComponentPopupMenu(BTD);
            DEBLO2.setName("DEBLO2");
            P_SEL0.add(DEBLO2);
            DEBLO2.setBounds(115, 10, 210, DEBLO2.getPreferredSize().height);

            //---- FINIAC ----
            FINIAC.setComponentPopupMenu(BTD);
            FINIAC.setName("FINIAC");
            P_SEL0.add(FINIAC);
            FINIAC.setBounds(115, 40, 210, FINIAC.getPreferredSize().height);

            //---- FINLO2 ----
            FINLO2.setComponentPopupMenu(BTD);
            FINLO2.setName("FINLO2");
            P_SEL0.add(FINLO2);
            FINLO2.setBounds(115, 40, 210, FINLO2.getPreferredSize().height);

            //---- FINIAK ----
            FINIAK.setComponentPopupMenu(BTD);
            FINIAK.setName("FINIAK");
            P_SEL0.add(FINIAK);
            FINIAK.setBounds(115, 40, 210, FINIAK.getPreferredSize().height);

            //---- A1LIBD ----
            A1LIBD.setText("@A1LIBD@");
            A1LIBD.setName("A1LIBD");
            P_SEL0.add(A1LIBD);
            A1LIBD.setBounds(340, 12, 295, A1LIBD.getPreferredSize().height);

            //---- A1LIBF ----
            A1LIBF.setText("@A1LIBF@");
            A1LIBF.setName("A1LIBF");
            P_SEL0.add(A1LIBF);
            A1LIBF.setBounds(340, 42, 295, A1LIBF.getPreferredSize().height);
          }

          //---- OBJ_56 ----
          OBJ_56.setTitle("");
          OBJ_56.setName("OBJ_56");

          //---- OBJ_57 ----
          OBJ_57.setText("Magasin de stockage");
          OBJ_57.setName("OBJ_57");

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");

          //---- OBJ_60 ----
          OBJ_60.setText("Condition de vente");
          OBJ_60.setName("OBJ_60");

          //---- OBJ_59 ----
          OBJ_59.setTitle("");
          OBJ_59.setName("OBJ_59");

          //---- WCNV ----
          WCNV.setComponentPopupMenu(BTD);
          WCNV.setName("WCNV");

          //---- OBJ_62 ----
          OBJ_62.setText("@LIBCNV@");
          OBJ_62.setName("OBJ_62");

          //---- OBJ_63 ----
          OBJ_63.setTitle("");
          OBJ_63.setName("OBJ_63");

          //---- OBJ_64 ----
          OBJ_64.setText("Num\u00e9ro du tarif \u00e0 \u00e9diter");
          OBJ_64.setName("OBJ_64");

          //---- NTAR ----
          NTAR.setComponentPopupMenu(BTD);
          NTAR.setName("NTAR");

          //---- OBJ_66 ----
          OBJ_66.setTitle("");
          OBJ_66.setName("OBJ_66");

          //---- OBJ_67 ----
          OBJ_67.setText("Libell\u00e9(s) \u00e0 \u00e9diter");
          OBJ_67.setName("OBJ_67");

          //---- LIB1 ----
          LIB1.setText("1");
          LIB1.setComponentPopupMenu(BTD);
          LIB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB1.setName("LIB1");

          //---- LIB2 ----
          LIB2.setText("2");
          LIB2.setComponentPopupMenu(BTD);
          LIB2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB2.setName("LIB2");

          //---- LIB3 ----
          LIB3.setText("3");
          LIB3.setComponentPopupMenu(BTD);
          LIB3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB3.setName("LIB3");

          //---- LIB4 ----
          LIB4.setText("4");
          LIB4.setComponentPopupMenu(BTD);
          LIB4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB4.setName("LIB4");

          //---- OBJ_72 ----
          OBJ_72.setTitle("");
          OBJ_72.setName("OBJ_72");

          //---- OBJ_73 ----
          OBJ_73.setText("Nombre d'\u00e9tiquettes (1,2,3 ou 4)");
          OBJ_73.setName("OBJ_73");

          //---- NBETQ ----
          NBETQ.setComponentPopupMenu(BTD);
          NBETQ.setName("NBETQ");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(94, 94, 94)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                .addGap(101, 101, 101)
                .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(NTAR, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                .addGap(109, 109, 109)
                .addComponent(LIB1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(LIB3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(LIB2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addComponent(LIB4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NBETQ, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(NTAR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(LIB1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(LIB3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(LIB2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(LIB4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(NBETQ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_44;
  private XRiCheckBox WTOU;
  private JPanel P_SEL0;
  private JLabel OBJ_55;
  private JLabel OBJ_50;
  private XRiTextField DEBIAF;
  private XRiTextField FINIAF;
  private RiZoneSortie FALIBD;
  private RiZoneSortie FALIBF;
  private XRiTextField DEBIAC;
  private XRiTextField DEBIAK;
  private XRiTextField DEBLO2;
  private XRiTextField FINIAC;
  private XRiTextField FINLO2;
  private XRiTextField FINIAK;
  private RiZoneSortie A1LIBD;
  private RiZoneSortie A1LIBF;
  private JXTitledSeparator OBJ_56;
  private JLabel OBJ_57;
  private XRiTextField WMAG;
  private JLabel OBJ_60;
  private JXTitledSeparator OBJ_59;
  private XRiTextField WCNV;
  private RiZoneSortie OBJ_62;
  private JXTitledSeparator OBJ_63;
  private JLabel OBJ_64;
  private XRiTextField NTAR;
  private JXTitledSeparator OBJ_66;
  private JLabel OBJ_67;
  private XRiCheckBox LIB1;
  private XRiCheckBox LIB2;
  private XRiCheckBox LIB3;
  private XRiCheckBox LIB4;
  private JXTitledSeparator OBJ_72;
  private JLabel OBJ_73;
  private XRiTextField NBETQ;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
