
package ri.serien.libecranrpg.sgvm.SGVM3GFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM3GFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] ETAA_Value = { "ATT", "FAC", };
  private String[] ETAV_Value = { "ATT", "FAC", };
  private String[] REGR_Value = { "", "B", "A", };
  
  public SGVM3GFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REGR.setValeurs(REGR_Value, null);
    ETAV.setValeurs(ETAV_Value, null);
    ETAA.setValeurs(ETAA_Value, null);
    OPT3.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT1.setValeursSelection("X", " ");
    GENV.setValeursSelection("OUI", "   ");
    GENA.setValeursSelection("OUI", "NON");
    EDT.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMAG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // OPT3.setVisible( lexique.isPresent("OPT3"));
    // OPT3.setSelected(lexique.HostFieldGetData("OPT3").equalsIgnoreCase("X"));
    // OPT2.setVisible( lexique.isPresent("OPT2"));
    // OPT2.setSelected(lexique.HostFieldGetData("OPT2").equalsIgnoreCase("X"));
    // OPT1.setVisible( lexique.isPresent("OPT1"));
    // OPT1.setSelected(lexique.HostFieldGetData("OPT1").equalsIgnoreCase("X"));
    // EDT.setVisible( lexique.isPresent("EDT"));
    // EDT.setSelected(lexique.HostFieldGetData("EDT").equalsIgnoreCase("OUI"));
    // GENA.setVisible( lexique.isPresent("GENA"));
    // GENA.setSelected(lexique.HostFieldGetData("GENA").equalsIgnoreCase("OUI"));
    // GENV.setVisible( lexique.isPresent("GENV"));
    // GENV.setSelected(lexique.HostFieldGetData("GENV").equalsIgnoreCase("OUI"));
    OBJ_45.setVisible(lexique.isPresent("LIBMAG"));
    // ETAA.setVisible( lexique.isPresent("ETAA"));
    // ETAA.setEnabled( lexique.isPresent("ETAA"));
    // ETAV.setVisible( lexique.isPresent("ETAV"));
    // ETAV.setEnabled( lexique.isPresent("ETAV"));
    // REGR.setVisible( lexique.isPresent("REGR"));
    // REGR.setEnabled( lexique.isPresent("REGR"));
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OPT3.isSelected())
    // lexique.HostFieldPutData("OPT3", 0, "X");
    // else
    // lexique.HostFieldPutData("OPT3", 0, " ");
    // if (OPT2.isSelected())
    // lexique.HostFieldPutData("OPT2", 0, "X");
    // else
    // lexique.HostFieldPutData("OPT2", 0, " ");
    // if (OPT1.isSelected())
    // lexique.HostFieldPutData("OPT1", 0, "X");
    // else
    // lexique.HostFieldPutData("OPT1", 0, " ");
    // if (EDT.isSelected())
    // lexique.HostFieldPutData("EDT", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EDT", 0, "NON");
    // if (GENA.isSelected())
    // lexique.HostFieldPutData("GENA", 0, "OUI");
    // else
    // lexique.HostFieldPutData("GENA", 0, "NON");
    // if (GENV.isSelected())
    // lexique.HostFieldPutData("GENV", 0, "OUI");
    // else
    // lexique.HostFieldPutData("GENV", 0, " ");
    // lexique.HostFieldPutData("ETAA", 0, ETAA_Value[ETAA.getSelectedIndex()]);
    // lexique.HostFieldPutData("ETAV", 0, ETAV_Value[ETAV.getSelectedIndex()]);
    // lexique.HostFieldPutData("REGR", 0, REGR_Value[REGR.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_25 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    OBJ_30 = new JXTitledSeparator();
    panel1 = new JPanel();
    DATDEB = new XRiCalendrier();
    OBJ_32 = new JLabel();
    OBJ_33 = new JLabel();
    DATFIN = new XRiCalendrier();
    panel2 = new JPanel();
    MAG = new XRiTextField();
    OBJ_45 = new RiZoneSortie();
    panel3 = new JPanel();
    EDT = new XRiCheckBox();
    panel5 = new JPanel();
    GENA = new XRiCheckBox();
    OBJ_47 = new JLabel();
    ETAA = new XRiComboBox();
    panel6 = new JPanel();
    GENV = new XRiCheckBox();
    OBJ_46 = new JLabel();
    ETAV = new XRiComboBox();
    REGR = new XRiComboBox();
    panel4 = new JPanel();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    OBJ_24 = new JXTitledSeparator();
    OBJ_48 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1110, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(910, 440));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_25 ----
          OBJ_25.setTitle("Options de traitement");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Magasin");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_30 ----
          OBJ_30.setTitle("P\u00e9riode \u00e0 traiter");
          OBJ_30.setName("OBJ_30");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(35, 10, 105, DATDEB.getPreferredSize().height);

            //---- OBJ_32 ----
            OBJ_32.setText("Du");
            OBJ_32.setName("OBJ_32");
            panel1.add(OBJ_32);
            OBJ_32.setBounds(5, 15, 30, 18);

            //---- OBJ_33 ----
            OBJ_33.setText("au");
            OBJ_33.setName("OBJ_33");
            panel1.add(OBJ_33);
            OBJ_33.setBounds(160, 15, 18, 18);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(185, 10, 105, DATFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(null);
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- MAG ----
            MAG.setComponentPopupMenu(BTD);
            MAG.setName("MAG");
            panel2.add(MAG);
            MAG.setBounds(10, 10, 34, MAG.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("@LIBMAG@");
            OBJ_45.setName("OBJ_45");
            panel2.add(OBJ_45);
            OBJ_45.setBounds(50, 12, 250, OBJ_45.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- EDT ----
            EDT.setText("Edition \u00e9tat de contr\u00f4le");
            EDT.setComponentPopupMenu(null);
            EDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDT.setName("EDT");
            panel3.add(EDT);
            EDT.setBounds(25, 5, 175, 20);

            //======== panel5 ========
            {
              panel5.setBorder(null);
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- GENA ----
              GENA.setText("G\u00e9n\u00e9ration facture d'achat");
              GENA.setComponentPopupMenu(null);
              GENA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              GENA.setName("GENA");
              panel5.add(GENA);
              GENA.setBounds(10, 10, 200, 20);

              //---- OBJ_47 ----
              OBJ_47.setText("Etat");
              OBJ_47.setName("OBJ_47");
              panel5.add(OBJ_47);
              OBJ_47.setBounds(215, 10, 35, 20);

              //---- ETAA ----
              ETAA.setModel(new DefaultComboBoxModel(new String[] {
                "En attente",
                "Factur\u00e9"
              }));
              ETAA.setComponentPopupMenu(BTD);
              ETAA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ETAA.setName("ETAA");
              panel5.add(ETAA);
              ETAA.setBounds(250, 7, 115, ETAA.getPreferredSize().height);
            }
            panel3.add(panel5);
            panel5.setBounds(15, 25, 385, 35);

            //======== panel6 ========
            {
              panel6.setBorder(null);
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- GENV ----
              GENV.setText("G\u00e9n\u00e9ration facture de cession");
              GENV.setComponentPopupMenu(null);
              GENV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              GENV.setName("GENV");
              panel6.add(GENV);
              GENV.setBounds(10, 10, 199, 20);

              //---- OBJ_46 ----
              OBJ_46.setText("Etat");
              OBJ_46.setName("OBJ_46");
              panel6.add(OBJ_46);
              OBJ_46.setBounds(215, 10, 30, 20);

              //---- ETAV ----
              ETAV.setModel(new DefaultComboBoxModel(new String[] {
                "En attente",
                "Factur\u00e9"
              }));
              ETAV.setComponentPopupMenu(BTD);
              ETAV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ETAV.setName("ETAV");
              panel6.add(ETAV);
              ETAV.setBounds(250, 7, 115, ETAV.getPreferredSize().height);
            }
            panel3.add(panel6);
            panel6.setBounds(15, 55, 385, 35);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //---- REGR ----
          REGR.setModel(new DefaultComboBoxModel(new String[] {
            "D\u00e9taill\u00e9",
            "Regroupement par bon",
            "Regroupement par article"
          }));
          REGR.setComponentPopupMenu(BTD);
          REGR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REGR.setName("REGR");

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OPT1 ----
            OPT1.setText("Au prix de vente");
            OPT1.setComponentPopupMenu(null);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel4.add(OPT1);
            OPT1.setBounds(5, 5, 157, 20);

            //---- OPT2 ----
            OPT2.setText("Au prix de revient");
            OPT2.setComponentPopupMenu(null);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel4.add(OPT2);
            OPT2.setBounds(5, 35, 157, 20);

            //---- OPT3 ----
            OPT3.setText("Au prix filiale");
            OPT3.setComponentPopupMenu(null);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setName("OPT3");
            panel4.add(OPT3);
            OPT3.setBounds(5, 65, 157, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_24 ----
          OBJ_24.setTitle("Chiffrage");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_48 ----
          OBJ_48.setTitle("Option de regroupement");
          OBJ_48.setName("OBJ_48");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REGR, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addGap(225, 225, 225)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(REGR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_30;
  private JPanel panel1;
  private XRiCalendrier DATDEB;
  private JLabel OBJ_32;
  private JLabel OBJ_33;
  private XRiCalendrier DATFIN;
  private JPanel panel2;
  private XRiTextField MAG;
  private RiZoneSortie OBJ_45;
  private JPanel panel3;
  private XRiCheckBox EDT;
  private JPanel panel5;
  private XRiCheckBox GENA;
  private JLabel OBJ_47;
  private XRiComboBox ETAA;
  private JPanel panel6;
  private XRiCheckBox GENV;
  private JLabel OBJ_46;
  private XRiComboBox ETAV;
  private XRiComboBox REGR;
  private JPanel panel4;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_48;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
