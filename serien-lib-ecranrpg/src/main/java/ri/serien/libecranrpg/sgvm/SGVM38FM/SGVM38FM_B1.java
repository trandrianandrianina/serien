
package ri.serien.libecranrpg.sgvm.SGVM38FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM38FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORT = "Exportation tableur";
  
  public SGVM38FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    
    // - Ajouter un bouton personnalisé
    snBarreBouton.ajouterBouton(BOUTON_EXPORT, 'e', true);
    // -
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Composant snPlageDate
    snPlageDatesBons.setDefilement(EnumDefilementPlageDate.SANS_DEFILEMENT);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    
    // -- Visible par défaut
    pnlSelectionPlageRepresentants.setVisible(!WTOU.isSelected());
    pnlSelectionPlageMagasins.setVisible(!WTOU2.isSelected());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    chargerComposantRepresentants();
    chargerComposantMagasins();
    
    // Charger composant snPlageDate
    snPlageDatesBons.setDateDebutParChampRPG(lexique, "DATDEB");
    snPlageDatesBons.setDateFinParChampRPG(lexique, "DATFIN");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Composant snPlageDate
    snPlageDatesBons.renseignerChampRPGDebut(lexique, "DATDEB");
    snPlageDatesBons.renseignerChampRPGFin(lexique, "DATFIN");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORT)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chargerComposantRepresentants() {
    snRepresentantDebut.setSession(getSession());
    snRepresentantDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantDebut.setTousAutorise(false);
    snRepresentantDebut.charger(false);
    snRepresentantDebut.setSelectionParChampRPG(lexique, "REPDEB");
    
    snRepresentantFin.setSession(getSession());
    snRepresentantFin.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantFin.setTousAutorise(false);
    snRepresentantFin.charger(false);
    snRepresentantFin.setSelectionParChampRPG(lexique, "REPFIN");
  }
  
  private void chargerComposantMagasins() {
    snMagasinDebut.setSession(getSession());
    snMagasinDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinDebut.setTousAutorise(false);
    snMagasinDebut.charger(false);
    snMagasinDebut.setSelectionParChampRPG(lexique, "MAGDEB");
    
    snMagasinFin.setSession(getSession());
    snMagasinFin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinFin.setTousAutorise(false);
    snMagasinFin.charger(false);
    snMagasinFin.setSelectionParChampRPG(lexique, "MAGFIN");
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    pnlSelectionPlageRepresentants.setVisible(!pnlSelectionPlageRepresentants.isVisible());
  }
  
  private void WTOU2ItemStateChanged(ItemEvent e) {
    pnlSelectionPlageMagasins.setVisible(!pnlSelectionPlageMagasins.isVisible());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantRepresentants();
    chargerComposantMagasins();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanel();
    pnlGauche = new SNPanel();
    pnlPlageDateBons = new SNPanelTitre();
    lbPlageBons = new SNLabelChamp();
    snPlageDatesBons = new SNPlageDate();
    pnlPlageRepresentants = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    pnlSelectionPlageRepresentants = new SNPanel();
    lbRepresentantDebut = new SNLabelChamp();
    snRepresentantDebut = new SNRepresentant();
    lbRepresentantFin = new SNLabelChamp();
    snRepresentantFin = new SNRepresentant();
    pnlPlageMagasins = new SNPanelTitre();
    WTOU2 = new XRiCheckBox();
    pnlSelectionPlageMagasins = new SNPanel();
    lbMagasinDebut = new SNLabelChamp();
    snMagasinDebut = new SNMagasin();
    lbMagasinFin = new SNLabelChamp();
    snMagasinFin = new SNMagasin();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlPlageDateBons ========
        {
          pnlPlageDateBons.setTitre("Plage de dates \u00e0 traiter");
          pnlPlageDateBons.setName("pnlPlageDateBons");
          pnlPlageDateBons.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageDateBons.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageDateBons.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPlageDateBons.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageDateBons.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbPlageBons ----
          lbPlageBons.setText("Edition des bons");
          lbPlageBons.setName("lbPlageBons");
          pnlPlageDateBons.add(lbPlageBons, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snPlageDatesBons ----
          snPlageDatesBons.setName("snPlageDatesBons");
          pnlPlageDateBons.add(snPlageDatesBons, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageDateBons, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlPlageRepresentants ========
        {
          pnlPlageRepresentants.setTitre("Plage des repr\u00e9sentants \u00e0 traiter");
          pnlPlageRepresentants.setName("pnlPlageRepresentants");
          pnlPlageRepresentants.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageRepresentants.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlPlageRepresentants.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageRepresentants.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageRepresentants.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMaximumSize(new Dimension(142, 30));
          WTOU.setMinimumSize(new Dimension(142, 30));
          WTOU.setPreferredSize(new Dimension(142, 30));
          WTOU.setName("WTOU");
          WTOU.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTOUItemStateChanged(e);
            }
          });
          pnlPlageRepresentants.add(WTOU, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionPlageRepresentants ========
          {
            pnlSelectionPlageRepresentants.setName("pnlSelectionPlageRepresentants");
            pnlSelectionPlageRepresentants.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionPlageRepresentants.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageRepresentants.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageRepresentants.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionPlageRepresentants.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbRepresentantDebut ----
            lbRepresentantDebut.setText("Premier repr\u00e9sentant");
            lbRepresentantDebut.setName("lbRepresentantDebut");
            pnlSelectionPlageRepresentants.add(lbRepresentantDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snRepresentantDebut ----
            snRepresentantDebut.setName("snRepresentantDebut");
            pnlSelectionPlageRepresentants.add(snRepresentantDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbRepresentantFin ----
            lbRepresentantFin.setText("Dernier repr\u00e9sentant");
            lbRepresentantFin.setName("lbRepresentantFin");
            pnlSelectionPlageRepresentants.add(lbRepresentantFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snRepresentantFin ----
            snRepresentantFin.setName("snRepresentantFin");
            pnlSelectionPlageRepresentants.add(snRepresentantFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageRepresentants.add(pnlSelectionPlageRepresentants, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageRepresentants, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlPlageMagasins ========
        {
          pnlPlageMagasins.setTitre("Plage des magasins \u00e0 traiter");
          pnlPlageMagasins.setName("pnlPlageMagasins");
          pnlPlageMagasins.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageMagasins.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlPlageMagasins.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageMagasins.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageMagasins.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU2.setMaximumSize(new Dimension(142, 30));
          WTOU2.setMinimumSize(new Dimension(142, 30));
          WTOU2.setPreferredSize(new Dimension(142, 30));
          WTOU2.setName("WTOU2");
          WTOU2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTOU2ItemStateChanged(e);
            }
          });
          pnlPlageMagasins.add(WTOU2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionPlageMagasins ========
          {
            pnlSelectionPlageMagasins.setName("pnlSelectionPlageMagasins");
            pnlSelectionPlageMagasins.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionPlageMagasins.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageMagasins.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageMagasins.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionPlageMagasins.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbMagasinDebut ----
            lbMagasinDebut.setText("Premier magasin");
            lbMagasinDebut.setName("lbMagasinDebut");
            pnlSelectionPlageMagasins.add(lbMagasinDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snMagasinDebut ----
            snMagasinDebut.setName("snMagasinDebut");
            pnlSelectionPlageMagasins.add(snMagasinDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbMagasinFin ----
            lbMagasinFin.setText("Dernier magasin");
            lbMagasinFin.setName("lbMagasinFin");
            pnlSelectionPlageMagasins.add(lbMagasinFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snMagasinFin ----
            snMagasinFin.setName("snMagasinFin");
            pnlSelectionPlageMagasins.add(snMagasinFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageMagasins.add(pnlSelectionPlageMagasins, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageMagasins, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setEditable(false);
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanel pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPlageDateBons;
  private SNLabelChamp lbPlageBons;
  private SNPlageDate snPlageDatesBons;
  private SNPanelTitre pnlPlageRepresentants;
  private XRiCheckBox WTOU;
  private SNPanel pnlSelectionPlageRepresentants;
  private SNLabelChamp lbRepresentantDebut;
  private SNRepresentant snRepresentantDebut;
  private SNLabelChamp lbRepresentantFin;
  private SNRepresentant snRepresentantFin;
  private SNPanelTitre pnlPlageMagasins;
  private XRiCheckBox WTOU2;
  private SNPanel pnlSelectionPlageMagasins;
  private SNLabelChamp lbMagasinDebut;
  private SNMagasin snMagasinDebut;
  private SNLabelChamp lbMagasinFin;
  private SNMagasin snMagasinFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
