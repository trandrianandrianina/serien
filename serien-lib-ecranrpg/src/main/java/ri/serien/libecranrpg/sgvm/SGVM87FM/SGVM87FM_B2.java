/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM87FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.EnumCategorieDate;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SGVM87FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private ArrayList<EnumCategorieDate> listeEnumCategorieDate = new ArrayList<EnumCategorieDate>();
  
  public SGVM87FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    TRTDIF.setValeursSelection("OUI", "NON");
    WCOM1.setValeursSelection("OUI", "NON");
    T29.setValeursSelection("OUI", "   ");
    TRT3.setValeursSelection("OUI", "NON");
    TRT2.setValeursSelection("OUI", "NON");
    TRT1.setValeursSelection("OUI", "NON");
    RAZTRT.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Chargement de la liste des valeurs de l'EnumDatePreEnregister
    for (int i = 0; i < EnumCategorieDate.values().length; i++) {
      listeEnumCategorieDate.add(EnumCategorieDate.values()[i]);
    }
    
    // Chargement de la comboBox avec les élements de la liste
    for (EnumCategorieDate categorieDate : listeEnumCategorieDate) {
      cbCategorieDate.addItem(categorieDate);
    }
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCorus.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    if (lexique.HostFieldGetData("WCOM2").equals("OUI")) {
      WCOM2.setSelected(true);
    }
    else if (lexique.HostFieldGetData("WCOM3").equals("OUI")) {
      WCOM3.setSelected(true);
    }
    else {
      rbTraiterTout.setSelected(true);
    }
    
    // // Sélection d'un choix par défaut
    if (lexique.HostFieldGetData("TRT1").equals("OUI")) {
      TRT1.setSelected(true);
    }
    else {
      TRT1.setSelected(false);
    }
    
    if (lexique.HostFieldGetData("TRT3").equals("OUI")) {
      TRT3.setSelected(true);
    }
    else {
      TRT3.setSelected(false);
    }
    
    // Disponibilité de certaine option
    if (TRT1.isSelected()) {
      TRT2.setEnabled(true);
    }
    else {
      TRT2.setEnabled(false);
      TRT2.setSelected(false);
    }
    if (TRT3.isSelected()) {
      T29.setEnabled(true);
    }
    else {
      T29.setEnabled(false);
      T29.setSelected(false);
    }
    
    // Recupere la saisi précedente
    cbCategorieDate.setSelectedItem(EnumCategorieDate.valueOfByCode(lexique.HostFieldGetData("WTDL").toString()));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    lexique.HostFieldPutData("WTDL", 0, ((EnumCategorieDate) cbCategorieDate.getSelectedItem()).getCode());
    
    if (WCOM2.isSelected()) {
      lexique.HostFieldPutData("WCOM2", 0, "OUI");
      lexique.HostFieldPutData("WCOM3", 0, "NON");
    }
    else if (WCOM3.isSelected()) {
      lexique.HostFieldPutData("WCOM2", 0, "NON");
      lexique.HostFieldPutData("WCOM3", 0, "OUI");
    }
    else {
      lexique.HostFieldPutData("WCOM2", 0, "NON");
      lexique.HostFieldPutData("WCOM3", 0, "NON");
    }
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void editionEtatPreparationActionPerformed(ActionEvent e) {
    if (TRT1.isSelected()) {
      TRT2.setEnabled(true);
    }
  }
  
  private void editionBonPreparationActionPerformed(ActionEvent e) {
    if (TRT3.isSelected()) {
      T29.setEnabled(true);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbDateDeLivraisonDu = new SNLabelChamp();
    pnlDateLivraison = new SNPanel();
    WDATD = new XRiCalendrier();
    lbDateDeLivraisonAu = new SNLabelChamp();
    WDATF = new XRiCalendrier();
    lbCategorieDateLivraison = new SNLabelChamp();
    cbCategorieDate = new SNComboBox();
    lbCodeDeSelection = new SNLabelChamp();
    pnlSelectionDeBon = new SNPanel();
    WCODD = new XRiTextField();
    lbA = new SNLabelChamp();
    WCODF = new XRiTextField();
    lbNumeroDeCommandeDe = new SNLabelChamp();
    pnlNumeroDeCommande = new SNPanel();
    WNUMD = new XRiTextField();
    lbANumeroDeCommande = new SNLabelChamp();
    WNUMF = new XRiTextField();
    pnlTraitement = new SNPanelTitre();
    rbTraiterTout = new JRadioButton();
    WCOM2 = new JRadioButton();
    WCOM3 = new JRadioButton();
    WCOM1 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCorus = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    TRT1 = new XRiCheckBox();
    TRT2 = new XRiCheckBox();
    TRT3 = new XRiCheckBox();
    T29 = new XRiCheckBox();
    pnlOptionTraitement = new SNPanelTitre();
    TRTDIF = new XRiCheckBox();
    RAZTRT = new XRiCheckBox();
    pnlNombreCommandeATraiter = new SNPanel();
    lbNombreMaxDeCommande = new SNLabelChamp();
    MAXCMD = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    btgTriCommande = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(960, 640));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("LOCTP");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateDeLivraisonDu ----
            lbDateDeLivraisonDu.setText("Date de livraison du");
            lbDateDeLivraisonDu.setMaximumSize(new Dimension(170, 30));
            lbDateDeLivraisonDu.setMinimumSize(new Dimension(170, 30));
            lbDateDeLivraisonDu.setPreferredSize(new Dimension(170, 30));
            lbDateDeLivraisonDu.setName("lbDateDeLivraisonDu");
            pnlCritereDeSelection.add(lbDateDeLivraisonDu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateLivraison ========
            {
              pnlDateLivraison.setName("pnlDateLivraison");
              pnlDateLivraison.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateLivraison.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDATD ----
              WDATD.setPreferredSize(new Dimension(110, 29));
              WDATD.setMinimumSize(new Dimension(110, 30));
              WDATD.setMaximumSize(new Dimension(110, 30));
              WDATD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATD.setRequestFocusEnabled(false);
              WDATD.setName("WDATD");
              pnlDateLivraison.add(WDATD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDateDeLivraisonAu ----
              lbDateDeLivraisonAu.setText("au");
              lbDateDeLivraisonAu.setPreferredSize(new Dimension(16, 30));
              lbDateDeLivraisonAu.setMinimumSize(new Dimension(16, 30));
              lbDateDeLivraisonAu.setMaximumSize(new Dimension(16, 30));
              lbDateDeLivraisonAu.setName("lbDateDeLivraisonAu");
              pnlDateLivraison.add(lbDateDeLivraisonAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDATF ----
              WDATF.setPreferredSize(new Dimension(110, 30));
              WDATF.setMinimumSize(new Dimension(110, 30));
              WDATF.setMaximumSize(new Dimension(110, 30));
              WDATF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATF.setName("WDATF");
              pnlDateLivraison.add(WDATF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlDateLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieDateLivraison ----
            lbCategorieDateLivraison.setText("Cat\u00e9gorie date de livraison");
            lbCategorieDateLivraison.setPreferredSize(new Dimension(170, 30));
            lbCategorieDateLivraison.setMinimumSize(new Dimension(170, 30));
            lbCategorieDateLivraison.setMaximumSize(new Dimension(170, 30));
            lbCategorieDateLivraison.setName("lbCategorieDateLivraison");
            pnlCritereDeSelection.add(lbCategorieDateLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbCategorieDate ----
            cbCategorieDate.setEnabled(false);
            cbCategorieDate.setName("cbCategorieDate");
            pnlCritereDeSelection.add(cbCategorieDate, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeDeSelection ----
            lbCodeDeSelection.setText("S\u00e9lection de");
            lbCodeDeSelection.setMaximumSize(new Dimension(170, 30));
            lbCodeDeSelection.setMinimumSize(new Dimension(170, 30));
            lbCodeDeSelection.setPreferredSize(new Dimension(170, 30));
            lbCodeDeSelection.setName("lbCodeDeSelection");
            pnlCritereDeSelection.add(lbCodeDeSelection, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlSelectionDeBon ========
            {
              pnlSelectionDeBon.setName("pnlSelectionDeBon");
              pnlSelectionDeBon.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WCODD ----
              WCODD.setComponentPopupMenu(null);
              WCODD.setPreferredSize(new Dimension(24, 30));
              WCODD.setMinimumSize(new Dimension(24, 30));
              WCODD.setMaximumSize(new Dimension(24, 30));
              WCODD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WCODD.setName("WCODD");
              pnlSelectionDeBon.add(WCODD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbA ----
              lbA.setText("\u00e0");
              lbA.setPreferredSize(new Dimension(8, 30));
              lbA.setMinimumSize(new Dimension(8, 30));
              lbA.setMaximumSize(new Dimension(8, 30));
              lbA.setName("lbA");
              pnlSelectionDeBon.add(lbA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WCODF ----
              WCODF.setComponentPopupMenu(null);
              WCODF.setMaximumSize(new Dimension(24, 30));
              WCODF.setMinimumSize(new Dimension(24, 30));
              WCODF.setPreferredSize(new Dimension(24, 30));
              WCODF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WCODF.setName("WCODF");
              pnlSelectionDeBon.add(WCODF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlSelectionDeBon, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNumeroDeCommandeDe ----
            lbNumeroDeCommandeDe.setText("Commande \u00e0 traiter de");
            lbNumeroDeCommandeDe.setPreferredSize(new Dimension(170, 30));
            lbNumeroDeCommandeDe.setMinimumSize(new Dimension(170, 30));
            lbNumeroDeCommandeDe.setMaximumSize(new Dimension(170, 30));
            lbNumeroDeCommandeDe.setName("lbNumeroDeCommandeDe");
            pnlCritereDeSelection.add(lbNumeroDeCommandeDe, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlNumeroDeCommande ========
            {
              pnlNumeroDeCommande.setName("pnlNumeroDeCommande");
              pnlNumeroDeCommande.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WNUMD ----
              WNUMD.setComponentPopupMenu(null);
              WNUMD.setPreferredSize(new Dimension(70, 30));
              WNUMD.setMinimumSize(new Dimension(70, 30));
              WNUMD.setMaximumSize(new Dimension(70, 30));
              WNUMD.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNUMD.setName("WNUMD");
              pnlNumeroDeCommande.add(WNUMD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbANumeroDeCommande ----
              lbANumeroDeCommande.setText("\u00e0");
              lbANumeroDeCommande.setMaximumSize(new Dimension(8, 30));
              lbANumeroDeCommande.setMinimumSize(new Dimension(8, 30));
              lbANumeroDeCommande.setPreferredSize(new Dimension(8, 30));
              lbANumeroDeCommande.setName("lbANumeroDeCommande");
              pnlNumeroDeCommande.add(lbANumeroDeCommande, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WNUMF ----
              WNUMF.setComponentPopupMenu(null);
              WNUMF.setMaximumSize(new Dimension(70, 30));
              WNUMF.setMinimumSize(new Dimension(70, 30));
              WNUMF.setPreferredSize(new Dimension(70, 30));
              WNUMF.setFont(new Font("sansserif", Font.PLAIN, 14));
              WNUMF.setName("WNUMF");
              pnlNumeroDeCommande.add(WNUMF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlNumeroDeCommande, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTraitement ========
          {
            pnlTraitement.setTitre("Traitement");
            pnlTraitement.setName("pnlTraitement");
            pnlTraitement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTraitement.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTraitement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTraitement.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTraitement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- rbTraiterTout ----
            rbTraiterTout.setText("Tout traiter");
            rbTraiterTout.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbTraiterTout.setPreferredSize(new Dimension(325, 30));
            rbTraiterTout.setMinimumSize(new Dimension(325, 30));
            rbTraiterTout.setMaximumSize(new Dimension(325, 30));
            rbTraiterTout.setName("rbTraiterTout");
            pnlTraitement.add(rbTraiterTout, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WCOM2 ----
            WCOM2.setText("Traiter seulement les commandes compl\u00e8tes");
            WCOM2.setComponentPopupMenu(null);
            WCOM2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WCOM2.setFont(new Font("sansserif", Font.PLAIN, 14));
            WCOM2.setPreferredSize(new Dimension(325, 30));
            WCOM2.setMinimumSize(new Dimension(325, 30));
            WCOM2.setMaximumSize(new Dimension(325, 30));
            WCOM2.setName("WCOM2");
            pnlTraitement.add(WCOM2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WCOM3 ----
            WCOM3.setText("Traiter seulement les reliquats");
            WCOM3.setComponentPopupMenu(null);
            WCOM3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WCOM3.setFont(new Font("sansserif", Font.PLAIN, 14));
            WCOM3.setMaximumSize(new Dimension(325, 30));
            WCOM3.setMinimumSize(new Dimension(325, 30));
            WCOM3.setPreferredSize(new Dimension(325, 30));
            WCOM3.setName("WCOM3");
            pnlTraitement.add(WCOM3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WCOM1 ----
            WCOM1.setText("Livraison partielle interdite");
            WCOM1.setComponentPopupMenu(null);
            WCOM1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WCOM1.setFont(new Font("sansserif", Font.PLAIN, 14));
            WCOM1.setPreferredSize(new Dimension(188, 30));
            WCOM1.setMinimumSize(new Dimension(188, 30));
            WCOM1.setMaximumSize(new Dimension(188, 30));
            WCOM1.setName("WCOM1");
            pnlTraitement.add(WCOM1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCorus ----
            tfPeriodeEnCorus.setText("@WENCX@");
            tfPeriodeEnCorus.setEditable(false);
            tfPeriodeEnCorus.setEnabled(false);
            tfPeriodeEnCorus.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCorus.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCorus.setName("tfPeriodeEnCorus");
            pnlEtablissement.add(tfPeriodeEnCorus, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Options d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 55, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- TRT1 ----
            TRT1.setText("Etat de pr\u00e9paration");
            TRT1.setComponentPopupMenu(null);
            TRT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            TRT1.setPreferredSize(new Dimension(227, 30));
            TRT1.setMinimumSize(new Dimension(227, 30));
            TRT1.setMaximumSize(new Dimension(227, 30));
            TRT1.setName("TRT1");
            TRT1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                editionEtatPreparationActionPerformed(e);
              }
            });
            pnlOptionEdition.add(TRT1, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TRT2 ----
            TRT2.setText("Chiffr\u00e9");
            TRT2.setComponentPopupMenu(null);
            TRT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            TRT2.setPreferredSize(new Dimension(200, 30));
            TRT2.setMinimumSize(new Dimension(200, 30));
            TRT2.setMaximumSize(new Dimension(200, 30));
            TRT2.setName("TRT2");
            pnlOptionEdition.add(TRT2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TRT3 ----
            TRT3.setText("Bons de pr\u00e9paration");
            TRT3.setComponentPopupMenu(null);
            TRT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            TRT3.setPreferredSize(new Dimension(227, 30));
            TRT3.setMinimumSize(new Dimension(227, 30));
            TRT3.setMaximumSize(new Dimension(227, 30));
            TRT3.setName("TRT3");
            TRT3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                editionBonPreparationActionPerformed(e);
              }
            });
            pnlOptionEdition.add(TRT3, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- T29 ----
            T29.setText("Regroup\u00e9");
            T29.setComponentPopupMenu(null);
            T29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T29.setFont(new Font("sansserif", Font.PLAIN, 14));
            T29.setMaximumSize(new Dimension(200, 30));
            T29.setMinimumSize(new Dimension(200, 30));
            T29.setPreferredSize(new Dimension(200, 30));
            T29.setName("T29");
            pnlOptionEdition.add(T29, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlOptionTraitement ========
          {
            pnlOptionTraitement.setTitre("Option de traitement");
            pnlOptionTraitement.setName("pnlOptionTraitement");
            pnlOptionTraitement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionTraitement.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- TRTDIF ----
            TRTDIF.setText("Traitement diff\u00e9r\u00e9");
            TRTDIF.setComponentPopupMenu(null);
            TRTDIF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRTDIF.setFont(new Font("sansserif", Font.PLAIN, 14));
            TRTDIF.setPreferredSize(new Dimension(227, 30));
            TRTDIF.setMinimumSize(new Dimension(227, 30));
            TRTDIF.setMaximumSize(new Dimension(227, 30));
            TRTDIF.setName("TRTDIF");
            pnlOptionTraitement.add(TRTDIF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RAZTRT ----
            RAZTRT.setText("Effacement des traitements pr\u00e9c\u00e9dents");
            RAZTRT.setComponentPopupMenu(null);
            RAZTRT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RAZTRT.setFont(new Font("sansserif", Font.PLAIN, 14));
            RAZTRT.setMinimumSize(new Dimension(271, 30));
            RAZTRT.setMaximumSize(new Dimension(271, 30));
            RAZTRT.setPreferredSize(new Dimension(271, 30));
            RAZTRT.setName("RAZTRT");
            pnlOptionTraitement.add(RAZTRT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlNombreCommandeATraiter ========
            {
              pnlNombreCommandeATraiter.setName("pnlNombreCommandeATraiter");
              pnlNombreCommandeATraiter.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNombreCommandeATraiter.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlNombreCommandeATraiter.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNombreCommandeATraiter.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlNombreCommandeATraiter.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbNombreMaxDeCommande ----
              lbNombreMaxDeCommande.setText("Nombre maximal de commande \u00e0 traiter");
              lbNombreMaxDeCommande.setMinimumSize(new Dimension(300, 30));
              lbNombreMaxDeCommande.setMaximumSize(new Dimension(300, 30));
              lbNombreMaxDeCommande.setPreferredSize(new Dimension(300, 30));
              lbNombreMaxDeCommande.setName("lbNombreMaxDeCommande");
              pnlNombreCommandeATraiter.add(lbNombreMaxDeCommande, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAXCMD ----
              MAXCMD.setComponentPopupMenu(null);
              MAXCMD.setFont(new Font("sansserif", Font.PLAIN, 14));
              MAXCMD.setMaximumSize(new Dimension(40, 30));
              MAXCMD.setMinimumSize(new Dimension(40, 30));
              MAXCMD.setPreferredSize(new Dimension(40, 30));
              MAXCMD.setName("MAXCMD");
              pnlNombreCommandeATraiter.add(MAXCMD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOptionTraitement.add(pnlNombreCommandeATraiter, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionTraitement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    
    // ---- btgTriCommande ----
    btgTriCommande.add(rbTraiterTout);
    btgTriCommande.add(WCOM2);
    btgTriCommande.add(WCOM3);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbDateDeLivraisonDu;
  private SNPanel pnlDateLivraison;
  private XRiCalendrier WDATD;
  private SNLabelChamp lbDateDeLivraisonAu;
  private XRiCalendrier WDATF;
  private SNLabelChamp lbCategorieDateLivraison;
  private SNComboBox cbCategorieDate;
  private SNLabelChamp lbCodeDeSelection;
  private SNPanel pnlSelectionDeBon;
  private XRiTextField WCODD;
  private SNLabelChamp lbA;
  private XRiTextField WCODF;
  private SNLabelChamp lbNumeroDeCommandeDe;
  private SNPanel pnlNumeroDeCommande;
  private XRiTextField WNUMD;
  private SNLabelChamp lbANumeroDeCommande;
  private XRiTextField WNUMF;
  private SNPanelTitre pnlTraitement;
  private JRadioButton rbTraiterTout;
  private JRadioButton WCOM2;
  private JRadioButton WCOM3;
  private XRiCheckBox WCOM1;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCorus;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox TRT1;
  private XRiCheckBox TRT2;
  private XRiCheckBox TRT3;
  private XRiCheckBox T29;
  private SNPanelTitre pnlOptionTraitement;
  private XRiCheckBox TRTDIF;
  private XRiCheckBox RAZTRT;
  private SNPanel pnlNombreCommandeATraiter;
  private SNLabelChamp lbNombreMaxDeCommande;
  private XRiTextField MAXCMD;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  private ButtonGroup btgTriCommande;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
