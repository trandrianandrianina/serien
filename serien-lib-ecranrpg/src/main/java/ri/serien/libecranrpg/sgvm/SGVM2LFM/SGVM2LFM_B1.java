
package ri.serien.libecranrpg.sgvm.SGVM2LFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM2LFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM2LFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // A VOIR
    snPlageDates.setDefilement(EnumDefilementPlageDate.SANS_DEFILEMENT);
    
    // Ajout
    WTDAT.setValeursSelection("**", "  ");
    WTCLI.setValeursSelection("**", "  ");
    
    // Initialisation Combobox
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    pnlSelectionPlageDates.setVisible(!WTDAT.isSelected());
    pnlSelectionPlageClients.setVisible(!WTCLI.isSelected());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation de la plage de dates
    snPlageDates.setDateDebutParChampRPG(lexique, "DATDEB");
    snPlageDates.setDateFinParChampRPG(lexique, "DATFIN");
    
    chargerComposantPremierClient();
    chargerComposantDernierClient();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snPremierClient.renseignerChampRPG(lexique, "DEBCLI");
    snDernierClient.renseignerChampRPG(lexique, "FINCLI");
    snPlageDates.renseignerChampRPGDebut(lexique, "DATDEB");
    snPlageDates.renseignerChampRPGFin(lexique, "DATFIN");
    
  }
  
  private void chargerComposantPremierClient() {
    snPremierClient.setSession(getSession());
    snPremierClient.setIdEtablissement(snEtablissement.getIdSelection());
    snPremierClient.charger(false);
    snPremierClient.setSelectionParChampRPG(lexique, "DEBCLI");
  }
  
  private void chargerComposantDernierClient() {
    snDernierClient.setSession(getSession());
    snDernierClient.setIdEtablissement(snEtablissement.getIdSelection());
    snDernierClient.charger(false);
    snDernierClient.setSelectionParChampRPG(lexique, "FINCLI");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    if (pSNBouton.isBouton(EnumBouton.EDITER)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
      lexique.HostScreenSendKey(this, "F3");
    }
  }
  
  // Modification de la Visibilité des sélections de plage en fonction de la sélection complète
  private void WTCLIItemStateChanged(ItemEvent e) {
    pnlSelectionPlageClients.setVisible(!pnlSelectionPlageClients.isVisible());
  }
  
  private void WTDATItemStateChanged(ItemEvent e) {
    pnlSelectionPlageDates.setVisible(!pnlSelectionPlageDates.isVisible());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantPremierClient();
      chargerComposantDernierClient();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlPlageDates = new SNPanelTitre();
    WTDAT = new XRiCheckBox();
    pnlSelectionPlageDates = new SNPanel();
    tfDateDebut = new SNLabelChamp();
    snPlageDates = new SNPlageDate();
    pnlPlageClients = new SNPanelTitre();
    WTCLI = new XRiCheckBox();
    pnlSelectionPlageClients = new SNPanel();
    lbPremierClient = new SNLabelChamp();
    snPremierClient = new SNClientPrincipal();
    lbDernierClient = new SNLabelChamp();
    snDernierClient = new SNClientPrincipal();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionTypeLettre = new SNPanelTitre();
    lbTypeDeLettre = new SNLabelChamp();
    TLET = new XRiComboBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(980, 580));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout(1, 2));

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlPlageDates ========
        {
          pnlPlageDates.setForeground(Color.black);
          pnlPlageDates.setTitre("Plage des dates \u00e0 traiter");
          pnlPlageDates.setName("pnlPlageDates");
          pnlPlageDates.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageDates.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlPlageDates.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageDates.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageDates.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTDAT ----
          WTDAT.setText("S\u00e9lection compl\u00e8te");
          WTDAT.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTDAT.setMaximumSize(new Dimension(45, 30));
          WTDAT.setMinimumSize(new Dimension(45, 30));
          WTDAT.setPreferredSize(new Dimension(45, 30));
          WTDAT.setName("WTDAT");
          WTDAT.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTDATItemStateChanged(e);
            }
          });
          pnlPlageDates.add(WTDAT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionPlageDates ========
          {
            pnlSelectionPlageDates.setName("pnlSelectionPlageDates");
            pnlSelectionPlageDates.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionPlageDates.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageDates.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlSelectionPlageDates.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionPlageDates.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- tfDateDebut ----
            tfDateDebut.setText("Plage de factures");
            tfDateDebut.setName("tfDateDebut");
            pnlSelectionPlageDates.add(tfDateDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snPlageDates ----
            snPlageDates.setName("snPlageDates");
            pnlSelectionPlageDates.add(snPlageDates, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageDates.add(pnlSelectionPlageDates, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageDates, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlPlageClients ========
        {
          pnlPlageClients.setForeground(Color.black);
          pnlPlageClients.setTitre("Plage des clients \u00e0 traiter");
          pnlPlageClients.setName("pnlPlageClients");
          pnlPlageClients.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageClients.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlPlageClients.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageClients.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageClients.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTCLI ----
          WTCLI.setText("S\u00e9lection compl\u00e8te");
          WTCLI.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTCLI.setMaximumSize(new Dimension(45, 30));
          WTCLI.setMinimumSize(new Dimension(45, 30));
          WTCLI.setPreferredSize(new Dimension(45, 30));
          WTCLI.setName("WTCLI");
          WTCLI.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTCLIItemStateChanged(e);
            }
          });
          pnlPlageClients.add(WTCLI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionPlageClients ========
          {
            pnlSelectionPlageClients.setName("pnlSelectionPlageClients");
            pnlSelectionPlageClients.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbPremierClient ----
            lbPremierClient.setText("Premier client");
            lbPremierClient.setName("lbPremierClient");
            pnlSelectionPlageClients.add(lbPremierClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snPremierClient ----
            snPremierClient.setName("snPremierClient");
            pnlSelectionPlageClients.add(snPremierClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbDernierClient ----
            lbDernierClient.setText("Dernier client");
            lbDernierClient.setName("lbDernierClient");
            pnlSelectionPlageClients.add(lbDernierClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snDernierClient ----
            snDernierClient.setName("snDernierClient");
            pnlSelectionPlageClients.add(snDernierClient, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageClients.add(pnlSelectionPlageClients, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageClients, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setEditable(false);
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptionTypeLettre ========
        {
          pnlOptionTypeLettre.setTitre("Option");
          pnlOptionTypeLettre.setName("pnlOptionTypeLettre");
          pnlOptionTypeLettre.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptionTypeLettre.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlOptionTypeLettre.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlOptionTypeLettre.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlOptionTypeLettre.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbTypeDeLettre ----
          lbTypeDeLettre.setText("Type de lettre");
          lbTypeDeLettre.setName("lbTypeDeLettre");
          pnlOptionTypeLettre.add(lbTypeDeLettre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- TLET ----
          TLET.setMinimumSize(new Dimension(50, 30));
          TLET.setPreferredSize(new Dimension(50, 30));
          TLET.setFont(new Font("sansserif", Font.PLAIN, 14));
          TLET.setMaximumSize(new Dimension(50, 30));
          TLET.setModel(new DefaultComboBoxModel(new String[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
          }));
          TLET.setName("TLET");
          pnlOptionTypeLettre.add(TLET, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptionTypeLettre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPlageDates;
  private XRiCheckBox WTDAT;
  private SNPanel pnlSelectionPlageDates;
  private SNLabelChamp tfDateDebut;
  private SNPlageDate snPlageDates;
  private SNPanelTitre pnlPlageClients;
  private XRiCheckBox WTCLI;
  private SNPanel pnlSelectionPlageClients;
  private SNLabelChamp lbPremierClient;
  private SNClientPrincipal snPremierClient;
  private SNLabelChamp lbDernierClient;
  private SNClientPrincipal snDernierClient;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionTypeLettre;
  private SNLabelChamp lbTypeDeLettre;
  private XRiComboBox TLET;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
