
package ri.serien.libecranrpg.sgvm.SGVM63FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM63FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM63FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EANA3.setValeurs("3", "R3");
    EANA2.setValeurs("2", "R3");
    EANA1.setValeurs("1", "R3");
    ESTA2.setValeurs("2", "R2");
    ESTA1.setValeurs("1", "R2");
    EOPT2.setValeurs("2", "R1");
    EOPT1.setValeurs("1", "R1");
    WTOU.setValeursSelection("**", "  ");
    EREP.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    EANA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SANA1@")).trim());
    EANA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SANA2@")).trim());
    EANA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SANA3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    // EFIND.setVisible( lexique.isPresent("EFIND"));
    // EDEBD.setVisible( lexique.isPresent("EDEBD"));
    EREP_doublon_44.setVisible(lexique.isPresent("EREP"));
    
    // WTOU.setVisible( lexique.isPresent("WTOU"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // EREP.setVisible( lexique.isPresent("EREP"));
    // EREP.setSelected(lexique.HostFieldGetData("EREP").equalsIgnoreCase("**"));
    // EANA3.setVisible( lexique.isPresent("SANA3"));
    // EANA3.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("3"));
    // EANA2.setVisible( lexique.isPresent("SANA2"));
    // EANA2.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("2"));
    // EANA1.setVisible( lexique.isPresent("SANA1"));
    // EANA1.setSelected(lexique.HostFieldGetData("R3").equalsIgnoreCase("1"));
    // ESTA2.setVisible( lexique.isPresent("ESTA2"));
    // ESTA2.setSelected(lexique.HostFieldGetData("R2").equalsIgnoreCase("2"));
    // ESTA1.setSelected(lexique.HostFieldGetData("R2").equalsIgnoreCase("1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    // EOPT2.setVisible( lexique.isPresent("EOPT2"));
    // EOPT2.setSelected(lexique.HostFieldGetData("R1").equalsIgnoreCase("2"));
    // EOPT1.setVisible( lexique.isPresent("EOPT1"));
    // EOPT1.setSelected(lexique.HostFieldGetData("R1").equalsIgnoreCase("1"));
    
    P_SEL0.setVisible(!lexique.HostFieldGetData("EREP").trim().equalsIgnoreCase("**"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    OBJ_25.setVisible(lexique.isPresent("EOPT1"));
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (EREP.isSelected())
    // lexique.HostFieldPutData("EREP", 0, "**");
    // else
    // lexique.HostFieldPutData("EREP", 0, " ");
    // if (EANA3.isSelected())
    // lexique.HostFieldPutData("R3", 0, "3");
    // if (EANA2.isSelected())
    // lexique.HostFieldPutData("R3", 0, "2");
    // if (EANA1.isSelected())
    // lexique.HostFieldPutData("R3", 0, "1");
    // if (ESTA2.isSelected())
    // lexique.HostFieldPutData("R2", 0, "2");
    // if (ESTA1.isSelected())
    // lexique.HostFieldPutData("R2", 0, "1");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (EOPT2.isSelected())
    // lexique.HostFieldPutData("R1", 0, "2");
    // if (EOPT1.isSelected())
    // lexique.HostFieldPutData("R1", 0, "1");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvm63"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void EREPActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_20 = new JXTitledSeparator();
    OBJ_19 = new JXTitledSeparator();
    OBJ_23 = new JXTitledSeparator();
    OBJ_25 = new JXTitledSeparator();
    OBJ_37 = new JXTitledSeparator();
    OBJ_32 = new JXTitledSeparator();
    P_SEL1 = new JPanel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    EDEBD = new XRiTextField();
    EFIND = new XRiTextField();
    P_SEL0 = new JPanel();
    OBJ_43 = new JLabel();
    EREP_doublon_44 = new XRiTextField();
    EOPT1 = new XRiRadioButton();
    EOPT2 = new XRiRadioButton();
    REPON1 = new XRiCheckBox();
    ESTA1 = new XRiRadioButton();
    ESTA2 = new XRiRadioButton();
    EREP = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    ECAINF = new XRiTextField();
    ECASUP = new XRiTextField();
    OBJ_39 = new JLabel();
    OBJ_38 = new JLabel();
    panel1 = new JPanel();
    EANA1 = new XRiRadioButton();
    EANA2 = new XRiRadioButton();
    EANA3 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(795, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_20 ----
          OBJ_20.setTitle("Limites / CA");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_19 ----
          OBJ_19.setTitle("Options");
          OBJ_19.setName("OBJ_19");

          //---- OBJ_23 ----
          OBJ_23.setTitle("Plage d\u00e9partements");
          OBJ_23.setName("OBJ_23");

          //---- OBJ_25 ----
          OBJ_25.setTitle("Option d\u00e9sir\u00e9e");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_37 ----
          OBJ_37.setTitle("P\u00e9riode \u00e0 analyser");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_32 ----
          OBJ_32.setTitle("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_32.setName("OBJ_32");

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_30 ----
            OBJ_30.setText("Code d\u00e9but");
            OBJ_30.setName("OBJ_30");
            P_SEL1.add(OBJ_30);
            OBJ_30.setBounds(45, 12, 75, 18);

            //---- OBJ_31 ----
            OBJ_31.setText("Code fin");
            OBJ_31.setName("OBJ_31");
            P_SEL1.add(OBJ_31);
            OBJ_31.setBounds(45, 41, 75, 18);

            //---- EDEBD ----
            EDEBD.setComponentPopupMenu(BTD);
            EDEBD.setName("EDEBD");
            P_SEL1.add(EDEBD);
            EDEBD.setBounds(181, 7, 34, EDEBD.getPreferredSize().height);

            //---- EFIND ----
            EFIND.setComponentPopupMenu(BTD);
            EFIND.setName("EFIND");
            P_SEL1.add(EFIND);
            EFIND.setBounds(181, 36, 34, EFIND.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL1.setMinimumSize(preferredSize);
              P_SEL1.setPreferredSize(preferredSize);
            }
          }

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");

            //---- OBJ_43 ----
            OBJ_43.setText("Code repr\u00e9sentant");
            OBJ_43.setName("OBJ_43");

            //---- EREP_doublon_44 ----
            EREP_doublon_44.setComponentPopupMenu(BTD);
            EREP_doublon_44.setName("EREP_doublon_44");

            GroupLayout P_SEL0Layout = new GroupLayout(P_SEL0);
            P_SEL0.setLayout(P_SEL0Layout);
            P_SEL0Layout.setHorizontalGroup(
              P_SEL0Layout.createParallelGroup()
                .addGroup(P_SEL0Layout.createSequentialGroup()
                  .addGap(43, 43, 43)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                  .addGap(22, 22, 22)
                  .addComponent(EREP_doublon_44, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
            );
            P_SEL0Layout.setVerticalGroup(
              P_SEL0Layout.createParallelGroup()
                .addGroup(P_SEL0Layout.createSequentialGroup()
                  .addGap(17, 17, 17)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_SEL0Layout.createSequentialGroup()
                  .addGap(12, 12, 12)
                  .addComponent(EREP_doublon_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //---- EOPT1 ----
          EOPT1.setText("Statistiques sur le command\u00e9");
          EOPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EOPT1.setName("EOPT1");

          //---- EOPT2 ----
          EOPT2.setText("Statistiques sur le factur\u00e9");
          EOPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EOPT2.setName("EOPT2");

          //---- REPON1 ----
          REPON1.setText("Etat r\u00e9duit (1 ligne par client)");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- ESTA1 ----
          ESTA1.setText("Statistiques mois en cours");
          ESTA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESTA1.setName("ESTA1");

          //---- ESTA2 ----
          ESTA2.setText("Statistiques mois suivant");
          ESTA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESTA2.setName("ESTA2");

          //---- EREP ----
          EREP.setText("S\u00e9lection compl\u00e8te");
          EREP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EREP.setName("EREP");
          EREP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              EREPActionPerformed(e);
            }
          });

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- ECAINF ----
          ECAINF.setComponentPopupMenu(BTD);
          ECAINF.setName("ECAINF");

          //---- ECASUP ----
          ECASUP.setComponentPopupMenu(BTD);
          ECASUP.setName("ECASUP");

          //---- OBJ_39 ----
          OBJ_39.setText("Sup\u00e9rieure");
          OBJ_39.setName("OBJ_39");

          //---- OBJ_38 ----
          OBJ_38.setText("Inf\u00e9rieure");
          OBJ_38.setName("OBJ_38");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- EANA1 ----
            EANA1.setText("@SANA1@");
            EANA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EANA1.setName("EANA1");

            //---- EANA2 ----
            EANA2.setText("@SANA2@");
            EANA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EANA2.setName("EANA2");

            //---- EANA3 ----
            EANA3.setText("@SANA3@");
            EANA3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EANA3.setName("EANA3");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(EANA1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EANA2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EANA3, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(EANA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(EANA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(EANA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 725, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 435, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ESTA1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addGap(135, 135, 135)
                .addComponent(EOPT1, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ESTA2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addGap(135, 135, 135)
                .addComponent(EOPT2, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 725, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addComponent(ECAINF, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                  .addComponent(ECASUP, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
                .addGap(66, 66, 66)
                .addComponent(EREP, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 435, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                .addGap(95, 95, 95)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 725, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(ESTA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EOPT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(ESTA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EOPT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(ECAINF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(ECASUP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(EREP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- R1_GRP ----
    ButtonGroup R1_GRP = new ButtonGroup();
    R1_GRP.add(EOPT1);
    R1_GRP.add(EOPT2);

    //---- R2_GRP ----
    ButtonGroup R2_GRP = new ButtonGroup();
    R2_GRP.add(ESTA1);
    R2_GRP.add(ESTA2);

    //---- R3_GRP ----
    ButtonGroup R3_GRP = new ButtonGroup();
    R3_GRP.add(EANA1);
    R3_GRP.add(EANA2);
    R3_GRP.add(EANA3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_20;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_23;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_37;
  private JXTitledSeparator OBJ_32;
  private JPanel P_SEL1;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private XRiTextField EDEBD;
  private XRiTextField EFIND;
  private JPanel P_SEL0;
  private JLabel OBJ_43;
  private XRiTextField EREP_doublon_44;
  private XRiRadioButton EOPT1;
  private XRiRadioButton EOPT2;
  private XRiCheckBox REPON1;
  private XRiRadioButton ESTA1;
  private XRiRadioButton ESTA2;
  private XRiCheckBox EREP;
  private XRiCheckBox WTOU;
  private XRiTextField ECAINF;
  private XRiTextField ECASUP;
  private JLabel OBJ_39;
  private JLabel OBJ_38;
  private JPanel panel1;
  private XRiRadioButton EANA1;
  private XRiRadioButton EANA2;
  private XRiRadioButton EANA3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
