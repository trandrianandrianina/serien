
package ri.serien.libecranrpg.sgvm.SGVM3QFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class SGVM3QFM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public SGVM3QFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    WA1 = new XRiTextField();
    WA2 = new XRiTextField();
    WA3 = new XRiTextField();
    WA4 = new XRiTextField();
    WA5 = new XRiTextField();
    OBJ_5 = new JLabel();
    OBJ_7 = new JLabel();
    OBJ_9 = new JLabel();
    OBJ_11 = new JLabel();
    OBJ_13 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- WA1 ----
        WA1.setName("WA1");
        p_contenu.add(WA1);
        WA1.setBounds(110, 10, 210, WA1.getPreferredSize().height);

        //---- WA2 ----
        WA2.setName("WA2");
        p_contenu.add(WA2);
        WA2.setBounds(110, 40, 210, WA2.getPreferredSize().height);

        //---- WA3 ----
        WA3.setName("WA3");
        p_contenu.add(WA3);
        WA3.setBounds(110, 70, 210, WA3.getPreferredSize().height);

        //---- WA4 ----
        WA4.setName("WA4");
        p_contenu.add(WA4);
        WA4.setBounds(110, 95, 210, WA4.getPreferredSize().height);

        //---- WA5 ----
        WA5.setName("WA5");
        p_contenu.add(WA5);
        WA5.setBounds(110, 125, 210, WA5.getPreferredSize().height);

        //---- OBJ_5 ----
        OBJ_5.setText("Article 1");
        OBJ_5.setName("OBJ_5");
        p_contenu.add(OBJ_5);
        OBJ_5.setBounds(25, 15, 68, 20);

        //---- OBJ_7 ----
        OBJ_7.setText("Article 1");
        OBJ_7.setName("OBJ_7");
        p_contenu.add(OBJ_7);
        OBJ_7.setBounds(25, 45, 68, 20);

        //---- OBJ_9 ----
        OBJ_9.setText("Article 1");
        OBJ_9.setName("OBJ_9");
        p_contenu.add(OBJ_9);
        OBJ_9.setBounds(25, 70, 68, 20);

        //---- OBJ_11 ----
        OBJ_11.setText("Article 1");
        OBJ_11.setName("OBJ_11");
        p_contenu.add(OBJ_11);
        OBJ_11.setBounds(25, 100, 68, 20);

        //---- OBJ_13 ----
        OBJ_13.setText("Article 1");
        OBJ_13.setName("OBJ_13");
        p_contenu.add(OBJ_13);
        OBJ_13.setBounds(25, 130, 68, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private XRiTextField WA1;
  private XRiTextField WA2;
  private XRiTextField WA3;
  private XRiTextField WA4;
  private XRiTextField WA5;
  private JLabel OBJ_5;
  private JLabel OBJ_7;
  private JLabel OBJ_9;
  private JLabel OBJ_11;
  private JLabel OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
