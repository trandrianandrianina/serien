
package ri.serien.libecranrpg.sgvm.SGVM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM06FM_B7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM06FM_B7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    OPT6.setValeurs("6", "OPT6");
    OPT5.setValeurs("5", "OPT5");
    OPT8.setValeurs("8", "OPT8");
    OPT4.setValeurs("4", "OPT4");
    OPT3.setValeurs("3", "OPT3");
    OPT7.setValeurs("7", "OPT7");
    OPT2.setValeurs("2", "OPT2");
    OPT1.setValeurs("1", "OPT1");
    PRIX6.setValeursSelection("X", " ");
    PRIX7.setValeursSelection("X", " ");
    PRIX8.setValeursSelection("X", " ");
    PRIX9.setValeursSelection("X", " ");
    PRIX10.setValeursSelection("X", " ");
    PRIX5.setValeursSelection("X", " ");
    PRIX4.setValeursSelection("X", " ");
    PRIX3.setValeursSelection("X", " ");
    PRIX2.setValeursSelection("X", " ");
    PRIX1.setValeursSelection("X", " ");
    REPEDT.setValeursSelection("O", "N");
    REPREF.setValeursSelection("O", "N");
    LIB4.setValeursSelection("X", " ");
    LIB3.setValeursSelection("X", " ");
    LIB2.setValeursSelection("X", " ");
    LIB1.setValeursSelection("X", " ");
    ARTHIS.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    OBJ_62.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Mot de classement @MOTCX@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FAMI@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FAMI@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFTAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    
    FAMDEB.setVisible(lexique.HostFieldGetData("FAMI").equalsIgnoreCase("Code famille à éditer"));
    FAMDEB.setEnabled(lexique.isPresent("FAMDEB"));
    RACNV.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    OBJ_61.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    RACNV.setEnabled(!lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    MCLA1.setVisible(lexique.isTrue("33") || lexique.isTrue("34"));
    MCLA1.setEnabled(!lexique.isPresent("MCLA1"));
    OBJ_75.setVisible(true);
    OBJ_75.setText("Référence Tarif à éditer");
    OBJ_68.setVisible(lexique.HostFieldGetData("FAMI").equalsIgnoreCase("Code famille à éditer"));
    OBJ_84.setVisible(lexique.isPresent("WCOL"));
    OBJ_78.setVisible(true);
    OBJ_64.setVisible(lexique.HostFieldGetData("FAMI").equalsIgnoreCase("Code Famille à éditer"));
    OBJ_63.setVisible(lexique.HostFieldGetData("FAMI").equalsIgnoreCase("Code Sous-Famille à éditer"));
    OBJ_62.setVisible(lexique.isTrue("33") || lexique.isTrue("34"));
    
    if (lexique.isTrue("33")) {
      OBJ_62.setTitle("Mot de classement 1");
    }
    if (lexique.isTrue("34")) {
      OBJ_62.setTitle("Mot de classement 2");
    }
    
    if (lexique.isTrue("92")) {
      OPT2.setText("Article d'une famille");
    }
    else {
      OPT2.setText("Article par famille");
    }
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel5 = new JPanel();
    OBJ_61 = new JXTitledSeparator();
    MCLA1 = new XRiTextField();
    RACNV = new XRiTextField();
    OBJ_62 = new JXTitledSeparator();
    OBJ_67 = new JLabel();
    OBJ_63 = new JXTitledSeparator();
    OBJ_68 = new JLabel();
    FAMDEB = new XRiTextField();
    OBJ_64 = new JXTitledSeparator();
    OBJ_46 = new JXTitledSeparator();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    WDEV = new XRiTextField();
    WDEV1 = new XRiTextField();
    ATDAPX = new XRiCalendrier();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_65 = new JLabel();
    OPTABC = new XRiComboBox();
    OPTAB1 = new XRiComboBox();
    ARTHIS = new XRiCheckBox();
    OBJ_52 = new JXTitledSeparator();
    OBJ_78 = new JXTitledSeparator();
    OBJ_75 = new JLabel();
    REFTAR = new XRiTextField();
    WCOL = new XRiTextField();
    OBJ_84 = new JLabel();
    OBJ_91 = new JXTitledSeparator();
    LIB1 = new XRiCheckBox();
    LIB2 = new XRiCheckBox();
    LIB3 = new XRiCheckBox();
    LIB4 = new XRiCheckBox();
    OBJ_96 = new JXTitledSeparator();
    REPREF = new XRiCheckBox();
    REPEDT = new XRiCheckBox();
    label1 = new JLabel();
    FRSFIN = new XRiTextField();
    OBJ_116 = new JXTitledSeparator();
    PRIX1 = new XRiCheckBox();
    PRIX2 = new XRiCheckBox();
    PRIX3 = new XRiCheckBox();
    PRIX4 = new XRiCheckBox();
    PRIX5 = new XRiCheckBox();
    PRIX10 = new XRiCheckBox();
    PRIX9 = new XRiCheckBox();
    PRIX8 = new XRiCheckBox();
    PRIX7 = new XRiCheckBox();
    PRIX6 = new XRiCheckBox();
    panel1 = new JPanel();
    OPT1 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT7 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    OPT4 = new XRiRadioButton();
    OPT8 = new XRiRadioButton();
    OPT5 = new XRiRadioButton();
    OPT6 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    RB_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 200));
          menus_haut.setPreferredSize(new Dimension(160, 200));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt_export ----
            riSousMenu_bt_export.setText("Exportation tableur");
            riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
            riSousMenu_bt_export.setName("riSousMenu_bt_export");
            riSousMenu_bt_export.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt_exportActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt_export);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- OBJ_61 ----
            OBJ_61.setTitle("Rattachement CNV (Facultatif)");
            OBJ_61.setName("OBJ_61");
            panel5.add(OBJ_61);
            OBJ_61.setBounds(20, 15, 335, OBJ_61.getPreferredSize().height);

            //---- MCLA1 ----
            MCLA1.setComponentPopupMenu(BTD);
            MCLA1.setName("MCLA1");
            panel5.add(MCLA1);
            MCLA1.setBounds(105, 45, 210, MCLA1.getPreferredSize().height);

            //---- RACNV ----
            RACNV.setComponentPopupMenu(BTD);
            RACNV.setName("RACNV");
            panel5.add(RACNV);
            RACNV.setBounds(180, 45, 60, RACNV.getPreferredSize().height);

            //---- OBJ_62 ----
            OBJ_62.setTitle("Mot de classement @MOTCX@");
            OBJ_62.setName("OBJ_62");
            panel5.add(OBJ_62);
            OBJ_62.setBounds(20, 15, 335, OBJ_62.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("@FAMI@");
            OBJ_67.setName("OBJ_67");
            panel5.add(OBJ_67);
            OBJ_67.setBounds(55, 50, 184, 18);

            //---- OBJ_63 ----
            OBJ_63.setTitle("");
            OBJ_63.setName("OBJ_63");
            panel5.add(OBJ_63);
            OBJ_63.setBounds(20, 15, 335, OBJ_63.getPreferredSize().height);

            //---- OBJ_68 ----
            OBJ_68.setText("@FAMI@");
            OBJ_68.setName("OBJ_68");
            panel5.add(OBJ_68);
            OBJ_68.setBounds(55, 50, 184, 18);

            //---- FAMDEB ----
            FAMDEB.setComponentPopupMenu(BTD);
            FAMDEB.setName("FAMDEB");
            panel5.add(FAMDEB);
            FAMDEB.setBounds(195, 45, 40, FAMDEB.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setTitle("");
            OBJ_64.setName("OBJ_64");
            panel5.add(OBJ_64);
            OBJ_64.setBounds(20, 15, 335, OBJ_64.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_46 ----
          OBJ_46.setTitle("");
          OBJ_46.setName("OBJ_46");

          //---- OBJ_47 ----
          OBJ_47.setText("S\u00e9lection date d'application");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_49 ----
          OBJ_49.setText("Devises");
          OBJ_49.setName("OBJ_49");

          //---- WDEV ----
          WDEV.setComponentPopupMenu(BTD);
          WDEV.setName("WDEV");

          //---- WDEV1 ----
          WDEV1.setComponentPopupMenu(BTD);
          WDEV1.setName("WDEV1");

          //---- ATDAPX ----
          ATDAPX.setName("ATDAPX");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_65 ----
          OBJ_65.setText("Filtre sur code ABC de l'article");
          OBJ_65.setName("OBJ_65");

          //---- OPTABC ----
          OPTABC.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTABC.setComponentPopupMenu(BTD);
          OPTABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTABC.setName("OPTABC");

          //---- OPTAB1 ----
          OPTAB1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTAB1.setComponentPopupMenu(BTD);
          OPTAB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTAB1.setName("OPTAB1");

          //---- ARTHIS ----
          ARTHIS.setText("Seulement Articles ayant d\u00e9ja tourn\u00e9s");
          ARTHIS.setComponentPopupMenu(BTD);
          ARTHIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARTHIS.setName("ARTHIS");

          //---- OBJ_52 ----
          OBJ_52.setTitle("Options possibles dans un tarif");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_78 ----
          OBJ_78.setTitle("");
          OBJ_78.setName("OBJ_78");

          //---- OBJ_75 ----
          OBJ_75.setText("@RFTAR@");
          OBJ_75.setName("OBJ_75");

          //---- REFTAR ----
          REFTAR.setComponentPopupMenu(BTD);
          REFTAR.setName("REFTAR");

          //---- WCOL ----
          WCOL.setComponentPopupMenu(BTD);
          WCOL.setName("WCOL");

          //---- OBJ_84 ----
          OBJ_84.setText("Colonne tarif \u00e0 \u00e9diter");
          OBJ_84.setName("OBJ_84");

          //---- OBJ_91 ----
          OBJ_91.setTitle("Libell\u00e9s articles \u00e0 \u00e9diter");
          OBJ_91.setName("OBJ_91");

          //---- LIB1 ----
          LIB1.setText("1");
          LIB1.setComponentPopupMenu(BTD);
          LIB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB1.setName("LIB1");

          //---- LIB2 ----
          LIB2.setText("2");
          LIB2.setComponentPopupMenu(BTD);
          LIB2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB2.setName("LIB2");

          //---- LIB3 ----
          LIB3.setText("3");
          LIB3.setComponentPopupMenu(BTD);
          LIB3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB3.setName("LIB3");

          //---- LIB4 ----
          LIB4.setText("4");
          LIB4.setComponentPopupMenu(BTD);
          LIB4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB4.setName("LIB4");

          //---- OBJ_96 ----
          OBJ_96.setTitle("");
          OBJ_96.setName("OBJ_96");

          //---- REPREF ----
          REPREF.setText("Edition r\u00e9f\u00e9rence fournisseur");
          REPREF.setComponentPopupMenu(BTD);
          REPREF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPREF.setName("REPREF");

          //---- REPEDT ----
          REPEDT.setText("Edition format 12 pouces");
          REPEDT.setComponentPopupMenu(BTD);
          REPEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPEDT.setName("REPEDT");

          //---- label1 ----
          label1.setText("Fournisseur \u00e0 traiter");
          label1.setName("label1");

          //---- FRSFIN ----
          FRSFIN.setName("FRSFIN");

          //---- OBJ_116 ----
          OBJ_116.setTitle("Num\u00e9ro tarif \u00e0 \u00e9diter");
          OBJ_116.setName("OBJ_116");

          //---- PRIX1 ----
          PRIX1.setText("1");
          PRIX1.setComponentPopupMenu(BTD);
          PRIX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX1.setName("PRIX1");

          //---- PRIX2 ----
          PRIX2.setText("2");
          PRIX2.setComponentPopupMenu(BTD);
          PRIX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX2.setName("PRIX2");

          //---- PRIX3 ----
          PRIX3.setText("3");
          PRIX3.setComponentPopupMenu(BTD);
          PRIX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX3.setName("PRIX3");

          //---- PRIX4 ----
          PRIX4.setText("4");
          PRIX4.setComponentPopupMenu(BTD);
          PRIX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX4.setName("PRIX4");

          //---- PRIX5 ----
          PRIX5.setText("5");
          PRIX5.setComponentPopupMenu(BTD);
          PRIX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX5.setName("PRIX5");

          //---- PRIX10 ----
          PRIX10.setText("10");
          PRIX10.setComponentPopupMenu(BTD);
          PRIX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX10.setName("PRIX10");

          //---- PRIX9 ----
          PRIX9.setText("9");
          PRIX9.setComponentPopupMenu(BTD);
          PRIX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX9.setName("PRIX9");

          //---- PRIX8 ----
          PRIX8.setText("8");
          PRIX8.setComponentPopupMenu(BTD);
          PRIX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX8.setName("PRIX8");

          //---- PRIX7 ----
          PRIX7.setText("7");
          PRIX7.setComponentPopupMenu(BTD);
          PRIX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX7.setName("PRIX7");

          //---- PRIX6 ----
          PRIX6.setText("6");
          PRIX6.setComponentPopupMenu(BTD);
          PRIX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX6.setName("PRIX6");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OPT1 ----
            OPT1.setText("Toutes les r\u00e9f\u00e9rences");
            OPT1.setComponentPopupMenu(BTD);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel1.add(OPT1);
            OPT1.setBounds(10, 10, 282, 18);

            //---- OPT2 ----
            OPT2.setText("Articles par famille");
            OPT2.setComponentPopupMenu(BTD);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel1.add(OPT2);
            OPT2.setBounds(10, 29, 282, 18);

            //---- OPT7 ----
            OPT7.setText("Articles par sous-famille");
            OPT7.setComponentPopupMenu(BTD);
            OPT7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT7.setName("OPT7");
            panel1.add(OPT7);
            OPT7.setBounds(10, 48, 282, 20);

            //---- OPT3 ----
            OPT3.setText("Articles sur mot de classement 1");
            OPT3.setComponentPopupMenu(BTD);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setName("OPT3");
            panel1.add(OPT3);
            OPT3.setBounds(10, 69, 282, 18);

            //---- OPT4 ----
            OPT4.setText("Articles de m\u00eame mot classement 2");
            OPT4.setComponentPopupMenu(BTD);
            OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT4.setName("OPT4");
            panel1.add(OPT4);
            OPT4.setBounds(10, 88, 282, 18);

            //---- OPT8 ----
            OPT8.setText("Articles par mot class.2 et famille");
            OPT8.setComponentPopupMenu(BTD);
            OPT8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT8.setName("OPT8");
            panel1.add(OPT8);
            OPT8.setBounds(10, 107, 282, 18);

            //---- OPT5 ----
            OPT5.setText("Articles de m\u00eame rattachement CNV");
            OPT5.setComponentPopupMenu(BTD);
            OPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT5.setName("OPT5");
            panel1.add(OPT5);
            OPT5.setBounds(10, 126, 282, 18);

            //---- OPT6 ----
            OPT6.setText("Articles sur zones personnalis\u00e9es");
            OPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT6.setName("OPT6");
            panel1.add(OPT6);
            OPT6.setBounds(10, 145, 282, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 855, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 855, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(ATDAPX, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                .addGap(100, 100, 100)
                .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OPTABC, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(OPTAB1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(150, 150, 150)
                .addComponent(WDEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WDEV1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(135, 135, 135)
                .addComponent(ARTHIS, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 385, GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(REPREF, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
                      .addComponent(REPEDT, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                        .addGap(65, 65, 65)
                        .addComponent(FRSFIN, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addGap(110, 110, 110)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(REFTAR, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addComponent(WCOL, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_116, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PRIX1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PRIX6, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX8, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX9, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(PRIX10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(LIB1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(LIB2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(LIB3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(LIB4, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE)
                    .addGap(25, 25, 25)
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE))))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(ATDAPX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OPTABC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OPTAB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WDEV1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ARTHIS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addComponent(REFTAR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_116, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(PRIX1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(PRIX6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRIX10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(17, 17, 17)
                    .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(11, 11, 11)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(LIB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIB3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIB4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(REPREF, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                    .addGap(9, 9, 9)
                    .addComponent(REPEDT, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                    .addGap(4, 4, 4)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                      .addComponent(FRSFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //---- RB_GRP ----
    RB_GRP.add(OPT1);
    RB_GRP.add(OPT2);
    RB_GRP.add(OPT7);
    RB_GRP.add(OPT3);
    RB_GRP.add(OPT4);
    RB_GRP.add(OPT8);
    RB_GRP.add(OPT5);
    RB_GRP.add(OPT6);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel5;
  private JXTitledSeparator OBJ_61;
  private XRiTextField MCLA1;
  private XRiTextField RACNV;
  private JXTitledSeparator OBJ_62;
  private JLabel OBJ_67;
  private JXTitledSeparator OBJ_63;
  private JLabel OBJ_68;
  private XRiTextField FAMDEB;
  private JXTitledSeparator OBJ_64;
  private JXTitledSeparator OBJ_46;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private XRiTextField WDEV;
  private XRiTextField WDEV1;
  private XRiCalendrier ATDAPX;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JLabel OBJ_65;
  private XRiComboBox OPTABC;
  private XRiComboBox OPTAB1;
  private XRiCheckBox ARTHIS;
  private JXTitledSeparator OBJ_52;
  private JXTitledSeparator OBJ_78;
  private JLabel OBJ_75;
  private XRiTextField REFTAR;
  private XRiTextField WCOL;
  private JLabel OBJ_84;
  private JXTitledSeparator OBJ_91;
  private XRiCheckBox LIB1;
  private XRiCheckBox LIB2;
  private XRiCheckBox LIB3;
  private XRiCheckBox LIB4;
  private JXTitledSeparator OBJ_96;
  private XRiCheckBox REPREF;
  private XRiCheckBox REPEDT;
  private JLabel label1;
  private XRiTextField FRSFIN;
  private JXTitledSeparator OBJ_116;
  private XRiCheckBox PRIX1;
  private XRiCheckBox PRIX2;
  private XRiCheckBox PRIX3;
  private XRiCheckBox PRIX4;
  private XRiCheckBox PRIX5;
  private XRiCheckBox PRIX10;
  private XRiCheckBox PRIX9;
  private XRiCheckBox PRIX8;
  private XRiCheckBox PRIX7;
  private XRiCheckBox PRIX6;
  private JPanel panel1;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT7;
  private XRiRadioButton OPT3;
  private XRiRadioButton OPT4;
  private XRiRadioButton OPT8;
  private XRiRadioButton OPT5;
  private XRiRadioButton OPT6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private ButtonGroup RB_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
