/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM321F;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.sngroupe.SNGroupe;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM414] Gestion des ventes -> Statistiques de ventes -> Statistiques clients -> Ventes par client et par article
 */
public class SGVM321F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  /**
   * Constructeur.
   */
  public SGVM321F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    // Lier les composants catégorie clients
    snCategorieClientDebut.lierComposantFin(snCategorieClientFin);
    
    // Lier les composants groupes, famille, sous famlille
    snGroupeDebut.lierComposantFin(snGroupeFin);
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snSousFamilleDebut.lierComposantFin(snSousFamilleFin);
    
    snGroupeDebut.lierComposantFamille(snFamilleDebut);
    snGroupeDebut.lierComposantSousFamille(snSousFamilleDebut);
    
    snGroupeFin.lierComposantFamille(snFamilleFin);
    snGroupeFin.lierComposantSousFamille(snSousFamilleFin);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gérer les erreurs automatiquement
    gererLesErreurs("19");
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Sélectionner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Rafraichir le logo de l'établissemnt
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Rafraichir le type d'importation
    if (lexique.HostFieldGetData("DETART").trim().equalsIgnoreCase("O")) {
      rbVentesClientArticle.setSelected(true);
      rbVentesClient.setSelected(false);
    }
    else {
      rbVentesClient.setSelected(true);
      rbVentesClientArticle.setSelected(false);
    }
    
    // Rafraichir la plage de dates période 1
    snPeriode1.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPeriode1.setDateDebutParChampRPG(lexique, "PERDEB");
    snPeriode1.setDateFinParChampRPG(lexique, "PERFIN");
    
    // Rafraichir la plage de dates période 2
    snPeriode2.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPeriode2.setDateDebutParChampRPG(lexique, "PERDEB2");
    snPeriode2.setDateFinParChampRPG(lexique, "PERFIN2");
    
    // Rafraichir la plage de catégories de clients
    snCategorieClientDebut.setSession(getSession());
    snCategorieClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientDebut.setTousAutorise(true);
    snCategorieClientDebut.charger(false);
    snCategorieClientDebut.setSelectionParChampRPG(lexique, "CATDEB");
    
    snCategorieClientFin.setSession(getSession());
    snCategorieClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientFin.setTousAutorise(true);
    snCategorieClientFin.charger(false);
    snCategorieClientFin.setSelectionParChampRPG(lexique, "CATFIN");
    
    // Rafraichir la plage de clients
    snClientDebut.setSession(getSession());
    snClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snClientDebut.charger(false);
    snClientDebut.setSelectionParChampRPG(lexique, "CLIDEB");
    
    snClientFin.setSession(getSession());
    snClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snClientFin.charger(false);
    snClientFin.setSelectionParChampRPG(lexique, "CLIFIN");
    
    // Rafraichir la plage de groupes
    snGroupeDebut.setSession(getSession());
    snGroupeDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snGroupeDebut.charger(false);
    snGroupeDebut.setSelectionParChampRPG(lexique, "GRPDEB");
    
    snGroupeFin.setSession(getSession());
    snGroupeFin.setIdEtablissement(snEtablissement.getIdSelection());
    snGroupeFin.charger(false);
    snGroupeFin.setSelectionParChampRPG(lexique, "GRPFIN");
    
    // Rafraichir la plage de familles
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
    
    // Rafraichir la plage de sous-familles
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.charger(false);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "SFADEB");
    
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.charger(false);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "SFAFIN");
    
    // Rafraichir la plage d'articles
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "ARTDEB");
    
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "ARTFin");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Récupérer l'établissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Récupérer le type d'exporation
    if (rbVentesClientArticle.isSelected()) {
      lexique.HostFieldPutData("DETART", 0, "O");
    }
    else {
      lexique.HostFieldPutData("DETART", 0, " ");
    }
    
    // Récupérer les plages de dates
    snPeriode1.renseignerChampRPGDebut(lexique, "PERDEB");
    snPeriode1.renseignerChampRPGFin(lexique, "PERFIN");
    snPeriode2.renseignerChampRPGDebut(lexique, "PERDEB2");
    snPeriode2.renseignerChampRPGFin(lexique, "PERFIN2");
    
    // Récupérer la plage de catégories de clients
    snCategorieClientDebut.renseignerChampRPG(lexique, "CATDEB");
    snCategorieClientFin.renseignerChampRPG(lexique, "CATFIN");
    
    // Récupérer la plage de clients
    snClientDebut.renseignerChampRPG(lexique, "CLIDEB");
    snClientFin.renseignerChampRPG(lexique, "CLIFIN");
    
    // Récupérer la plage de groupes
    snGroupeDebut.renseignerChampRPG(lexique, "GRPDEB");
    snGroupeFin.renseignerChampRPG(lexique, "GRPFIN");
    
    // Récupérer la plage de familles
    snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
    snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    
    // Récupérer la plage de sous-familles
    snSousFamilleDebut.renseignerChampRPG(lexique, "SFADEB");
    snSousFamilleFin.renseignerChampRPG(lexique, "SFAFIN");
    
    // Récupérer la plage d'articles
    snArticleDebut.renseignerChampRPG(lexique, "ARTDEB");
    snArticleFin.renseignerChampRPG(lexique, "ARTFIN");
  }
  
  /**
   * Traiter les clics sur les boutons de la barre de boutons.
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    sNPanelTitre3 = new SNPanelTitre();
    rbVentesClientArticle = new JRadioButton();
    rbVentesClient = new JRadioButton();
    pnlPeriode = new SNPanelTitre();
    lbPeriode1 = new SNLabelChamp();
    snPeriode1 = new SNPlageDate();
    lbPeriode2 = new SNLabelChamp();
    snPeriode2 = new SNPlageDate();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlSelectionClient = new SNPanelTitre();
    lbCategorieClientsDe = new SNLabelChamp();
    snCategorieClientDebut = new SNCategorieClient();
    lbCategorieClientsA = new SNLabelUnite();
    snCategorieClientFin = new SNCategorieClient();
    lbClientsDe = new SNLabelChamp();
    snClientDebut = new SNClientPrincipal();
    lbClientsA = new SNLabelUnite();
    snClientFin = new SNClientPrincipal();
    pnlSelectionArticle = new SNPanelTitre();
    lbGroupesDe = new SNLabelChamp();
    snGroupeDebut = new SNGroupe();
    lbGroupesA = new SNLabelUnite();
    snGroupeFin = new SNGroupe();
    lbFamillesDe = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFamillesA = new SNLabelUnite();
    snFamilleFin = new SNFamille();
    lbSousFamillesDe = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamillesA = new SNLabelUnite();
    snSousFamilleFin = new SNSousFamille();
    lbArticlesDe = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticlesA = new SNLabelUnite();
    snArticleFin = new SNArticle();
    
    // ======== this ========
    setMaximumSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Ventes par client et par article");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout(1, 0, 5, 5));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== sNPanelTitre3 ========
          {
            sNPanelTitre3.setTitre("Type d'exportation");
            sNPanelTitre3.setName("sNPanelTitre3");
            sNPanelTitre3.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanelTitre3.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanelTitre3.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanelTitre3.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanelTitre3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- rbVentesClientArticle ----
            rbVentesClientArticle.setText("Ventes par client et par article");
            rbVentesClientArticle.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbVentesClientArticle.setSelected(true);
            rbVentesClientArticle.setMaximumSize(new Dimension(211, 30));
            rbVentesClientArticle.setMinimumSize(new Dimension(211, 30));
            rbVentesClientArticle.setPreferredSize(new Dimension(211, 30));
            rbVentesClientArticle.setName("rbVentesClientArticle");
            sNPanelTitre3.add(rbVentesClientArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbVentesClient ----
            rbVentesClient.setText("Ventes par client");
            rbVentesClient.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbVentesClient.setPreferredSize(new Dimension(128, 30));
            rbVentesClient.setMinimumSize(new Dimension(128, 30));
            rbVentesClient.setMaximumSize(new Dimension(128, 30));
            rbVentesClient.setName("rbVentesClient");
            sNPanelTitre3.add(rbVentesClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(sNPanelTitre3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlPeriode ========
          {
            pnlPeriode.setTitre("S\u00e9lection dates de facturation");
            pnlPeriode.setName("pnlPeriode");
            pnlPeriode.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPeriode.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPeriode.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPeriode.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPeriode.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriode1 ----
            lbPeriode1.setText("P\u00e9riode 1 ");
            lbPeriode1.setName("lbPeriode1");
            pnlPeriode.add(lbPeriode1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPeriode1 ----
            snPeriode1.setName("snPeriode1");
            pnlPeriode.add(snPeriode1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriode2 ----
            lbPeriode2.setText("P\u00e9riode 2 ");
            lbPeriode2.setName("lbPeriode2");
            pnlPeriode.add(lbPeriode2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snPeriode2 ----
            snPeriode2.setName("snPeriode2");
            pnlPeriode.add(snPeriode2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlSelectionClient ========
      {
        pnlSelectionClient.setTitre("S\u00e9lection clients");
        pnlSelectionClient.setName("pnlSelectionClient");
        pnlSelectionClient.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSelectionClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlSelectionClient.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlSelectionClient.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlSelectionClient.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCategorieClientsDe ----
        lbCategorieClientsDe.setText("Cat\u00e9gories clients de");
        lbCategorieClientsDe.setName("lbCategorieClientsDe");
        pnlSelectionClient.add(lbCategorieClientsDe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCategorieClientDebut ----
        snCategorieClientDebut.setName("snCategorieClientDebut");
        pnlSelectionClient.add(snCategorieClientDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbCategorieClientsA ----
        lbCategorieClientsA.setText("\u00e0");
        lbCategorieClientsA.setInheritsPopupMenu(false);
        lbCategorieClientsA.setMaximumSize(new Dimension(20, 30));
        lbCategorieClientsA.setMinimumSize(new Dimension(20, 30));
        lbCategorieClientsA.setPreferredSize(new Dimension(20, 30));
        lbCategorieClientsA.setHorizontalAlignment(SwingConstants.CENTER);
        lbCategorieClientsA.setName("lbCategorieClientsA");
        pnlSelectionClient.add(lbCategorieClientsA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCategorieClientFin ----
        snCategorieClientFin.setName("snCategorieClientFin");
        pnlSelectionClient.add(snCategorieClientFin, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbClientsDe ----
        lbClientsDe.setText("Clients de");
        lbClientsDe.setName("lbClientsDe");
        pnlSelectionClient.add(lbClientsDe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snClientDebut ----
        snClientDebut.setName("snClientDebut");
        pnlSelectionClient.add(snClientDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbClientsA ----
        lbClientsA.setText("\u00e0");
        lbClientsA.setInheritsPopupMenu(false);
        lbClientsA.setMaximumSize(new Dimension(20, 30));
        lbClientsA.setMinimumSize(new Dimension(20, 30));
        lbClientsA.setPreferredSize(new Dimension(20, 30));
        lbClientsA.setHorizontalAlignment(SwingConstants.CENTER);
        lbClientsA.setName("lbClientsA");
        pnlSelectionClient.add(lbClientsA, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snClientFin ----
        snClientFin.setName("snClientFin");
        pnlSelectionClient.add(snClientFin, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSelectionClient,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlSelectionArticle ========
      {
        pnlSelectionArticle.setTitre("S\u00e9lection articles");
        pnlSelectionArticle.setName("pnlSelectionArticle");
        pnlSelectionArticle.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSelectionArticle.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlSelectionArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlSelectionArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlSelectionArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbGroupesDe ----
        lbGroupesDe.setText("Groupes de");
        lbGroupesDe.setName("lbGroupesDe");
        pnlSelectionArticle.add(lbGroupesDe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snGroupeDebut ----
        snGroupeDebut.setName("snGroupeDebut");
        pnlSelectionArticle.add(snGroupeDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbGroupesA ----
        lbGroupesA.setText("\u00e0");
        lbGroupesA.setInheritsPopupMenu(false);
        lbGroupesA.setMaximumSize(new Dimension(20, 30));
        lbGroupesA.setMinimumSize(new Dimension(20, 30));
        lbGroupesA.setPreferredSize(new Dimension(20, 30));
        lbGroupesA.setHorizontalAlignment(SwingConstants.CENTER);
        lbGroupesA.setName("lbGroupesA");
        pnlSelectionArticle.add(lbGroupesA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snGroupeFin ----
        snGroupeFin.setName("snGroupeFin");
        pnlSelectionArticle.add(snGroupeFin, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFamillesDe ----
        lbFamillesDe.setText("Familles de");
        lbFamillesDe.setName("lbFamillesDe");
        pnlSelectionArticle.add(lbFamillesDe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snFamilleDebut ----
        snFamilleDebut.setName("snFamilleDebut");
        pnlSelectionArticle.add(snFamilleDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbFamillesA ----
        lbFamillesA.setText("\u00e0");
        lbFamillesA.setInheritsPopupMenu(false);
        lbFamillesA.setMaximumSize(new Dimension(20, 30));
        lbFamillesA.setMinimumSize(new Dimension(20, 30));
        lbFamillesA.setPreferredSize(new Dimension(20, 30));
        lbFamillesA.setHorizontalAlignment(SwingConstants.CENTER);
        lbFamillesA.setName("lbFamillesA");
        pnlSelectionArticle.add(lbFamillesA, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snFamilleFin ----
        snFamilleFin.setName("snFamilleFin");
        pnlSelectionArticle.add(snFamilleFin, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSousFamillesDe ----
        lbSousFamillesDe.setText("Sous-familles de");
        lbSousFamillesDe.setName("lbSousFamillesDe");
        pnlSelectionArticle.add(lbSousFamillesDe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snSousFamilleDebut ----
        snSousFamilleDebut.setName("snSousFamilleDebut");
        pnlSelectionArticle.add(snSousFamilleDebut, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbSousFamillesA ----
        lbSousFamillesA.setText("\u00e0");
        lbSousFamillesA.setInheritsPopupMenu(false);
        lbSousFamillesA.setMaximumSize(new Dimension(20, 30));
        lbSousFamillesA.setMinimumSize(new Dimension(20, 30));
        lbSousFamillesA.setPreferredSize(new Dimension(20, 30));
        lbSousFamillesA.setHorizontalAlignment(SwingConstants.CENTER);
        lbSousFamillesA.setName("lbSousFamillesA");
        pnlSelectionArticle.add(lbSousFamillesA, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snSousFamilleFin ----
        snSousFamilleFin.setName("snSousFamilleFin");
        pnlSelectionArticle.add(snSousFamilleFin, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbArticlesDe ----
        lbArticlesDe.setText("Articles de");
        lbArticlesDe.setName("lbArticlesDe");
        pnlSelectionArticle.add(lbArticlesDe, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snArticleDebut ----
        snArticleDebut.setMaximumSize(new Dimension(350, 30));
        snArticleDebut.setMinimumSize(new Dimension(350, 30));
        snArticleDebut.setPreferredSize(new Dimension(350, 30));
        snArticleDebut.setName("snArticleDebut");
        pnlSelectionArticle.add(snArticleDebut, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbArticlesA ----
        lbArticlesA.setText("\u00e0");
        lbArticlesA.setInheritsPopupMenu(false);
        lbArticlesA.setMaximumSize(new Dimension(20, 30));
        lbArticlesA.setMinimumSize(new Dimension(20, 30));
        lbArticlesA.setPreferredSize(new Dimension(20, 30));
        lbArticlesA.setHorizontalAlignment(SwingConstants.CENTER);
        lbArticlesA.setName("lbArticlesA");
        pnlSelectionArticle.add(lbArticlesA, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snArticleFin ----
        snArticleFin.setMaximumSize(new Dimension(350, 30));
        snArticleFin.setMinimumSize(new Dimension(350, 30));
        snArticleFin.setPreferredSize(new Dimension(350, 30));
        snArticleFin.setName("snArticleFin");
        pnlSelectionArticle.add(snArticleFin, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSelectionArticle,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- TypExportation ----
    ButtonGroup TypExportation = new ButtonGroup();
    TypExportation.add(rbVentesClientArticle);
    TypExportation.add(rbVentesClient);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre sNPanelTitre3;
  private JRadioButton rbVentesClientArticle;
  private JRadioButton rbVentesClient;
  private SNPanelTitre pnlPeriode;
  private SNLabelChamp lbPeriode1;
  private SNPlageDate snPeriode1;
  private SNLabelChamp lbPeriode2;
  private SNPlageDate snPeriode2;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlSelectionClient;
  private SNLabelChamp lbCategorieClientsDe;
  private SNCategorieClient snCategorieClientDebut;
  private SNLabelUnite lbCategorieClientsA;
  private SNCategorieClient snCategorieClientFin;
  private SNLabelChamp lbClientsDe;
  private SNClientPrincipal snClientDebut;
  private SNLabelUnite lbClientsA;
  private SNClientPrincipal snClientFin;
  private SNPanelTitre pnlSelectionArticle;
  private SNLabelChamp lbGroupesDe;
  private SNGroupe snGroupeDebut;
  private SNLabelUnite lbGroupesA;
  private SNGroupe snGroupeFin;
  private SNLabelChamp lbFamillesDe;
  private SNFamille snFamilleDebut;
  private SNLabelUnite lbFamillesA;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSousFamillesDe;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelUnite lbSousFamillesA;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbArticlesDe;
  private SNArticle snArticleDebut;
  private SNLabelUnite lbArticlesA;
  private SNArticle snArticleFin;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
