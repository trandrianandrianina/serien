
package ri.serien.libecranrpg.sgvm.SGVMA1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class SGVMA1FM_M1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMA1FM_M1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@MSLB1@")).trim()));
    MSLB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSLB2@")).trim());
    panel2.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@MSLB3@")).trim()));
    MSLB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSLB4@")).trim());
    TIME.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*TIME@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    Date jour = new Date();
    // TIME.setText(DateFormat.getTimeInstance().format(jour));
    
    DateFormat mediumDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    
    TIME.setText(mediumDateFormat.format(jour));
    
    
    panel1.setVisible(!lexique.HostFieldGetData("MSLB2").trim().equalsIgnoreCase(""));
    panel2.setVisible(!lexique.HostFieldGetData("MSLB4").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle("ATTENTION");
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REPONS", 0, "R");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REPONS", 0, "V");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REPONS", 0, "E");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REPON1", 0, "V");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REPON1", 0, "E");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    MSLB2 = new RiZoneSortie();
    panel2 = new JPanel();
    MSLB4 = new RiZoneSortie();
    TIME = new JLabel();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(605, 230));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Affichage devis");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Affichage commande");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Extraction commande");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("@MSLB1@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- MSLB2 ----
          MSLB2.setText("@MSLB2@");
          MSLB2.setName("MSLB2");
          panel1.add(MSLB2);
          MSLB2.setBounds(15, 35, 370, MSLB2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("@MSLB3@"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- MSLB4 ----
          MSLB4.setText("@MSLB4@");
          MSLB4.setName("MSLB4");
          panel2.add(MSLB4);
          MSLB4.setBounds(15, 35, 370, MSLB4.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        //---- TIME ----
        TIME.setText("@*TIME@");
        TIME.setHorizontalAlignment(SwingConstants.RIGHT);
        TIME.setName("TIME");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(200, 200, 200)
              .addComponent(TIME, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(TIME, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== riSousMenu7 ========
    {
      riSousMenu7.setName("riSousMenu7");

      //---- riSousMenu_bt7 ----
      riSousMenu_bt7.setText("Extraction devis");
      riSousMenu_bt7.setName("riSousMenu_bt7");
      riSousMenu_bt7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt7ActionPerformed(e);
        }
      });
      riSousMenu7.add(riSousMenu_bt7);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie MSLB2;
  private JPanel panel2;
  private RiZoneSortie MSLB4;
  private JLabel TIME;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
