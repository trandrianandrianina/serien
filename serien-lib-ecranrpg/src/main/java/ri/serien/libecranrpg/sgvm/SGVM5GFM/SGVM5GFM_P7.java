
package ri.serien.libecranrpg.sgvm.SGVM5GFM;
// Nom Fichier: pop_SGVM5GFM_FMTP7_FMTF1_1179.java

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM5GFM_P7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM5GFM_P7(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_8 = new JLabel();

    //======== this ========
    setPreferredSize(new Dimension(290, 115));
    setName("this");
    setLayout(null);

    //---- OBJ_8 ----
    OBJ_8.setText("G\u00e9n\u00e9ration Brdx de Hausse en cours");
    OBJ_8.setName("OBJ_8");
    add(OBJ_8);
    OBJ_8.setBounds(35, 35, 222, 20);

    setPreferredSize(new Dimension(290, 115));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
