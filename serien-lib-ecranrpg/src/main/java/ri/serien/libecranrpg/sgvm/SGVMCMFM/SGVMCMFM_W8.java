
package ri.serien.libecranrpg.sgvm.SGVMCMFM;
// Nom Fichier: pop_SGVMCMFM_FMTW8_FMTF1_1287.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVMCMFM_W8 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMCMFM_W8(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    PCMNBE.setEnabled(lexique.isPresent("PCMNBE"));
    
    // TODO Icones
    OBJ_11.setIcon(lexique.chargerImage("images/retour.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_9 = new JLabel();
    OBJ_11 = new JButton();
    PCMNBE = new XRiTextField();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_9 ----
    OBJ_9.setText("S\u00e9lection Termin\u00e9e, Nombre de Clients");
    OBJ_9.setName("OBJ_9");
    add(OBJ_9);
    OBJ_9.setBounds(30, 24, 237, 20);

    //---- OBJ_11 ----
    OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });
    add(OBJ_11);
    OBJ_11.setBounds(280, 55, 56, 40);

    //---- PCMNBE ----
    PCMNBE.setName("PCMNBE");
    add(PCMNBE);
    PCMNBE.setBounds(280, 20, 50, PCMNBE.getPreferredSize().height);

    setPreferredSize(new Dimension(350, 105));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_9;
  private JButton OBJ_11;
  private XRiTextField PCMNBE;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
