
package ri.serien.libecranrpg.sgvm.SGVMD3FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMD3FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMD3FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOUA.setValeursSelection("**", "  ");
    WTOUFA.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // WTOUFA.setEnabled( lexique.isPresent("WTOUFA"));
    // WTOUFA.setSelected(lexique.HostFieldGetData("WTOUFA").equalsIgnoreCase("**"));
    // WTOUA.setEnabled( lexique.isPresent("WTOUA"));
    // WTOUA.setSelected(lexique.HostFieldGetData("WTOUA").equalsIgnoreCase("**"));
    // WTOUM.setEnabled( lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    // DATFIN.setEnabled( lexique.isPresent("DATFIN"));
    // DATDEB.setEnabled( lexique.isPresent("DATDEB"));
    panel5.setVisible(!lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    panel6.setVisible(!lexique.HostFieldGetData("WTOUFA").equalsIgnoreCase("**"));
    panel7.setVisible(!lexique.HostFieldGetData("WTOUA").equalsIgnoreCase("**"));
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOUFA.isSelected())
    // lexique.HostFieldPutData("WTOUFA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUFA", 0, " ");
    // if (WTOUA.isSelected())
    // lexique.HostFieldPutData("WTOUA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUA", 0, " ");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    panel5.setVisible(!panel5.isVisible());
  }
  
  private void WTOUFAActionPerformed(ActionEvent e) {
    panel6.setVisible(!panel6.isVisible());
  }
  
  private void WTOUAActionPerformed(ActionEvent e) {
    panel7.setVisible(!panel7.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_31 = new JXTitledSeparator();
    OBJ_32 = new JXTitledSeparator();
    OBJ_33 = new JXTitledSeparator();
    OBJ_34 = new JXTitledSeparator();
    panel1 = new JPanel();
    WTOUM = new XRiCheckBox();
    panel5 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    panel2 = new JPanel();
    WTOUFA = new XRiCheckBox();
    panel3 = new JPanel();
    WTOUA = new XRiCheckBox();
    panel7 = new JPanel();
    OBJ_63 = new JLabel();
    EDEBA = new XRiTextField();
    OBJ_62 = new JLabel();
    EFINA = new XRiTextField();
    panel4 = new JPanel();
    OBJ_41 = new JLabel();
    DATDEB = new XRiCalendrier();
    OBJ_43 = new JLabel();
    DATFIN = new XRiCalendrier();
    panel6 = new JPanel();
    OBJ_58 = new JLabel();
    EDEB = new XRiTextField();
    OBJ_60 = new JLabel();
    EFIN = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 490));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_31 ----
          OBJ_31.setTitle("Plage de familles");
          OBJ_31.setName("OBJ_31");

          //---- OBJ_32 ----
          OBJ_32.setTitle("Plage d'articles");
          OBJ_32.setName("OBJ_32");

          //---- OBJ_33 ----
          OBJ_33.setTitle("Plage de dates de livraison souhait\u00e9es");
          OBJ_33.setName("OBJ_33");

          //---- OBJ_34 ----
          OBJ_34.setTitle("Code(s) magasin(s) \u00e0 traiter");
          OBJ_34.setName("OBJ_34");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setComponentPopupMenu(null);
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            panel1.add(WTOUM);
            WTOUM.setBounds(15, 15, 150, 20);

            //======== panel5 ========
            {
              panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              panel5.add(MA01);
              MA01.setBounds(10, 6, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              panel5.add(MA02);
              MA02.setBounds(46, 6, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              panel5.add(MA03);
              MA03.setBounds(82, 6, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              panel5.add(MA04);
              MA04.setBounds(118, 6, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              panel5.add(MA05);
              MA05.setBounds(154, 6, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              panel5.add(MA06);
              MA06.setBounds(190, 6, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              panel5.add(MA07);
              MA07.setBounds(226, 6, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              panel5.add(MA08);
              MA08.setBounds(262, 6, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              panel5.add(MA09);
              MA09.setBounds(298, 6, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              panel5.add(MA10);
              MA10.setBounds(334, 6, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              panel5.add(MA11);
              MA11.setBounds(370, 6, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              panel5.add(MA12);
              MA12.setBounds(406, 6, 34, MA12.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel5);
            panel5.setBounds(165, 5, 455, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WTOUFA ----
            WTOUFA.setText("S\u00e9lection compl\u00e8te");
            WTOUFA.setComponentPopupMenu(null);
            WTOUFA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUFA.setName("WTOUFA");
            WTOUFA.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUFAActionPerformed(e);
              }
            });
            panel2.add(WTOUFA);
            WTOUFA.setBounds(10, 10, 150, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WTOUA ----
            WTOUA.setText("S\u00e9lection compl\u00e8te");
            WTOUA.setComponentPopupMenu(null);
            WTOUA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUA.setName("WTOUA");
            WTOUA.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUAActionPerformed(e);
              }
            });
            panel3.add(WTOUA);
            WTOUA.setBounds(15, 15, 150, 20);

            //======== panel7 ========
            {
              panel7.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);

              //---- OBJ_63 ----
              OBJ_63.setText("D\u00e9but");
              OBJ_63.setName("OBJ_63");
              panel7.add(OBJ_63);
              OBJ_63.setBounds(10, 10, 43, 20);

              //---- EDEBA ----
              EDEBA.setComponentPopupMenu(BTD);
              EDEBA.setName("EDEBA");
              panel7.add(EDEBA);
              EDEBA.setBounds(65, 6, 210, EDEBA.getPreferredSize().height);

              //---- OBJ_62 ----
              OBJ_62.setText("Fin");
              OBJ_62.setName("OBJ_62");
              panel7.add(OBJ_62);
              OBJ_62.setBounds(305, 10, 35, 20);

              //---- EFINA ----
              EFINA.setComponentPopupMenu(BTD);
              EFINA.setName("EFINA");
              panel7.add(EFINA);
              EFINA.setBounds(340, 6, 210, EFINA.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel7);
            panel7.setBounds(165, 5, 560, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_41 ----
            OBJ_41.setText("Date de d\u00e9but");
            OBJ_41.setName("OBJ_41");
            panel4.add(OBJ_41);
            OBJ_41.setBounds(20, 10, 105, 20);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel4.add(DATDEB);
            DATDEB.setBounds(165, 5, 105, DATDEB.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Date de fin");
            OBJ_43.setName("OBJ_43");
            panel4.add(OBJ_43);
            OBJ_43.setBounds(340, 10, 75, 20);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel4.add(DATFIN);
            DATFIN.setBounds(439, 5, 105, DATFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel6 ========
          {
            panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_58 ----
            OBJ_58.setText("Plage de d\u00e9but");
            OBJ_58.setName("OBJ_58");
            panel6.add(OBJ_58);
            OBJ_58.setBounds(10, 10, 105, 20);

            //---- EDEB ----
            EDEB.setComponentPopupMenu(BTD);
            EDEB.setName("EDEB");
            panel6.add(EDEB);
            EDEB.setBounds(115, 6, 40, EDEB.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setText("Plage de fin");
            OBJ_60.setName("OBJ_60");
            panel6.add(OBJ_60);
            OBJ_60.setBounds(200, 10, 85, 20);

            //---- EFIN ----
            EFIN.setComponentPopupMenu(BTD);
            EFIN.setName("EFIN");
            panel6.add(EFIN);
            EFIN.setBounds(285, 6, 40, EFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(77, 77, 77)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 745, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_31;
  private JXTitledSeparator OBJ_32;
  private JXTitledSeparator OBJ_33;
  private JXTitledSeparator OBJ_34;
  private JPanel panel1;
  private XRiCheckBox WTOUM;
  private JPanel panel5;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPanel panel2;
  private XRiCheckBox WTOUFA;
  private JPanel panel3;
  private XRiCheckBox WTOUA;
  private JPanel panel7;
  private JLabel OBJ_63;
  private XRiTextField EDEBA;
  private JLabel OBJ_62;
  private XRiTextField EFINA;
  private JPanel panel4;
  private JLabel OBJ_41;
  private XRiCalendrier DATDEB;
  private JLabel OBJ_43;
  private XRiCalendrier DATFIN;
  private JPanel panel6;
  private JLabel OBJ_58;
  private XRiTextField EDEB;
  private JLabel OBJ_60;
  private XRiTextField EFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
