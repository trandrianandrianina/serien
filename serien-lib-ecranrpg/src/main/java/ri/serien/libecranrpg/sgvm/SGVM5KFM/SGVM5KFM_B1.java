
package ri.serien.libecranrpg.sgvm.SGVM5KFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM5KFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORT = "Exportation tableur";
  
  public SGVM5KFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    
    // - Ajouter un bouton personnalisé
    snBarreBouton.ajouterBouton(BOUTON_EXPORT, 'e', true);
    // -
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Composant snPlageDate
    snPlageDatesBons.setDefilement(EnumDefilementPlageDate.SANS_DEFILEMENT);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    
    pnlSelectionPlageClients.setVisible(!WTOU.isSelected());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charger composant snPlageDate
    snPlageDatesBons.setDateDebutParChampRPG(lexique, "PERDEB");
    snPlageDatesBons.setDateFinParChampRPG(lexique, "PERFIN");
    
    chargerComposantClients();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Composant snPlageDate
    snPlageDatesBons.renseignerChampRPGDebut(lexique, "PERDEB");
    snPlageDatesBons.renseignerChampRPGFin(lexique, "PERFIN");
    
    snClientDebut.renseignerChampRPG(lexique, "CLIDEB");
    snClientFin.renseignerChampRPG(lexique, "CLIFIN");
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORT)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chargerComposantClients() {
    snClientDebut.setSession(getSession());
    snClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snClientDebut.charger(false);
    snClientDebut.setSelectionParChampRPG(lexique, "CLIDEB");
    
    snClientFin.setSession(getSession());
    snClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snClientFin.charger(false);
    snClientFin.setSelectionParChampRPG(lexique, "CLIFIN");
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    pnlSelectionPlageClients.setVisible(!pnlSelectionPlageClients.isVisible());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantClients();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlPlageDatesBons = new SNPanelTitre();
    lbPlageDatesBons = new SNLabelChamp();
    snPlageDatesBons = new SNPlageDate();
    pnlPlageClients = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    pnlSelectionPlageClients = new SNPanel();
    lbClientDebut = new SNLabelChamp();
    snClientDebut = new SNClientPrincipal();
    lbClientFin = new SNLabelChamp();
    snClientFin = new SNClientPrincipal();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlPlageDatesBons ========
        {
          pnlPlageDatesBons.setTitre("Plage de dates \u00e0 traiter");
          pnlPlageDatesBons.setName("pnlPlageDatesBons");
          pnlPlageDatesBons.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbPlageDatesBons ----
          lbPlageDatesBons.setText("P\u00e9riode \u00e0 \u00e9diter");
          lbPlageDatesBons.setName("lbPlageDatesBons");
          pnlPlageDatesBons.add(lbPlageDatesBons, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snPlageDatesBons ----
          snPlageDatesBons.setName("snPlageDatesBons");
          pnlPlageDatesBons.add(snPlageDatesBons, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageDatesBons, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlPlageClients ========
        {
          pnlPlageClients.setTitre("Plage des clients \u00e0 traiter");
          pnlPlageClients.setName("pnlPlageClients");
          pnlPlageClients.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageClients.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlPlageClients.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageClients.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageClients.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMaximumSize(new Dimension(45, 30));
          WTOU.setMinimumSize(new Dimension(45, 30));
          WTOU.setName("WTOU");
          WTOU.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTOUItemStateChanged(e);
            }
          });
          pnlPlageClients.add(WTOU, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionPlageClients ========
          {
            pnlSelectionPlageClients.setName("pnlSelectionPlageClients");
            pnlSelectionPlageClients.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionPlageClients.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbClientDebut ----
            lbClientDebut.setText("Premier client");
            lbClientDebut.setName("lbClientDebut");
            pnlSelectionPlageClients.add(lbClientDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snClientDebut ----
            snClientDebut.setName("snClientDebut");
            pnlSelectionPlageClients.add(snClientDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbClientFin ----
            lbClientFin.setText("Dernier client");
            lbClientFin.setName("lbClientFin");
            pnlSelectionPlageClients.add(lbClientFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snClientFin ----
            snClientFin.setName("snClientFin");
            pnlSelectionPlageClients.add(snClientFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageClients.add(pnlSelectionPlageClients, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageClients, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setEditable(false);
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPlageDatesBons;
  private SNLabelChamp lbPlageDatesBons;
  private SNPlageDate snPlageDatesBons;
  private SNPanelTitre pnlPlageClients;
  private XRiCheckBox WTOU;
  private SNPanel pnlSelectionPlageClients;
  private SNLabelChamp lbClientDebut;
  private SNClientPrincipal snClientDebut;
  private SNLabelChamp lbClientFin;
  private SNClientPrincipal snClientFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
