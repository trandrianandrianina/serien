
package ri.serien.libecranrpg.sgvm.SGVMCMFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMCMFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CCDAP3_Value = { "OUI", "NON", };
  private String[] CCSLIV_Value = { "OUI", "NON", };
  private String[] CCSPRI_Value = { "OUI", "NON", };
  private String[] CCVCAF_Value = { "", "B", "H", };
  
  public SGVMCMFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CCDAP3.setValeurs(CCDAP3_Value, null);
    CCSLIV.setValeurs(CCSLIV_Value, null);
    CCSPRI.setValeurs(CCSPRI_Value, null);
    CCVCAF.setValeurs(CCVCAF_Value, null);
    CCDMAI.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    CCGRPF.setEnabled(lexique.isPresent("CCGRPF"));
    CCGRPD.setEnabled(lexique.isPresent("CCGRPD"));
    CCDZC5.setEnabled(lexique.isPresent("CCDZC5"));
    CCDZC4.setEnabled(lexique.isPresent("CCDZC4"));
    CCDZC3.setEnabled(lexique.isPresent("CCDZC3"));
    CCDZC2.setEnabled(lexique.isPresent("CCDZC2"));
    CCDZC1.setEnabled(lexique.isPresent("CCDZC1"));
    CCDRP5.setEnabled(lexique.isPresent("CCDRP5"));
    CCDRP4.setEnabled(lexique.isPresent("CCDRP4"));
    CCDRP3.setEnabled(lexique.isPresent("CCDRP3"));
    CCDRP2.setEnabled(lexique.isPresent("CCDRP2"));
    CCDRP1.setEnabled(lexique.isPresent("CCDRP1"));
    CCDRPF.setEnabled(lexique.isPresent("CCDRPF"));
    CCDRPD.setEnabled(lexique.isPresent("CCDRPD"));
    CCSFAF.setEnabled(lexique.isPresent("CCSFAF"));
    CCFAMF.setEnabled(lexique.isPresent("CCFAMF"));
    CCSFAD.setEnabled(lexique.isPresent("CCSFAD"));
    CCFAMD.setEnabled(lexique.isPresent("CCFAMD"));
    CCLIVF.setEnabled(lexique.isPresent("CCLIVF"));
    CCLIVD.setEnabled(lexique.isPresent("CCLIVD"));
    CCDCA5.setEnabled(lexique.isPresent("CCDCA5"));
    CCDCA4.setEnabled(lexique.isPresent("CCDCA4"));
    CCDCA3.setEnabled(lexique.isPresent("CCDCA3"));
    CCDCA2.setEnabled(lexique.isPresent("CCDCA2"));
    CCDCAT.setEnabled(lexique.isPresent("CCDCAT"));
    CCPAY5.setEnabled(lexique.isPresent("CCPAY5"));
    CCPAY4.setEnabled(lexique.isPresent("CCPAY4"));
    CCPAY3.setEnabled(lexique.isPresent("CCPAY3"));
    CCPAY2.setEnabled(lexique.isPresent("CCPAY2"));
    CCPAY1.setEnabled(lexique.isPresent("CCPAY1"));
    CCPAYF.setEnabled(lexique.isPresent("CCPAYF"));
    CCPAYD.setEnabled(lexique.isPresent("CCPAYD"));
    CCDCT5.setEnabled(lexique.isPresent("CCDCT5"));
    CCDCT4.setEnabled(lexique.isPresent("CCDCT4"));
    CCDCT3.setEnabled(lexique.isPresent("CCDCT3"));
    CCDCT2.setEnabled(lexique.isPresent("CCDCT2"));
    CCDCT1.setEnabled(lexique.isPresent("CCDCT1"));
    CCDCTF.setEnabled(lexique.isPresent("CCDCTF"));
    CCDCTD.setEnabled(lexique.isPresent("CCDCTD"));
    
    CCPRFX.setEnabled(lexique.isPresent("CCPRFX"));
    CCPRDX.setEnabled(lexique.isPresent("CCPRDX"));
    CCDCP5.setEnabled(lexique.isPresent("CCDCP5"));
    CCDCP4.setEnabled(lexique.isPresent("CCDCP4"));
    CCDCP3.setEnabled(lexique.isPresent("CCDCP3"));
    CCDCP2.setEnabled(lexique.isPresent("CCDCP2"));
    CCDCP1.setEnabled(lexique.isPresent("CCDCP1"));
    CCDCPF.setEnabled(lexique.isPresent("CCDCPF"));
    CCDCPD.setEnabled(lexique.isPresent("CCDCPD"));
    CCNAP5.setEnabled(lexique.isPresent("CCNAP5"));
    CCNAP4.setEnabled(lexique.isPresent("CCNAP4"));
    CCNAP3.setEnabled(lexique.isPresent("CCNAP3"));
    CCNAP2.setEnabled(lexique.isPresent("CCNAP2"));
    CCNAP1.setEnabled(lexique.isPresent("CCNAP1"));
    CCNAPF.setEnabled(lexique.isPresent("CCNAPF"));
    CCNAPD.setEnabled(lexique.isPresent("CCNAPD"));
    CCCLIF.setEnabled(lexique.isPresent("CCCLIF"));
    CCCLID.setEnabled(lexique.isPresent("CCCLID"));
    CCDZG5.setEnabled(lexique.isPresent("CCDZG5"));
    CCDZG4.setEnabled(lexique.isPresent("CCDZG4"));
    CCDZG3.setEnabled(lexique.isPresent("CCDZG3"));
    CCDZG2.setEnabled(lexique.isPresent("CCDZG2"));
    CCDZG1.setEnabled(lexique.isPresent("CCDZG1"));
    CCDZGF.setEnabled(lexique.isPresent("CCDZGF"));
    CCDZGD.setEnabled(lexique.isPresent("CCDZGD"));
    CCDCAF.setEnabled(lexique.isPresent("CCDCAF"));
    CCDCAD.setEnabled(lexique.isPresent("CCDCAD"));
    // CCDAP3.setEnabled( lexique.isPresent("CCDAP3"));
    // CCSLIV.setEnabled( lexique.isPresent("CCSLIV"));
    // CCSPRI.setEnabled( lexique.isPresent("CCSPRI"));
    // CCDMAI.setEnabled( lexique.isPresent("CCDMAI"));
    // CCDMAI.setSelected(lexique.HostFieldGetData("CCDMAI").equalsIgnoreCase("OUI"));
    
    // CCVCAF.setEnabled( lexique.isPresent("CCVCAF"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@ @TITPG2@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("CCDAP3", 0, CCDAP3_Value[CCDAP3.getSelectedIndex()]);
    // lexique.HostFieldPutData("CCSLIV", 0, CCSLIV_Value[CCSLIV.getSelectedIndex()]);
    // lexique.HostFieldPutData("CCSPRI", 0, CCSPRI_Value[CCSPRI.getSelectedIndex()]);
    // if (CCDMAI.isSelected())
    // lexique.HostFieldPutData("CCDMAI", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CCDMAI", 0, "NON");
    // lexique.HostFieldPutData("CCVCAF", 0, CCVCAF_Value[CCVCAF.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    separator1 = compFactory.createSeparator("Crit\u00e8res de s\u00e9lection");
    separator2 = compFactory.createSeparator("Contact");
    CCVCAF = new XRiComboBox();
    OBJ_135 = new JLabel();
    OBJ_127 = new JLabel();
    CCSPRI = new XRiComboBox();
    CCSLIV = new XRiComboBox();
    CCDAP3 = new XRiComboBox();
    OBJ_131 = new JLabel();
    OBJ_149 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_76 = new JLabel();
    CCDCAD = new XRiTextField();
    CCDCAF = new XRiTextField();
    CCDZGD = new XRiTextField();
    CCDZGF = new XRiTextField();
    CCDZG1 = new XRiTextField();
    CCDZG2 = new XRiTextField();
    CCDZG3 = new XRiTextField();
    CCDZG4 = new XRiTextField();
    CCDZG5 = new XRiTextField();
    CCCLIF = new XRiTextField();
    CCNAPD = new XRiTextField();
    CCNAPF = new XRiTextField();
    CCNAP1 = new XRiTextField();
    CCNAP2 = new XRiTextField();
    CCNAP3 = new XRiTextField();
    CCNAP4 = new XRiTextField();
    CCNAP5 = new XRiTextField();
    CCDCPD = new XRiTextField();
    CCDCPF = new XRiTextField();
    CCDCP1 = new XRiTextField();
    CCDCP2 = new XRiTextField();
    CCDCP3 = new XRiTextField();
    CCDCP4 = new XRiTextField();
    CCDCP5 = new XRiTextField();
    CCPRDX = new XRiTextField();
    CCPRFX = new XRiTextField();
    CCDCTD = new XRiTextField();
    CCDCTF = new XRiTextField();
    CCDCT1 = new XRiTextField();
    CCDCT2 = new XRiTextField();
    CCDCT3 = new XRiTextField();
    CCDCT4 = new XRiTextField();
    CCDCT5 = new XRiTextField();
    CCPAYD = new XRiTextField();
    CCPAYF = new XRiTextField();
    CCPAY1 = new XRiTextField();
    CCPAY2 = new XRiTextField();
    CCPAY3 = new XRiTextField();
    CCPAY4 = new XRiTextField();
    CCPAY5 = new XRiTextField();
    CCLIVD = new XRiTextField();
    CCLIVF = new XRiTextField();
    CCFAMD = new XRiTextField();
    CCSFAD = new XRiTextField();
    CCFAMF = new XRiTextField();
    CCSFAF = new XRiTextField();
    CCDRPD = new XRiTextField();
    CCDRPF = new XRiTextField();
    CCDRP1 = new XRiTextField();
    CCDRP2 = new XRiTextField();
    CCDRP3 = new XRiTextField();
    CCDRP4 = new XRiTextField();
    CCDRP5 = new XRiTextField();
    CCDZC1 = new XRiTextField();
    CCDZC2 = new XRiTextField();
    CCDZC3 = new XRiTextField();
    CCDZC4 = new XRiTextField();
    CCDZC5 = new XRiTextField();
    OBJ_78 = new JLabel();
    CCGRPD = new XRiTextField();
    CCGRPF = new XRiTextField();
    OBJ_121 = new JLabel();
    OBJ_133 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_129 = new JLabel();
    CCCLID = new XRiTextField();
    CCDMAI = new XRiCheckBox();
    OBJ_146 = new JLabel();
    CCDCAT = new XRiTextField();
    CCDCA2 = new XRiTextField();
    CCDCA3 = new XRiTextField();
    CCDCA4 = new XRiTextField();
    CCDCA5 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Extentions matricielles");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- separator1 ----
          separator1.setName("separator1");

          //---- separator2 ----
          separator2.setName("separator2");

          //---- CCVCAF ----
          CCVCAF.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Baisse",
            "Hausse"
          }));
          CCVCAF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCVCAF.setName("CCVCAF");

          //---- OBJ_135 ----
          OBJ_135.setText("Variation par rapport \u00e0 l'exercice pr\u00e9cedent");
          OBJ_135.setName("OBJ_135");

          //---- OBJ_127 ----
          OBJ_127.setText("Groupe / famille / sous-famille");
          OBJ_127.setName("OBJ_127");

          //---- CCSPRI ----
          CCSPRI.setModel(new DefaultComboBoxModel(new String[] {
            "OUI",
            "NON"
          }));
          CCSPRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCSPRI.setName("CCSPRI");

          //---- CCSLIV ----
          CCSLIV.setModel(new DefaultComboBoxModel(new String[] {
            "OUI",
            "NON"
          }));
          CCSLIV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCSLIV.setName("CCSLIV");

          //---- CCDAP3 ----
          CCDAP3.setModel(new DefaultComboBoxModel(new String[] {
            "OUI",
            "NON"
          }));
          CCDAP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCDAP3.setName("CCDAP3");

          //---- OBJ_131 ----
          OBJ_131.setText("P\u00e9riode d'analyse de");
          OBJ_131.setName("OBJ_131");

          //---- OBJ_149 ----
          OBJ_149.setText("Adresse centrale");
          OBJ_149.setName("OBJ_149");

          //---- OBJ_128 ----
          OBJ_128.setText("Chiffre d'affaires");
          OBJ_128.setName("OBJ_128");

          //---- OBJ_53 ----
          OBJ_53.setText("Clts principaux");
          OBJ_53.setName("OBJ_53");

          //---- OBJ_67 ----
          OBJ_67.setText("Code Pays");
          OBJ_67.setName("OBJ_67");

          //---- OBJ_55 ----
          OBJ_55.setText("Clients livr\u00e9s");
          OBJ_55.setName("OBJ_55");

          //---- OBJ_112 ----
          OBJ_112.setText("Zones perso");
          OBJ_112.setName("OBJ_112");

          //---- OBJ_103 ----
          OBJ_103.setText("Repr\u00e9sentant");
          OBJ_103.setName("OBJ_103");

          //---- OBJ_58 ----
          OBJ_58.setText("Code postal");
          OBJ_58.setName("OBJ_58");

          //---- OBJ_47 ----
          OBJ_47.setText("N\u00b0 client");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_102 ----
          OBJ_102.setText("Code Ape");
          OBJ_102.setName("OBJ_102");

          //---- OBJ_86 ----
          OBJ_86.setText("Cat\u00e9gorie");
          OBJ_86.setName("OBJ_86");

          //---- OBJ_76 ----
          OBJ_76.setText("Zone g\u00e9o");
          OBJ_76.setName("OBJ_76");

          //---- CCDCAD ----
          CCDCAD.setComponentPopupMenu(BTD);
          CCDCAD.setName("CCDCAD");

          //---- CCDCAF ----
          CCDCAF.setComponentPopupMenu(BTD);
          CCDCAF.setName("CCDCAF");

          //---- CCDZGD ----
          CCDZGD.setComponentPopupMenu(BTD);
          CCDZGD.setName("CCDZGD");

          //---- CCDZGF ----
          CCDZGF.setComponentPopupMenu(BTD);
          CCDZGF.setName("CCDZGF");

          //---- CCDZG1 ----
          CCDZG1.setComponentPopupMenu(BTD);
          CCDZG1.setName("CCDZG1");

          //---- CCDZG2 ----
          CCDZG2.setComponentPopupMenu(BTD);
          CCDZG2.setName("CCDZG2");

          //---- CCDZG3 ----
          CCDZG3.setComponentPopupMenu(BTD);
          CCDZG3.setName("CCDZG3");

          //---- CCDZG4 ----
          CCDZG4.setComponentPopupMenu(BTD);
          CCDZG4.setName("CCDZG4");

          //---- CCDZG5 ----
          CCDZG5.setComponentPopupMenu(BTD);
          CCDZG5.setName("CCDZG5");

          //---- CCCLIF ----
          CCCLIF.setComponentPopupMenu(BTD);
          CCCLIF.setName("CCCLIF");

          //---- CCNAPD ----
          CCNAPD.setComponentPopupMenu(BTD);
          CCNAPD.setName("CCNAPD");

          //---- CCNAPF ----
          CCNAPF.setComponentPopupMenu(BTD);
          CCNAPF.setName("CCNAPF");

          //---- CCNAP1 ----
          CCNAP1.setComponentPopupMenu(BTD);
          CCNAP1.setName("CCNAP1");

          //---- CCNAP2 ----
          CCNAP2.setComponentPopupMenu(BTD);
          CCNAP2.setName("CCNAP2");

          //---- CCNAP3 ----
          CCNAP3.setComponentPopupMenu(BTD);
          CCNAP3.setName("CCNAP3");

          //---- CCNAP4 ----
          CCNAP4.setComponentPopupMenu(BTD);
          CCNAP4.setName("CCNAP4");

          //---- CCNAP5 ----
          CCNAP5.setComponentPopupMenu(BTD);
          CCNAP5.setName("CCNAP5");

          //---- CCDCPD ----
          CCDCPD.setComponentPopupMenu(BTD);
          CCDCPD.setName("CCDCPD");

          //---- CCDCPF ----
          CCDCPF.setComponentPopupMenu(BTD);
          CCDCPF.setName("CCDCPF");

          //---- CCDCP1 ----
          CCDCP1.setComponentPopupMenu(BTD);
          CCDCP1.setName("CCDCP1");

          //---- CCDCP2 ----
          CCDCP2.setComponentPopupMenu(BTD);
          CCDCP2.setName("CCDCP2");

          //---- CCDCP3 ----
          CCDCP3.setComponentPopupMenu(BTD);
          CCDCP3.setName("CCDCP3");

          //---- CCDCP4 ----
          CCDCP4.setComponentPopupMenu(BTD);
          CCDCP4.setName("CCDCP4");

          //---- CCDCP5 ----
          CCDCP5.setComponentPopupMenu(BTD);
          CCDCP5.setName("CCDCP5");

          //---- CCPRDX ----
          CCPRDX.setToolTipText("mm.aa");
          CCPRDX.setComponentPopupMenu(BTD);
          CCPRDX.setName("CCPRDX");

          //---- CCPRFX ----
          CCPRFX.setToolTipText("mm.aa");
          CCPRFX.setComponentPopupMenu(BTD);
          CCPRFX.setName("CCPRFX");

          //---- CCDCTD ----
          CCDCTD.setComponentPopupMenu(BTD);
          CCDCTD.setName("CCDCTD");

          //---- CCDCTF ----
          CCDCTF.setComponentPopupMenu(BTD);
          CCDCTF.setName("CCDCTF");

          //---- CCDCT1 ----
          CCDCT1.setComponentPopupMenu(BTD);
          CCDCT1.setName("CCDCT1");

          //---- CCDCT2 ----
          CCDCT2.setComponentPopupMenu(BTD);
          CCDCT2.setName("CCDCT2");

          //---- CCDCT3 ----
          CCDCT3.setComponentPopupMenu(BTD);
          CCDCT3.setName("CCDCT3");

          //---- CCDCT4 ----
          CCDCT4.setComponentPopupMenu(BTD);
          CCDCT4.setName("CCDCT4");

          //---- CCDCT5 ----
          CCDCT5.setComponentPopupMenu(BTD);
          CCDCT5.setName("CCDCT5");

          //---- CCPAYD ----
          CCPAYD.setComponentPopupMenu(BTD);
          CCPAYD.setName("CCPAYD");

          //---- CCPAYF ----
          CCPAYF.setComponentPopupMenu(BTD);
          CCPAYF.setName("CCPAYF");

          //---- CCPAY1 ----
          CCPAY1.setComponentPopupMenu(BTD);
          CCPAY1.setName("CCPAY1");

          //---- CCPAY2 ----
          CCPAY2.setComponentPopupMenu(BTD);
          CCPAY2.setName("CCPAY2");

          //---- CCPAY3 ----
          CCPAY3.setComponentPopupMenu(BTD);
          CCPAY3.setName("CCPAY3");

          //---- CCPAY4 ----
          CCPAY4.setComponentPopupMenu(BTD);
          CCPAY4.setName("CCPAY4");

          //---- CCPAY5 ----
          CCPAY5.setComponentPopupMenu(BTD);
          CCPAY5.setName("CCPAY5");

          //---- CCLIVD ----
          CCLIVD.setComponentPopupMenu(BTD);
          CCLIVD.setName("CCLIVD");

          //---- CCLIVF ----
          CCLIVF.setComponentPopupMenu(BTD);
          CCLIVF.setName("CCLIVF");

          //---- CCFAMD ----
          CCFAMD.setComponentPopupMenu(BTD);
          CCFAMD.setName("CCFAMD");

          //---- CCSFAD ----
          CCSFAD.setComponentPopupMenu(BTD);
          CCSFAD.setName("CCSFAD");

          //---- CCFAMF ----
          CCFAMF.setComponentPopupMenu(BTD);
          CCFAMF.setName("CCFAMF");

          //---- CCSFAF ----
          CCSFAF.setComponentPopupMenu(BTD);
          CCSFAF.setName("CCSFAF");

          //---- CCDRPD ----
          CCDRPD.setComponentPopupMenu(BTD);
          CCDRPD.setName("CCDRPD");

          //---- CCDRPF ----
          CCDRPF.setComponentPopupMenu(BTD);
          CCDRPF.setName("CCDRPF");

          //---- CCDRP1 ----
          CCDRP1.setComponentPopupMenu(BTD);
          CCDRP1.setName("CCDRP1");

          //---- CCDRP2 ----
          CCDRP2.setComponentPopupMenu(BTD);
          CCDRP2.setName("CCDRP2");

          //---- CCDRP3 ----
          CCDRP3.setComponentPopupMenu(BTD);
          CCDRP3.setName("CCDRP3");

          //---- CCDRP4 ----
          CCDRP4.setComponentPopupMenu(BTD);
          CCDRP4.setName("CCDRP4");

          //---- CCDRP5 ----
          CCDRP5.setComponentPopupMenu(BTD);
          CCDRP5.setName("CCDRP5");

          //---- CCDZC1 ----
          CCDZC1.setComponentPopupMenu(BTD);
          CCDZC1.setName("CCDZC1");

          //---- CCDZC2 ----
          CCDZC2.setComponentPopupMenu(BTD);
          CCDZC2.setName("CCDZC2");

          //---- CCDZC3 ----
          CCDZC3.setComponentPopupMenu(BTD);
          CCDZC3.setName("CCDZC3");

          //---- CCDZC4 ----
          CCDZC4.setComponentPopupMenu(BTD);
          CCDZC4.setName("CCDZC4");

          //---- CCDZC5 ----
          CCDZC5.setComponentPopupMenu(BTD);
          CCDZC5.setName("CCDZC5");

          //---- OBJ_78 ----
          OBJ_78.setText("/");
          OBJ_78.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_78.setName("OBJ_78");

          //---- CCGRPD ----
          CCGRPD.setComponentPopupMenu(BTD);
          CCGRPD.setName("CCGRPD");

          //---- CCGRPF ----
          CCGRPF.setComponentPopupMenu(BTD);
          CCGRPF.setName("CCGRPF");

          //---- OBJ_121 ----
          OBJ_121.setText("\u00e0");
          OBJ_121.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_121.setName("OBJ_121");

          //---- OBJ_133 ----
          OBJ_133.setText("\u00e0");
          OBJ_133.setName("OBJ_133");

          //---- OBJ_50 ----
          OBJ_50.setText("/");
          OBJ_50.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_50.setName("OBJ_50");

          //---- OBJ_60 ----
          OBJ_60.setText("/");
          OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_60.setName("OBJ_60");

          //---- OBJ_69 ----
          OBJ_69.setText("/");
          OBJ_69.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_69.setName("OBJ_69");

          //---- OBJ_85 ----
          OBJ_85.setText("/");
          OBJ_85.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_85.setName("OBJ_85");

          //---- OBJ_94 ----
          OBJ_94.setText("/");
          OBJ_94.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_94.setName("OBJ_94");

          //---- OBJ_105 ----
          OBJ_105.setText("/");
          OBJ_105.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_105.setName("OBJ_105");

          //---- OBJ_129 ----
          OBJ_129.setText("/");
          OBJ_129.setName("OBJ_129");

          //---- CCCLID ----
          CCCLID.setComponentPopupMenu(BTD);
          CCCLID.setName("CCCLID");

          //---- CCDMAI ----
          CCDMAI.setText("Adresse email obligatoire");
          CCDMAI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCDMAI.setName("CCDMAI");

          //---- OBJ_146 ----
          OBJ_146.setText("Fonction");
          OBJ_146.setName("OBJ_146");

          //---- CCDCAT ----
          CCDCAT.setComponentPopupMenu(BTD);
          CCDCAT.setName("CCDCAT");

          //---- CCDCA2 ----
          CCDCA2.setComponentPopupMenu(BTD);
          CCDCA2.setName("CCDCA2");

          //---- CCDCA3 ----
          CCDCA3.setComponentPopupMenu(BTD);
          CCDCA3.setName("CCDCA3");

          //---- CCDCA4 ----
          CCDCA4.setComponentPopupMenu(BTD);
          CCDCA4.setName("CCDCA4");

          //---- CCDCA5 ----
          CCDCA5.setComponentPopupMenu(BTD);
          CCDCA5.setName("CCDCA5");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addComponent(CCCLID, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(CCLIVD, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(CCCLIF, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(CCLIVF, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(CCSPRI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(CCSLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79)
                .addComponent(CCDCPD, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(CCDCPF, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CCDCP1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CCDCP2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CCDCP3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CCDCP4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CCDCP5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67)
                .addComponent(CCPAYD, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(CCPAYF, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCPAY1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCPAY2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCPAY3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCPAY4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCPAY5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                .addGap(94, 94, 94)
                .addComponent(CCDZGD, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(CCDZGF, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDZG1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDZG2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDZG3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDZG4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDZG5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
                .addGap(92, 92, 92)
                .addComponent(CCDCTD, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(CCDCTF, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCDCT1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCDCT2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCDCT3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCDCT4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCDCT5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(CCNAPD, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(OBJ_94, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(CCNAPF, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCNAP1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCNAP2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCNAP3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCNAP4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCNAP5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_103, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addComponent(CCDRPD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(CCDRPF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(CCDRP1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(CCDRP2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(CCDRP3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(CCDRP4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(CCDRP5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(CCDZC1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(CCDZC2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(CCDZC3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(CCDZC4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(CCDZC5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_127, GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE)
                .addGap(76, 76, 76)
                .addComponent(CCGRPD, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCFAMD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCSFAD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_121, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(CCGRPF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCFAMF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCSFAF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(OBJ_149, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(CCDAP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(CCDCAD, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_129, GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(CCDCAF, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_131, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(CCPRDX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_133, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(CCPRFX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                .addGap(115, 115, 115)
                .addComponent(CCVCAF, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_146, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addComponent(CCDCAT, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDCA2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDCA3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDCA4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(CCDCA5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(CCDMAI, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCCLID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCLIVD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCCLIF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCLIVF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCSPRI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCSLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDCPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDCPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCPAYD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCPAYF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCPAY1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCPAY2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCPAY3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCPAY4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCPAY5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDZGD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDZGF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZG1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZG2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZG3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZG4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZG5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDCTD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDCTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCNAPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_94, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCNAPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCNAP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCNAP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCNAP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCNAP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCNAP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_103, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDRPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDRPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDRP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDRP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDRP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDRP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDRP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDZC1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZC2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZC3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZC4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDZC5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_127, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCGRPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCFAMD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCSFAD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_121, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCGRPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCFAMF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCSFAF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_149, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDAP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDCAD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_129, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDCAF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_131, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCPRDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_133, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCPRFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCVCAF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addComponent(separator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_146, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CCDCAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCA4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CCDCA5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(CCDMAI, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JComponent separator1;
  private JComponent separator2;
  private XRiComboBox CCVCAF;
  private JLabel OBJ_135;
  private JLabel OBJ_127;
  private XRiComboBox CCSPRI;
  private XRiComboBox CCSLIV;
  private XRiComboBox CCDAP3;
  private JLabel OBJ_131;
  private JLabel OBJ_149;
  private JLabel OBJ_128;
  private JLabel OBJ_53;
  private JLabel OBJ_67;
  private JLabel OBJ_55;
  private JLabel OBJ_112;
  private JLabel OBJ_103;
  private JLabel OBJ_58;
  private JLabel OBJ_47;
  private JLabel OBJ_102;
  private JLabel OBJ_86;
  private JLabel OBJ_76;
  private XRiTextField CCDCAD;
  private XRiTextField CCDCAF;
  private XRiTextField CCDZGD;
  private XRiTextField CCDZGF;
  private XRiTextField CCDZG1;
  private XRiTextField CCDZG2;
  private XRiTextField CCDZG3;
  private XRiTextField CCDZG4;
  private XRiTextField CCDZG5;
  private XRiTextField CCCLIF;
  private XRiTextField CCNAPD;
  private XRiTextField CCNAPF;
  private XRiTextField CCNAP1;
  private XRiTextField CCNAP2;
  private XRiTextField CCNAP3;
  private XRiTextField CCNAP4;
  private XRiTextField CCNAP5;
  private XRiTextField CCDCPD;
  private XRiTextField CCDCPF;
  private XRiTextField CCDCP1;
  private XRiTextField CCDCP2;
  private XRiTextField CCDCP3;
  private XRiTextField CCDCP4;
  private XRiTextField CCDCP5;
  private XRiTextField CCPRDX;
  private XRiTextField CCPRFX;
  private XRiTextField CCDCTD;
  private XRiTextField CCDCTF;
  private XRiTextField CCDCT1;
  private XRiTextField CCDCT2;
  private XRiTextField CCDCT3;
  private XRiTextField CCDCT4;
  private XRiTextField CCDCT5;
  private XRiTextField CCPAYD;
  private XRiTextField CCPAYF;
  private XRiTextField CCPAY1;
  private XRiTextField CCPAY2;
  private XRiTextField CCPAY3;
  private XRiTextField CCPAY4;
  private XRiTextField CCPAY5;
  private XRiTextField CCLIVD;
  private XRiTextField CCLIVF;
  private XRiTextField CCFAMD;
  private XRiTextField CCSFAD;
  private XRiTextField CCFAMF;
  private XRiTextField CCSFAF;
  private XRiTextField CCDRPD;
  private XRiTextField CCDRPF;
  private XRiTextField CCDRP1;
  private XRiTextField CCDRP2;
  private XRiTextField CCDRP3;
  private XRiTextField CCDRP4;
  private XRiTextField CCDRP5;
  private XRiTextField CCDZC1;
  private XRiTextField CCDZC2;
  private XRiTextField CCDZC3;
  private XRiTextField CCDZC4;
  private XRiTextField CCDZC5;
  private JLabel OBJ_78;
  private XRiTextField CCGRPD;
  private XRiTextField CCGRPF;
  private JLabel OBJ_121;
  private JLabel OBJ_133;
  private JLabel OBJ_50;
  private JLabel OBJ_60;
  private JLabel OBJ_69;
  private JLabel OBJ_85;
  private JLabel OBJ_94;
  private JLabel OBJ_105;
  private JLabel OBJ_129;
  private XRiTextField CCCLID;
  private XRiCheckBox CCDMAI;
  private JLabel OBJ_146;
  private XRiTextField CCDCAT;
  private XRiTextField CCDCA2;
  private XRiTextField CCDCA3;
  private XRiTextField CCDCA4;
  private XRiTextField CCDCA5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
