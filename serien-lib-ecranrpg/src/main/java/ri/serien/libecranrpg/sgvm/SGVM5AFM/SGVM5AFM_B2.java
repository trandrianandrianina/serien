
package ri.serien.libecranrpg.sgvm.SGVM5AFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SGVM5AFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM5AFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TREP2.setValeurs("4", "RB2");
    TNCL2.setValeurs("3", "RB2");
    TNFA2.setValeurs("2", "RB2");
    TGFA2.setValeurs("1", "RB2");
    TREP1.setValeurs("4", "RB1");
    TNCL1.setValeurs("3", "RB1");
    TNFA1.setValeurs("2", "RB1");
    TGFA1.setValeurs("1", "RB1");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    // TREP2.setSelected(lexique.HostFieldGetData("RB2").equalsIgnoreCase("4"));
    // TREP1.setSelected(lexique.HostFieldGetData("RB1").equalsIgnoreCase("4"));
    // TNCL2.setSelected(lexique.HostFieldGetData("RB2").equalsIgnoreCase("3"));
    // TNCL1.setSelected(lexique.HostFieldGetData("RB1").equalsIgnoreCase("3"));
    // TNFA2.setSelected(lexique.HostFieldGetData("RB2").equalsIgnoreCase("2"));
    // TNFA1.setSelected(lexique.HostFieldGetData("RB1").equalsIgnoreCase("2"));
    // TGFA2.setSelected(lexique.HostFieldGetData("RB2").equalsIgnoreCase("1"));
    // TGFA1.setSelected(lexique.HostFieldGetData("RB1").equalsIgnoreCase("1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Tri et sélection"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TREP2.isSelected())
    // lexique.HostFieldPutData("RB2", 0, "4");
    // if (TREP1.isSelected())
    // lexique.HostFieldPutData("RB1", 0, "4");
    // if (TNCL2.isSelected())
    // lexique.HostFieldPutData("RB2", 0, "3");
    // if (TNCL1.isSelected())
    // lexique.HostFieldPutData("RB1", 0, "3");
    // if (TNFA2.isSelected())
    // lexique.HostFieldPutData("RB2", 0, "2");
    // if (TNFA1.isSelected())
    // lexique.HostFieldPutData("RB1", 0, "2");
    // if (TGFA2.isSelected())
    // lexique.HostFieldPutData("RB2", 0, "1");
    // if (TGFA1.isSelected())
    // lexique.HostFieldPutData("RB1", 0, "1");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_6 = new JPanel();
    TGFA1 = new XRiRadioButton();
    TNFA1 = new XRiRadioButton();
    TNCL1 = new XRiRadioButton();
    TREP1 = new XRiRadioButton();
    OBJ_7 = new JPanel();
    TGFA2 = new XRiRadioButton();
    TNFA2 = new XRiRadioButton();
    TNCL2 = new XRiRadioButton();
    TREP2 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(630, 205));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== OBJ_6 ========
        {
          OBJ_6.setBorder(new TitledBorder("Premier niveau de tri"));
          OBJ_6.setOpaque(false);
          OBJ_6.setName("OBJ_6");
          OBJ_6.setLayout(null);

          //---- TGFA1 ----
          TGFA1.setText("Par groupe / famille");
          TGFA1.setComponentPopupMenu(null);
          TGFA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TGFA1.setName("TGFA1");
          OBJ_6.add(TGFA1);
          TGFA1.setBounds(19, 35, 163, TGFA1.getPreferredSize().height);

          //---- TNFA1 ----
          TNFA1.setText("Par num\u00e9ro de factures");
          TNFA1.setComponentPopupMenu(null);
          TNFA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TNFA1.setName("TNFA1");
          OBJ_6.add(TNFA1);
          TNFA1.setBounds(19, 65, 163, TNFA1.getPreferredSize().height);

          //---- TNCL1 ----
          TNCL1.setText("Par nom client");
          TNCL1.setComponentPopupMenu(null);
          TNCL1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TNCL1.setName("TNCL1");
          OBJ_6.add(TNCL1);
          TNCL1.setBounds(19, 95, 163, TNCL1.getPreferredSize().height);

          //---- TREP1 ----
          TREP1.setText("Par repr\u00e9sentant");
          TREP1.setComponentPopupMenu(null);
          TREP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TREP1.setName("TREP1");
          OBJ_6.add(TREP1);
          TREP1.setBounds(19, 125, 163, TREP1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_6.getComponentCount(); i++) {
              Rectangle bounds = OBJ_6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_6.setMinimumSize(preferredSize);
            OBJ_6.setPreferredSize(preferredSize);
          }
        }

        //======== OBJ_7 ========
        {
          OBJ_7.setBorder(new TitledBorder("Deuxi\u00e8me niveau de tri"));
          OBJ_7.setOpaque(false);
          OBJ_7.setName("OBJ_7");
          OBJ_7.setLayout(null);

          //---- TGFA2 ----
          TGFA2.setText("Par groupe / famille");
          TGFA2.setComponentPopupMenu(null);
          TGFA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TGFA2.setName("TGFA2");
          OBJ_7.add(TGFA2);
          TGFA2.setBounds(19, 35, 163, TGFA2.getPreferredSize().height);

          //---- TNFA2 ----
          TNFA2.setText("Par num\u00e9ro de factures");
          TNFA2.setComponentPopupMenu(null);
          TNFA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TNFA2.setName("TNFA2");
          OBJ_7.add(TNFA2);
          TNFA2.setBounds(19, 65, 163, TNFA2.getPreferredSize().height);

          //---- TNCL2 ----
          TNCL2.setText("Par nom client");
          TNCL2.setComponentPopupMenu(null);
          TNCL2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TNCL2.setName("TNCL2");
          OBJ_7.add(TNCL2);
          TNCL2.setBounds(19, 95, 163, TNCL2.getPreferredSize().height);

          //---- TREP2 ----
          TREP2.setText("Par repr\u00e9sentant");
          TREP2.setComponentPopupMenu(null);
          TREP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TREP2.setName("TREP2");
          OBJ_7.add(TREP2);
          TREP2.setBounds(19, 125, 163, TREP2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_7.getComponentCount(); i++) {
              Rectangle bounds = OBJ_7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_7.setMinimumSize(preferredSize);
            OBJ_7.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
              .addGap(17, 17, 17)
              .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //---- RB1_GRP ----
    ButtonGroup RB1_GRP = new ButtonGroup();
    RB1_GRP.add(TGFA1);
    RB1_GRP.add(TNFA1);
    RB1_GRP.add(TNCL1);
    RB1_GRP.add(TREP1);

    //---- RB2_GRP ----
    ButtonGroup RB2_GRP = new ButtonGroup();
    RB2_GRP.add(TGFA2);
    RB2_GRP.add(TNFA2);
    RB2_GRP.add(TNCL2);
    RB2_GRP.add(TREP2);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel OBJ_6;
  private XRiRadioButton TGFA1;
  private XRiRadioButton TNFA1;
  private XRiRadioButton TNCL1;
  private XRiRadioButton TREP1;
  private JPanel OBJ_7;
  private XRiRadioButton TGFA2;
  private XRiRadioButton TNFA2;
  private XRiRadioButton TNCL2;
  private XRiRadioButton TREP2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
