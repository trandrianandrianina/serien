
package ri.serien.libecranrpg.sgvm.SGVM39FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM39FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  public SGVM39FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    RECART.setValeursSelection("O", "N");
    EDBLO.setValeursSelection("O", "N");
    CHFCTR.setValeursSelection("OUI", "NON");
    FACDIR.setValeursSelection("OUI", "NON");
    OPTTRI.setValeursSelection("OUI", "NON");
    AGSTAT.setValeursSelection("OUI", "NON");
    DETART.setValeursSelection("OUI", "NON");
    BONFAC.setValeursSelection("OUI", "NON");
    WTDAT.setValeursSelection("**", "  ");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    // Gére les erreurs automatiquement
    gererLesErreurs("19");
    

    
    // Visibiliter des composants
    pnlBandeau.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    pnlPeriode.setVisible(!WTDAT.isSelected());
    if (WTDAT.isSelected()) {
      lbDateDebut.setText("Période à traiter");
    }
    else {
      lbDateDebut.setText("Période à traiter du");
    }
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    initialiserComposantTransporteur();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snTransporteur.renseignerChampRPG(lexique, "TRS");
    if (lexique.HostFieldGetData("TRS").trim().isEmpty()) {
      lexique.HostFieldPutData("TRS", 0, "**");
    }
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initialiserComposantTransporteur() {
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(snEtablissement.getIdSelection());
    snTransporteur.setTousAutorise(true);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "TRS");
  }
  
  private void selectionCompletePeriodeActionPerformed(ActionEvent e) {
    pnlPeriode.setVisible(!pnlPeriode.isVisible());
    if (WTDAT.isSelected()) {
      lbDateDebut.setText("Période à traiter");
    }
    else {
      lbDateDebut.setText("Période à traiter du");
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      initialiserComposantTransporteur();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanelContenu();
    pnlBandeau = new SNPanel();
    lbLOCTP = new SNLabelChamp();
    pnlContenu = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCriteresDeSelection = new SNPanelTitre();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    lbDateDebut = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    pnlPeriode = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbDateFin = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    WTDAT = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    BONFAC = new XRiCheckBox();
    DETART = new XRiCheckBox();
    AGSTAT = new XRiCheckBox();
    OPTTRI = new XRiCheckBox();
    CHFCTR = new XRiCheckBox();
    EDBLO = new XRiCheckBox();
    RECART = new XRiCheckBox();
    FACDIR = new XRiCheckBox();
    pnlNombreExemplaire = new SNPanel();
    lbNombreExemplaire = new SNLabelChamp();
    NBEX = new XRiTextField();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(930, 715));
    setPreferredSize(new Dimension(930, 715));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      pnlNord.add(bpPresentation);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //======== pnlBandeau ========
      {
        pnlBandeau.setName("pnlBandeau");
        pnlBandeau.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlBandeau.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlBandeau.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlBandeau.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlBandeau.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 22));
        lbLOCTP.setPreferredSize(new Dimension(120, 22));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlBandeau.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlBandeau, BorderLayout.NORTH);

      //======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridLayout());

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlCriteresDeSelection ========
          {
            pnlCriteresDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresDeSelection.setName("pnlCriteresDeSelection");
            pnlCriteresDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlCriteresDeSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlCriteresDeSelection.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlCriteresDeSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlCriteresDeSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbTransporteur ----
            lbTransporteur.setText("Transporteur");
            lbTransporteur.setName("lbTransporteur");
            pnlCriteresDeSelection.add(lbTransporteur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snTransporteur ----
            snTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
            snTransporteur.setName("snTransporteur");
            pnlCriteresDeSelection.add(snTransporteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbDateDebut ----
            lbDateDebut.setText("P\u00e9riode \u00e0 traiter du");
            lbDateDebut.setName("lbDateDebut");
            pnlCriteresDeSelection.add(lbDateDebut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlPeriodeAEditer.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)pnlPeriodeAEditer.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlPeriodeAEditer.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlPeriodeAEditer.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //======== pnlPeriode ========
              {
                pnlPeriode.setOpaque(false);
                pnlPeriode.setName("pnlPeriode");
                pnlPeriode.setLayout(new GridBagLayout());
                ((GridBagLayout)pnlPeriode.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
                ((GridBagLayout)pnlPeriode.getLayout()).rowHeights = new int[] {0, 0};
                ((GridBagLayout)pnlPeriode.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
                ((GridBagLayout)pnlPeriode.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

                //---- PERDEB ----
                PERDEB.setMinimumSize(new Dimension(110, 30));
                PERDEB.setPreferredSize(new Dimension(110, 30));
                PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
                PERDEB.setName("PERDEB");
                pnlPeriode.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));

                //---- lbDateFin ----
                lbDateFin.setText("au");
                lbDateFin.setPreferredSize(new Dimension(20, 30));
                lbDateFin.setMinimumSize(new Dimension(20, 30));
                lbDateFin.setMaximumSize(new Dimension(20, 30));
                lbDateFin.setName("lbDateFin");
                pnlPeriode.add(lbDateFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));

                //---- PERFIN ----
                PERFIN.setMinimumSize(new Dimension(110, 30));
                PERFIN.setPreferredSize(new Dimension(110, 30));
                PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
                PERFIN.setName("PERFIN");
                pnlPeriode.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPeriodeAEditer.add(pnlPeriode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- WTDAT ----
              WTDAT.setText("S\u00e9lection compl\u00e8te");
              WTDAT.setComponentPopupMenu(null);
              WTDAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTDAT.setFont(new Font("sansserif", Font.PLAIN, 14));
              WTDAT.setPreferredSize(new Dimension(142, 30));
              WTDAT.setMinimumSize(new Dimension(142, 30));
              WTDAT.setName("WTDAT");
              WTDAT.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  selectionCompletePeriodeActionPerformed(e);
                }
              });
              pnlPeriodeAEditer.add(WTDAT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresDeSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setEditable(false);
            tfEnCours.setEnabled(false);
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlOptionEdition ========
          {
            pnlOptionEdition.setOpaque(false);
            pnlOptionEdition.setTitre("Options d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlOptionEdition.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlOptionEdition.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlOptionEdition.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlOptionEdition.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- BONFAC ----
            BONFAC.setText("Edition des bons d\u00e9j\u00e0 factur\u00e9s");
            BONFAC.setComponentPopupMenu(null);
            BONFAC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BONFAC.setFont(new Font("sansserif", Font.PLAIN, 14));
            BONFAC.setMinimumSize(new Dimension(213, 30));
            BONFAC.setPreferredSize(new Dimension(213, 30));
            BONFAC.setName("BONFAC");
            pnlOptionEdition.add(BONFAC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 10), 0, 0));

            //---- DETART ----
            DETART.setText("D\u00e9tail des lignes articles");
            DETART.setComponentPopupMenu(null);
            DETART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DETART.setFont(new Font("sansserif", Font.PLAIN, 14));
            DETART.setMinimumSize(new Dimension(175, 30));
            DETART.setPreferredSize(new Dimension(175, 30));
            DETART.setName("DETART");
            pnlOptionEdition.add(DETART, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 10), 0, 0));

            //---- AGSTAT ----
            AGSTAT.setText("Edition des articles non g\u00e9r\u00e9s en stats");
            AGSTAT.setComponentPopupMenu(null);
            AGSTAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AGSTAT.setFont(new Font("sansserif", Font.PLAIN, 14));
            AGSTAT.setMinimumSize(new Dimension(263, 30));
            AGSTAT.setPreferredSize(new Dimension(263, 30));
            AGSTAT.setName("AGSTAT");
            pnlOptionEdition.add(AGSTAT, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 10), 0, 0));

            //---- OPTTRI ----
            OPTTRI.setText("Edition non tri\u00e9e par client");
            OPTTRI.setComponentPopupMenu(null);
            OPTTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPTTRI.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPTTRI.setMinimumSize(new Dimension(187, 30));
            OPTTRI.setPreferredSize(new Dimension(187, 30));
            OPTTRI.setName("OPTTRI");
            pnlOptionEdition.add(OPTTRI, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 10), 0, 0));

            //---- CHFCTR ----
            CHFCTR.setText("Chiffrage du transport");
            CHFCTR.setComponentPopupMenu(null);
            CHFCTR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CHFCTR.setFont(new Font("sansserif", Font.PLAIN, 14));
            CHFCTR.setPreferredSize(new Dimension(161, 30));
            CHFCTR.setMinimumSize(new Dimension(161, 30));
            CHFCTR.setName("CHFCTR");
            pnlOptionEdition.add(CHFCTR, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 0), 0, 0));

            //---- EDBLO ----
            EDBLO.setText("Edition du bloc-notes du client");
            EDBLO.setComponentPopupMenu(null);
            EDBLO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDBLO.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDBLO.setPreferredSize(new Dimension(212, 30));
            EDBLO.setMinimumSize(new Dimension(212, 30));
            EDBLO.setName("EDBLO");
            pnlOptionEdition.add(EDBLO, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 0), 0, 0));

            //---- RECART ----
            RECART.setText("R\u00e9capitulatif par transporteur");
            RECART.setComponentPopupMenu(null);
            RECART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RECART.setFont(new Font("sansserif", Font.PLAIN, 14));
            RECART.setPreferredSize(new Dimension(207, 30));
            RECART.setMinimumSize(new Dimension(207, 30));
            RECART.setName("RECART");
            pnlOptionEdition.add(RECART, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 0), 0, 0));

            //---- FACDIR ----
            FACDIR.setText("Avec facture directe");
            FACDIR.setComponentPopupMenu(null);
            FACDIR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            FACDIR.setFont(new Font("sansserif", Font.PLAIN, 14));
            FACDIR.setPreferredSize(new Dimension(148, 30));
            FACDIR.setMinimumSize(new Dimension(148, 30));
            FACDIR.setName("FACDIR");
            pnlOptionEdition.add(FACDIR, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 0), 0, 0));

            //======== pnlNombreExemplaire ========
            {
              pnlNombreExemplaire.setName("pnlNombreExemplaire");
              pnlNombreExemplaire.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlNombreExemplaire.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)pnlNombreExemplaire.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlNombreExemplaire.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlNombreExemplaire.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbNombreExemplaire ----
              lbNombreExemplaire.setText("Nombre d'exemplaires");
              lbNombreExemplaire.setName("lbNombreExemplaire");
              pnlNombreExemplaire.add(lbNombreExemplaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- NBEX ----
              NBEX.setComponentPopupMenu(null);
              NBEX.setMinimumSize(new Dimension(24, 28));
              NBEX.setPreferredSize(new Dimension(24, 28));
              NBEX.setFont(new Font("sansserif", Font.PLAIN, 14));
              NBEX.setName("NBEX");
              pnlNombreExemplaire.add(NBEX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOptionEdition.add(pnlNombreExemplaire, new GridBagConstraints(0, 4, 3, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlDroite);
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlBandeau;
  private SNLabelChamp lbLOCTP;
  private SNPanel pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresDeSelection;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private SNLabelChamp lbDateDebut;
  private SNPanel pnlPeriodeAEditer;
  private SNPanel pnlPeriode;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbDateFin;
  private XRiCalendrier PERFIN;
  private XRiCheckBox WTDAT;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox BONFAC;
  private XRiCheckBox DETART;
  private XRiCheckBox AGSTAT;
  private XRiCheckBox OPTTRI;
  private XRiCheckBox CHFCTR;
  private XRiCheckBox EDBLO;
  private XRiCheckBox RECART;
  private XRiCheckBox FACDIR;
  private SNPanel pnlNombreExemplaire;
  private SNLabelChamp lbNombreExemplaire;
  private XRiTextField NBEX;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
