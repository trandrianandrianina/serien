
package ri.serien.libecranrpg.sgvm.SGVM24FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM24FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_TRI = "Tri et sélection";
  
  public SGVM24FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_TRI, 't', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Ajout
    
    EDTFAX.setValeursSelection("OUI", "NON");
    WRED.setValeursSelection("OUI", "NON");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Initialise l'option Tri et sélection à NON
    lexique.HostFieldPutData("UTRI", 0, " ");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    pnlDebutFinReleves.setVisible(!WTOU.isSelected());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostFieldPutData("WCHOIX", 0, " ");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_TRI)) {
        lexique.HostFieldPutData("UTRI", 0, "X");
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    pnlDebutFinReleves.setVisible(!WTOU.isSelected());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlPlageRelevesTraiter = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    pnlDebutFinReleves = new SNPanel();
    lbNumeroReleveDebut = new SNLabelChamp();
    NUMDEB = new XRiTextField();
    lbNumeroReleveFin = new SNLabelChamp();
    NUMFIN = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptions = new SNPanelTitre();
    EDTFAX = new XRiCheckBox();
    WRED = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlPlageRelevesTraiter ========
        {
          pnlPlageRelevesTraiter.setTitre("Plage des relev\u00e9s \u00e0 traiter");
          pnlPlageRelevesTraiter.setName("pnlPlageRelevesTraiter");
          pnlPlageRelevesTraiter.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageRelevesTraiter.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlPlageRelevesTraiter.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageRelevesTraiter.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageRelevesTraiter.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMaximumSize(new Dimension(250, 30));
          WTOU.setMinimumSize(new Dimension(250, 30));
          WTOU.setPreferredSize(new Dimension(250, 30));
          WTOU.setName("WTOU");
          WTOU.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTOUItemStateChanged(e);
            }
          });
          pnlPlageRelevesTraiter.add(WTOU, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlDebutFinReleves ========
          {
            pnlDebutFinReleves.setName("pnlDebutFinReleves");
            pnlDebutFinReleves.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDebutFinReleves.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlDebutFinReleves.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlDebutFinReleves.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlDebutFinReleves.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbNumeroReleveDebut ----
            lbNumeroReleveDebut.setText("Num\u00e9ro de relev\u00e9 de d\u00e9but");
            lbNumeroReleveDebut.setPreferredSize(new Dimension(200, 30));
            lbNumeroReleveDebut.setMinimumSize(new Dimension(200, 30));
            lbNumeroReleveDebut.setMaximumSize(new Dimension(200, 30));
            lbNumeroReleveDebut.setName("lbNumeroReleveDebut");
            pnlDebutFinReleves.add(lbNumeroReleveDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- NUMDEB ----
            NUMDEB.setMinimumSize(new Dimension(70, 30));
            NUMDEB.setPreferredSize(new Dimension(70, 30));
            NUMDEB.setName("NUMDEB");
            pnlDebutFinReleves.add(NUMDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbNumeroReleveFin ----
            lbNumeroReleveFin.setText("Num\u00e9ro de relev\u00e9 de fin");
            lbNumeroReleveFin.setPreferredSize(new Dimension(200, 30));
            lbNumeroReleveFin.setMinimumSize(new Dimension(200, 30));
            lbNumeroReleveFin.setMaximumSize(new Dimension(200, 30));
            lbNumeroReleveFin.setName("lbNumeroReleveFin");
            pnlDebutFinReleves.add(lbNumeroReleveFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- NUMFIN ----
            NUMFIN.setName("NUMFIN");
            pnlDebutFinReleves.add(NUMFIN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageRelevesTraiter.add(pnlDebutFinReleves, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageRelevesTraiter, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setEditable(false);
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptions ========
        {
          pnlOptions.setTitre("Options d'\u00e9dition");
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- EDTFAX ----
          EDTFAX.setText("Envoi par email");
          EDTFAX.setPreferredSize(new Dimension(150, 30));
          EDTFAX.setMaximumSize(new Dimension(150, 30));
          EDTFAX.setMinimumSize(new Dimension(150, 30));
          EDTFAX.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDTFAX.setName("EDTFAX");
          pnlOptions.add(EDTFAX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- WRED ----
          WRED.setText("R\u00e9\u00e9dition");
          WRED.setFont(new Font("sansserif", Font.PLAIN, 14));
          WRED.setMaximumSize(new Dimension(82, 30));
          WRED.setMinimumSize(new Dimension(82, 30));
          WRED.setPreferredSize(new Dimension(82, 30));
          WRED.setName("WRED");
          pnlOptions.add(WRED, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPlageRelevesTraiter;
  private XRiCheckBox WTOU;
  private SNPanel pnlDebutFinReleves;
  private SNLabelChamp lbNumeroReleveDebut;
  private XRiTextField NUMDEB;
  private SNLabelChamp lbNumeroReleveFin;
  private XRiTextField NUMFIN;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox EDTFAX;
  private XRiCheckBox WRED;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
