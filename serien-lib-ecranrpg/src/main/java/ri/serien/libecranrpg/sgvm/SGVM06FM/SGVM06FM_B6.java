
package ri.serien.libecranrpg.sgvm.SGVM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM06FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM06FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    OPT6.setValeurs("6", "OPT6");
    OPT5.setValeurs("5", "OPT5");
    OPT8.setValeurs("8", "OPT8");
    OPT4.setValeurs("4", "OPT4");
    OPT3.setValeurs("3", "OPT3");
    OPT7.setValeurs("7", "OPT7");
    OPT2.setValeurs("2", "OPT2");
    OPT1.setValeurs("1", "OPT1");
    WTOUS.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
    REPEDT.setValeursSelection("O", "N");
    REPREF.setValeursSelection("O", "N");
    LIB4.setValeursSelection("X", " ");
    LIB3.setValeursSelection("X", " ");
    LIB2.setValeursSelection("X", " ");
    LIB1.setValeursSelection("X", " ");
    PRIX6.setValeursSelection("X", " ");
    PRIX7.setValeursSelection("X", " ");
    PRIX8.setValeursSelection("X", " ");
    PRIX9.setValeursSelection("X", " ");
    PRIX10.setValeursSelection("X", " ");
    PRIX5.setValeursSelection("X", " ");
    PRIX4.setValeursSelection("X", " ");
    PRIX3.setValeursSelection("X", " ");
    PRIX2.setValeursSelection("X", " ");
    PRIX1.setValeursSelection("X", " ");
    REPON1.setValeursSelection("OUI", "NON");
    WTTAR.setValeursSelection("**", "  ");
    ARTHIS.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_107.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG2@")).trim());
    OBJ_67.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    OBJ_76.setVisible(lexique.isPresent("WTOUS"));
    OBJ_75.setVisible(lexique.isPresent("WTOU"));
    OBJ_89.setVisible(lexique.isTrue("29"));
    
    ZP5.setVisible(lexique.isPresent("ZP5"));
    ZP4.setVisible(lexique.isPresent("ZP4"));
    ZP3.setVisible(lexique.isPresent("ZP3"));
    ZP2.setVisible(lexique.isPresent("ZP2"));
    ZP1.setVisible(lexique.isPresent("ZP1"));
    
    RACNV.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    RACNV.setEnabled(!lexique.HostFieldGetData("RB").trim().equalsIgnoreCase("5"));
    FINTAR.setVisible(lexique.isPresent("FINTAR"));
    // ATDAPX.setVisible( lexique.isPresent("ATDAPX"));
    DEBTAR.setVisible(lexique.isPresent("DEBTAR"));
    
    OBJ_105.setVisible(lexique.isPresent("WTTAR"));
    OBJ_102.setVisible(lexique.isPresent("WTTAR"));
    OBJ_114.setVisible(lexique.isPresent("FINART"));
    OBJ_110.setVisible(lexique.isPresent("DEBART"));
    
    OBJ_78.setVisible(lexique.isPresent("WTOU"));
    OBJ_100.setVisible(lexique.isTrue("32"));
    FINART.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    FINART.setEnabled(lexique.isPresent("FINART"));
    DEBART.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    DEBART.setEnabled(lexique.isPresent("DEBART"));
    OBJ_92.setVisible(lexique.isPresent("WTOUS"));
    MCLA1.setVisible(lexique.isPresent("MCLA1"));
    MCLA1.setEnabled(!lexique.isPresent("MCLA1"));
    REPON1_OBJ_113_55.setVisible(lexique.isPresent("SFADEB"));
    REPON1_OBJ_113_55.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    REPON1.setVisible(lexique.isTrue("32"));
    
    OBJ_116.setVisible(lexique.isPresent("PRIX1"));
    OBJ_95.setVisible(!lexique.HostFieldGetData("RFRE").trim().equalsIgnoreCase("Référence tarif à éditer"));
    
    // OPTABC.setSelectedItem(lexique.HostFieldGetData("OPTABC"));
    // OPTAB1.setSelectedItem(lexique.HostFieldGetData("OPTAB1"));
    
    if (lexique.isTrue("92")) {
      OPT2.setText("Article d'une famille");
    }
    else {
      OPT2.setText("Article par famille");
    }
    
    if (lexique.isTrue("32") && !lexique.isTrue("34")) {
      OBJ_67.setTitle("Plage famille à traiter");
    }
    if (lexique.isTrue("39")) {
      OBJ_67.setTitle("Plage sous-famille à traiter");
    }
    if (lexique.isTrue("33")) {
      OBJ_67.setTitle("Mot de classement 1");
    }
    if (lexique.isTrue("34")) {
      OBJ_67.setTitle("Mot de classement 2");
    }
    if (lexique.isTrue("35")) {
      OBJ_67.setTitle("Rattachement CNV (facultatif)");
    }
    if (lexique.isTrue("30")) {
      OBJ_67.setTitle("Zones personnalisées");
    }
    if (lexique.isTrue("29")) {
      OBJ_67.setTitle("Mot de classement 2 et code famille");
    }
    
    if (lexique.isTrue("31")) {
      OBJ_107.setTitle("Plage d'articles à éditer");
    }
    else {
      OBJ_107.setTitle("");
    }
    
    if (lexique.isTrue("32")) {
      OBJ_95.setTitle("");
    }
    else {
      OBJ_95.setTitle("Plage tarifs à éditer");
    }
    
    // *** Visibilié panels
    P_SEL2.setVisible(!WTOU.isSelected());
    panel7.setVisible(!WTOUS.isSelected());
    P_SEL7.setVisible(!WTTAR.isSelected());
    panel8.setVisible(lexique.isPresent("WTOU"));
    panel2.setVisible(lexique.isPresent("WTOUS"));
    P_SEL3.setVisible(lexique.isPresent("RACNV") || lexique.isPresent("ZP1"));
    panel9.setVisible(lexique.isPresent("MCLA1") || lexique.isPresent("MCLA2"));
    MCLA2.setVisible(lexique.isPresent("MCLA2"));
    MCLA1.setVisible(lexique.isPresent("MCLA1"));
    
    FAMDEB2.setVisible(lexique.isPresent("FAMDEB"));
    FAMDEB2.setText(lexique.HostFieldGetData("FAMDEB"));
    
    MCLA2.setText(lexique.HostFieldGetData("MCLA2"));
    
    

    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
    if (REPON1_OBJ_113_55.isSelected()) {
      lexique.HostFieldPutData("REPON1", 0, "OUI");
    }
    else {
      lexique.HostFieldPutData("REPON1", 0, "NON");
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTTARActionPerformed(ActionEvent e) {
    P_SEL7.setVisible(!P_SEL7.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOUSActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_52 = new JXTitledSeparator();
    OBJ_46 = new JXTitledSeparator();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    WDEV = new XRiTextField();
    WDEV1 = new XRiTextField();
    ATDAPX = new XRiCalendrier();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_62 = new JLabel();
    OPTABC = new XRiComboBox();
    OPTAB1 = new XRiComboBox();
    ARTHIS = new XRiCheckBox();
    OBJ_95 = new JXTitledSeparator();
    WTTAR = new XRiCheckBox();
    P_SEL7 = new JPanel();
    OBJ_102 = new JLabel();
    OBJ_105 = new JLabel();
    FINTAR = new XRiTextField();
    DEBTAR = new XRiTextField();
    OBJ_100 = new JLabel();
    OBJ_107 = new JXTitledSeparator();
    OBJ_110 = new JLabel();
    DEBART = new XRiTextField();
    REPON1_OBJ_113_55 = new JCheckBox();
    FINART = new XRiTextField();
    OBJ_114 = new JLabel();
    REPON1 = new XRiCheckBox();
    OBJ_116 = new JXTitledSeparator();
    PRIX1 = new XRiCheckBox();
    PRIX2 = new XRiCheckBox();
    PRIX3 = new XRiCheckBox();
    PRIX4 = new XRiCheckBox();
    PRIX5 = new XRiCheckBox();
    PRIX10 = new XRiCheckBox();
    PRIX9 = new XRiCheckBox();
    PRIX8 = new XRiCheckBox();
    PRIX7 = new XRiCheckBox();
    PRIX6 = new XRiCheckBox();
    OBJ_132 = new JXTitledSeparator();
    OBJ_133 = new JLabel();
    LIB1 = new XRiCheckBox();
    LIB2 = new XRiCheckBox();
    LIB3 = new XRiCheckBox();
    LIB4 = new XRiCheckBox();
    REPREF = new XRiCheckBox();
    REPEDT = new XRiCheckBox();
    label1 = new JLabel();
    textField1 = new XRiTextField();
    panel4 = new JPanel();
    P_SEL3 = new JPanel();
    RACNV = new XRiTextField();
    ZP4 = new XRiTextField();
    ZP1 = new XRiTextField();
    ZP2 = new XRiTextField();
    ZP3 = new XRiTextField();
    ZP5 = new XRiTextField();
    OBJ_67 = new JXTitledSeparator();
    panel8 = new JPanel();
    WTOU = new XRiCheckBox();
    P_SEL2 = new JPanel();
    OBJ_78 = new JLabel();
    OBJ_75 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    panel2 = new JPanel();
    WTOUS = new XRiCheckBox();
    panel7 = new JPanel();
    OBJ_76 = new JLabel();
    OBJ_92 = new JLabel();
    SFADEB = new XRiTextField();
    SFAFIN = new XRiTextField();
    panel9 = new JPanel();
    MCLA1 = new XRiTextField();
    OBJ_89 = new JLabel();
    FAMDEB2 = new XRiTextField();
    MCLA2 = new XRiTextField();
    panel1 = new JPanel();
    OPT1 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT7 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    OPT4 = new XRiRadioButton();
    OPT8 = new XRiRadioButton();
    OPT5 = new XRiRadioButton();
    OPT6 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    RB_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
          lb_loctp_.setBounds(new Rectangle(new Point(190, 5), lb_loctp_.getPreferredSize()));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 200));
          menus_haut.setPreferredSize(new Dimension(160, 200));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt_export ----
            riSousMenu_bt_export.setText("Exportation tableur");
            riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
            riSousMenu_bt_export.setName("riSousMenu_bt_export");
            riSousMenu_bt_export.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt_exportActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt_export);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- OBJ_52 ----
          OBJ_52.setTitle("Options possibles dans un tarif");
          OBJ_52.setName("OBJ_52");
          p_contenu.add(OBJ_52);
          OBJ_52.setBounds(30, 215, 400, OBJ_52.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setTitle("");
          OBJ_46.setName("OBJ_46");
          p_contenu.add(OBJ_46);
          OBJ_46.setBounds(30, 130, 855, OBJ_46.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("S\u00e9lection date d'application");
          OBJ_47.setName("OBJ_47");
          p_contenu.add(OBJ_47);
          OBJ_47.setBounds(45, 150, 199, 18);

          //---- OBJ_49 ----
          OBJ_49.setText("Devises");
          OBJ_49.setName("OBJ_49");
          p_contenu.add(OBJ_49);
          OBJ_49.setBounds(45, 180, 70, 18);

          //---- WDEV ----
          WDEV.setComponentPopupMenu(BTD);
          WDEV.setName("WDEV");
          p_contenu.add(WDEV);
          WDEV.setBounds(265, 175, 40, WDEV.getPreferredSize().height);

          //---- WDEV1 ----
          WDEV1.setComponentPopupMenu(BTD);
          WDEV1.setName("WDEV1");
          p_contenu.add(WDEV1);
          WDEV1.setBounds(305, 175, 40, WDEV1.getPreferredSize().height);

          //---- ATDAPX ----
          ATDAPX.setName("ATDAPX");
          p_contenu.add(ATDAPX);
          ATDAPX.setBounds(265, 145, 105, ATDAPX.getPreferredSize().height);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(30, 30, 855, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(200, 55, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(200, 85, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(45, 75, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(90, 70), bouton_etablissement.getPreferredSize()));

          //---- OBJ_62 ----
          OBJ_62.setText("Filtre sur code ABC de l'article");
          OBJ_62.setName("OBJ_62");
          p_contenu.add(OBJ_62);
          OBJ_62.setBounds(480, 149, 199, 20);

          //---- OPTABC ----
          OPTABC.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTABC.setComponentPopupMenu(BTD);
          OPTABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTABC.setName("OPTABC");
          p_contenu.add(OPTABC);
          OPTABC.setBounds(685, 146, 55, OPTABC.getPreferredSize().height);

          //---- OPTAB1 ----
          OPTAB1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTAB1.setComponentPopupMenu(BTD);
          OPTAB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTAB1.setName("OPTAB1");
          p_contenu.add(OPTAB1);
          OPTAB1.setBounds(740, 146, 55, OPTAB1.getPreferredSize().height);

          //---- ARTHIS ----
          ARTHIS.setText("Seulement Articles ayant d\u00e9ja tourn\u00e9s");
          ARTHIS.setComponentPopupMenu(BTD);
          ARTHIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARTHIS.setName("ARTHIS");
          p_contenu.add(ARTHIS);
          ARTHIS.setBounds(480, 179, 249, 20);

          //---- OBJ_95 ----
          OBJ_95.setTitle("Plage tarifs \u00e0 \u00e9diter");
          OBJ_95.setName("OBJ_95");
          p_contenu.add(OBJ_95);
          OBJ_95.setBounds(460, 215, 420, OBJ_95.getPreferredSize().height);

          //---- WTTAR ----
          WTTAR.setText("S\u00e9lection compl\u00e8te");
          WTTAR.setComponentPopupMenu(BTD);
          WTTAR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTTAR.setName("WTTAR");
          WTTAR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTTARActionPerformed(e);
            }
          });
          p_contenu.add(WTTAR);
          WTTAR.setBounds(480, 250, 141, 16);

          //======== P_SEL7 ========
          {
            P_SEL7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL7.setOpaque(false);
            P_SEL7.setName("P_SEL7");
            P_SEL7.setLayout(null);

            //---- OBJ_102 ----
            OBJ_102.setText("Tarif D\u00e9but");
            OBJ_102.setName("OBJ_102");
            P_SEL7.add(OBJ_102);
            OBJ_102.setBounds(23, 9, 142, 18);

            //---- OBJ_105 ----
            OBJ_105.setText("Tarif Fin");
            OBJ_105.setName("OBJ_105");
            P_SEL7.add(OBJ_105);
            OBJ_105.setBounds(23, 33, 147, 18);

            //---- FINTAR ----
            FINTAR.setComponentPopupMenu(BTD);
            FINTAR.setName("FINTAR");
            P_SEL7.add(FINTAR);
            FINTAR.setBounds(170, 28, 70, FINTAR.getPreferredSize().height);

            //---- DEBTAR ----
            DEBTAR.setComponentPopupMenu(BTD);
            DEBTAR.setName("DEBTAR");
            P_SEL7.add(DEBTAR);
            DEBTAR.setBounds(170, 4, 70, DEBTAR.getPreferredSize().height);

            //---- OBJ_100 ----
            OBJ_100.setText("R\u00e9f\u00e9rence tarif \u00e0 \u00e9diter");
            OBJ_100.setName("OBJ_100");
            P_SEL7.add(OBJ_100);
            OBJ_100.setBounds(23, 9, 156, 18);
          }
          p_contenu.add(P_SEL7);
          P_SEL7.setBounds(625, 240, 245, 57);

          //---- OBJ_107 ----
          OBJ_107.setTitle("@PLAG2@");
          OBJ_107.setName("OBJ_107");
          p_contenu.add(OBJ_107);
          OBJ_107.setBounds(460, 305, 420, OBJ_107.getPreferredSize().height);

          //---- OBJ_110 ----
          OBJ_110.setText("Code de d\u00e9but");
          OBJ_110.setName("OBJ_110");
          p_contenu.add(OBJ_110);
          OBJ_110.setBounds(480, 331, 120, OBJ_110.getPreferredSize().height);

          //---- DEBART ----
          DEBART.setComponentPopupMenu(BTD);
          DEBART.setName("DEBART");
          p_contenu.add(DEBART);
          DEBART.setBounds(610, 325, 210, DEBART.getPreferredSize().height);

          //---- REPON1_OBJ_113_55 ----
          REPON1_OBJ_113_55.setText("Saut de page / sous famille");
          REPON1_OBJ_113_55.setComponentPopupMenu(BTD);
          REPON1_OBJ_113_55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1_OBJ_113_55.setName("REPON1_OBJ_113_55");
          p_contenu.add(REPON1_OBJ_113_55);
          REPON1_OBJ_113_55.setBounds(555, 330, 204, REPON1_OBJ_113_55.getPreferredSize().height);

          //---- FINART ----
          FINART.setComponentPopupMenu(BTD);
          FINART.setName("FINART");
          p_contenu.add(FINART);
          FINART.setBounds(610, 355, 210, FINART.getPreferredSize().height);

          //---- OBJ_114 ----
          OBJ_114.setText("Code de fin");
          OBJ_114.setName("OBJ_114");
          p_contenu.add(OBJ_114);
          OBJ_114.setBounds(480, 361, 120, OBJ_114.getPreferredSize().height);

          //---- REPON1 ----
          REPON1.setText("Saut de page / famille");
          REPON1.setComponentPopupMenu(BTD);
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");
          p_contenu.add(REPON1);
          REPON1.setBounds(555, 360, 204, REPON1.getPreferredSize().height);

          //---- OBJ_116 ----
          OBJ_116.setTitle("Num\u00e9ro tarif \u00e0 \u00e9diter");
          OBJ_116.setName("OBJ_116");
          p_contenu.add(OBJ_116);
          OBJ_116.setBounds(460, 395, 420, OBJ_116.getPreferredSize().height);

          //---- PRIX1 ----
          PRIX1.setText("1");
          PRIX1.setComponentPopupMenu(BTD);
          PRIX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX1.setName("PRIX1");
          p_contenu.add(PRIX1);
          PRIX1.setBounds(480, 415, 40, PRIX1.getPreferredSize().height);

          //---- PRIX2 ----
          PRIX2.setText("2");
          PRIX2.setComponentPopupMenu(BTD);
          PRIX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX2.setName("PRIX2");
          p_contenu.add(PRIX2);
          PRIX2.setBounds(555, 415, 40, PRIX2.getPreferredSize().height);

          //---- PRIX3 ----
          PRIX3.setText("3");
          PRIX3.setComponentPopupMenu(BTD);
          PRIX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX3.setName("PRIX3");
          p_contenu.add(PRIX3);
          PRIX3.setBounds(630, 415, 40, PRIX3.getPreferredSize().height);

          //---- PRIX4 ----
          PRIX4.setText("4");
          PRIX4.setComponentPopupMenu(BTD);
          PRIX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX4.setName("PRIX4");
          p_contenu.add(PRIX4);
          PRIX4.setBounds(705, 415, 40, PRIX4.getPreferredSize().height);

          //---- PRIX5 ----
          PRIX5.setText("5");
          PRIX5.setComponentPopupMenu(BTD);
          PRIX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX5.setName("PRIX5");
          p_contenu.add(PRIX5);
          PRIX5.setBounds(780, 415, 40, PRIX5.getPreferredSize().height);

          //---- PRIX10 ----
          PRIX10.setText("10");
          PRIX10.setComponentPopupMenu(BTD);
          PRIX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX10.setName("PRIX10");
          p_contenu.add(PRIX10);
          PRIX10.setBounds(780, 435, 40, PRIX10.getPreferredSize().height);

          //---- PRIX9 ----
          PRIX9.setText("9");
          PRIX9.setComponentPopupMenu(BTD);
          PRIX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX9.setName("PRIX9");
          p_contenu.add(PRIX9);
          PRIX9.setBounds(705, 435, 40, PRIX9.getPreferredSize().height);

          //---- PRIX8 ----
          PRIX8.setText("8");
          PRIX8.setComponentPopupMenu(BTD);
          PRIX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX8.setName("PRIX8");
          p_contenu.add(PRIX8);
          PRIX8.setBounds(630, 435, 40, PRIX8.getPreferredSize().height);

          //---- PRIX7 ----
          PRIX7.setText("7");
          PRIX7.setComponentPopupMenu(BTD);
          PRIX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX7.setName("PRIX7");
          p_contenu.add(PRIX7);
          PRIX7.setBounds(555, 435, 40, PRIX7.getPreferredSize().height);

          //---- PRIX6 ----
          PRIX6.setText("6");
          PRIX6.setComponentPopupMenu(BTD);
          PRIX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRIX6.setName("PRIX6");
          p_contenu.add(PRIX6);
          PRIX6.setBounds(480, 435, 40, PRIX6.getPreferredSize().height);

          //---- OBJ_132 ----
          OBJ_132.setTitle("");
          OBJ_132.setName("OBJ_132");
          p_contenu.add(OBJ_132);
          OBJ_132.setBounds(460, 470, 420, OBJ_132.getPreferredSize().height);

          //---- OBJ_133 ----
          OBJ_133.setText("Libell\u00e9s articles \u00e0 \u00e9diter");
          OBJ_133.setName("OBJ_133");
          p_contenu.add(OBJ_133);
          OBJ_133.setBounds(480, 480, 144, 18);

          //---- LIB1 ----
          LIB1.setText("1");
          LIB1.setComponentPopupMenu(BTD);
          LIB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB1.setName("LIB1");
          p_contenu.add(LIB1);
          LIB1.setBounds(630, 480, 40, LIB1.getPreferredSize().height);

          //---- LIB2 ----
          LIB2.setText("2");
          LIB2.setComponentPopupMenu(BTD);
          LIB2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB2.setName("LIB2");
          p_contenu.add(LIB2);
          LIB2.setBounds(680, 480, 40, LIB2.getPreferredSize().height);

          //---- LIB3 ----
          LIB3.setText("3");
          LIB3.setComponentPopupMenu(BTD);
          LIB3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB3.setName("LIB3");
          p_contenu.add(LIB3);
          LIB3.setBounds(730, 480, 40, LIB3.getPreferredSize().height);

          //---- LIB4 ----
          LIB4.setText("4");
          LIB4.setComponentPopupMenu(BTD);
          LIB4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB4.setName("LIB4");
          p_contenu.add(LIB4);
          LIB4.setBounds(780, 480, 40, LIB4.getPreferredSize().height);

          //---- REPREF ----
          REPREF.setText("Edition r\u00e9f\u00e9rence fournisseur");
          REPREF.setComponentPopupMenu(BTD);
          REPREF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPREF.setName("REPREF");
          p_contenu.add(REPREF);
          REPREF.setBounds(480, 505, 205, 16);

          //---- REPEDT ----
          REPEDT.setText("Edition format 12 pouces");
          REPEDT.setComponentPopupMenu(BTD);
          REPEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPEDT.setName("REPEDT");
          p_contenu.add(REPEDT);
          REPEDT.setBounds(680, 505, 175, 16);

          //---- label1 ----
          label1.setText("Fournisseur \u00e0 traiter");
          label1.setName("label1");
          p_contenu.add(label1);
          label1.setBounds(480, 530, 200, 20);

          //---- textField1 ----
          textField1.setName("textField1");
          p_contenu.add(textField1);
          textField1.setBounds(680, 525, 90, textField1.getPreferredSize().height);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== P_SEL3 ========
            {
              P_SEL3.setOpaque(false);
              P_SEL3.setName("P_SEL3");
              P_SEL3.setLayout(null);

              //---- RACNV ----
              RACNV.setComponentPopupMenu(BTD);
              RACNV.setName("RACNV");
              P_SEL3.add(RACNV);
              RACNV.setBounds(160, 25, 60, RACNV.getPreferredSize().height);

              //---- ZP4 ----
              ZP4.setName("ZP4");
              P_SEL3.add(ZP4);
              ZP4.setBounds(210, 25, 31, ZP4.getPreferredSize().height);

              //---- ZP1 ----
              ZP1.setName("ZP1");
              P_SEL3.add(ZP1);
              ZP1.setBounds(65, 25, 31, ZP1.getPreferredSize().height);

              //---- ZP2 ----
              ZP2.setName("ZP2");
              P_SEL3.add(ZP2);
              ZP2.setBounds(115, 25, 31, ZP2.getPreferredSize().height);

              //---- ZP3 ----
              ZP3.setName("ZP3");
              P_SEL3.add(ZP3);
              ZP3.setBounds(160, 25, 31, ZP3.getPreferredSize().height);

              //---- ZP5 ----
              ZP5.setName("ZP5");
              P_SEL3.add(ZP5);
              ZP5.setBounds(255, 25, 31, ZP5.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL3.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL3.setMinimumSize(preferredSize);
                P_SEL3.setPreferredSize(preferredSize);
              }
            }
            panel4.add(P_SEL3);
            P_SEL3.setBounds(12, 22, 375, 85);

            //---- OBJ_67 ----
            OBJ_67.setTitle("@PLAG3@");
            OBJ_67.setName("OBJ_67");
            panel4.add(OBJ_67);
            OBJ_67.setBounds(5, 5, 409, OBJ_67.getPreferredSize().height);

            //======== panel8 ========
            {
              panel8.setOpaque(false);
              panel8.setName("panel8");
              panel8.setLayout(null);

              //---- WTOU ----
              WTOU.setText("S\u00e9lection compl\u00e8te");
              WTOU.setComponentPopupMenu(BTD);
              WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTOU.setName("WTOU");
              WTOU.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  WTOUActionPerformed(e);
                }
              });
              panel8.add(WTOU);
              WTOU.setBounds(100, 15, 141, 15);

              //======== P_SEL2 ========
              {
                P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
                P_SEL2.setOpaque(false);
                P_SEL2.setName("P_SEL2");
                P_SEL2.setLayout(null);

                //---- OBJ_78 ----
                OBJ_78.setText("Code famille de d\u00e9but");
                OBJ_78.setName("OBJ_78");
                P_SEL2.add(OBJ_78);
                OBJ_78.setBounds(25, 15, 151, 18);

                //---- OBJ_75 ----
                OBJ_75.setText("Code famille de fin");
                OBJ_75.setName("OBJ_75");
                P_SEL2.add(OBJ_75);
                OBJ_75.setBounds(25, 40, 165, 18);

                //---- FAMDEB ----
                FAMDEB.setComponentPopupMenu(BTD);
                FAMDEB.setName("FAMDEB");
                P_SEL2.add(FAMDEB);
                FAMDEB.setBounds(220, 10, 40, FAMDEB.getPreferredSize().height);

                //---- FAMFIN ----
                FAMFIN.setComponentPopupMenu(BTD);
                FAMFIN.setName("FAMFIN");
                P_SEL2.add(FAMFIN);
                FAMFIN.setBounds(220, 35, 40, FAMFIN.getPreferredSize().height);
              }
              panel8.add(P_SEL2);
              P_SEL2.setBounds(15, 45, 335, 70);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel8);
            panel8.setBounds(12, 22, 360, 125);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- WTOUS ----
              WTOUS.setText("S\u00e9lection compl\u00e8te");
              WTOUS.setComponentPopupMenu(BTD);
              WTOUS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTOUS.setName("WTOUS");
              WTOUS.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  WTOUSActionPerformed(e);
                }
              });
              panel2.add(WTOUS);
              WTOUS.setBounds(85, 10, 141, 15);

              //======== panel7 ========
              {
                panel7.setBorder(new BevelBorder(BevelBorder.LOWERED));
                panel7.setOpaque(false);
                panel7.setName("panel7");
                panel7.setLayout(null);

                //---- OBJ_76 ----
                OBJ_76.setText("Sous-famille de d\u00e9but");
                OBJ_76.setName("OBJ_76");
                panel7.add(OBJ_76);
                OBJ_76.setBounds(15, 15, 175, 18);

                //---- OBJ_92 ----
                OBJ_92.setText("Sous-famille de fin");
                OBJ_92.setName("OBJ_92");
                panel7.add(OBJ_92);
                OBJ_92.setBounds(15, 45, 175, 18);

                //---- SFADEB ----
                SFADEB.setComponentPopupMenu(BTD);
                SFADEB.setName("SFADEB");
                panel7.add(SFADEB);
                SFADEB.setBounds(190, 10, 60, SFADEB.getPreferredSize().height);

                //---- SFAFIN ----
                SFAFIN.setComponentPopupMenu(BTD);
                SFAFIN.setName("SFAFIN");
                panel7.add(SFAFIN);
                SFAFIN.setBounds(190, 40, 60, SFAFIN.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel7.getComponentCount(); i++) {
                    Rectangle bounds = panel7.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel7.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel7.setMinimumSize(preferredSize);
                  panel7.setPreferredSize(preferredSize);
                }
              }
              panel2.add(panel7);
              panel7.setBounds(20, 35, 310, 75);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel2);
            panel2.setBounds(12, 22, 360, 125);

            //======== panel9 ========
            {
              panel9.setOpaque(false);
              panel9.setName("panel9");
              panel9.setLayout(null);

              //---- MCLA1 ----
              MCLA1.setComponentPopupMenu(BTD);
              MCLA1.setName("MCLA1");
              panel9.add(MCLA1);
              MCLA1.setBounds(60, 10, 210, MCLA1.getPreferredSize().height);

              //---- OBJ_89 ----
              OBJ_89.setText("Code famille \u00e0 \u00e9diter");
              OBJ_89.setName("OBJ_89");
              panel9.add(OBJ_89);
              OBJ_89.setBounds(70, 50, 135, 20);

              //---- FAMDEB2 ----
              FAMDEB2.setComponentPopupMenu(BTD);
              FAMDEB2.setName("FAMDEB2");
              panel9.add(FAMDEB2);
              FAMDEB2.setBounds(230, 46, 40, FAMDEB2.getPreferredSize().height);

              //---- MCLA2 ----
              MCLA2.setComponentPopupMenu(BTD);
              MCLA2.setName("MCLA2");
              panel9.add(MCLA2);
              MCLA2.setBounds(60, 10, 210, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel9.getComponentCount(); i++) {
                  Rectangle bounds = panel9.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel9.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel9.setMinimumSize(preferredSize);
                panel9.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel9);
            panel9.setBounds(12, 22, 355, 90);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(30, 415, panel4.getPreferredSize().width, 160);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OPT1 ----
            OPT1.setText("Toutes les r\u00e9f\u00e9rences");
            OPT1.setComponentPopupMenu(BTD);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel1.add(OPT1);
            OPT1.setBounds(10, 10, 282, 18);

            //---- OPT2 ----
            OPT2.setText("Articles par famille");
            OPT2.setComponentPopupMenu(BTD);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel1.add(OPT2);
            OPT2.setBounds(10, 29, 282, 18);

            //---- OPT7 ----
            OPT7.setText("Articles par sous-famille");
            OPT7.setComponentPopupMenu(BTD);
            OPT7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT7.setName("OPT7");
            panel1.add(OPT7);
            OPT7.setBounds(10, 48, 282, 20);

            //---- OPT3 ----
            OPT3.setText("Articles sur mot de classement 1");
            OPT3.setComponentPopupMenu(BTD);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setName("OPT3");
            panel1.add(OPT3);
            OPT3.setBounds(10, 69, 282, 18);

            //---- OPT4 ----
            OPT4.setText("Articles de m\u00eame mot classement 2");
            OPT4.setComponentPopupMenu(BTD);
            OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT4.setName("OPT4");
            panel1.add(OPT4);
            OPT4.setBounds(10, 88, 282, 18);

            //---- OPT8 ----
            OPT8.setText("Articles par mot class.2 et famille");
            OPT8.setComponentPopupMenu(BTD);
            OPT8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT8.setName("OPT8");
            panel1.add(OPT8);
            OPT8.setBounds(10, 107, 282, 18);

            //---- OPT5 ----
            OPT5.setText("Articles de m\u00eame rattachement CNV");
            OPT5.setComponentPopupMenu(BTD);
            OPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT5.setName("OPT5");
            panel1.add(OPT5);
            OPT5.setBounds(10, 126, 282, 18);

            //---- OPT6 ----
            OPT6.setText("Articles sur zones personnalis\u00e9es");
            OPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT6.setName("OPT6");
            panel1.add(OPT6);
            OPT6.setBounds(10, 145, 282, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(40, 240, 315, 170);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //---- RB_GRP ----
    RB_GRP.add(OPT1);
    RB_GRP.add(OPT2);
    RB_GRP.add(OPT7);
    RB_GRP.add(OPT3);
    RB_GRP.add(OPT4);
    RB_GRP.add(OPT8);
    RB_GRP.add(OPT5);
    RB_GRP.add(OPT6);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_52;
  private JXTitledSeparator OBJ_46;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private XRiTextField WDEV;
  private XRiTextField WDEV1;
  private XRiCalendrier ATDAPX;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JLabel OBJ_62;
  private XRiComboBox OPTABC;
  private XRiComboBox OPTAB1;
  private XRiCheckBox ARTHIS;
  private JXTitledSeparator OBJ_95;
  private XRiCheckBox WTTAR;
  private JPanel P_SEL7;
  private JLabel OBJ_102;
  private JLabel OBJ_105;
  private XRiTextField FINTAR;
  private XRiTextField DEBTAR;
  private JLabel OBJ_100;
  private JXTitledSeparator OBJ_107;
  private JLabel OBJ_110;
  private XRiTextField DEBART;
  private JCheckBox REPON1_OBJ_113_55;
  private XRiTextField FINART;
  private JLabel OBJ_114;
  private XRiCheckBox REPON1;
  private JXTitledSeparator OBJ_116;
  private XRiCheckBox PRIX1;
  private XRiCheckBox PRIX2;
  private XRiCheckBox PRIX3;
  private XRiCheckBox PRIX4;
  private XRiCheckBox PRIX5;
  private XRiCheckBox PRIX10;
  private XRiCheckBox PRIX9;
  private XRiCheckBox PRIX8;
  private XRiCheckBox PRIX7;
  private XRiCheckBox PRIX6;
  private JXTitledSeparator OBJ_132;
  private JLabel OBJ_133;
  private XRiCheckBox LIB1;
  private XRiCheckBox LIB2;
  private XRiCheckBox LIB3;
  private XRiCheckBox LIB4;
  private XRiCheckBox REPREF;
  private XRiCheckBox REPEDT;
  private JLabel label1;
  private XRiTextField textField1;
  private JPanel panel4;
  private JPanel P_SEL3;
  private XRiTextField RACNV;
  private XRiTextField ZP4;
  private XRiTextField ZP1;
  private XRiTextField ZP2;
  private XRiTextField ZP3;
  private XRiTextField ZP5;
  private JXTitledSeparator OBJ_67;
  private JPanel panel8;
  private XRiCheckBox WTOU;
  private JPanel P_SEL2;
  private JLabel OBJ_78;
  private JLabel OBJ_75;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JPanel panel2;
  private XRiCheckBox WTOUS;
  private JPanel panel7;
  private JLabel OBJ_76;
  private JLabel OBJ_92;
  private XRiTextField SFADEB;
  private XRiTextField SFAFIN;
  private JPanel panel9;
  private XRiTextField MCLA1;
  private JLabel OBJ_89;
  private XRiTextField FAMDEB2;
  private XRiTextField MCLA2;
  private JPanel panel1;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT7;
  private XRiRadioButton OPT3;
  private XRiRadioButton OPT4;
  private XRiRadioButton OPT8;
  private XRiRadioButton OPT5;
  private XRiRadioButton OPT6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private ButtonGroup RB_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
