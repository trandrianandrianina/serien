
package ri.serien.libecranrpg.sgvm.SGVM66FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM292A] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Ventes inférieures au PUMP
 * Indicateur : 00000101
 * Titre : Edition des état des ventes inférieures au PUMP
 */
public class SGVM66FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private Message LOCTP = null;
  
  public SGVM66FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    // Liée les composants pour les plages de sélection
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snMagasinDebut.lierComposantFin(snMagasinFin);
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité des composants
    lbFamilleFin.setVisible(snFamilleFin.isVisible());
    lbFamilleDebut.setVisible(snFamilleDebut.isVisible());
    
    // initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charger les composant
    chargerMagasin();
    chargerFamille();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snMagasinDebut.getIdSelection() == null && snMagasinFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTMAG", 0, "**");
    }
    else {
      snMagasinDebut.renseignerChampRPG(lexique, "MAGDEB");
      snMagasinFin.renseignerChampRPG(lexique, "MAGFIN");
      lexique.HostFieldPutData("WTMAG", 0, "  ");
    }
    if (snFamilleDebut.getIdSelection() == null && snFamilleFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTFAM", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTFAM", 0, "  ");
      snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
      snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge la liste des magasins lors du changement d'etablissement
   */
  private void chargerMagasin() {
    snMagasinDebut.setSession(getSession());
    snMagasinDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinDebut.setTousAutorise(true);
    snMagasinDebut.charger(false);
    snMagasinDebut.setSelectionParChampRPG(lexique, "MAGDEB");
    
    snMagasinFin.setSession(getSession());
    snMagasinFin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinFin.setTousAutorise(true);
    snMagasinFin.charger(false);
    snMagasinFin.setSelectionParChampRPG(lexique, "MAGFIN");
  }
  
  /**
   * Charge le composant famille lors du changement d'etablissement
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerMagasin();
      chargerFamille();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbPeriodeAEditer = new SNLabelChamp();
    MOIDEB = new XRiCalendrier();
    lbA = new SNLabelChamp();
    MOIFIN = new XRiCalendrier();
    lbMagasinDebut = new SNLabelChamp();
    snMagasinDebut = new SNMagasin();
    lbMagasinFin = new SNLabelChamp();
    snMagasinFin = new SNMagasin();
    lbFamilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFamilleFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des \u00e9tat des ventes inf\u00e9rieures au PUMP");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriodeAEditer ----
            lbPeriodeAEditer.setText("P\u00e9riode \u00e0 \u00e9diter du");
            lbPeriodeAEditer.setMinimumSize(new Dimension(180, 30));
            lbPeriodeAEditer.setPreferredSize(new Dimension(180, 30));
            lbPeriodeAEditer.setName("lbPeriodeAEditer");
            pnlCritereSelection.add(lbPeriodeAEditer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MOIDEB ----
            MOIDEB.setTypeSaisie(6);
            MOIDEB.setPreferredSize(new Dimension(110, 30));
            MOIDEB.setMinimumSize(new Dimension(110, 30));
            MOIDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            MOIDEB.setName("MOIDEB");
            pnlCritereSelection.add(MOIDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbA ----
            lbA.setText("au");
            lbA.setHorizontalAlignment(SwingConstants.CENTER);
            lbA.setMinimumSize(new Dimension(16, 30));
            lbA.setPreferredSize(new Dimension(16, 30));
            lbA.setMaximumSize(new Dimension(16, 30));
            lbA.setName("lbA");
            pnlCritereSelection.add(lbA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MOIFIN ----
            MOIFIN.setTypeSaisie(6);
            MOIFIN.setMinimumSize(new Dimension(110, 30));
            MOIFIN.setPreferredSize(new Dimension(110, 30));
            MOIFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            MOIFIN.setName("MOIFIN");
            pnlCritereSelection.add(MOIFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasinDebut ----
            lbMagasinDebut.setText("Magasin de d\u00e9but");
            lbMagasinDebut.setMinimumSize(new Dimension(180, 30));
            lbMagasinDebut.setPreferredSize(new Dimension(180, 30));
            lbMagasinDebut.setName("lbMagasinDebut");
            pnlCritereSelection.add(lbMagasinDebut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasinDebut ----
            snMagasinDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasinDebut.setName("snMagasinDebut");
            pnlCritereSelection.add(snMagasinDebut, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasinFin ----
            lbMagasinFin.setText("Magasin de fin");
            lbMagasinFin.setMinimumSize(new Dimension(180, 30));
            lbMagasinFin.setPreferredSize(new Dimension(180, 30));
            lbMagasinFin.setName("lbMagasinFin");
            pnlCritereSelection.add(lbMagasinFin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasinFin ----
            snMagasinFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasinFin.setName("snMagasinFin");
            pnlCritereSelection.add(snMagasinFin, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleDebut ----
            lbFamilleDebut.setText("Famille de d\u00e9but");
            lbFamilleDebut.setMinimumSize(new Dimension(180, 30));
            lbFamilleDebut.setPreferredSize(new Dimension(180, 30));
            lbFamilleDebut.setName("lbFamilleDebut");
            pnlCritereSelection.add(lbFamilleDebut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereSelection.add(snFamilleDebut, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleFin ----
            lbFamilleFin.setText("Famille de fin");
            lbFamilleFin.setMinimumSize(new Dimension(75, 30));
            lbFamilleFin.setPreferredSize(new Dimension(75, 30));
            lbFamilleFin.setMaximumSize(new Dimension(75, 30));
            lbFamilleFin.setName("lbFamilleFin");
            pnlCritereSelection.add(lbFamilleFin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snFamilleFin ----
            snFamilleFin.setName("snFamilleFin");
            pnlCritereSelection.add(snFamilleFin, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbPeriodeAEditer;
  private XRiCalendrier MOIDEB;
  private SNLabelChamp lbA;
  private XRiCalendrier MOIFIN;
  private SNLabelChamp lbMagasinDebut;
  private SNMagasin snMagasinDebut;
  private SNLabelChamp lbMagasinFin;
  private SNMagasin snMagasinFin;
  private SNLabelChamp lbFamilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFamilleFin;
  private SNFamille snFamilleFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
