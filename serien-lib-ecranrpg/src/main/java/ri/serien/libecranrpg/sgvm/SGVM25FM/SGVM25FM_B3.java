
package ri.serien.libecranrpg.sgvm.SGVM25FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.EnumOrdreCritereDeTriEtDeSelection;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.affaire.snaffaire.SNAffaire;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.metier.vente.documentvente.snmodeexpedition.SNModeExpedition;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * Cette écran est utilisé dans tout les autre écran SGVM25FM, bouton Tier l'édition
 */
public class SGVM25FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  private ArrayList<SNComboBox> listeComboTri = new ArrayList<SNComboBox>();
  private ArrayList<EnumOrdreCritereDeTriEtDeSelection> listeEnumOrdreDeTri = new ArrayList<EnumOrdreCritereDeTriEtDeSelection>();
  private boolean isInitialise = false;
  
  public SGVM25FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    // Ajout
    initDiverses();
    setTitle("Tri et Sélection");
    
    snCanalDeVenteDebut.lierComposantFin(snCanalDeVenteFin);
    snCategorieClientDebut.lierComposantFin(snCategorieClientFin);
    
    sNBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    sNBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    sNBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Chargement de la liste des valeurs de l'EnumOrdreTri
    for (int i = 0; i < EnumOrdreCritereDeTriEtDeSelection.values().length; i++) {
      listeEnumOrdreDeTri.add(EnumOrdreCritereDeTriEtDeSelection.values()[i]);
    }
    
    // Ajout des Combobox dans listComboTri
    listeComboTri.add(cbTriRepresentant);
    listeComboTri.add(cbTriVendeur);
    listeComboTri.add(cbTriMagasin);
    listeComboTri.add(cbTriSectionAnalytique);
    listeComboTri.add(cbTriActivite);
    listeComboTri.add(cbTriClient);
    listeComboTri.add(cbTriTransporteur);
    listeComboTri.add(cbTriModeExpedition);
    listeComboTri.add(cbTriCanalDeVente);
    listeComboTri.add(cbTriCategorieClient);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // Fonctions diverses après initialisation des données
    setDiverses();
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // initialise les combobox de critères de tri
    initialiserCombo();
    
    // charge le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // charge les transporteur
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(idEtablissement);
    snTransporteur.setTousAutorise(true);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "WCTR");
    
    // charge la catégorie client
    snCategorieClientDebut.setSession(getSession());
    snCategorieClientDebut.setIdEtablissement(idEtablissement);
    snCategorieClientDebut.setTousAutorise(true);
    snCategorieClientDebut.charger(false);
    snCategorieClientDebut.setSelectionParChampRPG(lexique, "WCATD");
    
    snCategorieClientFin.setSession(getSession());
    snCategorieClientFin.setIdEtablissement(idEtablissement);
    snCategorieClientFin.setTousAutorise(true);
    snCategorieClientFin.charger(false);
    snCategorieClientFin.setSelectionParChampRPG(lexique, "WCATF");
    
    // charge le vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(idEtablissement);
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "WVDE");
    
    // charge les representant
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(idEtablissement);
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "WREP");
    
    // charge les sections analytique
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(idEtablissement);
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "WSEC");
    
    // initialise les sections analytique
    snClient.setSession(getSession());
    snClient.setIdEtablissement(idEtablissement);
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "WCLI");
    
    // charge la catégorie client
    snCanalDeVenteDebut.setSession(getSession());
    snCanalDeVenteDebut.setIdEtablissement(idEtablissement);
    snCanalDeVenteDebut.setTousAutorise(true);
    snCanalDeVenteDebut.charger(false);
    snCanalDeVenteDebut.setSelectionParChampRPG(lexique, "WCAND");
    
    snCanalDeVenteFin.setSession(getSession());
    snCanalDeVenteFin.setIdEtablissement(idEtablissement);
    snCanalDeVenteFin.setTousAutorise(true);
    snCanalDeVenteFin.charger(false);
    snCanalDeVenteFin.setSelectionParChampRPG(lexique, "WCANF");
    
    // charge le mode d'expedition
    snModeExpedition.setSession(getSession());
    snModeExpedition.setIdEtablissement(idEtablissement);
    snModeExpedition.setTousAutorise(true);
    snModeExpedition.charger(false);
    snModeExpedition.setSelectionParChampRPG(lexique, "WEXP");
    
    // charge les affaires
    snAffaire.setSession(getSession());
    snAffaire.setIdEtablissement(idEtablissement);
    snAffaire.setTousAutorise(true);
    snAffaire.charger(false);
    snAffaire.setSelectionParChampRPG(lexique, "WACT");
  }
  
  @Override
  public void getData() {
    super.getData();
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snCategorieClientDebut.renseignerChampRPG(lexique, "WCATD");
    snCategorieClientFin.renseignerChampRPG(lexique, "WCATF");
    snTransporteur.renseignerChampRPG(lexique, "WCTR");
    snVendeur.renseignerChampRPG(lexique, "WVDE");
    snRepresentant.renseignerChampRPG(lexique, "WREP");
    snSectionAnalytique.renseignerChampRPG(lexique, "WSEC");
    snClient.renseignerChampRPG(lexique, "WCLI");
    snModeExpedition.renseignerChampRPG(lexique, "WEXP");
    snAffaire.renseignerChampRPG(lexique, "WACT");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * initialise les combobox de critères de tri
   */
  private void initialiserCombo() {
    isInitialise = false;
    for (SNComboBox combo : listeComboTri) {
      // initialise les combo n'ayant aucune sélection avec les valeur de listeEnumOrdreDeTri
      if (combo.getSelectedItem() == null || combo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        combo.removeAllItems();
        for (EnumOrdreCritereDeTriEtDeSelection enumTri : listeEnumOrdreDeTri) {
          combo.addItem(enumTri);
        }
      }
      else {
        // Permet de mettre à jour les combo ayant une sélection sans pour autant perdre celle ci
        EnumOrdreCritereDeTriEtDeSelection selectionEnCours = (EnumOrdreCritereDeTriEtDeSelection) combo.getSelectedItem();
        combo.removeAllItems();
        for (EnumOrdreCritereDeTriEtDeSelection enumTri : listeEnumOrdreDeTri) {
          combo.addItem(enumTri);
        }
        combo.addItem(selectionEnCours);
        combo.setSelectedItem(selectionEnCours);
      }
    }
    isInitialise = true;
  }
  
  /**
   * gére les sélection des combo ainsi que la liste des EnumOrdreCriteresDeTriEtSelection
   */
  private void traiterSelectionCombo(ItemEvent pE, SNComboBox pCombo, String champRPG) {
    // Récupère la valeur qui à étais désélectionner afin de la rajouter à listeEnumOrdreDeTri si cette valeur n'existe pas dans celle-ci
    if (pE.getStateChange() == ItemEvent.DESELECTED) {
      if (!listeEnumOrdreDeTri.contains(pE.getItem())) {
        listeEnumOrdreDeTri.add((EnumOrdreCritereDeTriEtDeSelection) pE.getItem());
      }
    }
    // si on a une selection valide
    if (pCombo.getSelectedItem() != null) {
      if (!pCombo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        listeEnumOrdreDeTri.remove(pCombo.getSelectedItem());
      }
      initialiserCombo();
      if (pCombo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        lexique.HostFieldPutData(champRPG, 0, " ");
      }
      else {
        lexique.HostFieldPutData(champRPG, 0, ((EnumOrdreCritereDeTriEtDeSelection) pCombo.getSelectedItem()).getStringCode());
      }
    }
  }
  
  private void cbTriRepresentantItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriRepresentant, "WTI1");
    }
  }
  
  private void cbTriVendeurItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriVendeur, "WTI2");
    }
  }
  
  private void cbTriMagasinItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriMagasin, "WTI3");
    }
  }
  
  private void cbTriSectionAnalytiqueItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriSectionAnalytique, "WTI4");
    }
  }
  
  private void cbTriActiviteItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriActivite, "WTI5");
    }
  }
  
  private void cbTriClientItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriClient, "WTI6");
    }
  }
  
  private void cbTriTransporteurItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriRepresentant, "WTI8");
    }
  }
  
  private void cbTriModeExpeditionItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriModeExpedition, "WTI9");
    }
  }
  
  private void cbTriCanalDeVenteItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriCanalDeVente, "WTI10");
    }
  }
  
  private void cbTriCategorieClientItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriCategorieClient, "WTI11");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlCriteres = new SNPanelTitre();
    lbRepresentant = new SNLabelChamp();
    cbTriRepresentant = new SNComboBox();
    snRepresentant = new SNRepresentant();
    lbVendeur = new SNLabelChamp();
    cbTriVendeur = new SNComboBox();
    snVendeur = new SNVendeur();
    lbMagasin = new SNLabelChamp();
    cbTriMagasin = new SNComboBox();
    snMagasin = new SNMagasin();
    lbSection = new SNLabelChamp();
    cbTriSectionAnalytique = new SNComboBox();
    snSectionAnalytique = new SNSectionAnalytique();
    lbAffaire = new SNLabelChamp();
    cbTriActivite = new SNComboBox();
    snAffaire = new SNAffaire();
    lbClient = new SNLabelChamp();
    cbTriClient = new SNComboBox();
    snClient = new SNClientPrincipal();
    lbTransporteur = new SNLabelChamp();
    cbTriTransporteur = new SNComboBox();
    snTransporteur = new SNTransporteur();
    lbModeExpedition = new SNLabelChamp();
    cbTriModeExpedition = new SNComboBox();
    snModeExpedition = new SNModeExpedition();
    lbCanalDeVente = new SNLabelChamp();
    cbTriCanalDeVente = new SNComboBox();
    pnlCanalVente = new SNPanel();
    snCanalDeVenteDebut = new SNCanalDeVente();
    lbA = new SNLabelChamp();
    snCanalDeVenteFin = new SNCanalDeVente();
    lbCategorieClient = new SNLabelChamp();
    cbTriCategorieClient = new SNComboBox();
    pnlCategorieClient = new SNPanel();
    snCategorieClientDebut = new SNCategorieClient();
    lbA2 = new SNLabelChamp();
    snCategorieClientFin = new SNCategorieClient();
    sNBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(1050, 475));
    setPreferredSize(new Dimension(1050, 475));
    setMaximumSize(new Dimension(1050, 475));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

      //======== pnlCriteres ========
      {
        pnlCriteres.setTitre("Crit\u00e8res de tri");
        pnlCriteres.setName("pnlCriteres");
        pnlCriteres.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCriteres.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlCriteres.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCriteres.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlCriteres.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbRepresentant ----
        lbRepresentant.setText("Repr\u00e9sentant");
        lbRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRepresentant.setName("lbRepresentant");
        pnlCriteres.add(lbRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriRepresentant ----
        cbTriRepresentant.setPreferredSize(new Dimension(155, 30));
        cbTriRepresentant.setMinimumSize(new Dimension(155, 30));
        cbTriRepresentant.setMaximumSize(new Dimension(155, 30));
        cbTriRepresentant.setName("cbTriRepresentant");
        cbTriRepresentant.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriRepresentantItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriRepresentant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snRepresentant ----
        snRepresentant.setName("snRepresentant");
        pnlCriteres.add(snRepresentant, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbVendeur ----
        lbVendeur.setText("Vendeur");
        lbVendeur.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbVendeur.setName("lbVendeur");
        pnlCriteres.add(lbVendeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriVendeur ----
        cbTriVendeur.setPreferredSize(new Dimension(155, 30));
        cbTriVendeur.setMinimumSize(new Dimension(155, 30));
        cbTriVendeur.setMaximumSize(new Dimension(155, 30));
        cbTriVendeur.setName("cbTriVendeur");
        cbTriVendeur.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriVendeurItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snVendeur ----
        snVendeur.setName("snVendeur");
        pnlCriteres.add(snVendeur, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMagasin ----
        lbMagasin.setText("Magasin");
        lbMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbMagasin.setName("lbMagasin");
        pnlCriteres.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriMagasin ----
        cbTriMagasin.setPreferredSize(new Dimension(155, 30));
        cbTriMagasin.setMinimumSize(new Dimension(155, 30));
        cbTriMagasin.setMaximumSize(new Dimension(155, 30));
        cbTriMagasin.setName("cbTriMagasin");
        cbTriMagasin.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriMagasinItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snMagasin ----
        snMagasin.setName("snMagasin");
        pnlCriteres.add(snMagasin, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbSection ----
        lbSection.setText("Section");
        lbSection.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbSection.setName("lbSection");
        pnlCriteres.add(lbSection, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriSectionAnalytique ----
        cbTriSectionAnalytique.setPreferredSize(new Dimension(155, 30));
        cbTriSectionAnalytique.setMinimumSize(new Dimension(155, 30));
        cbTriSectionAnalytique.setMaximumSize(new Dimension(155, 30));
        cbTriSectionAnalytique.setName("cbTriSectionAnalytique");
        cbTriSectionAnalytique.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriSectionAnalytiqueItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriSectionAnalytique, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snSectionAnalytique ----
        snSectionAnalytique.setName("snSectionAnalytique");
        pnlCriteres.add(snSectionAnalytique, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbAffaire ----
        lbAffaire.setText("Affaire");
        lbAffaire.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbAffaire.setName("lbAffaire");
        pnlCriteres.add(lbAffaire, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriActivite ----
        cbTriActivite.setPreferredSize(new Dimension(155, 30));
        cbTriActivite.setMinimumSize(new Dimension(155, 30));
        cbTriActivite.setMaximumSize(new Dimension(155, 30));
        cbTriActivite.setName("cbTriActivite");
        cbTriActivite.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriActiviteItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriActivite, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snAffaire ----
        snAffaire.setName("snAffaire");
        pnlCriteres.add(snAffaire, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbClient ----
        lbClient.setText("Client");
        lbClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbClient.setName("lbClient");
        pnlCriteres.add(lbClient, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriClient ----
        cbTriClient.setPreferredSize(new Dimension(155, 30));
        cbTriClient.setMinimumSize(new Dimension(155, 30));
        cbTriClient.setMaximumSize(new Dimension(155, 30));
        cbTriClient.setName("cbTriClient");
        cbTriClient.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriClientItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriClient, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snClient ----
        snClient.setName("snClient");
        pnlCriteres.add(snClient, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbTransporteur ----
        lbTransporteur.setText("Transporteur / tourn\u00e9e");
        lbTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbTransporteur.setName("lbTransporteur");
        pnlCriteres.add(lbTransporteur, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriTransporteur ----
        cbTriTransporteur.setPreferredSize(new Dimension(155, 30));
        cbTriTransporteur.setMinimumSize(new Dimension(155, 30));
        cbTriTransporteur.setMaximumSize(new Dimension(155, 30));
        cbTriTransporteur.setName("cbTriTransporteur");
        cbTriTransporteur.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriTransporteurItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriTransporteur, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snTransporteur ----
        snTransporteur.setName("snTransporteur");
        pnlCriteres.add(snTransporteur, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbModeExpedition ----
        lbModeExpedition.setText("Mode d'exp\u00e9dition / tourn\u00e9e");
        lbModeExpedition.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbModeExpedition.setMaximumSize(new Dimension(170, 30));
        lbModeExpedition.setPreferredSize(new Dimension(177, 30));
        lbModeExpedition.setMinimumSize(new Dimension(177, 30));
        lbModeExpedition.setName("lbModeExpedition");
        pnlCriteres.add(lbModeExpedition, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriModeExpedition ----
        cbTriModeExpedition.setPreferredSize(new Dimension(155, 30));
        cbTriModeExpedition.setMinimumSize(new Dimension(155, 30));
        cbTriModeExpedition.setMaximumSize(new Dimension(155, 30));
        cbTriModeExpedition.setName("cbTriModeExpedition");
        cbTriModeExpedition.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriModeExpeditionItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriModeExpedition, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snModeExpedition ----
        snModeExpedition.setName("snModeExpedition");
        pnlCriteres.add(snModeExpedition, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbCanalDeVente ----
        lbCanalDeVente.setText("Canal de vente");
        lbCanalDeVente.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCanalDeVente.setName("lbCanalDeVente");
        pnlCriteres.add(lbCanalDeVente, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriCanalDeVente ----
        cbTriCanalDeVente.setPreferredSize(new Dimension(155, 30));
        cbTriCanalDeVente.setMinimumSize(new Dimension(155, 30));
        cbTriCanalDeVente.setMaximumSize(new Dimension(155, 30));
        cbTriCanalDeVente.setName("cbTriCanalDeVente");
        cbTriCanalDeVente.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriCanalDeVenteItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriCanalDeVente, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== pnlCanalVente ========
        {
          pnlCanalVente.setName("pnlCanalVente");
          pnlCanalVente.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCanalVente.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCanalVente.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlCanalVente.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCanalVente.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- snCanalDeVenteDebut ----
          snCanalDeVenteDebut.setName("snCanalDeVenteDebut");
          pnlCanalVente.add(snCanalDeVenteDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbA ----
          lbA.setText("\u00e0");
          lbA.setHorizontalAlignment(SwingConstants.CENTER);
          lbA.setPreferredSize(new Dimension(8, 30));
          lbA.setMinimumSize(new Dimension(8, 30));
          lbA.setMaximumSize(new Dimension(8, 30));
          lbA.setName("lbA");
          pnlCanalVente.add(lbA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snCanalDeVenteFin ----
          snCanalDeVenteFin.setName("snCanalDeVenteFin");
          pnlCanalVente.add(snCanalDeVenteFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCriteres.add(pnlCanalVente, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbCategorieClient ----
        lbCategorieClient.setText("Cat\u00e9gorie client");
        lbCategorieClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCategorieClient.setName("lbCategorieClient");
        pnlCriteres.add(lbCategorieClient, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- cbTriCategorieClient ----
        cbTriCategorieClient.setPreferredSize(new Dimension(155, 30));
        cbTriCategorieClient.setMinimumSize(new Dimension(155, 30));
        cbTriCategorieClient.setMaximumSize(new Dimension(155, 30));
        cbTriCategorieClient.setName("cbTriCategorieClient");
        cbTriCategorieClient.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriCategorieClientItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriCategorieClient, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlCategorieClient ========
        {
          pnlCategorieClient.setName("pnlCategorieClient");
          pnlCategorieClient.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCategorieClient.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCategorieClient.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlCategorieClient.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCategorieClient.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- snCategorieClientDebut ----
          snCategorieClientDebut.setName("snCategorieClientDebut");
          pnlCategorieClient.add(snCategorieClientDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbA2 ----
          lbA2.setText("\u00e0");
          lbA2.setHorizontalAlignment(SwingConstants.CENTER);
          lbA2.setPreferredSize(new Dimension(8, 30));
          lbA2.setMaximumSize(new Dimension(8, 30));
          lbA2.setMinimumSize(new Dimension(8, 30));
          lbA2.setName("lbA2");
          pnlCategorieClient.add(lbA2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snCategorieClientFin ----
          snCategorieClientFin.setName("snCategorieClientFin");
          pnlCategorieClient.add(snCategorieClientFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCriteres.add(pnlCategorieClient, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCriteres, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- sNBarreBouton ----
    sNBarreBouton.setName("sNBarreBouton");
    add(sNBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlCriteres;
  private SNLabelChamp lbRepresentant;
  private SNComboBox cbTriRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbVendeur;
  private SNComboBox cbTriVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbMagasin;
  private SNComboBox cbTriMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbSection;
  private SNComboBox cbTriSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbAffaire;
  private SNComboBox cbTriActivite;
  private SNAffaire snAffaire;
  private SNLabelChamp lbClient;
  private SNComboBox cbTriClient;
  private SNClientPrincipal snClient;
  private SNLabelChamp lbTransporteur;
  private SNComboBox cbTriTransporteur;
  private SNTransporteur snTransporteur;
  private SNLabelChamp lbModeExpedition;
  private SNComboBox cbTriModeExpedition;
  private SNModeExpedition snModeExpedition;
  private SNLabelChamp lbCanalDeVente;
  private SNComboBox cbTriCanalDeVente;
  private SNPanel pnlCanalVente;
  private SNCanalDeVente snCanalDeVenteDebut;
  private SNLabelChamp lbA;
  private SNCanalDeVente snCanalDeVenteFin;
  private SNLabelChamp lbCategorieClient;
  private SNComboBox cbTriCategorieClient;
  private SNPanel pnlCategorieClient;
  private SNCategorieClient snCategorieClientDebut;
  private SNLabelChamp lbA2;
  private SNCategorieClient snCategorieClientFin;
  private SNBarreBouton sNBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
