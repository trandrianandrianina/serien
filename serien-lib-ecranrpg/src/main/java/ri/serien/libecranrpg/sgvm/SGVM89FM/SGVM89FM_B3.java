
package ri.serien.libecranrpg.sgvm.SGVM89FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM89FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _PAT001_Top = { "PAT001", "PAT002", "PAT003", "PAT004", "PAT005", "PAT006", "PAT007", "PAT008", "PAT009", "PAT010",
      "PAT011", "PAT012", "PAT013", "PAT014", "PAT015", "PAT016", };
  private String[] _PAT001_Title = { "Etb", "Identification", "Purge", "En cours", };
  private String[][] _PAT001_Data = { { "PAS001", "PAN001", "PAM001", "PAE001", }, { "PAS002", "PAN002", "PAM002", "PAE002", },
      { "PAS003", "PAN003", "PAM003", "PAE003", }, { "PAS004", "PAN004", "PAM004", "PAE004", }, { "PAS005", "PAN005", "PAM005", "PAE005", },
      { "PAS006", "PAN006", "PAM006", "PAE006", }, { "PAS007", "PAN007", "PAM007", "PAE007", }, { "PAS008", "PAN008", "PAM008", "PAE008", },
      { "PAS009", "PAN009", "PAM009", "PAE009", }, { "PAS010", "PAN010", "PAM010", "PAE010", }, { "PAS011", "PAN011", "PAM011", "PAE011", },
      { "PAS012", "PAN012", "PAM012", "PAE012", }, { "PAS013", "PAN013", "PAM013", "PAE013", }, { "PAS014", "PAN014", "PAM014", "PAE014", },
      { "PAS015", "PAN015", "PAM015", "PAE015", }, { "PAS016", "PAN016", "PAM016", "PAE016", }, };
  private int[] _PAT001_Width = { 33, 204, 45, 124, };
  
  public SGVM89FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    PAT001.setAspectTable(_PAT001_Top, _PAT001_Title, _PAT001_Data, _PAT001_Width, true, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _PAT001_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    OBJ_44.setEnabled(lexique.isPresent("WTETB"));
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTETB", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void PAT001MouseClicked(MouseEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_44 = new JButton();
    panel1 = new JPanel();
    SCROLLPANE_LIST1 = new JScrollPane();
    PAT001 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 200));
        menus_haut.setPreferredSize(new Dimension(160, 200));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt_export ----
          riSousMenu_bt_export.setText("Exportation tableur");
          riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
          riSousMenu_bt_export.setName("riSousMenu_bt_export");
          riSousMenu_bt_export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt_exportActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt_export);
        }
        menus_haut.add(riSousMenu6);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(620, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- OBJ_44 ----
          OBJ_44.setText("S\u00e9lection de toutes les soci\u00e9t\u00e9s");
          OBJ_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_44.setName("OBJ_44");
          OBJ_44.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_44ActionPerformed(e);
            }
          });
          p_contenu.add(OBJ_44);
          OBJ_44.setBounds(375, 390, 210, 26);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Choix de la soci\u00e9t\u00e9"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST1 ========
            {
              SCROLLPANE_LIST1.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

              //---- PAT001 ----
              PAT001.setName("PAT001");
              PAT001.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  PAT001MouseClicked(e);
                }
              });
              SCROLLPANE_LIST1.setViewportView(PAT001);
            }
            panel1.add(SCROLLPANE_LIST1);
            SCROLLPANE_LIST1.setBounds(25, 35, 470, 285);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(495, 35, 25, 130);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(495, 190, 25, 130);
          }
          p_contenu.add(panel1);
          panel1.setBounds(35, 35, 550, 347);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JButton OBJ_44;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable PAT001;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
