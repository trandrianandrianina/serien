/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Edition de la liste des articles par code article.
 * [GVM131] Gestion des ventes -> Fiches permanentes -> Articles -> Articles
 */
public class SGVM05FM_B5 extends SNPanelEcranRPG implements ioFrame {
  private static final String EXPORTER = "Exporter";
  
  private String[] ARTCNT_Value = { "", "1", "2", };
  private String[] ARTDRT_Value = { "", "1", "2", };
  
  public SGVM05FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    ARTDRT.setValeurs(ARTDRT_Value, null);
    ARTCNT.setValeurs(ARTCNT_Value, null);
    WTOU.setValeursSelection("**", "  ");
    
    snBarreBouton.ajouterBouton(EXPORTER, 'e', true);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@@TITPG2@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    if (lexique.isTrue("19")) {
      String message = lexique.HostFieldGetData("V03F");
      if (message != null && !message.isEmpty()) {
        DialogueInformation.afficher(message);
      }
    }
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    


    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Renseigner l'article de début
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "DEBLO2");
    
    // Renseigner l'article de fin
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "FINLO2");
    
    pnlDroitAuteur.setVisible(lexique.isPresent("ARTCNT") || lexique.isPresent("ARTDRT"));
    lbArticleContrat.setVisible(lexique.isPresent("ARTCNT"));
    lbDroitAuteur.setVisible(lexique.isPresent("ARTDRT"));
    snArticleDebut.setEnabled(!WTOU.isSelected());
    snArticleFin.setEnabled(!WTOU.isSelected());
    
    

  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snArticleDebut.renseignerChampRPG(lexique, "DEBLO2");
    snArticleFin.renseignerChampRPG(lexique, "FINLO2");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    snArticleDebut.setEnabled(!WTOU.isSelected());
    snArticleFin.setEnabled(!WTOU.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_bpresentation = new SNBandeauTitre();
    p_contenu = new JPanel();
    pnlEtablissement = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    z_wencx_ = new RiZoneSortie();
    pnlPlageArticle = new JPanel();
    WTOU = new XRiCheckBox();
    lnCodeDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbCodeFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    pnlDroitAuteur = new JPanel();
    lbArticleContrat = new SNLabelChamp();
    ARTCNT = new XRiComboBox();
    lbDroitAuteur = new SNLabelChamp();
    ARTDRT = new XRiComboBox();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());

    //---- p_bpresentation ----
    p_bpresentation.setText("@TITPG1@@TITPG2@");
    p_bpresentation.setName("p_bpresentation");
    add(p_bpresentation, BorderLayout.NORTH);

    //======== p_contenu ========
    {
      p_contenu.setPreferredSize(new Dimension(700, 400));
      p_contenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      p_contenu.setBackground(new Color(239, 239, 222));
      p_contenu.setName("p_contenu");
      p_contenu.setLayout(new GridBagLayout());
      ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
      ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlEtablissement ========
      {
        pnlEtablissement.setBorder(new TitledBorder("Etablissement"));
        pnlEtablissement.setOpaque(false);
        pnlEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlEtablissement.setName("pnlEtablissement");
        pnlEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 10), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPeriodeEnCours ----
        lbPeriodeEnCours.setText("P\u00e9riode en cours");
        lbPeriodeEnCours.setName("lbPeriodeEnCours");
        pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- z_wencx_ ----
        z_wencx_.setText("@WENCX@");
        z_wencx_.setPreferredSize(new Dimension(220, 30));
        z_wencx_.setMinimumSize(new Dimension(220, 30));
        z_wencx_.setFont(new Font("sansserif", Font.PLAIN, 14));
        z_wencx_.setName("z_wencx_");
        pnlEtablissement.add(z_wencx_, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_contenu.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlPlageArticle ========
      {
        pnlPlageArticle.setBorder(new TitledBorder("Plage d'articles"));
        pnlPlageArticle.setOpaque(false);
        pnlPlageArticle.setName("pnlPlageArticle");
        pnlPlageArticle.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlPlageArticle.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlPlageArticle.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlPlageArticle.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlPlageArticle.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- WTOU ----
        WTOU.setText("S\u00e9lection compl\u00e8te");
        WTOU.setComponentPopupMenu(BTD);
        WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WTOU.setMinimumSize(new Dimension(300, 30));
        WTOU.setPreferredSize(new Dimension(300, 30));
        WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTOU.setName("WTOU");
        WTOU.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            WTOUActionPerformed(e);
          }
        });
        pnlPlageArticle.add(WTOU, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lnCodeDebut ----
        lnCodeDebut.setText("Code de d\u00e9but");
        lnCodeDebut.setName("lnCodeDebut");
        pnlPlageArticle.add(lnCodeDebut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 10), 0, 0));

        //---- snArticleDebut ----
        snArticleDebut.setName("snArticleDebut");
        pnlPlageArticle.add(snArticleDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbCodeFin ----
        lbCodeFin.setText("Code de fin");
        lbCodeFin.setName("lbCodeFin");
        pnlPlageArticle.add(lbCodeFin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- snArticleFin ----
        snArticleFin.setName("snArticleFin");
        pnlPlageArticle.add(snArticleFin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_contenu.add(pnlPlageArticle, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlDroitAuteur ========
      {
        pnlDroitAuteur.setBorder(new TitledBorder("Droits d'auteurs"));
        pnlDroitAuteur.setOpaque(false);
        pnlDroitAuteur.setName("pnlDroitAuteur");
        pnlDroitAuteur.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroitAuteur.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroitAuteur.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroitAuteur.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlDroitAuteur.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbArticleContrat ----
        lbArticleContrat.setText("Articles / Contrat");
        lbArticleContrat.setName("lbArticleContrat");
        pnlDroitAuteur.add(lbArticleContrat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 10), 0, 0));

        //---- ARTCNT ----
        ARTCNT.setModel(new DefaultComboBoxModel(new String[] {
          "Tous",
          "Avec contrat",
          "Sans contrat"
        }));
        ARTCNT.setComponentPopupMenu(BTD);
        ARTCNT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ARTCNT.setMinimumSize(new Dimension(250, 30));
        ARTCNT.setPreferredSize(new Dimension(250, 30));
        ARTCNT.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARTCNT.setName("ARTCNT");
        pnlDroitAuteur.add(ARTCNT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDroitAuteur ----
        lbDroitAuteur.setText("Droits d'auteur");
        lbDroitAuteur.setName("lbDroitAuteur");
        pnlDroitAuteur.add(lbDroitAuteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- ARTDRT ----
        ARTDRT.setModel(new DefaultComboBoxModel(new String[] {
          "",
          "Soumis \u00e0 droits",
          "Non soumis"
        }));
        ARTDRT.setComponentPopupMenu(BTD);
        ARTDRT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ARTDRT.setMinimumSize(new Dimension(250, 30));
        ARTDRT.setPreferredSize(new Dimension(250, 30));
        ARTDRT.setName("ARTDRT");
        pnlDroitAuteur.add(ARTDRT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_contenu.add(pnlDroitAuteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(p_contenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre p_bpresentation;
  private JPanel p_contenu;
  private JPanel pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private RiZoneSortie z_wencx_;
  private JPanel pnlPlageArticle;
  private XRiCheckBox WTOU;
  private SNLabelChamp lnCodeDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbCodeFin;
  private SNArticle snArticleFin;
  private JPanel pnlDroitAuteur;
  private SNLabelChamp lbArticleContrat;
  private XRiComboBox ARTCNT;
  private SNLabelChamp lbDroitAuteur;
  private XRiComboBox ARTDRT;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
