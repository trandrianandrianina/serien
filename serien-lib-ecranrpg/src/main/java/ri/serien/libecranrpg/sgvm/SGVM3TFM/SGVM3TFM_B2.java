
package ri.serien.libecranrpg.sgvm.SGVM3TFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM3TFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM3TFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    INTL.setValeursSelection("OUI", "NON");
    RECAP.setValeursSelection("OUI", "NON");
    SAUT.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VDLIB@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO1@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB1@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO2@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB2@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO3@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    panel1.setVisible(lexique.isTrue("91"));
    panel2.setVisible(lexique.isTrue("N20"));
    panel5.setVisible(lexique.isTrue("N21"));
    panel4.setVisible(lexique.isTrue("N22"));
    // INTL.setSelected(lexique.HostFieldGetData("INTL").equalsIgnoreCase("OUI"));
    // RECAP.setSelected(lexique.HostFieldGetData("RECAP").equalsIgnoreCase("OUI"));
    // SAUT.setSelected(lexique.HostFieldGetData("SAUT").equalsIgnoreCase("OUI"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (INTL.isSelected())
    // lexique.HostFieldPutData("INTL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("INTL", 0, "NON");
    // if (RECAP.isSelected())
    // lexique.HostFieldPutData("RECAP", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RECAP", 0, "NON");
    // if (SAUT.isSelected())
    // lexique.HostFieldPutData("SAUT", 0, "OUI");
    // else
    // lexique.HostFieldPutData("SAUT", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_20 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    OBJ_19 = new JXTitledSeparator();
    OBJ_50 = new RiZoneSortie();
    SAUT = new XRiCheckBox();
    RECAP = new XRiCheckBox();
    INTL = new XRiCheckBox();
    OBJ_49 = new JLabel();
    VEND = new XRiTextField();
    panel1 = new JPanel();
    OBJ_32 = new RiZoneSortie();
    OBJ_30 = new JLabel();
    MAG = new XRiTextField();
    panel2 = new JPanel();
    OBJ_47 = new RiZoneSortie();
    UPER1 = new XRiTextField();
    UPER2 = new XRiTextField();
    OBJ_91 = new RiZoneSortie();
    OBJ_51 = new JLabel();
    panel5 = new JPanel();
    OBJ_59 = new RiZoneSortie();
    UPER3 = new XRiTextField();
    UPER4 = new XRiTextField();
    OBJ_55 = new RiZoneSortie();
    OBJ_61 = new JLabel();
    panel4 = new JPanel();
    OBJ_67 = new RiZoneSortie();
    UPER5 = new XRiTextField();
    UPER6 = new XRiTextField();
    OBJ_63 = new RiZoneSortie();
    OBJ_69 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_20 ----
          OBJ_20.setTitle("P\u00e9riode factur\u00e9e \u00e0 \u00e9diter");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Options d'\u00e9dition");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_19 ----
          OBJ_19.setTitle("");
          OBJ_19.setName("OBJ_19");

          //---- OBJ_50 ----
          OBJ_50.setText("@VDLIB@");
          OBJ_50.setName("OBJ_50");

          //---- SAUT ----
          SAUT.setText("Saut de page par journ\u00e9e");
          SAUT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SAUT.setName("SAUT");

          //---- RECAP ----
          RECAP.setText("R\u00e9capitulatif seulement");
          RECAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RECAP.setName("RECAP");

          //---- INTL ----
          INTL.setText("Edition avec interlignes");
          INTL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          INTL.setName("INTL");

          //---- OBJ_49 ----
          OBJ_49.setText("Code vendeur");
          OBJ_49.setName("OBJ_49");

          //---- VEND ----
          VEND.setComponentPopupMenu(BTD);
          VEND.setName("VEND");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_32 ----
            OBJ_32.setText("@MALIB@");
            OBJ_32.setName("OBJ_32");
            panel1.add(OBJ_32);
            OBJ_32.setBounds(235, 10, 250, OBJ_32.getPreferredSize().height);

            //---- OBJ_30 ----
            OBJ_30.setText("Code magasin");
            OBJ_30.setName("OBJ_30");
            panel1.add(OBJ_30);
            OBJ_30.setBounds(15, 10, 145, 18);

            //---- MAG ----
            MAG.setComponentPopupMenu(BTD);
            MAG.setName("MAG");
            panel1.add(MAG);
            MAG.setBounds(165, 5, 34, MAG.getPreferredSize().height);
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_47 ----
            OBJ_47.setText("@PERIO1@");
            OBJ_47.setName("OBJ_47");
            panel2.add(OBJ_47);
            OBJ_47.setBounds(20, 10, 111, OBJ_47.getPreferredSize().height);

            //---- UPER1 ----
            UPER1.setToolTipText("Jour");
            UPER1.setComponentPopupMenu(BTD);
            UPER1.setName("UPER1");
            panel2.add(UPER1);
            UPER1.setBounds(175, 8, 30, UPER1.getPreferredSize().height);

            //---- UPER2 ----
            UPER2.setToolTipText("Jour");
            UPER2.setComponentPopupMenu(BTD);
            UPER2.setName("UPER2");
            panel2.add(UPER2);
            UPER2.setBounds(240, 8, 30, UPER2.getPreferredSize().height);

            //---- OBJ_91 ----
            OBJ_91.setText("@LIB1@");
            OBJ_91.setName("OBJ_91");
            panel2.add(OBJ_91);
            OBJ_91.setBounds(280, 10, 195, OBJ_91.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("/");
            OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_51.setName("OBJ_51");
            panel2.add(OBJ_51);
            OBJ_51.setBounds(205, 13, 32, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- OBJ_59 ----
            OBJ_59.setText("@PERIO2@");
            OBJ_59.setName("OBJ_59");
            panel5.add(OBJ_59);
            OBJ_59.setBounds(20, 10, 111, OBJ_59.getPreferredSize().height);

            //---- UPER3 ----
            UPER3.setToolTipText("Jour");
            UPER3.setComponentPopupMenu(BTD);
            UPER3.setName("UPER3");
            panel5.add(UPER3);
            UPER3.setBounds(175, 8, 30, UPER3.getPreferredSize().height);

            //---- UPER4 ----
            UPER4.setToolTipText("Jour");
            UPER4.setComponentPopupMenu(BTD);
            UPER4.setName("UPER4");
            panel5.add(UPER4);
            UPER4.setBounds(240, 8, 30, UPER4.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("@LIB2@");
            OBJ_55.setName("OBJ_55");
            panel5.add(OBJ_55);
            OBJ_55.setBounds(280, 10, 195, OBJ_55.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("/");
            OBJ_61.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_61.setName("OBJ_61");
            panel5.add(OBJ_61);
            OBJ_61.setBounds(205, 13, 32, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_67 ----
            OBJ_67.setText("@PERIO3@");
            OBJ_67.setName("OBJ_67");
            panel4.add(OBJ_67);
            OBJ_67.setBounds(20, 10, 111, OBJ_67.getPreferredSize().height);

            //---- UPER5 ----
            UPER5.setToolTipText("Jour");
            UPER5.setComponentPopupMenu(BTD);
            UPER5.setName("UPER5");
            panel4.add(UPER5);
            UPER5.setBounds(175, 8, 30, UPER5.getPreferredSize().height);

            //---- UPER6 ----
            UPER6.setToolTipText("Jour");
            UPER6.setComponentPopupMenu(BTD);
            UPER6.setName("UPER6");
            panel4.add(UPER6);
            UPER6.setBounds(240, 8, 30, UPER6.getPreferredSize().height);

            //---- OBJ_63 ----
            OBJ_63.setText("@LIB3@");
            OBJ_63.setName("OBJ_63");
            panel4.add(OBJ_63);
            OBJ_63.setBounds(280, 10, 195, OBJ_63.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("/");
            OBJ_69.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_69.setName("OBJ_69");
            panel4.add(OBJ_69);
            OBJ_69.setBounds(205, 13, 32, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(VEND, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(RECAP, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(SAUT, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(INTL, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(VEND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(RECAP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(SAUT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(INTL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_20;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_19;
  private RiZoneSortie OBJ_50;
  private XRiCheckBox SAUT;
  private XRiCheckBox RECAP;
  private XRiCheckBox INTL;
  private JLabel OBJ_49;
  private XRiTextField VEND;
  private JPanel panel1;
  private RiZoneSortie OBJ_32;
  private JLabel OBJ_30;
  private XRiTextField MAG;
  private JPanel panel2;
  private RiZoneSortie OBJ_47;
  private XRiTextField UPER1;
  private XRiTextField UPER2;
  private RiZoneSortie OBJ_91;
  private JLabel OBJ_51;
  private JPanel panel5;
  private RiZoneSortie OBJ_59;
  private XRiTextField UPER3;
  private XRiTextField UPER4;
  private RiZoneSortie OBJ_55;
  private JLabel OBJ_61;
  private JPanel panel4;
  private RiZoneSortie OBJ_67;
  private XRiTextField UPER5;
  private XRiTextField UPER6;
  private RiZoneSortie OBJ_63;
  private JLabel OBJ_69;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
