
package ri.serien.libecranrpg.sgvm.SGVM11FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM11FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORTER = "Exporter";
  
  public SGVM11FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Boutons
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    // Ajout
    initDiverses();
    UBART.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    
    UBAR1.setEnabled(lexique.isPresent("UBAR1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@  @(TITLE)@"));
    
    

    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCriteresSelection = new SNPanelTitre();
    lbRemise = new SNLabelChamp();
    DATREM = new XRiCalendrier();
    lbBarème = new SNLabelChamp();
    UBAR1 = new XRiTextField();
    UBART = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfPeriodeEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriodeEnCours = new SNLabelChamp();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);

      //======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridLayout());

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {471, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlCriteresSelection ========
          {
            pnlCriteresSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresSelection.setName("pnlCriteresSelection");
            pnlCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlCriteresSelection.getLayout()).columnWidths = new int[] {245, 47, 0, 0};
            ((GridBagLayout)pnlCriteresSelection.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlCriteresSelection.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlCriteresSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbRemise ----
            lbRemise.setText("Remise p\u00e9riodique \u00e0 la fin du mois ");
            lbRemise.setName("lbRemise");
            pnlCriteresSelection.add(lbRemise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- DATREM ----
            DATREM.setComponentPopupMenu(null);
            DATREM.setTypeSaisie(6);
            DATREM.setPreferredSize(new Dimension(115, 30));
            DATREM.setMinimumSize(new Dimension(115, 30));
            DATREM.setMaximumSize(new Dimension(115, 30));
            DATREM.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATREM.setName("DATREM");
            pnlCriteresSelection.add(DATREM, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbBarème ----
            lbBarème.setText("Code bar\u00e8me des remises \u00e0 traiter");
            lbBarème.setName("lbBar\u00e8me");
            pnlCriteresSelection.add(lbBarème, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- UBAR1 ----
            UBAR1.setFont(new Font("sansserif", Font.PLAIN, 14));
            UBAR1.setMinimumSize(new Dimension(72, 30));
            UBAR1.setMaximumSize(new Dimension(72, 30));
            UBAR1.setPreferredSize(new Dimension(72, 30));
            UBAR1.setName("UBAR1");
            pnlCriteresSelection.add(UBAR1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- UBART ----
            UBART.setText("Toutes les remises");
            UBART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UBART.setFont(new Font("sansserif", Font.PLAIN, 14));
            UBART.setName("UBART");
            pnlCriteresSelection.add(UBART, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlDroite);
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresSelection;
  private SNLabelChamp lbRemise;
  private XRiCalendrier DATREM;
  private SNLabelChamp lbBarème;
  private XRiTextField UBAR1;
  private XRiCheckBox UBART;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfPeriodeEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
