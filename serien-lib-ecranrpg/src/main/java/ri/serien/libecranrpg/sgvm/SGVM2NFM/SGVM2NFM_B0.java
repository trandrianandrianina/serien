
package ri.serien.libecranrpg.sgvm.SGVM2NFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM2NFM_B0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM2NFM_B0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_5 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(260, 125));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setPreferredSize(new Dimension(315, 100));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_5 ----
          OBJ_5.setText("G\u00e9n\u00e9ration en cours...");
          OBJ_5.setName("OBJ_5");
          p_recup.add(OBJ_5);
          OBJ_5.setBounds(55, 35, 160, 26);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
