
package ri.serien.libecranrpg.sgvm.SGVM57FM;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM57FM_B2 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_EXPORTER = "Exporter";
  
  private String[] TYPEDT_Value = { "DET", "REC", "D+R", };
  
  public SGVM57FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    XROT.setValeursSelection("O", "N");
    WHOM.setValeursSelection("OUI", "NON");
    EDTNDP.setValeursSelection("O", " ");
    WRAZ.setValeursSelection("OUI", "NON");
    WGENB.setValeursSelection("OUI", "NON");
    
    // Initialisation de la barre des boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation du composant article
    snFamille.setSession(getSession());
    snFamille.setIdEtablissement(snEtablissement.getIdSelection());
    snFamille.setTousAutorise(true);
    snFamille.charger(true);
    snFamille.setSelectionParChampRPG(lexique, "EFAM");
    
    // Initialisation du composant fournisseur
    if (ckTousFournisseur.isSelected()) {
      snFournisseur.setVisible(false);
    }
    else {
      snFournisseur.setVisible(true);
      snFournisseur.setSession(getSession());
      snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
      snFournisseur.charger(false);
      snFournisseur.setSelectionParChampRPG(lexique, "EFRS");
    }
    
    chargerListeComposantsMagasin();
    
    lbRegle.setVisible(CODDP.isVisible());
    
    p_bpresentation.setIdEtablissement(snEtablissement.getIdSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snFamille.renseignerChampRPG(lexique, "EFAM");
    snFournisseur.renseignerChampRPG(lexique, "EFRS");
    
    // Gestion selection complete Magasin
    if (snMagasin1.getIdSelection() == null && snMagasin2.getIdSelection() == null && snMagasin3.getIdSelection() == null
        && snMagasin4.getIdSelection() == null && snMagasin5.getIdSelection() == null && snMagasin6.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOUM", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOUM", 0, "");
      snMagasin1.renseignerChampRPG(lexique, "MA01");
      snMagasin2.renseignerChampRPG(lexique, "MA02");
      snMagasin3.renseignerChampRPG(lexique, "MA03");
      snMagasin4.renseignerChampRPG(lexique, "MA04");
      snMagasin5.renseignerChampRPG(lexique, "MA05");
      snMagasin6.renseignerChampRPG(lexique, "MA06");
    }
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * snMagasin1 : "Tous" autorisé, "Aucun" interdit
   * snMagasin2-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On vérifie que le nombre de magasins n'est pas supérieur au nombre de composant magasin présent.
    // On charge un nombre de composants égal au nombre de choix possibles ou au nombre maximal de composants disponibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    for (int i = 0; i < listeComposant.size(); i++) {
      listeComposant.get(i).setVisible(false);
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On rend le composant snMagasin2 visibles si le composant snMagasin1 contient autre chose que "Tous".
      if (i == 0) {
        listeComposant.get(i).setVisible(!(snMagasin1.getIdSelection() == null));
      }
      // On rend les composants suivants visibles si le composant précédent contient autre chose que "Aucun" (null)
      else {
        listeComposant.get(i).setVisible(!(listeComposant.get(i - 1).getIdSelection() == null));
      }
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé.
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
  }
  
  // On observe le changement de valeur du composant snMagasin1.
  // On affiche ou non le composants snMagasin2 si snMagasin1 différent de "Aucun" (null).
  // On affiche le composant s'il est censé pouvoir être affiché.
  private void snMagasin1ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 2 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 2) {
      snMagasin2.setVisible(!(snMagasin1.getIdSelection() == null));
      lbMagasin2.setVisible(snMagasin2.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin1.getIdSelection() == null) {
      snMagasin2.setSelection(null);
    }
  }
  
  // On effectue la même observation pour chaque composant snMagasin.
  // On affiche le composant suivant selon les même conditions.
  private void snMagasin2ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 3 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 3) {
      snMagasin3.setVisible(!(snMagasin2.getIdSelection() == null));
      lbMagasin3.setVisible(snMagasin3.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin2.getIdSelection() == null) {
      snMagasin3.setSelection(null);
    }
  }
  
  private void snMagasin3ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 4 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 4) {
      snMagasin4.setVisible(!(snMagasin3.getIdSelection() == null));
      lbMagasin4.setVisible(snMagasin4.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin3.getIdSelection() == null) {
      snMagasin4.setSelection(null);
    }
  }
  
  private void snMagasin4ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 5 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 5) {
      snMagasin5.setVisible(!(snMagasin4.getIdSelection() == null));
      lbMagasin5.setVisible(snMagasin5.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin4.getIdSelection() == null) {
      snMagasin5.setSelection(null);
    }
  }
  
  private void snMagasin5ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 6 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 6) {
      snMagasin6.setVisible(!(snMagasin5.getIdSelection() == null));
      lbMagasin6.setVisible(snMagasin6.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin5.getIdSelection() == null) {
      snMagasin6.setSelection(null);
    }
  }
  
  private void ckTousFournisseurItemStateChanged(ItemEvent e) {
    try {
      if (ckTousFournisseur.isSelected()) {
        snFournisseur.setVisible(false);
      }
      else {
        snFournisseur.setVisible(true);
        snFournisseur.setSession(getSession());
        snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
        snFournisseur.charger(false);
        snFournisseur.setSelectionParChampRPG(lexique, "EFRS");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    pnlMagasin = new SNPanel();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    pnlAutreCriteres = new SNPanel();
    lbFamille = new SNLabelChamp();
    snFamille = new SNFamille();
    lbFournisseur = new SNLabelChamp();
    sNPanel2 = new SNPanel();
    snFournisseur = new SNFournisseur();
    ckTousFournisseur = new XRiCheckBox();
    lbPeriodeEdition = new SNLabelChamp();
    DATFIN = new XRiCalendrier();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlOptions = new SNPanelTitre();
    WGENB = new XRiCheckBox();
    WHOM = new XRiCheckBox();
    WRAZ = new XRiCheckBox();
    XROT = new XRiCheckBox();
    EDTNDP = new XRiCheckBox();
    sNPanel1 = new SNPanel();
    lbRegle = new SNLabelChamp();
    CODDP = new XRiTextField();
    OBJ_105 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlMagasin ========
          {
            pnlMagasin.setName("pnlMagasin");
            pnlMagasin.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMagasin.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlMagasin.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlMagasin.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlMagasin.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin1 ----
            lbMagasin1.setText("Magasin 1");
            lbMagasin1.setName("lbMagasin1");
            pnlMagasin.add(lbMagasin1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin1.setName("snMagasin1");
            snMagasin1.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin1ValueChanged(e);
              }
            });
            pnlMagasin.add(snMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin 2");
            lbMagasin2.setName("lbMagasin2");
            pnlMagasin.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin2 ----
            snMagasin2.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin2.setName("snMagasin2");
            snMagasin2.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin2ValueChanged(e);
              }
            });
            pnlMagasin.add(snMagasin2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Magasin 3");
            lbMagasin3.setName("lbMagasin3");
            pnlMagasin.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin3 ----
            snMagasin3.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin3.setName("snMagasin3");
            snMagasin3.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin3ValueChanged(e);
              }
            });
            pnlMagasin.add(snMagasin3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin4 ----
            lbMagasin4.setText("Magasin 4");
            lbMagasin4.setName("lbMagasin4");
            pnlMagasin.add(lbMagasin4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin4 ----
            snMagasin4.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin4.setName("snMagasin4");
            snMagasin4.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin4ValueChanged(e);
              }
            });
            pnlMagasin.add(snMagasin4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin5 ----
            lbMagasin5.setText("Magasin 5");
            lbMagasin5.setName("lbMagasin5");
            pnlMagasin.add(lbMagasin5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin5 ----
            snMagasin5.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin5.setName("snMagasin5");
            snMagasin5.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin5ValueChanged(e);
              }
            });
            pnlMagasin.add(snMagasin5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin6 ----
            lbMagasin6.setText("Magasin 6");
            lbMagasin6.setName("lbMagasin6");
            pnlMagasin.add(lbMagasin6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin6 ----
            snMagasin6.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin6.setName("snMagasin6");
            pnlMagasin.add(snMagasin6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCritereSelection.add(pnlMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlAutreCriteres ========
          {
            pnlAutreCriteres.setName("pnlAutreCriteres");
            pnlAutreCriteres.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlAutreCriteres.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlAutreCriteres.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlAutreCriteres.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlAutreCriteres.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbFamille ----
            lbFamille.setText("Famille");
            lbFamille.setName("lbFamille");
            pnlAutreCriteres.add(lbFamille, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamille ----
            snFamille.setName("snFamille");
            pnlAutreCriteres.add(snFamille, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setName("lbFournisseur");
            pnlAutreCriteres.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel2 ========
            {
              sNPanel2.setName("sNPanel2");
              sNPanel2.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
              ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- snFournisseur ----
              snFournisseur.setName("snFournisseur");
              sNPanel2.add(snFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ckTousFournisseur ----
              ckTousFournisseur.setText("Tous");
              ckTousFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
              ckTousFournisseur.setName("ckTousFournisseur");
              ckTousFournisseur.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  ckTousFournisseurItemStateChanged(e);
                }
              });
              sNPanel2.add(ckTousFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlAutreCriteres.add(sNPanel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEdition ----
            lbPeriodeEdition.setText("Date d'analyse");
            lbPeriodeEdition.setName("lbPeriodeEdition");
            pnlAutreCriteres.add(lbPeriodeEdition, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- DATFIN ----
            DATFIN.setMaximumSize(new Dimension(115, 30));
            DATFIN.setMinimumSize(new Dimension(115, 30));
            DATFIN.setPreferredSize(new Dimension(115, 30));
            DATFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATFIN.setName("DATFIN");
            pnlAutreCriteres.add(DATFIN, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCritereSelection.add(pnlAutreCriteres, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfEnCours ----
          tfEnCours.setText("@WENCX@");
          tfEnCours.setEnabled(false);
          tfEnCours.setPreferredSize(new Dimension(260, 30));
          tfEnCours.setMinimumSize(new Dimension(260, 30));
          tfEnCours.setMaximumSize(new Dimension(260, 30));
          tfEnCours.setName("tfEnCours");
          pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOptions ========
        {
          pnlOptions.setTitre("Options");
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- WGENB ----
          WGENB.setText("Generation des bordereaux de d\u00e9pr\u00e9ciation");
          WGENB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WGENB.setFont(new Font("sansserif", Font.PLAIN, 14));
          WGENB.setMinimumSize(new Dimension(124, 30));
          WGENB.setPreferredSize(new Dimension(124, 30));
          WGENB.setName("WGENB");
          pnlOptions.add(WGENB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- WHOM ----
          WHOM.setText("Valid\u00e9s");
          WHOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WHOM.setFont(new Font("sansserif", Font.PLAIN, 14));
          WHOM.setMinimumSize(new Dimension(124, 30));
          WHOM.setPreferredSize(new Dimension(124, 30));
          WHOM.setName("WHOM");
          pnlOptions.add(WHOM, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- WRAZ ----
          WRAZ.setText("Effacement des d\u00e9pr\u00e9ciations ant\u00e9rieures");
          WRAZ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WRAZ.setFont(new Font("sansserif", Font.PLAIN, 14));
          WRAZ.setMinimumSize(new Dimension(124, 30));
          WRAZ.setPreferredSize(new Dimension(124, 30));
          WRAZ.setName("WRAZ");
          pnlOptions.add(WRAZ, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- XROT ----
          XROT.setText("Etat r\u00e9duit");
          XROT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          XROT.setFont(new Font("sansserif", Font.PLAIN, 14));
          XROT.setMinimumSize(new Dimension(124, 30));
          XROT.setPreferredSize(new Dimension(124, 30));
          XROT.setName("XROT");
          pnlOptions.add(XROT, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EDTNDP ----
          EDTNDP.setText("Edition des articles non d\u00e9pr\u00e9ci\u00e9");
          EDTNDP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTNDP.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDTNDP.setMinimumSize(new Dimension(124, 30));
          EDTNDP.setPreferredSize(new Dimension(124, 30));
          EDTNDP.setName("EDTNDP");
          pnlOptions.add(EDTNDP, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbRegle ----
            lbRegle.setText("R\u00e8gle de d\u00e9pr\u00e9ciation \u00e0 utiliser");
            lbRegle.setMinimumSize(new Dimension(250, 30));
            lbRegle.setMaximumSize(new Dimension(300, 30));
            lbRegle.setPreferredSize(new Dimension(250, 30));
            lbRegle.setName("lbRegle");
            sNPanel1.add(lbRegle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- CODDP ----
            CODDP.setPreferredSize(new Dimension(50, 30));
            CODDP.setMinimumSize(new Dimension(50, 30));
            CODDP.setMaximumSize(new Dimension(50, 30));
            CODDP.setName("CODDP");
            sNPanel1.add(CODDP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlOptions.add(sNPanel1, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- OBJ_105 ----
    OBJ_105.setText("point de menu GVM3252, GVM396 1 2 3");
    OBJ_105.setName("OBJ_105");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNPanel pnlMagasin;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNPanel pnlAutreCriteres;
  private SNLabelChamp lbFamille;
  private SNFamille snFamille;
  private SNLabelChamp lbFournisseur;
  private SNPanel sNPanel2;
  private SNFournisseur snFournisseur;
  private XRiCheckBox ckTousFournisseur;
  private SNLabelChamp lbPeriodeEdition;
  private XRiCalendrier DATFIN;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox WGENB;
  private XRiCheckBox WHOM;
  private XRiCheckBox WRAZ;
  private XRiCheckBox XROT;
  private XRiCheckBox EDTNDP;
  private SNPanel sNPanel1;
  private SNLabelChamp lbRegle;
  private XRiTextField CODDP;
  private JLabel OBJ_105;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
