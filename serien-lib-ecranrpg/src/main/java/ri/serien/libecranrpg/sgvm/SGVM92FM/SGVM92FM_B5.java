
package ri.serien.libecranrpg.sgvm.SGVM92FM;
// Nom Fichier: b_SGVM92FM_FMTB5_FMTF1_565.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGVM92FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _table1_Title = { "A", "B", "C", "D", };
  private String[][] _table1_Data =
      { { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, },
          { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, },
          { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, },
          { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, }, { null, null, null, null, }, };
  private int[] _table1_Width = { 70, 200, 70, 30, };
  
  public SGVM92FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    table1.setModel(new DefaultTableModel(new Object[][] {
        { lexique.HostFieldGetData("CTS001"), lexique.HostFieldGetData("CTN001"), lexique.HostFieldGetData("CTM001"),
            lexique.HostFieldGetData("CTG001") },
        { lexique.HostFieldGetData("CTS002"), lexique.HostFieldGetData("CTN002"), lexique.HostFieldGetData("CTM002"),
            lexique.HostFieldGetData("CTG002") },
        { lexique.HostFieldGetData("CTS003"), lexique.HostFieldGetData("CTN003"), lexique.HostFieldGetData("CTM003"),
            lexique.HostFieldGetData("CTG003") },
        { lexique.HostFieldGetData("CTS004"), lexique.HostFieldGetData("CTN004"), lexique.HostFieldGetData("CTM004"),
            lexique.HostFieldGetData("CTG004") },
        { lexique.HostFieldGetData("CTS005"), lexique.HostFieldGetData("CTN005"), lexique.HostFieldGetData("CTM005"),
            lexique.HostFieldGetData("CTG005") },
        { lexique.HostFieldGetData("CTS006"), lexique.HostFieldGetData("CTN006"), lexique.HostFieldGetData("CTM006"),
            lexique.HostFieldGetData("CTG006") },
        { lexique.HostFieldGetData("CTS007"), lexique.HostFieldGetData("CTN007"), lexique.HostFieldGetData("CTM007"),
            lexique.HostFieldGetData("CTG007") },
        { lexique.HostFieldGetData("CTS008"), lexique.HostFieldGetData("CTN008"), lexique.HostFieldGetData("CTM008"),
            lexique.HostFieldGetData("CTG008") },
        { lexique.HostFieldGetData("CTS009"), lexique.HostFieldGetData("CTN009"), lexique.HostFieldGetData("CTM009"),
            lexique.HostFieldGetData("CTG009") },
        { lexique.HostFieldGetData("CTS010"), lexique.HostFieldGetData("CTN010"), lexique.HostFieldGetData("CTM010"),
            lexique.HostFieldGetData("CTG010") },
        { lexique.HostFieldGetData("CTS011"), lexique.HostFieldGetData("CTN011"), lexique.HostFieldGetData("CTM011"),
            lexique.HostFieldGetData("CTG011") },
        { lexique.HostFieldGetData("CTS012"), lexique.HostFieldGetData("CTN012"), lexique.HostFieldGetData("CTM012"),
            lexique.HostFieldGetData("CTG012") },
        { lexique.HostFieldGetData("CTS013"), lexique.HostFieldGetData("CTN013"), lexique.HostFieldGetData("CTM013"),
            lexique.HostFieldGetData("CTG013") },
        { lexique.HostFieldGetData("CTS014"), lexique.HostFieldGetData("CTN014"), lexique.HostFieldGetData("CTM014"),
            lexique.HostFieldGetData("CTG014") },
        { lexique.HostFieldGetData("CTS015"), lexique.HostFieldGetData("CTN015"), lexique.HostFieldGetData("CTM015"),
            lexique.HostFieldGetData("CTG015") },
        { lexique.HostFieldGetData("CTS016"), lexique.HostFieldGetData("CTN016"), lexique.HostFieldGetData("CTM016"),
            lexique.HostFieldGetData("CTG016") },
        { lexique.HostFieldGetData("CTS017"), lexique.HostFieldGetData("CTN017"), lexique.HostFieldGetData("CTM017"),
            lexique.HostFieldGetData("CTG017") },
        { lexique.HostFieldGetData("CTS018"), lexique.HostFieldGetData("CTN018"), lexique.HostFieldGetData("CTM018"),
            lexique.HostFieldGetData("CTG018") },
        { lexique.HostFieldGetData("CTS019"), lexique.HostFieldGetData("CTN019"), lexique.HostFieldGetData("CTM019"),
            lexique.HostFieldGetData("CTG019") },
        { lexique.HostFieldGetData("CTS020"), lexique.HostFieldGetData("CTN020"), lexique.HostFieldGetData("CTM020"),
            lexique.HostFieldGetData("CTG020") },
        { lexique.HostFieldGetData("CTS021"), lexique.HostFieldGetData("CTN021"), lexique.HostFieldGetData("CTM021"),
            lexique.HostFieldGetData("CTG021") },
        { lexique.HostFieldGetData("CTS022"), lexique.HostFieldGetData("CTN022"), lexique.HostFieldGetData("CTM022"),
            lexique.HostFieldGetData("CTG022") },
        { lexique.HostFieldGetData("CTS023"), lexique.HostFieldGetData("CTN023"), lexique.HostFieldGetData("CTM023"),
            lexique.HostFieldGetData("CTG023") },
        { lexique.HostFieldGetData("CTS024"), lexique.HostFieldGetData("CTN024"), lexique.HostFieldGetData("CTM024"),
            lexique.HostFieldGetData("CTG024") },
        { lexique.HostFieldGetData("CTS025"), lexique.HostFieldGetData("CTN025"), lexique.HostFieldGetData("CTM025"),
            lexique.HostFieldGetData("CTG025") },
        { lexique.HostFieldGetData("CTS026"), lexique.HostFieldGetData("CTN026"), lexique.HostFieldGetData("CTM026"),
            lexique.HostFieldGetData("CTG026") },
        { lexique.HostFieldGetData("CTS027"), lexique.HostFieldGetData("CTN027"), lexique.HostFieldGetData("CTM027"),
            lexique.HostFieldGetData("CTG027") },
        { lexique.HostFieldGetData("CTS028"), lexique.HostFieldGetData("CTN028"), lexique.HostFieldGetData("CTM028"),
            lexique.HostFieldGetData("CTG028") },
        { lexique.HostFieldGetData("CTS029"), lexique.HostFieldGetData("CTN029"), lexique.HostFieldGetData("CTM029"),
            lexique.HostFieldGetData("CTG029") },
        { lexique.HostFieldGetData("CTS030"), lexique.HostFieldGetData("CTN030"), lexique.HostFieldGetData("CTM030"),
            lexique.HostFieldGetData("CTG030") },
        { lexique.HostFieldGetData("CTS031"), lexique.HostFieldGetData("CTN031"), lexique.HostFieldGetData("CTM031"),
            lexique.HostFieldGetData("CTG031") },
        { lexique.HostFieldGetData("CTS032"), lexique.HostFieldGetData("CTN032"), lexique.HostFieldGetData("CTM032"),
            lexique.HostFieldGetData("CTG032") }, },
        new String[] { "Etb", "Identification", "Purge", "Mg" }) {
      boolean[] columnEditable = new boolean[] { false, false, false, false, false, false, false, false, false };
      
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnEditable[columnIndex];
      }
    });
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    // xH_Titre.setIcon(ManagerSessionClient.getInstance().getLogoImage(#ETB#.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvm92"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    scrollPane1 = new JScrollPane();
    table1 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    OBJ_50 = new JLabel();
    SCROLLPANE_LIST3 = new JScrollPane();
    LIST3 = new XRiTable();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap(881, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addComponent(bt_Fonctions)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Soci\u00e9t\u00e9(s) s\u00e9lectionn\u00e9e(s)"));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");

          //---- table1 ----
          table1.setName("table1");
          scrollPane1.setViewportView(table1);
        }
        panel1.add(scrollPane1);
        scrollPane1.setBounds(20, 30, 415, 290);

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        BT_PGUP.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGUPActionPerformed(e);
          }
        });
        panel1.add(BT_PGUP);
        BT_PGUP.setBounds(445, 30, 25, 135);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        BT_PGDOWN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGDOWNActionPerformed(e);
          }
        });
        panel1.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(445, 185, 25, 135);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 490, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(5, 5, 5)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //---- OBJ_50 ----
    OBJ_50.setText("Soci\u00e9t\u00e9 (s) S\u00e9lectionn\u00e9e (s)");
    OBJ_50.setName("OBJ_50");

    //======== SCROLLPANE_LIST3 ========
    {
      SCROLLPANE_LIST3.setComponentPopupMenu(BTD);
      SCROLLPANE_LIST3.setName("SCROLLPANE_LIST3");

      //---- LIST3 ----
      LIST3.setModel(new DefaultTableModel(
        new Object[][] {
          {"@CTS001@", "@CTN001@", "@CTM001@", "@CTG001@", "@null@", "@CTS001@", "@CTN001@", "@CTM001@", "@CTG001@"},
          {"@CTS002@", "@CTN002@", "@CTM002@", "@CTG002@", "@null@", "@CTS002@", "@CTN002@", "@CTM002@", "@CTG002@"},
          {"@CTS003@", "@CTN003@", "@CTM003@", "@CTG003@", "@null@", "@CTS003@", "@CTN003@", "@CTM003@", "@CTG003@"},
          {"@CTS004@", "@CTN004@", "@CTM004@", "@CTG004@", "@null@", "@CTS004@", "@CTN004@", "@CTM004@", "@CTG004@"},
          {"@CTS005@", "@CTN005@", "@CTM005@", "@CTG005@", "@null@", "@CTS005@", "@CTN005@", "@CTM005@", "@CTG005@"},
          {"@CTS006@", "@CTN006@", "@CTM006@", "@CTG006@", "@null@", "@CTS006@", "@CTN006@", "@CTM006@", "@CTG006@"},
          {"@CTS007@", "@CTN007@", "@CTM007@", "@CTG007@", "@null@", "@CTS007@", "@CTN007@", "@CTM007@", "@CTG007@"},
          {"@CTS008@", "@CTN008@", "@CTM008@", "@CTG008@", "@null@", "@CTS008@", "@CTN008@", "@CTM008@", "@CTG008@"},
          {"@CTS009@", "@CTN009@", "@CTM009@", "@CTG009@", "@null@", "@CTS009@", "@CTN009@", "@CTM009@", "@CTG009@"},
          {"@CTS010@", "@CTN010@", "@CTM010@", "@CTG010@", "@null@", "@CTS010@", "@CTN010@", "@CTM010@", "@CTG010@"},
          {"@CTS011@", "@CTN011@", "@CTM011@", "@CTG011@", "@null@", "@CTS011@", "@CTN011@", "@CTM011@", "@CTG011@"},
          {"@CTS012@", "@CTN012@", "@CTM012@", "@CTG012@", "@null@", "@CTS012@", "@CTN012@", "@CTM012@", "@CTG012@"},
          {"@CTS013@", "@CTN013@", "@CTM013@", "@CTG013@", "@null@", "@CTS013@", "@CTN013@", "@CTM013@", "@CTG013@"},
          {"@CTS014@", "@CTN014@", "@CTM014@", "@CTG014@", "@null@", "@CTS014@", "@CTN014@", "@CTM014@", "@CTG014@"},
          {"@CTS015@", "@CTN015@", "@CTM015@", "@CTG015@", "@null@", "@CTS015@", "@CTN015@", "@CTM015@", "@CTG015@"},
          {null, null, null, null, null, null, null, null, null},
        },
        new String[] {
          "Etb", "Identification", "Purge", "Mg", "", "Etb", "Identification", "Purge", "Mg"
        }
      ) {
        boolean[] columnEditable = new boolean[] {
          false, false, false, false, false, false, false, false, false
        };
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
          return columnEditable[columnIndex];
        }
      });
      {
        TableColumnModel cm = LIST3.getColumnModel();
        cm.getColumn(0).setPreferredWidth(28);
        cm.getColumn(1).setPreferredWidth(205);
        cm.getColumn(2).setPreferredWidth(48);
        cm.getColumn(3).setPreferredWidth(19);
        cm.getColumn(4).setPreferredWidth(3);
        cm.getColumn(5).setPreferredWidth(28);
        cm.getColumn(6).setPreferredWidth(205);
        cm.getColumn(7).setPreferredWidth(48);
        cm.getColumn(8).setPreferredWidth(19);
      }
      LIST3.setName("LIST3");
      SCROLLPANE_LIST3.setViewportView(LIST3);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private JPanel panel1;
  private JScrollPane scrollPane1;
  private XRiTable table1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JLabel OBJ_50;
  private JScrollPane SCROLLPANE_LIST3;
  private XRiTable LIST3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
