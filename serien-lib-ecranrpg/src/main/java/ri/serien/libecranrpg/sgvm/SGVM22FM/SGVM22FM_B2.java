
package ri.serien.libecranrpg.sgvm.SGVM22FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.EnumOrdreCritereDeTriEtDeSelection;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostal.SNCodePostal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.affaire.snaffaire.SNAffaire;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * [GVM2811] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Factures
 * Indicateur : 00000001
 * Titre : Tri de l'édition (Boite de dialogue)
 * 
 * [GVM2812] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Duplicatas de factures
 * Indicateur : 00010001
 * Titre : Tri de l'édition (Boite de dialogue)
 * 
 * [GVM2813] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Factures/traites
 * Indicateur : 00001001
 * Titre : Tri de l'édition (Boite de dialogue)
 * 
 * [GVM2814] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Duplicatas de factures/traites
 * Indicateur : 00011001
 * Titre : Tri de l'édition (Boite de dialogue)
 * 
 * [GVM2816] Gestion des ventes -> Documents de ventes -> Editions factures et traites -> Factures -> Dématérialisation des factures
 * Indicateur : 00010101
 * Titre : Tri de l'édition (Boite de dialogue)
 */
public class SGVM22FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private ArrayList<SNComboBox> listeComboTri = new ArrayList<SNComboBox>();
  private ArrayList<EnumOrdreCritereDeTriEtDeSelection> listeEnumOrdreDeTri = new ArrayList<EnumOrdreCritereDeTriEtDeSelection>();
  private boolean isInitialise = false;
  
  public SGVM22FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    setTitle("Critères de tri et de sélection");
    // Ajout
    initDiverses();
    
    snCanalDeVenteDebut.lierComposantFin(snCanalDeVenteFin);
    snCategorieClientDebut.lierComposantFin(snCategorieClientFin);
    
    // Chargement de la liste des valeurs de l'EnumOrdreTri
    for (int i = 0; i < EnumOrdreCritereDeTriEtDeSelection.values().length; i++) {
      listeEnumOrdreDeTri.add(EnumOrdreCritereDeTriEtDeSelection.values()[i]);
    }
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
    // Ajout des Combobox dans listComboTri
    listeComboTri.add(cbTriRepresentant);
    listeComboTri.add(cbTriVendeur);
    listeComboTri.add(cbTriMagasin);
    listeComboTri.add(cbTriSectionAnalytique);
    listeComboTri.add(cbTriAffaire);
    listeComboTri.add(cbTriClient);
    listeComboTri.add(cbTriCodePostal);
    listeComboTri.add(cbTriNumeroBon);
    listeComboTri.add(cbTriCanaleDeVente);
    listeComboTri.add(cbTriCategorieClient);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    // Activation des composant
    snMagasin.setEnabled(lexique.isPresent("WMAG"));
    snCodePostal.setEnabled(lexique.isPresent("WCDP"));
    snAffaire.setEnabled(lexique.isPresent("WACT"));
    snSectionAnalytique.setEnabled(lexique.isPresent("WSEC"));
    WNBON.setEnabled(lexique.isPresent("WNBON"));
    snCanalDeVenteDebut.setEnabled(lexique.isPresent("WCAND"));
    snCanalDeVenteFin.setEnabled(lexique.isPresent("WCANF"));
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // initialise les combobox de critères de tri
    initialiserCombo();
    
    // Charge le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // Charge les catégories client
    snCategorieClientDebut.setSession(getSession());
    snCategorieClientDebut.setIdEtablissement(idEtablissement);
    snCategorieClientDebut.setTousAutorise(true);
    snCategorieClientDebut.charger(false);
    snCategorieClientDebut.setSelectionParChampRPG(lexique, "WCATD");
    
    snCategorieClientFin.setSession(getSession());
    snCategorieClientFin.setIdEtablissement(idEtablissement);
    snCategorieClientFin.setTousAutorise(true);
    snCategorieClientFin.charger(false);
    snCategorieClientFin.setSelectionParChampRPG(lexique, "WCATF");
    
    // Charge le representant
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(idEtablissement);
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "WREP");
    
    // Charge le vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(idEtablissement);
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "WVDE");
    
    // Charge l'affaire
    snAffaire.setSession(getSession());
    snAffaire.setIdEtablissement(idEtablissement);
    snAffaire.setTousAutorise(true);
    snAffaire.charger(false);
    snAffaire.setSelectionParChampRPG(lexique, "WACT");
    
    // Charge le client
    snClient.setSession(getSession());
    snClient.setIdEtablissement(idEtablissement);
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "WCLI");
    
    // Charge la section analytique
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(idEtablissement);
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "WSEC");
    
    // Charge les canaux de vente
    snCanalDeVenteDebut.setSession(getSession());
    snCanalDeVenteDebut.setIdEtablissement(idEtablissement);
    snCanalDeVenteDebut.setTousAutorise(true);
    snCanalDeVenteDebut.charger(false);
    snCanalDeVenteDebut.setSelectionParChampRPG(lexique, "WCAND");
    
    snCanalDeVenteFin.setSession(getSession());
    snCanalDeVenteFin.setIdEtablissement(idEtablissement);
    snCanalDeVenteFin.setTousAutorise(true);
    snCanalDeVenteFin.charger(false);
    snCanalDeVenteFin.setSelectionParChampRPG(lexique, "WCANF");
    
    // Charge le code postal
    snCodePostal.setSession(getSession());
    snCodePostal.setIdEtablissement(idEtablissement);
    snCodePostal.setTousAutorise(true);
    snCodePostal.charger(false);
    snCodePostal.setSelectionParChampRPG(lexique, "WCDP");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snCategorieClientDebut.renseignerChampRPG(lexique, "WCATD");
    snCategorieClientFin.renseignerChampRPG(lexique, "WCATF");
    snRepresentant.renseignerChampRPG(lexique, "WREP");
    snVendeur.renseignerChampRPG(lexique, "WVDE");
    snAffaire.renseignerChampRPG(lexique, "WACT");
    snClient.renseignerChampRPG(lexique, "WCLI");
    snSectionAnalytique.renseignerChampRPG(lexique, "WSEC");
    snCanalDeVenteDebut.renseignerChampRPG(lexique, "WCAND");
    snCanalDeVenteFin.renseignerChampRPG(lexique, "WCANF");
    snCodePostal.renseignerChampRPG(lexique, "WCDP");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * initialise les combobox de critères de tri
   */
  private void initialiserCombo() {
    isInitialise = false;
    for (SNComboBox combo : listeComboTri) {
      // initialise les combo n'ayant aucune sélection avec les valeur de listeEnumOrdreDeTri
      if (combo.getSelectedItem() == null || combo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        combo.removeAllItems();
        for (EnumOrdreCritereDeTriEtDeSelection enumTri : listeEnumOrdreDeTri) {
          combo.addItem(enumTri);
        }
      }
      else {
        // Permet de mettre à jour les combo ayant une sélection sans pour autant perdre celle ci
        EnumOrdreCritereDeTriEtDeSelection selectionEnCours = (EnumOrdreCritereDeTriEtDeSelection) combo.getSelectedItem();
        combo.removeAllItems();
        for (EnumOrdreCritereDeTriEtDeSelection enumTri : listeEnumOrdreDeTri) {
          combo.addItem(enumTri);
        }
        combo.addItem(selectionEnCours);
        combo.setSelectedItem(selectionEnCours);
      }
    }
    isInitialise = true;
  }
  
  /**
   * gére les sélection des combo ainsi que la liste des EnumOrdreCriteresDeTriEtSelection
   */
  private void traiterSelectionCombo(ItemEvent pE, SNComboBox pCombo, String champRPG) {
    // Récupère la valeur qui à étais désélectionner afin de la rajouter à listeEnumOrdreDeTri si cette valeur n'existe pas dans celle-ci
    if (pE.getStateChange() == ItemEvent.DESELECTED) {
      if (!listeEnumOrdreDeTri.contains(pE.getItem())) {
        listeEnumOrdreDeTri.add((EnumOrdreCritereDeTriEtDeSelection) pE.getItem());
      }
    }
    // si on a une selection valide
    if (pCombo.getSelectedItem() != null) {
      if (!pCombo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        listeEnumOrdreDeTri.remove(pCombo.getSelectedItem());
      }
      initialiserCombo();
      if (pCombo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        lexique.HostFieldPutData(champRPG, 0, " ");
      }
      else {
        lexique.HostFieldPutData(champRPG, 0, ((EnumOrdreCritereDeTriEtDeSelection) pCombo.getSelectedItem()).getStringCode());
      }
    }
  }
  
  /**
   * gestion de la combo cbTriRepresentannt
   */
  private void cbRepresentantItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriRepresentant, "WTI1");
    }
  }
  
  /**
   * gestion de la combo cbVendeur
   */
  private void cbVendeurItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriVendeur, "WTI2");
    }
  }
  
  /**
   * gestion de la combo cbMagasin
   */
  private void cbMagasinItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriMagasin, "WTI3");
    }
  }
  
  /**
   * gestion de la combo cbSectionAnalytique
   */
  private void cbSectionAnalytiqueItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriSectionAnalytique, "WTI4");
    }
  }
  
  /**
   * gestion de la combo cbAffaire
   */
  private void cbAffaireItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriAffaire, "WTI5");
    }
  }
  
  /**
   * gestion de la combo cbClient
   */
  private void cbClientItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriClient, "WTI6");
    }
  }
  
  /**
   * gestion de la combo cbCodePostal
   */
  private void cbCodePostalItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriCodePostal, "WTI8");
    }
  }
  
  /**
   * gestion de la combo cbNumeroBon
   */
  private void cbNumeroBonItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriNumeroBon, "WTI9");
    }
  }
  
  /**
   * gestion de la combo cbCanalDeVente
   */
  private void cbCanaleDeVenteItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriCanaleDeVente, "WTI10");
    }
  }
  
  /**
   * gestion de la combo cbCategorieClient
   */
  private void cbCategorieClientItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriCategorieClient, "WTI11");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlCriteres = new SNPanel();
    lbClient = new SNLabelChamp();
    lbCodePostal = new SNLabelChamp();
    lbRepresentant = new SNLabelChamp();
    cbTriRepresentant = new SNComboBox();
    snRepresentant = new SNRepresentant();
    lbVendeur = new SNLabelChamp();
    cbTriVendeur = new SNComboBox();
    snVendeur = new SNVendeur();
    lbMagasin = new SNLabelChamp();
    cbTriMagasin = new SNComboBox();
    lbAffaire = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbSectionAnalytique = new SNLabelChamp();
    cbTriSectionAnalytique = new SNComboBox();
    snSectionAnalytique = new SNSectionAnalytique();
    cbTriAffaire = new SNComboBox();
    snAffaire = new SNAffaire();
    cbTriClient = new SNComboBox();
    snClient = new SNClientPrincipal();
    cbTriCodePostal = new SNComboBox();
    snCodePostal = new SNCodePostal();
    lbNumeroDeBon = new SNLabelChamp();
    cbTriNumeroBon = new SNComboBox();
    lbCanalDeVente = new SNLabelChamp();
    WNBON = new XRiTextField();
    cbTriCanaleDeVente = new SNComboBox();
    pnlCanalVente = new SNPanel();
    snCanalDeVenteDebut = new SNCanalDeVente();
    lbACanalDeVente = new SNLabelChamp();
    snCanalDeVenteFin = new SNCanalDeVente();
    lbCategorieClient = new SNLabelChamp();
    cbTriCategorieClient = new SNComboBox();
    pnlCategorieClient = new SNPanel();
    snCategorieClientDebut = new SNCategorieClient();
    lbA = new SNLabelChamp();
    snCategorieClientFin = new SNCategorieClient();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(1000, 440));
    setPreferredSize(new Dimension(1000, 440));
    setMaximumSize(new Dimension(1000, 440));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

      //======== pnlCriteres ========
      {
        pnlCriteres.setPreferredSize(new Dimension(125, 30));
        pnlCriteres.setMinimumSize(new Dimension(125, 30));
        pnlCriteres.setMaximumSize(new Dimension(125, 30));
        pnlCriteres.setName("pnlCriteres");
        pnlCriteres.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCriteres.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlCriteres.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCriteres.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlCriteres.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbClient ----
        lbClient.setText("Client");
        lbClient.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbClient.setPreferredSize(new Dimension(125, 30));
        lbClient.setMinimumSize(new Dimension(125, 30));
        lbClient.setMaximumSize(new Dimension(125, 30));
        lbClient.setName("lbClient");
        pnlCriteres.add(lbClient, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbCodePostal ----
        lbCodePostal.setText("Code postal");
        lbCodePostal.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal.setPreferredSize(new Dimension(125, 30));
        lbCodePostal.setMinimumSize(new Dimension(125, 30));
        lbCodePostal.setMaximumSize(new Dimension(125, 30));
        lbCodePostal.setName("lbCodePostal");
        pnlCriteres.add(lbCodePostal, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbRepresentant ----
        lbRepresentant.setText("Representant");
        lbRepresentant.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRepresentant.setMinimumSize(new Dimension(125, 30));
        lbRepresentant.setPreferredSize(new Dimension(125, 30));
        lbRepresentant.setMaximumSize(new Dimension(125, 30));
        lbRepresentant.setName("lbRepresentant");
        pnlCriteres.add(lbRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriRepresentant ----
        cbTriRepresentant.setPreferredSize(new Dimension(155, 30));
        cbTriRepresentant.setMinimumSize(new Dimension(155, 30));
        cbTriRepresentant.setMaximumSize(new Dimension(155, 30));
        cbTriRepresentant.setName("cbTriRepresentant");
        cbTriRepresentant.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbRepresentantItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriRepresentant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snRepresentant ----
        snRepresentant.setName("snRepresentant");
        pnlCriteres.add(snRepresentant, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbVendeur ----
        lbVendeur.setText("Vendeur");
        lbVendeur.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbVendeur.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbVendeur.setPreferredSize(new Dimension(125, 30));
        lbVendeur.setMinimumSize(new Dimension(125, 30));
        lbVendeur.setMaximumSize(new Dimension(125, 30));
        lbVendeur.setName("lbVendeur");
        pnlCriteres.add(lbVendeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriVendeur ----
        cbTriVendeur.setPreferredSize(new Dimension(155, 30));
        cbTriVendeur.setMinimumSize(new Dimension(155, 30));
        cbTriVendeur.setMaximumSize(new Dimension(155, 30));
        cbTriVendeur.setName("cbTriVendeur");
        cbTriVendeur.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbVendeurItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snVendeur ----
        snVendeur.setName("snVendeur");
        pnlCriteres.add(snVendeur, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMagasin ----
        lbMagasin.setText("Magasin");
        lbMagasin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbMagasin.setMinimumSize(new Dimension(125, 30));
        lbMagasin.setPreferredSize(new Dimension(125, 30));
        lbMagasin.setMaximumSize(new Dimension(125, 30));
        lbMagasin.setName("lbMagasin");
        pnlCriteres.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriMagasin ----
        cbTriMagasin.setPreferredSize(new Dimension(155, 30));
        cbTriMagasin.setMinimumSize(new Dimension(155, 30));
        cbTriMagasin.setMaximumSize(new Dimension(155, 30));
        cbTriMagasin.setName("cbTriMagasin");
        cbTriMagasin.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbMagasinItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbAffaire ----
        lbAffaire.setText("Affaire");
        lbAffaire.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbAffaire.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbAffaire.setMinimumSize(new Dimension(125, 30));
        lbAffaire.setPreferredSize(new Dimension(125, 30));
        lbAffaire.setMaximumSize(new Dimension(125, 30));
        lbAffaire.setName("lbAffaire");
        pnlCriteres.add(lbAffaire, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snMagasin ----
        snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        snMagasin.setName("snMagasin");
        pnlCriteres.add(snMagasin, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbSectionAnalytique ----
        lbSectionAnalytique.setText("Section analytique");
        lbSectionAnalytique.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbSectionAnalytique.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbSectionAnalytique.setPreferredSize(new Dimension(125, 30));
        lbSectionAnalytique.setMinimumSize(new Dimension(125, 30));
        lbSectionAnalytique.setMaximumSize(new Dimension(125, 30));
        lbSectionAnalytique.setName("lbSectionAnalytique");
        pnlCriteres.add(lbSectionAnalytique, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriSectionAnalytique ----
        cbTriSectionAnalytique.setPreferredSize(new Dimension(155, 30));
        cbTriSectionAnalytique.setMinimumSize(new Dimension(155, 30));
        cbTriSectionAnalytique.setMaximumSize(new Dimension(155, 30));
        cbTriSectionAnalytique.setName("cbTriSectionAnalytique");
        cbTriSectionAnalytique.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbSectionAnalytiqueItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriSectionAnalytique, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snSectionAnalytique ----
        snSectionAnalytique.setName("snSectionAnalytique");
        pnlCriteres.add(snSectionAnalytique, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- cbTriAffaire ----
        cbTriAffaire.setPreferredSize(new Dimension(155, 30));
        cbTriAffaire.setMinimumSize(new Dimension(155, 30));
        cbTriAffaire.setMaximumSize(new Dimension(155, 30));
        cbTriAffaire.setName("cbTriAffaire");
        cbTriAffaire.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbAffaireItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriAffaire, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snAffaire ----
        snAffaire.setName("snAffaire");
        pnlCriteres.add(snAffaire, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- cbTriClient ----
        cbTriClient.setPreferredSize(new Dimension(155, 30));
        cbTriClient.setMinimumSize(new Dimension(155, 30));
        cbTriClient.setMaximumSize(new Dimension(155, 30));
        cbTriClient.setName("cbTriClient");
        cbTriClient.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbClientItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriClient, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snClient ----
        snClient.setName("snClient");
        pnlCriteres.add(snClient, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- cbTriCodePostal ----
        cbTriCodePostal.setPreferredSize(new Dimension(155, 30));
        cbTriCodePostal.setMinimumSize(new Dimension(155, 30));
        cbTriCodePostal.setMaximumSize(new Dimension(155, 30));
        cbTriCodePostal.setName("cbTriCodePostal");
        cbTriCodePostal.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbCodePostalItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriCodePostal, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snCodePostal ----
        snCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        snCodePostal.setName("snCodePostal");
        pnlCriteres.add(snCodePostal, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNumeroDeBon ----
        lbNumeroDeBon.setText("Num\u00e9ro de bon");
        lbNumeroDeBon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbNumeroDeBon.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNumeroDeBon.setMinimumSize(new Dimension(125, 30));
        lbNumeroDeBon.setPreferredSize(new Dimension(125, 30));
        lbNumeroDeBon.setMaximumSize(new Dimension(125, 30));
        lbNumeroDeBon.setName("lbNumeroDeBon");
        pnlCriteres.add(lbNumeroDeBon, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriNumeroBon ----
        cbTriNumeroBon.setPreferredSize(new Dimension(155, 30));
        cbTriNumeroBon.setMinimumSize(new Dimension(155, 30));
        cbTriNumeroBon.setMaximumSize(new Dimension(155, 30));
        cbTriNumeroBon.setName("cbTriNumeroBon");
        cbTriNumeroBon.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbNumeroBonItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriNumeroBon, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbCanalDeVente ----
        lbCanalDeVente.setText("Canal de vente");
        lbCanalDeVente.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbCanalDeVente.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCanalDeVente.setPreferredSize(new Dimension(125, 30));
        lbCanalDeVente.setMinimumSize(new Dimension(125, 30));
        lbCanalDeVente.setMaximumSize(new Dimension(125, 30));
        lbCanalDeVente.setName("lbCanalDeVente");
        pnlCriteres.add(lbCanalDeVente, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNBON ----
        WNBON.setPreferredSize(new Dimension(70, 30));
        WNBON.setMinimumSize(new Dimension(70, 30));
        WNBON.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNBON.setName("WNBON");
        pnlCriteres.add(WNBON, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- cbTriCanaleDeVente ----
        cbTriCanaleDeVente.setPreferredSize(new Dimension(155, 30));
        cbTriCanaleDeVente.setMinimumSize(new Dimension(155, 30));
        cbTriCanaleDeVente.setMaximumSize(new Dimension(155, 30));
        cbTriCanaleDeVente.setName("cbTriCanaleDeVente");
        cbTriCanaleDeVente.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbCanaleDeVenteItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriCanaleDeVente, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== pnlCanalVente ========
        {
          pnlCanalVente.setName("pnlCanalVente");
          pnlCanalVente.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCanalVente.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCanalVente.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlCanalVente.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCanalVente.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- snCanalDeVenteDebut ----
          snCanalDeVenteDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          snCanalDeVenteDebut.setName("snCanalDeVenteDebut");
          pnlCanalVente.add(snCanalDeVenteDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbACanalDeVente ----
          lbACanalDeVente.setText("\u00e0");
          lbACanalDeVente.setMinimumSize(new Dimension(15, 30));
          lbACanalDeVente.setPreferredSize(new Dimension(15, 30));
          lbACanalDeVente.setHorizontalAlignment(SwingConstants.LEFT);
          lbACanalDeVente.setName("lbACanalDeVente");
          pnlCanalVente.add(lbACanalDeVente, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snCanalDeVenteFin ----
          snCanalDeVenteFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snCanalDeVenteFin.setName("snCanalDeVenteFin");
          pnlCanalVente.add(snCanalDeVenteFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCriteres.add(pnlCanalVente, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbCategorieClient ----
        lbCategorieClient.setText("Cat\u00e9gorie client");
        lbCategorieClient.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbCategorieClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCategorieClient.setMinimumSize(new Dimension(125, 30));
        lbCategorieClient.setPreferredSize(new Dimension(125, 30));
        lbCategorieClient.setMaximumSize(new Dimension(125, 30));
        lbCategorieClient.setName("lbCategorieClient");
        pnlCriteres.add(lbCategorieClient, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- cbTriCategorieClient ----
        cbTriCategorieClient.setPreferredSize(new Dimension(155, 30));
        cbTriCategorieClient.setMinimumSize(new Dimension(155, 30));
        cbTriCategorieClient.setMaximumSize(new Dimension(155, 30));
        cbTriCategorieClient.setName("cbTriCategorieClient");
        cbTriCategorieClient.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbCategorieClientItemStateChanged(e);
          }
        });
        pnlCriteres.add(cbTriCategorieClient, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlCategorieClient ========
        {
          pnlCategorieClient.setName("pnlCategorieClient");
          pnlCategorieClient.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCategorieClient.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCategorieClient.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlCategorieClient.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCategorieClient.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- snCategorieClientDebut ----
          snCategorieClientDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          snCategorieClientDebut.setName("snCategorieClientDebut");
          pnlCategorieClient.add(snCategorieClientDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbA ----
          lbA.setText("\u00e0");
          lbA.setPreferredSize(new Dimension(15, 30));
          lbA.setMinimumSize(new Dimension(15, 30));
          lbA.setHorizontalAlignment(SwingConstants.LEFT);
          lbA.setName("lbA");
          pnlCategorieClient.add(lbA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snCategorieClientFin ----
          snCategorieClientFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snCategorieClientFin.setName("snCategorieClientFin");
          pnlCategorieClient.add(snCategorieClientFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCriteres.add(pnlCategorieClient, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCriteres, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanel pnlCriteres;
  private SNLabelChamp lbClient;
  private SNLabelChamp lbCodePostal;
  private SNLabelChamp lbRepresentant;
  private SNComboBox cbTriRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbVendeur;
  private SNComboBox cbTriVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbMagasin;
  private SNComboBox cbTriMagasin;
  private SNLabelChamp lbAffaire;
  private SNMagasin snMagasin;
  private SNLabelChamp lbSectionAnalytique;
  private SNComboBox cbTriSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNComboBox cbTriAffaire;
  private SNAffaire snAffaire;
  private SNComboBox cbTriClient;
  private SNClientPrincipal snClient;
  private SNComboBox cbTriCodePostal;
  private SNCodePostal snCodePostal;
  private SNLabelChamp lbNumeroDeBon;
  private SNComboBox cbTriNumeroBon;
  private SNLabelChamp lbCanalDeVente;
  private XRiTextField WNBON;
  private SNComboBox cbTriCanaleDeVente;
  private SNPanel pnlCanalVente;
  private SNCanalDeVente snCanalDeVenteDebut;
  private SNLabelChamp lbACanalDeVente;
  private SNCanalDeVente snCanalDeVenteFin;
  private SNLabelChamp lbCategorieClient;
  private SNComboBox cbTriCategorieClient;
  private SNPanel pnlCategorieClient;
  private SNCategorieClient snCategorieClientDebut;
  private SNLabelChamp lbA;
  private SNCategorieClient snCategorieClientFin;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
