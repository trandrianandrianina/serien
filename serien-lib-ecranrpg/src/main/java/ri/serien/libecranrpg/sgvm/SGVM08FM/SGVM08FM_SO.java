
package ri.serien.libecranrpg.sgvm.SGVM08FM;
// Nom Fichier: pop_SGVM08FM_FMTSO_FMTF1_104.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM08FM_SO extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM08FM_SO(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_6);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TXT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_6.setIcon(lexique.chargerImage("images/OK.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_4 = new JButton();
    OBJ_6 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_5 ----
    OBJ_5.setText("@TXT@");
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(80, 40, 261, 20);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }
    add(P_PnlOpts);
    P_PnlOpts.setBounds(1036, 15, 55, 516);

    //---- OBJ_4 ----
    OBJ_4.setText("");
    OBJ_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_4.setName("OBJ_4");
    add(OBJ_4);
    OBJ_4.setBounds(13, 28, 54, 44);

    //---- OBJ_6 ----
    OBJ_6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_6.setName("OBJ_6");
    add(OBJ_6);
    OBJ_6.setBounds(295, 80, 56, 40);

    setPreferredSize(new Dimension(360, 127));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_5;
  private JPanel P_PnlOpts;
  private JButton OBJ_4;
  private JButton OBJ_6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
