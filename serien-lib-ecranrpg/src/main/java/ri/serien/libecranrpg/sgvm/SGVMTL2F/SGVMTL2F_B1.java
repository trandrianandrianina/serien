
package ri.serien.libecranrpg.sgvm.SGVMTL2F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SGVMTL2F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMTL2F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WCHOIX.setValeurs("1", WCHOIX_GRP);
    WCHOIX_2.setValeurs("2");
    WCHOIX_3.setValeurs("3");
    WCHOIX_4.setValeurs("4");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // WCHOIX_2.setEnabled( lexique.isPresent("WCHOIX"));
    // WCHOIX_2.setSelected(lexique.HostFieldGetData("WCHOIX").equalsIgnoreCase("2"));
    // WCHOIX.setEnabled( lexique.isPresent("WCHOIX"));
    // WCHOIX.setSelected(lexique.HostFieldGetData("WCHOIX").equalsIgnoreCase("1"));
    // WCHOIX_4.setEnabled( lexique.isPresent("WCHOIX"));
    // WCHOIX_4.setSelected(lexique.HostFieldGetData("WCHOIX").equalsIgnoreCase("4"));
    // WCHOIX_3.setEnabled( lexique.isPresent("WCHOIX"));
    // WCHOIX_3.setSelected(lexique.HostFieldGetData("WCHOIX").equalsIgnoreCase("3"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WCHOIX_2.isSelected())
    // lexique.HostFieldPutData("WCHOIX", 0, "2");
    // if (WCHOIX.isSelected())
    // lexique.HostFieldPutData("WCHOIX", 0, "1");
    // if (WCHOIX_4.isSelected())
    // lexique.HostFieldPutData("WCHOIX", 0, "4");
    // if (WCHOIX_3.isSelected())
    // lexique.HostFieldPutData("WCHOIX", 0, "3");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WCHOIX = new XRiRadioButton();
    WCHOIX_2 = new XRiRadioButton();
    WCHOIX_3 = new XRiRadioButton();
    WCHOIX_4 = new XRiRadioButton();
    WCHOIX_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(445, 195));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Tri de la commande"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WCHOIX ----
          WCHOIX.setText("Par code article");
          WCHOIX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCHOIX.setName("WCHOIX");
          panel1.add(WCHOIX);
          WCHOIX.setBounds(20, 30, 135, 20);

          //---- WCHOIX_2 ----
          WCHOIX_2.setText("Par code famille");
          WCHOIX_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCHOIX_2.setName("WCHOIX_2");
          panel1.add(WCHOIX_2);
          WCHOIX_2.setBounds(20, 58, 135, 20);

          //---- WCHOIX_3 ----
          WCHOIX_3.setText("Par code regroupement");
          WCHOIX_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCHOIX_3.setName("WCHOIX_3");
          panel1.add(WCHOIX_3);
          WCHOIX_3.setBounds(20, 86, 190, 20);

          //---- WCHOIX_4 ----
          WCHOIX_4.setText("Par regroupement statistique");
          WCHOIX_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCHOIX_4.setName("WCHOIX_4");
          panel1.add(WCHOIX_4);
          WCHOIX_4.setBounds(20, 114, 205, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- WCHOIX_GRP ----
    WCHOIX_GRP.add(WCHOIX);
    WCHOIX_GRP.add(WCHOIX_2);
    WCHOIX_GRP.add(WCHOIX_3);
    WCHOIX_GRP.add(WCHOIX_4);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton WCHOIX;
  private XRiRadioButton WCHOIX_2;
  private XRiRadioButton WCHOIX_3;
  private XRiRadioButton WCHOIX_4;
  private ButtonGroup WCHOIX_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
