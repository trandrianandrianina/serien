
package ri.serien.libecranrpg.sgvm.SGVM92FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM92FM_B7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM92FM_B7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SAUV@")).trim());
    DGCPLX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGCPLX@")).trim());
    DGDE1X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE1X@")).trim());
    DGFE1X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE1X@")).trim());
    DGDECX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDECX@")).trim());
    DGSUIX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGSUIX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    CODMAG_ck.setVisible(lexique.isPresent("CODMAG"));
    CODMAG_ck.setSelected(lexique.HostFieldGetData("CODMAG").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!CODMAG_ck.isSelected() & CODMAG_ck.isVisible());
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (CODMAG_ck.isSelected()) {
      lexique.HostFieldPutData("CODMAG", 0, "**");
      // else
      // lexique.HostFieldPutData("CODMAG", 0, " ");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void CODMAGActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!CODMAG_ck.isSelected()) {
      CODMAG.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_40 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    OBJ_44 = new JXTitledSeparator();
    OBJ_42 = new JXTitledSeparator();
    OBJ_59 = new JLabel();
    P_SEL0 = new JPanel();
    CODMAG = new XRiTextField();
    label1 = new JLabel();
    DGCPLX = new RiZoneSortie();
    OBJ_58 = new JLabel();
    CODMAG_ck = new JCheckBox();
    DGDE1X = new RiZoneSortie();
    DGFE1X = new RiZoneSortie();
    DGDECX = new RiZoneSortie();
    DGSUIX = new RiZoneSortie();
    OBJ_53 = new JLabel();
    OBJ_56 = new JLabel();
    DATLIM = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(520, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 75, 450, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 100, 260, z_dgnom_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 120, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 115), bouton_etablissement.getPreferredSize()));

          //---- OBJ_40 ----
          OBJ_40.setTitle("Code magasin");
          OBJ_40.setName("OBJ_40");
          p_contenu.add(OBJ_40);
          OBJ_40.setBounds(35, 342, 450, OBJ_40.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setTitle("P\u00e9riode en cours");
          OBJ_43.setName("OBJ_43");
          p_contenu.add(OBJ_43);
          OBJ_43.setBounds(35, 221, 450, OBJ_43.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setTitle("Exercice en cours");
          OBJ_44.setName("OBJ_44");
          p_contenu.add(OBJ_44);
          OBJ_44.setBounds(35, 165, 450, OBJ_44.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setTitle("");
          OBJ_42.setName("OBJ_42");
          p_contenu.add(OBJ_42);
          OBJ_42.setBounds(35, 287, 450, OBJ_42.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setText("@SAUV@");
          OBJ_59.setName("OBJ_59");
          p_contenu.add(OBJ_59);
          OBJ_59.setBounds(40, 35, 460, 20);

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- CODMAG ----
            CODMAG.setComponentPopupMenu(BTD);
            CODMAG.setName("CODMAG");
            P_SEL0.add(CODMAG);
            CODMAG.setBounds(105, 10, 30, CODMAG.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Code");
            label1.setName("label1");
            P_SEL0.add(label1);
            label1.setBounds(15, 14, 55, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL0);
          P_SEL0.setBounds(255, 372, 211, 45);

          //---- DGCPLX ----
          DGCPLX.setText("@DGCPLX@");
          DGCPLX.setName("DGCPLX");
          p_contenu.add(DGCPLX);
          DGCPLX.setBounds(205, 135, 260, DGCPLX.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("Date limite de purge (JJMMAA)");
          OBJ_58.setName("OBJ_58");
          p_contenu.add(OBJ_58);
          OBJ_58.setBounds(50, 311, 193, 20);

          //---- CODMAG_ck ----
          CODMAG_ck.setText("Selection compl\u00e8te");
          CODMAG_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CODMAG_ck.setName("CODMAG_ck");
          CODMAG_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              CODMAGActionPerformed(e);
            }
          });
          p_contenu.add(CODMAG_ck);
          CODMAG_ck.setBounds(50, 384, 140, 20);

          //---- DGDE1X ----
          DGDE1X.setComponentPopupMenu(BTD);
          DGDE1X.setText("@DGDE1X@");
          DGDE1X.setName("DGDE1X");
          p_contenu.add(DGDE1X);
          DGDE1X.setBounds(50, 190, 64, DGDE1X.getPreferredSize().height);

          //---- DGFE1X ----
          DGFE1X.setComponentPopupMenu(BTD);
          DGFE1X.setText("@DGFE1X@");
          DGFE1X.setName("DGFE1X");
          p_contenu.add(DGFE1X);
          DGFE1X.setBounds(205, 190, 64, DGFE1X.getPreferredSize().height);

          //---- DGDECX ----
          DGDECX.setComponentPopupMenu(BTD);
          DGDECX.setText("@DGDECX@");
          DGDECX.setName("DGDECX");
          p_contenu.add(DGDECX);
          DGDECX.setBounds(50, 246, 64, DGDECX.getPreferredSize().height);

          //---- DGSUIX ----
          DGSUIX.setComponentPopupMenu(BTD);
          DGSUIX.setText("@DGSUIX@");
          DGSUIX.setName("DGSUIX");
          p_contenu.add(DGSUIX);
          DGSUIX.setBounds(205, 246, 64, DGSUIX.getPreferredSize().height);

          //---- OBJ_53 ----
          OBJ_53.setText("\u00e0");
          OBJ_53.setName("OBJ_53");
          p_contenu.add(OBJ_53);
          OBJ_53.setBounds(155, 194, 11, 20);

          //---- OBJ_56 ----
          OBJ_56.setText("\u00e0");
          OBJ_56.setName("OBJ_56");
          p_contenu.add(OBJ_56);
          OBJ_56.setBounds(155, 250, 11, 20);

          //---- DATLIM ----
          DATLIM.setName("DATLIM");
          p_contenu.add(DATLIM);
          DATLIM.setBounds(360, 307, 105, DATLIM.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_40;
  private JXTitledSeparator OBJ_43;
  private JXTitledSeparator OBJ_44;
  private JXTitledSeparator OBJ_42;
  private JLabel OBJ_59;
  private JPanel P_SEL0;
  private XRiTextField CODMAG;
  private JLabel label1;
  private RiZoneSortie DGCPLX;
  private JLabel OBJ_58;
  private JCheckBox CODMAG_ck;
  private RiZoneSortie DGDE1X;
  private RiZoneSortie DGFE1X;
  private RiZoneSortie DGDECX;
  private RiZoneSortie DGSUIX;
  private JLabel OBJ_53;
  private JLabel OBJ_56;
  private XRiCalendrier DATLIM;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
