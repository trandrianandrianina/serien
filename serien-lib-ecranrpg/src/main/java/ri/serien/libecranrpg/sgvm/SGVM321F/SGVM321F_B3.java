/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM321F;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM296B] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons facturés -> Ventes par facture et par article
 */
public class SGVM321F_B3 extends SNPanelEcranRPG implements ioFrame {
  
  /**
   * Constructeur.
   */
  public SGVM321F_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    // Lier les composants catégorie clients
    snCategorieClientDebut.lierComposantFin(snCategorieClientFin);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gérer les erreurs automatiquement
    gererLesErreurs("19");
    
    // Rafraîchir le logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Rafraîchir l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Rafraîchir la plage de dates
    snPeriode.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPeriode.setDateDebutParChampRPG(lexique, "PERDEB");
    snPeriode.setDateFinParChampRPG(lexique, "PERFIN");
    
    // Rafraîchir la plage de catégories de clients
    snCategorieClientDebut.setSession(getSession());
    snCategorieClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientDebut.setTousAutorise(true);
    snCategorieClientDebut.charger(false);
    snCategorieClientDebut.renseignerChampRPG(lexique, "CATDEB");
    
    snCategorieClientFin.setSession(getSession());
    snCategorieClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClientFin.setTousAutorise(true);
    snCategorieClientFin.charger(false);
    snCategorieClientFin.renseignerChampRPG(lexique, "CATFIN");
    
    // Rafraîchir la plage de clients
    snClientDebut.setSession(getSession());
    snClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snClientDebut.charger(false);
    snClientDebut.setSelectionParChampRPG(lexique, "CLIDEB");
    
    snClientFin.setSession(getSession());
    snClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snClientFin.charger(false);
    snClientFin.setSelectionParChampRPG(lexique, "CLIFIN");
  }
  
  @Override
  public void getData() {
    super.getData();
    // Récupérer l'établissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Récupérer la plage de dates
    snPeriode.renseignerChampRPGDebut(lexique, "PERDEB");
    snPeriode.renseignerChampRPGFin(lexique, "PERFIN");
    
    // Récupérer la catégorie de clients
    snCategorieClientDebut.renseignerChampRPG(lexique, "CATDEB");
    snCategorieClientFin.renseignerChampRPG(lexique, "CATFIN");
    
    // Récupérer le client
    snClientDebut.renseignerChampRPG(lexique, "CLIDEB");
    snClientFin.renseignerChampRPG(lexique, "CLIFIN");
  }
  
  // Traiter les clics sur les boutons de la barre de boutons
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlPeriode = new SNPanelTitre();
    lbDateDeFacturation = new SNLabelChamp();
    snPeriode = new SNPlageDate();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlCritereclients = new SNPanelTitre();
    lbCategorieClientsDe = new SNLabelChamp();
    snCategorieClientDebut = new SNCategorieClient();
    lbCategorieClientsA = new SNLabelUnite();
    snCategorieClientFin = new SNCategorieClient();
    lbClientsDe = new SNLabelChamp();
    snClientDebut = new SNClientPrincipal();
    lbClientsA = new SNLabelUnite();
    snClientFin = new SNClientPrincipal();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMaximumSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Ventes par facture et par article");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout(1, 0, 5, 5));

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlPeriode ========
          {
            pnlPeriode.setTitre("S\u00e9lection dates de facturation");
            pnlPeriode.setName("pnlPeriode");
            pnlPeriode.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlPeriode.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlPeriode.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlPeriode.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlPeriode.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- lbDateDeFacturation ----
            lbDateDeFacturation.setText("Dates de facturation");
            lbDateDeFacturation.setName("lbDateDeFacturation");
            pnlPeriode.add(lbDateDeFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snPeriode ----
            snPeriode.setName("snPeriode");
            pnlPeriode.add(snPeriode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlPeriode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlCritereclients ========
      {
        pnlCritereclients.setTitre("S\u00e9lection clients");
        pnlCritereclients.setName("pnlCritereclients");
        pnlCritereclients.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCritereclients.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCritereclients.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlCritereclients.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlCritereclients.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbCategorieClientsDe ----
        lbCategorieClientsDe.setText("Cat\u00e9gories clients de");
        lbCategorieClientsDe.setMaximumSize(new Dimension(200, 30));
        lbCategorieClientsDe.setMinimumSize(new Dimension(200, 30));
        lbCategorieClientsDe.setName("lbCategorieClientsDe");
        pnlCritereclients.add(lbCategorieClientsDe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snCategorieClientDebut ----
        snCategorieClientDebut.setName("snCategorieClientDebut");
        pnlCritereclients.add(snCategorieClientDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbCategorieClientsA ----
        lbCategorieClientsA.setText("\u00e0");
        lbCategorieClientsA.setInheritsPopupMenu(false);
        lbCategorieClientsA.setMaximumSize(new Dimension(20, 30));
        lbCategorieClientsA.setMinimumSize(new Dimension(20, 30));
        lbCategorieClientsA.setPreferredSize(new Dimension(20, 30));
        lbCategorieClientsA.setHorizontalAlignment(SwingConstants.CENTER);
        lbCategorieClientsA.setName("lbCategorieClientsA");
        pnlCritereclients.add(lbCategorieClientsA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snCategorieClientFin ----
        snCategorieClientFin.setName("snCategorieClientFin");
        pnlCritereclients.add(snCategorieClientFin, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbClientsDe ----
        lbClientsDe.setText("Clients de");
        lbClientsDe.setName("lbClientsDe");
        pnlCritereclients.add(lbClientsDe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snClientDebut ----
        snClientDebut.setName("snClientDebut");
        pnlCritereclients.add(snClientDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbClientsA ----
        lbClientsA.setText("\u00e0");
        lbClientsA.setInheritsPopupMenu(false);
        lbClientsA.setMaximumSize(new Dimension(20, 30));
        lbClientsA.setMinimumSize(new Dimension(20, 30));
        lbClientsA.setPreferredSize(new Dimension(20, 30));
        lbClientsA.setHorizontalAlignment(SwingConstants.CENTER);
        lbClientsA.setName("lbClientsA");
        pnlCritereclients.add(lbClientsA, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snClientFin ----
        snClientFin.setName("snClientFin");
        pnlCritereclients.add(snClientFin, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCritereclients, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPeriode;
  private SNLabelChamp lbDateDeFacturation;
  private SNPlageDate snPeriode;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlCritereclients;
  private SNLabelChamp lbCategorieClientsDe;
  private SNCategorieClient snCategorieClientDebut;
  private SNLabelUnite lbCategorieClientsA;
  private SNCategorieClient snCategorieClientFin;
  private SNLabelChamp lbClientsDe;
  private SNClientPrincipal snClientDebut;
  private SNLabelUnite lbClientsA;
  private SNClientPrincipal snClientFin;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
