
package ri.serien.libecranrpg.sgvm.SGVM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM50FM_B1 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  // Variables
  private String[] TDVFIN_Value = { "0", "1", "2", "3", "4", "5", "6", };
  private String[] TDVDEB_Value = { "0", "1", "2", "3", "4", "5", "6", };
  
  /**
   * Constructeur.
   */
  public SGVM50FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    OPT1.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT3.setValeursSelection("X", " ");
    NONSTA.setValeursSelection("OUI", "NON");
    WREL.setValeursSelection("X", " ");
    WTRIC.setValeurs("X", btgTri);
    WTRIV.setValeurs("X", btgTri);
    TDVDEB.setValeurs(TDVDEB_Value, null);
    TDVFIN.setValeurs(TDVFIN_Value, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    rafraichirEtablissement();
    rafraichirVendeur();
    rafraichirCoursier();
    
    // Titre
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Verifie si les boutons "Option de tri" sont deja présélectionnés
    WTRIV.setSelected(!lexique.HostFieldGetData("WTRIV").trim().equalsIgnoreCase(""));
    WTRIC.setSelected(!lexique.HostFieldGetData("WTRIC").trim().equalsIgnoreCase(""));
    pnlOptionDeTri.setVisible(WTRIV.isVisible());
    
    // Contrôle de la présence du panel "Options d'édition "
    if (!lexique.isPresent("OPT1") && !lexique.isPresent("OPT2") && !lexique.isPresent("OPT3")) {
      pnlOptionsEdition.setVisible(false);
    }
    else {
      pnlOptionsEdition.setVisible(true);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    // Le vendeur
    if (snVendeur.isVisible()) {
      if (snVendeur.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOU", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU", 0, "  ");
        snVendeur.renseignerChampRPG(lexique, "VDE");
      }
    }
    // Le couriser
    if (snCoursier.isVisible()) {
      if (snCoursier.getIdSelection() == null) {
        lexique.HostFieldPutData("WTCOUR", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTCOUR", 0, "  ");
        snCoursier.renseignerChampRPG(lexique, "COURSI");
      }
    }
  }
  
  /**
   * Initialise le composant établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  /**
   * Initialise le composant vendeur suivant l'établissement.
   */
  private void rafraichirVendeur() {
    boolean venteComptoir = lexique.isTrue("92");
    
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "VDE");
  }
  
  /**
   * Initialise le composant transporteur suivant l'établissement.
   */
  private void rafraichirCoursier() {
    boolean venteComptoir = lexique.isTrue("92");
    ckCoursier.setVisible(venteComptoir);
    snCoursier.setVisible(venteComptoir);
    if (!venteComptoir) {
      return;
    }
    
    if (ckCoursier.isSelected()) {
      snVendeur.setEnabled(false);
      snCoursier.setEnabled(true);
    }
    else {
      snVendeur.setEnabled(true);
      snCoursier.setEnabled(false);
    }
    
    snCoursier.setSession(getSession());
    snCoursier.setIdEtablissement(snEtablissement.getIdSelection());
    snCoursier.setTousAutorise(true);
    snCoursier.charger(false);
    snCoursier.setSelectionParChampRPG(lexique, "COURSI");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void miChoixPossiblesActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckCoursierActionPerformed(ActionEvent e) {
    try {
      if (ckCoursier.isSelected()) {
        snVendeur.setEnabled(false);
        snCoursier.setEnabled(true);
      }
      else {
        snVendeur.setEnabled(true);
        snCoursier.setEnabled(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlSelection = new SNPanelTitre();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    ckCoursier = new JCheckBox();
    snCoursier = new SNTransporteur();
    lbEtatDevis = new SNLabelChamp();
    pnlEtatDevis = new SNPanel();
    TDVDEB = new XRiComboBox();
    lbA = new SNLabelChamp();
    TDVFIN = new XRiComboBox();
    lbNatureMontant = new SNLabelChamp();
    NATMTT = new XRiComboBox();
    lbPeriodeAEditer = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    lbDu = new SNLabelChamp();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    WREL = new XRiCheckBox();
    NONSTA = new XRiCheckBox();
    pnlOptionDeTri = new SNPanelTitre();
    WTRIV = new XRiRadioButton();
    WTRIC = new XRiRadioButton();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfDateEnCours = new SNTexte();
    pnlOptionsEdition = new SNPanelTitre();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    miChoixPossibles = new JMenuItem();
    miAideEnLigne = new JMenuItem();
    btgTri = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(870, 500));
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlSelection ========
        {
          pnlSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlSelection.setName("pnlSelection");
          pnlSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlSelection.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbVendeur ----
          lbVendeur.setText("Vendeur");
          lbVendeur.setName("lbVendeur");
          pnlSelection.add(lbVendeur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snVendeur ----
          snVendeur.setName("snVendeur");
          pnlSelection.add(snVendeur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ckCoursier ----
          ckCoursier.setText("ou Coursier");
          ckCoursier.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckCoursier.setHorizontalAlignment(SwingConstants.RIGHT);
          ckCoursier.setName("ckCoursier");
          ckCoursier.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ckCoursierActionPerformed(e);
            }
          });
          pnlSelection.add(ckCoursier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snCoursier ----
          snCoursier.setName("snCoursier");
          pnlSelection.add(snCoursier, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbEtatDevis ----
          lbEtatDevis.setText("Etat des devis");
          lbEtatDevis.setName("lbEtatDevis");
          pnlSelection.add(lbEtatDevis, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlEtatDevis ========
          {
            pnlEtatDevis.setName("pnlEtatDevis");
            pnlEtatDevis.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlEtatDevis.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlEtatDevis.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlEtatDevis.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlEtatDevis.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- TDVDEB ----
            TDVDEB.setModel(new DefaultComboBoxModel(new String[] {
              "Attente",
              "Valid\u00e9",
              "Envoy\u00e9",
              "Sign\u00e9",
              "Validit\u00e9 d\u00e9pass\u00e9e",
              "Perdu",
              "Clotur\u00e9"
            }));
            TDVDEB.setComponentPopupMenu(BTD);
            TDVDEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TDVDEB.setPreferredSize(new Dimension(140, 30));
            TDVDEB.setMinimumSize(new Dimension(140, 30));
            TDVDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            TDVDEB.setName("TDVDEB");
            pnlEtatDevis.add(TDVDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbA ----
            lbA.setText("\u00e0");
            lbA.setPreferredSize(new Dimension(20, 30));
            lbA.setMinimumSize(new Dimension(20, 30));
            lbA.setName("lbA");
            pnlEtatDevis.add(lbA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- TDVFIN ----
            TDVFIN.setModel(new DefaultComboBoxModel(new String[] {
              "Attente",
              "Valid\u00e9",
              "Envoy\u00e9",
              "Sign\u00e9",
              "Validit\u00e9 d\u00e9pass\u00e9e",
              "Perdu",
              "Clotur\u00e9"
            }));
            TDVFIN.setComponentPopupMenu(BTD);
            TDVFIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TDVFIN.setMinimumSize(new Dimension(140, 30));
            TDVFIN.setPreferredSize(new Dimension(140, 30));
            TDVFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            TDVFIN.setName("TDVFIN");
            pnlEtatDevis.add(TDVFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlSelection.add(pnlEtatDevis, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbNatureMontant ----
          lbNatureMontant.setText("Nature du montant");
          lbNatureMontant.setName("lbNatureMontant");
          pnlSelection.add(lbNatureMontant, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NATMTT ----
          NATMTT.setModel(new DefaultComboBoxModel(new String[] {
            "HT ",
            "TTC"
          }));
          NATMTT.setComponentPopupMenu(BTD);
          NATMTT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NATMTT.setPreferredSize(new Dimension(100, 30));
          NATMTT.setMinimumSize(new Dimension(100, 30));
          NATMTT.setFont(new Font("sansserif", Font.PLAIN, 14));
          NATMTT.setName("NATMTT");
          pnlSelection.add(NATMTT, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeAEditer ----
          lbPeriodeAEditer.setText("P\u00e9riode \u00e0 \u00e9diter");
          lbPeriodeAEditer.setName("lbPeriodeAEditer");
          pnlSelection.add(lbPeriodeAEditer, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlPeriodeAEditer ========
          {
            pnlPeriodeAEditer.setOpaque(false);
            pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
            pnlPeriodeAEditer.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlPeriodeAEditer.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)pnlPeriodeAEditer.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlPeriodeAEditer.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlPeriodeAEditer.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- lbDu ----
            lbDu.setText("du");
            lbDu.setPreferredSize(new Dimension(30, 30));
            lbDu.setMinimumSize(new Dimension(30, 30));
            lbDu.setMaximumSize(new Dimension(30, 30));
            lbDu.setName("lbDu");
            pnlPeriodeAEditer.add(lbDu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- PERDEB ----
            PERDEB.setPreferredSize(new Dimension(125, 30));
            PERDEB.setMinimumSize(new Dimension(150, 30));
            PERDEB.setMaximumSize(new Dimension(150, 30));
            PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            PERDEB.setName("PERDEB");
            pnlPeriodeAEditer.add(PERDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbAu ----
            lbAu.setText("au");
            lbAu.setPreferredSize(new Dimension(30, 30));
            lbAu.setMinimumSize(new Dimension(30, 30));
            lbAu.setMaximumSize(new Dimension(30, 30));
            lbAu.setName("lbAu");
            pnlPeriodeAEditer.add(lbAu, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- PERFIN ----
            PERFIN.setPreferredSize(new Dimension(125, 30));
            PERFIN.setMinimumSize(new Dimension(150, 30));
            PERFIN.setMaximumSize(new Dimension(150, 30));
            PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            PERFIN.setName("PERFIN");
            pnlPeriodeAEditer.add(PERFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- WREL ----
          WREL.setText("Client \u00e0 relancer sur la p\u00e9riode");
          WREL.setComponentPopupMenu(BTD);
          WREL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WREL.setFont(new Font("sansserif", Font.PLAIN, 14));
          WREL.setPreferredSize(new Dimension(300, 30));
          WREL.setMinimumSize(new Dimension(300, 30));
          WREL.setMaximumSize(new Dimension(300, 30));
          WREL.setName("WREL");
          pnlSelection.add(WREL, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- NONSTA ----
          NONSTA.setText("Prise en compte articles non g\u00e9r\u00e9s en stats");
          NONSTA.setComponentPopupMenu(BTD);
          NONSTA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NONSTA.setFont(new Font("sansserif", Font.PLAIN, 14));
          NONSTA.setPreferredSize(new Dimension(300, 30));
          NONSTA.setMinimumSize(new Dimension(300, 30));
          NONSTA.setMaximumSize(new Dimension(300, 30));
          NONSTA.setName("NONSTA");
          pnlSelection.add(NONSTA, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptionDeTri ========
        {
          pnlOptionDeTri.setTitre("Options de tri");
          pnlOptionDeTri.setName("pnlOptionDeTri");
          pnlOptionDeTri.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptionDeTri.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlOptionDeTri.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlOptionDeTri.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlOptionDeTri.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTRIV ----
          WTRIV.setText("Vendeur");
          WTRIV.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTRIV.setMinimumSize(new Dimension(135, 30));
          WTRIV.setPreferredSize(new Dimension(135, 30));
          WTRIV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTRIV.setName("WTRIV");
          pnlOptionDeTri.add(WTRIV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- WTRIC ----
          WTRIC.setText("Coursier");
          WTRIC.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTRIC.setPreferredSize(new Dimension(135, 30));
          WTRIC.setMinimumSize(new Dimension(135, 30));
          WTRIC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTRIC.setName("WTRIC");
          pnlOptionDeTri.add(WTRIC, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlOptionDeTri, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlEtablissementSelectionne ========
        {
          pnlEtablissementSelectionne.setTitre("Etablissement");
          pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
          pnlEtablissementSelectionne.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissementSelectionne.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfDateEnCours ----
          tfDateEnCours.setText("@WENCX@");
          tfDateEnCours.setPreferredSize(new Dimension(260, 30));
          tfDateEnCours.setMinimumSize(new Dimension(260, 30));
          tfDateEnCours.setEditable(false);
          tfDateEnCours.setEnabled(false);
          tfDateEnCours.setName("tfDateEnCours");
          pnlEtablissementSelectionne.add(tfDateEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptionsEdition ========
        {
          pnlOptionsEdition.setOpaque(false);
          pnlOptionsEdition.setTitre("Options d'\u00e9dition");
          pnlOptionsEdition.setName("pnlOptionsEdition");
          pnlOptionsEdition.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptionsEdition.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlOptionsEdition.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlOptionsEdition.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)pnlOptionsEdition.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- OPT1 ----
          OPT1.setText("Bons homologu\u00e9s non exp\u00e9di\u00e9s (HOM)");
          OPT1.setComponentPopupMenu(BTD);
          OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT1.setPreferredSize(new Dimension(300, 30));
          OPT1.setMinimumSize(new Dimension(250, 30));
          OPT1.setMaximumSize(new Dimension(250, 30));
          OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT1.setName("OPT1");
          pnlOptionsEdition.add(OPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OPT2 ----
          OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
          OPT2.setComponentPopupMenu(BTD);
          OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT2.setMaximumSize(new Dimension(250, 30));
          OPT2.setMinimumSize(new Dimension(250, 30));
          OPT2.setPreferredSize(new Dimension(250, 30));
          OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT2.setName("OPT2");
          pnlOptionsEdition.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OPT3 ----
          OPT3.setText("Bons factur\u00e9s (FAC)");
          OPT3.setComponentPopupMenu(BTD);
          OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT3.setMaximumSize(new Dimension(250, 30));
          OPT3.setMinimumSize(new Dimension(250, 30));
          OPT3.setPreferredSize(new Dimension(250, 30));
          OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT3.setName("OPT3");
          pnlOptionsEdition.add(OPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptionsEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- miChoixPossibles ----
      miChoixPossibles.setText("Choix possibles");
      miChoixPossibles.setName("miChoixPossibles");
      miChoixPossibles.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossiblesActionPerformed(e);
        }
      });
      BTD.add(miChoixPossibles);

      //---- miAideEnLigne ----
      miAideEnLigne.setText("Aide en ligne");
      miAideEnLigne.setName("miAideEnLigne");
      miAideEnLigne.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigneActionPerformed(e);
        }
      });
      BTD.add(miAideEnLigne);
    }

    //---- btgTri ----
    btgTri.add(WTRIV);
    btgTri.add(WTRIC);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlSelection;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private JCheckBox ckCoursier;
  private SNTransporteur snCoursier;
  private SNLabelChamp lbEtatDevis;
  private SNPanel pnlEtatDevis;
  private XRiComboBox TDVDEB;
  private SNLabelChamp lbA;
  private XRiComboBox TDVFIN;
  private SNLabelChamp lbNatureMontant;
  private XRiComboBox NATMTT;
  private SNLabelChamp lbPeriodeAEditer;
  private SNPanel pnlPeriodeAEditer;
  private SNLabelChamp lbDu;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private XRiCheckBox WREL;
  private XRiCheckBox NONSTA;
  private SNPanelTitre pnlOptionDeTri;
  private XRiRadioButton WTRIV;
  private XRiRadioButton WTRIC;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfDateEnCours;
  private SNPanelTitre pnlOptionsEdition;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem miChoixPossibles;
  private JMenuItem miAideEnLigne;
  private ButtonGroup btgTri;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
