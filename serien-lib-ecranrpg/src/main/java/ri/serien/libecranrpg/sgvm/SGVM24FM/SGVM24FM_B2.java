
package ri.serien.libecranrpg.sgvm.SGVM24FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class SGVM24FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM24FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Ajout
    WTR3.setValeursSelection("X", " ");
    WTR1.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDialog(true);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    snClient.setVisible(WTR1.isSelected());
    snMagasin.setVisible(WTR3.isSelected());
    
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // Initialisation client principal
    snClient.setSession(getSession());
    snClient.setIdEtablissement(idEtablissement);
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "WCLI");
    
    // Initialisation magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAGD");
    
    // Titre de la fenêtre
    setTitle("Tri et Sélection");
  }
  
  @Override
  public void getData() {
    super.getData();
    snMagasin.renseignerChampRPG(lexique, "WMAGD");
    snClient.renseignerChampRPG(lexique, "WCLI");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTR1ItemStateChanged(ItemEvent e) {
    snClient.setVisible(WTR1.isSelected());
  }
  
  private void WTR3ItemStateChanged(ItemEvent e) {
    snMagasin.setVisible(WTR3.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlCriteresDeTri = new SNPanelTitre();
    WTR1 = new XRiCheckBox();
    snClient = new SNClientPrincipal();
    WTR3 = new XRiCheckBox();
    snMagasin = new SNMagasin();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(550, 200));
    setPreferredSize(new Dimension(550, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlCriteresDeTri ========
      {
        pnlCriteresDeTri.setTitre("Crit\u00e8res de tri");
        pnlCriteresDeTri.setName("pnlCriteresDeTri");
        pnlCriteresDeTri.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- WTR1 ----
        WTR1.setText("Client");
        WTR1.setMaximumSize(new Dimension(100, 30));
        WTR1.setMinimumSize(new Dimension(100, 30));
        WTR1.setPreferredSize(new Dimension(100, 30));
        WTR1.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTR1.setName("WTR1");
        WTR1.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            WTR1ItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(WTR1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snClient ----
        snClient.setName("snClient");
        pnlCriteresDeTri.add(snClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- WTR3 ----
        WTR3.setText("Magasin");
        WTR3.setMaximumSize(new Dimension(69, 30));
        WTR3.setMinimumSize(new Dimension(69, 30));
        WTR3.setPreferredSize(new Dimension(69, 30));
        WTR3.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTR3.setName("WTR3");
        WTR3.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            WTR3ItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(WTR3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snMagasin ----
        snMagasin.setMaximumSize(new Dimension(500, 30));
        snMagasin.setMinimumSize(new Dimension(500, 30));
        snMagasin.setPreferredSize(new Dimension(500, 30));
        snMagasin.setName("snMagasin");
        pnlCriteresDeTri.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCriteresDeTri);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlCriteresDeTri;
  private XRiCheckBox WTR1;
  private SNClientPrincipal snClient;
  private XRiCheckBox WTR3;
  private SNMagasin snMagasin;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
