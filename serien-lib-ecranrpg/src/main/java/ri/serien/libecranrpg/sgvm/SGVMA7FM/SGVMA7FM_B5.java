
package ri.serien.libecranrpg.sgvm.SGVMA7FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMA7FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMA7FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    OPT5.setValeurs("5", "RB");
    OPT4.setValeurs("4", "RB");
    OPT3.setValeurs("3", "RB");
    OPT2.setValeurs("2", "RB");
    OPT1.setValeurs("1", "RB");
    EDT12.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_47.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Mot de classement @MOTCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    NPRIX.setEnabled(lexique.isPresent("NPRIX"));
    OBJ_55.setVisible(lexique.isPresent("FAM01"));
    // FAM02.setVisible( lexique.isPresent("FAM02"));
    // FAM01.setVisible( lexique.isPresent("FAM01"));
    // NUMCLI.setVisible( lexique.isPresent("NUMCLI"));
    REFTAR.setEnabled(lexique.isPresent("REFTAR"));
    // ZDATE.setEnabled( lexique.isPresent("ZDATE"));
    // RACNV.setVisible( lexique.isPresent("RACNV"));
    // CNV.setVisible( lexique.isPresent("CNV"));
    // MCLA.setVisible( lexique.isPresent("MCLA"));
    OBJ_65.setVisible(!lexique.isTrue("93"));
    OBJ_49.setVisible(lexique.isPresent("CNV"));
    // EDT12.setVisible( lexique.isPresent("EDT12"));
    // EDT12.setSelected(lexique.HostFieldGetData("EDT12").equalsIgnoreCase("O"));
    OBJ_66.setVisible(lexique.isTrue("93"));
    // OPT5.setEnabled( lexique.isPresent("RB"));
    // OPT5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // OPT4.setEnabled( lexique.isPresent("RB"));
    // OPT4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // OPT3.setEnabled( lexique.isPresent("RB"));
    // OPT3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // OPT2.setEnabled( lexique.isPresent("RB"));
    // OPT2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // OPT1.setEnabled( lexique.isPresent("RB"));
    // OPT1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    OBJ_47.setVisible(lexique.isPresent("MCLA"));
    OBJ_46.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    OBJ_45.setVisible(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // OPTAB1.setEnabled( !lexique.isPresent("OPTAB1"));
    // OPTABC.setEnabled( !lexique.isPresent("OPTABC"));
    // OPTABC.setSelectedItem(lexique.HostFieldGetData("OPTABC"));
    // OPTAB1.setSelectedItem(lexique.HostFieldGetData("OPTAB1"));
    OBJ_51.setVisible(lexique.isTrue("93"));
    OBJ_52.setVisible(!lexique.isTrue("93"));
    
    if (lexique.isTrue("34")) {
      OBJ_47.setTitle("Mot de classement 2");
    }
    if (lexique.isTrue("33")) {
      OBJ_47.setTitle("Mot de classement 1");
    }
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@&&G_TITRE@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (EDT12.isSelected())
    // lexique.HostFieldPutData("EDT12", 0, "O");
    // else
    // lexique.HostFieldPutData("EDT12", 0, "N");
    // if (OPT5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (OPT4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (OPT3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (OPT2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (OPT1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_49 = new JXTitledSeparator();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    CNV = new XRiTextField();
    NUMCLI = new XRiTextField();
    panel4 = new JPanel();
    OPT1 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    OPT4 = new XRiRadioButton();
    OPT5 = new XRiRadioButton();
    OBJ_530 = new JXTitledSeparator();
    OBJ_64 = new JLabel();
    OPTABC = new XRiComboBox();
    OBJ_62 = new JLabel();
    OPTAB1 = new XRiComboBox();
    MCLA = new XRiTextField();
    RACNV = new XRiTextField();
    FAM01 = new XRiTextField();
    OBJ_55 = new JLabel();
    FAM02 = new XRiTextField();
    OBJ_45 = new JXTitledSeparator();
    OBJ_46 = new JXTitledSeparator();
    OBJ_47 = new JXTitledSeparator();
    OBJ_59 = new JXTitledSeparator();
    REFTAR = new XRiTextField();
    NPRIX = new XRiTextField();
    OBJ_66 = new JLabel();
    OBJ_69 = new JLabel();
    EDT12 = new XRiCheckBox();
    OBJ_63 = new JLabel();
    OBJ_65 = new JLabel();
    ZDATE = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(30, 30, 805, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(200, 55, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(200, 85, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(45, 70, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(90, 70), bouton_etablissement.getPreferredSize()));

          //---- OBJ_49 ----
          OBJ_49.setTitle("");
          OBJ_49.setName("OBJ_49");
          p_contenu.add(OBJ_49);
          OBJ_49.setBounds(30, 125, 805, OBJ_49.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("Code condition de vente \"CN\"");
          OBJ_51.setName("OBJ_51");
          p_contenu.add(OBJ_51);
          OBJ_51.setBounds(45, 140, 197, 20);

          //---- OBJ_52 ----
          OBJ_52.setText("Num\u00e9ro client \u00e0 traiter");
          OBJ_52.setName("OBJ_52");
          p_contenu.add(OBJ_52);
          OBJ_52.setBounds(45, 140, 181, 20);

          //---- CNV ----
          CNV.setComponentPopupMenu(BTD);
          CNV.setName("CNV");
          p_contenu.add(CNV);
          CNV.setBounds(275, 135, 60, CNV.getPreferredSize().height);

          //---- NUMCLI ----
          NUMCLI.setComponentPopupMenu(BTD);
          NUMCLI.setName("NUMCLI");
          p_contenu.add(NUMCLI);
          NUMCLI.setBounds(275, 135, 70, NUMCLI.getPreferredSize().height);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OPT1 ----
            OPT1.setText("Tous les articles");
            OPT1.setComponentPopupMenu(BTD);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel4.add(OPT1);
            OPT1.setBounds(15, 10, 266, 18);

            //---- OPT2 ----
            OPT2.setText("Articles d'une famille");
            OPT2.setComponentPopupMenu(BTD);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel4.add(OPT2);
            OPT2.setBounds(15, 36, 269, 18);

            //---- OPT3 ----
            OPT3.setText("Articles sur mot de classement 1");
            OPT3.setComponentPopupMenu(BTD);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setName("OPT3");
            panel4.add(OPT3);
            OPT3.setBounds(15, 62, 269, 18);

            //---- OPT4 ----
            OPT4.setText("Articles de m\u00eame mot classement 2");
            OPT4.setComponentPopupMenu(BTD);
            OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT4.setName("OPT4");
            panel4.add(OPT4);
            OPT4.setBounds(15, 88, 268, 18);

            //---- OPT5 ----
            OPT5.setText("Articles de m\u00eame rattachement CNV");
            OPT5.setComponentPopupMenu(BTD);
            OPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT5.setName("OPT5");
            panel4.add(OPT5);
            OPT5.setBounds(15, 114, 269, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(30, 225, 300, 150);

          //---- OBJ_530 ----
          OBJ_530.setTitle("Options possibles dans un tarif");
          OBJ_530.setName("OBJ_530");
          p_contenu.add(OBJ_530);
          OBJ_530.setBounds(30, 195, 400, OBJ_530.getPreferredSize().height);

          //---- OBJ_64 ----
          OBJ_64.setText("Filtre sur code ABC de l'article");
          OBJ_64.setName("OBJ_64");
          p_contenu.add(OBJ_64);
          OBJ_64.setBounds(435, 140, 181, 20);

          //---- OPTABC ----
          OPTABC.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTABC.setComponentPopupMenu(BTD);
          OPTABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTABC.setName("OPTABC");
          p_contenu.add(OPTABC);
          OPTABC.setBounds(635, 135, 50, OPTABC.getPreferredSize().height);

          //---- OBJ_62 ----
          OBJ_62.setText("\u00e0");
          OBJ_62.setName("OBJ_62");
          p_contenu.add(OBJ_62);
          OBJ_62.setBounds(710, 140, 11, 20);

          //---- OPTAB1 ----
          OPTAB1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTAB1.setComponentPopupMenu(BTD);
          OPTAB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTAB1.setName("OPTAB1");
          p_contenu.add(OPTAB1);
          OPTAB1.setBounds(740, 135, 50, OPTAB1.getPreferredSize().height);

          //---- MCLA ----
          MCLA.setComponentPopupMenu(BTD);
          MCLA.setName("MCLA");
          p_contenu.add(MCLA);
          MCLA.setBounds(80, 435, 210, MCLA.getPreferredSize().height);

          //---- RACNV ----
          RACNV.setComponentPopupMenu(BTD);
          RACNV.setName("RACNV");
          p_contenu.add(RACNV);
          RACNV.setBounds(145, 435, 60, RACNV.getPreferredSize().height);

          //---- FAM01 ----
          FAM01.setComponentPopupMenu(BTD);
          FAM01.setName("FAM01");
          p_contenu.add(FAM01);
          FAM01.setBounds(115, 435, 40, FAM01.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("\u00e0");
          OBJ_55.setName("OBJ_55");
          p_contenu.add(OBJ_55);
          OBJ_55.setBounds(165, 440, 29, 20);

          //---- FAM02 ----
          FAM02.setComponentPopupMenu(BTD);
          FAM02.setName("FAM02");
          p_contenu.add(FAM02);
          FAM02.setBounds(200, 435, 40, FAM02.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setTitle("Rattachement CNV (facultatif)");
          OBJ_45.setName("OBJ_45");
          p_contenu.add(OBJ_45);
          OBJ_45.setBounds(35, 400, 333, OBJ_45.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setTitle("Code famille \u00e0 \u00e9diter");
          OBJ_46.setName("OBJ_46");
          p_contenu.add(OBJ_46);
          OBJ_46.setBounds(35, 400, 333, OBJ_46.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setTitle("Mot de classement @MOTCX@");
          OBJ_47.setName("OBJ_47");
          p_contenu.add(OBJ_47);
          OBJ_47.setBounds(35, 400, 333, OBJ_47.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setTitle("Options suppl\u00e9mentaires");
          OBJ_59.setName("OBJ_59");
          p_contenu.add(OBJ_59);
          OBJ_59.setBounds(460, 195, 375, OBJ_59.getPreferredSize().height);

          //---- REFTAR ----
          REFTAR.setComponentPopupMenu(BTD);
          REFTAR.setName("REFTAR");
          p_contenu.add(REFTAR);
          REFTAR.setBounds(725, 231, 60, REFTAR.getPreferredSize().height);

          //---- NPRIX ----
          NPRIX.setComponentPopupMenu(BTD);
          NPRIX.setName("NPRIX");
          p_contenu.add(NPRIX);
          NPRIX.setBounds(725, 260, 30, NPRIX.getPreferredSize().height);

          //---- OBJ_66 ----
          OBJ_66.setText("Num\u00e9ro tarif \u00e0 \u00e9diter (01 \u00e0 10)");
          OBJ_66.setName("OBJ_66");
          p_contenu.add(OBJ_66);
          OBJ_66.setBounds(470, 264, 220, 20);

          //---- OBJ_69 ----
          OBJ_69.setText("Date de validit\u00e9 des prix");
          OBJ_69.setName("OBJ_69");
          p_contenu.add(OBJ_69);
          OBJ_69.setBounds(470, 293, 194, 20);

          //---- EDT12 ----
          EDT12.setText("Edition sur format 12 pouces");
          EDT12.setComponentPopupMenu(BTD);
          EDT12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDT12.setName("EDT12");
          p_contenu.add(EDT12);
          EDT12.setBounds(470, 335, 239, 16);

          //---- OBJ_63 ----
          OBJ_63.setText("R\u00e9f\u00e9rence tarif \u00e0 \u00e9diter  (ou **)");
          OBJ_63.setName("OBJ_63");
          p_contenu.add(OBJ_63);
          OBJ_63.setBounds(470, 235, 180, 20);

          //---- OBJ_65 ----
          OBJ_65.setText("Num\u00e9ro tarif (01 \u00e0 10 ou **)");
          OBJ_65.setName("OBJ_65");
          p_contenu.add(OBJ_65);
          OBJ_65.setBounds(470, 264, 180, 20);

          //---- ZDATE ----
          ZDATE.setName("ZDATE");
          p_contenu.add(ZDATE);
          ZDATE.setBounds(725, 289, 105, ZDATE.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(OPT1);
    buttonGroup1.add(OPT2);
    buttonGroup1.add(OPT3);
    buttonGroup1.add(OPT4);
    buttonGroup1.add(OPT5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_49;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private XRiTextField CNV;
  private XRiTextField NUMCLI;
  private JPanel panel4;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT3;
  private XRiRadioButton OPT4;
  private XRiRadioButton OPT5;
  private JXTitledSeparator OBJ_530;
  private JLabel OBJ_64;
  private XRiComboBox OPTABC;
  private JLabel OBJ_62;
  private XRiComboBox OPTAB1;
  private XRiTextField MCLA;
  private XRiTextField RACNV;
  private XRiTextField FAM01;
  private JLabel OBJ_55;
  private XRiTextField FAM02;
  private JXTitledSeparator OBJ_45;
  private JXTitledSeparator OBJ_46;
  private JXTitledSeparator OBJ_47;
  private JXTitledSeparator OBJ_59;
  private XRiTextField REFTAR;
  private XRiTextField NPRIX;
  private JLabel OBJ_66;
  private JLabel OBJ_69;
  private XRiCheckBox EDT12;
  private JLabel OBJ_63;
  private JLabel OBJ_65;
  private XRiCalendrier ZDATE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
