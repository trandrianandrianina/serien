
package ri.serien.libecranrpg.sgvm.SGVM23FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SGVM23FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private Message periodeArrete = null;
  
  public SGVM23FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    REL5.setValeurs("5", "RB");
    REL4.setValeurs("4", "RB");
    REL3.setValeurs("3", "RB");
    REL2.setValeurs("2", "RB");
    REL1.setValeurs("1", "RB");
    REPON1.setValeursSelection("1", " ");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    REL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD1@")).trim());
    REL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD2@")).trim());
    REL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD3@")).trim());
    REL4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD4@")).trim());
    REL5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COD5@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Gestion message periode arrêté
    if (lexique.isTrue("50")) {
      lbGenerationSurPeriodeArrete.setVisible(true);
      periodeArrete = periodeArrete.getMessageImportant("Génération ds relevés sur période arrêtée.");
      lbGenerationSurPeriodeArrete.setMessage(periodeArrete);
    }
    else {
      lbGenerationSurPeriodeArrete.setVisible(false);
    }
    
    // Visibilité des composant
    if (lexique.isTrue("60")) {
      snCanalDeVente.setVisible(true);
      lbCanalDeVente.setVisible(true);
    }
    else {
      snCanalDeVente.setVisible(false);
      lbCanalDeVente.setVisible(false);
    }
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Sélection des périodicité
    selectionnerPeriodicite();
    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charge les composants
    chargerListeMagasin();
    chargerListeCanalDeVente();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snCanalDeVente.renseignerChampRPG(lexique, "WCAN");
  }
  
  /**
   * Charge le composant canal de vente
   */
  private void chargerListeCanalDeVente() {
    snCanalDeVente.setSession(getSession());
    snCanalDeVente.setIdEtablissement(snEtablissement.getIdSelection());
    snCanalDeVente.setTousAutorise(true);
    snCanalDeVente.charger(true);
    snCanalDeVente.setSelectionParChampRPG(lexique, "WCAN");
  }
  
  /**
   * Charge le composant magasin
   */
  private void chargerListeMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  private void selectionnerPeriodicite() {
    if (!REL1.isSelected() || !REL2.isSelected() || !REL3.isSelected() || !REL4.isSelected() || !REL5.isSelected()) {
      if (REL1.isVisible()) {
        REL1.setSelected(true);
      }
      else if (REL2.isVisible()) {
        REL2.setSelected(true);
      }
      else if (REL3.isVisible()) {
        REL3.setSelected(true);
      }
      else if (REL4.isVisible()) {
        REL4.setSelected(true);
      }
      else if (REL5.isVisible()) {
        REL5.setSelected(true);
      }
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbDateDeReleverAGenerer = new SNLabelChamp();
    DATFAC = new XRiCalendrier();
    lbGenerationSurPeriodeArrete = new SNLabelTitre();
    sNLabelChamp2 = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbCanalDeVente = new SNLabelChamp();
    snCanalDeVente = new SNCanalDeVente();
    pnlPeriodiciteReleve = new SNPanelTitre();
    REL1 = new XRiRadioButton();
    REL2 = new XRiRadioButton();
    REL3 = new XRiRadioButton();
    REL4 = new XRiRadioButton();
    REL5 = new XRiRadioButton();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionsEdition = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("G\u00e9n\u00e9ration de relev\u00e9 de factures");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateDeReleverAGenerer ----
            lbDateDeReleverAGenerer.setText("Date de relev\u00e9s \u00e0 g\u00e9n\u00e9rer");
            lbDateDeReleverAGenerer.setPreferredSize(new Dimension(166, 30));
            lbDateDeReleverAGenerer.setMinimumSize(new Dimension(166, 30));
            lbDateDeReleverAGenerer.setMaximumSize(new Dimension(166, 30));
            lbDateDeReleverAGenerer.setName("lbDateDeReleverAGenerer");
            pnlCritereDeSelection.add(lbDateDeReleverAGenerer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DATFAC ----
            DATFAC.setPreferredSize(new Dimension(110, 30));
            DATFAC.setMinimumSize(new Dimension(110, 30));
            DATFAC.setMaximumSize(new Dimension(110, 30));
            DATFAC.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATFAC.setName("DATFAC");
            pnlCritereDeSelection.add(DATFAC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbGenerationSurPeriodeArrete ----
            lbGenerationSurPeriodeArrete.setText("G\u00e9n\u00e9ration des relev\u00e9s sur p\u00e9riode arr\u00eat\u00e9e.");
            lbGenerationSurPeriodeArrete.setName("lbGenerationSurPeriodeArrete");
            pnlCritereDeSelection.add(lbGenerationSurPeriodeArrete, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp2 ----
            sNLabelChamp2.setText("Magasin");
            sNLabelChamp2.setName("sNLabelChamp2");
            pnlCritereDeSelection.add(sNLabelChamp2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setEnabled(false);
            snMagasin.setName("snMagasin");
            pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCanalDeVente ----
            lbCanalDeVente.setText("Canal de vente");
            lbCanalDeVente.setName("lbCanalDeVente");
            pnlCritereDeSelection.add(lbCanalDeVente, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snCanalDeVente ----
            snCanalDeVente.setFont(new Font("sansserif", Font.PLAIN, 14));
            snCanalDeVente.setName("snCanalDeVente");
            pnlCritereDeSelection.add(snCanalDeVente, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlPeriodiciteReleve ========
          {
            pnlPeriodiciteReleve.setTitre("P\u00e9riodicit\u00e9 des relev\u00e9s \u00e0 traiter");
            pnlPeriodiciteReleve.setName("pnlPeriodiciteReleve");
            pnlPeriodiciteReleve.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPeriodiciteReleve.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlPeriodiciteReleve.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlPeriodiciteReleve.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPeriodiciteReleve.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- REL1 ----
            REL1.setText("@COD1@");
            REL1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REL1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REL1.setPreferredSize(new Dimension(89, 30));
            REL1.setMinimumSize(new Dimension(89, 30));
            REL1.setMaximumSize(new Dimension(89, 30));
            REL1.setName("REL1");
            pnlPeriodiciteReleve.add(REL1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REL2 ----
            REL2.setText("@COD2@");
            REL2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REL2.setFont(new Font("sansserif", Font.PLAIN, 14));
            REL2.setPreferredSize(new Dimension(89, 30));
            REL2.setMinimumSize(new Dimension(89, 30));
            REL2.setMaximumSize(new Dimension(89, 30));
            REL2.setName("REL2");
            pnlPeriodiciteReleve.add(REL2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REL3 ----
            REL3.setText("@COD3@");
            REL3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REL3.setFont(new Font("sansserif", Font.PLAIN, 14));
            REL3.setPreferredSize(new Dimension(89, 30));
            REL3.setMinimumSize(new Dimension(89, 30));
            REL3.setMaximumSize(new Dimension(89, 30));
            REL3.setName("REL3");
            pnlPeriodiciteReleve.add(REL3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REL4 ----
            REL4.setText("@COD4@");
            REL4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REL4.setFont(new Font("sansserif", Font.PLAIN, 14));
            REL4.setPreferredSize(new Dimension(89, 30));
            REL4.setMinimumSize(new Dimension(89, 30));
            REL4.setMaximumSize(new Dimension(89, 30));
            REL4.setName("REL4");
            pnlPeriodiciteReleve.add(REL4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REL5 ----
            REL5.setText("@COD5@");
            REL5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REL5.setFont(new Font("sansserif", Font.PLAIN, 14));
            REL5.setPreferredSize(new Dimension(89, 30));
            REL5.setMinimumSize(new Dimension(89, 30));
            REL5.setMaximumSize(new Dimension(89, 30));
            REL5.setName("REL5");
            pnlPeriodiciteReleve.add(REL5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlPeriodiciteReleve, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionsEdition ========
          {
            pnlOptionsEdition.setTitre("Options d'\u00e9ditions");
            pnlOptionsEdition.setName("pnlOptionsEdition");
            pnlOptionsEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- REPON1 ----
            REPON1.setText("Etat r\u00e9capitulatif");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setPreferredSize(new Dimension(124, 30));
            REPON1.setMinimumSize(new Dimension(124, 30));
            REPON1.setMaximumSize(new Dimension(124, 30));
            REPON1.setName("REPON1");
            pnlOptionsEdition.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionsEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(REL1);
    RB_GRP.add(REL2);
    RB_GRP.add(REL3);
    RB_GRP.add(REL4);
    RB_GRP.add(REL5);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbDateDeReleverAGenerer;
  private XRiCalendrier DATFAC;
  private SNLabelTitre lbGenerationSurPeriodeArrete;
  private SNLabelChamp sNLabelChamp2;
  private SNMagasin snMagasin;
  private SNLabelChamp lbCanalDeVente;
  private SNCanalDeVente snCanalDeVente;
  private SNPanelTitre pnlPeriodiciteReleve;
  private XRiRadioButton REL1;
  private XRiRadioButton REL2;
  private XRiRadioButton REL3;
  private XRiRadioButton REL4;
  private XRiRadioButton REL5;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionsEdition;
  private XRiCheckBox REPON1;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
