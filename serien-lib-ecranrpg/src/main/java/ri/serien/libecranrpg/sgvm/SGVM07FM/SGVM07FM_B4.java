
package ri.serien.libecranrpg.sgvm.SGVM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM07FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM07FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTCNV.setValeursSelection("**", "  ");
    OPT4.setValeursSelection("T", " ");
    OPT3.setValeursSelection("A", " ");
    OPT5.setValeursSelection("R", " ");
    OPT2.setValeursSelection("F", " ");
    OPT1.setValeursSelection("G", " ");
    OPT6.setValeursSelection("1", " ");
    OPT7.setValeursSelection("1", " ");
    WTCLT.setValeursSelection("**", "  ");
    SAUT.setValeursSelection("X", " ");
    EXPMPR.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOMD@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOMF@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CNLIBD@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CNLIBF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    OBJ_84.setVisible(lexique.isPresent("WTCLT"));
    OBJ_70.setVisible(lexique.isPresent("WTCLT"));
    OBJ_60.setVisible(lexique.isPresent("WTCNV"));
    OBJ_86.setVisible(lexique.isPresent("WTCLT"));
    OBJ_61.setVisible(lexique.isPresent("WTCNV"));
    P_SEL3.setVisible(!WTCLT.isSelected());
    P_SEL2.setVisible(!WTCNV.isSelected());
    
    panel1.setVisible(lexique.isTrue("92"));
    panel4.setVisible(lexique.isTrue("91"));
    
    panel3.setVisible(lexique.isTrue("97"));
    SAUT.setVisible(lexique.isTrue("97"));
    EXPMPR.setVisible(lexique.isTrue("97"));
    OBJ_43.setVisible(lexique.isTrue("97"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTCNVActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTCLTActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_43 = new JXTitledSeparator();
    EXPMPR = new XRiCheckBox();
    SAUT = new XRiCheckBox();
    panel1 = new JPanel();
    OBJ_70 = new JLabel();
    REPDEB = new XRiTextField();
    OBJ_84 = new JLabel();
    REPFIN = new XRiTextField();
    OBJ_67 = new JXTitledSeparator();
    P_SEL3 = new JPanel();
    OBJ_59 = new RiZoneSortie();
    OBJ_65 = new RiZoneSortie();
    DEBCLI = new XRiTextField();
    FINCLI = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_51 = new JXTitledSeparator();
    WTCLT = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_41 = new JXTitledSeparator();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT5 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    OPT4 = new XRiCheckBox();
    OPT6 = new XRiCheckBox();
    OPT7 = new XRiCheckBox();
    panel3 = new JPanel();
    OBJ_42 = new JXTitledSeparator();
    OBJ_83 = new JLabel();
    OBJ_69 = new JLabel();
    WCOLTR = new XRiTextField();
    OBJ_85 = new JLabel();
    WDAPD = new XRiCalendrier();
    WDAPF = new XRiCalendrier();
    panel4 = new JPanel();
    OBJ_50 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_58 = new RiZoneSortie();
    OBJ_64 = new RiZoneSortie();
    DEBCNV = new XRiTextField();
    FINCNV = new XRiTextField();
    WTCNV = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 650, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 62, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 92, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 79, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 77), bouton_etablissement.getPreferredSize()));

          //---- OBJ_43 ----
          OBJ_43.setTitle("");
          OBJ_43.setName("OBJ_43");
          p_contenu.add(OBJ_43);
          OBJ_43.setBounds(35, 530, 650, OBJ_43.getPreferredSize().height);

          //---- EXPMPR ----
          EXPMPR.setText("Exportation pour modification sur Conditions de ventes");
          EXPMPR.setComponentPopupMenu(BTD);
          EXPMPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EXPMPR.setName("EXPMPR");
          p_contenu.add(EXPMPR);
          EXPMPR.setBounds(50, 560, 346, 20);

          //---- SAUT ----
          SAUT.setText("Saut de page / rattachement");
          SAUT.setComponentPopupMenu(BTD);
          SAUT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SAUT.setName("SAUT");
          p_contenu.add(SAUT);
          SAUT.setBounds(50, 535, 193, 20);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_70 ----
            OBJ_70.setText("Code de d\u00e9but");
            OBJ_70.setName("OBJ_70");
            panel1.add(OBJ_70);
            OBJ_70.setBounds(65, 160, 135, 18);

            //---- REPDEB ----
            REPDEB.setComponentPopupMenu(BTD);
            REPDEB.setName("REPDEB");
            panel1.add(REPDEB);
            REPDEB.setBounds(200, 155, 30, REPDEB.getPreferredSize().height);

            //---- OBJ_84 ----
            OBJ_84.setText("Code de fin");
            OBJ_84.setName("OBJ_84");
            panel1.add(OBJ_84);
            OBJ_84.setBounds(275, 160, 105, 18);

            //---- REPFIN ----
            REPFIN.setComponentPopupMenu(BTD);
            REPFIN.setName("REPFIN");
            panel1.add(REPFIN);
            REPFIN.setBounds(390, 155, 30, REPFIN.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setTitle("Plage codes repr\u00e9sentants");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(5, 135, 650, OBJ_67.getPreferredSize().height);

            //======== P_SEL3 ========
            {
              P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL3.setOpaque(false);
              P_SEL3.setName("P_SEL3");
              P_SEL3.setLayout(null);

              //---- OBJ_59 ----
              OBJ_59.setText("@CLNOMD@");
              OBJ_59.setName("OBJ_59");
              P_SEL3.add(OBJ_59);
              OBJ_59.setBounds(200, 7, 306, OBJ_59.getPreferredSize().height);

              //---- OBJ_65 ----
              OBJ_65.setText("@CLNOMF@");
              OBJ_65.setName("OBJ_65");
              P_SEL3.add(OBJ_65);
              OBJ_65.setBounds(200, 32, 306, OBJ_65.getPreferredSize().height);

              //---- DEBCLI ----
              DEBCLI.setComponentPopupMenu(BTD);
              DEBCLI.setName("DEBCLI");
              P_SEL3.add(DEBCLI);
              DEBCLI.setBounds(130, 5, 60, DEBCLI.getPreferredSize().height);

              //---- FINCLI ----
              FINCLI.setComponentPopupMenu(BTD);
              FINCLI.setName("FINCLI");
              P_SEL3.add(FINCLI);
              FINCLI.setBounds(130, 30, 60, FINCLI.getPreferredSize().height);

              //---- OBJ_86 ----
              OBJ_86.setText("Num\u00e9ro de d\u00e9but");
              OBJ_86.setName("OBJ_86");
              P_SEL3.add(OBJ_86);
              OBJ_86.setBounds(15, 10, 115, 18);

              //---- OBJ_66 ----
              OBJ_66.setText("Num\u00e9ro de fin");
              OBJ_66.setName("OBJ_66");
              P_SEL3.add(OBJ_66);
              OBJ_66.setBounds(15, 35, 115, 18);
            }
            panel1.add(P_SEL3);
            P_SEL3.setBounds(70, 60, 535, 65);

            //---- OBJ_51 ----
            OBJ_51.setTitle("Plage de num\u00e9ros clients");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(5, 10, 650, OBJ_51.getPreferredSize().height);

            //---- WTCLT ----
            WTCLT.setText("S\u00e9lection compl\u00e8te");
            WTCLT.setComponentPopupMenu(BTD);
            WTCLT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTCLT.setName("WTCLT");
            WTCLT.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTCLTActionPerformed(e);
              }
            });
            panel1.add(WTCLT);
            WTCLT.setBounds(20, 30, 141, WTCLT.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(30, 122, 700, 195);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_41 ----
            OBJ_41.setTitle("Types rattachements CNV");
            OBJ_41.setName("OBJ_41");
            panel2.add(OBJ_41);
            OBJ_41.setBounds(10, 15, 300, OBJ_41.getPreferredSize().height);

            //---- OPT1 ----
            OPT1.setText("groupe");
            OPT1.setComponentPopupMenu(BTD);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel2.add(OPT1);
            OPT1.setBounds(15, 35, 285, 16);

            //---- OPT2 ----
            OPT2.setText("famille");
            OPT2.setComponentPopupMenu(BTD);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel2.add(OPT2);
            OPT2.setBounds(15, 55, 285, 20);

            //---- OPT5 ----
            OPT5.setText("ensemble d'articles");
            OPT5.setComponentPopupMenu(BTD);
            OPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT5.setName("OPT5");
            panel2.add(OPT5);
            OPT5.setBounds(15, 79, 285, 17);

            //---- OPT3 ----
            OPT3.setText("article");
            OPT3.setComponentPopupMenu(BTD);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setName("OPT3");
            panel2.add(OPT3);
            OPT3.setBounds(15, 124, 285, 20);

            //---- OPT4 ----
            OPT4.setText("tarif");
            OPT4.setComponentPopupMenu(BTD);
            OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT4.setName("OPT4");
            panel2.add(OPT4);
            OPT4.setBounds(15, 100, 285, 20);

            //---- OPT6 ----
            OPT6.setText("regroupement d'achat");
            OPT6.setComponentPopupMenu(BTD);
            OPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT6.setName("OPT6");
            panel2.add(OPT6);
            OPT6.setBounds(15, 148, 285, 20);

            //---- OPT7 ----
            OPT7.setText("fournisseur");
            OPT7.setComponentPopupMenu(BTD);
            OPT7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT7.setName("OPT7");
            panel2.add(OPT7);
            OPT7.setBounds(15, 172, 285, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(35, 322, panel2.getPreferredSize().width, 203);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_42 ----
            OBJ_42.setTitle("");
            OBJ_42.setName("OBJ_42");
            panel3.add(OBJ_42);
            OBJ_42.setBounds(10, 20, 325, 2);

            //---- OBJ_83 ----
            OBJ_83.setText("\u00e0");
            OBJ_83.setName("OBJ_83");
            panel3.add(OBJ_83);
            OBJ_83.setBounds(114, 64, 20, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("Dates validit\u00e9 de");
            OBJ_69.setName("OBJ_69");
            panel3.add(OBJ_69);
            OBJ_69.setBounds(30, 35, 104, 20);

            //---- WCOLTR ----
            WCOLTR.setComponentPopupMenu(BTD);
            WCOLTR.setName("WCOLTR");
            panel3.add(WCOLTR);
            WCOLTR.setBounds(215, 108, 26, WCOLTR.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("Colonne de tarif \u00e0 \u00e9diter");
            OBJ_85.setName("OBJ_85");
            panel3.add(OBJ_85);
            OBJ_85.setBounds(30, 112, 144, 20);

            //---- WDAPD ----
            WDAPD.setName("WDAPD");
            panel3.add(WDAPD);
            WDAPD.setBounds(215, 31, 105, WDAPD.getPreferredSize().height);

            //---- WDAPF ----
            WDAPF.setName("WDAPF");
            panel3.add(WDAPF);
            WDAPF.setBounds(215, 60, 105, WDAPF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(350, 322, 380, 202);

          //======== panel4 ========
          {
            panel4.setBorder(null);
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_50 ----
            OBJ_50.setTitle("Plage de conditions de vente CN");
            OBJ_50.setName("OBJ_50");
            panel4.add(OBJ_50);
            OBJ_50.setBounds(5, 10, 650, OBJ_50.getPreferredSize().height);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- OBJ_60 ----
              OBJ_60.setText("Code de d\u00e9but");
              OBJ_60.setName("OBJ_60");
              P_SEL2.add(OBJ_60);
              OBJ_60.setBounds(10, 13, 110, 18);

              //---- OBJ_61 ----
              OBJ_61.setText("Code de fin");
              OBJ_61.setName("OBJ_61");
              P_SEL2.add(OBJ_61);
              OBJ_61.setBounds(10, 43, 110, 18);

              //---- OBJ_58 ----
              OBJ_58.setText("@CNLIBD@");
              OBJ_58.setName("OBJ_58");
              P_SEL2.add(OBJ_58);
              OBJ_58.setBounds(195, 10, 313, OBJ_58.getPreferredSize().height);

              //---- OBJ_64 ----
              OBJ_64.setText("@CNLIBF@");
              OBJ_64.setName("OBJ_64");
              P_SEL2.add(OBJ_64);
              OBJ_64.setBounds(195, 40, 313, OBJ_64.getPreferredSize().height);

              //---- DEBCNV ----
              DEBCNV.setComponentPopupMenu(BTD);
              DEBCNV.setName("DEBCNV");
              P_SEL2.add(DEBCNV);
              DEBCNV.setBounds(125, 8, 60, DEBCNV.getPreferredSize().height);

              //---- FINCNV ----
              FINCNV.setComponentPopupMenu(BTD);
              FINCNV.setName("FINCNV");
              P_SEL2.add(FINCNV);
              FINCNV.setBounds(125, 38, 60, FINCNV.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL2.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL2.setMinimumSize(preferredSize);
                P_SEL2.setPreferredSize(preferredSize);
              }
            }
            panel4.add(P_SEL2);
            P_SEL2.setBounds(70, 55, 562, 71);

            //---- WTCNV ----
            WTCNV.setText("S\u00e9lection compl\u00e8te");
            WTCNV.setComponentPopupMenu(BTD);
            WTCNV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTCNV.setName("WTCNV");
            WTCNV.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTCNVActionPerformed(e);
              }
            });
            panel4.add(WTCNV);
            WTCNV.setBounds(20, 30, 141, WTCNV.getPreferredSize().height);
          }
          p_contenu.add(panel4);
          panel4.setBounds(30, 122, 710, 145);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_43;
  private XRiCheckBox EXPMPR;
  private XRiCheckBox SAUT;
  private JPanel panel1;
  private JLabel OBJ_70;
  private XRiTextField REPDEB;
  private JLabel OBJ_84;
  private XRiTextField REPFIN;
  private JXTitledSeparator OBJ_67;
  private JPanel P_SEL3;
  private RiZoneSortie OBJ_59;
  private RiZoneSortie OBJ_65;
  private XRiTextField DEBCLI;
  private XRiTextField FINCLI;
  private JLabel OBJ_86;
  private JLabel OBJ_66;
  private JXTitledSeparator OBJ_51;
  private XRiCheckBox WTCLT;
  private JPanel panel2;
  private JXTitledSeparator OBJ_41;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT5;
  private XRiCheckBox OPT3;
  private XRiCheckBox OPT4;
  private XRiCheckBox OPT6;
  private XRiCheckBox OPT7;
  private JPanel panel3;
  private JXTitledSeparator OBJ_42;
  private JLabel OBJ_83;
  private JLabel OBJ_69;
  private XRiTextField WCOLTR;
  private JLabel OBJ_85;
  private XRiCalendrier WDAPD;
  private XRiCalendrier WDAPF;
  private JPanel panel4;
  private JXTitledSeparator OBJ_50;
  private JPanel P_SEL2;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private RiZoneSortie OBJ_58;
  private RiZoneSortie OBJ_64;
  private XRiTextField DEBCNV;
  private XRiTextField FINCNV;
  private XRiCheckBox WTCNV;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
