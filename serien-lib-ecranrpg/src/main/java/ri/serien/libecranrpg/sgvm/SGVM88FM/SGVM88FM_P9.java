
package ri.serien.libecranrpg.sgvm.SGVM88FM;
// Nom Fichier: pop_SGVM88FM_FMTP9_FMTF1_631.java

import java.awt.Cursor;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM88FM_P9 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM88FM_P9(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_7);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBTAR@")).trim());
    OBJ_4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NPAGE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_4.setVisible(lexique.isPresent("NPAGE"));
    OBJ_9.setVisible(lexique.isPresent("NBTAR"));
    
    // TODO Icones
    OBJ_7.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Edition terminée"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JButton();
    OBJ_6 = new JLabel();
    OBJ_8 = new JLabel();
    OBJ_7 = new JButton();
    OBJ_9 = new JLabel();
    OBJ_4 = new JLabel();

    //======== this ========
    setName("this");

    //---- OBJ_5 ----
    OBJ_5.setText("");
    OBJ_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_5.setName("OBJ_5");

    //---- OBJ_6 ----
    OBJ_6.setText("Nombre de Pages");
    OBJ_6.setName("OBJ_6");

    //---- OBJ_8 ----
    OBJ_8.setText("CNV trait\u00e9es");
    OBJ_8.setName("OBJ_8");

    //---- OBJ_7 ----
    OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_7.setName("OBJ_7");

    //---- OBJ_9 ----
    OBJ_9.setText("@NBTAR@");
    OBJ_9.setName("OBJ_9");

    //---- OBJ_4 ----
    OBJ_4.setText("@NPAGE@");
    OBJ_4.setName("OBJ_4");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(13, 13, 13)
          .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
          .addGap(43, 43, 43)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE))
            .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE))
          .addGap(14, 14, 14)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
            .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)))
        .addGroup(layout.createSequentialGroup()
          .addGap(280, 280, 280)
          .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(14, 14, 14)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addGap(6, 6, 6)
              .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addGap(8, 8, 8)
              .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          .addGap(8, 8, 8)
          .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton OBJ_5;
  private JLabel OBJ_6;
  private JLabel OBJ_8;
  private JButton OBJ_7;
  private JLabel OBJ_9;
  private JLabel OBJ_4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
