
package ri.serien.libecranrpg.sgvm.SGVM6TFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.documentvente.sntypeavoir.SNTypeAvoir;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM6TFM_B1 extends SNPanelEcranRPG implements ioFrame {
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private boolean isPlanning = false;
  private String[] cbCodeDate_Value = { "*DAT", "*ENC", "*PRE", };
  private String[] cbCodeDate_Text = { "Date du jour", "Mois en cours", "Mois précédant", };
  
  public SGVM6TFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    cbCodeDate.removeAllItems();
    for (String texte : cbCodeDate_Text) {
      cbCodeDate.addItem(texte);
    }
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    isPlanning = lexique.isTrue("96");
    
    cbCodeDate.setVisible(isPlanning);
    lbPlanning.setVisible(isPlanning);
    DATD.setVisible(!isPlanning);
    DATF.setVisible(!isPlanning);
    lbAu.setVisible(!isPlanning);
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(idEtablissement);
    
    // Représentant
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(idEtablissement);
    snRepresentant.charger(false);
    snRepresentant.setTousAutorise(true);
    snRepresentant.setSelectionParChampRPG(lexique, "REP");
    
    // Type d'avoir
    snTypeAvoir.setSession(getSession());
    snTypeAvoir.setIdEtablissement(idEtablissement);
    snTypeAvoir.charger(false);
    snTypeAvoir.setTousAutorise(true);
    snTypeAvoir.setSelectionParChampRPG(lexique, "TYPAV");
    
    p_bpresentation.setCodeEtablissement(idEtablissement.getCodeEtablissement());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snRepresentant.renseignerChampRPG(lexique, "REP");
    snTypeAvoir.renseignerChampRPG(lexique, "TYPAV");
    
    if (isPlanning) {
      lexique.HostFieldPutData("DATD", 0, cbCodeDate_Value[cbCodeDate.getSelectedIndex()]);
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    p_sud = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbPeriodeAEditer = new SNLabelChamp();
    pnlPeriodeEditer = new SNPanel();
    DATD = new XRiCalendrier();
    lbAu = new JLabel();
    DATF = new XRiCalendrier();
    cbCodeDate = new SNComboBox();
    sNLabelChamp1 = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    sNLabelChamp2 = new SNLabelChamp();
    snTypeAvoir = new SNTypeAvoir();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfEnCours = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      p_sud.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());
        
        // ======== pnlMessage ========
        {
          pnlMessage.setName("pnlMessage");
          pnlMessage.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbLOCTP ----
          lbLOCTP.setText("@LOCTP@");
          lbLOCTP.setMinimumSize(new Dimension(120, 30));
          lbLOCTP.setPreferredSize(new Dimension(120, 30));
          lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbLOCTP.setName("lbLOCTP");
          pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPlanning ----
          lbPlanning.setText("Vous \u00eates en cours de param\u00e9trage en vue de la mise au planning");
          lbPlanning.setMinimumSize(new Dimension(120, 30));
          lbPlanning.setPreferredSize(new Dimension(120, 30));
          lbPlanning.setName("lbPlanning");
          pnlMessage.add(lbPlanning, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlMessage, BorderLayout.NORTH);
        
        // ======== pnlColonne ========
        {
          pnlColonne.setName("pnlColonne");
          pnlColonne.setLayout(new GridLayout());
          
          // ======== pnlGauche ========
          {
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlCritereDeSelection ========
            {
              pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
              pnlCritereDeSelection.setName("pnlCritereDeSelection");
              pnlCritereDeSelection.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPeriodeAEditer ----
              lbPeriodeAEditer.setText("P\u00e9riode \u00e0 traiter");
              lbPeriodeAEditer.setMinimumSize(new Dimension(200, 30));
              lbPeriodeAEditer.setPreferredSize(new Dimension(200, 30));
              lbPeriodeAEditer.setMaximumSize(new Dimension(200, 30));
              lbPeriodeAEditer.setName("lbPeriodeAEditer");
              pnlCritereDeSelection.add(lbPeriodeAEditer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlPeriodeEditer ========
              {
                pnlPeriodeEditer.setName("pnlPeriodeEditer");
                pnlPeriodeEditer.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPeriodeEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPeriodeEditer.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPeriodeEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlPeriodeEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- DATD ----
                DATD.setFont(new Font("sansserif", Font.PLAIN, 14));
                DATD.setMinimumSize(new Dimension(110, 30));
                DATD.setPreferredSize(new Dimension(110, 30));
                DATD.setName("DATD");
                pnlPeriodeEditer.add(DATD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbAu ----
                lbAu.setText("au");
                lbAu.setHorizontalTextPosition(SwingConstants.CENTER);
                lbAu.setHorizontalAlignment(SwingConstants.CENTER);
                lbAu.setFont(new Font("sansserif", Font.PLAIN, 14));
                lbAu.setName("lbAu");
                pnlPeriodeEditer.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- DATF ----
                DATF.setFont(new Font("sansserif", Font.PLAIN, 14));
                DATF.setMinimumSize(new Dimension(110, 30));
                DATF.setPreferredSize(new Dimension(110, 30));
                DATF.setName("DATF");
                pnlPeriodeEditer.add(DATF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- cbCodeDate ----
                cbCodeDate.setModel(new DefaultComboBoxModel(new String[] { "Date du jour", "D\u00e9but du mois en cours",
                    "Fin du mois en cours", "D\u00e9but du mois pr\u00e9c\u00e9dent", "Fin du mois pr\u00e9c\u00e9dent" }));
                cbCodeDate.setBackground(Color.white);
                cbCodeDate.setFont(new Font("sansserif", Font.PLAIN, 14));
                cbCodeDate.setName("cbCodeDate");
                pnlPeriodeEditer.add(cbCodeDate, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlCritereDeSelection.add(pnlPeriodeEditer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp1 ----
              sNLabelChamp1.setText("Repr\u00e9sentant");
              sNLabelChamp1.setName("sNLabelChamp1");
              pnlCritereDeSelection.add(sNLabelChamp1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snRepresentant ----
              snRepresentant.setName("snRepresentant");
              pnlCritereDeSelection.add(snRepresentant, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp2 ----
              sNLabelChamp2.setText("Type d'avoir");
              sNLabelChamp2.setName("sNLabelChamp2");
              pnlCritereDeSelection.add(sNLabelChamp2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snTypeAvoir ----
              snTypeAvoir.setName("snTypeAvoir");
              pnlCritereDeSelection.add(snTypeAvoir, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlColonne.add(pnlGauche);
          
          // ======== pnlDroite ========
          {
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlEtablissement ========
            {
              pnlEtablissement.setTitre("Etablissement");
              pnlEtablissement.setName("pnlEtablissement");
              pnlEtablissement.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- lbEtablissement ----
              lbEtablissement.setText("Etablissement en cours");
              lbEtablissement.setName("lbEtablissement");
              pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snEtablissement ----
              snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
              snEtablissement.setName("snEtablissement");
              pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbPeriodeEnCours ----
              lbPeriodeEnCours.setText("P\u00e9riode en cours");
              lbPeriodeEnCours.setName("lbPeriodeEnCours");
              pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfEnCours ----
              tfEnCours.setText("@WENCX@");
              tfEnCours.setEditable(false);
              tfEnCours.setMinimumSize(new Dimension(260, 30));
              tfEnCours.setPreferredSize(new Dimension(260, 30));
              tfEnCours.setEnabled(false);
              tfEnCours.setName("tfEnCours");
              pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlColonne.add(pnlDroite);
        }
        pnlContenu.add(pnlColonne, BorderLayout.CENTER);
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanelFond p_sud;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNMessage lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbPeriodeAEditer;
  private SNPanel pnlPeriodeEditer;
  private XRiCalendrier DATD;
  private JLabel lbAu;
  private XRiCalendrier DATF;
  private SNComboBox cbCodeDate;
  private SNLabelChamp sNLabelChamp1;
  private SNRepresentant snRepresentant;
  private SNLabelChamp sNLabelChamp2;
  private SNTypeAvoir snTypeAvoir;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfEnCours;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
