
package ri.serien.libecranrpg.sgvm.SGVMT1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMT1FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMT1FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU3.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    
    
    CODTRP.setEnabled(lexique.isPresent("CODTRP"));
    MA12.setEnabled(lexique.isPresent("MA12"));
    MA11.setEnabled(lexique.isPresent("MA11"));
    MA10.setEnabled(lexique.isPresent("MA10"));
    MA09.setEnabled(lexique.isPresent("MA09"));
    MA08.setEnabled(lexique.isPresent("MA08"));
    MA07.setEnabled(lexique.isPresent("MA07"));
    MA06.setEnabled(lexique.isPresent("MA06"));
    MA05.setEnabled(lexique.isPresent("MA05"));
    MA04.setEnabled(lexique.isPresent("MA04"));
    MA03.setEnabled(lexique.isPresent("MA03"));
    MA02.setEnabled(lexique.isPresent("MA02"));
    MA01.setEnabled(lexique.isPresent("MA01"));
    SUFFIN.setVisible(lexique.isPresent("SUFFIN"));
    SUFDEB.setVisible(lexique.isPresent("SUFDEB"));
    CLIFIN.setVisible(lexique.isPresent("CLIFIN"));
    CLIDEB.setVisible(lexique.isPresent("CLIDEB"));
    MA01_ck.setSelected(lexique.HostFieldGetData("MA01").equalsIgnoreCase("**"));
    // DATREF.setEnabled( lexique.isPresent("DATREF"));
    FAMFIN.setEnabled(lexique.isPresent("FAMFIN"));
    FAMDEB.setEnabled(lexique.isPresent("FAMDEB"));
    // WTOU3.setVisible( lexique.isPresent("WTOU3"));
    // WTOU3.setEnabled( lexique.isPresent("WTOU3"));
    // WTOU3.setSelected(lexique.HostFieldGetData("WTOU3").equalsIgnoreCase("**"));
    ARTFIN.setEnabled(lexique.isPresent("ARTFIN"));
    ARTDEB.setEnabled(lexique.isPresent("ARTDEB"));
    // WTOU1.setVisible( lexique.isPresent("WTOU1"));
    // WTOU1.setEnabled( lexique.isPresent("WTOU1"));
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    // WTOU2.setVisible( lexique.isPresent("WTOU2"));
    // WTOU2.setEnabled( lexique.isPresent("WTOU2"));
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    
    // ********************* Visibilité panels
    panel5.setVisible(!MA01_ck.isSelected());
    panel8.setVisible(!WTOU1.isSelected());
    panel4.setVisible(!WTOU2.isSelected());
    panel2.setVisible(!WTOU3.isSelected());
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (MA01_ck.isSelected()) {
      lexique.HostFieldPutData("MA01", 0, "**");
    }
    else {
      lexique.HostFieldPutData("MA01", 0, "  ");
      // if (WTOU3.isSelected())
      // lexique.HostFieldPutData("WTOU3", 0, "**");
      // else
      // lexique.HostFieldPutData("WTOU3", 0, " ");
      // if (WTOU1.isSelected())
      // lexique.HostFieldPutData("WTOU1", 0, "**");
      // else
      // lexique.HostFieldPutData("WTOU1", 0, " ");
      // if (WTOU2.isSelected())
      // lexique.HostFieldPutData("WTOU2", 0, "**");
      // else
      // lexique.HostFieldPutData("WTOU2", 0, " ");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU3ActionPerformed(ActionEvent e) {
    panel2.setVisible(!panel2.isVisible());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    panel4.setVisible(!panel4.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    panel8.setVisible(!panel8.isVisible());
  }
  
  private void MA01ActionPerformed(ActionEvent e) {
    panel5.setVisible(!panel5.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_27 = new JXTitledSeparator();
    OBJ_62 = new JLabel();
    panel3 = new JPanel();
    OBJ_24 = new JXTitledSeparator();
    WTOU2 = new XRiCheckBox();
    panel4 = new JPanel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    OBJ_66 = new JLabel();
    OBJ_59 = new JLabel();
    panel7 = new JPanel();
    WTOU1 = new XRiCheckBox();
    CODTRP = new XRiTextField();
    OBJ_65 = new JLabel();
    OBJ_25 = new JXTitledSeparator();
    panel8 = new JPanel();
    OBJ_63 = new JLabel();
    CLIDEB = new XRiTextField();
    SUFDEB = new XRiTextField();
    SUFFIN = new XRiTextField();
    CLIFIN = new XRiTextField();
    OBJ_64 = new JLabel();
    panel6 = new JPanel();
    panel5 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA10 = new XRiTextField();
    MA09 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    MA01_ck = new JCheckBox();
    xTitledSeparator1 = new JXTitledSeparator();
    DATREF = new XRiCalendrier();
    panel1 = new JPanel();
    OBJ_23 = new JXTitledSeparator();
    WTOU3 = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_60 = new JLabel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_61 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(695, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_27 ----
          OBJ_27.setTitle("Codes magasins \u00e0 traiter");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_62 ----
          OBJ_62.setText("Date de r\u00e9f\u00e9rence");
          OBJ_62.setName("OBJ_62");

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_24 ----
            OBJ_24.setTitle("Plage de codes groupe-famille");
            OBJ_24.setName("OBJ_24");
            panel3.add(OBJ_24);
            OBJ_24.setBounds(5, 5, 625, OBJ_24.getPreferredSize().height);

            //---- WTOU2 ----
            WTOU2.setText("Tous les codes groupe-famille");
            WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU2.setName("WTOU2");
            WTOU2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU2ActionPerformed(e);
              }
            });
            panel3.add(WTOU2);
            WTOU2.setBounds(20, 50, 207, 20);

            //======== panel4 ========
            {
              panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- FAMDEB ----
              FAMDEB.setComponentPopupMenu(BTD);
              FAMDEB.setName("FAMDEB");
              panel4.add(FAMDEB);
              FAMDEB.setBounds(160, 10, 44, FAMDEB.getPreferredSize().height);

              //---- FAMFIN ----
              FAMFIN.setComponentPopupMenu(BTD);
              FAMFIN.setName("FAMFIN");
              panel4.add(FAMFIN);
              FAMFIN.setBounds(160, 40, 44, FAMFIN.getPreferredSize().height);

              //---- OBJ_66 ----
              OBJ_66.setText("Groupe-famille de d\u00e9but");
              OBJ_66.setName("OBJ_66");
              panel4.add(OBJ_66);
              OBJ_66.setBounds(15, 14, 140, 20);

              //---- OBJ_59 ----
              OBJ_59.setText("Groupe-famille de fin");
              OBJ_59.setName("OBJ_59");
              panel4.add(OBJ_59);
              OBJ_59.setBounds(15, 44, 126, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel4);
            panel4.setBounds(235, 25, 385, 70);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- WTOU1 ----
            WTOU1.setText("Tous les num\u00e9ros clients");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });
            panel7.add(WTOU1);
            WTOU1.setBounds(20, 62, 174, 20);

            //---- CODTRP ----
            CODTRP.setComponentPopupMenu(BTD);
            CODTRP.setName("CODTRP");
            panel7.add(CODTRP);
            CODTRP.setBounds(175, 25, 34, CODTRP.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("Transporteur");
            OBJ_65.setName("OBJ_65");
            panel7.add(OBJ_65);
            OBJ_65.setBounds(20, 30, 81, 20);

            //---- OBJ_25 ----
            OBJ_25.setTitle("Plage des num\u00e9ros clients");
            OBJ_25.setName("OBJ_25");
            panel7.add(OBJ_25);
            OBJ_25.setBounds(5, 5, 625, OBJ_25.getPreferredSize().height);

            //======== panel8 ========
            {
              panel8.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel8.setOpaque(false);
              panel8.setName("panel8");
              panel8.setLayout(null);

              //---- OBJ_63 ----
              OBJ_63.setText("Client de d\u00e9but");
              OBJ_63.setName("OBJ_63");
              panel8.add(OBJ_63);
              OBJ_63.setBounds(15, 14, 91, 20);

              //---- CLIDEB ----
              CLIDEB.setComponentPopupMenu(BTD);
              CLIDEB.setName("CLIDEB");
              panel8.add(CLIDEB);
              CLIDEB.setBounds(160, 10, 60, CLIDEB.getPreferredSize().height);

              //---- SUFDEB ----
              SUFDEB.setComponentPopupMenu(BTD);
              SUFDEB.setName("SUFDEB");
              panel8.add(SUFDEB);
              SUFDEB.setBounds(220, 10, 36, SUFDEB.getPreferredSize().height);

              //---- SUFFIN ----
              SUFFIN.setComponentPopupMenu(BTD);
              SUFFIN.setName("SUFFIN");
              panel8.add(SUFFIN);
              SUFFIN.setBounds(220, 40, 36, SUFFIN.getPreferredSize().height);

              //---- CLIFIN ----
              CLIFIN.setComponentPopupMenu(BTD);
              CLIFIN.setName("CLIFIN");
              panel8.add(CLIFIN);
              CLIFIN.setBounds(160, 40, 60, CLIFIN.getPreferredSize().height);

              //---- OBJ_64 ----
              OBJ_64.setText("Client de fin");
              OBJ_64.setName("OBJ_64");
              panel8.add(OBJ_64);
              OBJ_64.setBounds(15, 44, 70, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }
            panel7.add(panel8);
            panel8.setBounds(235, 35, 385, 75);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //======== panel5 ========
            {
              panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              panel5.add(MA01);
              MA01.setBounds(10, 10, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              panel5.add(MA02);
              MA02.setBounds(46, 10, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              panel5.add(MA03);
              MA03.setBounds(82, 10, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              panel5.add(MA04);
              MA04.setBounds(118, 10, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              panel5.add(MA05);
              MA05.setBounds(154, 10, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              panel5.add(MA06);
              MA06.setBounds(190, 10, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              panel5.add(MA07);
              MA07.setBounds(226, 10, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              panel5.add(MA08);
              MA08.setBounds(262, 10, 34, MA08.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              panel5.add(MA10);
              MA10.setBounds(334, 10, 34, MA10.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              panel5.add(MA09);
              MA09.setBounds(298, 10, 34, MA09.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              panel5.add(MA11);
              MA11.setBounds(370, 10, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              panel5.add(MA12);
              MA12.setBounds(406, 10, 34, MA12.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel6.add(panel5);
            panel5.setBounds(160, 5, 445, 50);

            //---- MA01_ck ----
            MA01_ck.setText("Tous");
            MA01_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MA01_ck.setName("MA01_ck");
            MA01_ck.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                MA01ActionPerformed(e);
              }
            });
            panel6.add(MA01_ck);
            MA01_ck.setBounds(5, 20, 52, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("");
          xTitledSeparator1.setName("xTitledSeparator1");

          //---- DATREF ----
          DATREF.setName("DATREF");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_23 ----
            OBJ_23.setTitle("Plage articles \u00e0 traiter");
            OBJ_23.setName("OBJ_23");
            panel1.add(OBJ_23);
            OBJ_23.setBounds(5, 5, 625, OBJ_23.getPreferredSize().height);

            //---- WTOU3 ----
            WTOU3.setText("Tous les articles");
            WTOU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU3.setName("WTOU3");
            WTOU3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU3ActionPerformed(e);
              }
            });
            panel1.add(WTOU3);
            WTOU3.setBounds(20, 52, 123, 20);

            //======== panel2 ========
            {
              panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_60 ----
              OBJ_60.setText("Code de d\u00e9but");
              OBJ_60.setName("OBJ_60");
              panel2.add(OBJ_60);
              OBJ_60.setBounds(15, 14, 93, 20);

              //---- ARTDEB ----
              ARTDEB.setComponentPopupMenu(BTD);
              ARTDEB.setName("ARTDEB");
              panel2.add(ARTDEB);
              ARTDEB.setBounds(160, 10, 214, ARTDEB.getPreferredSize().height);

              //---- ARTFIN ----
              ARTFIN.setComponentPopupMenu(BTD);
              ARTFIN.setName("ARTFIN");
              panel2.add(ARTFIN);
              ARTFIN.setBounds(160, 40, 214, ARTFIN.getPreferredSize().height);

              //---- OBJ_61 ----
              OBJ_61.setText("Code de fin");
              OBJ_61.setName("OBJ_61");
              panel2.add(OBJ_61);
              OBJ_61.setBounds(15, 44, 72, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(235, 25, 385, 75);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(DATREF, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 615, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DATREF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(55, 55, 55)
                    .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_27;
  private JLabel OBJ_62;
  private JPanel panel3;
  private JXTitledSeparator OBJ_24;
  private XRiCheckBox WTOU2;
  private JPanel panel4;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JLabel OBJ_66;
  private JLabel OBJ_59;
  private JPanel panel7;
  private XRiCheckBox WTOU1;
  private XRiTextField CODTRP;
  private JLabel OBJ_65;
  private JXTitledSeparator OBJ_25;
  private JPanel panel8;
  private JLabel OBJ_63;
  private XRiTextField CLIDEB;
  private XRiTextField SUFDEB;
  private XRiTextField SUFFIN;
  private XRiTextField CLIFIN;
  private JLabel OBJ_64;
  private JPanel panel6;
  private JPanel panel5;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA10;
  private XRiTextField MA09;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JCheckBox MA01_ck;
  private JXTitledSeparator xTitledSeparator1;
  private XRiCalendrier DATREF;
  private JPanel panel1;
  private JXTitledSeparator OBJ_23;
  private XRiCheckBox WTOU3;
  private JPanel panel2;
  private JLabel OBJ_60;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_61;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
