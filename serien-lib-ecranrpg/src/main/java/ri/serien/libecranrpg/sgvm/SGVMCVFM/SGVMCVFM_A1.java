
package ri.serien.libecranrpg.sgvm.SGVMCVFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVMCVFM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMCVFM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    


    
    
    // PCVCNV.setVisible(!lexique.HostFieldGetData("PCVCNV").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    
    // Titre
    // setTitle(interpreteurD.analyseExpression("@PCVETB@ - FICHE CLIENT @CLCLI@ @CLLIV@"));
    
    setTitle(interpreteurD.analyseExpression("@PCVETB@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCNG");
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCNG");
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCNP");
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCNP");
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCNV");
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCNV");
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCPC");
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PCVCPC");
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_17_OBJ_17 = new JLabel();
    OBJ_23_OBJ_23 = new JLabel();
    OBJ_24_OBJ_24 = new JLabel();
    OBJ_25_OBJ_25 = new JLabel();
    PCVCNG = new XRiTextField();
    CNDGS = new XRiTextField();
    PCVCNP = new XRiTextField();
    CNDGP = new XRiTextField();
    CNCLIS = new XRiTextField();
    PCVCPC = new XRiTextField();
    CNCLIP = new XRiTextField();
    PCVCNV = new XRiTextField();
    OBJ_26 = new SNBoutonDetail();
    OBJ_27 = new SNBoutonDetail();
    OBJ_28 = new SNBoutonDetail();
    OBJ_29 = new SNBoutonDetail();
    OBJ_30 = new SNBoutonDetail();
    OBJ_32 = new SNBoutonDetail();
    OBJ_34 = new SNBoutonDetail();
    OBJ_35 = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(1015, 255));
    setPreferredSize(new Dimension(1015, 255));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(170, 400));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setMinimumSize(new Dimension(170, 400));
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Conditions g\u00e9n\u00e9rales");
              riSousMenu_bt6.setToolTipText("Conditions de vente g\u00e9n\u00e9rales");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Conditions quantitatives");
              riSousMenu_bt7.setToolTipText("Conditions quantitatives");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Acc\u00e9s bloc-notes ");
              riSousMenu_bt14.setToolTipText("Acc\u00e9s bloc-notes condition de vente client/article");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(825, 255));
        p_contenu.setMinimumSize(new Dimension(825, 255));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_17_OBJ_17 ----
          OBJ_17_OBJ_17.setText("Conditions standards \u00e9tablissement");
          OBJ_17_OBJ_17.setName("OBJ_17_OBJ_17");
          panel2.add(OBJ_17_OBJ_17);
          OBJ_17_OBJ_17.setBounds(26, 37, 252, 20);

          //---- OBJ_23_OBJ_23 ----
          OBJ_23_OBJ_23.setText("Conditions promotionnelles \u00e9tablissement");
          OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");
          panel2.add(OBJ_23_OBJ_23);
          OBJ_23_OBJ_23.setBounds(26, 77, 252, 20);

          //---- OBJ_24_OBJ_24 ----
          OBJ_24_OBJ_24.setText("Conditions standards client");
          OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");
          panel2.add(OBJ_24_OBJ_24);
          OBJ_24_OBJ_24.setBounds(26, 120, 228, 20);

          //---- OBJ_25_OBJ_25 ----
          OBJ_25_OBJ_25.setText("Conditions promotionnelles client");
          OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
          panel2.add(OBJ_25_OBJ_25);
          OBJ_25_OBJ_25.setBounds(26, 162, 233, 20);

          //---- PCVCNG ----
          PCVCNG.setName("PCVCNG");
          panel2.add(PCVCNG);
          PCVCNG.setBounds(286, 33, 70, PCVCNG.getPreferredSize().height);

          //---- CNDGS ----
          CNDGS.setName("CNDGS");
          panel2.add(CNDGS);
          CNDGS.setBounds(356, 33, 310, CNDGS.getPreferredSize().height);

          //---- PCVCNP ----
          PCVCNP.setName("PCVCNP");
          panel2.add(PCVCNP);
          PCVCNP.setBounds(286, 73, 70, PCVCNP.getPreferredSize().height);

          //---- CNDGP ----
          CNDGP.setName("CNDGP");
          panel2.add(CNDGP);
          CNDGP.setBounds(356, 73, 310, CNDGP.getPreferredSize().height);

          //---- CNCLIS ----
          CNCLIS.setName("CNCLIS");
          panel2.add(CNCLIS);
          CNCLIS.setBounds(356, 116, 310, CNCLIS.getPreferredSize().height);

          //---- PCVCPC ----
          PCVCPC.setName("PCVCPC");
          panel2.add(PCVCPC);
          PCVCPC.setBounds(286, 158, 70, PCVCPC.getPreferredSize().height);

          //---- CNCLIP ----
          CNCLIP.setName("CNCLIP");
          panel2.add(CNCLIP);
          CNCLIP.setBounds(356, 158, 310, CNCLIP.getPreferredSize().height);

          //---- PCVCNV ----
          PCVCNV.setName("PCVCNV");
          panel2.add(PCVCNV);
          PCVCNV.setBounds(286, 116, 70, PCVCNV.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("");
          OBJ_26.setToolTipText("Conditions de vente g\u00e9n\u00e9rales");
          OBJ_26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_26.setName("OBJ_26");
          OBJ_26.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_26ActionPerformed(e);
            }
          });
          panel2.add(OBJ_26);
          OBJ_26.setBounds(665, 27, 40, 40);

          //---- OBJ_27 ----
          OBJ_27.setText("");
          OBJ_27.setToolTipText("Conditions quantitatives");
          OBJ_27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_27.setName("OBJ_27");
          OBJ_27.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_27ActionPerformed(e);
            }
          });
          panel2.add(OBJ_27);
          OBJ_27.setBounds(700, 27, 40, 40);

          //---- OBJ_28 ----
          OBJ_28.setText("");
          OBJ_28.setToolTipText("Conditions de vente g\u00e9n\u00e9rales");
          OBJ_28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_28.setName("OBJ_28");
          OBJ_28.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_28ActionPerformed(e);
            }
          });
          panel2.add(OBJ_28);
          OBJ_28.setBounds(665, 67, 40, 40);

          //---- OBJ_29 ----
          OBJ_29.setText("");
          OBJ_29.setToolTipText("Conditions quantitatives");
          OBJ_29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_29.setName("OBJ_29");
          OBJ_29.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_29ActionPerformed(e);
            }
          });
          panel2.add(OBJ_29);
          OBJ_29.setBounds(700, 67, 40, 40);

          //---- OBJ_30 ----
          OBJ_30.setText("");
          OBJ_30.setToolTipText("Conditions de vente g\u00e9n\u00e9rales");
          OBJ_30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_30.setName("OBJ_30");
          OBJ_30.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_30ActionPerformed(e);
            }
          });
          panel2.add(OBJ_30);
          OBJ_30.setBounds(665, 110, 40, 40);

          //---- OBJ_32 ----
          OBJ_32.setText("");
          OBJ_32.setToolTipText("Conditions quantitatives");
          OBJ_32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_32.setName("OBJ_32");
          OBJ_32.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_32ActionPerformed(e);
            }
          });
          panel2.add(OBJ_32);
          OBJ_32.setBounds(700, 110, 40, 40);

          //---- OBJ_34 ----
          OBJ_34.setText("");
          OBJ_34.setToolTipText("Conditions de vente g\u00e9n\u00e9rales");
          OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_34.setName("OBJ_34");
          OBJ_34.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_34ActionPerformed(e);
            }
          });
          panel2.add(OBJ_34);
          OBJ_34.setBounds(665, 152, 40, 40);

          //---- OBJ_35 ----
          OBJ_35.setText("");
          OBJ_35.setToolTipText("Conditions quantitatives");
          OBJ_35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_35.setName("OBJ_35");
          OBJ_35.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_35ActionPerformed(e);
            }
          });
          panel2.add(OBJ_35);
          OBJ_35.setBounds(700, 152, 40, 40);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(15, 25, 810, 205);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_17_OBJ_17;
  private JLabel OBJ_23_OBJ_23;
  private JLabel OBJ_24_OBJ_24;
  private JLabel OBJ_25_OBJ_25;
  private XRiTextField PCVCNG;
  private XRiTextField CNDGS;
  private XRiTextField PCVCNP;
  private XRiTextField CNDGP;
  private XRiTextField CNCLIS;
  private XRiTextField PCVCPC;
  private XRiTextField CNCLIP;
  private XRiTextField PCVCNV;
  private SNBoutonDetail OBJ_26;
  private SNBoutonDetail OBJ_27;
  private SNBoutonDetail OBJ_28;
  private SNBoutonDetail OBJ_29;
  private SNBoutonDetail OBJ_30;
  private SNBoutonDetail OBJ_32;
  private SNBoutonDetail OBJ_34;
  private SNBoutonDetail OBJ_35;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
