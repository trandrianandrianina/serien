/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM88FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente.SNConditionVenteEtClient;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Ecran pour modifier un groupe de conditions de ventes.
 * 
 * Points de menu :
 * [GVM652] Gestion des ventes -> Opérations et éditions diverses -> Mise à Jour de prix -> Modification de condition de vente
 */
public class SGVM88FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private String MODE_CONDITION_QUANTITATIVE = "Q";
  private String[] UARR_Title = { "", "0,05", "0,10", "0,25", "0,50", "1,00", "10,00", };
  private String[] UARR_Value = { "", "1", "2", "3", "4", "5", "6", };
  
  /**
   * Constructeur.
   */
  public SGVM88FM_B1(ArrayList<?> param) {
    super(param);
    
    // Initialiser les composants
    initComponents();
    
    // Configurer la version 2 du look
    setVersionLook(2);
    initDiverses();
    
    // Configurer les valeurs RPG associées aux boîtes à cocher
    OPT2.setValeursSelection("1", " ");
    OPT1.setValeursSelection("1", " ");
    UTYP5.setValeursSelection("R", " ");
    UTYP4.setValeursSelection("G", " ");
    UTYP3.setValeursSelection("F", " ");
    UTYP2.setValeursSelection("T", " ");
    UTYP1.setValeursSelection("A", " ");
    
    // Configurer les valeurs de la liste déroulante
    UARR.setValeurs(UARR_Value, UARR_Title);
    
    // Configuer les valeurs de la liste déroulante pour les conditions quantitiatives
    cbConditionQuantitative.addItem("Ne pas modifier les conditions quantitatives");
    cbConditionQuantitative.addItem("Modifier uniquement les conditions quantitatives");
    
    // Ajouter la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        traiterBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation
        .setText(lexique.TranslationTable(interpreteurD.analyseExpression("Modifications de conditions de ventes @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Ne pas afficher de sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Mettre à jour le logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charger l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charger la condition de vente (en réalité le paramètre CN ou l'identifiant client)
    snConditionVenteEtClient.setSession(getSession());
    snConditionVenteEtClient.setIdEtablissement(snEtablissement.getIdSelection());
    snConditionVenteEtClient.setSelectionParChampRPG(lexique, "UCNV");
    
    // Mettre à jour le critère sur les conditions quantitatives
    if (lexique.HostFieldGetData("UCAT").equals(MODE_CONDITION_QUANTITATIVE)) {
      cbConditionQuantitative.setSelectedIndex(1);
    }
    else {
      cbConditionQuantitative.setSelectedIndex(0);
    }
    
    // Charger les devises
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(snEtablissement.getIdSelection());
    snDevise.setAucunAutorise(true);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "UDEV");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Renseigner la condition de vente
    snConditionVenteEtClient.renseignerChampRPG(lexique, "UCNV");
    
    // Renseigner la devise
    snDevise.renseignerChampRPG(lexique, "UDEV");
    
    // Renseigner le critère sur les conditions de ventes
    if (cbConditionQuantitative.getSelectedIndex() == 1) {
      lexique.HostFieldPutData("UCAT", 0, MODE_CONDITION_QUANTITATIVE);
    }
    else {
      lexique.HostFieldPutData("UCAT", 0, " ");
    }
  }
  
  /**
   * Traiter les clics sur les boutons.
   */
  private void traiterBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbCNV = new SNLabelChamp();
    snConditionVenteEtClient = new SNConditionVenteEtClient();
    lbRattachements = new SNLabelChamp();
    UTYP1 = new XRiCheckBox();
    UTYP2 = new XRiCheckBox();
    UTYP3 = new XRiCheckBox();
    UTYP4 = new XRiCheckBox();
    UTYP5 = new XRiCheckBox();
    lbConditionQuantitative = new SNLabelChamp();
    cbConditionQuantitative = new XRiComboBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionTraitement = new SNPanelTitre();
    lbCoeff = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    UCOEF = new XRiTextField();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    lbArrondi = new SNLabelChamp();
    UARR = new XRiComboBox();
    lbDevise2 = new SNLabelChamp();
    snDevise = new SNDevise();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(960, 640));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Modification de conditions de ventes @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("LOCTP");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbCNV ----
            lbCNV.setText("Rattachement client");
            lbCNV.setMaximumSize(new Dimension(170, 30));
            lbCNV.setMinimumSize(new Dimension(170, 30));
            lbCNV.setPreferredSize(new Dimension(170, 30));
            lbCNV.setName("lbCNV");
            pnlCritereDeSelection.add(lbCNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snConditionVenteEtClient ----
            snConditionVenteEtClient.setName("snConditionVenteEtClient");
            pnlCritereDeSelection.add(snConditionVenteEtClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbRattachements ----
            lbRattachements.setText("Type rattachement article");
            lbRattachements.setMaximumSize(new Dimension(170, 30));
            lbRattachements.setMinimumSize(new Dimension(170, 30));
            lbRattachements.setPreferredSize(new Dimension(170, 30));
            lbRattachements.setName("lbRattachements");
            pnlCritereDeSelection.add(lbRattachements, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- UTYP1 ----
            UTYP1.setText("Articles");
            UTYP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP1.setMinimumSize(new Dimension(150, 30));
            UTYP1.setPreferredSize(new Dimension(150, 30));
            UTYP1.setFont(new Font("sansserif", Font.PLAIN, 14));
            UTYP1.setName("UTYP1");
            pnlCritereDeSelection.add(UTYP1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- UTYP2 ----
            UTYP2.setText("Tarifs");
            UTYP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP2.setMinimumSize(new Dimension(150, 30));
            UTYP2.setPreferredSize(new Dimension(150, 30));
            UTYP2.setFont(new Font("sansserif", Font.PLAIN, 14));
            UTYP2.setName("UTYP2");
            pnlCritereDeSelection.add(UTYP2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- UTYP3 ----
            UTYP3.setText("Famille d'articles");
            UTYP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP3.setMinimumSize(new Dimension(150, 30));
            UTYP3.setPreferredSize(new Dimension(150, 30));
            UTYP3.setFont(new Font("sansserif", Font.PLAIN, 14));
            UTYP3.setName("UTYP3");
            pnlCritereDeSelection.add(UTYP3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- UTYP4 ----
            UTYP4.setText("Groupe d'articles");
            UTYP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP4.setMinimumSize(new Dimension(150, 30));
            UTYP4.setPreferredSize(new Dimension(150, 30));
            UTYP4.setFont(new Font("sansserif", Font.PLAIN, 14));
            UTYP4.setName("UTYP4");
            pnlCritereDeSelection.add(UTYP4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- UTYP5 ----
            UTYP5.setText("Ensemble d'articles");
            UTYP5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP5.setMinimumSize(new Dimension(150, 30));
            UTYP5.setPreferredSize(new Dimension(150, 30));
            UTYP5.setFont(new Font("sansserif", Font.PLAIN, 14));
            UTYP5.setName("UTYP5");
            pnlCritereDeSelection.add(UTYP5, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbConditionQuantitative ----
            lbConditionQuantitative.setText("Conditions quantitatives");
            lbConditionQuantitative.setMaximumSize(new Dimension(170, 30));
            lbConditionQuantitative.setMinimumSize(new Dimension(170, 30));
            lbConditionQuantitative.setPreferredSize(new Dimension(170, 30));
            lbConditionQuantitative.setName("lbConditionQuantitative");
            pnlCritereDeSelection.add(lbConditionQuantitative, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbConditionQuantitative ----
            cbConditionQuantitative.setComponentPopupMenu(null);
            cbConditionQuantitative.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            cbConditionQuantitative.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbConditionQuantitative.setBackground(Color.white);
            cbConditionQuantitative.setMinimumSize(new Dimension(100, 30));
            cbConditionQuantitative.setPreferredSize(new Dimension(100, 30));
            cbConditionQuantitative.setName("cbConditionQuantitative");
            pnlCritereDeSelection.add(cbConditionQuantitative, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEditable(false);
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlOptionTraitement ========
          {
            pnlOptionTraitement.setTitre("Option de traitement");
            pnlOptionTraitement.setName("pnlOptionTraitement");
            pnlOptionTraitement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionTraitement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbCoeff ----
            lbCoeff.setText("Coefficient \u00e0 appliquer");
            lbCoeff.setName("lbCoeff");
            pnlOptionTraitement.add(lbCoeff, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- UCOEF ----
              UCOEF.setMinimumSize(new Dimension(80, 30));
              UCOEF.setPreferredSize(new Dimension(80, 30));
              UCOEF.setFont(new Font("sansserif", Font.PLAIN, 14));
              UCOEF.setName("UCOEF");
              sNPanel1.add(UCOEF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OPT1 ----
              OPT1.setText("sur le prix net");
              OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
              OPT1.setName("OPT1");
              sNPanel1.add(OPT1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OPT2 ----
              OPT2.setText("Sur le prix de base");
              OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
              OPT2.setName("OPT2");
              sNPanel1.add(OPT2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOptionTraitement.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArrondi ----
            lbArrondi.setText("Arrondi");
            lbArrondi.setName("lbArrondi");
            pnlOptionTraitement.add(lbArrondi, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- UARR ----
            UARR.setComponentPopupMenu(null);
            UARR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UARR.setFont(new Font("sansserif", Font.PLAIN, 14));
            UARR.setBackground(Color.white);
            UARR.setMinimumSize(new Dimension(100, 30));
            UARR.setPreferredSize(new Dimension(100, 30));
            UARR.setName("UARR");
            pnlOptionTraitement.add(UARR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDevise2 ----
            lbDevise2.setText("Devise");
            lbDevise2.setName("lbDevise2");
            pnlOptionTraitement.add(lbDevise2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snDevise ----
            snDevise.setName("snDevise");
            pnlOptionTraitement.add(snDevise, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbCNV;
  private SNConditionVenteEtClient snConditionVenteEtClient;
  private SNLabelChamp lbRattachements;
  private XRiCheckBox UTYP1;
  private XRiCheckBox UTYP2;
  private XRiCheckBox UTYP3;
  private XRiCheckBox UTYP4;
  private XRiCheckBox UTYP5;
  private SNLabelChamp lbConditionQuantitative;
  private XRiComboBox cbConditionQuantitative;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionTraitement;
  private SNLabelChamp lbCoeff;
  private SNPanel sNPanel1;
  private XRiTextField UCOEF;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private SNLabelChamp lbArrondi;
  private XRiComboBox UARR;
  private SNLabelChamp lbDevise2;
  private SNDevise snDevise;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
