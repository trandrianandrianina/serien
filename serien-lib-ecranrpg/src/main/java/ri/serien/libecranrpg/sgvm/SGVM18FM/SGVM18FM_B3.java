
package ri.serien.libecranrpg.sgvm.SGVM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVM18FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM18FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM01@")).trim());
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM02@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM03@")).trim());
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM04@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM05@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM06@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM07@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM08@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM09@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM11@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM12@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM13@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM14@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM15@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM16@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM17@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM18@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM19@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM20@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO Récup spécifique
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_6 = new JPanel();
    OBJ_10 = new JLabel();
    NUMDEB = new XRiTextField();
    SUFDEB = new XRiTextField();
    panel1 = new JPanel();
    OBJ_15 = new JLabel();
    OBJ_16 = new JLabel();
    OBJ_17 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_19 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_34 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(405, 360));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== OBJ_6 ========
        {
          OBJ_6.setBorder(new TitledBorder(new EtchedBorder(), "", TitledBorder.LEADING, TitledBorder.TOP));
          OBJ_6.setOpaque(false);
          OBJ_6.setName("OBJ_6");
          OBJ_6.setLayout(null);

          //---- OBJ_10 ----
          OBJ_10.setText("Num\u00e9ro du bon \u00e0 traiter");
          OBJ_10.setName("OBJ_10");
          OBJ_6.add(OBJ_10);
          OBJ_10.setBounds(11, 9, 142, 18);

          //---- NUMDEB ----
          NUMDEB.setComponentPopupMenu(BTD);
          NUMDEB.setName("NUMDEB");
          OBJ_6.add(NUMDEB);
          NUMDEB.setBounds(30, 35, 70, NUMDEB.getPreferredSize().height);

          //---- SUFDEB ----
          SUFDEB.setComponentPopupMenu(BTD);
          SUFDEB.setName("SUFDEB");
          OBJ_6.add(SUFDEB);
          SUFDEB.setBounds(100, 35, 20, SUFDEB.getPreferredSize().height);
        }

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("R\u00e9capitulatif"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_15 ----
          OBJ_15.setText("@NUM01@");
          OBJ_15.setName("OBJ_15");
          panel1.add(OBJ_15);
          OBJ_15.setBounds(10, 30, 70, 16);

          //---- OBJ_16 ----
          OBJ_16.setText("@NUM02@");
          OBJ_16.setName("OBJ_16");
          panel1.add(OBJ_16);
          OBJ_16.setBounds(10, 50, 70, 16);

          //---- OBJ_17 ----
          OBJ_17.setText("@NUM03@");
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(10, 70, 70, 16);

          //---- OBJ_18 ----
          OBJ_18.setText("@NUM04@");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(10, 90, 70, 16);

          //---- OBJ_19 ----
          OBJ_19.setText("@NUM05@");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(10, 110, 70, 16);

          //---- OBJ_20 ----
          OBJ_20.setText("@NUM06@");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(10, 130, 70, 16);

          //---- OBJ_21 ----
          OBJ_21.setText("@NUM07@");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(10, 150, 70, 16);

          //---- OBJ_22 ----
          OBJ_22.setText("@NUM08@");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(10, 170, 70, 16);

          //---- OBJ_23 ----
          OBJ_23.setText("@NUM09@");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(10, 190, 70, 16);

          //---- OBJ_24 ----
          OBJ_24.setText("@NUM10@");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(10, 210, 70, 16);

          //---- OBJ_25 ----
          OBJ_25.setText("@NUM11@");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(95, 30, 70, 16);

          //---- OBJ_26 ----
          OBJ_26.setText("@NUM12@");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(95, 50, 70, 16);

          //---- OBJ_27 ----
          OBJ_27.setText("@NUM13@");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(95, 70, 70, 16);

          //---- OBJ_28 ----
          OBJ_28.setText("@NUM14@");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(95, 90, 70, 16);

          //---- OBJ_29 ----
          OBJ_29.setText("@NUM15@");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(95, 110, 70, 16);

          //---- OBJ_30 ----
          OBJ_30.setText("@NUM16@");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(95, 130, 70, 16);

          //---- OBJ_31 ----
          OBJ_31.setText("@NUM17@");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(95, 150, 70, 16);

          //---- OBJ_32 ----
          OBJ_32.setText("@NUM18@");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(95, 170, 70, 16);

          //---- OBJ_33 ----
          OBJ_33.setText("@NUM19@");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(95, 190, 70, 16);

          //---- OBJ_34 ----
          OBJ_34.setText("@NUM20@");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(95, 210, 70, 16);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(35, 35, 35)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel OBJ_6;
  private JLabel OBJ_10;
  private XRiTextField NUMDEB;
  private XRiTextField SUFDEB;
  private JPanel panel1;
  private JLabel OBJ_15;
  private JLabel OBJ_16;
  private JLabel OBJ_17;
  private JLabel OBJ_18;
  private JLabel OBJ_19;
  private JLabel OBJ_20;
  private JLabel OBJ_21;
  private JLabel OBJ_22;
  private JLabel OBJ_23;
  private JLabel OBJ_24;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private JLabel OBJ_32;
  private JLabel OBJ_33;
  private JLabel OBJ_34;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
