
package ri.serien.libecranrpg.sgvm.SGVM8BFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM8BFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM8BFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_40 = new JXTitledSeparator();
    OBJ_80 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_76 = new JLabel();
    CNVDEB = new XRiTextField();
    CNVFIN = new XRiTextField();
    GEODEB = new XRiTextField();
    GEOFIN = new XRiTextField();
    WCCX = new XRiTextField();
    CATDEB = new XRiTextField();
    CATFIN = new XRiTextField();
    MAGDEB = new XRiTextField();
    MAGFIN = new XRiTextField();
    MEXDEB = new XRiTextField();
    MEXFIN = new XRiTextField();
    CTRDEB = new XRiTextField();
    CTRFIN = new XRiTextField();
    RCCX = new XRiTextField();
    OBJ_81 = new JPanel();
    panel1 = new JPanel();
    OBJ_46 = new JXTitledSeparator();
    OBJ_49 = new JLabel();
    OBJ_73 = new JLabel();
    CLIDP = new XRiTextField();
    CLIFP = new XRiTextField();
    CLIDS = new XRiTextField();
    CLIFS = new XRiTextField();
    panel2 = new JPanel();
    OBJ_47 = new JXTitledSeparator();
    ART1 = new XRiTextField();
    ART2 = new XRiTextField();
    ART3 = new XRiTextField();
    ART4 = new XRiTextField();
    ART5 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(670, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_40 ----
          OBJ_40.setTitle("S\u00e9lections suppl\u00e9mentaires");
          OBJ_40.setName("OBJ_40");

          //---- OBJ_80 ----
          OBJ_80.setText("Code centrale / rang");
          OBJ_80.setName("OBJ_80");

          //---- OBJ_77 ----
          OBJ_77.setText("Zone g\u00e9ographique");
          OBJ_77.setName("OBJ_77");

          //---- OBJ_75 ----
          OBJ_75.setText("Condition de vente");
          OBJ_75.setName("OBJ_75");

          //---- OBJ_78 ----
          OBJ_78.setText("Mode exp\u00e9dition");
          OBJ_78.setName("OBJ_78");

          //---- OBJ_79 ----
          OBJ_79.setText("Transporteur");
          OBJ_79.setName("OBJ_79");

          //---- OBJ_74 ----
          OBJ_74.setText("Cat\u00e9gorie client");
          OBJ_74.setName("OBJ_74");

          //---- OBJ_76 ----
          OBJ_76.setText("Magasin");
          OBJ_76.setName("OBJ_76");

          //---- CNVDEB ----
          CNVDEB.setComponentPopupMenu(BTD);
          CNVDEB.setName("CNVDEB");

          //---- CNVFIN ----
          CNVFIN.setComponentPopupMenu(BTD);
          CNVFIN.setName("CNVFIN");

          //---- GEODEB ----
          GEODEB.setComponentPopupMenu(BTD);
          GEODEB.setName("GEODEB");

          //---- GEOFIN ----
          GEOFIN.setComponentPopupMenu(BTD);
          GEOFIN.setName("GEOFIN");

          //---- WCCX ----
          WCCX.setComponentPopupMenu(BTD);
          WCCX.setName("WCCX");

          //---- CATDEB ----
          CATDEB.setComponentPopupMenu(BTD);
          CATDEB.setName("CATDEB");

          //---- CATFIN ----
          CATFIN.setComponentPopupMenu(BTD);
          CATFIN.setName("CATFIN");

          //---- MAGDEB ----
          MAGDEB.setComponentPopupMenu(BTD);
          MAGDEB.setName("MAGDEB");

          //---- MAGFIN ----
          MAGFIN.setComponentPopupMenu(BTD);
          MAGFIN.setName("MAGFIN");

          //---- MEXDEB ----
          MEXDEB.setComponentPopupMenu(BTD);
          MEXDEB.setName("MEXDEB");

          //---- MEXFIN ----
          MEXFIN.setComponentPopupMenu(BTD);
          MEXFIN.setName("MEXFIN");

          //---- CTRDEB ----
          CTRDEB.setComponentPopupMenu(BTD);
          CTRDEB.setName("CTRDEB");

          //---- CTRFIN ----
          CTRFIN.setComponentPopupMenu(BTD);
          CTRFIN.setName("CTRFIN");

          //---- RCCX ----
          RCCX.setComponentPopupMenu(BTD);
          RCCX.setName("RCCX");

          //======== OBJ_81 ========
          {
            OBJ_81.setName("OBJ_81");
            OBJ_81.setLayout(null);
          }

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_46 ----
            OBJ_46.setTitle("Clients \u00e0 traiter");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(10, 20, 310, OBJ_46.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("Client de d\u00e9but");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(25, 55, 85, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("Client de fin");
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(25, 85, 70, 20);

            //---- CLIDP ----
            CLIDP.setComponentPopupMenu(BTD);
            CLIDP.setName("CLIDP");
            panel1.add(CLIDP);
            CLIDP.setBounds(180, 51, 60, CLIDP.getPreferredSize().height);

            //---- CLIFP ----
            CLIFP.setComponentPopupMenu(BTD);
            CLIFP.setName("CLIFP");
            panel1.add(CLIFP);
            CLIFP.setBounds(180, 81, 60, CLIFP.getPreferredSize().height);

            //---- CLIDS ----
            CLIDS.setComponentPopupMenu(BTD);
            CLIDS.setName("CLIDS");
            panel1.add(CLIDS);
            CLIDS.setBounds(255, 51, 36, CLIDS.getPreferredSize().height);

            //---- CLIFS ----
            CLIFS.setComponentPopupMenu(BTD);
            CLIFS.setName("CLIFS");
            panel1.add(CLIFS);
            CLIFS.setBounds(255, 81, 36, CLIFS.getPreferredSize().height);
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_47 ----
            OBJ_47.setTitle("Articles fiche observation");
            OBJ_47.setName("OBJ_47");
            panel2.add(OBJ_47);
            OBJ_47.setBounds(10, 10, 275, OBJ_47.getPreferredSize().height);

            //---- ART1 ----
            ART1.setComponentPopupMenu(BTD);
            ART1.setName("ART1");
            panel2.add(ART1);
            ART1.setBounds(25, 41, 114, ART1.getPreferredSize().height);

            //---- ART2 ----
            ART2.setComponentPopupMenu(BTD);
            ART2.setName("ART2");
            panel2.add(ART2);
            ART2.setBounds(25, 70, 114, ART2.getPreferredSize().height);

            //---- ART3 ----
            ART3.setComponentPopupMenu(BTD);
            ART3.setName("ART3");
            panel2.add(ART3);
            ART3.setBounds(25, 100, 114, ART3.getPreferredSize().height);

            //---- ART4 ----
            ART4.setComponentPopupMenu(BTD);
            ART4.setName("ART4");
            panel2.add(ART4);
            ART4.setBounds(140, 41, 114, ART4.getPreferredSize().height);

            //---- ART5 ----
            ART5.setComponentPopupMenu(BTD);
            ART5.setName("ART5");
            panel2.add(ART5);
            ART5.setBounds(140, 70, 114, ART5.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(CATDEB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CNVDEB, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MAGDEB, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addComponent(GEODEB, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(CATFIN, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CNVFIN, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MAGFIN, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addComponent(GEOFIN, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(MEXDEB, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
                    .addGap(17, 17, 17)
                    .addComponent(CTRDEB, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(115, 115, 115)
                    .addComponent(WCCX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(MEXFIN, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CTRFIN, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addComponent(RCCX, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(CATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(CNVDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(MAGDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(GEODEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(CATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(CNVFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(MAGFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(GEOFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(MEXDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(CTRDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WCCX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(MEXFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(CTRFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(RCCX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_40;
  private JLabel OBJ_80;
  private JLabel OBJ_77;
  private JLabel OBJ_75;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private JLabel OBJ_74;
  private JLabel OBJ_76;
  private XRiTextField CNVDEB;
  private XRiTextField CNVFIN;
  private XRiTextField GEODEB;
  private XRiTextField GEOFIN;
  private XRiTextField WCCX;
  private XRiTextField CATDEB;
  private XRiTextField CATFIN;
  private XRiTextField MAGDEB;
  private XRiTextField MAGFIN;
  private XRiTextField MEXDEB;
  private XRiTextField MEXFIN;
  private XRiTextField CTRDEB;
  private XRiTextField CTRFIN;
  private XRiTextField RCCX;
  private JPanel OBJ_81;
  private JPanel panel1;
  private JXTitledSeparator OBJ_46;
  private JLabel OBJ_49;
  private JLabel OBJ_73;
  private XRiTextField CLIDP;
  private XRiTextField CLIFP;
  private XRiTextField CLIDS;
  private XRiTextField CLIFS;
  private JPanel panel2;
  private JXTitledSeparator OBJ_47;
  private XRiTextField ART1;
  private XRiTextField ART2;
  private XRiTextField ART3;
  private XRiTextField ART4;
  private XRiTextField ART5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
