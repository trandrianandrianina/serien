/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM87FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class SGVM87FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM87FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Ajout
    initDiverses();
    UTRI1.setValeurs("1", "RB");
    UTRI2.setValeurs("2", "RB");
    UTRI3.setValeurs("3", "RB");
    ARRMAC.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Specifique
    // +++++++++++++++++++++++++++++++++++++
    // Disponibilité des composants
    DEPDIM.setEnabled(lexique.isPresent("DEPDIM"));
    DEPDIH.setEnabled(lexique.isPresent("DEPDIH"));
    
    // Visibilité des composants
    lbHeureDeTraitement.setVisible(lexique.isPresent("DEPDIH"));
    lbHeures.setVisible(lexique.isPresent("DEPDIH"));
    lbMinutes.setVisible(lexique.isPresent("DEPDIH"));
    pnlInformationDiffere
        .setVisible(ARRMAC.isVisible() || lbHeureDeTraitement.isVisible() || lbHeures.isVisible() || lbMinutes.isVisible());
    pnlTriBonPreparation.setVisible(UTRI1.isVisible() || UTRI2.isVisible() || UTRI3.isVisible());
    
    // Titre
    setTitle("Compléments d'informations");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlTriBonPreparation = new SNPanelTitre();
    UTRI1 = new XRiRadioButton();
    UTRI2 = new XRiRadioButton();
    UTRI3 = new XRiRadioButton();
    pnlInformationDiffere = new SNPanelTitre();
    lbHeureDeTraitement = new SNLabelChamp();
    DEPDIH = new XRiTextField();
    lbHeures = new SNLabelUnite();
    DEPDIM = new XRiTextField();
    lbMinutes = new SNLabelUnite();
    ARRMAC = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(425, 350));
    setPreferredSize(new Dimension(425, 350));
    setMaximumSize(new Dimension(425, 350));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //======== pnlTriBonPreparation ========
      {
        pnlTriBonPreparation.setTitre("Tri des bons de pr\u00e9paration");
        pnlTriBonPreparation.setName("pnlTriBonPreparation");
        pnlTriBonPreparation.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlTriBonPreparation.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlTriBonPreparation.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlTriBonPreparation.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlTriBonPreparation.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- UTRI1 ----
        UTRI1.setText("Par num\u00e9ro de client");
        UTRI1.setComponentPopupMenu(null);
        UTRI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        UTRI1.setFont(new Font("sansserif", Font.PLAIN, 14));
        UTRI1.setMinimumSize(new Dimension(200, 30));
        UTRI1.setMaximumSize(new Dimension(200, 30));
        UTRI1.setPreferredSize(new Dimension(200, 30));
        UTRI1.setName("UTRI1");
        pnlTriBonPreparation.add(UTRI1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- UTRI2 ----
        UTRI2.setText("Par date de commande");
        UTRI2.setComponentPopupMenu(null);
        UTRI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        UTRI2.setFont(new Font("sansserif", Font.PLAIN, 14));
        UTRI2.setMinimumSize(new Dimension(200, 30));
        UTRI2.setMaximumSize(new Dimension(200, 30));
        UTRI2.setPreferredSize(new Dimension(200, 30));
        UTRI2.setName("UTRI2");
        pnlTriBonPreparation.add(UTRI2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- UTRI3 ----
        UTRI3.setText("Par montant d\u00e9croissant liv.");
        UTRI3.setComponentPopupMenu(null);
        UTRI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        UTRI3.setFont(new Font("sansserif", Font.PLAIN, 14));
        UTRI3.setMinimumSize(new Dimension(200, 30));
        UTRI3.setPreferredSize(new Dimension(200, 30));
        UTRI3.setMaximumSize(new Dimension(200, 30));
        UTRI3.setName("UTRI3");
        pnlTriBonPreparation.add(UTRI3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlTriBonPreparation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));

      //======== pnlInformationDiffere ========
      {
        pnlInformationDiffere.setTitre("Informations traitement diff\u00e9r\u00e9");
        pnlInformationDiffere.setName("pnlInformationDiffere");
        pnlInformationDiffere.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlInformationDiffere.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlInformationDiffere.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlInformationDiffere.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlInformationDiffere.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbHeureDeTraitement ----
        lbHeureDeTraitement.setText("Heure de traitement");
        lbHeureDeTraitement.setComponentPopupMenu(null);
        lbHeureDeTraitement.setName("lbHeureDeTraitement");
        pnlInformationDiffere.add(lbHeureDeTraitement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DEPDIH ----
        DEPDIH.setComponentPopupMenu(null);
        DEPDIH.setFont(new Font("sansserif", Font.PLAIN, 14));
        DEPDIH.setPreferredSize(new Dimension(30, 30));
        DEPDIH.setMinimumSize(new Dimension(30, 30));
        DEPDIH.setMaximumSize(new Dimension(30, 30));
        DEPDIH.setName("DEPDIH");
        pnlInformationDiffere.add(DEPDIH, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbHeures ----
        lbHeures.setText("heures");
        lbHeures.setPreferredSize(new Dimension(44, 30));
        lbHeures.setMaximumSize(new Dimension(44, 30));
        lbHeures.setMinimumSize(new Dimension(44, 30));
        lbHeures.setName("lbHeures");
        pnlInformationDiffere.add(lbHeures, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DEPDIM ----
        DEPDIM.setComponentPopupMenu(null);
        DEPDIM.setFont(new Font("sansserif", Font.PLAIN, 14));
        DEPDIM.setPreferredSize(new Dimension(30, 30));
        DEPDIM.setMinimumSize(new Dimension(30, 30));
        DEPDIM.setMaximumSize(new Dimension(30, 30));
        DEPDIM.setName("DEPDIM");
        pnlInformationDiffere.add(DEPDIM, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbMinutes ----
        lbMinutes.setText("Minutes");
        lbMinutes.setPreferredSize(new Dimension(50, 30));
        lbMinutes.setMinimumSize(new Dimension(50, 30));
        lbMinutes.setMaximumSize(new Dimension(50, 30));
        lbMinutes.setName("lbMinutes");
        pnlInformationDiffere.add(lbMinutes, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- ARRMAC ----
        ARRMAC.setText("Arr\u00eat de la machine");
        ARRMAC.setComponentPopupMenu(null);
        ARRMAC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ARRMAC.setMaximumSize(new Dimension(128, 30));
        ARRMAC.setMinimumSize(new Dimension(128, 30));
        ARRMAC.setPreferredSize(new Dimension(128, 30));
        ARRMAC.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARRMAC.setName("ARRMAC");
        pnlInformationDiffere.add(ARRMAC, new GridBagConstraints(0, 1, 5, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformationDiffere, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(UTRI1);
    RB_GRP.add(UTRI2);
    RB_GRP.add(UTRI3);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlTriBonPreparation;
  private XRiRadioButton UTRI1;
  private XRiRadioButton UTRI2;
  private XRiRadioButton UTRI3;
  private SNPanelTitre pnlInformationDiffere;
  private SNLabelChamp lbHeureDeTraitement;
  private XRiTextField DEPDIH;
  private SNLabelUnite lbHeures;
  private XRiTextField DEPDIM;
  private SNLabelUnite lbMinutes;
  private XRiCheckBox ARRMAC;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
