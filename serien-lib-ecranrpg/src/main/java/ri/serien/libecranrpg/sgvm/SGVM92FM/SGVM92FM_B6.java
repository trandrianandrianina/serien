
package ri.serien.libecranrpg.sgvm.SGVM92FM;
// Nom Fichier: b_SGVM92FM_FMTB6_FMTF1_566.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGVM92FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _table1_Title = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", };
  private String[][] _table1_Data =
      { { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, },
          { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, }, };
  private int[] _table1_Width = { 30, 60, 15, 30, 60, 15, 30, 60, 15, 30, 60, 15, 30, 60, 15, 30, 60, 15, 30, 60, };
  
  public SGVM92FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    table1.setModel(new DefaultTableModel(new Object[][] {
        { lexique.HostFieldGetData("CTS001"), lexique.HostFieldGetData("CTM001"), null, lexique.HostFieldGetData("CTS002"),
            lexique.HostFieldGetData("CTM002"), null, lexique.HostFieldGetData("CTS003"), lexique.HostFieldGetData("CTM003"), null,
            lexique.HostFieldGetData("CTS004"), lexique.HostFieldGetData("CTM004"), null, lexique.HostFieldGetData("CTS005"),
            lexique.HostFieldGetData("CTM005"), null, lexique.HostFieldGetData("CTS006"), lexique.HostFieldGetData("CTM006"), null,
            lexique.HostFieldGetData("CTS007"), lexique.HostFieldGetData("CTM007") },
        { lexique.HostFieldGetData("CTS008"), lexique.HostFieldGetData("CTM008"), null, lexique.HostFieldGetData("CTS009"),
            lexique.HostFieldGetData("CTM009"), null, lexique.HostFieldGetData("CTS010"), lexique.HostFieldGetData("CTM010"), null,
            lexique.HostFieldGetData("CTS011"), lexique.HostFieldGetData("CTM011"), null, lexique.HostFieldGetData("CTS012"),
            lexique.HostFieldGetData("CTM012"), null, lexique.HostFieldGetData("CTS013"), lexique.HostFieldGetData("CTM013"), null,
            lexique.HostFieldGetData("CTS014"), lexique.HostFieldGetData("CTM014") },
        { lexique.HostFieldGetData("CTS015"), lexique.HostFieldGetData("CTM015"), null, lexique.HostFieldGetData("CTS016"),
            lexique.HostFieldGetData("CTM016"), null, lexique.HostFieldGetData("CTS017"), lexique.HostFieldGetData("CTM017"), null,
            lexique.HostFieldGetData("CTS018"), lexique.HostFieldGetData("CTM018"), null, lexique.HostFieldGetData("CTS019"),
            lexique.HostFieldGetData("CTM019"), null, lexique.HostFieldGetData("CTS020"), lexique.HostFieldGetData("CTM020"), null,
            lexique.HostFieldGetData("CTS021"), lexique.HostFieldGetData("CTM021") },
        { lexique.HostFieldGetData("CTS022"), lexique.HostFieldGetData("CTM022"), null, lexique.HostFieldGetData("CTS023"),
            lexique.HostFieldGetData("CTM023"), null, lexique.HostFieldGetData("CTS024"), lexique.HostFieldGetData("CTM024"), null,
            lexique.HostFieldGetData("CTS025"), lexique.HostFieldGetData("CTM025"), null, lexique.HostFieldGetData("CTS026"),
            lexique.HostFieldGetData("CTM026"), null, lexique.HostFieldGetData("CTS027"), lexique.HostFieldGetData("CTM027"), null,
            lexique.HostFieldGetData("CTS028"), lexique.HostFieldGetData("CTM028") },
        { lexique.HostFieldGetData("CTS029"), lexique.HostFieldGetData("CTM029"), null, lexique.HostFieldGetData("CTS030"),
            lexique.HostFieldGetData("CTM030"), null, lexique.HostFieldGetData("CTS031"), lexique.HostFieldGetData("CTM031"), null,
            lexique.HostFieldGetData("CTS032"), lexique.HostFieldGetData("CTM032"), null, lexique.HostFieldGetData("CTS033"),
            lexique.HostFieldGetData("CTM033"), null, lexique.HostFieldGetData("CTS034"), lexique.HostFieldGetData("CTM034"), null,
            lexique.HostFieldGetData("CTS035"), lexique.HostFieldGetData("CTM035") },
        { lexique.HostFieldGetData("CTS036"), lexique.HostFieldGetData("CTM036"), null, lexique.HostFieldGetData("CTS037"),
            lexique.HostFieldGetData("CTM037"), null, lexique.HostFieldGetData("CTS038"), lexique.HostFieldGetData("CTM038"), null,
            lexique.HostFieldGetData("CTS039"), lexique.HostFieldGetData("CTM039"), null, lexique.HostFieldGetData("CTS040"),
            lexique.HostFieldGetData("CTM040"), null, lexique.HostFieldGetData("CTS041"), lexique.HostFieldGetData("CTM041"), null,
            lexique.HostFieldGetData("CTS042"), lexique.HostFieldGetData("CTM042") },
        { lexique.HostFieldGetData("CTS043"), lexique.HostFieldGetData("CTM043"), null, lexique.HostFieldGetData("CTS044"),
            lexique.HostFieldGetData("CTM044"), null, lexique.HostFieldGetData("CTS045"), lexique.HostFieldGetData("CTM045"), null,
            lexique.HostFieldGetData("CTS046"), lexique.HostFieldGetData("CTM046"), null, lexique.HostFieldGetData("CTS047"),
            lexique.HostFieldGetData("CTM047"), null, lexique.HostFieldGetData("CTS048"), lexique.HostFieldGetData("CTM048"), null,
            lexique.HostFieldGetData("CTS049"), lexique.HostFieldGetData("CTM049") },
        { lexique.HostFieldGetData("CTS050"), lexique.HostFieldGetData("CTM050"), null, lexique.HostFieldGetData("CTS051"),
            lexique.HostFieldGetData("CTM051"), null, lexique.HostFieldGetData("CTS052"), lexique.HostFieldGetData("CTM052"), null,
            lexique.HostFieldGetData("CTS053"), lexique.HostFieldGetData("CTM053"), null, lexique.HostFieldGetData("CTS054"),
            lexique.HostFieldGetData("CTM054"), null, lexique.HostFieldGetData("CTS055"), lexique.HostFieldGetData("CTM055"), null,
            lexique.HostFieldGetData("CTS056"), lexique.HostFieldGetData("CTM056") },
        { lexique.HostFieldGetData("CTS057"), lexique.HostFieldGetData("CTM057"), null, lexique.HostFieldGetData("CTS058"),
            lexique.HostFieldGetData("CTM058"), null, lexique.HostFieldGetData("CTS059"), lexique.HostFieldGetData("CTM059"), null,
            lexique.HostFieldGetData("CTS060"), lexique.HostFieldGetData("CTM060"), null, lexique.HostFieldGetData("CTS061"),
            lexique.HostFieldGetData("CTM061"), null, lexique.HostFieldGetData("CTS062"), lexique.HostFieldGetData("CTM062"), null,
            lexique.HostFieldGetData("CTS063"), lexique.HostFieldGetData("CTM063") },
        { lexique.HostFieldGetData("CTS064"), lexique.HostFieldGetData("CTM064"), null, lexique.HostFieldGetData("CTS065"),
            lexique.HostFieldGetData("CTM065"), null, lexique.HostFieldGetData("CTS066"), lexique.HostFieldGetData("CTM066"), null,
            lexique.HostFieldGetData("CTS067"), lexique.HostFieldGetData("CTM067"), null, lexique.HostFieldGetData("CTS068"),
            lexique.HostFieldGetData("CTM068"), null, lexique.HostFieldGetData("CTS069"), lexique.HostFieldGetData("CTM069"), null,
            lexique.HostFieldGetData("CTS070"), lexique.HostFieldGetData("CTM070") },
        { lexique.HostFieldGetData("CTS071"), lexique.HostFieldGetData("CTM071"), null, lexique.HostFieldGetData("CTS072"),
            lexique.HostFieldGetData("CTM072"), null, lexique.HostFieldGetData("CTS073"), lexique.HostFieldGetData("CTM073"), null,
            lexique.HostFieldGetData("CTS074"), lexique.HostFieldGetData("CTM074"), null, lexique.HostFieldGetData("CTS075"),
            lexique.HostFieldGetData("CTM075"), null, lexique.HostFieldGetData("CTS076"), lexique.HostFieldGetData("CTM076"), null,
            lexique.HostFieldGetData("CTS077"), lexique.HostFieldGetData("CTM077") },
        { lexique.HostFieldGetData("CTS078"), lexique.HostFieldGetData("CTM078"), null, lexique.HostFieldGetData("CTS079"),
            lexique.HostFieldGetData("CTM079"), null, lexique.HostFieldGetData("CTS080"), lexique.HostFieldGetData("CTM080"), null,
            lexique.HostFieldGetData("CTS081"), lexique.HostFieldGetData("CTM081"), null, lexique.HostFieldGetData("CTS082"),
            lexique.HostFieldGetData("CTM082"), null, lexique.HostFieldGetData("CTS083"), lexique.HostFieldGetData("CTM083"), null,
            lexique.HostFieldGetData("CTS084"), lexique.HostFieldGetData("CTM084") },
        { lexique.HostFieldGetData("CTS085"), lexique.HostFieldGetData("CTM085"), null, lexique.HostFieldGetData("CTS086"),
            lexique.HostFieldGetData("CTM086"), null, lexique.HostFieldGetData("CTS087"), lexique.HostFieldGetData("CTM087"), null,
            lexique.HostFieldGetData("CTS088"), lexique.HostFieldGetData("CTM088"), null, lexique.HostFieldGetData("CTS089"),
            lexique.HostFieldGetData("CTM089"), null, lexique.HostFieldGetData("CTS090"), lexique.HostFieldGetData("CTM090"), null,
            lexique.HostFieldGetData("CTS091"), lexique.HostFieldGetData("CTM091") },
        { lexique.HostFieldGetData("CTS092"), lexique.HostFieldGetData("CTM092"), null, lexique.HostFieldGetData("CTS093"),
            lexique.HostFieldGetData("CTM093"), null, lexique.HostFieldGetData("CTS094"), lexique.HostFieldGetData("CTM094"), null,
            lexique.HostFieldGetData("CTS095"), lexique.HostFieldGetData("CTM095"), null, lexique.HostFieldGetData("CTS096"),
            lexique.HostFieldGetData("CTM096"), null, lexique.HostFieldGetData("CTS097"), lexique.HostFieldGetData("CTM097"), null,
            lexique.HostFieldGetData("CTS098"), lexique.HostFieldGetData("CTM098") },
        { lexique.HostFieldGetData("CTS099"), lexique.HostFieldGetData("CTM099"), null, lexique.HostFieldGetData("CTS100"),
            lexique.HostFieldGetData("CTM100"), null, lexique.HostFieldGetData("CTS101"), lexique.HostFieldGetData("CTM101"), null,
            lexique.HostFieldGetData("CTS102"), lexique.HostFieldGetData("CTM102"), null, lexique.HostFieldGetData("CTS103"),
            lexique.HostFieldGetData("CTM103"), null, lexique.HostFieldGetData("CTS104"), lexique.HostFieldGetData("CTM104"), null,
            lexique.HostFieldGetData("CTS105"), lexique.HostFieldGetData("CTM105") },
        { lexique.HostFieldGetData("CTS106"), lexique.HostFieldGetData("CTM106"), null, lexique.HostFieldGetData("CTS107"),
            lexique.HostFieldGetData("CTM107"), null, lexique.HostFieldGetData("CTS108"), lexique.HostFieldGetData("CTM108"), null,
            lexique.HostFieldGetData("CTS109"), lexique.HostFieldGetData("CTM109"), null, lexique.HostFieldGetData("CTS110"),
            lexique.HostFieldGetData("CTM110"), null, lexique.HostFieldGetData("CTS111"), lexique.HostFieldGetData("CTM111"), null,
            lexique.HostFieldGetData("CTS112"), lexique.HostFieldGetData("CTM112") }
    
    }, new String[] { "Etb", "Purge", null, "Etb", "Purge", null, "Etb", "Purge", null, "Etb", "Purge", null, "Etb", "Purge", null, "Etb",
        "Purge", null, "Etb", "Purge" }) {
      boolean[] columnEditable = new boolean[] { false, false, false, false, false, false, false, false, false, false, false, false, false,
          false, false, false, false, false, false, false };
      
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnEditable[columnIndex];
      }
    });
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    // xH_Titre.setIcon(ManagerSessionClient.getInstance().getLogoImage(#ETB#.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvm92"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    scrollPane1 = new JScrollPane();
    table1 = new XRiTable();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    SCROLLPANE_LIST4 = new JScrollPane();
    LIST4 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_61 = new JLabel();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap(881, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addComponent(bt_Fonctions)
              .addContainerGap(3, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("R\u00e9capitulatif des Etablissements \u00e0 purger sur la p\u00e9riode"));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");

          //---- table1 ----
          table1.setName("table1");
          scrollPane1.setViewportView(table1);
        }
        panel1.add(scrollPane1);
        scrollPane1.setBounds(20, 30, 715, 290);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 755, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(30, 30, 30)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== SCROLLPANE_LIST4 ========
    {
      SCROLLPANE_LIST4.setComponentPopupMenu(BTD);
      SCROLLPANE_LIST4.setName("SCROLLPANE_LIST4");

      //---- LIST4 ----
      LIST4.setModel(new DefaultTableModel(
        new Object[][] {
          {"@CTS001@", "@CTM001@", "@null@", "@CTS001@", "@CTM001@", "@null@", "@CTS001@", "@CTM001@", "@null@", "@CTS001@", "@CTM001@", "@null@", "@CTS001@", "@CTM001@", "@null@", "@CTS001@", "@CTM001@", "@null@", "@CTS001@", "@CTM001@"},
          {"@CTS002@", "@CTM002@", "@null@", "@CTS002@", "@CTM002@", "@null@", "@CTS002@", "@CTM002@", "@null@", "@CTS002@", "@CTM002@", "@null@", "@CTS002@", "@CTM002@", "@null@", "@CTS002@", "@CTM002@", "@null@", "@CTS002@", "@CTM002@"},
          {"@CTS003@", "@CTM003@", "@null@", "@CTS003@", "@CTM003@", "@null@", "@CTS003@", "@CTM003@", "@null@", "@CTS003@", "@CTM003@", "@null@", "@CTS003@", "@CTM003@", "@null@", "@CTS003@", "@CTM003@", "@null@", "@CTS003@", "@CTM003@"},
          {"@CTS004@", "@CTM004@", "@null@", "@CTS004@", "@CTM004@", "@null@", "@CTS004@", "@CTM004@", "@null@", "@CTS004@", "@CTM004@", "@null@", "@CTS004@", "@CTM004@", "@null@", "@CTS004@", "@CTM004@", "@null@", "@CTS004@", "@CTM004@"},
          {"@CTS005@", "@CTM005@", "@null@", "@CTS005@", "@CTM005@", "@null@", "@CTS005@", "@CTM005@", "@null@", "@CTS005@", "@CTM005@", "@null@", "@CTS005@", "@CTM005@", "@null@", "@CTS005@", "@CTM005@", "@null@", "@CTS005@", "@CTM005@"},
          {"@CTS006@", "@CTM006@", "@null@", "@CTS006@", "@CTM006@", "@null@", "@CTS006@", "@CTM006@", "@null@", "@CTS006@", "@CTM006@", "@null@", "@CTS006@", "@CTM006@", "@null@", "@CTS006@", "@CTM006@", "@null@", "@CTS006@", "@CTM006@"},
          {"@CTS007@", "@CTM007@", "@null@", "@CTS007@", "@CTM007@", "@null@", "@CTS007@", "@CTM007@", "@null@", "@CTS007@", "@CTM007@", "@null@", "@CTS007@", "@CTM007@", "@null@", "@CTS007@", "@CTM007@", "@null@", "@CTS007@", "@CTM007@"},
          {"@CTS008@", "@CTM008@", "@null@", "@CTS008@", "@CTM008@", "@null@", "@CTS008@", "@CTM008@", "@null@", "@CTS008@", "@CTM008@", "@null@", "@CTS008@", "@CTM008@", "@null@", "@CTS008@", "@CTM008@", "@null@", "@CTS008@", "@CTM008@"},
          {"@CTS009@", "@CTM009@", "@null@", "@CTS009@", "@CTM009@", "@null@", "@CTS009@", "@CTM009@", "@null@", "@CTS009@", "@CTM009@", "@null@", "@CTS009@", "@CTM009@", "@null@", "@CTS009@", "@CTM009@", "@null@", "@CTS009@", "@CTM009@"},
          {"@CTS010@", "@CTM010@", "@null@", "@CTS010@", "@CTM010@", "@null@", "@CTS010@", "@CTM010@", "@null@", "@CTS010@", "@CTM010@", "@null@", "@CTS010@", "@CTM010@", "@null@", "@CTS010@", "@CTM010@", "@null@", "@CTS010@", "@CTM010@"},
          {"@CTS011@", "@CTM011@", "@null@", "@CTS011@", "@CTM011@", "@null@", "@CTS011@", "@CTM011@", "@null@", "@CTS011@", "@CTM011@", "@null@", "@CTS011@", "@CTM011@", "@null@", "@CTS011@", "@CTM011@", "@null@", "@CTS011@", "@CTM011@"},
          {"@CTS012@", "@CTM012@", "@null@", "@CTS012@", "@CTM012@", "@null@", "@CTS012@", "@CTM012@", "@null@", "@CTS012@", "@CTM012@", "@null@", "@CTS012@", "@CTM012@", "@null@", "@CTS012@", "@CTM012@", "@null@", "@CTS012@", "@CTM012@"},
          {"@CTS013@", "@CTM013@", "@null@", "@CTS013@", "@CTM013@", "@null@", "@CTS013@", "@CTM013@", "@null@", "@CTS013@", "@CTM013@", "@null@", "@CTS013@", "@CTM013@", "@null@", "@CTS013@", "@CTM013@", "@null@", "@CTS013@", "@CTM013@"},
          {"@CTS014@", "@CTM014@", "@null@", "@CTS014@", "@CTM014@", "@null@", "@CTS014@", "@CTM014@", "@null@", "@CTS014@", "@CTM014@", "@null@", "@CTS014@", "@CTM014@", "@null@", "@CTS014@", "@CTM014@", "@null@", "@CTS014@", "@CTM014@"},
          {"@CTS015@", "@CTM015@", "@null@", "@CTS015@", "@CTM015@", "@null@", "@CTS015@", "@CTM015@", "@null@", "@CTS015@", "@CTM015@", "@null@", "@CTS015@", "@CTM015@", "@null@", "@CTS015@", "@CTM015@", "@null@", "@CTS015@", "@CTM015@"},
          {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
        },
        new String[] {
          "Etb", "Purge", "", "Etb", "Purge", "", "Etb", "Purge", "", "Etb", "Purge", "", "Etb", "Purge", "", "Etb", "Purge", "", "Etb", "Purge"
        }
      ) {
        boolean[] columnEditable = new boolean[] {
          false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
        };
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
          return columnEditable[columnIndex];
        }
      });
      {
        TableColumnModel cm = LIST4.getColumnModel();
        cm.getColumn(0).setPreferredWidth(33);
        cm.getColumn(1).setPreferredWidth(42);
        cm.getColumn(2).setPreferredWidth(3);
        cm.getColumn(3).setPreferredWidth(33);
        cm.getColumn(4).setPreferredWidth(42);
        cm.getColumn(5).setPreferredWidth(3);
        cm.getColumn(6).setPreferredWidth(33);
        cm.getColumn(7).setPreferredWidth(42);
        cm.getColumn(8).setPreferredWidth(3);
        cm.getColumn(9).setPreferredWidth(33);
        cm.getColumn(10).setPreferredWidth(42);
        cm.getColumn(11).setPreferredWidth(3);
        cm.getColumn(12).setPreferredWidth(33);
        cm.getColumn(13).setPreferredWidth(42);
        cm.getColumn(14).setPreferredWidth(3);
        cm.getColumn(15).setPreferredWidth(33);
        cm.getColumn(16).setPreferredWidth(42);
        cm.getColumn(17).setPreferredWidth(3);
        cm.getColumn(18).setPreferredWidth(33);
        cm.getColumn(19).setPreferredWidth(42);
      }
      LIST4.setName("LIST4");
      SCROLLPANE_LIST4.setViewportView(LIST4);
    }

    //---- BT_PGUP ----
    BT_PGUP.setText("");
    BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
    BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGUP.setName("BT_PGUP");
    BT_PGUP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_PGUPActionPerformed(e);
      }
    });

    //---- BT_PGDOWN ----
    BT_PGDOWN.setText("");
    BT_PGDOWN.setToolTipText("Page suivante");
    BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGDOWN.setName("BT_PGDOWN");
    BT_PGDOWN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_PGDOWNActionPerformed(e);
      }
    });

    //---- OBJ_61 ----
    OBJ_61.setText("Soci\u00e9t\u00e9 (s) S\u00e9lectionn\u00e9e (s)");
    OBJ_61.setName("OBJ_61");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private JPanel panel1;
  private JScrollPane scrollPane1;
  private XRiTable table1;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JScrollPane SCROLLPANE_LIST4;
  private XRiTable LIST4;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_61;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
