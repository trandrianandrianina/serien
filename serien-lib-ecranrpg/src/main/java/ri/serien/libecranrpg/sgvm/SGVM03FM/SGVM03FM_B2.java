
package ri.serien.libecranrpg.sgvm.SGVM03FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.client.EnumCodeAttentionClient;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM1712] Gestion des ventes -> Fiches permanentes -> Editions clients -> Listes -> Par numéro
 * Indicateur : 01000001
 * Titre : Edition du fichier des clients par numéro
 */
public class SGVM03FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  private Message LOCTP = null;
  
  public SGVM03FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Ajout d'élements dans la combobox
    if (cbSelectionOptions.getItemCount() == 0) {
      cbSelectionOptions.removeAllItems();
      cbSelectionOptions.addItem("Tous");
      cbSelectionOptions.setSelectedItem(lexique.HostFieldGetData("WTNS"));
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_CLIENT_ACTIF);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_ZONE_EN_ROUGE);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_CLIENT_INTERDIT);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_LIVRAISON_INTERDITE);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_ATTENTE_DEPASSEMENT);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_PAIEMENT_A_LA_COMMANDE);
      cbSelectionOptions.addItem(EnumCodeAttentionClient.ATTENTION_CLIENT_DESACTIVE);
    }
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Initialisation des composants
    chargerComposantMagasin();
    chargerComposantDevis();
    chargerComposantClient();
  }
  
  @Override
  public void getData() {
    super.getData();
    recupererValeurCombobox();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snClientDebut.renseignerChampRPG(lexique, "DEBLOG");
    snClientFin.renseignerChampRPG(lexique, "FINLOG");
    snDevise.renseignerChampRPG(lexique, "WDEV");
    if (snClientDebut.getIdSelection() == null && snClientFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
    }
  }
  
  /**
   * Charge la liste des magasin suivant l'etablissemenet
   */
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Charge la liste des clients suivant l'etablissemenet
   */
  private void chargerComposantClient() {
    snClientDebut.setSession(getSession());
    snClientDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snClientDebut.charger(true);
    snClientDebut.setSelectionParChampRPG(lexique, "DEBLOG");
    
    snClientFin.setSession(getSession());
    snClientFin.setIdEtablissement(snEtablissement.getIdSelection());
    snClientFin.charger(true);
    snClientFin.setSelectionParChampRPG(lexique, "FINLOG");
  }
  
  /**
   * Charge la liste des devis suivant l'etablissemenet
   */
  private void chargerComposantDevis() {
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(snEtablissement.getIdSelection());
    snDevise.setTousAutorise(true);
    snDevise.charger(true);
    snDevise.setSelectionParChampRPG(lexique, "WDEV");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Permet de récuperer quelle est l'option séléctionner dans le combobox
   */
  private void recupererValeurCombobox() {
    if (cbSelectionOptions.getSelectedItem().equals(null)) {
      return;
    }
    else {
      if (cbSelectionOptions.getSelectedItem() instanceof String) {
        lexique.HostFieldPutData("WTOU2", 0, "**");
      }
      else {
        switch ((EnumCodeAttentionClient) cbSelectionOptions.getSelectedItem()) {
          case ATTENTION_CLIENT_ACTIF:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_CLIENT_ACTIF.getCode().toString());
            break;
          case ATTENTION_ZONE_EN_ROUGE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_ZONE_EN_ROUGE.getCode().toString());
            break;
          case ATTENTION_CLIENT_INTERDIT:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_CLIENT_INTERDIT.getCode().toString());
            break;
          case ATTENTION_LIVRAISON_INTERDITE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_LIVRAISON_INTERDITE.getCode().toString());
            break;
          case ATTENTION_ATTENTE_DEPASSEMENT:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_ATTENTE_DEPASSEMENT.getCode().toString());
            break;
          case ATTENTION_PAIEMENT_A_LA_COMMANDE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_PAIEMENT_A_LA_COMMANDE.getCode().toString());
            break;
          case ATTENTION_CLIENT_DESACTIVE:
            lexique.HostFieldPutData("WTNS", 0, EnumCodeAttentionClient.ATTENTION_CLIENT_DESACTIVE.getCode().toString());
            break;
          default:
            break;
        }
      }
    }
  }
  
  private void btRechercheActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantMagasin();
      chargerComposantClient();
      chargerComposantDevis();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbNumeroClientDebut = new SNLabelChamp();
    snClientDebut = new SNClientPrincipal();
    lbNumeroClientFin = new SNLabelChamp();
    snClientFin = new SNClientPrincipal();
    lbTopAttention = new SNLabelChamp();
    cbSelectionOptions = new SNComboBox();
    pnlDevis = new SNLabelChamp();
    snDevise = new SNDevise();
    lbCodeMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition du fichier des clients par num\u00e9ro");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.LEADING);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroClientDebut ----
            lbNumeroClientDebut.setText("Num\u00e9ro client de d\u00e9but");
            lbNumeroClientDebut.setMinimumSize(new Dimension(155, 30));
            lbNumeroClientDebut.setMaximumSize(new Dimension(155, 30));
            lbNumeroClientDebut.setPreferredSize(new Dimension(155, 30));
            lbNumeroClientDebut.setName("lbNumeroClientDebut");
            pnlCritereSelection.add(lbNumeroClientDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClientDebut ----
            snClientDebut.setMinimumSize(new Dimension(300, 30));
            snClientDebut.setMaximumSize(new Dimension(300, 30));
            snClientDebut.setPreferredSize(new Dimension(300, 30));
            snClientDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snClientDebut.setName("snClientDebut");
            pnlCritereSelection.add(snClientDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNumeroClientFin ----
            lbNumeroClientFin.setText("Num\u00e9ro client de fin");
            lbNumeroClientFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbNumeroClientFin.setMinimumSize(new Dimension(155, 30));
            lbNumeroClientFin.setPreferredSize(new Dimension(155, 30));
            lbNumeroClientFin.setMaximumSize(new Dimension(155, 30));
            lbNumeroClientFin.setName("lbNumeroClientFin");
            pnlCritereSelection.add(lbNumeroClientFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClientFin ----
            snClientFin.setMinimumSize(new Dimension(300, 30));
            snClientFin.setMaximumSize(new Dimension(300, 30));
            snClientFin.setPreferredSize(new Dimension(300, 30));
            snClientFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snClientFin.setName("snClientFin");
            pnlCritereSelection.add(snClientFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTopAttention ----
            lbTopAttention.setText("Top attention");
            lbTopAttention.setMinimumSize(new Dimension(155, 30));
            lbTopAttention.setMaximumSize(new Dimension(155, 30));
            lbTopAttention.setPreferredSize(new Dimension(155, 30));
            lbTopAttention.setName("lbTopAttention");
            pnlCritereSelection.add(lbTopAttention, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbSelectionOptions ----
            cbSelectionOptions.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbSelectionOptions.setPreferredSize(new Dimension(260, 30));
            cbSelectionOptions.setMinimumSize(new Dimension(260, 30));
            cbSelectionOptions.setName("cbSelectionOptions");
            pnlCritereSelection.add(cbSelectionOptions, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- pnlDevis ----
            pnlDevis.setText("Devise");
            pnlDevis.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlDevis.setMinimumSize(new Dimension(155, 30));
            pnlDevis.setMaximumSize(new Dimension(155, 30));
            pnlDevis.setPreferredSize(new Dimension(155, 30));
            pnlDevis.setName("pnlDevis");
            pnlCritereSelection.add(pnlDevis, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snDevise ----
            snDevise.setName("snDevise");
            pnlCritereSelection.add(snDevise, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeMagasin ----
            lbCodeMagasin.setText("Magasin");
            lbCodeMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbCodeMagasin.setName("lbCodeMagasin");
            pnlCritereSelection.add(lbCodeMagasin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setName("snMagasin");
            pnlCritereSelection.add(snMagasin, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbNumeroClientDebut;
  private SNClientPrincipal snClientDebut;
  private SNLabelChamp lbNumeroClientFin;
  private SNClientPrincipal snClientFin;
  private SNLabelChamp lbTopAttention;
  private SNComboBox cbSelectionOptions;
  private SNLabelChamp pnlDevis;
  private SNDevise snDevise;
  private SNLabelChamp lbCodeMagasin;
  private SNMagasin snMagasin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
