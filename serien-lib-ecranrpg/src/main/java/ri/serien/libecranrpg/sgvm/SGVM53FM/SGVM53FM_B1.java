
package ri.serien.libecranrpg.sgvm.SGVM53FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM53FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM53FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOUS.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
    LB3.setValeursSelection("X", " ");
    LB2.setValeursSelection("X", " ");
    LB1.setValeursSelection("X", " ");
    WTOUM.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_40.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    panel4.setVisible(lexique.isPresent("WTOU"));
    panel5.setVisible(lexique.isPresent("WTOUS"));
    GPFIN.setEnabled(lexique.isPresent("GPFIN"));
    GPDEB.setEnabled(lexique.isPresent("GPDEB"));
    EFIN.setVisible(lexique.isPresent("EFIN"));
    EDEB.setVisible(lexique.isPresent("EDEB"));
    OBJ_82.setVisible(lexique.isPresent("CODLOT"));
    FINIAE.setVisible(lexique.isPresent("FINIAE"));
    DEBIAE.setVisible(lexique.isPresent("DEBIAE"));
    // LB3.setSelected(lexique.HostFieldGetData("LB3").equalsIgnoreCase("X"));
    // LB2.setSelected(lexique.HostFieldGetData("LB2").equalsIgnoreCase("X"));
    OBJ_77.setVisible(lexique.isPresent("CODLOT"));
    // LB1.setSelected(lexique.HostFieldGetData("LB1").equalsIgnoreCase("X"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // WTOUS.setSelected(lexique.HostFieldGetData("WTOUS").equalsIgnoreCase("**"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    CODART.setVisible(lexique.isPresent("CODART"));
    CODLOT.setVisible(lexique.isPresent("CODLOT"));
    P0.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    P1.setVisible(!lexique.HostFieldGetData("WTOUS").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**"));
    OBJ_75.setVisible(lexique.isPresent("CODART"));
    
    OBJ_84.setVisible(lexique.isPresent("GPDEB"));
    OBJ_85.setVisible(lexique.isPresent("GPDEB"));
    OBJ_41.setVisible(lexique.isPresent("GPDEB"));
    
    if (lexique.isPresent("EDEB")) {
      OBJ_40.setTitle("Plage de Familles à traiter");
    }
    if (lexique.isPresent("DEBIAE")) {
      OBJ_40.setTitle("Plage de Sous-Familles à traiter");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (LB3.isSelected())
    // lexique.HostFieldPutData("LB3", 0, "X");
    // else
    // lexique.HostFieldPutData("LB3", 0, " ");
    // if (LB2.isSelected())
    // lexique.HostFieldPutData("LB2", 0, "X");
    // else
    // lexique.HostFieldPutData("LB2", 0, " ");
    // if (LB1.isSelected())
    // lexique.HostFieldPutData("LB1", 0, "X");
    // else
    // lexique.HostFieldPutData("LB1", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (WTOUS.isSelected())
    // lexique.HostFieldPutData("WTOUS", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUS", 0, " ");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvm53"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P0.setVisible(!P0.isVisible());
  }
  
  private void WTOUSActionPerformed(ActionEvent e) {
    P1.setVisible(!P1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_40 = new JXTitledSeparator();
    OBJ_47 = new JXTitledSeparator();
    OBJ_41 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    OBJ_84 = new JLabel();
    WTOUM = new XRiCheckBox();
    GPDEB = new XRiTextField();
    GPFIN = new XRiTextField();
    OBJ_85 = new JLabel();
    panel1 = new JPanel();
    OBJ_74 = new JXTitledSeparator();
    LB1 = new XRiCheckBox();
    LB2 = new XRiCheckBox();
    LB3 = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_75 = new JXTitledSeparator();
    OBJ_77 = new JLabel();
    CODART = new XRiTextField();
    CODLOT = new XRiTextField();
    OBJ_82 = new JLabel();
    panel4 = new JPanel();
    P0 = new JPanel();
    OBJ_72 = new JLabel();
    EDEB = new XRiTextField();
    OBJ_73 = new JLabel();
    EFIN = new XRiTextField();
    WTOU = new XRiCheckBox();
    panel5 = new JPanel();
    WTOUS = new XRiCheckBox();
    P1 = new JPanel();
    OBJ_76 = new JLabel();
    DEBIAE = new XRiTextField();
    OBJ_78 = new JLabel();
    FINIAE = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_83 = new JXTitledSeparator();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 730, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //---- OBJ_40 ----
          OBJ_40.setTitle("@PLAG@");
          OBJ_40.setName("OBJ_40");
          p_contenu.add(OBJ_40);
          OBJ_40.setBounds(35, 250, 730, OBJ_40.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setTitle("Codes magasins");
          OBJ_47.setName("OBJ_47");
          p_contenu.add(OBJ_47);
          OBJ_47.setBounds(35, 120, 730, OBJ_47.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setTitle("");
          OBJ_41.setName("OBJ_41");
          p_contenu.add(OBJ_41);
          OBJ_41.setBounds(35, 200, 730, OBJ_41.getPreferredSize().height);

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- MA01 ----
            MA01.setComponentPopupMenu(BTD);
            MA01.setName("MA01");
            P_SEL0.add(MA01);
            MA01.setBounds(10, 10, 34, MA01.getPreferredSize().height);

            //---- MA02 ----
            MA02.setComponentPopupMenu(BTD);
            MA02.setName("MA02");
            P_SEL0.add(MA02);
            MA02.setBounds(46, 10, 34, MA02.getPreferredSize().height);

            //---- MA03 ----
            MA03.setComponentPopupMenu(BTD);
            MA03.setName("MA03");
            P_SEL0.add(MA03);
            MA03.setBounds(82, 10, 34, MA03.getPreferredSize().height);

            //---- MA04 ----
            MA04.setComponentPopupMenu(BTD);
            MA04.setName("MA04");
            P_SEL0.add(MA04);
            MA04.setBounds(118, 10, 34, MA04.getPreferredSize().height);

            //---- MA05 ----
            MA05.setComponentPopupMenu(BTD);
            MA05.setName("MA05");
            P_SEL0.add(MA05);
            MA05.setBounds(154, 10, 34, MA05.getPreferredSize().height);

            //---- MA06 ----
            MA06.setComponentPopupMenu(BTD);
            MA06.setName("MA06");
            P_SEL0.add(MA06);
            MA06.setBounds(190, 10, 34, MA06.getPreferredSize().height);

            //---- MA07 ----
            MA07.setComponentPopupMenu(BTD);
            MA07.setName("MA07");
            P_SEL0.add(MA07);
            MA07.setBounds(226, 10, 34, MA07.getPreferredSize().height);

            //---- MA08 ----
            MA08.setComponentPopupMenu(BTD);
            MA08.setName("MA08");
            P_SEL0.add(MA08);
            MA08.setBounds(262, 10, 34, MA08.getPreferredSize().height);

            //---- MA09 ----
            MA09.setComponentPopupMenu(BTD);
            MA09.setName("MA09");
            P_SEL0.add(MA09);
            MA09.setBounds(298, 10, 34, MA09.getPreferredSize().height);

            //---- MA10 ----
            MA10.setComponentPopupMenu(BTD);
            MA10.setName("MA10");
            P_SEL0.add(MA10);
            MA10.setBounds(334, 10, 34, MA10.getPreferredSize().height);

            //---- MA11 ----
            MA11.setComponentPopupMenu(BTD);
            MA11.setName("MA11");
            P_SEL0.add(MA11);
            MA11.setBounds(370, 10, 34, MA11.getPreferredSize().height);

            //---- MA12 ----
            MA12.setComponentPopupMenu(BTD);
            MA12.setName("MA12");
            P_SEL0.add(MA12);
            MA12.setBounds(406, 10, 34, MA12.getPreferredSize().height);
          }
          p_contenu.add(P_SEL0);
          P_SEL0.setBounds(205, 145, 460, 43);

          //---- OBJ_84 ----
          OBJ_84.setText("Code type produit");
          OBJ_84.setName("OBJ_84");
          p_contenu.add(OBJ_84);
          OBJ_84.setBounds(50, 219, 150, 20);

          //---- WTOUM ----
          WTOUM.setText("S\u00e9lection compl\u00e8te");
          WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUM.setName("WTOUM");
          WTOUM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUMActionPerformed(e);
            }
          });
          p_contenu.add(WTOUM);
          WTOUM.setBounds(50, 152, 141, WTOUM.getPreferredSize().height);

          //---- GPDEB ----
          GPDEB.setComponentPopupMenu(BTD);
          GPDEB.setName("GPDEB");
          p_contenu.add(GPDEB);
          GPDEB.setBounds(205, 215, 24, GPDEB.getPreferredSize().height);

          //---- GPFIN ----
          GPFIN.setComponentPopupMenu(BTD);
          GPFIN.setName("GPFIN");
          p_contenu.add(GPFIN);
          GPFIN.setBounds(252, 215, 24, GPFIN.getPreferredSize().height);

          //---- OBJ_85 ----
          OBJ_85.setText("/");
          OBJ_85.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_85.setName("OBJ_85");
          p_contenu.add(OBJ_85);
          OBJ_85.setBounds(234, 221, 13, OBJ_85.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_74 ----
            OBJ_74.setTitle("Edition des libell\u00e9s compl\u00e9mentaires");
            OBJ_74.setName("OBJ_74");
            panel1.add(OBJ_74);
            OBJ_74.setBounds(0, 5, 300, OBJ_74.getPreferredSize().height);

            //---- LB1 ----
            LB1.setText("Libell\u00e9 1");
            LB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB1.setName("LB1");
            panel1.add(LB1);
            LB1.setBounds(15, 25, 102, LB1.getPreferredSize().height);

            //---- LB2 ----
            LB2.setText("Libell\u00e9 2");
            LB2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB2.setName("LB2");
            panel1.add(LB2);
            LB2.setBounds(121, 25, 99, LB2.getPreferredSize().height);

            //---- LB3 ----
            LB3.setText("Libell\u00e9 3");
            LB3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB3.setName("LB3");
            panel1.add(LB3);
            LB3.setBounds(229, 25, 93, LB3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(35, 335, 350, 60);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- OBJ_75 ----
            OBJ_75.setTitle("S\u00e9lection");
            OBJ_75.setName("OBJ_75");

            //---- OBJ_77 ----
            OBJ_77.setText("Code article");
            OBJ_77.setName("OBJ_77");

            //---- CODART ----
            CODART.setComponentPopupMenu(BTD);
            CODART.setName("CODART");

            //---- CODLOT ----
            CODLOT.setComponentPopupMenu(BTD);
            CODLOT.setName("CODLOT");

            //---- OBJ_82 ----
            OBJ_82.setText("Lot");
            OBJ_82.setName("OBJ_82");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(CODART, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                      .addGap(57, 57, 57)
                      .addComponent(CODLOT, GroupLayout.PREFERRED_SIZE, 264, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addContainerGap()
                      .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 382, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap())
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CODART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CODLOT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel2);
          panel2.setBounds(375, 335, 415, 85);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== P0 ========
            {
              P0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P0.setOpaque(false);
              P0.setName("P0");
              P0.setLayout(null);

              //---- OBJ_72 ----
              OBJ_72.setText("Code famille d\u00e9but");
              OBJ_72.setName("OBJ_72");
              P0.add(OBJ_72);
              OBJ_72.setBounds(15, 15, 155, 18);

              //---- EDEB ----
              EDEB.setComponentPopupMenu(BTD);
              EDEB.setName("EDEB");
              P0.add(EDEB);
              EDEB.setBounds(165, 10, 44, EDEB.getPreferredSize().height);

              //---- OBJ_73 ----
              OBJ_73.setText("Code famille fin");
              OBJ_73.setName("OBJ_73");
              P0.add(OBJ_73);
              OBJ_73.setBounds(240, 15, 137, 18);

              //---- EFIN ----
              EFIN.setComponentPopupMenu(BTD);
              EFIN.setName("EFIN");
              P0.add(EFIN);
              EFIN.setBounds(385, 10, 44, EFIN.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P0.getComponentCount(); i++) {
                  Rectangle bounds = P0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P0.setMinimumSize(preferredSize);
                P0.setPreferredSize(preferredSize);
              }
            }
            panel4.add(P0);
            P0.setBounds(180, 10, 460, 45);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel4.add(WTOU);
            WTOU.setBounds(25, 23, 141, WTOU.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(25, 265, 745, 65);

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- WTOUS ----
            WTOUS.setText("S\u00e9lection compl\u00e8te");
            WTOUS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUS.setName("WTOUS");
            WTOUS.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUSActionPerformed(e);
              }
            });
            panel5.add(WTOUS);
            WTOUS.setBounds(25, 16, 141, WTOUS.getPreferredSize().height);

            //======== P1 ========
            {
              P1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P1.setOpaque(false);
              P1.setName("P1");
              P1.setLayout(null);

              //---- OBJ_76 ----
              OBJ_76.setText("Code sous-famille d\u00e9but");
              OBJ_76.setName("OBJ_76");
              P1.add(OBJ_76);
              OBJ_76.setBounds(15, 10, 155, 18);

              //---- DEBIAE ----
              DEBIAE.setComponentPopupMenu(BTD);
              DEBIAE.setName("DEBIAE");
              P1.add(DEBIAE);
              DEBIAE.setBounds(165, 5, 64, DEBIAE.getPreferredSize().height);

              //---- OBJ_78 ----
              OBJ_78.setText("Code sous-famille fin");
              OBJ_78.setName("OBJ_78");
              P1.add(OBJ_78);
              OBJ_78.setBounds(240, 10, 137, 18);

              //---- FINIAE ----
              FINIAE.setComponentPopupMenu(BTD);
              FINIAE.setName("FINIAE");
              P1.add(FINIAE);
              FINIAE.setBounds(385, 5, 64, FINIAE.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P1.getComponentCount(); i++) {
                  Rectangle bounds = P1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P1.setMinimumSize(preferredSize);
                P1.setPreferredSize(preferredSize);
              }
            }
            panel5.add(P1);
            P1.setBounds(180, 5, 460, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel5);
          panel5.setBounds(25, 270, 700, 60);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_83 ----
    OBJ_83.setTitle("");
    OBJ_83.setName("OBJ_83");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_40;
  private JXTitledSeparator OBJ_47;
  private JXTitledSeparator OBJ_41;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JLabel OBJ_84;
  private XRiCheckBox WTOUM;
  private XRiTextField GPDEB;
  private XRiTextField GPFIN;
  private JLabel OBJ_85;
  private JPanel panel1;
  private JXTitledSeparator OBJ_74;
  private XRiCheckBox LB1;
  private XRiCheckBox LB2;
  private XRiCheckBox LB3;
  private JPanel panel2;
  private JXTitledSeparator OBJ_75;
  private JLabel OBJ_77;
  private XRiTextField CODART;
  private XRiTextField CODLOT;
  private JLabel OBJ_82;
  private JPanel panel4;
  private JPanel P0;
  private JLabel OBJ_72;
  private XRiTextField EDEB;
  private JLabel OBJ_73;
  private XRiTextField EFIN;
  private XRiCheckBox WTOU;
  private JPanel panel5;
  private XRiCheckBox WTOUS;
  private JPanel P1;
  private JLabel OBJ_76;
  private XRiTextField DEBIAE;
  private JLabel OBJ_78;
  private XRiTextField FINIAE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JXTitledSeparator OBJ_83;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
