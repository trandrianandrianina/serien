
package ri.serien.libecranrpg.sgvm.SGVML1FM;
// Nom Fichier: pop_SGVML1FM_FMTB2_FMTF1_796.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVML1FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVML1FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSF01@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_6.setVisible(lexique.isPresent("MSF01"));
    
    // TODO Icones
    
    // V07F
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    BT_ENTER = new JButton();
    panel2 = new JPanel();
    OBJ_6 = new JLabel();
    P_PnlOpts = new JPanel();

    //======== this ========
    setPreferredSize(new Dimension(325, 100));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- BT_ENTER ----
      BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ENTER.setToolTipText("OK");
      BT_ENTER.setName("BT_ENTER");
      BT_ENTER.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      panel1.add(BT_ENTER);
      BT_ENTER.setBounds(265, 0, 56, 40);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    add(panel1, BorderLayout.SOUTH);

    //======== panel2 ========
    {
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- OBJ_6 ----
      OBJ_6.setText("@MSF01@");
      OBJ_6.setName("OBJ_6");
      panel2.add(OBJ_6);
      OBJ_6.setBounds(15, 20, 288, 20);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel2.getComponentCount(); i++) {
          Rectangle bounds = panel2.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel2.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel2.setMinimumSize(preferredSize);
        panel2.setPreferredSize(preferredSize);
      }
    }
    add(panel2, BorderLayout.CENTER);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      P_PnlOpts.setPreferredSize(new Dimension(55, 75));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel1;
  private JButton BT_ENTER;
  private JPanel panel2;
  private JLabel OBJ_6;
  private JPanel P_PnlOpts;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
