
package ri.serien.libecranrpg.sgvm.SGVM27FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM27FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_TRI = "Tri et sélection";
  
  public SGVM27FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    
    // - Ajouter un bouton personnalisé
    snBarreBouton.ajouterBouton(BOUTON_TRI, 't', true);
    // -
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    pnlSelectionPlageFactures.setVisible(!WTOU.isSelected());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    chargerComposantMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Magasin
    snMagasinSelectionne.renseignerChampRPG(lexique, "MAG");
  }
  
  // charger composant par fonction
  private void chargerComposantMagasin() {
    snMagasinSelectionne.setSession(getSession());
    snMagasinSelectionne.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinSelectionne.setTousAutorise(true);
    snMagasinSelectionne.charger(false);
    snMagasinSelectionne.setSelectionParChampRPG(lexique, "MAG");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_TRI)) {
        lexique.HostFieldPutData("WCHOIX", 0, "X");
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    pnlSelectionPlageFactures.setVisible(!pnlSelectionPlageFactures.isVisible());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantMagasin();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlMagasin = new SNPanelTitre();
    lbMagasinSelectionne = new SNLabelChamp();
    snMagasinSelectionne = new SNMagasin();
    pnlFactures = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    pnlSelectionPlageFactures = new SNPanel();
    lbFactureDebut = new SNLabelChamp();
    NUMDEB = new XRiTextField();
    lbFactureFin = new SNLabelChamp();
    NUMFIN = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 660));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlMagasin ========
        {
          pnlMagasin.setTitre("Magasin \u00e0 traiter");
          pnlMagasin.setName("pnlMagasin");
          pnlMagasin.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlMagasin.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlMagasin.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlMagasin.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlMagasin.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbMagasinSelectionne ----
          lbMagasinSelectionne.setText("Magasin");
          lbMagasinSelectionne.setName("lbMagasinSelectionne");
          pnlMagasin.add(lbMagasinSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasinSelectionne ----
          snMagasinSelectionne.setName("snMagasinSelectionne");
          pnlMagasin.add(snMagasinSelectionne, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlFactures ========
        {
          pnlFactures.setTitre("Plage des factures \u00e0 traiter");
          pnlFactures.setName("pnlFactures");
          pnlFactures.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlFactures.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlFactures.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlFactures.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlFactures.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTOU ----
          WTOU.setText("S\u00e9lectionner toutes les factures");
          WTOU.setComponentPopupMenu(null);
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMaximumSize(new Dimension(129, 30));
          WTOU.setMinimumSize(new Dimension(129, 30));
          WTOU.setPreferredSize(new Dimension(129, 30));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });
          pnlFactures.add(WTOU, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionPlageFactures ========
          {
            pnlSelectionPlageFactures.setName("pnlSelectionPlageFactures");
            pnlSelectionPlageFactures.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionPlageFactures.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageFactures.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageFactures.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionPlageFactures.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbFactureDebut ----
            lbFactureDebut.setText("Num\u00e9ro de facture de d\u00e9but");
            lbFactureDebut.setMaximumSize(new Dimension(200, 30));
            lbFactureDebut.setMinimumSize(new Dimension(200, 30));
            lbFactureDebut.setPreferredSize(new Dimension(200, 30));
            lbFactureDebut.setName("lbFactureDebut");
            pnlSelectionPlageFactures.add(lbFactureDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- NUMDEB ----
            NUMDEB.setPreferredSize(new Dimension(80, 30));
            NUMDEB.setMinimumSize(new Dimension(80, 30));
            NUMDEB.setMaximumSize(new Dimension(80, 30));
            NUMDEB.setName("NUMDEB");
            pnlSelectionPlageFactures.add(NUMDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbFactureFin ----
            lbFactureFin.setText("Num\u00e9ro de facture de fin");
            lbFactureFin.setName("lbFactureFin");
            pnlSelectionPlageFactures.add(lbFactureFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- NUMFIN ----
            NUMFIN.setMaximumSize(new Dimension(80, 30));
            NUMFIN.setMinimumSize(new Dimension(80, 30));
            NUMFIN.setPreferredSize(new Dimension(80, 30));
            NUMFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            NUMFIN.setName("NUMFIN");
            pnlSelectionPlageFactures.add(NUMFIN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFactures.add(pnlSelectionPlageFactures, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlFactures, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setEditable(false);
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlMagasin;
  private SNLabelChamp lbMagasinSelectionne;
  private SNMagasin snMagasinSelectionne;
  private SNPanelTitre pnlFactures;
  private XRiCheckBox WTOU;
  private SNPanel pnlSelectionPlageFactures;
  private SNLabelChamp lbFactureDebut;
  private XRiTextField NUMDEB;
  private SNLabelChamp lbFactureFin;
  private XRiTextField NUMFIN;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
