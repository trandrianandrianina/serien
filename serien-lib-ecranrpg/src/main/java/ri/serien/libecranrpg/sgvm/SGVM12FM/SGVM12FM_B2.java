
package ri.serien.libecranrpg.sgvm.SGVM12FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM12FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public SGVM12FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    ETAR.setValeursSelection("OUI", "NON");
    EDTF14.setValeursSelection("OUI", "NON");
    ARTDES.setValeursSelection("OUI", "NON");
    EDTF65.setValeursSelection("O", "N");
    WTOU.setValeursSelection("**", "");
    
    // Initialisation de la barre des boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation du composant article début
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "DEBLO2");
    
    // Initialisation du composant article début
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "FINLO2");
    
    // Initialisation du composant dates
    snPlageDate.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPlageDate.setDateDebutParChampRPG(lexique, "ATDAPX");
    snPlageDate.setDateFinParChampRPG(lexique, "ATDAFX");
    lbDateValidite.setVisible(lexique.isTrue("(20) and (N97)"));
    snPlageDate.setVisible(lexique.isTrue("(20) and (N97)"));
    
    // Libellés
    LIB1.setSelected(!lexique.HostFieldGetData("LIB1").trim().isEmpty());
    LIB2.setSelected(!lexique.HostFieldGetData("LIB2").trim().isEmpty());
    LIB3.setSelected(!lexique.HostFieldGetData("LIB3").trim().isEmpty());
    LIB4.setSelected(!lexique.HostFieldGetData("LIB4").trim().isEmpty());
    
    lbNumeroTarif.setVisible(lexique.isTrue("20"));
    NTAR.setVisible(lexique.isTrue("20"));
    lbPrix.setVisible(lexique.isTrue("20"));
    WPTAR.setVisible(lexique.isTrue("20"));
    lbLigne.setVisible(LIGDEP.isVisible());
    
    // Si on a choisi "tous" on cache les champs de sélection de plage
    if (WTOU.isSelected()) {
      lbArticle1.setVisible(false);
      lbArticle2.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else {
      lbArticle1.setVisible(true);
      lbArticle2.setVisible(true);
      snArticleDebut.setVisible(true);
      snArticleFin.setVisible(true);
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snPlageDate.renseignerChampRPGDebut(lexique, "ATDAPX");
    snPlageDate.renseignerChampRPGFin(lexique, "ATDAFX");
    
    if (LIB1.isSelected()) {
      lexique.HostFieldPutData("LIB1", 0, "1");
    }
    else {
      lexique.HostFieldPutData("LIB1", 0, "");
    }
    if (LIB2.isSelected()) {
      lexique.HostFieldPutData("LIB2", 0, "1");
    }
    else {
      lexique.HostFieldPutData("LIB2", 0, "");
    }
    if (LIB3.isSelected()) {
      lexique.HostFieldPutData("LIB3", 0, "1");
    }
    else {
      lexique.HostFieldPutData("LIB3", 0, "");
    }
    if (LIB4.isSelected()) {
      lexique.HostFieldPutData("LIB4", 0, "1");
    }
    else {
      lexique.HostFieldPutData("LIB4", 0, "");
    }
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelectionArticle = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    lbArticle1 = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticle2 = new SNLabelChamp();
    snArticleFin = new SNArticle();
    lbDateValidite = new SNLabelChamp();
    snPlageDate = new SNPlageDate();
    lbNombreEtiquettes = new SNLabelChamp();
    NBETQ = new XRiSpinner();
    lbNumeroTarif = new SNLabelChamp();
    NTAR = new XRiSpinner();
    lbLibelle = new SNLabelChamp();
    pnlLibelles = new SNPanel();
    LIB1 = new XRiCheckBox();
    LIB2 = new XRiCheckBox();
    LIB3 = new XRiCheckBox();
    LIB4 = new XRiCheckBox();
    lbPrix = new SNLabelChamp();
    WPTAR = new XRiComboBox();
    lbLigne = new SNLabelChamp();
    LIGDEP = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlEtatAvoir = new SNPanelTitre();
    ETAR = new XRiCheckBox();
    EDTF14 = new XRiCheckBox();
    EDTF65 = new XRiCheckBox();
    ARTDES = new XRiCheckBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlCritereSelectionArticle ========
        {
          pnlCritereSelectionArticle.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelectionArticle.setName("pnlCritereSelectionArticle");
          pnlCritereSelectionArticle.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- WTOU ----
          WTOU.setText("Tous les articles");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMinimumSize(new Dimension(124, 30));
          WTOU.setPreferredSize(new Dimension(124, 30));
          WTOU.setEnabled(false);
          WTOU.setName("WTOU");
          pnlCritereSelectionArticle.add(WTOU, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticle1 ----
          lbArticle1.setText("Article de d\u00e9but");
          lbArticle1.setName("lbArticle1");
          pnlCritereSelectionArticle.add(lbArticle1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleDebut ----
          snArticleDebut.setEnabled(false);
          snArticleDebut.setName("snArticleDebut");
          pnlCritereSelectionArticle.add(snArticleDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticle2 ----
          lbArticle2.setText("Article de fin");
          lbArticle2.setName("lbArticle2");
          pnlCritereSelectionArticle.add(lbArticle2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleFin ----
          snArticleFin.setEnabled(false);
          snArticleFin.setName("snArticleFin");
          pnlCritereSelectionArticle.add(snArticleFin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDateValidite ----
          lbDateValidite.setText("Date de validit\u00e9 du tarif");
          lbDateValidite.setName("lbDateValidite");
          pnlCritereSelectionArticle.add(lbDateValidite, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snPlageDate ----
          snPlageDate.setName("snPlageDate");
          pnlCritereSelectionArticle.add(snPlageDate, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNombreEtiquettes ----
          lbNombreEtiquettes.setText("Nombre d'\u00e9tiquettes");
          lbNombreEtiquettes.setName("lbNombreEtiquettes");
          pnlCritereSelectionArticle.add(lbNombreEtiquettes, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- NBETQ ----
          NBETQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NBETQ.setModel(new SpinnerListModel(new String[] { " ", "0", "1", "2", "3", "4" }) {
            {
              setValue("0");
            }
          });
          NBETQ.setFont(new Font("sansserif", Font.PLAIN, 14));
          NBETQ.setName("NBETQ");
          pnlCritereSelectionArticle.add(NBETQ, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNumeroTarif ----
          lbNumeroTarif.setText("Num\u00e9ro du tarif");
          lbNumeroTarif.setName("lbNumeroTarif");
          pnlCritereSelectionArticle.add(lbNumeroTarif, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- NTAR ----
          NTAR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NTAR.setModel(new SpinnerListModel(new String[] { " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }) {
            {
              setValue("0");
            }
          });
          NTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
          NTAR.setName("NTAR");
          pnlCritereSelectionArticle.add(NTAR, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLibelle ----
          lbLibelle.setText("Lib\u00e9ll\u00e9s \u00e0 \u00e9diter");
          lbLibelle.setName("lbLibelle");
          pnlCritereSelectionArticle.add(lbLibelle, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlLibelles ========
          {
            pnlLibelles.setName("pnlLibelles");
            pnlLibelles.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlLibelles.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlLibelles.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlLibelles.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlLibelles.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- LIB1 ----
            LIB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LIB1.setFont(new Font("sansserif", Font.PLAIN, 14));
            LIB1.setMinimumSize(new Dimension(50, 30));
            LIB1.setPreferredSize(new Dimension(50, 30));
            LIB1.setText("1");
            LIB1.setMaximumSize(new Dimension(50, 30));
            LIB1.setName("LIB1");
            pnlLibelles.add(LIB1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- LIB2 ----
            LIB2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LIB2.setFont(new Font("sansserif", Font.PLAIN, 14));
            LIB2.setMinimumSize(new Dimension(50, 30));
            LIB2.setPreferredSize(new Dimension(50, 30));
            LIB2.setText("2");
            LIB2.setMaximumSize(new Dimension(50, 30));
            LIB2.setName("LIB2");
            pnlLibelles.add(LIB2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- LIB3 ----
            LIB3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LIB3.setFont(new Font("sansserif", Font.PLAIN, 14));
            LIB3.setMinimumSize(new Dimension(50, 30));
            LIB3.setPreferredSize(new Dimension(50, 30));
            LIB3.setText("3");
            LIB3.setMaximumSize(new Dimension(50, 30));
            LIB3.setName("LIB3");
            pnlLibelles.add(LIB3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- LIB4 ----
            LIB4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LIB4.setFont(new Font("sansserif", Font.PLAIN, 14));
            LIB4.setMinimumSize(new Dimension(50, 30));
            LIB4.setPreferredSize(new Dimension(50, 30));
            LIB4.setText("4");
            LIB4.setMaximumSize(new Dimension(50, 30));
            LIB4.setName("LIB4");
            pnlLibelles.add(LIB4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCritereSelectionArticle.add(pnlLibelles, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPrix ----
          lbPrix.setText("Prix \u00e0 \u00e9diter");
          lbPrix.setName("lbPrix");
          pnlCritereSelectionArticle.add(lbPrix, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WPTAR ----
          WPTAR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPTAR.setModel(new DefaultComboBoxModel(new String[] { "HT ", "TTC" }));
          WPTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
          WPTAR.setBackground(Color.white);
          WPTAR.setName("WPTAR");
          pnlCritereSelectionArticle.add(WPTAR, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLigne ----
          lbLigne.setText("Ligne de d\u00e9part");
          lbLigne.setName("lbLigne");
          pnlCritereSelectionArticle.add(lbLigne, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- LIGDEP ----
          LIGDEP.setMinimumSize(new Dimension(30, 30));
          LIGDEP.setMaximumSize(new Dimension(30, 30));
          LIGDEP.setPreferredSize(new Dimension(30, 30));
          LIGDEP.setName("LIGDEP");
          pnlCritereSelectionArticle.add(LIGDEP, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelectionArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfEnCours ----
          tfEnCours.setText("@WENCX@");
          tfEnCours.setEnabled(false);
          tfEnCours.setPreferredSize(new Dimension(260, 30));
          tfEnCours.setMinimumSize(new Dimension(260, 30));
          tfEnCours.setMaximumSize(new Dimension(260, 30));
          tfEnCours.setName("tfEnCours");
          pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setEnabled(false);
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlEtatAvoir ========
        {
          pnlEtatAvoir.setTitre("Options");
          pnlEtatAvoir.setName("pnlEtatAvoir");
          pnlEtatAvoir.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtatAvoir.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtatAvoir.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlEtatAvoir.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtatAvoir.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- ETAR ----
          ETAR.setText("Edition du tarif sur l'\u00e9tiquette");
          ETAR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ETAR.setFont(new Font("sansserif", Font.PLAIN, 14));
          ETAR.setMinimumSize(new Dimension(124, 30));
          ETAR.setPreferredSize(new Dimension(124, 30));
          ETAR.setName("ETAR");
          pnlEtatAvoir.add(ETAR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EDTF14 ----
          EDTF14.setText("Etiquettes / laser (format 2*7)");
          EDTF14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTF14.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDTF14.setMinimumSize(new Dimension(124, 30));
          EDTF14.setPreferredSize(new Dimension(124, 30));
          EDTF14.setName("EDTF14");
          pnlEtatAvoir.add(EDTF14, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EDTF65 ----
          EDTF65.setText("Etiquettes / laser (format 5*13) avec ligne d\u00e9part premi\u00e8re \u00e9tiquette");
          EDTF65.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTF65.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDTF65.setMinimumSize(new Dimension(124, 30));
          EDTF65.setPreferredSize(new Dimension(124, 30));
          EDTF65.setName("EDTF65");
          pnlEtatAvoir.add(EDTF65, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- ARTDES ----
          ARTDES.setText("Edition des articles d\u00e9sactiv\u00e9s");
          ARTDES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARTDES.setFont(new Font("sansserif", Font.PLAIN, 14));
          ARTDES.setMinimumSize(new Dimension(124, 30));
          ARTDES.setPreferredSize(new Dimension(124, 30));
          ARTDES.setName("ARTDES");
          pnlEtatAvoir.add(ARTDES, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtatAvoir, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelectionArticle;
  private XRiCheckBox WTOU;
  private SNLabelChamp lbArticle1;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticle2;
  private SNArticle snArticleFin;
  private SNLabelChamp lbDateValidite;
  private SNPlageDate snPlageDate;
  private SNLabelChamp lbNombreEtiquettes;
  private XRiSpinner NBETQ;
  private SNLabelChamp lbNumeroTarif;
  private XRiSpinner NTAR;
  private SNLabelChamp lbLibelle;
  private SNPanel pnlLibelles;
  private XRiCheckBox LIB1;
  private XRiCheckBox LIB2;
  private XRiCheckBox LIB3;
  private XRiCheckBox LIB4;
  private SNLabelChamp lbPrix;
  private XRiComboBox WPTAR;
  private SNLabelChamp lbLigne;
  private XRiTextField LIGDEP;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlEtatAvoir;
  private XRiCheckBox ETAR;
  private XRiCheckBox EDTF14;
  private XRiCheckBox EDTF65;
  private XRiCheckBox ARTDES;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
