
package ri.serien.libecranrpg.sgvm.SGVM4PFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2921] Gestion des ventes -> Documents de ventes -> Etats des bons -> Bons détaillés sur période -> Détail et marge
 * Indicateur : 00000001
 * Titre : Edition des bons de ventes détaillés
 */
public class SGVM4PFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  
  public SGVM4PFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfEnCours.setVisible(lexique.isPresent("WENCX"));
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Renseigne l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    ckTousFournisseurs.setSelected(!lexique.HostFieldGetData("WTOUF").isEmpty());
    
    // Charge les composants de sélection
    chargerComposantMagasin();
    chargerComposantFournisseur();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Renseigne les champ qui on été sélectionné
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    if (ckTousFournisseurs.isSelected()) {
      lexique.HostFieldPutData("WTOUF", 0, "**");
    }
    else {
      snFournisseur.renseignerChampRPG(lexique, "WCOL", "WFRS");
    }
    
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    if (lexique.HostFieldGetData("WMAG").equals("**") || lexique.HostFieldGetData("WMAG").trim().isEmpty()) {
      lexique.HostFieldPutData("WTOUM", 0, "**");
    }
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composant des magasins suivant l'établissement
   **/
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    if (lexique.HostFieldGetData("WTOUM").equals("**") || lexique.HostFieldGetData("WMAG").trim().isEmpty()) {
      snMagasin.setSelectionParChampRPG(lexique, "WTOUM");
    }
  }
  
  /**
   * Charge le composant des sections analytique suivant l'établissement
   **/
  private void chargerComposantFournisseur() {
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(true);
    if (ckTousFournisseurs.isSelected()) {
      snFournisseur.initialiserRecherche();
      snFournisseur.setEnabled(false);
    }
    else {
      snFournisseur.setSelectionParChampRPG(lexique, "WCOL", "WFRS");
      snFournisseur.setEnabled(true);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantMagasin();
      chargerComposantFournisseur();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckTousFournisseursActionPerformed(ActionEvent e) {
    try {
      chargerComposantFournisseur();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbPeriodeAediter = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    pnlRestrictionFournisseur = new SNPanelTitre();
    ckTousFournisseurs = new JCheckBox();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    pnlRestrictionMagasin = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des bons de ventes d\u00e9taill\u00e9s");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.LEADING);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbPeriodeAediter ----
            lbPeriodeAediter.setText("P\u00e9riode \u00e0 \u00e9diter du");
            lbPeriodeAediter.setMinimumSize(new Dimension(125, 30));
            lbPeriodeAediter.setPreferredSize(new Dimension(125, 30));
            lbPeriodeAediter.setMaximumSize(new Dimension(125, 30));
            lbPeriodeAediter.setName("lbPeriodeAediter");
            pnlCritereDeSelection.add(lbPeriodeAediter, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- PERDEB ----
              PERDEB.setNextFocusableComponent(null);
              PERDEB.setMinimumSize(new Dimension(110, 30));
              PERDEB.setPreferredSize(new Dimension(110, 30));
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setName("PERDEB");
              sNPanel1.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              sNPanel1.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERFIN ----
              PERFIN.setNextFocusableComponent(null);
              PERFIN.setPreferredSize(new Dimension(110, 30));
              PERFIN.setMinimumSize(new Dimension(110, 30));
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setName("PERFIN");
              sNPanel1.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlRestrictionFournisseur ========
          {
            pnlRestrictionFournisseur.setOpaque(false);
            pnlRestrictionFournisseur.setTitre("Restriction fournisseur");
            pnlRestrictionFournisseur.setName("pnlRestrictionFournisseur");
            pnlRestrictionFournisseur.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRestrictionFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRestrictionFournisseur.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRestrictionFournisseur.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRestrictionFournisseur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- ckTousFournisseurs ----
            ckTousFournisseurs.setText("Tous les fournisseurs");
            ckTousFournisseurs.setFont(new Font("sansserif", Font.PLAIN, 14));
            ckTousFournisseurs.setPreferredSize(new Dimension(158, 30));
            ckTousFournisseurs.setMinimumSize(new Dimension(158, 30));
            ckTousFournisseurs.setMaximumSize(new Dimension(158, 30));
            ckTousFournisseurs.setName("ckTousFournisseurs");
            ckTousFournisseurs.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ckTousFournisseursActionPerformed(e);
              }
            });
            pnlRestrictionFournisseur.add(ckTousFournisseurs, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setName("lbFournisseur");
            pnlRestrictionFournisseur.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snFournisseur ----
            snFournisseur.setName("snFournisseur");
            pnlRestrictionFournisseur.add(snFournisseur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlRestrictionFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlRestrictionMagasin ========
          {
            pnlRestrictionMagasin.setOpaque(false);
            pnlRestrictionMagasin.setTitre("Restriction magasin");
            pnlRestrictionMagasin.setName("pnlRestrictionMagasin");
            pnlRestrictionMagasin.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRestrictionMagasin.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRestrictionMagasin.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRestrictionMagasin.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRestrictionMagasin.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlRestrictionMagasin.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlRestrictionMagasin.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlRestrictionMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setEditable(false);
            tfEnCours.setEnabled(false);
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbPeriodeAediter;
  private SNPanel sNPanel1;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNPanelTitre pnlRestrictionFournisseur;
  private JCheckBox ckTousFournisseurs;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNPanelTitre pnlRestrictionMagasin;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
