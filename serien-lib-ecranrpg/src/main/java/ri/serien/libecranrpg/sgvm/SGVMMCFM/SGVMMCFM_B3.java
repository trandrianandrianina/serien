
package ri.serien.libecranrpg.sgvm.SGVMMCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVMMCFM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMMCFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TCF.setValeursSelection("X", " ");
    TCL.setValeursSelection("X", " ");
    ADSLIV.setValeursSelection("X", " ");
    ADSFAC.setValeursSelection("X", " ");
    DUPLOT.setValeursSelection("X", " ");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel2.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Modification numéro client"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("NOUCLI");
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_18 = new JLabel();
    NOUCLI = new XRiTextField();
    NOULIV = new XRiTextField();
    TCL = new XRiCheckBox();
    TCF = new XRiCheckBox();
    NOMCLI = new XRiTextField();
    OBJ_19 = new JLabel();
    ADSLIV = new XRiCheckBox();
    ADSFAC = new XRiCheckBox();
    DUPLOT = new XRiCheckBox();
    NOUTFA = new XRiTextField();
    OBJ_20 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    menus_haut = new JPanel();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(680, 320));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("@TITPG1@"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_18 ----
          OBJ_18.setText("Nouveau client");
          OBJ_18.setName("OBJ_18");
          panel2.add(OBJ_18);
          OBJ_18.setBounds(20, 44, 105, 20);

          //---- NOUCLI ----
          NOUCLI.setComponentPopupMenu(BTD);
          NOUCLI.setName("NOUCLI");
          panel2.add(NOUCLI);
          NOUCLI.setBounds(125, 40, 58, NOUCLI.getPreferredSize().height);

          //---- NOULIV ----
          NOULIV.setComponentPopupMenu(BTD);
          NOULIV.setName("NOULIV");
          panel2.add(NOULIV);
          NOULIV.setBounds(185, 40, 34, NOULIV.getPreferredSize().height);

          //---- TCL ----
          TCL.setText("Adresse de livraison");
          TCL.setComponentPopupMenu(BTD);
          TCL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TCL.setName("TCL");
          panel2.add(TCL);
          TCL.setBounds(20, 105, 148, 20);

          //---- TCF ----
          TCF.setText("Adresse de facturation");
          TCF.setComponentPopupMenu(BTD);
          TCF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TCF.setName("TCF");
          panel2.add(TCF);
          TCF.setBounds(200, 105, 159, 20);

          //---- NOMCLI ----
          NOMCLI.setComponentPopupMenu(BTD);
          NOMCLI.setName("NOMCLI");
          panel2.add(NOMCLI);
          NOMCLI.setBounds(125, 70, 314, NOMCLI.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Recherche");
          OBJ_19.setName("OBJ_19");
          panel2.add(OBJ_19);
          OBJ_19.setBounds(20, 74, 105, 20);

          //---- ADSLIV ----
          ADSLIV.setText("Duplication de l'adresse de livraison saisie");
          ADSLIV.setComponentPopupMenu(BTD);
          ADSLIV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ADSLIV.setName("ADSLIV");
          panel2.add(ADSLIV);
          ADSLIV.setBounds(20, 185, 410, 20);

          //---- ADSFAC ----
          ADSFAC.setText("Duplication de l'adresse de facturation saisie");
          ADSFAC.setComponentPopupMenu(BTD);
          ADSFAC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ADSFAC.setName("ADSFAC");
          panel2.add(ADSFAC);
          ADSFAC.setBounds(20, 215, 410, 20);

          //---- DUPLOT ----
          DUPLOT.setText("Duplication des lots");
          DUPLOT.setComponentPopupMenu(BTD);
          DUPLOT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DUPLOT.setName("DUPLOT");
          panel2.add(DUPLOT);
          DUPLOT.setBounds(20, 245, 410, 20);

          //---- NOUTFA ----
          NOUTFA.setComponentPopupMenu(BTD);
          NOUTFA.setName("NOUTFA");
          panel2.add(NOUTFA);
          NOUTFA.setBounds(200, 136, 24, NOUTFA.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("Nouveau type de facturation");
          OBJ_20.setName("OBJ_20");
          panel2.add(OBJ_20);
          OBJ_20.setBounds(20, 140, 175, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //======== menus_haut ========
    {
      menus_haut.setPreferredSize(new Dimension(170, 90));
      menus_haut.setOpaque(false);
      menus_haut.setName("menus_haut");
      menus_haut.setLayout(new VerticalLayout());

      //======== riMenu1 ========
      {
        riMenu1.setName("riMenu1");

        //---- riMenu_bt1 ----
        riMenu_bt1.setText("Client");
        riMenu_bt1.setName("riMenu_bt1");
        riMenu1.add(riMenu_bt1);
      }
      menus_haut.add(riMenu1);

      //======== riSousMenu1 ========
      {
        riSousMenu1.setName("riSousMenu1");

        //---- riSousMenu_bt1 ----
        riSousMenu_bt1.setText("Recherche ou cr\u00e9ation");
        riSousMenu_bt1.setToolTipText("Recherche ou cr\u00e9ation du nouveau client");
        riSousMenu_bt1.setName("riSousMenu_bt1");
        riSousMenu_bt1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            riSousMenu_bt1ActionPerformed(e);
          }
        });
        riSousMenu1.add(riSousMenu_bt1);
      }
      menus_haut.add(riSousMenu1);

      //======== riSousMenu2 ========
      {
        riSousMenu2.setName("riSousMenu2");

        //---- riSousMenu_bt2 ----
        riSousMenu_bt2.setText("Param\u00e8tres de duplication");
        riSousMenu_bt2.setToolTipText("Param\u00e8tres de duplication");
        riSousMenu_bt2.setName("riSousMenu_bt2");
        riSousMenu_bt2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            riSousMenu_bt2ActionPerformed(e);
          }
        });
        riSousMenu2.add(riSousMenu_bt2);
      }
      menus_haut.add(riSousMenu2);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_18;
  private XRiTextField NOUCLI;
  private XRiTextField NOULIV;
  private XRiCheckBox TCL;
  private XRiCheckBox TCF;
  private XRiTextField NOMCLI;
  private JLabel OBJ_19;
  private XRiCheckBox ADSLIV;
  private XRiCheckBox ADSFAC;
  private XRiCheckBox DUPLOT;
  private XRiTextField NOUTFA;
  private JLabel OBJ_20;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JPanel menus_haut;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
