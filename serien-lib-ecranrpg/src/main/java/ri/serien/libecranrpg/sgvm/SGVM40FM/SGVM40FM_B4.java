
package ri.serien.libecranrpg.sgvm.SGVM40FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM511] Gestion des ventes -> Opérations périodiques -> Comptabilisation des ventes -> Comptabilisation régulière
 * Indicateur: 10000001
 * Titre: Comptabilisation régulière des ventes
 */
public class SGVM40FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM40FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    DEMGLO.setValeursSelection("X", " ");
    CPTSPE.setValeursSelection("X", " ");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Indicateurs
    Boolean isMagasin = lexique.isTrue("31");
    Boolean isCanalVente = lexique.isTrue("41");
    Boolean isComptabilisationClient = lexique.isTrue("95");
    
    // Visibilité des composants
    CPTSPE.setVisible(isComptabilisationClient);
    if (isMagasin) {
      lbCanalDeVente.setVisible(false);
      snCanalDeVente.setVisible(false);
      snMagasin.setVisible(true);
      lbMagasin.setVisible(true);
    }
    else if (isCanalVente) {
      lbCanalDeVente.setVisible(true);
      snCanalDeVente.setVisible(true);
      snMagasin.setVisible(false);
      lbMagasin.setVisible(false);
    }
    else {
      lbCanalDeVente.setVisible(false);
      snCanalDeVente.setVisible(false);
      snMagasin.setVisible(false);
      lbMagasin.setVisible(false);
    }
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // initialisation des composants
    chargerComposantMagasin();
    chargerComposantCanalDeVente();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snCanalDeVente.isVisible()) {
      snCanalDeVente.renseignerChampRPG(lexique, "MAGCV");
    }
    if (snMagasin.isVisible()) {
      snMagasin.renseignerChampRPG(lexique, "MAGCV");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btRechercheActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WETB");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * Initialise le composant magasin
   */
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "MAGCV");
  }
  
  /**
   * Initialise le composant canal de vente
   */
  private void chargerComposantCanalDeVente() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "MAGCV");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantMagasin();
      chargerComposantCanalDeVente();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbCanalDeVente = new SNLabelChamp();
    snCanalDeVente = new SNCanalDeVente();
    lbPeriodeATraiter = new SNLabelChamp();
    PERIOD = new XRiCalendrier();
    DEMGLO = new XRiCheckBox();
    CPTSPE = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    lbEtablissement = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Comptabilisation r\u00e9guli\u00e8re des ventes");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlCritereDeSelection ========
        {
          pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereDeSelection.setName("pnlCritereDeSelection");
          pnlCritereDeSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlCritereDeSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snMagasin ----
          snMagasin.setName("snMagasin");
          pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbCanalDeVente ----
          lbCanalDeVente.setText("Canal de vente");
          lbCanalDeVente.setName("lbCanalDeVente");
          pnlCritereDeSelection.add(lbCanalDeVente, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snCanalDeVente ----
          snCanalDeVente.setName("snCanalDeVente");
          pnlCritereDeSelection.add(snCanalDeVente, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeATraiter ----
          lbPeriodeATraiter.setText("P\u00e9riode \u00e0 traiter");
          lbPeriodeATraiter.setName("lbPeriodeATraiter");
          pnlCritereDeSelection.add(lbPeriodeATraiter, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- PERIOD ----
          PERIOD.setTypeSaisie(6);
          PERIOD.setMinimumSize(new Dimension(110, 30));
          PERIOD.setMaximumSize(new Dimension(120, 30));
          PERIOD.setPreferredSize(new Dimension(110, 30));
          PERIOD.setFont(new Font("sansserif", Font.PLAIN, 14));
          PERIOD.setName("PERIOD");
          pnlCritereDeSelection.add(PERIOD, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- DEMGLO ----
          DEMGLO.setText("Comptabilisation globale");
          DEMGLO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DEMGLO.setFont(new Font("sansserif", Font.PLAIN, 14));
          DEMGLO.setMinimumSize(new Dimension(177, 30));
          DEMGLO.setPreferredSize(new Dimension(177, 30));
          DEMGLO.setName("DEMGLO");
          pnlCritereDeSelection.add(DEMGLO, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CPTSPE ----
          CPTSPE.setText("Comptabilisation des clients sp\u00e9ciaux");
          CPTSPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CPTSPE.setFont(new Font("sansserif", Font.PLAIN, 14));
          CPTSPE.setPreferredSize(new Dimension(258, 30));
          CPTSPE.setMinimumSize(new Dimension(258, 30));
          CPTSPE.setMaximumSize(new Dimension(258, 30));
          CPTSPE.setName("CPTSPE");
          pnlCritereDeSelection.add(CPTSPE, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- tfEnCours ----
          tfEnCours.setText("@WENCX@");
          tfEnCours.setEnabled(false);
          tfEnCours.setMinimumSize(new Dimension(260, 30));
          tfEnCours.setMaximumSize(new Dimension(260, 30));
          tfEnCours.setPreferredSize(new Dimension(260, 30));
          tfEnCours.setName("tfEnCours");
          pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbCanalDeVente;
  private SNCanalDeVente snCanalDeVente;
  private SNLabelChamp lbPeriodeATraiter;
  private XRiCalendrier PERIOD;
  private XRiCheckBox DEMGLO;
  private XRiCheckBox CPTSPE;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNLabelChamp lbEtablissement;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
