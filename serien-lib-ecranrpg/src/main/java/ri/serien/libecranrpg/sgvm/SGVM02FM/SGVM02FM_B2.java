
package ri.serien.libecranrpg.sgvm.SGVM02FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM1732] Gestion des ventes -> Fiches permanentes -> Editions clients -> N'ayant pas acheté depuis... -> Par représentant
 * Indicateur:01000001
 * Titre:Liste des clients par représentant n'ayant pas acheté depuis le ...
 * 
 * [GVM1742] Gestion des ventes -> Fiches permanentes -> Editions clients -> Par date de dernière vente -> Par représentant
 * Indicateur:01100001
 * Titre:Liste des clients par représentant par date de dernière vente
 */
public class SGVM02FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private String BOUTON_EXPORTER = "Exporter";
  private Message LOCTP = null;
  
  public SGVM02FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    LISTE.setValeurs("1", " ");
    DESACT.setValeurs(" ", "1");
    REPON1.setValeursSelection("L07", "L08");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    if (!LISTE.isSelected() || !DESACT.isSelected()) {
      LISTE.setSelected(true);
      DESACT.setSelected(false);
    }
    
    // Indicateur
    Boolean isParDate = lexique.isTrue("93");
    
    // Titre
    if (isParDate) {
      bpPresentation.setText("Liste des clients par représentant par date de dernière vente");
    }
    else {
      bpPresentation.setText("Liste des clients par représentant n'ayant pas acheté depuis le ...");
    }
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Visibilité des composants
    lbWDAT.setVisible(lexique.isPresent("WDAT"));
    lbWDATR.setVisible(lexique.isPresent("WDATR"));
    lbRestiction.setVisible(lexique.isPresent("WDATR"));
    pnlOptionsEdition.setVisible(lexique.isPresent("LISTE") || lexique.isPresent("DESACT"));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charge les composants
    chargerComposantRepresentant();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Gestion de la sélection complète
    if (snRepresentantDebut.getIdSelection() == null && snRepresentantFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
      snRepresentantDebut.renseignerChampRPG(lexique, "DEBREP");
      snRepresentantFin.renseignerChampRPG(lexique, "FINREP");
    }
  }
  
  /**
   * Charge le composant representant
   */
  private void chargerComposantRepresentant() {
    snRepresentantDebut.setSession(getSession());
    snRepresentantDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantDebut.setTousAutorise(true);
    snRepresentantDebut.charger(false);
    snRepresentantDebut.setSelectionParChampRPG(lexique, "DEBREP");
    
    snRepresentantFin.setSession(getSession());
    snRepresentantFin.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantFin.setTousAutorise(true);
    snRepresentantFin.charger(false);
    snRepresentantFin.setSelectionParChampRPG(lexique, "FINREP");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
      chargerComposantRepresentant();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbWDAT = new SNLabelChamp();
    WDAT = new XRiCalendrier();
    lbWDATR = new SNLabelChamp();
    WDATR = new XRiCalendrier();
    lbRestiction = new SNLabelUnite();
    REPON1 = new XRiCheckBox();
    lbDEBREP = new SNLabelChamp();
    snRepresentantDebut = new SNRepresentant();
    lbFINREP = new SNLabelChamp();
    snRepresentantFin = new SNRepresentant();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionsEdition = new SNPanelTitre();
    LISTE = new XRiRadioButton();
    DESACT = new XRiRadioButton();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setOpaque(false);
        pnlMessage.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setPreferredSize(new Dimension(700, 520));
        pnlColonne.setBackground(new Color(239, 239, 222));
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbWDAT ----
            lbWDAT.setText("N'ayant pas achet\u00e9 depuis le");
            lbWDAT.setName("lbWDAT");
            pnlCritereDeSelection.add(lbWDAT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WDAT ----
            WDAT.setMinimumSize(new Dimension(110, 30));
            WDAT.setMaximumSize(new Dimension(105, 28));
            WDAT.setPreferredSize(new Dimension(110, 30));
            WDAT.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDAT.setName("WDAT");
            pnlCritereDeSelection.add(WDAT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbWDATR ----
            lbWDATR.setText("Omettre les ventes avant le");
            lbWDATR.setMinimumSize(new Dimension(200, 30));
            lbWDATR.setPreferredSize(new Dimension(200, 30));
            lbWDATR.setName("lbWDATR");
            pnlCritereDeSelection.add(lbWDATR, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WDATR ----
            WDATR.setPreferredSize(new Dimension(110, 30));
            WDATR.setMinimumSize(new Dimension(110, 30));
            WDATR.setMaximumSize(new Dimension(105, 28));
            WDATR.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDATR.setName("WDATR");
            pnlCritereDeSelection.add(WDATR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbRestiction ----
            lbRestiction.setText("(restriction facultative)");
            lbRestiction.setMinimumSize(new Dimension(150, 30));
            lbRestiction.setMaximumSize(new Dimension(150, 30));
            lbRestiction.setPreferredSize(new Dimension(150, 30));
            lbRestiction.setName("lbRestiction");
            pnlCritereDeSelection.add(lbRestiction, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON1 ----
            REPON1.setText("Edition de l'adresse compl\u00e8te");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setPreferredSize(new Dimension(208, 30));
            REPON1.setMinimumSize(new Dimension(208, 30));
            REPON1.setName("REPON1");
            pnlCritereDeSelection.add(REPON1, new GridBagConstraints(0, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDEBREP ----
            lbDEBREP.setText("Repr\u00e9sentant de d\u00e9but");
            lbDEBREP.setName("lbDEBREP");
            pnlCritereDeSelection.add(lbDEBREP, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snRepresentantDebut ----
            snRepresentantDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snRepresentantDebut.setName("snRepresentantDebut");
            pnlCritereDeSelection.add(snRepresentantDebut, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFINREP ----
            lbFINREP.setText("Repr\u00e9sentant de fin");
            lbFINREP.setName("lbFINREP");
            pnlCritereDeSelection.add(lbFINREP, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snRepresentantFin ----
            snRepresentantFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snRepresentantFin.setName("snRepresentantFin");
            pnlCritereDeSelection.add(snRepresentantFin, new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setMinimumSize(new Dimension(400, 30));
            snEtablissement.setPreferredSize(new Dimension(400, 30));
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setPreferredSize(new Dimension(220, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(220, 30));
            tfPeriodeEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionsEdition ========
          {
            pnlOptionsEdition.setTitre("Options d'\u00e9dition");
            pnlOptionsEdition.setName("pnlOptionsEdition");
            pnlOptionsEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- LISTE ----
            LISTE.setText("Liste seulement");
            LISTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LISTE.setFont(new Font("sansserif", Font.PLAIN, 14));
            LISTE.setMinimumSize(new Dimension(300, 30));
            LISTE.setMaximumSize(new Dimension(300, 28));
            LISTE.setPreferredSize(new Dimension(300, 30));
            LISTE.setName("LISTE");
            pnlOptionsEdition.add(LISTE, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- DESACT ----
            DESACT.setText("Liste et d\u00e9sactivation des clients");
            DESACT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DESACT.setFont(new Font("sansserif", Font.PLAIN, 14));
            DESACT.setPreferredSize(new Dimension(300, 30));
            DESACT.setMinimumSize(new Dimension(300, 30));
            DESACT.setName("DESACT");
            pnlOptionsEdition.add(DESACT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionsEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- btgGroupe ----
    ButtonGroup btgGroupe = new ButtonGroup();
    btgGroupe.add(LISTE);
    btgGroupe.add(DESACT);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbWDAT;
  private XRiCalendrier WDAT;
  private SNLabelChamp lbWDATR;
  private XRiCalendrier WDATR;
  private SNLabelUnite lbRestiction;
  private XRiCheckBox REPON1;
  private SNLabelChamp lbDEBREP;
  private SNRepresentant snRepresentantDebut;
  private SNLabelChamp lbFINREP;
  private SNRepresentant snRepresentantFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionsEdition;
  private XRiRadioButton LISTE;
  private XRiRadioButton DESACT;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
