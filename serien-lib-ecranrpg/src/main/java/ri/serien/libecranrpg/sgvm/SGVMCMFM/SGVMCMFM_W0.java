
package ri.serien.libecranrpg.sgvm.SGVMCMFM;
// Nom Fichier: pop_SGVMCMFM_FMTW0_FMTF1_1286.java

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVMCMFM_W0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMCMFM_W0(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_8 = new JLabel();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_8 ----
    OBJ_8.setText("S\u00e9lection en cours");
    OBJ_8.setName("OBJ_8");
    add(OBJ_8);
    OBJ_8.setBounds(44, 24, 131, 20);

    setPreferredSize(new Dimension(218, 62));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
