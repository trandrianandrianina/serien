
package ri.serien.libecranrpg.sgvm.SGVM3HFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SGVM3HFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM3HFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPON2.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    WTOUM.setValeursSelection("**", "  ");
    CLICPT.setValeursSelection("OUI", "NON");
    FACNRG.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    CLIFIN.setEnabled(lexique.isPresent("CLIFIN"));
    CLIDEB.setEnabled(lexique.isPresent("CLIDEB"));
    // DATFIN.setEnabled( lexique.isPresent("DATFIN"));
    // DATDEB.setEnabled( lexique.isPresent("DATDEB"));
    // REPON2.setVisible( lexique.isPresent("REPON2"));
    // REPON2.setEnabled( lexique.isPresent("REPON2"));
    // REPON2.setSelected(lexique.HostFieldGetData("REPON2").equalsIgnoreCase("OUI"));
    // REPON1.setVisible( lexique.isPresent("REPON1"));
    // REPON1.setEnabled( lexique.isPresent("REPON1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    // WTOUM.setVisible( lexique.isPresent("WTOUM"));
    // WTOUM.setEnabled( lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUM").equalsIgnoreCase(""));
    MA12.setEnabled(lexique.isPresent("MA12"));
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (REPON2.isSelected())
    // lexique.HostFieldPutData("REPON2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON2", 0, "NON");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    panel3 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_20 = new JXTitledSeparator();
    OBJ_26 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    WTOUM = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    REPON2 = new XRiCheckBox();
    OBJ_28 = new JLabel();
    OBJ_30 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    panel1 = new JPanel();
    OBJ_42 = new JLabel();
    CLIDEB = new XRiTextField();
    CLIFIN = new XRiTextField();
    OBJ_44 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    CLICPT = new XRiCheckBox();
    FACNRG = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== panel3 ========
      {
        panel3.setBorder(new CompoundBorder(
          null,
          new EmptyBorder(10, 10, 10, 10)));
        panel3.setBackground(new Color(239, 239, 222));
        panel3.setName("panel3");
        panel3.setLayout(new GridBagLayout());
        ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {25, 174, 52, 109, 0, 586, 0};
        ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- sep_etablissement ----
        sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
        sep_etablissement.setName("sep_etablissement");
        panel3.add(sep_etablissement, new GridBagConstraints(0, 0, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- z_dgnom_ ----
        z_dgnom_.setText("@DGNOM@");
        z_dgnom_.setPreferredSize(new Dimension(260, 24));
        z_dgnom_.setMinimumSize(new Dimension(260, 24));
        z_dgnom_.setMaximumSize(new Dimension(260, 24));
        z_dgnom_.setName("z_dgnom_");
        panel3.add(z_dgnom_, new GridBagConstraints(3, 1, 3, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- z_wencx_ ----
        z_wencx_.setText("@WENCX@");
        z_wencx_.setMinimumSize(new Dimension(260, 24));
        z_wencx_.setMaximumSize(new Dimension(260, 24));
        z_wencx_.setPreferredSize(new Dimension(260, 24));
        z_wencx_.setName("z_wencx_");
        panel3.add(z_wencx_, new GridBagConstraints(3, 2, 3, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- z_etablissement_ ----
        z_etablissement_.setComponentPopupMenu(null);
        z_etablissement_.setText("@WETB@");
        z_etablissement_.setMaximumSize(new Dimension(40, 30));
        z_etablissement_.setMinimumSize(new Dimension(40, 30));
        z_etablissement_.setPreferredSize(new Dimension(40, 30));
        z_etablissement_.setName("z_etablissement_");
        panel3.add(z_etablissement_, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- bouton_etablissement ----
        bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
        bouton_etablissement.setName("bouton_etablissement");
        bouton_etablissement.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bouton_etablissementActionPerformed(e);
          }
        });
        panel3.add(bouton_etablissement, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- OBJ_20 ----
        OBJ_20.setTitle("Code(s) magasin(s) \u00e0 traiter");
        OBJ_20.setName("OBJ_20");
        panel3.add(OBJ_20, new GridBagConstraints(0, 5, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- OBJ_26 ----
        OBJ_26.setTitle("Plage de clients \u00e0 traiter");
        OBJ_26.setName("OBJ_26");
        panel3.add(OBJ_26, new GridBagConstraints(0, 8, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== P_SEL0 ========
        {
          P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
          P_SEL0.setOpaque(false);
          P_SEL0.setPreferredSize(new Dimension(460, 50));
          P_SEL0.setMinimumSize(new Dimension(460, 50));
          P_SEL0.setName("P_SEL0");
          P_SEL0.setLayout(null);

          //---- MA01 ----
          MA01.setComponentPopupMenu(BTD);
          MA01.setName("MA01");
          P_SEL0.add(MA01);
          MA01.setBounds(15, 10, 34, MA01.getPreferredSize().height);

          //---- MA02 ----
          MA02.setComponentPopupMenu(BTD);
          MA02.setName("MA02");
          P_SEL0.add(MA02);
          MA02.setBounds(51, 10, 34, MA02.getPreferredSize().height);

          //---- MA03 ----
          MA03.setComponentPopupMenu(BTD);
          MA03.setName("MA03");
          P_SEL0.add(MA03);
          MA03.setBounds(87, 10, 34, MA03.getPreferredSize().height);

          //---- MA04 ----
          MA04.setComponentPopupMenu(BTD);
          MA04.setName("MA04");
          P_SEL0.add(MA04);
          MA04.setBounds(123, 10, 34, MA04.getPreferredSize().height);

          //---- MA05 ----
          MA05.setComponentPopupMenu(BTD);
          MA05.setName("MA05");
          P_SEL0.add(MA05);
          MA05.setBounds(159, 10, 34, MA05.getPreferredSize().height);

          //---- MA06 ----
          MA06.setComponentPopupMenu(BTD);
          MA06.setName("MA06");
          P_SEL0.add(MA06);
          MA06.setBounds(195, 10, 34, MA06.getPreferredSize().height);

          //---- MA07 ----
          MA07.setComponentPopupMenu(BTD);
          MA07.setName("MA07");
          P_SEL0.add(MA07);
          MA07.setBounds(231, 10, 34, MA07.getPreferredSize().height);

          //---- MA08 ----
          MA08.setComponentPopupMenu(BTD);
          MA08.setName("MA08");
          P_SEL0.add(MA08);
          MA08.setBounds(267, 10, 34, MA08.getPreferredSize().height);

          //---- MA09 ----
          MA09.setComponentPopupMenu(BTD);
          MA09.setName("MA09");
          P_SEL0.add(MA09);
          MA09.setBounds(303, 10, 34, MA09.getPreferredSize().height);

          //---- MA10 ----
          MA10.setComponentPopupMenu(BTD);
          MA10.setName("MA10");
          P_SEL0.add(MA10);
          MA10.setBounds(339, 10, 34, MA10.getPreferredSize().height);

          //---- MA11 ----
          MA11.setComponentPopupMenu(BTD);
          MA11.setName("MA11");
          P_SEL0.add(MA11);
          MA11.setBounds(375, 10, 34, MA11.getPreferredSize().height);

          //---- MA12 ----
          MA12.setComponentPopupMenu(BTD);
          MA12.setName("MA12");
          P_SEL0.add(MA12);
          MA12.setBounds(411, 10, 34, MA12.getPreferredSize().height);
        }
        panel3.add(P_SEL0, new GridBagConstraints(3, 6, 3, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- WTOUM ----
        WTOUM.setText("S\u00e9lection compl\u00e8te");
        WTOUM.setComponentPopupMenu(BTD);
        WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WTOUM.setMinimumSize(new Dimension(135, 30));
        WTOUM.setMaximumSize(new Dimension(135, 30));
        WTOUM.setPreferredSize(new Dimension(135, 30));
        WTOUM.setName("WTOUM");
        WTOUM.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            WTOUMActionPerformed(e);
          }
        });
        panel3.add(WTOUM, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- REPON1 ----
        REPON1.setText("Trier par magasin");
        REPON1.setComponentPopupMenu(BTD);
        REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        REPON1.setPreferredSize(new Dimension(135, 30));
        REPON1.setMaximumSize(new Dimension(135, 30));
        REPON1.setMinimumSize(new Dimension(135, 30));
        REPON1.setName("REPON1");
        panel3.add(REPON1, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- REPON2 ----
        REPON2.setText("Trier par client");
        REPON2.setComponentPopupMenu(BTD);
        REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        REPON2.setPreferredSize(new Dimension(135, 30));
        REPON2.setMinimumSize(new Dimension(135, 30));
        REPON2.setMaximumSize(new Dimension(135, 30));
        REPON2.setName("REPON2");
        panel3.add(REPON2, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- OBJ_28 ----
        OBJ_28.setText("Du");
        OBJ_28.setPreferredSize(new Dimension(135, 30));
        OBJ_28.setMinimumSize(new Dimension(135, 30));
        OBJ_28.setMaximumSize(new Dimension(135, 30));
        OBJ_28.setHorizontalTextPosition(SwingConstants.RIGHT);
        OBJ_28.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_28.setName("OBJ_28");
        panel3.add(OBJ_28, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- OBJ_30 ----
        OBJ_30.setText("au");
        OBJ_30.setPreferredSize(new Dimension(50, 30));
        OBJ_30.setMinimumSize(new Dimension(50, 30));
        OBJ_30.setMaximumSize(new Dimension(50, 30));
        OBJ_30.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_30.setName("OBJ_30");
        panel3.add(OBJ_30, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("P\u00e9riode \u00e0 \u00e9diter");
        xTitledSeparator1.setName("xTitledSeparator1");
        panel3.add(xTitledSeparator1, new GridBagConstraints(0, 3, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== panel1 ========
        {
          panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
          panel1.setOpaque(false);
          panel1.setPreferredSize(new Dimension(250, 90));
          panel1.setMinimumSize(new Dimension(250, 90));
          panel1.setMaximumSize(new Dimension(250, 90));
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Code client de d\u00e9but");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(20, 14, 126, 20);

          //---- CLIDEB ----
          CLIDEB.setComponentPopupMenu(BTD);
          CLIDEB.setName("CLIDEB");
          panel1.add(CLIDEB);
          CLIDEB.setBounds(160, 10, 60, CLIDEB.getPreferredSize().height);

          //---- CLIFIN ----
          CLIFIN.setComponentPopupMenu(BTD);
          CLIFIN.setName("CLIFIN");
          panel1.add(CLIFIN);
          CLIFIN.setBounds(160, 40, 60, CLIFIN.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Code client de fin");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(20, 44, 105, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        panel3.add(panel1, new GridBagConstraints(3, 9, 3, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- DATDEB ----
        DATDEB.setPreferredSize(new Dimension(105, 28));
        DATDEB.setMinimumSize(new Dimension(105, 28));
        DATDEB.setMaximumSize(new Dimension(105, 28));
        DATDEB.setName("DATDEB");
        panel3.add(DATDEB, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DATFIN ----
        DATFIN.setMinimumSize(new Dimension(105, 28));
        DATFIN.setPreferredSize(new Dimension(105, 28));
        DATFIN.setMaximumSize(new Dimension(105, 28));
        DATFIN.setName("DATFIN");
        panel3.add(DATFIN, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- CLICPT ----
        CLICPT.setText("Clients comptants uniquement");
        CLICPT.setComponentPopupMenu(BTD);
        CLICPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        CLICPT.setPreferredSize(new Dimension(320, 30));
        CLICPT.setMinimumSize(new Dimension(320, 30));
        CLICPT.setMaximumSize(new Dimension(320, 30));
        CLICPT.setName("CLICPT");
        panel3.add(CLICPT, new GridBagConstraints(1, 10, 5, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- FACNRG ----
        FACNRG.setText("Factures non r\u00e9gl\u00e9es uniquement");
        FACNRG.setComponentPopupMenu(BTD);
        FACNRG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        FACNRG.setPreferredSize(new Dimension(320, 30));
        FACNRG.setMinimumSize(new Dimension(320, 30));
        FACNRG.setMaximumSize(new Dimension(320, 30));
        FACNRG.setName("FACNRG");
        panel3.add(FACNRG, new GridBagConstraints(1, 11, 5, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(panel3, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel panel3;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_20;
  private JXTitledSeparator OBJ_26;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private XRiCheckBox WTOUM;
  private XRiCheckBox REPON1;
  private XRiCheckBox REPON2;
  private JLabel OBJ_28;
  private JLabel OBJ_30;
  private JXTitledSeparator xTitledSeparator1;
  private JPanel panel1;
  private JLabel OBJ_42;
  private XRiTextField CLIDEB;
  private XRiTextField CLIFIN;
  private JLabel OBJ_44;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private XRiCheckBox CLICPT;
  private XRiCheckBox FACNRG;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
