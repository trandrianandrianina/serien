
package ri.serien.libecranrpg.sgvm.SGVM40FM;
// Nom Fichier: pop_SGVM40FM_FMTP7_FMTF1_553.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM40FM_P7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM40FM_P7(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void BT_ENTERActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_6 = new JLabel();
    OBJ_5 = new JLabel();
    P_PnlOpts = new JPanel();
    BT_ENTER = new JButton();

    //======== this ========
    setPreferredSize(new Dimension(360, 115));
    setName("this");

    //---- OBJ_6 ----
    OBJ_6.setText("Aucune facture comptabilis\u00e9e");
    OBJ_6.setName("OBJ_6");

    //---- OBJ_5 ----
    OBJ_5.setText("Traitement sans objet");
    OBJ_5.setName("OBJ_5");

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ENTERActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(80, 80, 80)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
          .addGap(5, 5, 5)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
          .addGap(715, 715, 715)
          .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(19, 19, 19)
          .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(70, 70, 70)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 516, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_6;
  private JLabel OBJ_5;
  private JPanel P_PnlOpts;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
