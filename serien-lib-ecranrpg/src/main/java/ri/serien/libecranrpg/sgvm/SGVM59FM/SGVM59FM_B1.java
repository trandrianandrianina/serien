/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvm.SGVM59FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Ecran RPG du point de menu [GVM2B1].
 * [GVM2B1] Gestion des ventes -> Documents de ventes -> Traitements commandes -> Global (expédition/facture/édition)
 */
public class SGVM59FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] CODEXP_Value = { "TRC", "TRB", "NON", };
  private Message LOCTP = null;
  
  public SGVM59FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    OPT3.setValeurs("3", "RB");
    OPT2.setValeurs("2", "RB");
    OPT1.setValeurs("1", "RB");
    CODEXP.setValeurs(CODEXP_Value, null);
    EXCCEE.setValeursSelection("OUI", "NON");
    EXCEXP.setValeursSelection("OUI", "NON");
    EXCOM.setValeursSelection("OUI", "NON");
    CODBPR.setValeursSelection("OUI", "NON");
    CODEXN.setValeursSelection("OUI", "NON");
    DUPCOM.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    pnlExclusion.setVisible(lexique.isPresent("EXCOM"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    rafraichirMagasin();
    rafraichirRepresentant();
    rafraichirVendeur();
    rafraichirTransporteur();
    rafraichirZoneGeographique();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "MAGTRT");
    snRepresentant.renseignerChampRPG(lexique, "REPTRT");
    snTransporteur.renseignerChampRPG(lexique, "CTRTRT");
    snVendeur.renseignerChampRPG(lexique, "VDETRT");
    snZoneGeographique.renseignerChampRPG(lexique, "ZGETRT");
  }
  
  /**
   * Initialise le composant magasin suivant l'etablissement
   */
  private void rafraichirMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "MAGTRT");
  }
  
  /**
   * Initialise le composant representant suivant l'etablissement
   */
  private void rafraichirRepresentant() {
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "REPTRT");
  }
  
  /**
   * Initialise le composant transporteur suivant l'etablissement
   */
  private void rafraichirTransporteur() {
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(snEtablissement.getIdSelection());
    snTransporteur.setTousAutorise(true);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "CTRTRT");
  }
  
  /**
   * Initialise le composant vendeur suivant l'etablissement
   */
  private void rafraichirVendeur() {
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "VDETRT");
  }
  
  /**
   * Initialise le composante Zone geographique suivant l'etablissement
   */
  private void rafraichirZoneGeographique() {
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(snEtablissement.getIdSelection());
    snZoneGeographique.setTousAutorise(true);
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelectionParChampRPG(lexique, "ZGETRT");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    lexique.WatchHelp(pmBTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbRepresentant = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    lbZoneGeographique = new SNLabelChamp();
    snZoneGeographique = new SNZoneGeographique();
    lbDateLivraison = new SNLabelChamp();
    DATLIV = new XRiCalendrier();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    pnlTypeCommande = new SNPanelTitre();
    lbDateTraitement = new SNLabelChamp();
    DATTRT = new XRiCalendrier();
    DUPCOM = new XRiCheckBox();
    lbTraitement = new SNLabelChamp();
    OPT1 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    lbBonExpedition = new SNLabelChamp();
    CODEXP = new XRiComboBox();
    CODEXN = new XRiCheckBox();
    CODBPR = new XRiCheckBox();
    pnlExclusion = new SNPanelTitre();
    EXCOM = new XRiCheckBox();
    EXCEXP = new XRiCheckBox();
    EXCCEE = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 22));
        lbLOCTP.setPreferredSize(new Dimension(120, 22));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection des commandes");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setPreferredSize(new Dimension(200, 30));
            lbMagasin.setMinimumSize(new Dimension(200, 30));
            lbMagasin.setMaximumSize(new Dimension(200, 30));
            lbMagasin.setName("lbMagasin");
            pnlCritereSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setName("snMagasin");
            pnlCritereSelection.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbRepresentant ----
            lbRepresentant.setText("Repr\u00e9sentant");
            lbRepresentant.setPreferredSize(new Dimension(200, 30));
            lbRepresentant.setMinimumSize(new Dimension(200, 30));
            lbRepresentant.setMaximumSize(new Dimension(200, 30));
            lbRepresentant.setName("lbRepresentant");
            pnlCritereSelection.add(lbRepresentant, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snRepresentant ----
            snRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
            snRepresentant.setName("snRepresentant");
            pnlCritereSelection.add(snRepresentant, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbZoneGeographique ----
            lbZoneGeographique.setText("Zone g\u00e9ographique");
            lbZoneGeographique.setPreferredSize(new Dimension(200, 30));
            lbZoneGeographique.setMinimumSize(new Dimension(200, 30));
            lbZoneGeographique.setMaximumSize(new Dimension(200, 30));
            lbZoneGeographique.setName("lbZoneGeographique");
            pnlCritereSelection.add(lbZoneGeographique, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snZoneGeographique ----
            snZoneGeographique.setName("snZoneGeographique");
            pnlCritereSelection.add(snZoneGeographique, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDateLivraison ----
            lbDateLivraison.setText("Date livraison pr\u00e9vue maximale");
            lbDateLivraison.setPreferredSize(new Dimension(200, 30));
            lbDateLivraison.setMinimumSize(new Dimension(200, 30));
            lbDateLivraison.setMaximumSize(new Dimension(200, 30));
            lbDateLivraison.setName("lbDateLivraison");
            pnlCritereSelection.add(lbDateLivraison, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DATLIV ----
            DATLIV.setComponentPopupMenu(null);
            DATLIV.setPreferredSize(new Dimension(110, 30));
            DATLIV.setMinimumSize(new Dimension(110, 30));
            DATLIV.setMaximumSize(new Dimension(110, 30));
            DATLIV.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATLIV.setName("DATLIV");
            pnlCritereSelection.add(DATLIV, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTransporteur ----
            lbTransporteur.setText("Transporteur");
            lbTransporteur.setPreferredSize(new Dimension(200, 30));
            lbTransporteur.setMinimumSize(new Dimension(200, 30));
            lbTransporteur.setMaximumSize(new Dimension(200, 30));
            lbTransporteur.setName("lbTransporteur");
            pnlCritereSelection.add(lbTransporteur, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snTransporteur ----
            snTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
            snTransporteur.setName("snTransporteur");
            pnlCritereSelection.add(snTransporteur, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVendeur ----
            lbVendeur.setText("Vendeur");
            lbVendeur.setPreferredSize(new Dimension(200, 30));
            lbVendeur.setMinimumSize(new Dimension(200, 30));
            lbVendeur.setMaximumSize(new Dimension(200, 30));
            lbVendeur.setName("lbVendeur");
            pnlCritereSelection.add(lbVendeur, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snVendeur ----
            snVendeur.setFont(new Font("sansserif", Font.PLAIN, 14));
            snVendeur.setName("snVendeur");
            pnlCritereSelection.add(snVendeur, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTypeCommande ========
          {
            pnlTypeCommande.setTitre("Traitement");
            pnlTypeCommande.setName("pnlTypeCommande");
            pnlTypeCommande.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTypeCommande.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTypeCommande.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTypeCommande.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTypeCommande.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateTraitement ----
            lbDateTraitement.setText("Date de traitement");
            lbDateTraitement.setName("lbDateTraitement");
            pnlTypeCommande.add(lbDateTraitement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DATTRT ----
            DATTRT.setComponentPopupMenu(null);
            DATTRT.setPreferredSize(new Dimension(110, 30));
            DATTRT.setMinimumSize(new Dimension(110, 30));
            DATTRT.setMaximumSize(new Dimension(110, 30));
            DATTRT.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATTRT.setName("DATTRT");
            pnlTypeCommande.add(DATTRT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- DUPCOM ----
            DUPCOM.setText("Dupliquer les commentaires lors des extractions");
            DUPCOM.setComponentPopupMenu(null);
            DUPCOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DUPCOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            DUPCOM.setMinimumSize(new Dimension(325, 30));
            DUPCOM.setPreferredSize(new Dimension(325, 30));
            DUPCOM.setMaximumSize(new Dimension(325, 30));
            DUPCOM.setName("DUPCOM");
            pnlTypeCommande.add(DUPCOM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTraitement ----
            lbTraitement.setText("Traitement \u00e0 effectuer");
            lbTraitement.setName("lbTraitement");
            pnlTypeCommande.add(lbTraitement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OPT1 ----
            OPT1.setText("Exp\u00e9dition");
            OPT1.setComponentPopupMenu(null);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setMaximumSize(new Dimension(250, 30));
            OPT1.setMinimumSize(new Dimension(250, 30));
            OPT1.setPreferredSize(new Dimension(250, 30));
            OPT1.setName("OPT1");
            pnlTypeCommande.add(OPT1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Exp\u00e9dition et facturation");
            OPT2.setComponentPopupMenu(null);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setMaximumSize(new Dimension(250, 30));
            OPT2.setMinimumSize(new Dimension(250, 30));
            OPT2.setPreferredSize(new Dimension(250, 30));
            OPT2.setName("OPT2");
            pnlTypeCommande.add(OPT2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Exp\u00e9dition, facturation et \u00e9dition des factures");
            OPT3.setComponentPopupMenu(null);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setMaximumSize(new Dimension(250, 30));
            OPT3.setMinimumSize(new Dimension(250, 30));
            OPT3.setPreferredSize(new Dimension(250, 30));
            OPT3.setName("OPT3");
            pnlTypeCommande.add(OPT3, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlTypeCommande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setEnabled(false);
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Options d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbBonExpedition ----
            lbBonExpedition.setText("Ordre de tri");
            lbBonExpedition.setPreferredSize(new Dimension(160, 30));
            lbBonExpedition.setMinimumSize(new Dimension(160, 30));
            lbBonExpedition.setMaximumSize(new Dimension(160, 30));
            lbBonExpedition.setName("lbBonExpedition");
            pnlOptionEdition.add(lbBonExpedition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- CODEXP ----
            CODEXP.setModel(new DefaultComboBoxModel(new String[] { "Tri\u00e9 par client", "Tri\u00e9 par bon", "Aucun tri" }));
            CODEXP.setComponentPopupMenu(null);
            CODEXP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CODEXP.setMinimumSize(new Dimension(225, 30));
            CODEXP.setMaximumSize(new Dimension(225, 30));
            CODEXP.setPreferredSize(new Dimension(225, 30));
            CODEXP.setFont(new Font("sansserif", Font.PLAIN, 14));
            CODEXP.setName("CODEXP");
            pnlOptionEdition.add(CODEXP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CODEXN ----
            CODEXN.setText("Ne pas chiffrer les bons d'exp\u00e9dition");
            CODEXN.setComponentPopupMenu(null);
            CODEXN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CODEXN.setMinimumSize(new Dimension(225, 30));
            CODEXN.setMaximumSize(new Dimension(225, 30));
            CODEXN.setPreferredSize(new Dimension(225, 30));
            CODEXN.setFont(new Font("sansserif", Font.PLAIN, 14));
            CODEXN.setName("CODEXN");
            pnlOptionEdition.add(CODEXN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CODBPR ----
            CODBPR.setText("Editer les bons de pr\u00e9paration");
            CODBPR.setComponentPopupMenu(null);
            CODBPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CODBPR.setMinimumSize(new Dimension(225, 30));
            CODBPR.setMaximumSize(new Dimension(225, 30));
            CODBPR.setPreferredSize(new Dimension(225, 30));
            CODBPR.setFont(new Font("sansserif", Font.PLAIN, 14));
            CODBPR.setName("CODBPR");
            pnlOptionEdition.add(CODBPR, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlExclusion ========
          {
            pnlExclusion.setTitre("Exclusions");
            pnlExclusion.setName("pnlExclusion");
            pnlExclusion.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlExclusion.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlExclusion.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlExclusion.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlExclusion.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- EXCOM ----
            EXCOM.setText("Des d\u00e9partements d'Outre-Mer");
            EXCOM.setComponentPopupMenu(null);
            EXCOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EXCOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            EXCOM.setPreferredSize(new Dimension(193, 30));
            EXCOM.setMinimumSize(new Dimension(193, 30));
            EXCOM.setName("EXCOM");
            pnlExclusion.add(EXCOM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EXCEXP ----
            EXCEXP.setText("Des bons de type export");
            EXCEXP.setComponentPopupMenu(null);
            EXCEXP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EXCEXP.setFont(new Font("sansserif", Font.PLAIN, 14));
            EXCEXP.setPreferredSize(new Dimension(193, 30));
            EXCEXP.setMinimumSize(new Dimension(193, 30));
            EXCEXP.setName("EXCEXP");
            pnlExclusion.add(EXCEXP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EXCCEE ----
            EXCCEE.setText("Des bons de type CEE");
            EXCCEE.setComponentPopupMenu(null);
            EXCCEE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EXCCEE.setFont(new Font("sansserif", Font.PLAIN, 14));
            EXCCEE.setPreferredSize(new Dimension(193, 30));
            EXCCEE.setMinimumSize(new Dimension(193, 30));
            EXCCEE.setName("EXCCEE");
            pnlExclusion.add(EXCCEE, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlExclusion, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    
    // ---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(OPT1);
    RB_GRP.add(OPT2);
    RB_GRP.add(OPT3);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbZoneGeographique;
  private SNZoneGeographique snZoneGeographique;
  private SNLabelChamp lbDateLivraison;
  private XRiCalendrier DATLIV;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private SNPanelTitre pnlTypeCommande;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier DATTRT;
  private XRiCheckBox DUPCOM;
  private SNLabelChamp lbTraitement;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionEdition;
  private SNLabelChamp lbBonExpedition;
  private XRiComboBox CODEXP;
  private XRiCheckBox CODEXN;
  private XRiCheckBox CODBPR;
  private SNPanelTitre pnlExclusion;
  private XRiCheckBox EXCOM;
  private XRiCheckBox EXCEXP;
  private XRiCheckBox EXCCEE;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
