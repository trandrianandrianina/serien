
package ri.serien.libecranrpg.sgvm.SGVMEDFM;
// Nom Fichier: pop_SGVMEDFM_FMTPC_FMTF1_994.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVMEDFM_PC extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMEDFM_PC(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_4 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_5 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_4 ----
    OBJ_4.setText("Traitement PC en cours ...");
    OBJ_4.setName("OBJ_4");
    add(OBJ_4);
    OBJ_4.setBounds(115, 30, 154, 20);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }
    add(P_PnlOpts);
    P_PnlOpts.setBounds(1036, 15, 55, 516);

    //---- OBJ_5 ----
    OBJ_5.setText("");
    OBJ_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(25, 20, 40, 40);

    setPreferredSize(new Dimension(305, 80));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_4;
  private JPanel P_PnlOpts;
  private JButton OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
