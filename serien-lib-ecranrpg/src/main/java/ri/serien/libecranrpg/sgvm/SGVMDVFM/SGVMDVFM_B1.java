
package ri.serien.libecranrpg.sgvm.SGVMDVFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class SGVMDVFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMDVFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IBON@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_8.setVisible(lexique.isPresent("IBON"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Dévérouillage"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("CONF", 0, "L07");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_9 = new JLabel();
    OBJ_7 = new JLabel();
    OBJ_10 = new JLabel();
    OBJ_11 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_8 = new RiZoneSortie();
    OBJ_13 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(715, 265));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("D\u00e9verrouillage d'un document"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(new GridBagLayout());
          ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {320, 0, 0};
          ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {31, 35, 45, 30, 30, 25, 0};
          ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- OBJ_9 ----
          OBJ_9.setText("Attention");
          OBJ_9.setForeground(new Color(255, 0, 51));
          OBJ_9.setFont(OBJ_9.getFont().deriveFont(OBJ_9.getFont().getStyle() | Font.BOLD, OBJ_9.getFont().getSize() + 2f));
          OBJ_9.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_9.setName("OBJ_9");
          panel2.add(OBJ_9, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_7 ----
          OBJ_7.setText("Vous vous appr\u00eatez \u00e0 d\u00e9verrouiller le document de vente");
          OBJ_7.setName("OBJ_7");
          panel2.add(OBJ_7, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_10 ----
          OBJ_10.setText("Ceci est une op\u00e9ration grave.");
          OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD));
          OBJ_10.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_10.setName("OBJ_10");
          panel2.add(OBJ_10, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_11 ----
          OBJ_11.setText("Ce document de vente est peut-\u00eatre en cours de traitement sur un autre \u00e9cran. ");
          OBJ_11.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_11.setName("OBJ_11");
          panel2.add(OBJ_11, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_12 ----
          OBJ_12.setText("Dans ce cas cliquez sur 'Retour' et attendez que le document soit lib\u00e9r\u00e9. ");
          OBJ_12.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_12.setName("OBJ_12");
          panel2.add(OBJ_12, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_8 ----
          OBJ_8.setText("@IBON@");
          OBJ_8.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_8.setFont(OBJ_8.getFont().deriveFont(OBJ_8.getFont().getStyle() | Font.BOLD));
          OBJ_8.setName("OBJ_8");
          panel2.add(OBJ_8, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_13 ----
          OBJ_13.setText("En cas de doute, contactez votre assistance t\u00e9l\u00e9phonique");
          OBJ_13.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_13.setName("OBJ_13");
          panel2.add(OBJ_13, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_9;
  private JLabel OBJ_7;
  private JLabel OBJ_10;
  private JLabel OBJ_11;
  private JLabel OBJ_12;
  private RiZoneSortie OBJ_8;
  private JLabel OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
