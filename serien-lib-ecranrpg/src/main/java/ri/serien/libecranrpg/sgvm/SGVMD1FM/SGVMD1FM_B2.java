
package ri.serien.libecranrpg.sgvm.SGVMD1FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMD1FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public SGVMD1FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    pbPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEtablissement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Message d'erreur
    if (lexique.HostFieldGetData("NBADIF").trim().equals("1")) {
      lbMessageGeneration.setMessage(Message.getMessageImportant(lexique.HostFieldGetData("NBADIF")
          + " achat différé n'a pas été généré (la liste est générée dans le fichier tableur 'achats_differes_non_generes.csv')"));
    }
    else {
      lbMessageGeneration.setMessage(Message.getMessageImportant(lexique.HostFieldGetData("NBADIF")
          + " achats différés n'ont pas été générés (la liste est générée dans le fichier tableur 'achats_differes_non_generes.csv')"));
    }
    
    // Choix d'option
    rbAbandonner.setSelected(lexique.HostFieldGetData("OADIF").trim().equalsIgnoreCase("1"));
    rbGenererSansDifferes.setSelected(lexique.HostFieldGetData("OADIF").trim().equalsIgnoreCase("2"));
    rbGenererAvecDifferes.setSelected(lexique.HostFieldGetData("OADIF").trim().equalsIgnoreCase("3"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@"));
    
    pbPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snFournisseur.renseignerChampRPG(lexique, "WCOL", "WFRS");
    
    if (rbAbandonner.isSelected()) {
      lexique.HostFieldPutData("OADIF", 0, "1");
    }
    else if (rbGenererSansDifferes.isSelected()) {
      lexique.HostFieldPutData("OADIF", 0, "2");
    }
    else if (rbGenererAvecDifferes.isSelected()) {
      lexique.HostFieldPutData("OADIF", 0, "3");
    }
    else {
      lexique.HostFieldPutData("OADIF", 0, "");
    }
    
  }
  
  /**
   * Charge le composant fournisseur
   */
  private void chargerFournisseur() {
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(true);
    snFournisseur.setSelectionParChampRPG(lexique, "WCOL", "WFRS");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerFournisseur();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void selectionCompleteActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void calendrierActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    pbPresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCriteresSelection = new SNPanelTitre();
    lbPeriode = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    DATDEB = new XRiCalendrier();
    lbAuPeriode = new SNLabelChamp();
    DATFIN = new XRiCalendrier();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEtablissement = new SNTexte();
    pnlMessageErreur = new SNPanel();
    lbMessageGeneration = new SNMessage();
    lbQuestion = new SNLabelUnite();
    rbAbandonner = new SNRadioButton();
    rbGenererSansDifferes = new SNRadioButton();
    rbGenererAvecDifferes = new SNRadioButton();
    OADIF_group = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- pbPresentation ----
      pbPresentation.setText("@TITPG1@ @TITPG2@");
      pbPresentation.setName("pbPresentation");
      p_nord.add(pbPresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCriteresSelection ========
          {
            pnlCriteresSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresSelection.setName("pnlCriteresSelection");
            pnlCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode \u00e0 traiter");
            lbPeriode.setMaximumSize(new Dimension(200, 30));
            lbPeriode.setPreferredSize(new Dimension(190, 30));
            lbPeriode.setMinimumSize(new Dimension(190, 30));
            lbPeriode.setName("lbPeriode");
            pnlCriteresSelection.add(lbPeriode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- DATDEB ----
              DATDEB.setPreferredSize(new Dimension(110, 30));
              DATDEB.setMinimumSize(new Dimension(110, 30));
              DATDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATDEB.setName("DATDEB");
              DATDEB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  calendrierActionPerformed(e);
                }
              });
              pnlPeriodeAEditer.add(DATDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAuPeriode ----
              lbAuPeriode.setText("au");
              lbAuPeriode.setPreferredSize(new Dimension(16, 30));
              lbAuPeriode.setMaximumSize(new Dimension(16, 30));
              lbAuPeriode.setMinimumSize(new Dimension(16, 30));
              lbAuPeriode.setName("lbAuPeriode");
              pnlPeriodeAEditer.add(lbAuPeriode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DATFIN ----
              DATFIN.setMinimumSize(new Dimension(110, 30));
              DATFIN.setPreferredSize(new Dimension(110, 30));
              DATFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATFIN.setName("DATFIN");
              DATFIN.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  calendrierActionPerformed(e);
                }
              });
              pnlPeriodeAEditer.add(DATFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setPreferredSize(new Dimension(190, 30));
            lbFournisseur.setMinimumSize(new Dimension(190, 30));
            lbFournisseur.setMaximumSize(new Dimension(200, 30));
            lbFournisseur.setName("lbFournisseur");
            pnlCriteresSelection.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snFournisseur ----
            snFournisseur.setName("snFournisseur");
            pnlCriteresSelection.add(snFournisseur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionne ========
          {
            pnlEtablissementSelectionne.setTitre("Etablissement");
            pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
            pnlEtablissementSelectionne.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionne.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfDateEtablissement ----
            tfDateEtablissement.setText("@WENCX@");
            tfDateEtablissement.setMaximumSize(new Dimension(272, 20));
            tfDateEtablissement.setEditable(false);
            tfDateEtablissement.setPreferredSize(new Dimension(272, 30));
            tfDateEtablissement.setMinimumSize(new Dimension(272, 30));
            tfDateEtablissement.setEnabled(false);
            tfDateEtablissement.setName("tfDateEtablissement");
            pnlEtablissementSelectionne.add(tfDateEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
      
      // ======== pnlMessageErreur ========
      {
        pnlMessageErreur.setName("pnlMessageErreur");
        pnlMessageErreur.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessageErreur.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessageErreur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlMessageErreur.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessageErreur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbMessageGeneration ----
        lbMessageGeneration.setText("xxx achats diff\u00e9r\u00e9s n'ont pas \u00e9t\u00e9 g\u00e9n\u00e9r\u00e9s");
        lbMessageGeneration.setName("lbMessageGeneration");
        pnlMessageErreur.add(lbMessageGeneration, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbQuestion ----
        lbQuestion.setText("Que souhaitez vous faire ?");
        lbQuestion.setName("lbQuestion");
        pnlMessageErreur.add(lbQuestion, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbAbandonner ----
        rbAbandonner.setText("Abandonner la g\u00e9n\u00e9ration des avoirs");
        rbAbandonner.setName("rbAbandonner");
        pnlMessageErreur.add(rbAbandonner, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbGenererSansDifferes ----
        rbGenererSansDifferes.setText("G\u00e9n\u00e9rer les avoirs sans prendre en compte les achats diff\u00e9r\u00e9s");
        rbGenererSansDifferes.setName("rbGenererSansDifferes");
        pnlMessageErreur.add(rbGenererSansDifferes, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbGenererAvecDifferes ----
        rbGenererAvecDifferes.setText(
            "G\u00e9n\u00e9rer les avoirs pour toutes les d\u00e9rogations en cours y compris ces achats diff\u00e9r\u00e9s qui ne seront donc pas g\u00e9n\u00e9r\u00e9s");
        rbGenererAvecDifferes.setName("rbGenererAvecDifferes");
        pnlMessageErreur.add(rbGenererAvecDifferes, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessageErreur, BorderLayout.SOUTH);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- OADIF_group ----
    OADIF_group.add(rbAbandonner);
    OADIF_group.add(rbGenererSansDifferes);
    OADIF_group.add(rbGenererAvecDifferes);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre pbPresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresSelection;
  private SNLabelChamp lbPeriode;
  private SNPanel pnlPeriodeAEditer;
  private XRiCalendrier DATDEB;
  private SNLabelChamp lbAuPeriode;
  private XRiCalendrier DATFIN;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEtablissement;
  private SNPanel pnlMessageErreur;
  private SNMessage lbMessageGeneration;
  private SNLabelUnite lbQuestion;
  private SNRadioButton rbAbandonner;
  private SNRadioButton rbGenererSansDifferes;
  private SNRadioButton rbGenererAvecDifferes;
  private ButtonGroup OADIF_group;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
