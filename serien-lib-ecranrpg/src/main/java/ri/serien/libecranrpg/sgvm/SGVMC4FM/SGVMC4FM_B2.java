
package ri.serien.libecranrpg.sgvm.SGVMC4FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVMC4FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMC4FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTI8.setValeursSelection("X", " ");
    WTI7.setValeursSelection("X", " ");
    WTI6.setValeursSelection("X", " ");
    WTI5.setValeursSelection("X", " ");
    WTI4.setValeursSelection("X", " ");
    WTI3.setValeursSelection("X", " ");
    WTI2.setValeursSelection("X", " ");
    WTI1.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    WMAG.setEnabled(lexique.isPresent("WMAG"));
    WVDE.setEnabled(lexique.isPresent("WVDE"));
    WREP.setEnabled(lexique.isPresent("WREP"));
    WACT.setEnabled(lexique.isPresent("WACT"));
    WSEC.setEnabled(lexique.isPresent("WSEC"));
    WCDP.setEnabled(lexique.isPresent("WCDP"));
    WNOM.setEnabled(lexique.isPresent("WNOM"));
    WCLI.setEnabled(lexique.isPresent("WCLI"));
    // WTI5.setVisible( lexique.isPresent("WTI5"));
    // WTI5.setSelected(lexique.HostFieldGetData("WTI5").equalsIgnoreCase("X"));
    // WTI4.setVisible( lexique.isPresent("WTI4"));
    // WTI4.setSelected(lexique.HostFieldGetData("WTI4").equalsIgnoreCase("X"));
    // WTI2.setVisible( lexique.isPresent("WTI2"));
    // WTI2.setSelected(lexique.HostFieldGetData("WTI2").equalsIgnoreCase("X"));
    // WTI3.setVisible( lexique.isPresent("WTI3"));
    // WTI3.setSelected(lexique.HostFieldGetData("WTI3").equalsIgnoreCase("X"));
    // WTI7.setVisible( lexique.isPresent("WTI7"));
    // WTI7.setSelected(lexique.HostFieldGetData("WTI7").equalsIgnoreCase("X"));
    // WTI6.setVisible( lexique.isPresent("WTI6"));
    // WTI6.setSelected(lexique.HostFieldGetData("WTI6").equalsIgnoreCase("X"));
    // WTI8.setVisible( lexique.isPresent("WTI8"));
    // WTI8.setSelected(lexique.HostFieldGetData("WTI8").equalsIgnoreCase("X"));
    // WTI1.setVisible( lexique.isPresent("WTI1"));
    // WTI1.setSelected(lexique.HostFieldGetData("WTI1").equalsIgnoreCase("X"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Edition des documents de VPC"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTI5.isSelected())
    // lexique.HostFieldPutData("WTI5", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI5", 0, " ");
    // if (WTI4.isSelected())
    // lexique.HostFieldPutData("WTI4", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI4", 0, " ");
    // if (WTI2.isSelected())
    // lexique.HostFieldPutData("WTI2", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI2", 0, " ");
    // if (WTI3.isSelected())
    // lexique.HostFieldPutData("WTI3", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI3", 0, " ");
    // if (WTI7.isSelected())
    // lexique.HostFieldPutData("WTI7", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI7", 0, " ");
    // if (WTI6.isSelected())
    // lexique.HostFieldPutData("WTI6", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI6", 0, " ");
    // if (WTI8.isSelected())
    // lexique.HostFieldPutData("WTI8", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI8", 0, " ");
    // if (WTI1.isSelected())
    // lexique.HostFieldPutData("WTI1", 0, "X");
    // else
    // lexique.HostFieldPutData("WTI1", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WTI1 = new XRiCheckBox();
    WREP = new XRiTextField();
    WVDE = new XRiTextField();
    WTI2 = new XRiCheckBox();
    WTI3 = new XRiCheckBox();
    WMAG = new XRiTextField();
    WSEC = new XRiTextField();
    WTI4 = new XRiCheckBox();
    WTI5 = new XRiCheckBox();
    WACT = new XRiTextField();
    WCLI = new XRiTextField();
    WTI6 = new XRiCheckBox();
    WTI7 = new XRiCheckBox();
    WNOM = new XRiTextField();
    WCDP = new XRiTextField();
    WTI8 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(465, 335));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Tri et s\u00e9lection"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WTI1 ----
          WTI1.setText("Par repr\u00e9sentant");
          WTI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI1.setName("WTI1");
          panel1.add(WTI1);
          WTI1.setBounds(20, 39, 124, 20);

          //---- WREP ----
          WREP.setComponentPopupMenu(BTD);
          WREP.setName("WREP");
          panel1.add(WREP);
          WREP.setBounds(175, 35, 34, WREP.getPreferredSize().height);

          //---- WVDE ----
          WVDE.setComponentPopupMenu(BTD);
          WVDE.setName("WVDE");
          panel1.add(WVDE);
          WVDE.setBounds(175, 63, 34, WVDE.getPreferredSize().height);

          //---- WTI2 ----
          WTI2.setText("Par vendeur");
          WTI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI2.setName("WTI2");
          panel1.add(WTI2);
          WTI2.setBounds(20, 67, 99, 20);

          //---- WTI3 ----
          WTI3.setText("Par magasin");
          WTI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI3.setName("WTI3");
          panel1.add(WTI3);
          WTI3.setBounds(20, 95, 102, 20);

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");
          panel1.add(WMAG);
          WMAG.setBounds(175, 91, 34, WMAG.getPreferredSize().height);

          //---- WSEC ----
          WSEC.setComponentPopupMenu(BTD);
          WSEC.setName("WSEC");
          panel1.add(WSEC);
          WSEC.setBounds(175, 119, 54, WSEC.getPreferredSize().height);

          //---- WTI4 ----
          WTI4.setText("Par section");
          WTI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI4.setName("WTI4");
          panel1.add(WTI4);
          WTI4.setBounds(20, 123, 93, 20);

          //---- WTI5 ----
          WTI5.setText("Par affaire");
          WTI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI5.setName("WTI5");
          panel1.add(WTI5);
          WTI5.setBounds(20, 151, 87, 20);

          //---- WACT ----
          WACT.setComponentPopupMenu(BTD);
          WACT.setName("WACT");
          panel1.add(WACT);
          WACT.setBounds(175, 147, 54, WACT.getPreferredSize().height);

          //---- WCLI ----
          WCLI.setComponentPopupMenu(BTD);
          WCLI.setName("WCLI");
          panel1.add(WCLI);
          WCLI.setBounds(175, 175, 60, WCLI.getPreferredSize().height);

          //---- WTI6 ----
          WTI6.setText("Par code client");
          WTI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI6.setName("WTI6");
          panel1.add(WTI6);
          WTI6.setBounds(20, 179, 114, 20);

          //---- WTI7 ----
          WTI7.setText("Par nom client");
          WTI7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI7.setName("WTI7");
          panel1.add(WTI7);
          WTI7.setBounds(20, 207, 109, 20);

          //---- WNOM ----
          WNOM.setComponentPopupMenu(BTD);
          WNOM.setName("WNOM");
          panel1.add(WNOM);
          WNOM.setBounds(175, 203, 74, WNOM.getPreferredSize().height);

          //---- WCDP ----
          WCDP.setComponentPopupMenu(BTD);
          WCDP.setName("WCDP");
          panel1.add(WCDP);
          WCDP.setBounds(175, 231, 64, WCDP.getPreferredSize().height);

          //---- WTI8 ----
          WTI8.setText("Par code postal");
          WTI8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTI8.setName("WTI8");
          panel1.add(WTI8);
          WTI8.setBounds(20, 235, 120, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox WTI1;
  private XRiTextField WREP;
  private XRiTextField WVDE;
  private XRiCheckBox WTI2;
  private XRiCheckBox WTI3;
  private XRiTextField WMAG;
  private XRiTextField WSEC;
  private XRiCheckBox WTI4;
  private XRiCheckBox WTI5;
  private XRiTextField WACT;
  private XRiTextField WCLI;
  private XRiCheckBox WTI6;
  private XRiCheckBox WTI7;
  private XRiTextField WNOM;
  private XRiTextField WCDP;
  private XRiCheckBox WTI8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
