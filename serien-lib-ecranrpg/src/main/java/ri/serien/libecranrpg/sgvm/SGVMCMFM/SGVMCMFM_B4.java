
package ri.serien.libecranrpg.sgvm.SGVMCMFM;
// Nom Fichier: pop_SGVMCMFM_FMTB4_FMTF1_1291.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGVMCMFM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _OBJ_17_Top=null;
  private String[] _B101_Title = { "", };
  private String[][] _B101_Data = { { "B101", }, { "B102", }, { "B103", }, { "B104", }, { "B105", }, { "B106", }, { "B107", }, { "B108", },
      { "B109", }, { "B110", }, { "B111", }, { "B112", }, { "B113", }, { "B114", }, { "B115", }, };
  private int[] _B101_Width = { 15, };
  private String[] _N101_Title = { "", };
  private String[][] _N101_Data = { { "N101", }, { "N102", }, { "N103", }, { "N104", }, { "N105", }, { "N106", }, { "N107", }, { "N108", },
      { "N109", }, { "N110", }, { "N111", }, { "N112", }, { "N113", }, { "N114", }, { "N115", }, };
  private int[] _N101_Width = { 15, };
  private String[] _L101_Title = { "", };
  private String[][] _L101_Data = { { "L101", }, { "L102", }, { "L103", }, { "L104", }, { "L105", }, { "L106", }, { "L107", }, { "L108", },
      { "L109", }, { "L110", }, { "L111", }, { "L112", }, { "L113", }, { "L114", }, { "L115", }, };
  private int[] _L101_Width = { 15, };
  private String[] _J101_Title = { "", };
  private String[][] _J101_Data = { { "J101", }, { "J102", }, { "J103", }, { "J104", }, { "J105", }, { "J106", }, { "J107", }, { "J108", },
      { "J109", }, { "J110", }, { "J111", }, { "J112", }, { "J113", }, { "J114", }, { "J115", }, };
  private int[] _J101_Width = { 15, };
  private String[] _H101_Title = { "", };
  private String[][] _H101_Data = { { "H101", }, { "H102", }, { "H103", }, { "H104", }, { "H105", }, { "H106", }, { "H107", }, { "H108", },
      { "H109", }, { "H110", }, { "H111", }, { "H112", }, { "H113", }, { "H114", }, { "H115", }, };
  private int[] _H101_Width = { 15, };
  private String[] _F101_Title = { "", };
  private String[][] _F101_Data = { { "F101", }, { "F102", }, { "F103", }, { "F104", }, { "F105", }, { "F106", }, { "F107", }, { "F108", },
      { "F109", }, { "F110", }, { "F111", }, { "F112", }, { "F113", }, { "F114", }, { "F115", }, };
  private int[] _F101_Width = { 15, };
  private String[] _D101_Title = { "", };
  private String[][] _D101_Data = { { "D101", }, { "D102", }, { "D103", }, { "D104", }, { "D105", }, { "D106", }, { "D107", }, { "D108", },
      { "D109", }, { "D110", }, { "D111", }, { "D112", }, { "D113", }, { "D114", }, { "D115", }, };
  private int[] _D101_Width = { 15, };
  // private String[][] _OBJ_17_Title_Data_Brut=null;
  // private String[] _OBJ_19_Top=null;
  // private String[][] _OBJ_19_Title_Data_Brut=null;
  // private String[] _OBJ_21_Top=null;
  // private String[][] _OBJ_21_Title_Data_Brut=null;
  // private String[] _OBJ_23_Top=null;
  // private String[][] _OBJ_23_Title_Data_Brut=null;
  // private String[] _OBJ_25_Top=null;
  // private String[][] _OBJ_25_Title_Data_Brut=null;
  // private String[] _OBJ_27_Top=null;
  // private String[][] _OBJ_27_Title_Data_Brut=null;
  // private String[] _OBJ_31_Top=null;
  // private String[][] _OBJ_31_Title_Data_Brut=null;
  
  public SGVMCMFM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    // _OBJ_17_Title_Data_Brut = initTable(OBJ_17, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // _OBJ_19_Title_Data_Brut = initTable(OBJ_19, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // _OBJ_21_Title_Data_Brut = initTable(OBJ_21, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // _OBJ_23_Title_Data_Brut = initTable(OBJ_23, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // _OBJ_25_Title_Data_Brut = initTable(OBJ_25, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // _OBJ_27_Title_Data_Brut = initTable(OBJ_27, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // _OBJ_31_Title_Data_Brut = initTable(OBJ_31, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // setDialog(true);
    
    // Ajout
    initDiverses();
    B101.setAspectTable(null, _B101_Title, _B101_Data, _B101_Width, false, null, null, null, null);
    N101.setAspectTable(null, _N101_Title, _N101_Data, _N101_Width, false, null, null, null, null);
    L101.setAspectTable(null, _L101_Title, _L101_Data, _L101_Width, false, null, null, null, null);
    J101.setAspectTable(null, _J101_Title, _J101_Data, _J101_Width, false, null, null, null, null);
    H101.setAspectTable(null, _H101_Title, _H101_Data, _H101_Width, false, null, null, null, null);
    F101.setAspectTable(null, _F101_Title, _F101_Data, _F101_Width, false, null, null, null, null);
    D101.setAspectTable(null, _D101_Title, _D101_Data, _D101_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(OBJ_29);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( OBJ_17, _OBJ_17_Title_Data_Brut, _OBJ_17_Top);
    // majTable( OBJ_19, _OBJ_19_Title_Data_Brut, _OBJ_19_Top);
    // majTable( OBJ_21, _OBJ_21_Title_Data_Brut, _OBJ_21_Top);
    // majTable( OBJ_23, _OBJ_23_Title_Data_Brut, _OBJ_23_Top);
    // majTable( OBJ_25, _OBJ_25_Title_Data_Brut, _OBJ_25_Top);
    // majTable( OBJ_27, _OBJ_27_Title_Data_Brut, _OBJ_27_Top);
    // majTable( OBJ_31, _OBJ_31_Title_Data_Brut, _OBJ_31_Top);
    
    
    
    // TODO Icones
    OBJ_29.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_30.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM -"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvm"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  /*
    private void D101MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(OBJ_17, _OBJ_17_Top, "1", "ENTER", e);
      if (D101.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(OBJ_17, _OBJ_17_Top, "1", "Enter");
      D101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  /*
    private void F101MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(OBJ_19, _OBJ_19_Top, "1", "ENTER", e);
      if (F101.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(OBJ_19, _OBJ_19_Top, "1", "Enter");
      F101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  /*
    private void H101MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(OBJ_21, _OBJ_21_Top, "1", "ENTER", e);
      if (H101.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(OBJ_21, _OBJ_21_Top, "1", "Enter");
      H101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  /*
    private void J101MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(OBJ_23, _OBJ_23_Top, "1", "ENTER", e);
      if (J101.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(OBJ_23, _OBJ_23_Top, "1", "Enter");
      J101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  /*
    private void L101MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(OBJ_25, _OBJ_25_Top, "1", "ENTER", e);
      if (L101.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(OBJ_25, _OBJ_25_Top, "1", "Enter");
      L101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  /*
    private void N101MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(OBJ_27, _OBJ_27_Top, "1", "ENTER", e);
      if (N101.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(OBJ_27, _OBJ_27_Top, "1", "Enter");
      N101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  /*
    private void B101MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(OBJ_31, _OBJ_31_Top, "1", "ENTER", e);
      if (B101.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(OBJ_31, _OBJ_31_Top, "1", "Enter");
      B101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    SCROLLPANE_OBJ_17 = new JScrollPane();
    D101 = new XRiTable();
    SCROLLPANE_OBJ_19 = new JScrollPane();
    F101 = new XRiTable();
    SCROLLPANE_OBJ_21 = new JScrollPane();
    H101 = new XRiTable();
    SCROLLPANE_OBJ_23 = new JScrollPane();
    J101 = new XRiTable();
    SCROLLPANE_OBJ_25 = new JScrollPane();
    L101 = new XRiTable();
    SCROLLPANE_OBJ_27 = new JScrollPane();
    N101 = new XRiTable();
    SCROLLPANE_OBJ_31 = new JScrollPane();
    B101 = new XRiTable();
    panel1 = new JPanel();
    OBJ_30 = new JButton();
    OBJ_29 = new JButton();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== SCROLLPANE_OBJ_17 ========
    {
      SCROLLPANE_OBJ_17.setComponentPopupMenu(BTD);
      SCROLLPANE_OBJ_17.setName("SCROLLPANE_OBJ_17");

      //---- D101 ----
      D101.setName("D101");
      SCROLLPANE_OBJ_17.setViewportView(D101);
    }
    add(SCROLLPANE_OBJ_17);
    SCROLLPANE_OBJ_17.setBounds(55, 20, 18, 250);

    //======== SCROLLPANE_OBJ_19 ========
    {
      SCROLLPANE_OBJ_19.setComponentPopupMenu(BTD);
      SCROLLPANE_OBJ_19.setName("SCROLLPANE_OBJ_19");

      //---- F101 ----
      F101.setName("F101");
      SCROLLPANE_OBJ_19.setViewportView(F101);
    }
    add(SCROLLPANE_OBJ_19);
    SCROLLPANE_OBJ_19.setBounds(85, 20, 18, 250);

    //======== SCROLLPANE_OBJ_21 ========
    {
      SCROLLPANE_OBJ_21.setComponentPopupMenu(BTD);
      SCROLLPANE_OBJ_21.setName("SCROLLPANE_OBJ_21");

      //---- H101 ----
      H101.setName("H101");
      SCROLLPANE_OBJ_21.setViewportView(H101);
    }
    add(SCROLLPANE_OBJ_21);
    SCROLLPANE_OBJ_21.setBounds(115, 20, 18, 250);

    //======== SCROLLPANE_OBJ_23 ========
    {
      SCROLLPANE_OBJ_23.setComponentPopupMenu(BTD);
      SCROLLPANE_OBJ_23.setName("SCROLLPANE_OBJ_23");

      //---- J101 ----
      J101.setName("J101");
      SCROLLPANE_OBJ_23.setViewportView(J101);
    }
    add(SCROLLPANE_OBJ_23);
    SCROLLPANE_OBJ_23.setBounds(145, 20, 18, 250);

    //======== SCROLLPANE_OBJ_25 ========
    {
      SCROLLPANE_OBJ_25.setComponentPopupMenu(BTD);
      SCROLLPANE_OBJ_25.setName("SCROLLPANE_OBJ_25");

      //---- L101 ----
      L101.setName("L101");
      SCROLLPANE_OBJ_25.setViewportView(L101);
    }
    add(SCROLLPANE_OBJ_25);
    SCROLLPANE_OBJ_25.setBounds(180, 20, 18, 250);

    //======== SCROLLPANE_OBJ_27 ========
    {
      SCROLLPANE_OBJ_27.setComponentPopupMenu(BTD);
      SCROLLPANE_OBJ_27.setName("SCROLLPANE_OBJ_27");

      //---- N101 ----
      N101.setName("N101");
      SCROLLPANE_OBJ_27.setViewportView(N101);
    }
    add(SCROLLPANE_OBJ_27);
    SCROLLPANE_OBJ_27.setBounds(210, 20, 20, 250);

    //======== SCROLLPANE_OBJ_31 ========
    {
      SCROLLPANE_OBJ_31.setComponentPopupMenu(BTD);
      SCROLLPANE_OBJ_31.setName("SCROLLPANE_OBJ_31");

      //---- B101 ----
      B101.setName("B101");
      SCROLLPANE_OBJ_31.setViewportView(B101);
    }
    add(SCROLLPANE_OBJ_31);
    SCROLLPANE_OBJ_31.setBounds(25, 20, 18, 250);

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_30 ----
      OBJ_30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_30.setToolTipText("Retour");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      panel1.add(OBJ_30);
      OBJ_30.setBounds(65, 5, 56, 40);

      //---- OBJ_29 ----
      OBJ_29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      panel1.add(OBJ_29);
      OBJ_29.setBounds(5, 5, 56, 40);
    }
    add(panel1);
    panel1.setBounds(115, 285, 130, 50);

    setPreferredSize(new Dimension(255, 350));

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      OBJ_4.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Exploitation");
      OBJ_9.setName("OBJ_9");
      OBJ_4.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane SCROLLPANE_OBJ_17;
  private XRiTable D101;
  private JScrollPane SCROLLPANE_OBJ_19;
  private XRiTable F101;
  private JScrollPane SCROLLPANE_OBJ_21;
  private XRiTable H101;
  private JScrollPane SCROLLPANE_OBJ_23;
  private XRiTable J101;
  private JScrollPane SCROLLPANE_OBJ_25;
  private XRiTable L101;
  private JScrollPane SCROLLPANE_OBJ_27;
  private XRiTable N101;
  private JScrollPane SCROLLPANE_OBJ_31;
  private XRiTable B101;
  private JPanel panel1;
  private JButton OBJ_30;
  private JButton OBJ_29;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
