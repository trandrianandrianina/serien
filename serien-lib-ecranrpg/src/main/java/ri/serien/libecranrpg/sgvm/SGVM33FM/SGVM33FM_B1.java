
package ri.serien.libecranrpg.sgvm.SGVM33FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM33FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] WCOD2_Value = { "", "2", };
  
  public SGVM33FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WCUMUL.setValeursSelection("OUI", "NON");
    WTOU.setValeursSelection("**", "  ");
    WCOD2.setValeurs(WCOD2_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_39.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@ARTI@")).trim());
    WRPLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRPLIB@")).trim());
    REP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@REP@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@4@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@5@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@3@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@2@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_55.setVisible(lexique.isPresent("WTOU"));
    OBJ_39.setVisible(lexique.isPresent("ARTX1"));
    WRPLIB.setVisible(lexique.isPresent("WTOU"));
    P_SEL0.setVisible(!(lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") || lexique.isTrue("91")));
    OBJ_49.setVisible(!lexique.HostFieldGetData("WCUMUL").trim().equalsIgnoreCase(""));
    OBJ_52.setVisible(lexique.isPresent("WTOU"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_27 = new JXTitledSeparator();
    OBJ_31 = new RiZoneSortie();
    OBJ_33 = new RiZoneSortie();
    WETB = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_39 = new JXTitledSeparator();
    OBJ_52 = new JXTitledSeparator();
    OBJ_44 = new JXTitledSeparator();
    OBJ_49 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    WCOD2 = new XRiComboBox();
    WRPLIB = new RiZoneSortie();
    OBJ_55 = new JLabel();
    REP = new RiZoneSortie();
    WTOU = new XRiCheckBox();
    panel1 = new JPanel();
    ARTX4 = new XRiTextField();
    OBJ_66 = new JLabel();
    OBJ_68 = new JLabel();
    ARTX5 = new XRiTextField();
    ARTX3 = new XRiTextField();
    OBJ_64 = new JLabel();
    OBJ_62 = new JLabel();
    ARTX2 = new XRiTextField();
    ARTX1 = new XRiTextField();
    OBJ_60 = new JLabel();
    panel2 = new JPanel();
    OBJ_47 = new JLabel();
    PERDEB = new XRiCalendrier();
    OBJ_48 = new JLabel();
    PERFIN = new XRiCalendrier();
    WCUMUL = new XRiCheckBox();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label1 ----
          label1.setText("@LOCTP@");
          label1.setMinimumSize(new Dimension(140, 22));
          label1.setPreferredSize(new Dimension(140, 22));
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setHorizontalTextPosition(SwingConstants.RIGHT);
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 100));
            menus_haut.setPreferredSize(new Dimension(160, 100));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Exportation tableur");
              riSousMenu_bt6.setToolTipText("Exportation tableur");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(870, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_27 ----
          OBJ_27.setTitle("Etablissement s\u00e9lectionn\u00e9");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_31 ----
          OBJ_31.setText("@DGNOM@");
          OBJ_31.setName("OBJ_31");

          //---- OBJ_33 ----
          OBJ_33.setText("@WENCX@");
          OBJ_33.setName("OBJ_33");

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setText("@WETB@");
          WETB.setName("WETB");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_39 ----
          OBJ_39.setTitle("@ARTI@");
          OBJ_39.setName("OBJ_39");

          //---- OBJ_52 ----
          OBJ_52.setTitle("Repr\u00e9sentant");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_44 ----
          OBJ_44.setTitle("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_44.setName("OBJ_44");

          //---- OBJ_49 ----
          OBJ_49.setTitle("");
          OBJ_49.setName("OBJ_49");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- WCOD2 ----
            WCOD2.setModel(new DefaultComboBoxModel(new String[] {
              "1er",
              "2\u00e9me"
            }));
            WCOD2.setComponentPopupMenu(BTDA);
            WCOD2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WCOD2.setName("WCOD2");
            P_SEL0.add(WCOD2);
            WCOD2.setBounds(195, 16, 66, WCOD2.getPreferredSize().height);

            //---- WRPLIB ----
            WRPLIB.setText("@WRPLIB@");
            WRPLIB.setName("WRPLIB");
            P_SEL0.add(WRPLIB);
            WRPLIB.setBounds(15, 50, 245, WRPLIB.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("Code repr\u00e9sentant");
            OBJ_55.setName("OBJ_55");
            P_SEL0.add(OBJ_55);
            OBJ_55.setBounds(15, 20, 120, 18);

            //---- REP ----
            REP.setComponentPopupMenu(BTD);
            REP.setText("@REP@");
            REP.setName("REP");
            P_SEL0.add(REP);
            REP.setBounds(135, 17, 34, REP.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setComponentPopupMenu(BTDA);
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- ARTX4 ----
            ARTX4.setComponentPopupMenu(BTD);
            ARTX4.setName("ARTX4");
            panel1.add(ARTX4);
            ARTX4.setBounds(50, 94, 210, ARTX4.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("@4@");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(10, 94, 28, 28);

            //---- OBJ_68 ----
            OBJ_68.setText("@5@");
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(10, 122, 28, 28);

            //---- ARTX5 ----
            ARTX5.setComponentPopupMenu(BTD);
            ARTX5.setName("ARTX5");
            panel1.add(ARTX5);
            ARTX5.setBounds(50, 122, 210, ARTX5.getPreferredSize().height);

            //---- ARTX3 ----
            ARTX3.setComponentPopupMenu(BTD);
            ARTX3.setName("ARTX3");
            panel1.add(ARTX3);
            ARTX3.setBounds(50, 66, 210, ARTX3.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("@3@");
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(10, 66, 28, 28);

            //---- OBJ_62 ----
            OBJ_62.setText("@2@");
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(10, 38, 28, 28);

            //---- ARTX2 ----
            ARTX2.setComponentPopupMenu(BTD);
            ARTX2.setName("ARTX2");
            panel1.add(ARTX2);
            ARTX2.setBounds(50, 38, 210, ARTX2.getPreferredSize().height);

            //---- ARTX1 ----
            ARTX1.setComponentPopupMenu(BTD);
            ARTX1.setName("ARTX1");
            panel1.add(ARTX1);
            ARTX1.setBounds(50, 10, 210, ARTX1.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setText("@1@");
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(10, 10, 28, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_47 ----
            OBJ_47.setText("Du");
            OBJ_47.setName("OBJ_47");
            panel2.add(OBJ_47);
            OBJ_47.setBounds(10, 20, 21, 18);

            //---- PERDEB ----
            PERDEB.setName("PERDEB");
            panel2.add(PERDEB);
            PERDEB.setBounds(43, 15, 105, PERDEB.getPreferredSize().height);

            //---- OBJ_48 ----
            OBJ_48.setText("au");
            OBJ_48.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_48.setName("OBJ_48");
            panel2.add(OBJ_48);
            OBJ_48.setBounds(160, 20, 30, 18);

            //---- PERFIN ----
            PERFIN.setName("PERFIN");
            panel2.add(PERFIN);
            PERFIN.setBounds(203, 15, 105, PERFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //---- WCUMUL ----
          WCUMUL.setText("Edition des cumuls seuls");
          WCUMUL.setComponentPopupMenu(BTDA);
          WCUMUL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCUMUL.setName("WCUMUL");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 800, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(72, 72, 72)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(WCUMUL, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(23, 23, 23)
                    .addComponent(WCUMUL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_27;
  private RiZoneSortie OBJ_31;
  private RiZoneSortie OBJ_33;
  private RiZoneSortie WETB;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_52;
  private JXTitledSeparator OBJ_44;
  private JXTitledSeparator OBJ_49;
  private JPanel P_SEL0;
  private XRiComboBox WCOD2;
  private RiZoneSortie WRPLIB;
  private JLabel OBJ_55;
  private RiZoneSortie REP;
  private XRiCheckBox WTOU;
  private JPanel panel1;
  private XRiTextField ARTX4;
  private JLabel OBJ_66;
  private JLabel OBJ_68;
  private XRiTextField ARTX5;
  private XRiTextField ARTX3;
  private JLabel OBJ_64;
  private JLabel OBJ_62;
  private XRiTextField ARTX2;
  private XRiTextField ARTX1;
  private JLabel OBJ_60;
  private JPanel panel2;
  private JLabel OBJ_47;
  private XRiCalendrier PERDEB;
  private JLabel OBJ_48;
  private XRiCalendrier PERFIN;
  private XRiCheckBox WCUMUL;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
