
package ri.serien.libecranrpg.sgvm.SGVMQ1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMQ1FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMQ1FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    MA01.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    WPER.setEnabled(lexique.isPresent("WPER"));
    MA12.setEnabled(lexique.isPresent("MA12"));
    MA11.setEnabled(lexique.isPresent("MA11"));
    MA10.setEnabled(lexique.isPresent("MA10"));
    MA09.setEnabled(lexique.isPresent("MA09"));
    MA08.setEnabled(lexique.isPresent("MA08"));
    MA07.setEnabled(lexique.isPresent("MA07"));
    MA06.setEnabled(lexique.isPresent("MA06"));
    MA05.setEnabled(lexique.isPresent("MA05"));
    MA04.setEnabled(lexique.isPresent("MA04"));
    MA03.setEnabled(lexique.isPresent("MA03"));
    MA02.setEnabled(lexique.isPresent("MA02"));
    MA01_doublon_37.setEnabled(lexique.isPresent("MA01"));
    WANNEE.setEnabled(lexique.isPresent("WANNEE"));
    FAMFIN.setEnabled(lexique.isPresent("FAMFIN"));
    FAMDEB.setEnabled(lexique.isPresent("FAMDEB"));
    // MA01.setVisible( lexique.isPresent("MA01"));
    // MA01.setEnabled( lexique.isPresent("MA01"));
    // MA01.setSelected(lexique.HostFieldGetData("MA01").equalsIgnoreCase("**"));
    ARTFIN.setEnabled(lexique.isPresent("ARTFIN"));
    ARTDEB.setEnabled(lexique.isPresent("ARTDEB"));
    // WTOU2.setVisible( lexique.isPresent("WTOU2"));
    // WTOU2.setEnabled( lexique.isPresent("WTOU2"));
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    // WTOU1.setVisible( lexique.isPresent("WTOU1"));
    // WTOU1.setEnabled( lexique.isPresent("WTOU1"));
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOU2").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOU1").trim().equalsIgnoreCase("**"));
    FAMFIN.setEnabled(lexique.isPresent("FAMFIN"));
    
    panel4.setVisible(!lexique.HostFieldGetData("MA01").trim().equalsIgnoreCase("**"));
    
    

    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (MA01.isSelected())
    // lexique.HostFieldPutData("MA01", 0, "**");
    // else
    // lexique.HostFieldPutData("MA01", 0, " ");
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU2", 0, " ");
    // if (WTOU1.isSelected())
    // lexique.HostFieldPutData("WTOU1", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU1", 0, " ");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void MA01ActionPerformed(ActionEvent e) {
    panel4.setVisible(!panel4.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_24 = new JXTitledSeparator();
    OBJ_25 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    panel1 = new JPanel();
    WANNEE = new XRiTextField();
    OBJ_49 = new JLabel();
    OBJ_48 = new JLabel();
    WPER = new XRiTextField();
    panel2 = new JPanel();
    P_SEL1 = new JPanel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    WTOU2 = new XRiCheckBox();
    panel3 = new JPanel();
    P_SEL0 = new JPanel();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    WTOU1 = new XRiCheckBox();
    panel5 = new JPanel();
    panel4 = new JPanel();
    MA01_doublon_37 = new XRiTextField();
    MA07 = new XRiTextField();
    MA02 = new XRiTextField();
    MA08 = new XRiTextField();
    MA03 = new XRiTextField();
    MA09 = new XRiTextField();
    MA04 = new XRiTextField();
    MA10 = new XRiTextField();
    MA05 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    MA06 = new XRiTextField();
    MA01 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_24 ----
          OBJ_24.setTitle("Code(s) Magasin(s) \u00e0 traiter");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_25 ----
          OBJ_25.setTitle("Plages de Codes Articles");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Plages de Codes Groupe-Famille");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_21 ----
          OBJ_21.setTitle("G\u00e9n\u00e9ration au");
          OBJ_21.setName("OBJ_21");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WANNEE ----
            WANNEE.setComponentPopupMenu(BTD);
            WANNEE.setName("WANNEE");
            panel1.add(WANNEE);
            WANNEE.setBounds(95, 5, 42, WANNEE.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("Ann\u00e9e de fin");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(10, 9, 90, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("N\u00b0 p\u00e9riode (RQ)");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(270, 9, 105, 20);

            //---- WPER ----
            WPER.setComponentPopupMenu(BTD);
            WPER.setName("WPER");
            panel1.add(WPER);
            WPER.setBounds(375, 5, 26, WPER.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- ARTDEB ----
              ARTDEB.setComponentPopupMenu(BTD);
              ARTDEB.setName("ARTDEB");
              P_SEL1.add(ARTDEB);
              ARTDEB.setBounds(120, 10, 210, ARTDEB.getPreferredSize().height);

              //---- ARTFIN ----
              ARTFIN.setComponentPopupMenu(BTD);
              ARTFIN.setName("ARTFIN");
              P_SEL1.add(ARTFIN);
              ARTFIN.setBounds(120, 40, 210, ARTFIN.getPreferredSize().height);

              //---- OBJ_56 ----
              OBJ_56.setText("Article de d\u00e9but");
              OBJ_56.setName("OBJ_56");
              P_SEL1.add(OBJ_56);
              OBJ_56.setBounds(12, 16, 108, 16);

              //---- OBJ_57 ----
              OBJ_57.setText("Article de fin");
              OBJ_57.setName("OBJ_57");
              P_SEL1.add(OBJ_57);
              OBJ_57.setBounds(12, 46, 108, 16);
            }
            panel2.add(P_SEL1);
            P_SEL1.setBounds(170, 5, 350, 80);

            //---- WTOU2 ----
            WTOU2.setText("S\u00e9lection compl\u00e9te");
            WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU2.setName("WTOU2");
            WTOU2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU2ActionPerformed(e);
              }
            });
            panel2.add(WTOU2);
            WTOU2.setBounds(5, 26, 151, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_54 ----
              OBJ_54.setText("Groupe-famille de d\u00e9but");
              OBJ_54.setName("OBJ_54");
              P_SEL0.add(OBJ_54);
              OBJ_54.setBounds(13, 13, 153, 22);

              //---- OBJ_55 ----
              OBJ_55.setText("Groupe-famille de fin");
              OBJ_55.setName("OBJ_55");
              P_SEL0.add(OBJ_55);
              OBJ_55.setBounds(13, 43, 132, 22);

              //---- FAMDEB ----
              FAMDEB.setComponentPopupMenu(BTD);
              FAMDEB.setName("FAMDEB");
              P_SEL0.add(FAMDEB);
              FAMDEB.setBounds(170, 10, 40, FAMDEB.getPreferredSize().height);

              //---- FAMFIN ----
              FAMFIN.setComponentPopupMenu(BTD);
              FAMFIN.setName("FAMFIN");
              P_SEL0.add(FAMFIN);
              FAMFIN.setBounds(170, 40, 40, FAMFIN.getPreferredSize().height);
            }
            panel3.add(P_SEL0);
            P_SEL0.setBounds(170, 5, 260, 80);

            //---- WTOU1 ----
            WTOU1.setText("S\u00e9lection compl\u00e9te");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });
            panel3.add(WTOU1);
            WTOU1.setBounds(5, 29, 151, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //======== panel4 ========
            {
              panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- MA01_doublon_37 ----
              MA01_doublon_37.setComponentPopupMenu(BTD);
              MA01_doublon_37.setName("MA01_doublon_37");
              panel4.add(MA01_doublon_37);
              MA01_doublon_37.setBounds(10, 10, 34, MA01_doublon_37.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              panel4.add(MA07);
              MA07.setBounds(10, 40, 34, MA07.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              panel4.add(MA02);
              MA02.setBounds(50, 10, 34, MA02.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              panel4.add(MA08);
              MA08.setBounds(50, 40, 34, MA08.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              panel4.add(MA03);
              MA03.setBounds(90, 10, 34, MA03.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              panel4.add(MA09);
              MA09.setBounds(90, 40, 34, MA09.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              panel4.add(MA04);
              MA04.setBounds(130, 10, 34, MA04.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              panel4.add(MA10);
              MA10.setBounds(130, 40, 34, MA10.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              panel4.add(MA05);
              MA05.setBounds(170, 10, 34, MA05.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              panel4.add(MA11);
              MA11.setBounds(170, 40, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              panel4.add(MA12);
              MA12.setBounds(210, 40, 34, MA12.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              panel4.add(MA06);
              MA06.setBounds(210, 10, 34, MA06.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel4);
            panel4.setBounds(170, 5, 260, 80);

            //---- MA01 ----
            MA01.setText("S\u00e9lection compl\u00e9te");
            MA01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MA01.setName("MA01");
            MA01.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                MA01ActionPerformed(e);
              }
            });
            panel5.add(MA01);
            MA01.setBounds(5, 35, 150, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_21;
  private JPanel panel1;
  private XRiTextField WANNEE;
  private JLabel OBJ_49;
  private JLabel OBJ_48;
  private XRiTextField WPER;
  private JPanel panel2;
  private JPanel P_SEL1;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private XRiCheckBox WTOU2;
  private JPanel panel3;
  private JPanel P_SEL0;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private XRiCheckBox WTOU1;
  private JPanel panel5;
  private JPanel panel4;
  private XRiTextField MA01_doublon_37;
  private XRiTextField MA07;
  private XRiTextField MA02;
  private XRiTextField MA08;
  private XRiTextField MA03;
  private XRiTextField MA09;
  private XRiTextField MA04;
  private XRiTextField MA10;
  private XRiTextField MA05;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private XRiTextField MA06;
  private XRiCheckBox MA01;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
