
package ri.serien.libecranrpg.sgvm.SGVM88FM;
// Nom Fichier: pop_SGVM88FM_FMTP0_FMTF1_630.java

import java.awt.Cursor;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM88FM_P0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM88FM_P0(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_8);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTAR@")).trim());
    OBJ_4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSGTAR@")).trim());
    OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CODTAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_8.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Mise à jour CNV"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_6 = new JLabel();
    OBJ_4 = new JLabel();
    OBJ_7 = new JButton();
    OBJ_9 = new JLabel();
    OBJ_8 = new JButton();
    OBJ_5 = new JLabel();

    //======== this ========
    setName("this");

    //---- OBJ_6 ----
    OBJ_6.setText("@LIBTAR@");
    OBJ_6.setName("OBJ_6");

    //---- OBJ_4 ----
    OBJ_4.setText("@MSGTAR@");
    OBJ_4.setName("OBJ_4");

    //---- OBJ_7 ----
    OBJ_7.setText("");
    OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_7.setName("OBJ_7");

    //---- OBJ_9 ----
    OBJ_9.setText("Edition en cours");
    OBJ_9.setName("OBJ_9");

    //---- OBJ_8 ----
    OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_8.setName("OBJ_8");

    //---- OBJ_5 ----
    OBJ_5.setText("@CODTAR@");
    OBJ_5.setName("OBJ_5");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(13, 13, 13)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
          .addGap(12, 12, 12)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(77, 77, 77)
              .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(77, 77, 77)
              .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 231, GroupLayout.PREFERRED_SIZE))
            .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 351, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addGap(316, 316, 316)
              .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)))
          .addGap(640, 640, 640))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
          .addGap(26, 26, 26)
          .addComponent(OBJ_5))
        .addGroup(layout.createSequentialGroup()
          .addGap(19, 19, 19)
          .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
          .addGap(11, 11, 11)
          .addComponent(OBJ_4)
          .addGap(15, 15, 15)
          .addComponent(OBJ_6)
          .addGap(8, 8, 8)
          .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_6;
  private JLabel OBJ_4;
  private JButton OBJ_7;
  private JLabel OBJ_9;
  private JButton OBJ_8;
  private JLabel OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
