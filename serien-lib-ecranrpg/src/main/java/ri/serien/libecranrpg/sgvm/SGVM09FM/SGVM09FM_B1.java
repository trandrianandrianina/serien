
package ri.serien.libecranrpg.sgvm.SGVM09FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;

/**
 * @author Stéphane Vénéri
 */
public class SGVM09FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM09FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel5.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@ article @L1ART@")).trim()));
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESSAG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_22.setVisible(!lexique.HostFieldGetData("MESSAG").trim().equalsIgnoreCase(""));
    OBJ_18.setVisible(!lexique.HostFieldGetData("MESSAG").trim().equalsIgnoreCase(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("GESTION DES KITS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("OPT1", 0, " ");
    lexique.HostFieldPutData("OPT2", 0, " ");
    lexique.HostFieldPutData("OPT3", 0, " ");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("OPT1", 0, "1");
    lexique.HostFieldPutData("OPT2", 0, " ");
    lexique.HostFieldPutData("OPT3", 0, " ");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("OPT1", 0, " ");
    lexique.HostFieldPutData("OPT2", 0, "1");
    lexique.HostFieldPutData("OPT3", 0, " ");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("OPT1", 0, " ");
    lexique.HostFieldPutData("OPT2", 0, " ");
    lexique.HostFieldPutData("OPT3", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    panel5 = new JPanel();
    OBJ_15 = new JButton();
    OBJ_16 = new JButton();
    OBJ_17 = new JButton();
    OBJ_22 = new JLabel();
    OBJ_18 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(910, 190));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel4 ========
        {
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder("@TITRE@ article @L1ART@"));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- OBJ_15 ----
            OBJ_15.setText("Visualisation des composants");
            OBJ_15.setComponentPopupMenu(BTD);
            OBJ_15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_15.setName("OBJ_15");
            OBJ_15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_15ActionPerformed(e);
              }
            });
            panel5.add(OBJ_15);
            OBJ_15.setBounds(10, 30, 215, 46);

            //---- OBJ_16 ----
            OBJ_16.setText("G\u00e9n\u00e9ration des lignes");
            OBJ_16.setComponentPopupMenu(BTD);
            OBJ_16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_16.setName("OBJ_16");
            OBJ_16.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_16ActionPerformed(e);
              }
            });
            panel5.add(OBJ_16);
            OBJ_16.setBounds(235, 30, 222, 46);

            //---- OBJ_17 ----
            OBJ_17.setText("Retour sans g\u00e9n\u00e9ration");
            OBJ_17.setComponentPopupMenu(BTD);
            OBJ_17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_17.setName("OBJ_17");
            OBJ_17.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_17ActionPerformed(e);
              }
            });
            panel5.add(OBJ_17);
            OBJ_17.setBounds(465, 30, 222, 46);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          panel4.add(panel5);
          panel5.setBounds(10, 15, 700, 95);

          //---- OBJ_22 ----
          OBJ_22.setIcon(new ImageIcon("images/w95mbx03.gif"));
          OBJ_22.setName("OBJ_22");
          panel4.add(OBJ_22);
          OBJ_22.setBounds(55, 115, 36, 32);

          //---- OBJ_18 ----
          OBJ_18.setText("@MESSAG@");
          OBJ_18.setName("OBJ_18");
          panel4.add(OBJ_18);
          OBJ_18.setBounds(105, 122, 534, 18);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel4;
  private JPanel panel5;
  private JButton OBJ_15;
  private JButton OBJ_16;
  private JButton OBJ_17;
  private JLabel OBJ_22;
  private JLabel OBJ_18;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
