
package ri.serien.libecranrpg.sgvm.SGVM40FM;
// Nom Fichier: pop_SGVM40FM_FMTP0_FMTF1_552.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVM40FM_P0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM40FM_P0(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    RECAPJ = new XRiTextField();
    OBJ_5 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_4 = new JButton();

    //======== this ========
    setPreferredSize(new Dimension(405, 110));
    setName("this");
    setLayout(null);

    //---- RECAPJ ----
    RECAPJ.setName("RECAPJ");
    add(RECAPJ);
    RECAPJ.setBounds(20, 75, 360, RECAPJ.getPreferredSize().height);

    //---- OBJ_5 ----
    OBJ_5.setText("Traitement en cours");
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(125, 30, 154, 20);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }
    add(P_PnlOpts);
    P_PnlOpts.setBounds(1036, 15, 55, 516);

    setPreferredSize(new Dimension(405, 110));

    //---- OBJ_4 ----
    OBJ_4.setText("");
    OBJ_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_4.setName("OBJ_4");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private XRiTextField RECAPJ;
  private JLabel OBJ_5;
  private JPanel P_PnlOpts;
  private JButton OBJ_4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
