
package ri.serien.libecranrpg.sgvm.SGVMSNFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMSNFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _T01_Top =
      { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "Libellé", };
  private String[][] _T01_Data = { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", },
      { "L09", }, { "L10", }, { "L11", }, { "L12", }, { "L13", }, { "L14", }, { "L15", }, };
  private int[] _T01_Width = { 28, };
  
  public SGVMSNFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WCRLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCRLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _T01_Top);
    
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Tableau de bord"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void label1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WCRI");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "Enter", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "Enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOISIR2ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "2", "Enter");
    T01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOISIR3ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "3", "Enter");
    T01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOISIR4ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "4", "Enter");
    T01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOISIR5ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "5", "Enter");
    T01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    CHOISIR2 = new JMenuItem();
    CHOISIR3 = new JMenuItem();
    CHOISIR4 = new JMenuItem();
    CHOISIR5 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus2 = new JPanel();
    menus_bas2 = new JPanel();
    navig_erreurs2 = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid2 = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour2 = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new SNPanelContenu();
    label1 = new JButton();
    WCRI = new XRiTextField();
    panel1 = new JPanel();
    SCROLLPANE_LIST3 = new JScrollPane();
    T01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    WCRLIB = new RiZoneSortie();
    BTD2 = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Statistiques p\u00e9riodiques");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- CHOISIR2 ----
      CHOISIR2.setText("Statistiques mensuelles");
      CHOISIR2.setName("CHOISIR2");
      CHOISIR2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR2ActionPerformed(e);
        }
      });
      BTD.add(CHOISIR2);

      //---- CHOISIR3 ----
      CHOISIR3.setText("Statistiques horaires");
      CHOISIR3.setName("CHOISIR3");
      CHOISIR3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR3ActionPerformed(e);
        }
      });
      BTD.add(CHOISIR3);

      //---- CHOISIR4 ----
      CHOISIR4.setText("Palmar\u00e8s");
      CHOISIR4.setName("CHOISIR4");
      CHOISIR4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR4ActionPerformed(e);
        }
      });
      BTD.add(CHOISIR4);

      //---- CHOISIR5 ----
      CHOISIR5.setText("Statistiques journali\u00e8res");
      CHOISIR5.setName("CHOISIR5");
      CHOISIR5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR5ActionPerformed(e);
        }
      });
      BTD.add(CHOISIR5);
      BTD.addSeparator();

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 862, Short.MAX_VALUE)
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 30, Short.MAX_VALUE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus2 ========
      {
        p_menus2.setPreferredSize(new Dimension(170, 0));
        p_menus2.setMinimumSize(new Dimension(170, 0));
        p_menus2.setBackground(new Color(238, 239, 241));
        p_menus2.setBorder(LineBorder.createGrayLineBorder());
        p_menus2.setName("p_menus2");
        p_menus2.setLayout(new BorderLayout());

        //======== menus_bas2 ========
        {
          menus_bas2.setOpaque(false);
          menus_bas2.setBackground(new Color(238, 239, 241));
          menus_bas2.setName("menus_bas2");
          menus_bas2.setLayout(new VerticalLayout());

          //======== navig_erreurs2 ========
          {
            navig_erreurs2.setName("navig_erreurs2");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs2.add(bouton_erreurs);
          }
          menus_bas2.add(navig_erreurs2);

          //======== navig_valid2 ========
          {
            navig_valid2.setName("navig_valid2");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid2.add(bouton_valider);
          }
          menus_bas2.add(navig_valid2);

          //======== navig_retour2 ========
          {
            navig_retour2.setName("navig_retour2");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour2.add(bouton_retour);
          }
          menus_bas2.add(navig_retour2);
        }
        p_menus2.add(menus_bas2, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus2.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus2, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setOpaque(true);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
        ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- label1 ----
        label1.setText("Recherche par crit\u00e8re");
        label1.setMinimumSize(new Dimension(190, 30));
        label1.setMaximumSize(new Dimension(190, 30));
        label1.setPreferredSize(new Dimension(190, 30));
        label1.setName("label1");
        label1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            label1ActionPerformed(e);
          }
        });
        p_contenu.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WCRI ----
        WCRI.setComponentPopupMenu(null);
        WCRI.setPreferredSize(new Dimension(40, 30));
        WCRI.setMinimumSize(new Dimension(40, 30));
        WCRI.setName("WCRI");
        p_contenu.add(WCRI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Sommaire des statistiques"));
          panel1.setOpaque(false);
          panel1.setPreferredSize(new Dimension(800, 350));
          panel1.setMinimumSize(new Dimension(800, 350));
          panel1.setName("panel1");
          panel1.setLayout(new GridBagLayout());
          ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
          ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //======== SCROLLPANE_LIST3 ========
          {
            SCROLLPANE_LIST3.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST3.setPreferredSize(new Dimension(700, 270));
            SCROLLPANE_LIST3.setMinimumSize(new Dimension(700, 270));
            SCROLLPANE_LIST3.setName("SCROLLPANE_LIST3");

            //---- T01 ----
            T01.setComponentPopupMenu(BTD);
            T01.setName("T01");
            T01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                T01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST3.setViewportView(T01);
          }
          panel1.add(SCROLLPANE_LIST3, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setPreferredSize(new Dimension(25, 125));
          BT_PGUP.setMinimumSize(new Dimension(25, 125));
          BT_PGUP.setMaximumSize(new Dimension(25, 125));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMaximumSize(new Dimension(25, 125));
          BT_PGDOWN.setMinimumSize(new Dimension(25, 125));
          BT_PGDOWN.setPreferredSize(new Dimension(25, 125));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(panel1, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- WCRLIB ----
        WCRLIB.setText("@WCRLIB@");
        WCRLIB.setMinimumSize(new Dimension(200, 30));
        WCRLIB.setMaximumSize(new Dimension(200, 30));
        WCRLIB.setPreferredSize(new Dimension(200, 30));
        WCRLIB.setName("WCRLIB");
        p_contenu.add(WCRLIB, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Invite");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_15);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem CHOISIR2;
  private JMenuItem CHOISIR3;
  private JMenuItem CHOISIR4;
  private JMenuItem CHOISIR5;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus2;
  private JPanel menus_bas2;
  private RiMenu navig_erreurs2;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid2;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour2;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelContenu p_contenu;
  private JButton label1;
  private XRiTextField WCRI;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST3;
  private XRiTable T01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private RiZoneSortie WCRLIB;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
