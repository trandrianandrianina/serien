
package ri.serien.libecranrpg.sgvm.SGVM27FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.EnumOrdreCritereDeTriEtDeSelection;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.affaire.snaffaire.SNAffaire;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVM27FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  // Declarations pour Combobox critères de tri ordonnés
  private ArrayList<SNComboBox> listeComboTri = new ArrayList<SNComboBox>();
  private ArrayList<EnumOrdreCritereDeTriEtDeSelection> listeEnumOrdreDeTri = new ArrayList<EnumOrdreCritereDeTriEtDeSelection>();
  private boolean isInitialise = false;
  
  public SGVM27FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    for (int i = 0; i < EnumOrdreCritereDeTriEtDeSelection.values().length; i++) {
      listeEnumOrdreDeTri.add(EnumOrdreCritereDeTriEtDeSelection.values()[i]);
    }
    
    // Ajout des Combobox dans listComboTri
    // Une ligne par critère de tri possible sélectionnable
    listeComboTri.add(cbTriRepresentant);
    listeComboTri.add(cbTriVendeur);
    listeComboTri.add(cbTriMagasin);
    listeComboTri.add(cbTriSectionAnalytique);
    listeComboTri.add(cbTriAffaire);
    listeComboTri.add(cbTriClient);
    listeComboTri.add(cbTriNumeroDeBon);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // Init spécifiques
    BONTRI.setEnabled(lexique.isPresent("BONTRI"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Edition de traites sur factures"));
    
    // initialise les combobox de critères de tri
    initialiserCombo();
    
    // Initialisation de composant lié à l'établissement
    // Initialisation représentant
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(idEtablissement);
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "WREP");
    
    // Initialisation vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(idEtablissement);
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "WVDE");
    
    // Initialisation magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // Initialisation section analytique
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(idEtablissement);
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "WSEC");
    
    // Initialisation affaire
    snAffaire.setSession(getSession());
    snAffaire.setIdEtablissement(idEtablissement);
    snAffaire.setTousAutorise(true);
    snAffaire.charger(false);
    snAffaire.setSelectionParChampRPG(lexique, "WACT");
    
    // Initialisation client
    snClient.setSession(getSession());
    snClient.setIdEtablissement(idEtablissement);
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "WCLI");
  }
  
  @Override
  public void getData() {
    super.getData();
    snRepresentant.renseignerChampRPG(lexique, "WREP");
    snVendeur.renseignerChampRPG(lexique, "WVDE");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snSectionAnalytique.renseignerChampRPG(lexique, "WSEC");
    snAffaire.renseignerChampRPG(lexique, "WACT");
    snClient.renseignerChampRPG(lexique, "WCLI");
    
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    if (pSNBouton.isBouton(EnumBouton.EDITER)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
      lexique.HostScreenSendKey(this, "F12");
    }
  }
  
  /**
   * Initialise les combobox de critères de tri
   */
  private void initialiserCombo() {
    isInitialise = false;
    for (SNComboBox combo : listeComboTri) {
      // Initialise les combo n'ayant aucune sélection avec les valeur de listeEnumOrdreDeTri
      if (combo.getSelectedItem() == null || combo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        combo.removeAllItems();
        for (EnumOrdreCritereDeTriEtDeSelection enumTri : listeEnumOrdreDeTri) {
          combo.addItem(enumTri);
        }
      }
      else {
        // Permet de mettre à jour les combo ayant une sélection sans pour autant perdre celle-ci
        EnumOrdreCritereDeTriEtDeSelection selectionEnCours = (EnumOrdreCritereDeTriEtDeSelection) combo.getSelectedItem();
        combo.removeAllItems();
        for (EnumOrdreCritereDeTriEtDeSelection enumTri : listeEnumOrdreDeTri) {
          combo.addItem(enumTri);
        }
        combo.addItem(selectionEnCours);
        combo.setSelectedItem(selectionEnCours);
      }
    }
    isInitialise = true;
  }
  
  /**
   * Gére les sélections des combo ainsi que la liste des EnumOrdreCriteresDeTriEtSelection
   */
  private void traiterSelectionCombo(ItemEvent pE, SNComboBox pCombo, String champRPG) {
    // Récupère la valeur qui a été désélectionnée afin de la rajouter à listeEnumOrdreDeTri si cette valeur n'existe pas dans celle-ci
    if (pE.getStateChange() == ItemEvent.DESELECTED) {
      if (!listeEnumOrdreDeTri.contains(pE.getItem())) {
        listeEnumOrdreDeTri.add((EnumOrdreCritereDeTriEtDeSelection) pE.getItem());
      }
    }
    // Si on a une selection valide
    if (pCombo.getSelectedItem() != null) {
      if (!pCombo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        listeEnumOrdreDeTri.remove(pCombo.getSelectedItem());
      }
      initialiserCombo();
      if (pCombo.getSelectedItem().equals(EnumOrdreCritereDeTriEtDeSelection.AUCUN_CRITERE)) {
        lexique.HostFieldPutData(champRPG, 0, " ");
      }
      else {
        lexique.HostFieldPutData(champRPG, 0, ((EnumOrdreCritereDeTriEtDeSelection) pCombo.getSelectedItem()).getStringCode());
      }
    }
  }
  
  private void cbTriRepresentantItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriRepresentant, "WTI1");
    }
  }
  
  private void cbTriVendeurItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriVendeur, "WTI2");
    }
  }
  
  private void cbTriMagasinItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriMagasin, "WTI3");
    }
  }
  
  private void cbTriSectionAnalytiqueItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriSectionAnalytique, "WTI4");
    }
  }
  
  private void cbTriAffaireItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriAffaire, "WTI5");
    }
  }
  
  private void cbTriClientItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriClient, "WTI6");
    }
  }
  
  private void cbTriNumeroDeBonItemStateChanged(ItemEvent e) {
    if (isInitialise) {
      traiterSelectionCombo(e, cbTriNumeroDeBon, "WTI8");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlCriteresDeTri = new SNPanelTitre();
    lbRepresentant = new SNLabelChamp();
    cbTriRepresentant = new SNComboBox();
    snRepresentant = new SNRepresentant();
    lbVendeur = new SNLabelChamp();
    cbTriVendeur = new SNComboBox();
    snVendeur = new SNVendeur();
    lbMagasin = new SNLabelChamp();
    cbTriMagasin = new SNComboBox();
    snMagasin = new SNMagasin();
    lbSectionAnalytique = new SNLabelChamp();
    cbTriSectionAnalytique = new SNComboBox();
    snSectionAnalytique = new SNSectionAnalytique();
    lbAffaire = new SNLabelChamp();
    cbTriAffaire = new SNComboBox();
    snAffaire = new SNAffaire();
    lbClient = new SNLabelChamp();
    cbTriClient = new SNComboBox();
    snClient = new SNClientPrincipal();
    lbNuméroDeBon = new SNLabelChamp();
    cbTriNumeroDeBon = new SNComboBox();
    pnlNumeroDeBon = new SNPanel();
    BONTRI = new XRiTextField();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(700, 400));
    setPreferredSize(new Dimension(700, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlCriteresDeTri ========
      {
        pnlCriteresDeTri.setTitre("S\u00e9lection et ordre des crit\u00e8res de tri");
        pnlCriteresDeTri.setName("pnlCriteresDeTri");
        pnlCriteresDeTri.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).columnWidths = new int[] {0, 155, 0, 0};
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlCriteresDeTri.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbRepresentant ----
        lbRepresentant.setText("Repr\u00e9sentant");
        lbRepresentant.setName("lbRepresentant");
        pnlCriteresDeTri.add(lbRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriRepresentant ----
        cbTriRepresentant.setName("cbTriRepresentant");
        cbTriRepresentant.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriRepresentantItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(cbTriRepresentant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snRepresentant ----
        snRepresentant.setName("snRepresentant");
        pnlCriteresDeTri.add(snRepresentant, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbVendeur ----
        lbVendeur.setText("Vendeur");
        lbVendeur.setName("lbVendeur");
        pnlCriteresDeTri.add(lbVendeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriVendeur ----
        cbTriVendeur.setName("cbTriVendeur");
        cbTriVendeur.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriVendeurItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(cbTriVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snVendeur ----
        snVendeur.setName("snVendeur");
        pnlCriteresDeTri.add(snVendeur, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMagasin ----
        lbMagasin.setText("Magasin");
        lbMagasin.setName("lbMagasin");
        pnlCriteresDeTri.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriMagasin ----
        cbTriMagasin.setName("cbTriMagasin");
        cbTriMagasin.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriMagasinItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(cbTriMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snMagasin ----
        snMagasin.setName("snMagasin");
        pnlCriteresDeTri.add(snMagasin, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbSectionAnalytique ----
        lbSectionAnalytique.setText("Section");
        lbSectionAnalytique.setName("lbSectionAnalytique");
        pnlCriteresDeTri.add(lbSectionAnalytique, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriSectionAnalytique ----
        cbTriSectionAnalytique.setName("cbTriSectionAnalytique");
        cbTriSectionAnalytique.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriSectionAnalytiqueItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(cbTriSectionAnalytique, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snSectionAnalytique ----
        snSectionAnalytique.setName("snSectionAnalytique");
        pnlCriteresDeTri.add(snSectionAnalytique, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbAffaire ----
        lbAffaire.setText("Affaire");
        lbAffaire.setName("lbAffaire");
        pnlCriteresDeTri.add(lbAffaire, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriAffaire ----
        cbTriAffaire.setName("cbTriAffaire");
        cbTriAffaire.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriAffaireItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(cbTriAffaire, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snAffaire ----
        snAffaire.setName("snAffaire");
        pnlCriteresDeTri.add(snAffaire, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbClient ----
        lbClient.setText("Client");
        lbClient.setName("lbClient");
        pnlCriteresDeTri.add(lbClient, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- cbTriClient ----
        cbTriClient.setName("cbTriClient");
        cbTriClient.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriClientItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(cbTriClient, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snClient ----
        snClient.setName("snClient");
        pnlCriteresDeTri.add(snClient, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNuméroDeBon ----
        lbNuméroDeBon.setText("Num\u00e9ro de bon");
        lbNuméroDeBon.setName("lbNum\u00e9roDeBon");
        pnlCriteresDeTri.add(lbNuméroDeBon, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- cbTriNumeroDeBon ----
        cbTriNumeroDeBon.setName("cbTriNumeroDeBon");
        cbTriNumeroDeBon.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbTriNumeroDeBonItemStateChanged(e);
          }
        });
        pnlCriteresDeTri.add(cbTriNumeroDeBon, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlNumeroDeBon ========
        {
          pnlNumeroDeBon.setName("pnlNumeroDeBon");
          pnlNumeroDeBon.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlNumeroDeBon.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlNumeroDeBon.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlNumeroDeBon.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)pnlNumeroDeBon.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- BONTRI ----
          BONTRI.setMinimumSize(new Dimension(70, 30));
          BONTRI.setPreferredSize(new Dimension(70, 30));
          BONTRI.setName("BONTRI");
          pnlNumeroDeBon.add(BONTRI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCriteresDeTri.add(pnlNumeroDeBon, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCriteresDeTri);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlCriteresDeTri;
  private SNLabelChamp lbRepresentant;
  private SNComboBox cbTriRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbVendeur;
  private SNComboBox cbTriVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbMagasin;
  private SNComboBox cbTriMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbSectionAnalytique;
  private SNComboBox cbTriSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbAffaire;
  private SNComboBox cbTriAffaire;
  private SNAffaire snAffaire;
  private SNLabelChamp lbClient;
  private SNComboBox cbTriClient;
  private SNClientPrincipal snClient;
  private SNLabelChamp lbNuméroDeBon;
  private SNComboBox cbTriNumeroDeBon;
  private SNPanel pnlNumeroDeBon;
  private XRiTextField BONTRI;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
