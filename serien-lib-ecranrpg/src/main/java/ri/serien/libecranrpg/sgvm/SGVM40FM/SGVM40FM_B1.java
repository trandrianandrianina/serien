
package ri.serien.libecranrpg.sgvm.SGVM40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM511] Gestion des ventes -> Opérations périodiques -> Comptabilisation des ventes -> Comptabilisation régulière
 * Indicateurs: 10000001
 * Titre: Comptabilisation régulière des ventes
 */
public class SGVM40FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message messageforçage = null;
  
  public SGVM40FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    FORCAG.setValeursSelection("*FOR", "    ");
    MAJPCA.setValeursSelection("O", " ");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    J101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J101@")).trim());
    J102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J102@")).trim());
    J103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J103@")).trim());
    J104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J104@")).trim());
    J105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J105@")).trim());
    J106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J106@")).trim());
    J107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J107@")).trim());
    J108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J108@")).trim());
    J109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J109@")).trim());
    J110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J110@")).trim());
    J120.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J120@")).trim());
    J119.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J119@")).trim());
    J118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J118@")).trim());
    J117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J117@")).trim());
    J116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J116@")).trim());
    J115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J115@")).trim());
    J114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J114@")).trim());
    J113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J113@")).trim());
    J112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J112@")).trim());
    J111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J111@")).trim());
    J121.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J121@")).trim());
    J122.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J122@")).trim());
    J123.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J123@")).trim());
    J124.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J124@")).trim());
    J125.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J125@")).trim());
    J126.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J126@")).trim());
    J127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J127@")).trim());
    J128.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J128@")).trim());
    J129.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J129@")).trim());
    J130.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J130@")).trim());
    J131.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J131@")).trim());
    lbPERIO1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO1@")).trim());
    EPER1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER1@")).trim());
    EPER2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER2@")).trim());
    EPER3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER3@")).trim());
    EPER4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER4@")).trim());
    EPER9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER9@")).trim());
    EPER10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER10@")).trim());
    EPER11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER11@")).trim());
    EPER12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER12@")).trim());
    EPER5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER5@")).trim());
    EPER13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER13@")).trim());
    EPER6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER6@")).trim());
    EPER14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER14@")).trim());
    EPER7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER7@")).trim());
    EPER15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER15@")).trim());
    EPER8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER8@")).trim());
    EPER16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER16@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Indicateur
    Boolean isMagasin = lexique.isTrue("31");
    
    pnlComptabilisation.setTitre(lexique.HostFieldGetData("LIPER1").trim());
    
    // Visibilité des composants
    lbMagasin.setVisible(isMagasin);
    snMagasin.setVisible(isMagasin);
    EPER1.setVisible(!lexique.HostFieldGetData("EPER1").trim().isEmpty());
    EPER2.setVisible(!lexique.HostFieldGetData("EPER2").trim().isEmpty());
    EPER3.setVisible(!lexique.HostFieldGetData("EPER3").trim().isEmpty());
    EPER4.setVisible(!lexique.HostFieldGetData("EPER4").trim().isEmpty());
    EPER5.setVisible(!lexique.HostFieldGetData("EPER5").trim().isEmpty());
    EPER6.setVisible(!lexique.HostFieldGetData("EPER6").trim().isEmpty());
    EPER7.setVisible(!lexique.HostFieldGetData("EPER7").trim().isEmpty());
    EPER8.setVisible(!lexique.HostFieldGetData("EPER8").trim().isEmpty());
    EPER9.setVisible(!lexique.HostFieldGetData("EPER9").trim().isEmpty());
    EPER10.setVisible(!lexique.HostFieldGetData("EPER10").trim().isEmpty());
    EPER11.setVisible(!lexique.HostFieldGetData("EPER11").trim().isEmpty());
    EPER12.setVisible(!lexique.HostFieldGetData("EPER12").trim().isEmpty());
    EPER13.setVisible(!lexique.HostFieldGetData("EPER13").trim().isEmpty());
    EPER14.setVisible(!lexique.HostFieldGetData("EPER14").trim().isEmpty());
    EPER15.setVisible(!lexique.HostFieldGetData("EPER15").trim().isEmpty());
    EPER16.setVisible(!lexique.HostFieldGetData("EPER16").trim().isEmpty());
    pnlRecapitulatifPeriode.setVisible(EPER1.isVisible());
    
    // Initialisation des message forçage
    messageforçage = messageforçage.getMessageImportant("forçage");
    lbForçage1.setVisible(!lexique.HostFieldGetData("EFOR1").trim().isEmpty());
    lbForçage1.setMessage(messageforçage);
    lbForçage2.setVisible(!lexique.HostFieldGetData("EFOR2").trim().isEmpty());
    lbForçage2.setMessage(messageforçage);
    lbForçage3.setVisible(!lexique.HostFieldGetData("EFOR3").trim().isEmpty());
    lbForçage3.setMessage(messageforçage);
    lbForçage4.setVisible(!lexique.HostFieldGetData("EFOR4").trim().isEmpty());
    lbForçage4.setMessage(messageforçage);
    lbForçage5.setVisible(!lexique.HostFieldGetData("EFOR5").trim().isEmpty());
    lbForçage5.setMessage(messageforçage);
    lbForçage6.setVisible(!lexique.HostFieldGetData("EFOR6").trim().isEmpty());
    lbForçage6.setMessage(messageforçage);
    lbForçage7.setVisible(!lexique.HostFieldGetData("EFOR7").trim().isEmpty());
    lbForçage7.setMessage(messageforçage);
    lbForçage8.setVisible(!lexique.HostFieldGetData("EFOR8").trim().isEmpty());
    lbForçage8.setMessage(messageforçage);
    lbForçage9.setVisible(!lexique.HostFieldGetData("EFOR9").trim().isEmpty());
    lbForçage9.setMessage(messageforçage);
    lbForçage10.setVisible(!lexique.HostFieldGetData("EFOR10").trim().isEmpty());
    lbForçage10.setMessage(messageforçage);
    lbForçage11.setVisible(!lexique.HostFieldGetData("EFOR11").trim().isEmpty());
    lbForçage11.setMessage(messageforçage);
    lbForçage12.setVisible(!lexique.HostFieldGetData("EFOR12").trim().isEmpty());
    lbForçage12.setMessage(messageforçage);
    lbForçage13.setVisible(!lexique.HostFieldGetData("EFOR13").trim().isEmpty());
    lbForçage13.setMessage(messageforçage);
    lbForçage14.setVisible(!lexique.HostFieldGetData("EFOR14").trim().isEmpty());
    lbForçage14.setMessage(messageforçage);
    lbForçage15.setVisible(!lexique.HostFieldGetData("EFOR15").trim().isEmpty());
    lbForçage15.setMessage(messageforçage);
    lbForçage16.setVisible(!lexique.HostFieldGetData("EFOR16").trim().isEmpty());
    lbForçage16.setMessage(messageforçage);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Charge les composants
    chargerComposantMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snMagasin.isVisible()) {
      snMagasin.renseignerChampRPG(lexique, "WMAG");
    }
  }
  
  /**
   * Charge le composants magasin
   */
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        if (!lexique.HostFieldGetData("EPER1").trim().isEmpty()) {
          lexique.HostScreenSendKey(this, "F6");
        }
        else {
          lexique.HostScreenSendKey(this, "ENTER");
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlComptabilisation = new SNPanelTitre();
    J101 = new SNTexte();
    J102 = new SNTexte();
    J103 = new SNTexte();
    J104 = new SNTexte();
    J105 = new SNTexte();
    J106 = new SNTexte();
    J107 = new SNTexte();
    J108 = new SNTexte();
    J109 = new SNTexte();
    J110 = new SNTexte();
    J120 = new SNTexte();
    J119 = new SNTexte();
    J118 = new SNTexte();
    J117 = new SNTexte();
    J116 = new SNTexte();
    J115 = new SNTexte();
    J114 = new SNTexte();
    J113 = new SNTexte();
    J112 = new SNTexte();
    J111 = new SNTexte();
    J121 = new SNTexte();
    J122 = new SNTexte();
    J123 = new SNTexte();
    J124 = new SNTexte();
    J125 = new SNTexte();
    J126 = new SNTexte();
    J127 = new SNTexte();
    J128 = new SNTexte();
    J129 = new SNTexte();
    J130 = new SNTexte();
    J131 = new SNTexte();
    MAJPCA = new XRiCheckBox();
    pnCritereDeSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbPeriodeAComptabiliser = new SNLabelChamp();
    UPER1 = new XRiTextField();
    lbAu = new SNLabelChamp();
    UPER2 = new XRiTextField();
    lbPERIO1 = new SNLabelUnite();
    FORCAG = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    pnlRecapitulatifPeriode = new SNPanelTitre();
    EPER1 = new SNTexte();
    EPER2 = new SNTexte();
    EPER3 = new SNTexte();
    EPER4 = new SNTexte();
    lbForçage4 = new SNLabelUnite();
    lbForçage3 = new SNLabelUnite();
    lbForçage2 = new SNLabelUnite();
    lbForçage1 = new SNLabelUnite();
    EPER9 = new SNTexte();
    EPER10 = new SNTexte();
    EPER11 = new SNTexte();
    EPER12 = new SNTexte();
    lbForçage12 = new SNLabelUnite();
    lbForçage11 = new SNLabelUnite();
    lbForçage10 = new SNLabelUnite();
    lbForçage9 = new SNLabelUnite();
    EPER5 = new SNTexte();
    lbForçage5 = new SNLabelUnite();
    EPER13 = new SNTexte();
    lbForçage13 = new SNLabelUnite();
    EPER6 = new SNTexte();
    lbForçage6 = new SNLabelUnite();
    EPER14 = new SNTexte();
    lbForçage14 = new SNLabelUnite();
    EPER7 = new SNTexte();
    lbForçage7 = new SNLabelUnite();
    EPER15 = new SNTexte();
    lbForçage15 = new SNLabelUnite();
    EPER8 = new SNTexte();
    lbForçage8 = new SNLabelUnite();
    EPER16 = new SNTexte();
    lbForçage16 = new SNLabelUnite();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Comptabilisation r\u00e9guli\u00e8re des ventes");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlComptabilisation ========
        {
          pnlComptabilisation.setTitre("[Titre dans le code]");
          pnlComptabilisation.setName("pnlComptabilisation");
          pnlComptabilisation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlComptabilisation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlComptabilisation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlComptabilisation.getLayout()).columnWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlComptabilisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- J101 ----
          J101.setText("@J101@");
          J101.setMinimumSize(new Dimension(36, 30));
          J101.setPreferredSize(new Dimension(36, 30));
          J101.setEnabled(false);
          J101.setName("J101");
          pnlComptabilisation.add(J101, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J102 ----
          J102.setText("@J102@");
          J102.setMinimumSize(new Dimension(36, 30));
          J102.setPreferredSize(new Dimension(36, 30));
          J102.setEnabled(false);
          J102.setName("J102");
          pnlComptabilisation.add(J102, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J103 ----
          J103.setText("@J103@");
          J103.setMinimumSize(new Dimension(36, 30));
          J103.setPreferredSize(new Dimension(36, 30));
          J103.setEnabled(false);
          J103.setName("J103");
          pnlComptabilisation.add(J103, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J104 ----
          J104.setText("@J104@");
          J104.setMinimumSize(new Dimension(36, 30));
          J104.setPreferredSize(new Dimension(36, 30));
          J104.setEnabled(false);
          J104.setName("J104");
          pnlComptabilisation.add(J104, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J105 ----
          J105.setText("@J105@");
          J105.setMinimumSize(new Dimension(36, 30));
          J105.setPreferredSize(new Dimension(36, 30));
          J105.setEnabled(false);
          J105.setName("J105");
          pnlComptabilisation.add(J105, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J106 ----
          J106.setText("@J106@");
          J106.setMinimumSize(new Dimension(36, 30));
          J106.setPreferredSize(new Dimension(36, 30));
          J106.setEnabled(false);
          J106.setName("J106");
          pnlComptabilisation.add(J106, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J107 ----
          J107.setText("@J107@");
          J107.setMinimumSize(new Dimension(36, 30));
          J107.setPreferredSize(new Dimension(36, 30));
          J107.setEnabled(false);
          J107.setName("J107");
          pnlComptabilisation.add(J107, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J108 ----
          J108.setText("@J108@");
          J108.setMinimumSize(new Dimension(36, 30));
          J108.setPreferredSize(new Dimension(36, 30));
          J108.setEnabled(false);
          J108.setName("J108");
          pnlComptabilisation.add(J108, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J109 ----
          J109.setText("@J109@");
          J109.setMinimumSize(new Dimension(36, 30));
          J109.setPreferredSize(new Dimension(36, 30));
          J109.setEnabled(false);
          J109.setName("J109");
          pnlComptabilisation.add(J109, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J110 ----
          J110.setText("@J110@");
          J110.setMinimumSize(new Dimension(36, 30));
          J110.setPreferredSize(new Dimension(36, 30));
          J110.setEnabled(false);
          J110.setName("J110");
          pnlComptabilisation.add(J110, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J120 ----
          J120.setText("@J120@");
          J120.setMinimumSize(new Dimension(36, 30));
          J120.setPreferredSize(new Dimension(36, 30));
          J120.setEnabled(false);
          J120.setName("J120");
          pnlComptabilisation.add(J120, new GridBagConstraints(9, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J119 ----
          J119.setText("@J119@");
          J119.setMinimumSize(new Dimension(36, 30));
          J119.setPreferredSize(new Dimension(36, 30));
          J119.setEnabled(false);
          J119.setName("J119");
          pnlComptabilisation.add(J119, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J118 ----
          J118.setText("@J118@");
          J118.setMinimumSize(new Dimension(36, 30));
          J118.setPreferredSize(new Dimension(36, 30));
          J118.setEnabled(false);
          J118.setName("J118");
          pnlComptabilisation.add(J118, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J117 ----
          J117.setText("@J117@");
          J117.setMinimumSize(new Dimension(36, 30));
          J117.setPreferredSize(new Dimension(36, 30));
          J117.setEnabled(false);
          J117.setName("J117");
          pnlComptabilisation.add(J117, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J116 ----
          J116.setText("@J116@");
          J116.setMinimumSize(new Dimension(36, 30));
          J116.setPreferredSize(new Dimension(36, 30));
          J116.setEnabled(false);
          J116.setName("J116");
          pnlComptabilisation.add(J116, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J115 ----
          J115.setText("@J115@");
          J115.setMinimumSize(new Dimension(36, 30));
          J115.setPreferredSize(new Dimension(36, 30));
          J115.setEnabled(false);
          J115.setName("J115");
          pnlComptabilisation.add(J115, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J114 ----
          J114.setText("@J114@");
          J114.setMinimumSize(new Dimension(36, 30));
          J114.setPreferredSize(new Dimension(36, 30));
          J114.setEnabled(false);
          J114.setName("J114");
          pnlComptabilisation.add(J114, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J113 ----
          J113.setText("@J113@");
          J113.setMinimumSize(new Dimension(36, 30));
          J113.setPreferredSize(new Dimension(36, 30));
          J113.setEnabled(false);
          J113.setName("J113");
          pnlComptabilisation.add(J113, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J112 ----
          J112.setText("@J112@");
          J112.setMinimumSize(new Dimension(36, 30));
          J112.setPreferredSize(new Dimension(36, 30));
          J112.setEnabled(false);
          J112.setName("J112");
          pnlComptabilisation.add(J112, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J111 ----
          J111.setText("@J111@");
          J111.setMinimumSize(new Dimension(36, 30));
          J111.setPreferredSize(new Dimension(36, 30));
          J111.setEnabled(false);
          J111.setName("J111");
          pnlComptabilisation.add(J111, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J121 ----
          J121.setText("@J121@");
          J121.setMinimumSize(new Dimension(36, 30));
          J121.setPreferredSize(new Dimension(36, 30));
          J121.setEnabled(false);
          J121.setName("J121");
          pnlComptabilisation.add(J121, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J122 ----
          J122.setText("@J122@");
          J122.setMinimumSize(new Dimension(36, 30));
          J122.setPreferredSize(new Dimension(36, 30));
          J122.setEnabled(false);
          J122.setName("J122");
          pnlComptabilisation.add(J122, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J123 ----
          J123.setText("@J123@");
          J123.setMinimumSize(new Dimension(36, 30));
          J123.setPreferredSize(new Dimension(36, 30));
          J123.setEnabled(false);
          J123.setName("J123");
          pnlComptabilisation.add(J123, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J124 ----
          J124.setText("@J124@");
          J124.setMinimumSize(new Dimension(36, 30));
          J124.setPreferredSize(new Dimension(36, 30));
          J124.setEnabled(false);
          J124.setName("J124");
          pnlComptabilisation.add(J124, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J125 ----
          J125.setText("@J125@");
          J125.setMinimumSize(new Dimension(36, 30));
          J125.setPreferredSize(new Dimension(36, 30));
          J125.setEnabled(false);
          J125.setName("J125");
          pnlComptabilisation.add(J125, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J126 ----
          J126.setText("@J126@");
          J126.setMinimumSize(new Dimension(36, 30));
          J126.setPreferredSize(new Dimension(36, 30));
          J126.setEnabled(false);
          J126.setName("J126");
          pnlComptabilisation.add(J126, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J127 ----
          J127.setText("@J127@");
          J127.setMinimumSize(new Dimension(36, 30));
          J127.setPreferredSize(new Dimension(36, 30));
          J127.setEnabled(false);
          J127.setName("J127");
          pnlComptabilisation.add(J127, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J128 ----
          J128.setText("@J128@");
          J128.setMinimumSize(new Dimension(36, 30));
          J128.setPreferredSize(new Dimension(36, 30));
          J128.setEnabled(false);
          J128.setName("J128");
          pnlComptabilisation.add(J128, new GridBagConstraints(7, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J129 ----
          J129.setText("@J129@");
          J129.setMinimumSize(new Dimension(36, 30));
          J129.setPreferredSize(new Dimension(36, 30));
          J129.setEnabled(false);
          J129.setName("J129");
          pnlComptabilisation.add(J129, new GridBagConstraints(8, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J130 ----
          J130.setText("@J130@");
          J130.setMinimumSize(new Dimension(36, 30));
          J130.setPreferredSize(new Dimension(36, 30));
          J130.setEnabled(false);
          J130.setName("J130");
          pnlComptabilisation.add(J130, new GridBagConstraints(9, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- J131 ----
          J131.setText("@J131@");
          J131.setMinimumSize(new Dimension(36, 30));
          J131.setPreferredSize(new Dimension(36, 30));
          J131.setEnabled(false);
          J131.setName("J131");
          pnlComptabilisation.add(J131, new GridBagConstraints(10, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- MAJPCA ----
          MAJPCA.setText("Mise \u00e0 jour globale des comptes auxiliaires");
          MAJPCA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MAJPCA.setFont(new Font("sansserif", Font.PLAIN, 14));
          MAJPCA.setPreferredSize(new Dimension(294, 30));
          MAJPCA.setMinimumSize(new Dimension(294, 30));
          MAJPCA.setMaximumSize(new Dimension(294, 30));
          MAJPCA.setName("MAJPCA");
          pnlComptabilisation.add(MAJPCA, new GridBagConstraints(0, 3, 11, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlComptabilisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnCritereDeSelection ========
        {
          pnCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnCritereDeSelection.setName("pnCritereDeSelection");
          pnCritereDeSelection.setLayout(new GridBagLayout());
          ((GridBagLayout) pnCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnCritereDeSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin.setEnabled(false);
          snMagasin.setName("snMagasin");
          pnCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPeriodeAComptabiliser ----
          lbPeriodeAComptabiliser.setText("P\u00e9riode \u00e0 comptabiliser du");
          lbPeriodeAComptabiliser.setMinimumSize(new Dimension(175, 30));
          lbPeriodeAComptabiliser.setPreferredSize(new Dimension(175, 30));
          lbPeriodeAComptabiliser.setMaximumSize(new Dimension(175, 30));
          lbPeriodeAComptabiliser.setName("lbPeriodeAComptabiliser");
          pnCritereDeSelection.add(lbPeriodeAComptabiliser, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- UPER1 ----
          UPER1.setMinimumSize(new Dimension(30, 30));
          UPER1.setPreferredSize(new Dimension(30, 30));
          UPER1.setFont(new Font("sansserif", Font.PLAIN, 14));
          UPER1.setName("UPER1");
          pnCritereDeSelection.add(UPER1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbAu ----
          lbAu.setText("au");
          lbAu.setHorizontalAlignment(SwingConstants.CENTER);
          lbAu.setMinimumSize(new Dimension(16, 30));
          lbAu.setPreferredSize(new Dimension(16, 30));
          lbAu.setMaximumSize(new Dimension(16, 30));
          lbAu.setName("lbAu");
          pnCritereDeSelection.add(lbAu, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- UPER2 ----
          UPER2.setPreferredSize(new Dimension(30, 30));
          UPER2.setMinimumSize(new Dimension(30, 30));
          UPER2.setFont(new Font("sansserif", Font.PLAIN, 14));
          UPER2.setName("UPER2");
          pnCritereDeSelection.add(UPER2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPERIO1 ----
          lbPERIO1.setText("@PERIO1@");
          lbPERIO1.setMinimumSize(new Dimension(125, 30));
          lbPERIO1.setPreferredSize(new Dimension(125, 30));
          lbPERIO1.setMaximumSize(new Dimension(125, 30));
          lbPERIO1.setName("lbPERIO1");
          pnCritereDeSelection.add(lbPERIO1, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- FORCAG ----
          FORCAG.setText("Demande de for\u00e7age de la comptabilisation");
          FORCAG.setToolTipText("Demande for\u00e7age comptabilisation");
          FORCAG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FORCAG.setFont(new Font("sansserif", Font.PLAIN, 14));
          FORCAG.setMaximumSize(new Dimension(297, 30));
          FORCAG.setMinimumSize(new Dimension(297, 30));
          FORCAG.setPreferredSize(new Dimension(297, 30));
          FORCAG.setName("FORCAG");
          pnCritereDeSelection.add(FORCAG, new GridBagConstraints(0, 2, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnCritereDeSelection, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setEnabled(false);
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlRecapitulatifPeriode ========
        {
          pnlRecapitulatifPeriode.setTitre("R\u00e9capitulatif des p\u00e9riodes");
          pnlRecapitulatifPeriode.setName("pnlRecapitulatifPeriode");
          pnlRecapitulatifPeriode.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRecapitulatifPeriode.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRecapitulatifPeriode.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRecapitulatifPeriode.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRecapitulatifPeriode.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- EPER1 ----
          EPER1.setText("@EPER1@");
          EPER1.setPreferredSize(new Dimension(140, 30));
          EPER1.setMinimumSize(new Dimension(140, 30));
          EPER1.setEnabled(false);
          EPER1.setName("EPER1");
          pnlRecapitulatifPeriode.add(EPER1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER2 ----
          EPER2.setText("@EPER2@");
          EPER2.setPreferredSize(new Dimension(140, 30));
          EPER2.setMinimumSize(new Dimension(140, 30));
          EPER2.setEnabled(false);
          EPER2.setName("EPER2");
          pnlRecapitulatifPeriode.add(EPER2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER3 ----
          EPER3.setText("@EPER3@");
          EPER3.setPreferredSize(new Dimension(140, 30));
          EPER3.setMinimumSize(new Dimension(140, 30));
          EPER3.setEnabled(false);
          EPER3.setName("EPER3");
          pnlRecapitulatifPeriode.add(EPER3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER4 ----
          EPER4.setText("@EPER4@");
          EPER4.setPreferredSize(new Dimension(140, 30));
          EPER4.setMinimumSize(new Dimension(140, 30));
          EPER4.setEnabled(false);
          EPER4.setName("EPER4");
          pnlRecapitulatifPeriode.add(EPER4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage4 ----
          lbForçage4.setText("For\u00e7age");
          lbForçage4.setForeground(Color.red);
          lbForçage4.setMaximumSize(new Dimension(50, 30));
          lbForçage4.setPreferredSize(new Dimension(60, 30));
          lbForçage4.setMinimumSize(new Dimension(60, 30));
          lbForçage4.setName("lbFor\u00e7age4");
          pnlRecapitulatifPeriode.add(lbForçage4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage3 ----
          lbForçage3.setText("For\u00e7age");
          lbForçage3.setForeground(Color.red);
          lbForçage3.setMaximumSize(new Dimension(50, 30));
          lbForçage3.setPreferredSize(new Dimension(60, 30));
          lbForçage3.setMinimumSize(new Dimension(60, 30));
          lbForçage3.setName("lbFor\u00e7age3");
          pnlRecapitulatifPeriode.add(lbForçage3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage2 ----
          lbForçage2.setText("For\u00e7age");
          lbForçage2.setForeground(Color.red);
          lbForçage2.setMaximumSize(new Dimension(50, 30));
          lbForçage2.setPreferredSize(new Dimension(60, 30));
          lbForçage2.setMinimumSize(new Dimension(60, 30));
          lbForçage2.setName("lbFor\u00e7age2");
          pnlRecapitulatifPeriode.add(lbForçage2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage1 ----
          lbForçage1.setText("For\u00e7age");
          lbForçage1.setForeground(Color.red);
          lbForçage1.setMaximumSize(new Dimension(50, 30));
          lbForçage1.setPreferredSize(new Dimension(60, 30));
          lbForçage1.setMinimumSize(new Dimension(60, 30));
          lbForçage1.setName("lbFor\u00e7age1");
          pnlRecapitulatifPeriode.add(lbForçage1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER9 ----
          EPER9.setText("@EPER9@");
          EPER9.setPreferredSize(new Dimension(140, 30));
          EPER9.setMinimumSize(new Dimension(140, 30));
          EPER9.setEnabled(false);
          EPER9.setName("EPER9");
          pnlRecapitulatifPeriode.add(EPER9, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER10 ----
          EPER10.setText("@EPER10@");
          EPER10.setPreferredSize(new Dimension(140, 30));
          EPER10.setMinimumSize(new Dimension(140, 30));
          EPER10.setEnabled(false);
          EPER10.setName("EPER10");
          pnlRecapitulatifPeriode.add(EPER10, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER11 ----
          EPER11.setText("@EPER11@");
          EPER11.setPreferredSize(new Dimension(140, 30));
          EPER11.setMinimumSize(new Dimension(140, 30));
          EPER11.setEnabled(false);
          EPER11.setName("EPER11");
          pnlRecapitulatifPeriode.add(EPER11, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER12 ----
          EPER12.setText("@EPER12@");
          EPER12.setPreferredSize(new Dimension(140, 30));
          EPER12.setMinimumSize(new Dimension(140, 30));
          EPER12.setEnabled(false);
          EPER12.setName("EPER12");
          pnlRecapitulatifPeriode.add(EPER12, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage12 ----
          lbForçage12.setText("For\u00e7age");
          lbForçage12.setForeground(Color.red);
          lbForçage12.setMaximumSize(new Dimension(50, 30));
          lbForçage12.setPreferredSize(new Dimension(60, 30));
          lbForçage12.setMinimumSize(new Dimension(60, 30));
          lbForçage12.setName("lbFor\u00e7age12");
          pnlRecapitulatifPeriode.add(lbForçage12, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbForçage11 ----
          lbForçage11.setText("For\u00e7age");
          lbForçage11.setForeground(Color.red);
          lbForçage11.setMaximumSize(new Dimension(50, 30));
          lbForçage11.setPreferredSize(new Dimension(60, 30));
          lbForçage11.setMinimumSize(new Dimension(60, 30));
          lbForçage11.setName("lbFor\u00e7age11");
          pnlRecapitulatifPeriode.add(lbForçage11, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbForçage10 ----
          lbForçage10.setText("For\u00e7age");
          lbForçage10.setForeground(Color.red);
          lbForçage10.setMaximumSize(new Dimension(50, 30));
          lbForçage10.setPreferredSize(new Dimension(60, 30));
          lbForçage10.setMinimumSize(new Dimension(60, 30));
          lbForçage10.setName("lbFor\u00e7age10");
          pnlRecapitulatifPeriode.add(lbForçage10, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbForçage9 ----
          lbForçage9.setText("For\u00e7age");
          lbForçage9.setForeground(Color.red);
          lbForçage9.setMaximumSize(new Dimension(50, 30));
          lbForçage9.setPreferredSize(new Dimension(60, 30));
          lbForçage9.setMinimumSize(new Dimension(60, 30));
          lbForçage9.setName("lbFor\u00e7age9");
          pnlRecapitulatifPeriode.add(lbForçage9, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EPER5 ----
          EPER5.setText("@EPER5@");
          EPER5.setPreferredSize(new Dimension(140, 30));
          EPER5.setMinimumSize(new Dimension(140, 30));
          EPER5.setEnabled(false);
          EPER5.setName("EPER5");
          pnlRecapitulatifPeriode.add(EPER5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage5 ----
          lbForçage5.setText("For\u00e7age");
          lbForçage5.setForeground(Color.red);
          lbForçage5.setMaximumSize(new Dimension(50, 30));
          lbForçage5.setPreferredSize(new Dimension(60, 30));
          lbForçage5.setMinimumSize(new Dimension(60, 30));
          lbForçage5.setName("lbFor\u00e7age5");
          pnlRecapitulatifPeriode.add(lbForçage5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER13 ----
          EPER13.setText("@EPER13@");
          EPER13.setPreferredSize(new Dimension(140, 30));
          EPER13.setMinimumSize(new Dimension(140, 30));
          EPER13.setEnabled(false);
          EPER13.setName("EPER13");
          pnlRecapitulatifPeriode.add(EPER13, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage13 ----
          lbForçage13.setText("For\u00e7age");
          lbForçage13.setForeground(Color.red);
          lbForçage13.setMaximumSize(new Dimension(50, 30));
          lbForçage13.setPreferredSize(new Dimension(60, 30));
          lbForçage13.setMinimumSize(new Dimension(60, 30));
          lbForçage13.setName("lbFor\u00e7age13");
          pnlRecapitulatifPeriode.add(lbForçage13, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EPER6 ----
          EPER6.setText("@EPER6@");
          EPER6.setPreferredSize(new Dimension(140, 30));
          EPER6.setMinimumSize(new Dimension(140, 30));
          EPER6.setEnabled(false);
          EPER6.setName("EPER6");
          pnlRecapitulatifPeriode.add(EPER6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage6 ----
          lbForçage6.setText("For\u00e7age");
          lbForçage6.setForeground(Color.red);
          lbForçage6.setMaximumSize(new Dimension(50, 30));
          lbForçage6.setPreferredSize(new Dimension(60, 30));
          lbForçage6.setMinimumSize(new Dimension(60, 30));
          lbForçage6.setName("lbFor\u00e7age6");
          pnlRecapitulatifPeriode.add(lbForçage6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER14 ----
          EPER14.setText("@EPER14@");
          EPER14.setPreferredSize(new Dimension(140, 30));
          EPER14.setMinimumSize(new Dimension(140, 30));
          EPER14.setEnabled(false);
          EPER14.setName("EPER14");
          pnlRecapitulatifPeriode.add(EPER14, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage14 ----
          lbForçage14.setText("For\u00e7age");
          lbForçage14.setForeground(Color.red);
          lbForçage14.setMaximumSize(new Dimension(50, 30));
          lbForçage14.setPreferredSize(new Dimension(60, 30));
          lbForçage14.setMinimumSize(new Dimension(60, 30));
          lbForçage14.setName("lbFor\u00e7age14");
          pnlRecapitulatifPeriode.add(lbForçage14, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EPER7 ----
          EPER7.setText("@EPER7@");
          EPER7.setPreferredSize(new Dimension(140, 30));
          EPER7.setMinimumSize(new Dimension(140, 30));
          EPER7.setEnabled(false);
          EPER7.setName("EPER7");
          pnlRecapitulatifPeriode.add(EPER7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage7 ----
          lbForçage7.setText("For\u00e7age");
          lbForçage7.setForeground(Color.red);
          lbForçage7.setMaximumSize(new Dimension(50, 30));
          lbForçage7.setPreferredSize(new Dimension(60, 30));
          lbForçage7.setMinimumSize(new Dimension(60, 30));
          lbForçage7.setName("lbFor\u00e7age7");
          pnlRecapitulatifPeriode.add(lbForçage7, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EPER15 ----
          EPER15.setText("@EPER15@");
          EPER15.setPreferredSize(new Dimension(140, 30));
          EPER15.setMinimumSize(new Dimension(140, 30));
          EPER15.setEnabled(false);
          EPER15.setName("EPER15");
          pnlRecapitulatifPeriode.add(EPER15, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbForçage15 ----
          lbForçage15.setText("For\u00e7age");
          lbForçage15.setForeground(Color.red);
          lbForçage15.setMaximumSize(new Dimension(50, 30));
          lbForçage15.setPreferredSize(new Dimension(60, 30));
          lbForçage15.setMinimumSize(new Dimension(60, 30));
          lbForçage15.setName("lbFor\u00e7age15");
          pnlRecapitulatifPeriode.add(lbForçage15, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EPER8 ----
          EPER8.setText("@EPER8@");
          EPER8.setPreferredSize(new Dimension(140, 30));
          EPER8.setMinimumSize(new Dimension(140, 30));
          EPER8.setEnabled(false);
          EPER8.setName("EPER8");
          pnlRecapitulatifPeriode.add(EPER8, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbForçage8 ----
          lbForçage8.setText("For\u00e7age");
          lbForçage8.setForeground(Color.red);
          lbForçage8.setMaximumSize(new Dimension(50, 30));
          lbForçage8.setPreferredSize(new Dimension(60, 30));
          lbForçage8.setMinimumSize(new Dimension(60, 30));
          lbForçage8.setName("lbFor\u00e7age8");
          pnlRecapitulatifPeriode.add(lbForçage8, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EPER16 ----
          EPER16.setText("@EPER16@");
          EPER16.setPreferredSize(new Dimension(140, 30));
          EPER16.setMinimumSize(new Dimension(140, 30));
          EPER16.setEnabled(false);
          EPER16.setName("EPER16");
          pnlRecapitulatifPeriode.add(EPER16, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbForçage16 ----
          lbForçage16.setText("For\u00e7age");
          lbForçage16.setForeground(Color.red);
          lbForçage16.setMaximumSize(new Dimension(50, 30));
          lbForçage16.setPreferredSize(new Dimension(60, 30));
          lbForçage16.setMinimumSize(new Dimension(60, 30));
          lbForçage16.setName("lbFor\u00e7age16");
          pnlRecapitulatifPeriode.add(lbForçage16, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlRecapitulatifPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setEnabled(false);
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlComptabilisation;
  private SNTexte J101;
  private SNTexte J102;
  private SNTexte J103;
  private SNTexte J104;
  private SNTexte J105;
  private SNTexte J106;
  private SNTexte J107;
  private SNTexte J108;
  private SNTexte J109;
  private SNTexte J110;
  private SNTexte J120;
  private SNTexte J119;
  private SNTexte J118;
  private SNTexte J117;
  private SNTexte J116;
  private SNTexte J115;
  private SNTexte J114;
  private SNTexte J113;
  private SNTexte J112;
  private SNTexte J111;
  private SNTexte J121;
  private SNTexte J122;
  private SNTexte J123;
  private SNTexte J124;
  private SNTexte J125;
  private SNTexte J126;
  private SNTexte J127;
  private SNTexte J128;
  private SNTexte J129;
  private SNTexte J130;
  private SNTexte J131;
  private XRiCheckBox MAJPCA;
  private SNPanelTitre pnCritereDeSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbPeriodeAComptabiliser;
  private XRiTextField UPER1;
  private SNLabelChamp lbAu;
  private XRiTextField UPER2;
  private SNLabelUnite lbPERIO1;
  private XRiCheckBox FORCAG;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNPanelTitre pnlRecapitulatifPeriode;
  private SNTexte EPER1;
  private SNTexte EPER2;
  private SNTexte EPER3;
  private SNTexte EPER4;
  private SNLabelUnite lbForçage4;
  private SNLabelUnite lbForçage3;
  private SNLabelUnite lbForçage2;
  private SNLabelUnite lbForçage1;
  private SNTexte EPER9;
  private SNTexte EPER10;
  private SNTexte EPER11;
  private SNTexte EPER12;
  private SNLabelUnite lbForçage12;
  private SNLabelUnite lbForçage11;
  private SNLabelUnite lbForçage10;
  private SNLabelUnite lbForçage9;
  private SNTexte EPER5;
  private SNLabelUnite lbForçage5;
  private SNTexte EPER13;
  private SNLabelUnite lbForçage13;
  private SNTexte EPER6;
  private SNLabelUnite lbForçage6;
  private SNTexte EPER14;
  private SNLabelUnite lbForçage14;
  private SNTexte EPER7;
  private SNLabelUnite lbForçage7;
  private SNTexte EPER15;
  private SNLabelUnite lbForçage15;
  private SNTexte EPER8;
  private SNLabelUnite lbForçage8;
  private SNTexte EPER16;
  private SNLabelUnite lbForçage16;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
