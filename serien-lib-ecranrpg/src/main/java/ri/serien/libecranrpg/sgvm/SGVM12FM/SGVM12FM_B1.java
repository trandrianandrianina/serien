
package ri.serien.libecranrpg.sgvm.SGVM12FM;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM12FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public SGVM12FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    DEMGEN.setValeursSelection("OUI", "NON");
    WTOU.setValeursSelection("**", "");
    
    // Initialisation de la barre des boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation du composant article début
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "DEBLO2");
    // Si on a choisi "tous" on cache les champs de sélection de plage
    if (WTOU.isSelected()) {
      lbArticle1.setVisible(false);
      snArticleDebut.setVisible(false);
    }
    else {
      lbArticle1.setVisible(true);
      snArticleDebut.setEnabled(true);
    }
    
    // Initialisation du composant article début
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "FINLO2");
    // Si on a choisi "tous" on cache les champs de sélection de plage
    if (WTOU.isSelected()) {
      lbArticle1.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else {
      lbArticle1.setVisible(true);
      snArticleFin.setVisible(true);
    }
    
    pnlCritereSelectionArticle.setVisible(lexique.isTrue("92"));
    // Les sélections suivantes ne sont plus au menu (grisées). On les garde en cas de réactivation, il faudra alors les compléter.
    pnlCritereSelectionMot1.setVisible(lexique.isTrue("91"));
    pnlCritereSelectionFamille.setVisible(lexique.isTrue("93"));
    pnlCritereSelectionMot2.setVisible(lexique.isTrue("94"));
    
    p_bpresentation.setIdEtablissement(snEtablissement.getIdSelection());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snArticleDebut.renseignerChampRPG(lexique, "DEBLO2");
    snArticleFin.renseignerChampRPG(lexique, "FINLO2");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    try {
      // Si on a choisi "tous" on cache les champs de sélection de plage
      if (WTOU.isSelected()) {
        lbArticle1.setVisible(false);
        lbArticle2.setVisible(false);
        snArticleDebut.setVisible(false);
        snArticleFin.setVisible(false);
      }
      else {
        lbArticle1.setVisible(true);
        lbArticle2.setVisible(true);
        snArticleDebut.setVisible(true);
        snArticleFin.setVisible(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelectionArticle = new SNPanelTitre();
    WTOU = new XRiCheckBox();
    lbArticle1 = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticle2 = new SNLabelChamp();
    snArticleFin = new SNArticle();
    pnlCritereSelectionMot1 = new SNPanelTitre();
    lbArticle3 = new SNLabelChamp();
    lbArticle4 = new SNLabelChamp();
    pnlCritereSelectionMot2 = new SNPanelTitre();
    lbArticle9 = new SNLabelChamp();
    lbArticle10 = new SNLabelChamp();
    pnlCritereSelectionFamille = new SNPanelTitre();
    lbArticle5 = new SNLabelChamp();
    lbArticle6 = new SNLabelChamp();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlEtatAvoir = new SNPanelTitre();
    DEMGEN = new XRiCheckBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlCritereSelectionArticle ========
        {
          pnlCritereSelectionArticle.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelectionArticle.setName("pnlCritereSelectionArticle");
          pnlCritereSelectionArticle.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- WTOU ----
          WTOU.setText("Tous les articles");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTOU.setMinimumSize(new Dimension(124, 30));
          WTOU.setPreferredSize(new Dimension(124, 30));
          WTOU.setName("WTOU");
          WTOU.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTOUItemStateChanged(e);
            }
          });
          pnlCritereSelectionArticle.add(WTOU, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticle1 ----
          lbArticle1.setText("Article de d\u00e9but");
          lbArticle1.setName("lbArticle1");
          pnlCritereSelectionArticle.add(lbArticle1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleDebut ----
          snArticleDebut.setName("snArticleDebut");
          pnlCritereSelectionArticle.add(snArticleDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticle2 ----
          lbArticle2.setText("Article de fin");
          lbArticle2.setName("lbArticle2");
          pnlCritereSelectionArticle.add(lbArticle2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snArticleFin ----
          snArticleFin.setName("snArticleFin");
          pnlCritereSelectionArticle.add(snArticleFin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelectionArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCritereSelectionMot1 ========
        {
          pnlCritereSelectionMot1.setTitre("Crit\u00e8res de s\u00e9lection (plus au menu)");
          pnlCritereSelectionMot1.setName("pnlCritereSelectionMot1");
          pnlCritereSelectionMot1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelectionMot1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionMot1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionMot1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelectionMot1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbArticle3 ----
          lbArticle3.setText("Mot de classement de d\u00e9but");
          lbArticle3.setName("lbArticle3");
          pnlCritereSelectionMot1.add(lbArticle3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbArticle4 ----
          lbArticle4.setText("Mot de classement de fin");
          lbArticle4.setName("lbArticle4");
          pnlCritereSelectionMot1.add(lbArticle4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlGauche.add(pnlCritereSelectionMot1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCritereSelectionMot2 ========
        {
          pnlCritereSelectionMot2.setTitre("Crit\u00e8res de s\u00e9lection (plus au menu)");
          pnlCritereSelectionMot2.setName("pnlCritereSelectionMot2");
          pnlCritereSelectionMot2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelectionMot2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionMot2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionMot2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelectionMot2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbArticle9 ----
          lbArticle9.setText("Mot de classement de d\u00e9but");
          lbArticle9.setName("lbArticle9");
          pnlCritereSelectionMot2.add(lbArticle9, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbArticle10 ----
          lbArticle10.setText("Mot de classement de fin");
          lbArticle10.setName("lbArticle10");
          pnlCritereSelectionMot2.add(lbArticle10, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlGauche.add(pnlCritereSelectionMot2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCritereSelectionFamille ========
        {
          pnlCritereSelectionFamille.setTitre("Crit\u00e8res de s\u00e9lection (plus au menu)");
          pnlCritereSelectionFamille.setName("pnlCritereSelectionFamille");
          pnlCritereSelectionFamille.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelectionFamille.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionFamille.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelectionFamille.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelectionFamille.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbArticle5 ----
          lbArticle5.setText("Famille de d\u00e9but");
          lbArticle5.setName("lbArticle5");
          pnlCritereSelectionFamille.add(lbArticle5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbArticle6 ----
          lbArticle6.setText("Famille de fin");
          lbArticle6.setName("lbArticle6");
          pnlCritereSelectionFamille.add(lbArticle6, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlGauche.add(pnlCritereSelectionFamille, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfEnCours ----
          tfEnCours.setText("@WENCX@");
          tfEnCours.setEnabled(false);
          tfEnCours.setPreferredSize(new Dimension(260, 30));
          tfEnCours.setMinimumSize(new Dimension(260, 30));
          tfEnCours.setMaximumSize(new Dimension(260, 30));
          tfEnCours.setName("tfEnCours");
          pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlEtatAvoir ========
        {
          pnlEtatAvoir.setTitre("Options");
          pnlEtatAvoir.setName("pnlEtatAvoir");
          pnlEtatAvoir.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtatAvoir.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtatAvoir.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtatAvoir.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtatAvoir.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- DEMGEN ----
          DEMGEN.setText("Edition du gencod");
          DEMGEN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DEMGEN.setFont(new Font("sansserif", Font.PLAIN, 14));
          DEMGEN.setMinimumSize(new Dimension(124, 30));
          DEMGEN.setPreferredSize(new Dimension(124, 30));
          DEMGEN.setName("DEMGEN");
          pnlEtatAvoir.add(DEMGEN, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtatAvoir, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelectionArticle;
  private XRiCheckBox WTOU;
  private SNLabelChamp lbArticle1;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticle2;
  private SNArticle snArticleFin;
  private SNPanelTitre pnlCritereSelectionMot1;
  private SNLabelChamp lbArticle3;
  private SNLabelChamp lbArticle4;
  private SNPanelTitre pnlCritereSelectionMot2;
  private SNLabelChamp lbArticle9;
  private SNLabelChamp lbArticle10;
  private SNPanelTitre pnlCritereSelectionFamille;
  private SNLabelChamp lbArticle5;
  private SNLabelChamp lbArticle6;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlEtatAvoir;
  private XRiCheckBox DEMGEN;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
