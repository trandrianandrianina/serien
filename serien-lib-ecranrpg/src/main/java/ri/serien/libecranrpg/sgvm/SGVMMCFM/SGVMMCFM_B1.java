
package ri.serien.libecranrpg.sgvm.SGVMMCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMMCFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVMMCFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    ADSLIV.setValeursSelection("X", "");
    ADSFAC.setValeursSelection("X", "");
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_23.setVisible(lexique.isPresent("WENCX"));
    OBJ_24.setVisible(lexique.isPresent("DGNOM"));
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_19 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_20 = new JXTitledSeparator();
    OBJ_24 = new RiZoneSortie();
    OBJ_23 = new RiZoneSortie();
    OBJ_31 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_25 = new JLabel();
    EBON = new XRiTextField();
    ANCCLI = new XRiTextField();
    NOUCLI = new XRiTextField();
    WETB = new XRiTextField();
    ANCLIV = new XRiTextField();
    NOULIV = new XRiTextField();
    ESUF = new XRiTextField();
    riBoutonRecherche1 = new SNBoutonRecherche();
    OBJ_22 = new JXTitledSeparator();
    OBJ_27 = new JLabel();
    ANCTFA = new XRiTextField();
    NOUTFA = new XRiTextField();
    OBJ_30 = new JLabel();
    OBJ_26 = new JXTitledSeparator();
    ADSLIV = new XRiCheckBox();
    ADSFAC = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- OBJ_19 ----
          OBJ_19.setTitle("Codes client");
          OBJ_19.setName("OBJ_19");
          p_contenu.add(OBJ_19);
          OBJ_19.setBounds(35, 300, 515, OBJ_19.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setTitle("Etablissement s\u00e9lectionn\u00e9");
          OBJ_21.setName("OBJ_21");
          p_contenu.add(OBJ_21);
          OBJ_21.setBounds(30, 30, 515, OBJ_21.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setTitle("Bon");
          OBJ_20.setName("OBJ_20");
          p_contenu.add(OBJ_20);
          OBJ_20.setBounds(30, 127, 515, OBJ_20.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("@DGNOM@");
          OBJ_24.setName("OBJ_24");
          p_contenu.add(OBJ_24);
          OBJ_24.setBounds(210, 62, 260, OBJ_24.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("@WENCX@");
          OBJ_23.setName("OBJ_23");
          p_contenu.add(OBJ_23);
          OBJ_23.setBounds(210, 92, 260, OBJ_23.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("Nouveau code client livr\u00e9");
          OBJ_31.setName("OBJ_31");
          p_contenu.add(OBJ_31);
          OBJ_31.setBounds(60, 356, 190, 28);

          //---- OBJ_28 ----
          OBJ_28.setText("Ancien code client livr\u00e9");
          OBJ_28.setName("OBJ_28");
          p_contenu.add(OBJ_28);
          OBJ_28.setBounds(60, 326, 190, 28);

          //---- OBJ_25 ----
          OBJ_25.setText("Num\u00e9ro de bon");
          OBJ_25.setName("OBJ_25");
          p_contenu.add(OBJ_25);
          OBJ_25.setBounds(60, 150, 96, 28);

          //---- EBON ----
          EBON.setComponentPopupMenu(BTD);
          EBON.setName("EBON");
          p_contenu.add(EBON);
          EBON.setBounds(250, 150, 60, EBON.getPreferredSize().height);

          //---- ANCCLI ----
          ANCCLI.setComponentPopupMenu(BTD);
          ANCCLI.setName("ANCCLI");
          p_contenu.add(ANCCLI);
          ANCCLI.setBounds(250, 326, 60, ANCCLI.getPreferredSize().height);

          //---- NOUCLI ----
          NOUCLI.setComponentPopupMenu(BTD);
          NOUCLI.setName("NOUCLI");
          p_contenu.add(NOUCLI);
          NOUCLI.setBounds(250, 356, 60, NOUCLI.getPreferredSize().height);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_contenu.add(WETB);
          WETB.setBounds(60, 72, 40, WETB.getPreferredSize().height);

          //---- ANCLIV ----
          ANCLIV.setComponentPopupMenu(BTD);
          ANCLIV.setName("ANCLIV");
          p_contenu.add(ANCLIV);
          ANCLIV.setBounds(310, 326, 34, ANCLIV.getPreferredSize().height);

          //---- NOULIV ----
          NOULIV.setComponentPopupMenu(BTD);
          NOULIV.setName("NOULIV");
          p_contenu.add(NOULIV);
          NOULIV.setBounds(310, 356, 34, NOULIV.getPreferredSize().height);

          //---- ESUF ----
          ESUF.setComponentPopupMenu(BTD);
          ESUF.setName("ESUF");
          p_contenu.add(ESUF);
          ESUF.setBounds(310, 150, 20, ESUF.getPreferredSize().height);

          //---- riBoutonRecherche1 ----
          riBoutonRecherche1.setName("riBoutonRecherche1");
          p_contenu.add(riBoutonRecherche1);
          riBoutonRecherche1.setBounds(new Rectangle(new Point(105, 72), riBoutonRecherche1.getPreferredSize()));

          //---- OBJ_22 ----
          OBJ_22.setTitle("Types de facturation");
          OBJ_22.setName("OBJ_22");
          p_contenu.add(OBJ_22);
          OBJ_22.setBounds(30, 195, 515, OBJ_22.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("Ancien type de facturation");
          OBJ_27.setName("OBJ_27");
          p_contenu.add(OBJ_27);
          OBJ_27.setBounds(60, 220, 190, 28);

          //---- ANCTFA ----
          ANCTFA.setComponentPopupMenu(BTD);
          ANCTFA.setName("ANCTFA");
          p_contenu.add(ANCTFA);
          ANCTFA.setBounds(250, 220, 20, ANCTFA.getPreferredSize().height);

          //---- NOUTFA ----
          NOUTFA.setComponentPopupMenu(BTD);
          NOUTFA.setName("NOUTFA");
          p_contenu.add(NOUTFA);
          NOUTFA.setBounds(250, 255, 20, NOUTFA.getPreferredSize().height);

          //---- OBJ_30 ----
          OBJ_30.setText("Nouveau type de facturation");
          OBJ_30.setName("OBJ_30");
          p_contenu.add(OBJ_30);
          OBJ_30.setBounds(60, 255, 190, 28);

          //---- OBJ_26 ----
          OBJ_26.setTitle("Adresses");
          OBJ_26.setName("OBJ_26");
          p_contenu.add(OBJ_26);
          OBJ_26.setBounds(35, 400, 515, OBJ_26.getPreferredSize().height);

          //---- ADSLIV ----
          ADSLIV.setText("Conservation de l'adresse de livraison saisie");
          ADSLIV.setName("ADSLIV");
          p_contenu.add(ADSLIV);
          ADSLIV.setBounds(60, 425, 330, 23);

          //---- ADSFAC ----
          ADSFAC.setText("Conservation de l'adresse de facturation saisie");
          ADSFAC.setName("ADSFAC");
          p_contenu.add(ADSFAC);
          ADSFAC.setBounds(60, 455, 330, 23);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_20;
  private RiZoneSortie OBJ_24;
  private RiZoneSortie OBJ_23;
  private JLabel OBJ_31;
  private JLabel OBJ_28;
  private JLabel OBJ_25;
  private XRiTextField EBON;
  private XRiTextField ANCCLI;
  private XRiTextField NOUCLI;
  private XRiTextField WETB;
  private XRiTextField ANCLIV;
  private XRiTextField NOULIV;
  private XRiTextField ESUF;
  private SNBoutonRecherche riBoutonRecherche1;
  private JXTitledSeparator OBJ_22;
  private JLabel OBJ_27;
  private XRiTextField ANCTFA;
  private XRiTextField NOUTFA;
  private JLabel OBJ_30;
  private JXTitledSeparator OBJ_26;
  private XRiCheckBox ADSLIV;
  private XRiCheckBox ADSFAC;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
