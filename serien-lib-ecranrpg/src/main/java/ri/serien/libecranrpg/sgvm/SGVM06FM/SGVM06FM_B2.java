
package ri.serien.libecranrpg.sgvm.SGVM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM06FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM06FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    OPT6.setValeurs("6", "OPT6");
    OPT5.setValeurs("5", "OPT5");
    OPT8.setValeurs("8", "OPT8");
    OPT4.setValeurs("4", "OPT4");
    OPT3.setValeurs("3", "OPT3");
    OPT7.setValeurs("7", "OPT7");
    OPT2.setValeurs("2", "OPT2");
    OPT1.setValeurs("1", "OPT1");
    WTOUS.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_64.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    OBJ_73.setVisible(lexique.isPresent("WTOU"));
    OBJ_81.setVisible(lexique.isPresent("WTOU"));
    OBJ_72.setVisible(lexique.isPresent("WTOUS"));
    OBJ_70.setVisible(lexique.isPresent("WTOUS"));
    
    panel4.setVisible(lexique.isTrue("39"));
    panel5.setVisible(lexique.isTrue("32"));
    OBJ_73.setVisible(lexique.isTrue("92"));
    OBJ_74.setVisible(!lexique.isTrue("92"));
    
    if (lexique.isTrue("32")) {
      OBJ_64.setTitle("Plage de famille à traiter");
    }
    if (lexique.isTrue("39")) {
      OBJ_64.setTitle("Plage de sous-famille à traiter");
    }
    
    if (lexique.isTrue("92")) {
      OPT2.setText("Article d'une famille");
    }
    else {
      OPT2.setText("Article par famille");
    }
    
    // ***** Visibilié panels
    P_SEL7.setVisible(!WTOU.isSelected());
    panel6.setVisible(!WTOUS.isSelected());
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL7.setVisible(!P_SEL7.isVisible());
  }
  
  private void WTOUSActionPerformed(ActionEvent e) {
    panel6.setVisible(!panel6.isVisible());
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_64 = new JXTitledSeparator();
    panel5 = new JPanel();
    WTOU = new XRiCheckBox();
    P_SEL7 = new JPanel();
    FAMFIN = new XRiTextField();
    FAMDEB = new XRiTextField();
    OBJ_73 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_74 = new JLabel();
    panel4 = new JPanel();
    WTOUS = new XRiCheckBox();
    panel6 = new JPanel();
    OBJ_70 = new JLabel();
    OBJ_72 = new JLabel();
    SFADEB = new XRiTextField();
    SFAFIN = new XRiTextField();
    OBJ_52 = new JXTitledSeparator();
    OBJ_46 = new JXTitledSeparator();
    OPTABC = new XRiComboBox();
    OPTAB1 = new XRiComboBox();
    OBJ_61 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    WDEV = new XRiTextField();
    WDEV1 = new XRiTextField();
    ATDAPX = new XRiCalendrier();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    panel1 = new JPanel();
    OPT1 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT7 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    OPT4 = new XRiRadioButton();
    OPT8 = new XRiRadioButton();
    OPT5 = new XRiRadioButton();
    OPT6 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    RB_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
          lb_loctp_.setBounds(new Rectangle(new Point(190, 5), lb_loctp_.getPreferredSize()));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 200));
          menus_haut.setPreferredSize(new Dimension(160, 200));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt_export ----
            riSousMenu_bt_export.setText("Exportation tableur");
            riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
            riSousMenu_bt_export.setName("riSousMenu_bt_export");
            riSousMenu_bt_export.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt_exportActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt_export);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_64 ----
          OBJ_64.setTitle("@PLAG@");
          OBJ_64.setName("OBJ_64");

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setRequestFocusEnabled(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setComponentPopupMenu(BTD);
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel5.add(WTOU);
            WTOU.setBounds(25, 15, 141, 15);

            //======== P_SEL7 ========
            {
              P_SEL7.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL7.setOpaque(false);
              P_SEL7.setName("P_SEL7");
              P_SEL7.setLayout(null);

              //---- FAMFIN ----
              FAMFIN.setComponentPopupMenu(BTD);
              FAMFIN.setName("FAMFIN");
              P_SEL7.add(FAMFIN);
              FAMFIN.setBounds(215, 35, 40, FAMFIN.getPreferredSize().height);

              //---- FAMDEB ----
              FAMDEB.setComponentPopupMenu(BTD);
              FAMDEB.setName("FAMDEB");
              P_SEL7.add(FAMDEB);
              FAMDEB.setBounds(215, 5, 40, FAMDEB.getPreferredSize().height);

              //---- OBJ_73 ----
              OBJ_73.setText("Famille \u00e0 \u00e9diter");
              OBJ_73.setName("OBJ_73");
              P_SEL7.add(OBJ_73);
              OBJ_73.setBounds(15, 10, 151, 18);

              //---- OBJ_81 ----
              OBJ_81.setText("Code famille de fin");
              OBJ_81.setName("OBJ_81");
              P_SEL7.add(OBJ_81);
              OBJ_81.setBounds(15, 40, 159, 18);

              //---- OBJ_74 ----
              OBJ_74.setText("Code famille de d\u00e9but");
              OBJ_74.setName("OBJ_74");
              P_SEL7.add(OBJ_74);
              OBJ_74.setBounds(15, 10, 151, 18);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL7.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL7.setMinimumSize(preferredSize);
                P_SEL7.setPreferredSize(preferredSize);
              }
            }
            panel5.add(P_SEL7);
            P_SEL7.setBounds(25, 40, 300, 70);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- WTOUS ----
            WTOUS.setText("S\u00e9lection compl\u00e8te");
            WTOUS.setComponentPopupMenu(BTD);
            WTOUS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUS.setName("WTOUS");
            WTOUS.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUSActionPerformed(e);
              }
            });
            panel4.add(WTOUS);
            WTOUS.setBounds(25, 15, 141, 15);

            //======== panel6 ========
            {
              panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- OBJ_70 ----
              OBJ_70.setText("Code sous-famille de d\u00e9but");
              OBJ_70.setName("OBJ_70");
              panel6.add(OBJ_70);
              OBJ_70.setBounds(15, 10, 205, 18);

              //---- OBJ_72 ----
              OBJ_72.setText("Code sous-famille de fin");
              OBJ_72.setName("OBJ_72");
              panel6.add(OBJ_72);
              OBJ_72.setBounds(15, 40, 205, 18);

              //---- SFADEB ----
              SFADEB.setComponentPopupMenu(BTD);
              SFADEB.setName("SFADEB");
              panel6.add(SFADEB);
              SFADEB.setBounds(220, 5, 60, SFADEB.getPreferredSize().height);

              //---- SFAFIN ----
              SFAFIN.setComponentPopupMenu(BTD);
              SFAFIN.setName("SFAFIN");
              panel6.add(SFAFIN);
              SFAFIN.setBounds(220, 35, 60, SFAFIN.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel6);
            panel6.setBounds(25, 40, 330, 70);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_52 ----
          OBJ_52.setTitle("Options possibles dans un tarif");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_46 ----
          OBJ_46.setTitle("");
          OBJ_46.setName("OBJ_46");

          //---- OPTABC ----
          OPTABC.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTABC.setComponentPopupMenu(BTD);
          OPTABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTABC.setName("OPTABC");

          //---- OPTAB1 ----
          OPTAB1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "G",
            "N",
            "T"
          }));
          OPTAB1.setComponentPopupMenu(BTD);
          OPTAB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTAB1.setName("OPTAB1");

          //---- OBJ_61 ----
          OBJ_61.setText("Filtre sur code ABC de l'article");
          OBJ_61.setName("OBJ_61");

          //---- OBJ_47 ----
          OBJ_47.setText("S\u00e9lection date d'application");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_49 ----
          OBJ_49.setText("Devises");
          OBJ_49.setName("OBJ_49");

          //---- WDEV ----
          WDEV.setComponentPopupMenu(BTD);
          WDEV.setName("WDEV");

          //---- WDEV1 ----
          WDEV1.setComponentPopupMenu(BTD);
          WDEV1.setName("WDEV1");

          //---- ATDAPX ----
          ATDAPX.setName("ATDAPX");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OPT1 ----
            OPT1.setText("Toutes les r\u00e9f\u00e9rences");
            OPT1.setComponentPopupMenu(BTD);
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel1.add(OPT1);
            OPT1.setBounds(10, 10, 282, 18);

            //---- OPT2 ----
            OPT2.setText("Articles par famille");
            OPT2.setComponentPopupMenu(BTD);
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel1.add(OPT2);
            OPT2.setBounds(10, 29, 282, 18);

            //---- OPT7 ----
            OPT7.setText("Articles par sous-famille");
            OPT7.setComponentPopupMenu(BTD);
            OPT7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT7.setName("OPT7");
            panel1.add(OPT7);
            OPT7.setBounds(10, 48, 282, 20);

            //---- OPT3 ----
            OPT3.setText("Articles sur mot de classement 1");
            OPT3.setComponentPopupMenu(BTD);
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setName("OPT3");
            panel1.add(OPT3);
            OPT3.setBounds(10, 69, 282, 18);

            //---- OPT4 ----
            OPT4.setText("Articles de m\u00eame mot classement 2");
            OPT4.setComponentPopupMenu(BTD);
            OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT4.setName("OPT4");
            panel1.add(OPT4);
            OPT4.setBounds(10, 88, 282, 18);

            //---- OPT8 ----
            OPT8.setText("Articles par mot class.2 et famille");
            OPT8.setComponentPopupMenu(BTD);
            OPT8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT8.setName("OPT8");
            panel1.add(OPT8);
            OPT8.setBounds(10, 107, 282, 18);

            //---- OPT5 ----
            OPT5.setText("Articles de m\u00eame rattachement CNV");
            OPT5.setComponentPopupMenu(BTD);
            OPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT5.setName("OPT5");
            panel1.add(OPT5);
            OPT5.setBounds(10, 126, 282, 18);

            //---- OPT6 ----
            OPT6.setText("Articles sur zones personnalis\u00e9es");
            OPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT6.setName("OPT6");
            panel1.add(OPT6);
            OPT6.setBounds(10, 145, 282, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(44, 44, 44)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(82, 82, 82)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(44, 44, 44)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                    .addGap(21, 21, 21)
                    .addComponent(ATDAPX, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(44, 44, 44)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(150, 150, 150)
                    .addComponent(WDEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(WDEV1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(75, 75, 75)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                    .addGap(21, 21, 21)
                    .addComponent(OPTABC, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(OPTAB1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))))
                .addGap(129, 129, 129))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ATDAPX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WDEV1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OPTABC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OPTAB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //---- RB_GRP ----
    RB_GRP.add(OPT1);
    RB_GRP.add(OPT2);
    RB_GRP.add(OPT7);
    RB_GRP.add(OPT3);
    RB_GRP.add(OPT4);
    RB_GRP.add(OPT8);
    RB_GRP.add(OPT5);
    RB_GRP.add(OPT6);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_64;
  private JPanel panel5;
  private XRiCheckBox WTOU;
  private JPanel P_SEL7;
  private XRiTextField FAMFIN;
  private XRiTextField FAMDEB;
  private JLabel OBJ_73;
  private JLabel OBJ_81;
  private JLabel OBJ_74;
  private JPanel panel4;
  private XRiCheckBox WTOUS;
  private JPanel panel6;
  private JLabel OBJ_70;
  private JLabel OBJ_72;
  private XRiTextField SFADEB;
  private XRiTextField SFAFIN;
  private JXTitledSeparator OBJ_52;
  private JXTitledSeparator OBJ_46;
  private XRiComboBox OPTABC;
  private XRiComboBox OPTAB1;
  private JLabel OBJ_61;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private XRiTextField WDEV;
  private XRiTextField WDEV1;
  private XRiCalendrier ATDAPX;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JPanel panel1;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT7;
  private XRiRadioButton OPT3;
  private XRiRadioButton OPT4;
  private XRiRadioButton OPT8;
  private XRiRadioButton OPT5;
  private XRiRadioButton OPT6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private ButtonGroup RB_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
