
package ri.serien.libecranrpg.sgvm.SGVML1FM;
// Nom Fichier: pop_SGVML1FM_FMTB1_FMTF1_795.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVML1FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVML1FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    NBLOC.setEnabled(lexique.isPresent("NBLOC"));
    WECH.setEnabled(lexique.isPresent("WECH"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_23.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="F12"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvml1"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel3 = new JPanel();
    panel1 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_23 = new JButton();
    panel4 = new JPanel();
    panel2 = new JPanel();
    OBJ_16 = new JLabel();
    WECH = new XRiTextField();
    OBJ_18 = new JLabel();
    NBLOC = new XRiTextField();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    P_PnlOpts = new JPanel();

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== panel3 ========
    {
      panel3.setName("panel3");
      panel3.setLayout(null);

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("Ok");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_22ActionPerformed(e);
          }
        });
        panel1.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        //---- OBJ_23 ----
        OBJ_23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_23.setToolTipText("Retour");
        OBJ_23.setName("OBJ_23");
        OBJ_23.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_23ActionPerformed(e);
          }
        });
        panel1.add(OBJ_23);
        OBJ_23.setBounds(60, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      panel3.add(panel1);
      panel1.setBounds(150, 5, 120, 50);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel3.getComponentCount(); i++) {
          Rectangle bounds = panel3.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel3.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel3.setMinimumSize(preferredSize);
        panel3.setPreferredSize(preferredSize);
      }
    }
    add(panel3, BorderLayout.SOUTH);

    //======== panel4 ========
    {
      panel4.setName("panel4");
      panel4.setLayout(null);

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder(""));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- OBJ_16 ----
        OBJ_16.setText("P\u00e9riodicit\u00e9 de facturation");
        OBJ_16.setName("OBJ_16");
        panel2.add(OBJ_16);
        OBJ_16.setBounds(15, 15, 169, 20);

        //---- WECH ----
        WECH.setComponentPopupMenu(BTD);
        WECH.setName("WECH");
        panel2.add(WECH);
        WECH.setBounds(210, 11, 26, WECH.getPreferredSize().height);

        //---- OBJ_18 ----
        OBJ_18.setText("Nombre de locations");
        OBJ_18.setName("OBJ_18");
        panel2.add(OBJ_18);
        OBJ_18.setBounds(15, 45, 147, 20);

        //---- NBLOC ----
        NBLOC.setComponentPopupMenu(BTD);
        NBLOC.setName("NBLOC");
        panel2.add(NBLOC);
        NBLOC.setBounds(210, 41, 26, NBLOC.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      panel4.add(panel2);
      panel2.setBounds(5, 5, 260, 85);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel4.getComponentCount(); i++) {
          Rectangle bounds = panel4.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel4.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel4.setMinimumSize(preferredSize);
        panel4.setPreferredSize(preferredSize);
      }
    }
    add(panel4, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      P_PnlOpts.setPreferredSize(new Dimension(55, 78));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel3;
  private JPanel panel1;
  private JButton BT_ENTER;
  private JButton OBJ_23;
  private JPanel panel4;
  private JPanel panel2;
  private JLabel OBJ_16;
  private XRiTextField WECH;
  private JLabel OBJ_18;
  private XRiTextField NBLOC;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JPanel P_PnlOpts;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
