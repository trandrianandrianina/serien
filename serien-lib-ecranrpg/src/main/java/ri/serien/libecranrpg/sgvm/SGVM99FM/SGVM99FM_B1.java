
package ri.serien.libecranrpg.sgvm.SGVM99FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM99FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVM99FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ESTA3.setValeurs("3", "RB");
    ESTA2.setValeurs("2", "RB");
    ESTA1.setValeurs("1", "RB");
    WTOU2.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
    WTOUR.setValeursSelection("**", "  ");
    WTOUC.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    ESTA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Statistiques jusqu'au @SDAT1@")).trim());
    ESTA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Statistiques jusqu'au @SDAT2@")).trim());
    ESTA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Statistiques jusqu'au @SDAT3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    OBJ_39.setVisible(lexique.isPresent("WTOUC"));
    OBJ_38.setVisible(lexique.isPresent("WTOUR"));
    
    // WTOUR.setSelected(lexique.HostFieldGetData("WTOUR").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTOUR.isSelected() & WTOUR.isVisible());
    // WTOUC.setSelected(lexique.HostFieldGetData("WTOUC").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!WTOUC.isSelected() & WTOUC.isVisible());
    
    // ESTA3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // ESTA2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // ESTA1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!WTOU1.isSelected() & WTOU1.isVisible());
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    P_SEL3.setVisible(!WTOU2.isSelected() & WTOU2.isVisible());
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU2", 0, " ");
    // if (WTOU1.isSelected())
    // lexique.HostFieldPutData("WTOU1", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU1", 0, " ");
    // if (WTOUR.isSelected())
    // lexique.HostFieldPutData("WTOUR", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUR", 0, " ");
    // if (WTOUC.isSelected())
    // lexique.HostFieldPutData("WTOUC", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUC", 0, " ");
    // if (ESTA3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (ESTA2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (ESTA1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOURActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
  }
  
  private void WTOUCActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_54 = new JXTitledSeparator();
    OBJ_38 = new JXTitledSeparator();
    OBJ_39 = new JXTitledSeparator();
    OBJ_58 = new JXTitledSeparator();
    OBJ_59 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    OBJ_64 = new JLabel();
    OBJ_70 = new JLabel();
    EDEBD = new XRiTextField();
    EFIND = new XRiTextField();
    P_SEL3 = new JPanel();
    OBJ_66 = new JLabel();
    OBJ_72 = new JLabel();
    EDEBF = new XRiTextField();
    EFINF = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_44 = new JLabel();
    ECAT = new XRiTextField();
    ESTA1 = new XRiRadioButton();
    ESTA2 = new XRiRadioButton();
    ESTA3 = new XRiRadioButton();
    WTOUC = new XRiCheckBox();
    WTOUR = new XRiCheckBox();
    WTOU1 = new XRiCheckBox();
    WTOU2 = new XRiCheckBox();
    P_SEL0 = new JPanel();
    OBJ_43 = new JLabel();
    EREP = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(520, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_54 ----
          OBJ_54.setTitle("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_54.setName("OBJ_54");

          //---- OBJ_38 ----
          OBJ_38.setTitle("Repr\u00e9sentant \u00e0 traiter");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_39 ----
          OBJ_39.setTitle("Cat\u00e9gories clients \u00e0 traiter");
          OBJ_39.setName("OBJ_39");

          //---- OBJ_58 ----
          OBJ_58.setTitle("Plage de d\u00e9partements \u00e0 traiter");
          OBJ_58.setName("OBJ_58");

          //---- OBJ_59 ----
          OBJ_59.setTitle("Plage de familles \u00e0 traiter");
          OBJ_59.setName("OBJ_59");

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- OBJ_64 ----
            OBJ_64.setText("N\u00b0 d\u00e9partement de d\u00e9but");
            OBJ_64.setName("OBJ_64");
            P_SEL2.add(OBJ_64);
            OBJ_64.setBounds(15, 10, 150, 18);

            //---- OBJ_70 ----
            OBJ_70.setText("N\u00b0 d\u00e9partement de fin");
            OBJ_70.setName("OBJ_70");
            P_SEL2.add(OBJ_70);
            OBJ_70.setBounds(15, 40, 125, 18);

            //---- EDEBD ----
            EDEBD.setComponentPopupMenu(BTD);
            EDEBD.setName("EDEBD");
            P_SEL2.add(EDEBD);
            EDEBD.setBounds(205, 5, 28, EDEBD.getPreferredSize().height);

            //---- EFIND ----
            EFIND.setComponentPopupMenu(BTD);
            EFIND.setName("EFIND");
            P_SEL2.add(EFIND);
            EFIND.setBounds(205, 35, 28, EFIND.getPreferredSize().height);
          }

          //======== P_SEL3 ========
          {
            P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL3.setOpaque(false);
            P_SEL3.setName("P_SEL3");
            P_SEL3.setLayout(null);

            //---- OBJ_66 ----
            OBJ_66.setText("Code famille de d\u00e9but");
            OBJ_66.setName("OBJ_66");
            P_SEL3.add(OBJ_66);
            OBJ_66.setBounds(15, 10, 123, 18);

            //---- OBJ_72 ----
            OBJ_72.setText("Code famille de fin");
            OBJ_72.setName("OBJ_72");
            P_SEL3.add(OBJ_72);
            OBJ_72.setBounds(15, 40, 123, 18);

            //---- EDEBF ----
            EDEBF.setComponentPopupMenu(BTD);
            EDEBF.setName("EDEBF");
            P_SEL3.add(EDEBF);
            EDEBF.setBounds(205, 5, 44, EDEBF.getPreferredSize().height);

            //---- EFINF ----
            EFINF.setComponentPopupMenu(BTD);
            EFINF.setName("EFINF");
            P_SEL3.add(EFINF);
            EFINF.setBounds(205, 35, 44, EFINF.getPreferredSize().height);
          }

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_44 ----
            OBJ_44.setText("Code cat\u00e9gorie");
            OBJ_44.setName("OBJ_44");
            P_SEL1.add(OBJ_44);
            OBJ_44.setBounds(15, 10, 114, 18);

            //---- ECAT ----
            ECAT.setComponentPopupMenu(BTD);
            ECAT.setName("ECAT");
            P_SEL1.add(ECAT);
            ECAT.setBounds(205, 5, 44, ECAT.getPreferredSize().height);
          }

          //---- ESTA1 ----
          ESTA1.setText("Statistiques jusqu'au @SDAT1@");
          ESTA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESTA1.setName("ESTA1");

          //---- ESTA2 ----
          ESTA2.setText("Statistiques jusqu'au @SDAT2@");
          ESTA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESTA2.setName("ESTA2");

          //---- ESTA3 ----
          ESTA3.setText("Statistiques jusqu'au @SDAT3@");
          ESTA3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESTA3.setName("ESTA3");

          //---- WTOUC ----
          WTOUC.setText("S\u00e9lection compl\u00e8te");
          WTOUC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUC.setName("WTOUC");
          WTOUC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUCActionPerformed(e);
            }
          });

          //---- WTOUR ----
          WTOUR.setText("S\u00e9lection compl\u00e8te");
          WTOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUR.setName("WTOUR");
          WTOUR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOURActionPerformed(e);
            }
          });

          //---- WTOU1 ----
          WTOU1.setText("S\u00e9lection compl\u00e8te");
          WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU1.setName("WTOU1");
          WTOU1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU1ActionPerformed(e);
            }
          });

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU2.setName("WTOU2");
          WTOU2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU2ActionPerformed(e);
            }
          });

          //======== P_SEL0 ========
          {
            P_SEL0.setOpaque(false);
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_43 ----
            OBJ_43.setText("Code repr\u00e9sentant");
            OBJ_43.setName("OBJ_43");
            P_SEL0.add(OBJ_43);
            OBJ_43.setBounds(15, 10, 114, 18);

            //---- EREP ----
            EREP.setComponentPopupMenu(BTD);
            EREP.setName("EREP");
            P_SEL0.add(EREP);
            EREP.setBounds(205, 5, 34, EREP.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WTOUC, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WTOUR, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ESTA1, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ESTA2, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ESTA3, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(WTOUC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTOUR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(ESTA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(ESTA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(ESTA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(26, 26, 26)
                    .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(26, 26, 26)
                    .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(ESTA1);
    RB_GRP.add(ESTA2);
    RB_GRP.add(ESTA3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_54;
  private JXTitledSeparator OBJ_38;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_58;
  private JXTitledSeparator OBJ_59;
  private JPanel P_SEL2;
  private JLabel OBJ_64;
  private JLabel OBJ_70;
  private XRiTextField EDEBD;
  private XRiTextField EFIND;
  private JPanel P_SEL3;
  private JLabel OBJ_66;
  private JLabel OBJ_72;
  private XRiTextField EDEBF;
  private XRiTextField EFINF;
  private JPanel P_SEL1;
  private JLabel OBJ_44;
  private XRiTextField ECAT;
  private XRiRadioButton ESTA1;
  private XRiRadioButton ESTA2;
  private XRiRadioButton ESTA3;
  private XRiCheckBox WTOUC;
  private XRiCheckBox WTOUR;
  private XRiCheckBox WTOU1;
  private XRiCheckBox WTOU2;
  private JPanel P_SEL0;
  private JLabel OBJ_43;
  private XRiTextField EREP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
