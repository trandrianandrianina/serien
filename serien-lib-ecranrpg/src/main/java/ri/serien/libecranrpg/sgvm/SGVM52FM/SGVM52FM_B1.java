
package ri.serien.libecranrpg.sgvm.SGVM52FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM52FM_B1 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  private boolean isModeClient = false;
  private boolean isModeFamille = false;
  private boolean isModePlanning = false;
  
  public SGVM52FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    WTOUC.setValeursSelection("**", "  ");
    WTOUA.setValeursSelection("**", "  ");
    EDTMAV.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    lexique.setNomFichierTableur(
        lexique.HostFieldGetData("TITPG1").trim().replace('/', '_') + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    isModePlanning = lexique.isTrue("90");
    
    rafraichirEtablissement();
    IdEtablissement idEtablissement = snEtablissement.getIdSelection();
    
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Mode client
    isModeClient = lexique.isTrue("94");
    // Mode familles
    isModeFamille = lexique.isTrue("N94");
    
    WTOUC.setVisible(isModeClient);
    lbClient.setVisible(isModeClient);
    snClient.setVisible(isModeClient);
    if (isModeClient) {
      rafraichirClient();
    }
    
    WTOU.setVisible(isModeFamille);
    WTOUA.setVisible(isModeFamille);
    lbFamille.setVisible(isModeFamille);
    snFamille.setVisible(isModeFamille);
    lbArticles.setVisible(isModeFamille);
    snArticle1.setVisible(isModeFamille);
    snArticle2.setVisible(isModeFamille);
    pnlOptionsEdition.setVisible(EDTMAV.isVisible());
    if (isModeFamille) {
      rafraichirFamille();
      rafraichirArticle1();
      rafraichirArticle2();
    }
    
    // Message planning
    lbPlanning.setVisible(isModePlanning);
    if (isModePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
      // boites à choix date paramétrée
      cbDatePlanning.removeAllItems();
      cbDatePlanning.addItem("");
      cbDatePlanning.addItem("Date du jour");
      cbDatePlanning.addItem("Début du mois en cours");
      cbDatePlanning.addItem("Début du mois précédent");
      cbDatePlanning.addItem("Fin du mois précédent");
      cbDatePlanning2.removeAllItems();
      cbDatePlanning2.addItem("");
      cbDatePlanning2.addItem("Date du jour");
      cbDatePlanning2.addItem("Début du mois en cours");
      cbDatePlanning2.addItem("Début du mois précédent");
      cbDatePlanning2.addItem("Fin du mois précédent");
      if (Constantes.normerTexte(lexique.HostFieldGetData("PERDEB")).startsWith("*")) {
        rbDate.setSelected(false);
        rbDateParametree.setSelected(true);
      }
      else {
        rbDate.setSelected(true);
        rbDateParametree.setSelected(false);
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("PERDEB")).equals("*DAT")) {
        cbDatePlanning.setSelectedItem("Date du jour");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("PERDEB")).equals("*DME")) {
        cbDatePlanning.setSelectedItem("Début du mois en cours");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("PERDEB")).equals("*DMP")) {
        cbDatePlanning.setSelectedItem("Début du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("PERDEB")).equals("*FMP")) {
        cbDatePlanning.setSelectedItem("Fin du mois précédent");
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("PERFIN")).equals("*DAT")) {
        cbDatePlanning2.setSelectedItem("Date du jour");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("PERFIN")).equals("*DME")) {
        cbDatePlanning2.setSelectedItem("Début du mois en cours");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("PERFIN")).equals("*DMP")) {
        cbDatePlanning2.setSelectedItem("Début du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("PERFIN")).equals("*FMP")) {
        cbDatePlanning2.setSelectedItem("Fin du mois précédent");
      }
    }
    
    // Mode planning
    lbDate.setVisible(!isModePlanning);
    rbDate.setVisible(isModePlanning);
    rbDateParametree.setVisible(isModePlanning);
    pnlDateParametree.setVisible(isModePlanning);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (isModeClient) {
      snClient.renseignerChampRPG(lexique, "CLI", "LIV");
    }
    else {
      snFamille.renseignerChampRPG(lexique, "FAM");
      snArticle1.renseignerChampRPG(lexique, "ARTDEB");
      snArticle2.renseignerChampRPG(lexique, "ARTFIN");
    }
    
    if (isModePlanning) {
      switch (cbDatePlanning.getSelectedIndex()) {
        case 1:
          lexique.HostFieldPutData("PERDEB", 0, "*DAT");
          break;
        case 2:
          lexique.HostFieldPutData("PERDEB", 0, "*DME");
          break;
        case 3:
          lexique.HostFieldPutData("PERDEB", 0, "*DMP");
          break;
        case 4:
          lexique.HostFieldPutData("PERDEB", 0, "*FMP");
          break;
        
        default:
          break;
      }
      switch (cbDatePlanning2.getSelectedIndex()) {
        case 1:
          lexique.HostFieldPutData("PERFIN", 0, "*DAT");
          break;
        case 2:
          lexique.HostFieldPutData("PERFIN", 0, "*DME");
          break;
        case 3:
          lexique.HostFieldPutData("PERFIN", 0, "*DMP");
          break;
        case 4:
          lexique.HostFieldPutData("PERFIN", 0, "*FMP");
          break;
        
        default:
          break;
      }
    }
  }
  
  /**
   * Initialise le composant établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  private void rafraichirClient() {
    snClient.setSession(getSession());
    snClient.setIdEtablissement(snEtablissement.getIdSelection());
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "CLI", "LIV");
    snClient.setEnabled(!WTOUC.isSelected());
  }
  
  private void rafraichirFamille() {
    snFamille.setSession(getSession());
    snFamille.setIdEtablissement(snEtablissement.getIdSelection());
    snFamille.charger(false);
    snFamille.setSelectionParChampRPG(lexique, "FAM");
    snFamille.setEnabled(!WTOU.isSelected());
  }
  
  private void rafraichirArticle1() {
    snArticle1.setSession(getSession());
    snArticle1.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle1.charger(false);
    snArticle1.setSelectionParChampRPG(lexique, "ARTDEB");
    snArticle1.setEnabled(!WTOUA.isSelected());
  }
  
  private void rafraichirArticle2() {
    snArticle2.setSession(getSession());
    snArticle2.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle2.charger(false);
    snArticle2.setSelectionParChampRPG(lexique, "ARTFIN");
    snArticle2.setEnabled(!WTOUA.isSelected());
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUCActionPerformed(ActionEvent e) {
    try {
      if (WTOUC.isSelected()) {
        snClient.setSelection(null);
      }
      snClient.setEnabled(!WTOUC.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    try {
      if (WTOU.isSelected()) {
        snFamille.setSelection(null);
      }
      snFamille.setEnabled(!WTOU.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUAActionPerformed(ActionEvent e) {
    try {
      if (WTOUA.isSelected()) {
        snArticle1.setSelection(null);
        snArticle2.setSelection(null);
      }
      snArticle1.setEnabled(!WTOUA.isSelected());
      snArticle2.setEnabled(!WTOUA.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void PERDEBInputMethodTextChanged(InputMethodEvent e) {
    try {
      cbDatePlanning.setSelectedIndex(0);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void PERFINInputMethodTextChanged(InputMethodEvent e) {
    try {
      cbDatePlanning2.setSelectedIndex(0);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbDateItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbDateParametreeItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void modifierParametrageDate() {
    if (rbDate.isSelected()) {
      PERDEB.setEnabled(true);
      PERFIN.setEnabled(true);
      PERDEB.setDate(null);
      PERFIN.setDate(null);
      cbDatePlanning.setSelectedIndex(0);
      cbDatePlanning.setEnabled(false);
      cbDatePlanning2.setSelectedIndex(0);
      cbDatePlanning2.setEnabled(false);
    }
    else if (rbDateParametree.isSelected()) {
      lexique.HostFieldPutData("PERDEB", 0, "");
      lexique.HostFieldPutData("PERFIN", 0, "");
      PERDEB.setDate(null);
      PERDEB.setEnabled(false);
      PERFIN.setDate(null);
      PERFIN.setEnabled(false);
      cbDatePlanning.setEnabled(true);
      cbDatePlanning2.setEnabled(true);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    sNPanel2 = new SNPanel();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlSelection = new SNPanelTitre();
    pnlLibelleDate = new SNPanel();
    rbDate = new SNRadioButton();
    lbDate = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    lbDu = new SNLabelChamp();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    rbDateParametree = new SNRadioButton();
    pnlDateParametree = new SNPanel();
    cbDatePlanning = new SNComboBox();
    lbAu2 = new SNLabelChamp();
    cbDatePlanning2 = new SNComboBox();
    sNPanel1 = new SNPanel();
    WTOUC = new XRiCheckBox();
    snClient = new SNClient();
    lbClient = new SNLabelChamp();
    WTOU = new XRiCheckBox();
    lbFamille = new SNLabelChamp();
    snFamille = new SNFamille();
    WTOUA = new XRiCheckBox();
    lbArticles = new SNLabelChamp();
    snArticle1 = new SNArticle();
    snArticle2 = new SNArticle();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfDateEnCours = new SNTexte();
    pnlOptionsEdition = new SNPanelTitre();
    EDTMAV = new XRiCheckBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(980, 630));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      p_nord.add(bpPresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== sNPanel2 ========
    {
      sNPanel2.setName("sNPanel2");
      sNPanel2.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ---- lbPlanning ----
      lbPlanning.setText("Label Planning");
      lbPlanning.setName("lbPlanning");
      sNPanel2.add(lbPlanning,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlColonne ========
      {
        pnlColonne.setPreferredSize(new Dimension(870, 500));
        pnlColonne.setBackground(new Color(239, 239, 222));
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlSelection ========
          {
            pnlSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlSelection.setName("pnlSelection");
            pnlSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlLibelleDate ========
            {
              pnlLibelleDate.setName("pnlLibelleDate");
              pnlLibelleDate.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- rbDate ----
              rbDate.setText("P\u00e9riode \u00e0 \u00e9diter");
              rbDate.setPreferredSize(new Dimension(150, 30));
              rbDate.setMinimumSize(new Dimension(150, 30));
              rbDate.setMaximumSize(new Dimension(250, 30));
              rbDate.setName("rbDate");
              rbDate.addItemListener(e -> rbDateItemStateChanged(e));
              pnlLibelleDate.add(rbDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDate ----
              lbDate.setText("P\u00e9riode \u00e0 \u00e9diter");
              lbDate.setName("lbDate");
              pnlLibelleDate.add(lbDate, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlSelection.add(pnlLibelleDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setOpaque(false);
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbDu ----
              lbDu.setText("du");
              lbDu.setPreferredSize(new Dimension(30, 30));
              lbDu.setMinimumSize(new Dimension(30, 30));
              lbDu.setMaximumSize(new Dimension(30, 30));
              lbDu.setName("lbDu");
              pnlPeriodeAEditer.add(lbDu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERDEB ----
              PERDEB.setPreferredSize(new Dimension(125, 30));
              PERDEB.setMinimumSize(new Dimension(150, 30));
              PERDEB.setMaximumSize(new Dimension(150, 30));
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setName("PERDEB");
              PERDEB.addInputMethodListener(new InputMethodListener() {
                @Override
                public void caretPositionChanged(InputMethodEvent e) {
                }
                
                @Override
                public void inputMethodTextChanged(InputMethodEvent e) {
                  PERDEBInputMethodTextChanged(e);
                }
              });
              pnlPeriodeAEditer.add(PERDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setPreferredSize(new Dimension(30, 30));
              lbAu.setMinimumSize(new Dimension(30, 30));
              lbAu.setMaximumSize(new Dimension(30, 30));
              lbAu.setName("lbAu");
              pnlPeriodeAEditer.add(lbAu, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PERFIN ----
              PERFIN.setPreferredSize(new Dimension(125, 30));
              PERFIN.setMinimumSize(new Dimension(150, 30));
              PERFIN.setMaximumSize(new Dimension(150, 30));
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setName("PERFIN");
              PERFIN.addInputMethodListener(new InputMethodListener() {
                @Override
                public void caretPositionChanged(InputMethodEvent e) {
                }
                
                @Override
                public void inputMethodTextChanged(InputMethodEvent e) {
                  PERFINInputMethodTextChanged(e);
                }
              });
              pnlPeriodeAEditer.add(PERFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbDateParametree ----
            rbDateParametree.setText("ou date param\u00e9r\u00e9e");
            rbDateParametree.setPreferredSize(new Dimension(200, 30));
            rbDateParametree.setMinimumSize(new Dimension(200, 30));
            rbDateParametree.setMaximumSize(new Dimension(250, 30));
            rbDateParametree.setName("rbDateParametree");
            rbDateParametree.addItemListener(e -> rbDateParametreeItemStateChanged(e));
            pnlSelection.add(rbDateParametree, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateParametree ========
            {
              pnlDateParametree.setName("pnlDateParametree");
              pnlDateParametree.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- cbDatePlanning ----
              cbDatePlanning.setName("cbDatePlanning");
              pnlDateParametree.add(cbDatePlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu2 ----
              lbAu2.setText("\u00e0");
              lbAu2.setPreferredSize(new Dimension(30, 30));
              lbAu2.setMinimumSize(new Dimension(30, 30));
              lbAu2.setMaximumSize(new Dimension(30, 30));
              lbAu2.setName("lbAu2");
              pnlDateParametree.add(lbAu2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbDatePlanning2 ----
              cbDatePlanning2.setName("cbDatePlanning2");
              pnlDateParametree.add(cbDatePlanning2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlSelection.add(pnlDateParametree, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- WTOUC ----
              WTOUC.setText("Tous les clients");
              WTOUC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTOUC.setFont(new Font("sansserif", Font.PLAIN, 14));
              WTOUC.setName("WTOUC");
              WTOUC.addActionListener(e -> WTOUCActionPerformed(e));
              sNPanel1.add(WTOUC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- snClient ----
              snClient.setName("snClient");
              sNPanel1.add(snClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlSelection.add(sNPanel1, new GridBagConstraints(1, 2, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClient ----
            lbClient.setText("Client");
            lbClient.setName("lbClient");
            pnlSelection.add(lbClient, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WTOU ----
            WTOU.setText("Toutes les familles");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTOU.setName("WTOU");
            WTOU.addActionListener(e -> WTOUActionPerformed(e));
            pnlSelection.add(WTOU, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamille ----
            lbFamille.setText("Famille");
            lbFamille.setName("lbFamille");
            pnlSelection.add(lbFamille, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamille ----
            snFamille.setName("snFamille");
            pnlSelection.add(snFamille, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WTOUA ----
            WTOUA.setText("Tous les articles");
            WTOUA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUA.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTOUA.setName("WTOUA");
            WTOUA.addActionListener(e -> WTOUAActionPerformed(e));
            pnlSelection.add(WTOUA, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticles ----
            lbArticles.setText("Plage d'articles");
            lbArticles.setName("lbArticles");
            pnlSelection.add(lbArticles, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticle1 ----
            snArticle1.setName("snArticle1");
            pnlSelection.add(snArticle1, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snArticle2 ----
            snArticle2.setName("snArticle2");
            pnlSelection.add(snArticle2, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionne ========
          {
            pnlEtablissementSelectionne.setTitre("Etablissement");
            pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
            pnlEtablissementSelectionne.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
            pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissementSelectionne.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfDateEnCours ----
            tfDateEnCours.setText("@WENCX@");
            tfDateEnCours.setPreferredSize(new Dimension(260, 30));
            tfDateEnCours.setMinimumSize(new Dimension(260, 30));
            tfDateEnCours.setEditable(false);
            tfDateEnCours.setEnabled(false);
            tfDateEnCours.setName("tfDateEnCours");
            pnlEtablissementSelectionne.add(tfDateEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionsEdition ========
          {
            pnlOptionsEdition.setOpaque(false);
            pnlOptionsEdition.setTitre("Options d'\u00e9dition");
            pnlOptionsEdition.setName("pnlOptionsEdition");
            pnlOptionsEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- EDTMAV ----
            EDTMAV.setText("Edition de la marge en valeur");
            EDTMAV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTMAV.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTMAV.setName("EDTMAV");
            pnlOptionsEdition.add(EDTMAV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionsEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      sNPanel2.add(pnlColonne,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanel2, BorderLayout.CENTER);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbDate);
    buttonGroup1.add(rbDateParametree);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre bpPresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanel sNPanel2;
  private SNMessage lbPlanning;
  private SNPanelContenu pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlSelection;
  private SNPanel pnlLibelleDate;
  private SNRadioButton rbDate;
  private SNLabelChamp lbDate;
  private SNPanel pnlPeriodeAEditer;
  private SNLabelChamp lbDu;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNRadioButton rbDateParametree;
  private SNPanel pnlDateParametree;
  private SNComboBox cbDatePlanning;
  private SNLabelChamp lbAu2;
  private SNComboBox cbDatePlanning2;
  private SNPanel sNPanel1;
  private XRiCheckBox WTOUC;
  private SNClient snClient;
  private SNLabelChamp lbClient;
  private XRiCheckBox WTOU;
  private SNLabelChamp lbFamille;
  private SNFamille snFamille;
  private XRiCheckBox WTOUA;
  private SNLabelChamp lbArticles;
  private SNArticle snArticle1;
  private SNArticle snArticle2;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfDateEnCours;
  private SNPanelTitre pnlOptionsEdition;
  private XRiCheckBox EDTMAV;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
