
package ri.serien.libecranrpg.sgvm.SGVM3MFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVM3MFM_P9 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVM3MFM_P9(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    setDialog(true);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    pnlAffichagePagesBons.setTitre(lexique.TranslationTable(interpreteurD.analyseExpression("@TITLE@")).trim());
    tfNombrePages.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NPAGE@")).trim());
    tfNombreBons.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBBON@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlAffichagePagesBons = new SNPanelTitre();
    lbNombrePages = new SNLabelChamp();
    tfNombrePages = new SNTexte();
    lbNombreBons = new SNLabelChamp();
    tfNombreBons = new SNTexte();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(480, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlAffichagePagesBons ========
      {
        pnlAffichagePagesBons.setTitre("@TITLE@");
        pnlAffichagePagesBons.setName("pnlAffichagePagesBons");
        pnlAffichagePagesBons.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlAffichagePagesBons.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlAffichagePagesBons.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlAffichagePagesBons.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlAffichagePagesBons.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbNombrePages ----
        lbNombrePages.setText("Nombre de pages");
        lbNombrePages.setName("lbNombrePages");
        pnlAffichagePagesBons.add(lbNombrePages, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- tfNombrePages ----
        tfNombrePages.setEditable(false);
        tfNombrePages.setEnabled(false);
        tfNombrePages.setText("@NPAGE@");
        tfNombrePages.setName("tfNombrePages");
        pnlAffichagePagesBons.add(tfNombrePages, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNombreBons ----
        lbNombreBons.setText("Nombre de bons");
        lbNombreBons.setName("lbNombreBons");
        pnlAffichagePagesBons.add(lbNombreBons, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- tfNombreBons ----
        tfNombreBons.setEditable(false);
        tfNombreBons.setEnabled(false);
        tfNombreBons.setText("@NBBON@");
        tfNombreBons.setName("tfNombreBons");
        pnlAffichagePagesBons.add(tfNombreBons, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlAffichagePagesBons);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlAffichagePagesBons;
  private SNLabelChamp lbNombrePages;
  private SNTexte tfNombrePages;
  private SNLabelChamp lbNombreBons;
  private SNTexte tfNombreBons;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
