
package ri.serien.libecranrpg.sgvm.SGVM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM212] Gestion des ventes -> Documents de ventes -> Devis -> Edition des devis (sans pied)
 * Indicateur : 00000001 (indicateur 36 (suite du titre récupéré du GAP))
 * Titre : Edition des devis sans pied
 * 
 * [GVM213] Gestion des ventes -> Documents de ventes -> Devis -> Edition des devis (avec pied)
 * Indicateur : 00000001 (indicateur 36 (suite du titre récupéré du GAP))
 * Titre : Edition des devis avec pied
 * 
 * [GVM2711] Gestion des ventes -> Documents de ventes -> Editions -> Commandes -> Commandes chiffrées
 * Indicateur : 00000001 (indicateur 33)
 * Titre : Edition des commandes
 * 
 * [GVM2712] Gestion des ventes -> Documents de ventes -> Editions -> Commandes -> Commandes non chiffrées
 * Indicateur : 00000001 (indicateur 33)
 * Titre : Edition des commandes
 * 
 * [GVM2713] Gestion des ventes -> Documents de ventes -> Editions -> Commandes -> Commandes chiffrées par ligne
 * Indicateur : 00000001 (indicateur 33)
 * Titre : Edition des commandes
 * 
 * [GVM2714] Gestion des ventes -> Documents de ventes -> Editions -> Commandes -> Bons de préparation
 * Indicateur : 00000001 (indicateur 41)
 * Titre : Edition des bons de préparation
 * 
 * [GVM2715] Gestion des ventes -> Documents de ventes -> Editions -> Commandes -> Bons de préparation à date du jour
 * Indicateur : 00001001
 * Titre : Edition des bons de préparation à la date du jour
 * 
 * [GVM2716] Gestion des ventes -> Documents de ventes -> Editions -> Commandes -> Bon de préparation avec prix public
 * Indicateur : 00000001 (indicateur 41)
 * Titre : Edition des bons de préparation
 * 
 * [GVM2721] Gestion des ventes -> Documents de ventes -> Editions -> Expéditions -> Expéditions chiffrées
 * Indicateur : 00000001 (indicateur 34)
 * Titre : Edition des bons d'expédition
 * 
 * [GVM2722] Gestion des ventes -> Documents de ventes -> Editions -> Expéditions -> Expéditions non chiffrées
 * Indicateur : 00000001 (indicateur 34)
 * Titre : Edition des bons d'expédition
 * 
 * [GVM2723] Gestion des ventes -> Documents de ventes -> Editions -> Expéditions -> Expéditions avec prix public
 * Indicateur : 00000001 (indicateur 34)
 * Titre : Edition des bons d'expédition
 * 
 * [GVM273] Gestion des ventes -> Documents de ventes -> Editions -> Bons de facturation
 * Indicateur : 00000001 (indicateur 35)
 * Titre : Edition des bons de facturation
 * 
 * [GVM274] Gestion des ventes -> Documents de ventes -> Editions -> Bons en attente
 * Indicateur : 00000001 (indicateur 31)
 * Titre : Edition des bons en attente
 * 
 * [GVM275] Gestion des ventes -> Documents de ventes -> Editions -> Factures proforma
 * Indicateur : 00000001 (indicateur 32)
 * Titre : Edition des factures proforma
 */
public class SGVM25FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private String BOUTON_TRI = "Trier l'édition";
  private Message WTYPI = null;
  
  public SGVM25FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    WSER.setValeursSelection("OUI", "NON");
    EDTCTM.setValeursSelection("OUI", "NON");
    WFOR.setValeursSelection("OUI", "NON");
    WMAIL.setValeursSelection("OUI", "NON");
    WRED.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_TRI, 't', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbWTYPI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTYPI@")).trim());
    tfDateEtablissement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Initialsie WCHOIX afin de pas etre en mode tri à l'entrée du programme
    lexique.HostFieldPutData("WCHOIX", 0, " ");
    
    // Indicateurs
    Boolean isEditionBonAttente = lexique.isTrue("31");
    Boolean isEditionFactureProforma = lexique.isTrue("32");
    Boolean isEditionBonExpedition = lexique.isTrue("34");
    Boolean isEditionBonFacturation = lexique.isTrue("35");
    Boolean isEditionDevis = lexique.isTrue("36");
    Boolean isEditionBonPreparationJour = lexique.isTrue("95");
    Boolean isEditionBonPreparation = lexique.isTrue("(N36) and (N33) and (41)");
    Boolean isEditionCommande = lexique.isTrue("(N36) and (33)");
    Boolean isDateLivraison = lexique.isTrue("37");
    
    // Gestion des titres
    if (isEditionDevis) {
      bpPresentation.setText("Edition des devis " + lexique.HostFieldGetData("TITPG2"));
    }
    else if (isEditionBonPreparationJour) {
      bpPresentation.setText("Edition des bons de préparation à la date du jour");
    }
    else if (isEditionBonExpedition) {
      bpPresentation.setText("Edition des bons d'expédition");
    }
    else if (isEditionBonFacturation) {
      bpPresentation.setText("Edition des bons de facturation");
    }
    else if (isEditionBonAttente) {
      bpPresentation.setText("Edition des bons en attente");
    }
    else if (isEditionFactureProforma) {
      bpPresentation.setText("Edition des factures proforma");
    }
    else if (isEditionCommande) {
      bpPresentation.setText("Edition des commandes");
    }
    else if (isEditionBonPreparation) {
      bpPresentation.setText("Edition des bons de préparation");
    }
    
    if (isEditionBonExpedition) {
      WDATDX.setVisible(true);
      WDATFX.setVisible(true);
    }
    
    // Visibilité des composants
    tfDateEtablissement.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfDateEtablissement.isVisible());
    lbDateLivraison.setVisible(WDATDX.isVisible());
    lbDateFin.setVisible(WDATDX.isVisible());
    pnlDateDeLivraison.setVisible(WDATDX.isVisible());
    lbNbrExemplaire.setVisible(WNEX.isVisible());
    
    // Gestion de WTYPI
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    WTYPI = WTYPI.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbWTYPI.setMessage(WTYPI);
    
    if (isDateLivraison || isEditionBonExpedition) {
      if (isDateLivraison) {
        lbDateLivraison.setText("Date de livraison prévue du");
      }
      else if (isEditionBonExpedition) {
        lbDateLivraison.setText("Date d'expédition du");
      }
      lbDateLivraison.setVisible(true);
      lbDateFin.setVisible(true);
      pnlDateDeLivraison.setVisible(true);
    }
    else {
      lbDateLivraison.setVisible(false);
      lbDateFin.setVisible(false);
      pnlDateDeLivraison.setVisible(false);
    }
    
    // Indique le bon libellé suivant le point de menu
    if (isEditionDevis) {
      lbPlageDe.setText("Plage de devis de");
    }
    else {
      lbPlageDe.setText("Plage de documents de");
    }
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charger les composants
    chargerComposantMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    if (WNDEB.getText().isEmpty() && WNFIN.getText().isEmpty()) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
    }
  }
  
  /**
   * Initialise le composant magasin
   */
  public void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostFieldPutData("WCHOIX", 0, " ");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_TRI)) {
        lexique.HostFieldPutData("WCHOIX", 0, "X");
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void sNEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
      chargerComposantMagasin();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbWTYPI = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pbnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbDateLivraison = new SNLabelChamp();
    pnlDateDeLivraison = new SNPanel();
    WDATDX = new XRiCalendrier();
    lbDateFin = new SNLabelChamp();
    WDATFX = new XRiCalendrier();
    lbPlageDe = new SNLabelChamp();
    pnlPlageDe = new SNPanel();
    pnlPLageDeInfo = new SNPanel();
    WNDEB = new XRiTextField();
    WNDEBS = new XRiTextField();
    lbNumeroFin = new SNLabelChamp();
    WNFIN = new XRiTextField();
    WNFINS = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionner = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEtablissement = new SNTexte();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlOptions = new SNPanelTitre();
    lbNbrExemplaire = new SNLabelChamp();
    WNEX = new XRiTextField();
    WRED = new XRiCheckBox();
    WFOR = new XRiCheckBox();
    WSER = new XRiCheckBox();
    WMAIL = new XRiCheckBox();
    EDTCTM = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des bons");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbWTYPI ----
        lbWTYPI.setText("@WTYPI@");
        lbWTYPI.setPreferredSize(new Dimension(120, 30));
        lbWTYPI.setHorizontalTextPosition(SwingConstants.LEADING);
        lbWTYPI.setMinimumSize(new Dimension(64, 30));
        lbWTYPI.setName("lbWTYPI");
        pnlMessage.add(lbWTYPI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setPreferredSize(new Dimension(710, 520));
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pbnlGauche ========
        {
          pbnlGauche.setName("pbnlGauche");
          pbnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pbnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pbnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pbnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pbnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateLivraison ----
            lbDateLivraison.setText("Date de livraison pr\u00e9vue du");
            lbDateLivraison.setMinimumSize(new Dimension(175, 30));
            lbDateLivraison.setPreferredSize(new Dimension(175, 30));
            lbDateLivraison.setName("lbDateLivraison");
            pnlCritereSelection.add(lbDateLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateDeLivraison ========
            {
              pnlDateDeLivraison.setName("pnlDateDeLivraison");
              pnlDateDeLivraison.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateDeLivraison.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDATDX ----
              WDATDX.setMaximumSize(new Dimension(120, 28));
              WDATDX.setMinimumSize(new Dimension(110, 30));
              WDATDX.setPreferredSize(new Dimension(110, 30));
              WDATDX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATDX.setName("WDATDX");
              pnlDateDeLivraison.add(WDATDX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDateFin ----
              lbDateFin.setText("au");
              lbDateFin.setMinimumSize(new Dimension(16, 30));
              lbDateFin.setMaximumSize(new Dimension(16, 30));
              lbDateFin.setPreferredSize(new Dimension(16, 30));
              lbDateFin.setName("lbDateFin");
              pnlDateDeLivraison.add(lbDateFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDATFX ----
              WDATFX.setPreferredSize(new Dimension(110, 30));
              WDATFX.setMinimumSize(new Dimension(110, 30));
              WDATFX.setMaximumSize(new Dimension(110, 28));
              WDATFX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATFX.setName("WDATFX");
              pnlDateDeLivraison.add(WDATFX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlDateDeLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPlageDe ----
            lbPlageDe.setText("Plage de devis de");
            lbPlageDe.setMinimumSize(new Dimension(175, 30));
            lbPlageDe.setPreferredSize(new Dimension(175, 30));
            lbPlageDe.setName("lbPlageDe");
            pnlCritereSelection.add(lbPlageDe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlPlageDe ========
            {
              pnlPlageDe.setName("pnlPlageDe");
              pnlPlageDe.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPlageDe.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlPlageDe.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPlageDe.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPlageDe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ======== pnlPLageDeInfo ========
              {
                pnlPLageDeInfo.setOpaque(false);
                pnlPLageDeInfo.setName("pnlPLageDeInfo");
                pnlPLageDeInfo.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).columnWeights = new double[] { 1.0, 1.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPLageDeInfo.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
                
                // ---- WNDEB ----
                WNDEB.setPreferredSize(new Dimension(70, 30));
                WNDEB.setMinimumSize(new Dimension(70, 30));
                WNDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNDEB.setMaximumSize(new Dimension(70, 30));
                WNDEB.setName("WNDEB");
                pnlPLageDeInfo.add(WNDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WNDEBS ----
                WNDEBS.setMinimumSize(new Dimension(24, 30));
                WNDEBS.setPreferredSize(new Dimension(24, 30));
                WNDEBS.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNDEBS.setName("WNDEBS");
                pnlPLageDeInfo.add(WNDEBS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbNumeroFin ----
                lbNumeroFin.setText("\u00e0");
                lbNumeroFin.setMaximumSize(new Dimension(8, 30));
                lbNumeroFin.setPreferredSize(new Dimension(8, 30));
                lbNumeroFin.setMinimumSize(new Dimension(8, 30));
                lbNumeroFin.setName("lbNumeroFin");
                pnlPLageDeInfo.add(lbNumeroFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WNFIN ----
                WNFIN.setMinimumSize(new Dimension(70, 30));
                WNFIN.setPreferredSize(new Dimension(70, 30));
                WNFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNFIN.setMaximumSize(new Dimension(70, 30));
                WNFIN.setName("WNFIN");
                pnlPLageDeInfo.add(WNFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WNFINS ----
                WNFINS.setMinimumSize(new Dimension(24, 30));
                WNFINS.setPreferredSize(new Dimension(24, 30));
                WNFINS.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNFINS.setName("WNFINS");
                pnlPLageDeInfo.add(WNFINS, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPlageDe.add(pnlPLageDeInfo, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlPlageDe, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pbnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pbnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionner ========
          {
            pnlEtablissementSelectionner.setForeground(Color.black);
            pnlEtablissementSelectionner.setTitre("Etablissement et magasin");
            pnlEtablissementSelectionner.setName("pnlEtablissementSelectionner");
            pnlEtablissementSelectionner.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionner.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionner.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(e -> sNEtablissementValueChanged(e));
            pnlEtablissementSelectionner.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionner.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfDateEtablissement ----
            tfDateEtablissement.setText("@WENCX@");
            tfDateEtablissement.setEditable(false);
            tfDateEtablissement.setEnabled(false);
            tfDateEtablissement.setPreferredSize(new Dimension(260, 30));
            tfDateEtablissement.setMinimumSize(new Dimension(260, 30));
            tfDateEtablissement.setMaximumSize(new Dimension(260, 30));
            tfDateEtablissement.setName("tfDateEtablissement");
            pnlEtablissementSelectionner.add(tfDateEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlEtablissementSelectionner.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setEnabled(false);
            snMagasin.setName("snMagasin");
            pnlEtablissementSelectionner.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionner, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptions ========
          {
            pnlOptions.setOpaque(false);
            pnlOptions.setTitre("Options d'\u00e9dition");
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 198, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNbrExemplaire ----
            lbNbrExemplaire.setText("Nombre d'exemplaires");
            lbNbrExemplaire.setName("lbNbrExemplaire");
            pnlOptions.add(lbNbrExemplaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WNEX ----
            WNEX.setPreferredSize(new Dimension(24, 30));
            WNEX.setMinimumSize(new Dimension(24, 30));
            WNEX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNEX.setName("WNEX");
            pnlOptions.add(WNEX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WRED ----
            WRED.setText("R\u00e9\u00e9dition");
            WRED.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WRED.setFont(new Font("sansserif", Font.PLAIN, 14));
            WRED.setPreferredSize(new Dimension(82, 30));
            WRED.setMinimumSize(new Dimension(82, 30));
            WRED.setName("WRED");
            pnlOptions.add(WRED, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WFOR ----
            WFOR.setText("For\u00e7age");
            WFOR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WFOR.setFont(new Font("sansserif", Font.PLAIN, 14));
            WFOR.setMinimumSize(new Dimension(75, 30));
            WFOR.setPreferredSize(new Dimension(75, 30));
            WFOR.setName("WFOR");
            pnlOptions.add(WFOR, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WSER ----
            WSER.setText("Edition des num\u00e9ros de s\u00e9rie");
            WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSER.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSER.setPreferredSize(new Dimension(207, 30));
            WSER.setMinimumSize(new Dimension(207, 30));
            WSER.setName("WSER");
            pnlOptions.add(WSER, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WMAIL ----
            WMAIL.setText("Envoi notification par mail");
            WMAIL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WMAIL.setFont(new Font("sansserif", Font.PLAIN, 14));
            WMAIL.setMinimumSize(new Dimension(75, 30));
            WMAIL.setPreferredSize(new Dimension(75, 30));
            WMAIL.setName("WMAIL");
            pnlOptions.add(WMAIL, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EDTCTM ----
            EDTCTM.setText("Edition contr\u00f4le de marge");
            EDTCTM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTCTM.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTCTM.setMinimumSize(new Dimension(185, 30));
            EDTCTM.setPreferredSize(new Dimension(185, 30));
            EDTCTM.setName("EDTCTM");
            pnlOptions.add(EDTCTM, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbWTYPI;
  private SNPanel pnlColonne;
  private SNPanel pbnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbDateLivraison;
  private SNPanel pnlDateDeLivraison;
  private XRiCalendrier WDATDX;
  private SNLabelChamp lbDateFin;
  private XRiCalendrier WDATFX;
  private SNLabelChamp lbPlageDe;
  private SNPanel pnlPlageDe;
  private SNPanel pnlPLageDeInfo;
  private XRiTextField WNDEB;
  private XRiTextField WNDEBS;
  private SNLabelChamp lbNumeroFin;
  private XRiTextField WNFIN;
  private XRiTextField WNFINS;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionner;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlOptions;
  private SNLabelChamp lbNbrExemplaire;
  private XRiTextField WNEX;
  private XRiCheckBox WRED;
  private XRiCheckBox WFOR;
  private XRiCheckBox WSER;
  private XRiCheckBox WMAIL;
  private XRiCheckBox EDTCTM;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
