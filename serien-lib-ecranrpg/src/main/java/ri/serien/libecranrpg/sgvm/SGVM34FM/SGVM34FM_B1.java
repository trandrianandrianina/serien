
package ri.serien.libecranrpg.sgvm.SGVM34FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM2B3] Gestion des ventes -> Documents de ventes -> Traitements commandes -> Edition état de préparation
 * Indicateur : 10001001
 * Titre : Edition de l'état de préparation par numéro de commande
 * 
 * [GVM3981] Gestion des ventes -> Stocks -> Autres états de stocks -> Préparation des commandes -> Par numéro de bon de commande
 * Indicateur : 10010001
 * Titre : Edition de l'état de préparation par numéro de commande
 */
public class SGVM34FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String BOUTON_EXPORTER = "Exporter";
  private Message LOCTP = null;
  
  public SGVM34FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    REPON3.setValeursSelection("OUI", "   ");
    REPON2.setValeursSelection("OUI", "   ");
    TOUBON.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Disponibilité des composants
    EBON.setEnabled(!TOUBON.isSelected());
    EBONF.setEnabled(!TOUBON.isSelected());
    ESUF.setEnabled(!TOUBON.isSelected());
    ESUFF.setEnabled(!TOUBON.isSelected());
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charge le composant etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void selectionCompleteActionPerformed(ActionEvent e) {
    EBON.setEnabled(!TOUBON.isSelected());
    EBONF.setEnabled(!TOUBON.isSelected());
    ESUF.setEnabled(!TOUBON.isSelected());
    ESUFF.setEnabled(!TOUBON.isSelected());
    if (TOUBON.isSelected()) {
      EBON.setText("");
      EBONF.setText("");
      ESUF.setText("");
      ESUFF.setText("");
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbNumeroDeBonDebut = new SNLabelChamp();
    EBON = new XRiTextField();
    ESUF = new XRiTextField();
    TOUBON = new XRiCheckBox();
    lbNumeroDeBonFin = new SNLabelChamp();
    EBONF = new XRiTextField();
    ESUFF = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    REPON2 = new XRiCheckBox();
    REPON3 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition de l'\u00e9tat de pr\u00e9paration par num\u00e9ro de commande");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setMaximumSize(new Dimension(120, 30));
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDeBonDebut ----
            lbNumeroDeBonDebut.setText("Num\u00e9ro de bon d\u00e9but");
            lbNumeroDeBonDebut.setPreferredSize(new Dimension(157, 30));
            lbNumeroDeBonDebut.setMinimumSize(new Dimension(157, 30));
            lbNumeroDeBonDebut.setMaximumSize(new Dimension(157, 30));
            lbNumeroDeBonDebut.setName("lbNumeroDeBonDebut");
            pnlCritereDeSelection.add(lbNumeroDeBonDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EBON ----
            EBON.setFont(new Font("sansserif", Font.PLAIN, 14));
            EBON.setPreferredSize(new Dimension(70, 30));
            EBON.setMinimumSize(new Dimension(70, 30));
            EBON.setMaximumSize(new Dimension(70, 30));
            EBON.setName("EBON");
            pnlCritereDeSelection.add(EBON, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ESUF ----
            ESUF.setFont(new Font("sansserif", Font.PLAIN, 14));
            ESUF.setPreferredSize(new Dimension(25, 30));
            ESUF.setMinimumSize(new Dimension(25, 30));
            ESUF.setMaximumSize(new Dimension(25, 30));
            ESUF.setName("ESUF");
            pnlCritereDeSelection.add(ESUF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- TOUBON ----
            TOUBON.setText("Tous");
            TOUBON.setComponentPopupMenu(null);
            TOUBON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TOUBON.setFont(new Font("sansserif", Font.PLAIN, 14));
            TOUBON.setName("TOUBON");
            TOUBON.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                selectionCompleteActionPerformed(e);
              }
            });
            pnlCritereDeSelection.add(TOUBON, new GridBagConstraints(3, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbNumeroDeBonFin ----
            lbNumeroDeBonFin.setText("Num\u00e9ro de bon fin");
            lbNumeroDeBonFin.setName("lbNumeroDeBonFin");
            pnlCritereDeSelection.add(lbNumeroDeBonFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- EBONF ----
            EBONF.setFont(new Font("sansserif", Font.PLAIN, 14));
            EBONF.setName("EBONF");
            pnlCritereDeSelection.add(EBONF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- ESUFF ----
            ESUFF.setFont(new Font("sansserif", Font.PLAIN, 14));
            ESUFF.setPreferredSize(new Dimension(25, 30));
            ESUFF.setMinimumSize(new Dimension(25, 30));
            ESUFF.setMaximumSize(new Dimension(25, 30));
            ESUFF.setName("ESUFF");
            pnlCritereDeSelection.add(ESUFF, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Options d'\u00e9ditions");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- REPON1 ----
            REPON1.setText("Tri par num\u00e9ro de client");
            REPON1.setComponentPopupMenu(null);
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setMaximumSize(new Dimension(174, 30));
            REPON1.setMinimumSize(new Dimension(174, 30));
            REPON1.setPreferredSize(new Dimension(174, 30));
            REPON1.setName("REPON1");
            pnlOptionEdition.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON2 ----
            REPON2.setText("Saut de page");
            REPON2.setComponentPopupMenu(null);
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON2.setMaximumSize(new Dimension(128, 30));
            REPON2.setMinimumSize(new Dimension(128, 30));
            REPON2.setPreferredSize(new Dimension(128, 30));
            REPON2.setName("REPON2");
            pnlOptionEdition.add(REPON2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON3 ----
            REPON3.setText("Chiffrage du bon");
            REPON3.setComponentPopupMenu(null);
            REPON3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON3.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON3.setMaximumSize(new Dimension(128, 30));
            REPON3.setMinimumSize(new Dimension(128, 30));
            REPON3.setPreferredSize(new Dimension(128, 30));
            REPON3.setName("REPON3");
            pnlOptionEdition.add(REPON3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbNumeroDeBonDebut;
  private XRiTextField EBON;
  private XRiTextField ESUF;
  private XRiCheckBox TOUBON;
  private SNLabelChamp lbNumeroDeBonFin;
  private XRiTextField EBONF;
  private XRiTextField ESUFF;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox REPON1;
  private XRiCheckBox REPON2;
  private XRiCheckBox REPON3;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
