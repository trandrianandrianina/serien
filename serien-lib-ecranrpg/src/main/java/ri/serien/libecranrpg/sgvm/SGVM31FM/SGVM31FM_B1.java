
package ri.serien.libecranrpg.sgvm.SGVM31FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM5731] Gestion des ventes -> Opérations périodiques -> Commissionnement représentants -> Calcul des commissions -> Liste de contrôle
 * seulement
 * Indicateur:11100001 (91,92,93)
 * Titre:Liste de controle des commissionnement représentant
 * 
 * [GVM5732] Gestion des ventes -> Opérations périodiques -> Commissionnement représentants -> Calcul des commissions -> Génération
 * fichier des commissions
 * Indicateur:10000001 (91)
 * Titre:Génération des fichiers des commissionement représentant
 */
public class SGVM31FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  public SGVM31FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    UOPT4.setValeurs("4", "RB");
    UOPT3.setValeurs("3", "RB");
    UOPT2.setValeurs("2", "RB");
    UOPT1.setValeurs("1", "RB");
    
    REPON1.setValeursSelection("OUI", "NON");
    REPCO.setValeursSelection("OUI", "NON");
    REPCO0.setValeursSelection("OUI", "NON");
    REPON2.setValeursSelection("O", "N");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Indicateur
    Boolean is91 = lexique.isTrue("91");
    Boolean is92 = lexique.isTrue("92");
    Boolean is93 = lexique.isTrue("93");
    
    // Titre
    if (is91 && !is92) {
      bpPresentation.setText("Génération des fichiers des commissionement représentant");
    }
    if (is91 && is92) {
      bpPresentation.setText("Liste de controle des commissionnement représentant");
    }
    
    // Gestion de la visibilité
    if (is93) {
      snRepresentant.setVisible(true);
      lbRepresentant.setVisible(true);
    }
    else {
      snRepresentant.setVisible(false);
      lbRepresentant.setVisible(false);
    }
    
    // Selection des nvieau d'édition
    if (lexique.HostFieldGetData("UOPT1") == "X") {
      UOPT1.setSelected(true);
    }
    else if (lexique.HostFieldGetData("UOPT2") == "X") {
      UOPT2.setSelected(true);
    }
    else if (lexique.HostFieldGetData("UOPT3") == "X") {
      UOPT3.setSelected(true);
    }
    else if (lexique.HostFieldGetData("UOPT4") == "X") {
      UOPT4.setSelected(true);
    }
    else {
      UOPT1.setSelected(true);
    }
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Disponibilité des composant
    if (UOPT1.isSelected()) {
      REPON2.setEnabled(true);
    }
    else {
      REPON2.setEnabled(false);
    }
    
    chargerComposantRepresentant();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snRepresentant.getIdSelection() == null) {
      lexique.HostFieldPutData("CODREP", 0, "**");
    }
    else {
      snRepresentant.renseignerChampRPG(lexique, "CODREP");
    }
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Initialise le composant des representant suivant l'etablissement
   */
  private void chargerComposantRepresentant() {
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "CODREP");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerComposantRepresentant();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void detaileLigneCommisionerActionPerformed(ActionEvent e) {
    REPON2.setEnabled(true);
  }
  
  private void detailLigneDeVenteActionPerformed(ActionEvent e) {
    REPON2.setEnabled(false);
    REPON2.setSelected(false);
  }
  
  private void factureActionPerformed(ActionEvent e) {
    REPON2.setEnabled(false);
    REPON2.setSelected(false);
  }
  
  private void representantActionPerformed(ActionEvent e) {
    REPON2.setEnabled(false);
    REPON2.setSelected(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSeletion = new SNPanelTitre();
    lbPeriodeAComissioner = new SNLabelChamp();
    DATDEB = new XRiCalendrier();
    lbDateFin = new SNLabelChamp();
    DATFIN = new XRiCalendrier();
    lbNumeroFacture = new SNLabelChamp();
    pnlNumeroFacture = new SNPanel();
    NFADEB = new XRiTextField();
    lbA = new SNLabelChamp();
    NFAFIN = new XRiTextField();
    lbRepresentant = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    REPCO = new XRiCheckBox();
    REPCO0 = new XRiCheckBox();
    pnlNumeroFactureChoix = new SNPanelTitre();
    UOPT1 = new XRiRadioButton();
    REPON2 = new XRiCheckBox();
    UOPT2 = new XRiRadioButton();
    UOPT3 = new XRiRadioButton();
    UOPT4 = new XRiRadioButton();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setPreferredSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setBorder(null);
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSeletion ========
          {
            pnlCritereDeSeletion.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSeletion.setName("pnlCritereDeSeletion");
            pnlCritereDeSeletion.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSeletion.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSeletion.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSeletion.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSeletion.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPeriodeAComissioner ----
            lbPeriodeAComissioner.setText("P\u00e9riode \u00e0 commissioner de");
            lbPeriodeAComissioner.setMaximumSize(new Dimension(100, 30));
            lbPeriodeAComissioner.setPreferredSize(new Dimension(175, 30));
            lbPeriodeAComissioner.setName("lbPeriodeAComissioner");
            pnlCritereDeSeletion.add(lbPeriodeAComissioner, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DATDEB ----
            DATDEB.setPreferredSize(new Dimension(110, 30));
            DATDEB.setMinimumSize(new Dimension(110, 30));
            DATDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATDEB.setName("DATDEB");
            pnlCritereDeSeletion.add(DATDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDateFin ----
            lbDateFin.setText("\u00e0");
            lbDateFin.setMinimumSize(new Dimension(8, 30));
            lbDateFin.setPreferredSize(new Dimension(8, 30));
            lbDateFin.setMaximumSize(new Dimension(8, 30));
            lbDateFin.setName("lbDateFin");
            pnlCritereDeSeletion.add(lbDateFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DATFIN ----
            DATFIN.setMinimumSize(new Dimension(110, 30));
            DATFIN.setPreferredSize(new Dimension(110, 30));
            DATFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATFIN.setName("DATFIN");
            pnlCritereDeSeletion.add(DATFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNumeroFacture ----
            lbNumeroFacture.setText("Num\u00e9ros de factures de");
            lbNumeroFacture.setPreferredSize(new Dimension(175, 30));
            lbNumeroFacture.setName("lbNumeroFacture");
            pnlCritereDeSeletion.add(lbNumeroFacture, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlNumeroFacture ========
            {
              pnlNumeroFacture.setName("pnlNumeroFacture");
              pnlNumeroFacture.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNumeroFacture.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- NFADEB ----
              NFADEB.setMinimumSize(new Dimension(80, 30));
              NFADEB.setPreferredSize(new Dimension(80, 30));
              NFADEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              NFADEB.setName("NFADEB");
              pnlNumeroFacture.add(NFADEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbA ----
              lbA.setText("\u00e0");
              lbA.setMinimumSize(new Dimension(8, 30));
              lbA.setPreferredSize(new Dimension(8, 30));
              lbA.setMaximumSize(new Dimension(8, 30));
              lbA.setName("lbA");
              pnlNumeroFacture.add(lbA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- NFAFIN ----
              NFAFIN.setPreferredSize(new Dimension(80, 30));
              NFAFIN.setMinimumSize(new Dimension(80, 30));
              NFAFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              NFAFIN.setName("NFAFIN");
              pnlNumeroFacture.add(NFAFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSeletion.add(pnlNumeroFacture, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbRepresentant ----
            lbRepresentant.setText("Repr\u00e9sentant");
            lbRepresentant.setMaximumSize(new Dimension(100, 30));
            lbRepresentant.setPreferredSize(new Dimension(175, 30));
            lbRepresentant.setName("lbRepresentant");
            pnlCritereDeSeletion.add(lbRepresentant, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snRepresentant ----
            snRepresentant.setName("snRepresentant");
            pnlCritereDeSeletion.add(snRepresentant, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSeletion, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setPreferredSize(new Dimension(536, 110));
            pnlEtablissement.setMinimumSize(new Dimension(520, 110));
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setEditable(false);
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Options d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- REPON1 ----
            REPON1.setText("Client livr\u00e9");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setPreferredSize(new Dimension(100, 30));
            REPON1.setMinimumSize(new Dimension(100, 30));
            REPON1.setMaximumSize(new Dimension(100, 30));
            REPON1.setName("REPON1");
            pnlOptionEdition.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- REPCO ----
            REPCO.setText("Lignes commissionn\u00e9es");
            REPCO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPCO.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPCO.setMinimumSize(new Dimension(175, 30));
            REPCO.setPreferredSize(new Dimension(175, 30));
            REPCO.setMaximumSize(new Dimension(175, 30));
            REPCO.setName("REPCO");
            pnlOptionEdition.add(REPCO, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- REPCO0 ----
            REPCO0.setText("Lignes commissionn\u00e9es \u00e0 0");
            REPCO0.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPCO0.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPCO0.setPreferredSize(new Dimension(198, 30));
            REPCO0.setMinimumSize(new Dimension(198, 30));
            REPCO0.setName("REPCO0");
            pnlOptionEdition.add(REPCO0, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlNumeroFactureChoix ========
          {
            pnlNumeroFactureChoix.setTitre("Niveau d'\u00e9dition");
            pnlNumeroFactureChoix.setName("pnlNumeroFactureChoix");
            pnlNumeroFactureChoix.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlNumeroFactureChoix.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlNumeroFactureChoix.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlNumeroFactureChoix.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlNumeroFactureChoix.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- UOPT1 ----
            UOPT1.setText("D\u00e9tail des lignes commissionn\u00e9es");
            UOPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UOPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            UOPT1.setPreferredSize(new Dimension(245, 30));
            UOPT1.setMinimumSize(new Dimension(245, 30));
            UOPT1.setActionCommand("D\u00e9tail des lignes commissionn\u00e9es");
            UOPT1.setMaximumSize(new Dimension(245, 30));
            UOPT1.setName("UOPT1");
            UOPT1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                detaileLigneCommisionerActionPerformed(e);
              }
            });
            pnlNumeroFactureChoix.add(UOPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPON2 ----
            REPON2.setText("Avec d\u00e9tail quantit\u00e9 et prix unitaire");
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON2.setPreferredSize(new Dimension(240, 30));
            REPON2.setMinimumSize(new Dimension(240, 30));
            REPON2.setMaximumSize(new Dimension(240, 30));
            REPON2.setName("REPON2");
            pnlNumeroFactureChoix.add(REPON2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- UOPT2 ----
            UOPT2.setText("D\u00e9tail des lignes de ventes");
            UOPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UOPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            UOPT2.setMinimumSize(new Dimension(210, 30));
            UOPT2.setPreferredSize(new Dimension(210, 30));
            UOPT2.setMaximumSize(new Dimension(210, 30));
            UOPT2.setName("UOPT2");
            UOPT2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                detailLigneDeVenteActionPerformed(e);
              }
            });
            pnlNumeroFactureChoix.add(UOPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- UOPT3 ----
            UOPT3.setText("Facture");
            UOPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UOPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            UOPT3.setPreferredSize(new Dimension(210, 30));
            UOPT3.setMinimumSize(new Dimension(210, 30));
            UOPT3.setMaximumSize(new Dimension(210, 30));
            UOPT3.setName("UOPT3");
            UOPT3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                factureActionPerformed(e);
              }
            });
            pnlNumeroFactureChoix.add(UOPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- UOPT4 ----
            UOPT4.setText("Repr\u00e9sentant");
            UOPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UOPT4.setForeground(Color.black);
            UOPT4.setFont(new Font("sansserif", Font.PLAIN, 14));
            UOPT4.setMinimumSize(new Dimension(210, 30));
            UOPT4.setPreferredSize(new Dimension(210, 30));
            UOPT4.setMaximumSize(new Dimension(210, 30));
            UOPT4.setName("UOPT4");
            UOPT4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                representantActionPerformed(e);
              }
            });
            pnlNumeroFactureChoix.add(UOPT4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlNumeroFactureChoix, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(UOPT1);
    RB_GRP.add(UOPT2);
    RB_GRP.add(UOPT3);
    RB_GRP.add(UOPT4);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSeletion;
  private SNLabelChamp lbPeriodeAComissioner;
  private XRiCalendrier DATDEB;
  private SNLabelChamp lbDateFin;
  private XRiCalendrier DATFIN;
  private SNLabelChamp lbNumeroFacture;
  private SNPanel pnlNumeroFacture;
  private XRiTextField NFADEB;
  private SNLabelChamp lbA;
  private XRiTextField NFAFIN;
  private SNLabelChamp lbRepresentant;
  private SNRepresentant snRepresentant;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox REPON1;
  private XRiCheckBox REPCO;
  private XRiCheckBox REPCO0;
  private SNPanelTitre pnlNumeroFactureChoix;
  private XRiRadioButton UOPT1;
  private XRiCheckBox REPON2;
  private XRiRadioButton UOPT2;
  private XRiRadioButton UOPT3;
  private XRiRadioButton UOPT4;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
