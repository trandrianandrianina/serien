
package ri.serien.libecranrpg.sgvm.SGVM6SFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM6SFM_B1 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_EXPORTER = "Exporter";
  
  private String[] OPTCLI_Value = { "L", "F", };
  private String[] OPTDAT_Value = { "P", "S", };
  
  public SGVM6SFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    OPTCLI.setValeurs(OPTCLI_Value, null);
    OPTDAT.setValeurs(OPTDAT_Value, null);
    LIGAFF.setValeursSelection("1", " ");
    
    // Initialisation de la barre des boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation du composant magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // Initialisation du composant client 1
    snClient1.setSession(getSession());
    snClient1.setIdEtablissement(snEtablissement.getIdSelection());
    snClient1.charger(true);
    snClient1.setSelectionParChampRPG(lexique, "CLIDEB");
    
    // Initialisation du composant client 2
    snClient2.setSession(getSession());
    snClient2.setIdEtablissement(snEtablissement.getIdSelection());
    snClient2.charger(true);
    snClient2.setSelectionParChampRPG(lexique, "CLIFIN");
    
    // Initialisation du composant date
    snPlageDate.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPlageDate.setDateDebutParChampRPG(lexique, "DATDEB");
    snPlageDate.setDateFinParChampRPG(lexique, "DATFIN");
    
    // Initialisation du composant fournisseur
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(true);
    snFournisseur.setSelectionParChampRPG(lexique, "CODFRS");
    
    // Initialisation du composant famille 1
    snFamille1.setSession(getSession());
    snFamille1.setIdEtablissement(snEtablissement.getIdSelection());
    snFamille1.charger(true);
    snFamille1.setSelectionParChampRPG(lexique, "FAMDEB");
    
    // Initialisation du composant famille 2
    snFamille2.setSession(getSession());
    snFamille2.setIdEtablissement(snEtablissement.getIdSelection());
    snFamille2.charger(true);
    snFamille2.setSelectionParChampRPG(lexique, "FAMFIN");
    
    

    p_bpresentation.setIdEtablissement(snEtablissement.getIdSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snClient1.renseignerChampRPG(lexique, "CLIDEB");
    snClient2.renseignerChampRPG(lexique, "CLIFIN");
    snPlageDate.renseignerChampRPGDebut(lexique, "DATDEB");
    snPlageDate.renseignerChampRPGFin(lexique, "DATFIN");
    snFournisseur.renseignerChampRPG(lexique, "CODFRS");
    snFamille1.renseignerChampRPG(lexique, "FAMDEB");
    snFamille2.renseignerChampRPG(lexique, "FAMFIN");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbClient1 = new SNLabelChamp();
    snClient1 = new SNClientPrincipal();
    lbClient2 = new SNLabelChamp();
    snClient2 = new SNClientPrincipal();
    OPTCLI = new XRiComboBox();
    pnAutres = new SNPanelTitre();
    LIGAFF = new XRiCheckBox();
    pnlAutresSelections = new SNPanel();
    lbNumeros = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    NUMDEB = new XRiTextField();
    OBJ_65 = new JLabel();
    NUMFIN = new XRiTextField();
    lbDateLivraison = new SNLabelChamp();
    snPlageDate = new SNPlageDate();
    OPTDAT = new XRiComboBox();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbFamille1 = new SNLabelChamp();
    snFamille1 = new SNFamille();
    lbFamille2 = new SNLabelChamp();
    snFamille2 = new SNFamille();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnTri = new SNPanelTitre();
    lbParClient = new SNLabelChamp();
    WTRI1 = new XRiTextField();
    lbParClient2 = new SNLabelChamp();
    WTRI2 = new XRiTextField();
    lbParClient3 = new SNLabelChamp();
    WTRI3 = new XRiTextField();
    lbParClient4 = new SNLabelChamp();
    WTRI6 = new XRiTextField();
    lbParClient5 = new SNLabelChamp();
    WTRI4 = new XRiTextField();
    lbParClient6 = new SNLabelChamp();
    WTRI5 = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlCritereSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setName("snMagasin");
          pnlCritereSelection.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbClient1 ----
          lbClient1.setText("Client de d\u00e9but");
          lbClient1.setName("lbClient1");
          pnlCritereSelection.add(lbClient1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snClient1 ----
          snClient1.setName("snClient1");
          pnlCritereSelection.add(snClient1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbClient2 ----
          lbClient2.setText("Client de fin");
          lbClient2.setName("lbClient2");
          pnlCritereSelection.add(lbClient2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snClient2 ----
          snClient2.setName("snClient2");
          pnlCritereSelection.add(snClient2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OPTCLI ----
          OPTCLI.setModel(new DefaultComboBoxModel(new String[] { "Client livr\u00e9", "Client factur\u00e9" }));
          OPTCLI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTCLI.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPTCLI.setBackground(Color.white);
          OPTCLI.setMinimumSize(new Dimension(150, 30));
          OPTCLI.setPreferredSize(new Dimension(150, 30));
          OPTCLI.setMaximumSize(new Dimension(150, 30));
          OPTCLI.setName("OPTCLI");
          pnlCritereSelection.add(OPTCLI, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnAutres ========
        {
          pnAutres.setTitre("Autres s\u00e9lections");
          pnAutres.setName("pnAutres");
          pnAutres.setLayout(new GridBagLayout());
          ((GridBagLayout) pnAutres.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnAutres.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnAutres.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnAutres.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- LIGAFF ----
          LIGAFF.setText("Etat complet - 1 ligne par affectation");
          LIGAFF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIGAFF.setFont(new Font("sansserif", Font.PLAIN, 14));
          LIGAFF.setMinimumSize(new Dimension(124, 30));
          LIGAFF.setPreferredSize(new Dimension(124, 30));
          LIGAFF.setName("LIGAFF");
          pnAutres.add(LIGAFF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlAutresSelections ========
          {
            pnlAutresSelections.setName("pnlAutresSelections");
            pnlAutresSelections.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlAutresSelections.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlAutresSelections.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlAutresSelections.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlAutresSelections.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeros ----
            lbNumeros.setText("Num\u00e9ros de bons");
            lbNumeros.setName("lbNumeros");
            pnlAutresSelections.add(lbNumeros, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- NUMDEB ----
              NUMDEB.setMinimumSize(new Dimension(70, 30));
              NUMDEB.setMaximumSize(new Dimension(70, 30));
              NUMDEB.setPreferredSize(new Dimension(70, 30));
              NUMDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMDEB.setName("NUMDEB");
              sNPanel1.add(NUMDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_65 ----
              OBJ_65.setText("/");
              OBJ_65.setFont(new Font("sansserif", Font.BOLD, 14));
              OBJ_65.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_65.setName("OBJ_65");
              sNPanel1.add(OBJ_65, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- NUMFIN ----
              NUMFIN.setMinimumSize(new Dimension(70, 30));
              NUMFIN.setMaximumSize(new Dimension(70, 30));
              NUMFIN.setPreferredSize(new Dimension(70, 30));
              NUMFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMFIN.setName("NUMFIN");
              sNPanel1.add(NUMFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlAutresSelections.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDateLivraison ----
            lbDateLivraison.setText("Date de livraison");
            lbDateLivraison.setName("lbDateLivraison");
            pnlAutresSelections.add(lbDateLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDate ----
            snPlageDate.setName("snPlageDate");
            pnlAutresSelections.add(snPlageDate, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPTDAT ----
            OPTDAT
                .setModel(new DefaultComboBoxModel(new String[] { "Date de livraison pr\u00e9vue", "Date de livraison souhait\u00e9e" }));
            OPTDAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPTDAT.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPTDAT.setBackground(Color.white);
            OPTDAT.setPreferredSize(new Dimension(205, 30));
            OPTDAT.setMinimumSize(new Dimension(205, 30));
            OPTDAT.setMaximumSize(new Dimension(32767, 30));
            OPTDAT.setName("OPTDAT");
            pnlAutresSelections.add(OPTDAT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setName("lbFournisseur");
            pnlAutresSelections.add(lbFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseur ----
            snFournisseur.setName("snFournisseur");
            pnlAutresSelections.add(snFournisseur, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamille1 ----
            lbFamille1.setText("Famille de d\u00e9but");
            lbFamille1.setName("lbFamille1");
            pnlAutresSelections.add(lbFamille1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamille1 ----
            snFamille1.setName("snFamille1");
            pnlAutresSelections.add(snFamille1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamille2 ----
            lbFamille2.setText("Famille de fin");
            lbFamille2.setName("lbFamille2");
            pnlAutresSelections.add(lbFamille2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snFamille2 ----
            snFamille2.setName("snFamille2");
            pnlAutresSelections.add(snFamille2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnAutres.add(pnlAutresSelections, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnAutres, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfEnCours ----
          tfEnCours.setText("@WENCX@");
          tfEnCours.setEnabled(false);
          tfEnCours.setPreferredSize(new Dimension(260, 30));
          tfEnCours.setMinimumSize(new Dimension(260, 30));
          tfEnCours.setMaximumSize(new Dimension(260, 30));
          tfEnCours.setName("tfEnCours");
          pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnTri ========
        {
          pnTri.setTitre("Options de tri");
          pnTri.setName("pnTri");
          pnTri.setLayout(new GridBagLayout());
          ((GridBagLayout) pnTri.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnTri.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnTri.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnTri.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbParClient ----
          lbParClient.setText("Par client");
          lbParClient.setName("lbParClient");
          pnTri.add(lbParClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WTRI1 ----
          WTRI1.setToolTipText("Choisir un ordre de tri de 1 \u00e0 5");
          WTRI1.setComponentPopupMenu(null);
          WTRI1.setPreferredSize(new Dimension(30, 30));
          WTRI1.setMinimumSize(new Dimension(30, 30));
          WTRI1.setMaximumSize(new Dimension(30, 30));
          WTRI1.setName("WTRI1");
          pnTri.add(WTRI1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbParClient2 ----
          lbParClient2.setText("Par num\u00e9ro de bon");
          lbParClient2.setName("lbParClient2");
          pnTri.add(lbParClient2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WTRI2 ----
          WTRI2.setToolTipText("Choisir un ordre de tri de 1 \u00e0 5");
          WTRI2.setComponentPopupMenu(null);
          WTRI2.setPreferredSize(new Dimension(30, 30));
          WTRI2.setMinimumSize(new Dimension(30, 30));
          WTRI2.setMaximumSize(new Dimension(30, 30));
          WTRI2.setName("WTRI2");
          pnTri.add(WTRI2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbParClient3 ----
          lbParClient3.setText("Par fournisseur");
          lbParClient3.setName("lbParClient3");
          pnTri.add(lbParClient3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WTRI3 ----
          WTRI3.setToolTipText("Choisir un ordre de tri de 1 \u00e0 5");
          WTRI3.setComponentPopupMenu(null);
          WTRI3.setPreferredSize(new Dimension(30, 30));
          WTRI3.setMinimumSize(new Dimension(30, 30));
          WTRI3.setMaximumSize(new Dimension(30, 30));
          WTRI3.setName("WTRI3");
          pnTri.add(WTRI3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbParClient4 ----
          lbParClient4.setText("Par article");
          lbParClient4.setName("lbParClient4");
          pnTri.add(lbParClient4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WTRI6 ----
          WTRI6.setToolTipText("Choisir un ordre de tri de 1 \u00e0 5");
          WTRI6.setComponentPopupMenu(null);
          WTRI6.setPreferredSize(new Dimension(30, 30));
          WTRI6.setMinimumSize(new Dimension(30, 30));
          WTRI6.setMaximumSize(new Dimension(30, 30));
          WTRI6.setName("WTRI6");
          pnTri.add(WTRI6, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbParClient5 ----
          lbParClient5.setText("Par famille");
          lbParClient5.setName("lbParClient5");
          pnTri.add(lbParClient5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WTRI4 ----
          WTRI4.setToolTipText("Choisir un ordre de tri de 1 \u00e0 5");
          WTRI4.setComponentPopupMenu(null);
          WTRI4.setPreferredSize(new Dimension(30, 30));
          WTRI4.setMinimumSize(new Dimension(30, 30));
          WTRI4.setMaximumSize(new Dimension(30, 30));
          WTRI4.setName("WTRI4");
          pnTri.add(WTRI4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbParClient6 ----
          lbParClient6.setText("Par date de livraison");
          lbParClient6.setName("lbParClient6");
          pnTri.add(lbParClient6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WTRI5 ----
          WTRI5.setToolTipText("Choisir un ordre de tri de 1 \u00e0 5");
          WTRI5.setComponentPopupMenu(null);
          WTRI5.setPreferredSize(new Dimension(30, 30));
          WTRI5.setMinimumSize(new Dimension(30, 30));
          WTRI5.setMaximumSize(new Dimension(30, 30));
          WTRI5.setName("WTRI5");
          pnTri.add(WTRI5, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnTri, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbClient1;
  private SNClientPrincipal snClient1;
  private SNLabelChamp lbClient2;
  private SNClientPrincipal snClient2;
  private XRiComboBox OPTCLI;
  private SNPanelTitre pnAutres;
  private XRiCheckBox LIGAFF;
  private SNPanel pnlAutresSelections;
  private SNLabelChamp lbNumeros;
  private SNPanel sNPanel1;
  private XRiTextField NUMDEB;
  private JLabel OBJ_65;
  private XRiTextField NUMFIN;
  private SNLabelChamp lbDateLivraison;
  private SNPlageDate snPlageDate;
  private XRiComboBox OPTDAT;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbFamille1;
  private SNFamille snFamille1;
  private SNLabelChamp lbFamille2;
  private SNFamille snFamille2;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnTri;
  private SNLabelChamp lbParClient;
  private XRiTextField WTRI1;
  private SNLabelChamp lbParClient2;
  private XRiTextField WTRI2;
  private SNLabelChamp lbParClient3;
  private XRiTextField WTRI3;
  private SNLabelChamp lbParClient4;
  private XRiTextField WTRI6;
  private SNLabelChamp lbParClient5;
  private XRiTextField WTRI4;
  private SNLabelChamp lbParClient6;
  private XRiTextField WTRI5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
