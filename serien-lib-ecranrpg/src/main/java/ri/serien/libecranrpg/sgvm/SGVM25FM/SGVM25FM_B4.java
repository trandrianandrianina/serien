
package ri.serien.libecranrpg.sgvm.SGVM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM3B84] Gestion des ventes -> Stocks -> Gestion des stocks aux emplacements -> Edition étiquettes -> Bon expédition regroupé
 * Indicateur : 00000001
 * Titre : Edition des bons d'expédition regroupés
 */
public class SGVM25FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  private String BOUTON_TRI = "Trier l'édition";
  private Message WTYPI = null;
  
  public SGVM25FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_TRI, 't', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbWTYPI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTYPI@")).trim());
    tfDateEtablissement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Visibilité des composants
    tfDateEtablissement.setVisible(!lexique.HostFieldGetData("WENCX").trim().isEmpty());
    
    // Initialsie WCHOIX afin de pas etre en mode tri à l'entrée du programme
    lexique.HostFieldPutData("WCHOIX", 0, " ");
    
    // Gestion de WTYPI
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    WTYPI = WTYPI.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbWTYPI.setMessage(WTYPI);
    
    // Indicateur
    Boolean isPlageDevis = lexique.isTrue("36");
    
    if (isPlageDevis) {
      lbPlageDe.setText("Plage de devis de");
    }
    else {
      lbPlageDe.setText("Plage de documents de");
    }
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // logo
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Initialise l'etablissemment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charge les composants
    chargerMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    if (WNDEB.getText().isEmpty() && WNFIN.getText().isEmpty()) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
    }
  }
  
  /**
   * Charge la liste des magasins suivant l'établissement
   */
  public void chargerMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostFieldPutData("WCHOIX", 0, "");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_TRI)) {
        lexique.HostFieldPutData("WCHOIX", 0, "X");
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void sNEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerMagasin();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbWTYPI = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCriteresSelection = new SNPanelTitre();
    lbPlageDe = new SNLabelChamp();
    pnlNumeroDeCommande = new SNPanel();
    pnlPlageDeBonsDetail = new SNPanel();
    WNDEB = new XRiTextField();
    WNDEBS = new XRiTextField();
    lbNumeroFin = new SNLabelChamp();
    WNFIN = new XRiTextField();
    WNFINS = new XRiTextField();
    lbDateExpedition = new SNLabelChamp();
    pnlDateExpedition = new SNPanel();
    WDATDX = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    WDATFX = new XRiCalendrier();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEtablissement = new SNTexte();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlOptionEdition = new SNPanelTitre();
    lbNbrExemplaire = new SNLabelChamp();
    WNEX = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des bons d'exp\u00e9dition regroup\u00e9s");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbWTYPI ----
        lbWTYPI.setText("@WTYPI@");
        lbWTYPI.setPreferredSize(new Dimension(120, 30));
        lbWTYPI.setHorizontalTextPosition(SwingConstants.LEADING);
        lbWTYPI.setMinimumSize(new Dimension(64, 30));
        lbWTYPI.setName("lbWTYPI");
        pnlMessage.add(lbWTYPI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setPreferredSize(new Dimension(530, 500));
        pnlColonne.setBackground(new Color(239, 239, 222));
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCriteresSelection ========
          {
            pnlCriteresSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresSelection.setName("pnlCriteresSelection");
            pnlCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPlageDe ----
            lbPlageDe.setText("Plage de [voir code]");
            lbPlageDe.setMaximumSize(new Dimension(100, 30));
            lbPlageDe.setPreferredSize(new Dimension(175, 19));
            lbPlageDe.setMinimumSize(new Dimension(175, 30));
            lbPlageDe.setName("lbPlageDe");
            pnlCriteresSelection.add(lbPlageDe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlNumeroDeCommande ========
            {
              pnlNumeroDeCommande.setName("pnlNumeroDeCommande");
              pnlNumeroDeCommande.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ======== pnlPlageDeBonsDetail ========
              {
                pnlPlageDeBonsDetail.setOpaque(false);
                pnlPlageDeBonsDetail.setFont(new Font("sansserif", Font.PLAIN, 14));
                pnlPlageDeBonsDetail.setName("pnlPlageDeBonsDetail");
                pnlPlageDeBonsDetail.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPlageDeBonsDetail.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPlageDeBonsDetail.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPlageDeBonsDetail.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPlageDeBonsDetail.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- WNDEB ----
                WNDEB.setComponentPopupMenu(pmBTD);
                WNDEB.setPreferredSize(new Dimension(70, 30));
                WNDEB.setMinimumSize(new Dimension(70, 30));
                WNDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNDEB.setMaximumSize(new Dimension(70, 30));
                WNDEB.setName("WNDEB");
                pnlPlageDeBonsDetail.add(WNDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
                
                // ---- WNDEBS ----
                WNDEBS.setComponentPopupMenu(pmBTD);
                WNDEBS.setMinimumSize(new Dimension(24, 30));
                WNDEBS.setPreferredSize(new Dimension(24, 30));
                WNDEBS.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNDEBS.setName("WNDEBS");
                pnlPlageDeBonsDetail.add(WNDEBS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
                
                // ---- lbNumeroFin ----
                lbNumeroFin.setText("\u00e0");
                lbNumeroFin.setPreferredSize(new Dimension(8, 30));
                lbNumeroFin.setMinimumSize(new Dimension(8, 30));
                lbNumeroFin.setMaximumSize(new Dimension(8, 30));
                lbNumeroFin.setName("lbNumeroFin");
                pnlPlageDeBonsDetail.add(lbNumeroFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
                
                // ---- WNFIN ----
                WNFIN.setComponentPopupMenu(pmBTD);
                WNFIN.setMinimumSize(new Dimension(70, 30));
                WNFIN.setPreferredSize(new Dimension(70, 30));
                WNFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNFIN.setMaximumSize(new Dimension(70, 30));
                WNFIN.setName("WNFIN");
                pnlPlageDeBonsDetail.add(WNFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
                
                // ---- WNFINS ----
                WNFINS.setComponentPopupMenu(pmBTD);
                WNFINS.setPreferredSize(new Dimension(24, 30));
                WNFINS.setMinimumSize(new Dimension(24, 30));
                WNFINS.setFont(new Font("sansserif", Font.PLAIN, 14));
                WNFINS.setName("WNFINS");
                pnlPlageDeBonsDetail.add(WNFINS, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlNumeroDeCommande.add(pnlPlageDeBonsDetail, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresSelection.add(pnlNumeroDeCommande, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDateExpedition ----
            lbDateExpedition.setText("Date d'exp\u00e9dition");
            lbDateExpedition.setMaximumSize(new Dimension(100, 30));
            lbDateExpedition.setMinimumSize(new Dimension(175, 30));
            lbDateExpedition.setPreferredSize(new Dimension(175, 30));
            lbDateExpedition.setName("lbDateExpedition");
            pnlCriteresSelection.add(lbDateExpedition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlDateExpedition ========
            {
              pnlDateExpedition.setOpaque(false);
              pnlDateExpedition.setName("pnlDateExpedition");
              pnlDateExpedition.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateExpedition.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateExpedition.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateExpedition.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateExpedition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WDATDX ----
              WDATDX.setPreferredSize(new Dimension(110, 30));
              WDATDX.setMinimumSize(new Dimension(110, 30));
              WDATDX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATDX.setName("WDATDX");
              pnlDateExpedition.add(WDATDX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setHorizontalAlignment(SwingConstants.LEFT);
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlDateExpedition.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WDATFX ----
              WDATFX.setMinimumSize(new Dimension(110, 30));
              WDATFX.setPreferredSize(new Dimension(110, 30));
              WDATFX.setFont(new Font("sansserif", Font.PLAIN, 14));
              WDATFX.setName("WDATFX");
              pnlDateExpedition.add(WDATFX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresSelection.add(pnlDateExpedition, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionne ========
          {
            pnlEtablissementSelectionne.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlEtablissementSelectionne.setTitre("Etablissement et magasin");
            pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
            pnlEtablissementSelectionne.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                sNEtablissementValueChanged(e);
              }
            });
            pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionne.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfDateEtablissement ----
            tfDateEtablissement.setText("@WENCX@");
            tfDateEtablissement.setEditable(false);
            tfDateEtablissement.setEnabled(false);
            tfDateEtablissement.setPreferredSize(new Dimension(260, 30));
            tfDateEtablissement.setMinimumSize(new Dimension(260, 30));
            tfDateEtablissement.setMaximumSize(new Dimension(260, 30));
            tfDateEtablissement.setName("tfDateEtablissement");
            pnlEtablissementSelectionne.add(tfDateEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlEtablissementSelectionne.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setEnabled(false);
            snMagasin.setName("snMagasin");
            pnlEtablissementSelectionne.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlOptionEdition.setTitre("Option d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbNbrExemplaire ----
            lbNbrExemplaire.setText("Nombre d'exemplaires");
            lbNbrExemplaire.setName("lbNbrExemplaire");
            pnlOptionEdition.add(lbNbrExemplaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WNEX ----
            WNEX.setComponentPopupMenu(pmBTD);
            WNEX.setPreferredSize(new Dimension(20, 30));
            WNEX.setMinimumSize(new Dimension(20, 30));
            WNEX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNEX.setName("WNEX");
            pnlOptionEdition.add(WNEX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbWTYPI;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresSelection;
  private SNLabelChamp lbPlageDe;
  private SNPanel pnlNumeroDeCommande;
  private SNPanel pnlPlageDeBonsDetail;
  private XRiTextField WNDEB;
  private XRiTextField WNDEBS;
  private SNLabelChamp lbNumeroFin;
  private XRiTextField WNFIN;
  private XRiTextField WNFINS;
  private SNLabelChamp lbDateExpedition;
  private SNPanel pnlDateExpedition;
  private XRiCalendrier WDATDX;
  private SNLabelChamp lbAu;
  private XRiCalendrier WDATFX;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlOptionEdition;
  private SNLabelChamp lbNbrExemplaire;
  private XRiTextField WNEX;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
