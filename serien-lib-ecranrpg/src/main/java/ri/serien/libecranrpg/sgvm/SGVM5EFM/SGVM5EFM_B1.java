
package ri.serien.libecranrpg.sgvm.SGVM5EFM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVM5EFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORT = "Exportation tableur";
  
  public SGVM5EFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    OPT3.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT1.setValeursSelection("X", " ");
    OPT4.setValeursSelection("X", " ");
    WTSEC.setValeursSelection("**", "  ");
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    
    // - Ajouter un bouton personnalisé
    snBarreBouton.ajouterBouton(BOUTON_EXPORT, 'e', true);
    // -
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    
    // -- Visible par défaut
    pnlSelectionPlageSections.setVisible(!WTSEC.isSelected());
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charger composant snPlageDate
    snPlageDatesBons.setDateDebutParChampRPG(lexique, "PERDEB");
    snPlageDatesBons.setDateFinParChampRPG(lexique, "PERFIN");
    
    chargerComposantSections();
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Composant snPlageDate
    snPlageDatesBons.renseignerChampRPGDebut(lexique, "PERDEB");
    snPlageDatesBons.renseignerChampRPGFin(lexique, "PERFIN");
    
    snSectionDebut.renseignerChampRPG(lexique, "SECDEB");
    snSectionFin.renseignerChampRPG(lexique, "SECFIN");
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORT)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chargerComposantSections() {
    snSectionDebut.setSession(getSession());
    snSectionDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionDebut.setTousAutorise(false);
    snSectionDebut.charger(false);
    snSectionDebut.setSelectionParChampRPG(lexique, "SECDEB");
    
    snSectionFin.setSession(getSession());
    snSectionFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionFin.setTousAutorise(false);
    snSectionFin.charger(false);
    snSectionFin.setSelectionParChampRPG(lexique, "SECFIN");
  }
  
  private void WTSECItemStateChanged(ItemEvent e) {
    pnlSelectionPlageSections.setVisible(!pnlSelectionPlageSections.isVisible());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantSections();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlPlageSections = new SNPanelTitre();
    WTSEC = new XRiCheckBox();
    pnlSelectionPlageSections = new SNPanel();
    lbSectionDebut = new SNLabelChamp();
    snSectionDebut = new SNSectionAnalytique();
    lbSectionFin = new SNLabelChamp();
    snSectionFin = new SNSectionAnalytique();
    pnlPlageDatesBons = new SNPanelTitre();
    lbPlageDatesBons = new SNLabelChamp();
    snPlageDatesBons = new SNPlageDate();
    pnlNatureMontant = new SNPanelTitre();
    lbNatureMontant = new SNLabelChamp();
    NATMTT = new XRiComboBox();
    pnlSelectionEtatBons = new SNPanelTitre();
    OPT4 = new XRiCheckBox();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 650));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlPlageSections ========
        {
          pnlPlageSections.setTitre("Plage des sections \u00e0 traiter");
          pnlPlageSections.setName("pnlPlageSections");
          pnlPlageSections.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageSections.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlPlageSections.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageSections.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageSections.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- WTSEC ----
          WTSEC.setText("S\u00e9lection compl\u00e8te");
          WTSEC.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTSEC.setMaximumSize(new Dimension(45, 30));
          WTSEC.setMinimumSize(new Dimension(45, 30));
          WTSEC.setPreferredSize(new Dimension(45, 30));
          WTSEC.setName("WTSEC");
          WTSEC.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTSECItemStateChanged(e);
            }
          });
          pnlPlageSections.add(WTSEC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlSelectionPlageSections ========
          {
            pnlSelectionPlageSections.setName("pnlSelectionPlageSections");
            pnlSelectionPlageSections.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelectionPlageSections.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageSections.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelectionPlageSections.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelectionPlageSections.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbSectionDebut ----
            lbSectionDebut.setText("Premi\u00e8re section");
            lbSectionDebut.setName("lbSectionDebut");
            pnlSelectionPlageSections.add(lbSectionDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snSectionDebut ----
            snSectionDebut.setName("snSectionDebut");
            pnlSelectionPlageSections.add(snSectionDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbSectionFin ----
            lbSectionFin.setText("Derni\u00e8re section");
            lbSectionFin.setName("lbSectionFin");
            pnlSelectionPlageSections.add(lbSectionFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snSectionFin ----
            snSectionFin.setName("snSectionFin");
            pnlSelectionPlageSections.add(snSectionFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlageSections.add(pnlSelectionPlageSections, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageSections, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlPlageDatesBons ========
        {
          pnlPlageDatesBons.setTitre("Plage de dates \u00e0 traiter");
          pnlPlageDatesBons.setName("pnlPlageDatesBons");
          pnlPlageDatesBons.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageDatesBons.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbPlageDatesBons ----
          lbPlageDatesBons.setText("Edition des bons");
          lbPlageDatesBons.setName("lbPlageDatesBons");
          pnlPlageDatesBons.add(lbPlageDatesBons, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snPlageDatesBons ----
          snPlageDatesBons.setName("snPlageDatesBons");
          pnlPlageDatesBons.add(snPlageDatesBons, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlPlageDatesBons, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlNatureMontant ========
        {
          pnlNatureMontant.setName("pnlNatureMontant");
          pnlNatureMontant.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlNatureMontant.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlNatureMontant.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlNatureMontant.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlNatureMontant.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbNatureMontant ----
          lbNatureMontant.setText("Nature du montant");
          lbNatureMontant.setName("lbNatureMontant");
          pnlNatureMontant.add(lbNatureMontant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- NATMTT ----
          NATMTT.setModel(new DefaultComboBoxModel(new String[] {
            "HT ",
            "TTC"
          }));
          NATMTT.setFont(new Font("sansserif", Font.PLAIN, 14));
          NATMTT.setMaximumSize(new Dimension(100, 30));
          NATMTT.setMinimumSize(new Dimension(100, 30));
          NATMTT.setPreferredSize(new Dimension(100, 30));
          NATMTT.setName("NATMTT");
          pnlNatureMontant.add(NATMTT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlNatureMontant, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlSelectionEtatBons ========
        {
          pnlSelectionEtatBons.setTitre("S\u00e9lection de l'\u00e9tat des bons \u00e0 traiter");
          pnlSelectionEtatBons.setName("pnlSelectionEtatBons");
          pnlSelectionEtatBons.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlSelectionEtatBons.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlSelectionEtatBons.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlSelectionEtatBons.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlSelectionEtatBons.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- OPT4 ----
          OPT4.setText("Bon en attente non confirm\u00e9s (POS)");
          OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT4.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT4.setMaximumSize(new Dimension(45, 30));
          OPT4.setMinimumSize(new Dimension(45, 30));
          OPT4.setPreferredSize(new Dimension(45, 30));
          OPT4.setName("OPT4");
          pnlSelectionEtatBons.add(OPT4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OPT1 ----
          OPT1.setText("Bons valid\u00e9s non exp\u00e9di\u00e9s (VAL)");
          OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT1.setMaximumSize(new Dimension(45, 30));
          OPT1.setMinimumSize(new Dimension(45, 30));
          OPT1.setPreferredSize(new Dimension(45, 30));
          OPT1.setName("OPT1");
          pnlSelectionEtatBons.add(OPT1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OPT2 ----
          OPT2.setText("Bons exp\u00e9di\u00e9s non factur\u00e9s (EXP)");
          OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT2.setMaximumSize(new Dimension(45, 30));
          OPT2.setMinimumSize(new Dimension(45, 30));
          OPT2.setPreferredSize(new Dimension(45, 30));
          OPT2.setName("OPT2");
          pnlSelectionEtatBons.add(OPT2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OPT3 ----
          OPT3.setText("Bons factur\u00e9s (FAC)");
          OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
          OPT3.setMaximumSize(new Dimension(45, 30));
          OPT3.setMinimumSize(new Dimension(45, 30));
          OPT3.setPreferredSize(new Dimension(45, 30));
          OPT3.setName("OPT3");
          pnlSelectionEtatBons.add(OPT3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlSelectionEtatBons, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setEditable(false);
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlPlageSections;
  private XRiCheckBox WTSEC;
  private SNPanel pnlSelectionPlageSections;
  private SNLabelChamp lbSectionDebut;
  private SNSectionAnalytique snSectionDebut;
  private SNLabelChamp lbSectionFin;
  private SNSectionAnalytique snSectionFin;
  private SNPanelTitre pnlPlageDatesBons;
  private SNLabelChamp lbPlageDatesBons;
  private SNPlageDate snPlageDatesBons;
  private SNPanelTitre pnlNatureMontant;
  private SNLabelChamp lbNatureMontant;
  private XRiComboBox NATMTT;
  private SNPanelTitre pnlSelectionEtatBons;
  private XRiCheckBox OPT4;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
