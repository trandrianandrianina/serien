
package ri.serien.libecranrpg.sgvm.SGVMC7FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVMC7FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] EXCEL_Value = { "O", "N", "X" };
  
  public SGVMC7FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    EXCEL.setValeurs(EXCEL_Value, null);
    WTCLT.setValeursSelection("**", "");
    
    // Initialisation de la barre des boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation composant client 1
    snClientPrincipal1.setSession(getSession());
    snClientPrincipal1.setIdEtablissement(snEtablissement.getIdSelection());
    snClientPrincipal1.charger(false);
    snClientPrincipal1.setSelectionParChampRPG(lexique, "CLIDEB");
    
    // Initialisation composant client 2
    snClientPrincipal2.setSession(getSession());
    snClientPrincipal2.setIdEtablissement(snEtablissement.getIdSelection());
    snClientPrincipal2.charger(false);
    snClientPrincipal2.setSelectionParChampRPG(lexique, "CLIFIN");
    
    // Initialisation composant représentant 1
    snRepresentant1.setSession(getSession());
    snRepresentant1.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentant1.charger(false);
    snRepresentant1.setSelectionParChampRPG(lexique, "REPDEB");
    
    // Initialisation composant représentant 2
    snRepresentant2.setSession(getSession());
    snRepresentant2.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentant2.charger(false);
    snRepresentant2.setSelectionParChampRPG(lexique, "REPFIN");
    
    // Initialisation composant fournisseur 1
    snFournisseur1.setSession(getSession());
    snFournisseur1.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur1.charger(false);
    snFournisseur1.setSelectionParChampRPG(lexique, "WFRS1");
    
    // Initialisation composant fournisseur 2
    snFournisseur2.setSession(getSession());
    snFournisseur2.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur2.charger(false);
    snFournisseur2.setSelectionParChampRPG(lexique, "WFRS2");
    
    // Initialisation composant date
    snPlageDate.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPlageDate.setDateDebutParChampRPG(lexique, "WDAPD");
    snPlageDate.setDateFinParChampRPG(lexique, "WDAPF");
    
    // Si on a choisi "tous" on cache les champs de sélection de plage
    if (WTCLT.isSelected()) {
      lbClient1.setVisible(false);
      lbClient2.setVisible(false);
      snClientPrincipal1.setVisible(false);
      snClientPrincipal2.setVisible(false);
    }
    else {
      lbClient1.setVisible(true);
      lbClient2.setVisible(true);
      snClientPrincipal1.setVisible(true);
      snClientPrincipal2.setVisible(true);
    }
    
    p_bpresentation.setIdEtablissement(snEtablissement.getIdSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snClientPrincipal1.renseignerChampRPG(lexique, "CLIDEB");
    snClientPrincipal2.renseignerChampRPG(lexique, "CLIFIN");
    snRepresentant1.renseignerChampRPG(lexique, "REPDEB");
    snRepresentant2.renseignerChampRPG(lexique, "REPFIN");
    snFournisseur1.renseignerChampRPG(lexique, "WFRS1");
    snFournisseur2.renseignerChampRPG(lexique, "WFRS2");
    snPlageDate.renseignerChampRPGDebut(lexique, "WDAPD");
    snPlageDate.renseignerChampRPGFin(lexique, "WDAPF");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    try {
      // Si on a choisi "tous" on cache les champs de sélection de plage
      if (WTCLT.isSelected()) {
        lbClient1.setVisible(false);
        lbClient2.setVisible(false);
        snClientPrincipal1.setVisible(false);
        snClientPrincipal2.setVisible(false);
      }
      else {
        lbClient1.setVisible(true);
        lbClient2.setVisible(true);
        snClientPrincipal1.setVisible(true);
        snClientPrincipal2.setVisible(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    WTCLT = new XRiCheckBox();
    lbClient1 = new SNLabelChamp();
    snClientPrincipal1 = new SNClientPrincipal();
    lbClient2 = new SNLabelChamp();
    snClientPrincipal2 = new SNClientPrincipal();
    lbRepresentant1 = new SNLabelChamp();
    snRepresentant1 = new SNRepresentant();
    lbRepresentant2 = new SNLabelChamp();
    snRepresentant2 = new SNRepresentant();
    lbFournisseur1 = new SNLabelChamp();
    snFournisseur1 = new SNFournisseur();
    lbFournisseur2 = new SNLabelChamp();
    snFournisseur2 = new SNFournisseur();
    lbDate = new SNLabelChamp();
    snPlageDate = new SNPlageDate();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlOptions = new SNPanelTitre();
    EXCEL = new XRiComboBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- WTCLT ----
          WTCLT.setText("Tous les clients");
          WTCLT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTCLT.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTCLT.setMinimumSize(new Dimension(124, 30));
          WTCLT.setPreferredSize(new Dimension(124, 30));
          WTCLT.setName("WTCLT");
          WTCLT.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WTOUItemStateChanged(e);
            }
          });
          pnlCritereSelection.add(WTCLT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbClient1 ----
          lbClient1.setText("Client de d\u00e9but");
          lbClient1.setName("lbClient1");
          pnlCritereSelection.add(lbClient1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snClientPrincipal1 ----
          snClientPrincipal1.setName("snClientPrincipal1");
          pnlCritereSelection.add(snClientPrincipal1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbClient2 ----
          lbClient2.setText("Client de fin");
          lbClient2.setName("lbClient2");
          pnlCritereSelection.add(lbClient2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snClientPrincipal2 ----
          snClientPrincipal2.setName("snClientPrincipal2");
          pnlCritereSelection.add(snClientPrincipal2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbRepresentant1 ----
          lbRepresentant1.setText("Repr\u00e9sentant de d\u00e9but");
          lbRepresentant1.setName("lbRepresentant1");
          pnlCritereSelection.add(lbRepresentant1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snRepresentant1 ----
          snRepresentant1.setName("snRepresentant1");
          pnlCritereSelection.add(snRepresentant1, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbRepresentant2 ----
          lbRepresentant2.setText("Repr\u00e9sentant de fin");
          lbRepresentant2.setName("lbRepresentant2");
          pnlCritereSelection.add(lbRepresentant2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snRepresentant2 ----
          snRepresentant2.setName("snRepresentant2");
          pnlCritereSelection.add(snRepresentant2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbFournisseur1 ----
          lbFournisseur1.setText("Fournisseur de d\u00e9but");
          lbFournisseur1.setName("lbFournisseur1");
          pnlCritereSelection.add(lbFournisseur1, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snFournisseur1 ----
          snFournisseur1.setName("snFournisseur1");
          pnlCritereSelection.add(snFournisseur1, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbFournisseur2 ----
          lbFournisseur2.setText("Fournisseur de fin");
          lbFournisseur2.setName("lbFournisseur2");
          pnlCritereSelection.add(lbFournisseur2, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snFournisseur2 ----
          snFournisseur2.setName("snFournisseur2");
          pnlCritereSelection.add(snFournisseur2, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDate ----
          lbDate.setText("Date de validit\u00e9");
          lbDate.setName("lbDate");
          pnlCritereSelection.add(lbDate, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snPlageDate ----
          snPlageDate.setName("snPlageDate");
          pnlCritereSelection.add(snPlageDate, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfEnCours ----
          tfEnCours.setText("@WENCX@");
          tfEnCours.setEnabled(false);
          tfEnCours.setPreferredSize(new Dimension(260, 30));
          tfEnCours.setMinimumSize(new Dimension(260, 30));
          tfEnCours.setMaximumSize(new Dimension(260, 30));
          tfEnCours.setName("tfEnCours");
          pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOptions ========
        {
          pnlOptions.setTitre("Options d'\u00e9dition");
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- EXCEL ----
          EXCEL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EXCEL.setModel(new DefaultComboBoxModel(new String[] { "Tableur", "Papier", "XML" }));
          EXCEL.setBackground(Color.white);
          EXCEL.setFont(new Font("sansserif", Font.PLAIN, 14));
          EXCEL.setName("EXCEL");
          pnlOptions.add(EXCEL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private XRiCheckBox WTCLT;
  private SNLabelChamp lbClient1;
  private SNClientPrincipal snClientPrincipal1;
  private SNLabelChamp lbClient2;
  private SNClientPrincipal snClientPrincipal2;
  private SNLabelChamp lbRepresentant1;
  private SNRepresentant snRepresentant1;
  private SNLabelChamp lbRepresentant2;
  private SNRepresentant snRepresentant2;
  private SNLabelChamp lbFournisseur1;
  private SNFournisseur snFournisseur1;
  private SNLabelChamp lbFournisseur2;
  private SNFournisseur snFournisseur2;
  private SNLabelChamp lbDate;
  private SNPlageDate snPlageDate;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlOptions;
  private XRiComboBox EXCEL;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
