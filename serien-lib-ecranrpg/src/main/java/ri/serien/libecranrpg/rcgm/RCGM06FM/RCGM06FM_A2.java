
package ri.serien.libecranrpg.rcgm.RCGM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RCGM06FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 538, };
  private String[] WSOLDE_Value = { "", "1", "2", "3", "4", };
  private String[] WSOLDE_Title = { "", "Tous", "Solde différent de zéro", "Débiteurs", "Créditeurs", };
  private Color[][] _LIST_Text_Color = new Color[_WTP01_Top.length][1];
  private Color[][] _LIST_Fond_Color = null;
  
  public RCGM06FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WSOLDE.setValeurs(WSOLDE_Value, WSOLDE_Title);
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, _LIST_Text_Color, _LIST_Fond_Color, null);
    TIDX5.setValeurs("X");
    TIDX4.setValeurs("X");
    TIDX6.setValeurs("X");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    CHOISIR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+1=@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+2=@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+3=@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+4=@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+5=@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+6=@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+7=@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT/+8=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Couleurs de la liste (suivant indicateurs de 21 à 35)
    for (int i = 0; i < 15; i++) {
      
      if (lexique.isTrue(String.valueOf(i + 21))) {
        _LIST_Text_Color[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
      }
      
    }
    // Ajoute à la liste des oData les variables non liée directement à un composant graphique
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top, null, _LIST_Text_Color, _LIST_Fond_Color, null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // WSOLDE.setSelectedIndex(getIndice("WSOLDE", WSOLDE_Value));
    // SCAN4.setSelected(lexique.HostFieldGetData("SCAN4").equalsIgnoreCase("1"));
    // SCAN5.setSelected(lexique.HostFieldGetData("SCAN5").equalsIgnoreCase("1"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("5"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("4"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("6"));
    CHOISIR.setVisible(!interpreteurD.analyseExpression("@OPT/+1=@").trim().equalsIgnoreCase(""));
    OBJ_20.setVisible(!interpreteurD.analyseExpression("@OPT/+2=@").trim().equalsIgnoreCase(""));
    OBJ_21.setVisible(!interpreteurD.analyseExpression("@OPT/+3=@").trim().equalsIgnoreCase(""));
    OBJ_22.setVisible(!interpreteurD.analyseExpression("@OPT/+4=@").trim().equalsIgnoreCase(""));
    OBJ_23.setVisible(!interpreteurD.analyseExpression("@OPT/+5=@").trim().equalsIgnoreCase(""));
    OBJ_24.setVisible(!interpreteurD.analyseExpression("@OPT/+6=@").trim().equalsIgnoreCase(""));
    OBJ_25.setVisible(!interpreteurD.analyseExpression("@OPT/+7=@").trim().equalsIgnoreCase(""));
    OBJ_26.setVisible(!interpreteurD.analyseExpression("@OPT/+8=@").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    // BT_PFIN.setIcon(lexique.getImage("images/pfin20.png", true));
    // BT_PDEB.setIcon(lexique.getImage("images/pdeb20.png", true));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    BT_PDEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    BT_PFIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    
    // Titre
    // setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Résultat de la recherche @PC6LIB@")));
    setTitle(p_bpresentation.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WSOLDE", 0, WSOLDE_Value[WSOLDE.getSelectedIndex()]);
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "5");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "4");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "6");
    // if (SCAN4.isSelected())
    // lexique.HostFieldPutData("SCAN4", 0, "1");
    // else
    // lexique.HostFieldPutData("SCAN4", 0, " ");
    // if (SCAN5.isSelected())
    // lexique.HostFieldPutData("SCAN5", 0, "1");
    // else
    // lexique.HostFieldPutData("SCAN5", 0, " ");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_77ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_79ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-1=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-1=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-2=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-2=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-3=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-3=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-4=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-4=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-5=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-5=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-6=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-6=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-7=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-7=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, interpreteurD.analyseExpression("@OPT/-8=@"), "Enter");
    WTP01.setValeurTop(interpreteurD.analyseExpression("@OPT/-8=@"));
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void TRIAGEActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40 = new JLabel();
    WETB = new XRiTextField();
    OBJ_42 = new JLabel();
    INDNCG = new XRiTextField();
    OBJ_44 = new JLabel();
    INDNCA = new XRiTextField();
    OBJ_71 = new JLabel();
    FMTX = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Center = new JPanel();
    BT_PGUP = new JButton();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGDOWN = new JButton();
    BT_PDEB = new JButton();
    BT_PFIN = new JButton();
    panel4 = new JPanel();
    TIDX4 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    ARG4A = new XRiTextField();
    ARG5A = new XRiTextField();
    TIDX6 = new XRiRadioButton();
    ARG7A = new XRiTextField();
    OBJ_105 = new JLabel();
    ARG6N = new XRiTextField();
    ARG32A = new XRiTextField();
    OBJ_107 = new JLabel();
    panel3 = new JPanel();
    WSOLDE = new XRiComboBox();
    SOLD1 = new JLabel();
    OBJ_109 = new JLabel();
    WSDEB = new XRiTextField();
    WSFIN = new XRiTextField();
    OBJ_110 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    TRIAGE = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_28 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Recherche de comptes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40 ----
          OBJ_40.setText("Soci\u00e9t\u00e9");
          OBJ_40.setName("OBJ_40");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- OBJ_42 ----
          OBJ_42.setText("Compte g\u00e9n\u00e9ral");
          OBJ_42.setName("OBJ_42");

          //---- INDNCG ----
          INDNCG.setToolTipText("Saisissez num\u00e9ro de compte collectif ou d\u00e9but de compte g\u00e9n\u00e9ral");
          INDNCG.setComponentPopupMenu(BTD);
          INDNCG.setName("INDNCG");

          //---- OBJ_44 ----
          OBJ_44.setText("Auxiliaire");
          OBJ_44.setName("OBJ_44");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(BTD);
          INDNCA.setName("INDNCA");

          //---- OBJ_71 ----
          OBJ_71.setText("Format");
          OBJ_71.setName("OBJ_71");

          //---- FMTX ----
          FMTX.setComponentPopupMenu(BTD);
          FMTX.setName("FMTX");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(FMTX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(FMTX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_40)
                  .addComponent(OBJ_42)
                  .addComponent(OBJ_44)
                  .addComponent(OBJ_71)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Comptes d\u00e9sactiv\u00e9s");
              riSousMenu_bt6.setToolTipText("Affichage des comptes d\u00e9sactiv\u00e9s");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(930, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== P_Center ========
          {
            P_Center.setOpaque(false);
            P_Center.setName("P_Center");

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");

            //---- BT_PDEB ----
            BT_PDEB.setText("");
            BT_PDEB.setToolTipText("D\u00e9but de la liste");
            BT_PDEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PDEB.setName("BT_PDEB");
            BT_PDEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_77ActionPerformed(e);
              }
            });

            //---- BT_PFIN ----
            BT_PFIN.setText("");
            BT_PFIN.setToolTipText("Fin de la liste");
            BT_PFIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PFIN.setName("BT_PFIN");
            BT_PFIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_79ActionPerformed(e);
              }
            });

            //======== panel4 ========
            {
              panel4.setBackground(new Color(214, 217, 223));
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- TIDX4 ----
              TIDX4.setText("Clef de classement 1");
              TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX4.setName("TIDX4");
              panel4.add(TIDX4);
              TIDX4.setBounds(10, 24, 190, 20);

              //---- TIDX5 ----
              TIDX5.setText("Clef de classement 2");
              TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX5.setName("TIDX5");
              panel4.add(TIDX5);
              TIDX5.setBounds(10, 59, 151, 20);

              //---- ARG4A ----
              ARG4A.setComponentPopupMenu(BTD);
              ARG4A.setName("ARG4A");
              panel4.add(ARG4A);
              ARG4A.setBounds(200, 20, 160, ARG4A.getPreferredSize().height);

              //---- ARG5A ----
              ARG5A.setComponentPopupMenu(BTD);
              ARG5A.setName("ARG5A");
              panel4.add(ARG5A);
              ARG5A.setBounds(200, 55, 160, ARG5A.getPreferredSize().height);

              //---- TIDX6 ----
              TIDX6.setText("Code pays / Code postal");
              TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX6.setName("TIDX6");
              panel4.add(TIDX6);
              TIDX6.setBounds(10, 94, 160, 20);

              //---- ARG7A ----
              ARG7A.setComponentPopupMenu(BTD);
              ARG7A.setName("ARG7A");
              panel4.add(ARG7A);
              ARG7A.setBounds(200, 90, 36, ARG7A.getPreferredSize().height);

              //---- OBJ_105 ----
              OBJ_105.setText("/");
              OBJ_105.setName("OBJ_105");
              panel4.add(OBJ_105);
              OBJ_105.setBounds(245, 90, 10, 28);

              //---- ARG6N ----
              ARG6N.setComponentPopupMenu(BTD);
              ARG6N.setName("ARG6N");
              panel4.add(ARG6N);
              ARG6N.setBounds(255, 90, 52, ARG6N.getPreferredSize().height);

              //---- ARG32A ----
              ARG32A.setComponentPopupMenu(BTD);
              ARG32A.setName("ARG32A");
              panel4.add(ARG32A);
              ARG32A.setBounds(200, 125, 160, ARG32A.getPreferredSize().height);

              //---- OBJ_107 ----
              OBJ_107.setText("Recherche sur ville");
              OBJ_107.setName("OBJ_107");
              panel4.add(OBJ_107);
              OBJ_107.setBounds(33, 129, 115, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }

            //======== panel3 ========
            {
              panel3.setBackground(new Color(214, 217, 223));
              panel3.setBorder(new TitledBorder("Solde"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- WSOLDE ----
              WSOLDE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WSOLDE.setName("WSOLDE");
              panel3.add(WSOLDE);
              WSOLDE.setBounds(190, 40, 204, WSOLDE.getPreferredSize().height);

              //---- SOLD1 ----
              SOLD1.setText("Affichage du solde");
              SOLD1.setComponentPopupMenu(BTD);
              SOLD1.setName("SOLD1");
              panel3.add(SOLD1);
              SOLD1.setBounds(25, 43, 145, 20);

              //---- OBJ_109 ----
              OBJ_109.setText("Solde compris entre");
              OBJ_109.setName("OBJ_109");
              panel3.add(OBJ_109);
              OBJ_109.setBounds(25, 80, 145, 20);

              //---- WSDEB ----
              WSDEB.setComponentPopupMenu(BTD);
              WSDEB.setName("WSDEB");
              panel3.add(WSDEB);
              WSDEB.setBounds(190, 76, 96, WSDEB.getPreferredSize().height);

              //---- WSFIN ----
              WSFIN.setComponentPopupMenu(BTD);
              WSFIN.setName("WSFIN");
              panel3.add(WSFIN);
              WSFIN.setBounds(190, 114, 96, WSFIN.getPreferredSize().height);

              //---- OBJ_110 ----
              OBJ_110.setText("et");
              OBJ_110.setName("OBJ_110");
              panel3.add(OBJ_110);
              OBJ_110.setBounds(25, 118, 145, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }

            GroupLayout P_CenterLayout = new GroupLayout(P_Center);
            P_Center.setLayout(P_CenterLayout);
            P_CenterLayout.setHorizontalGroup(
              P_CenterLayout.createParallelGroup()
                .addGroup(P_CenterLayout.createSequentialGroup()
                  .addGap(30, 30, 30)
                  .addGroup(P_CenterLayout.createParallelGroup()
                    .addGroup(P_CenterLayout.createSequentialGroup()
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE))
                    .addGroup(P_CenterLayout.createSequentialGroup()
                      .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 830, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addGroup(P_CenterLayout.createParallelGroup()
                        .addComponent(BT_PDEB, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PFIN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(14, Short.MAX_VALUE))
            );
            P_CenterLayout.setVerticalGroup(
              P_CenterLayout.createParallelGroup()
                .addGroup(P_CenterLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addGroup(P_CenterLayout.createParallelGroup()
                    .addGroup(P_CenterLayout.createSequentialGroup()
                      .addGap(18, 18, 18)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3)
                  .addGroup(P_CenterLayout.createParallelGroup()
                    .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addGroup(P_CenterLayout.createSequentialGroup()
                      .addComponent(BT_PDEB, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(BT_PFIN, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(P_Center, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(P_Center, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("@OPT/+1=@");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_20 ----
      OBJ_20.setText("@OPT/+2=@");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("@OPT/+3=@");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("@OPT/+4=@");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("@OPT/+5=@");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("@OPT/+6=@");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("@OPT/+7=@");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("@OPT/+8=@");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- TRIAGE ----
      TRIAGE.setText("Tri\u00e9 par ...");
      TRIAGE.setName("TRIAGE");
      TRIAGE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          TRIAGEActionPerformed(e);
        }
      });
      BTD.add(TRIAGE);
      BTD.addSeparator();

      //---- OBJ_29 ----
      OBJ_29.setText("Choix possibles");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_28 ----
      OBJ_28.setText("Aide en ligne");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);
    }

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(TIDX4);
    buttonGroup1.add(TIDX5);
    buttonGroup1.add(TIDX6);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40;
  private XRiTextField WETB;
  private JLabel OBJ_42;
  private XRiTextField INDNCG;
  private JLabel OBJ_44;
  private XRiTextField INDNCA;
  private JLabel OBJ_71;
  private XRiTextField FMTX;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Center;
  private JButton BT_PGUP;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGDOWN;
  private JButton BT_PDEB;
  private JButton BT_PFIN;
  private JPanel panel4;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG4A;
  private XRiTextField ARG5A;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG7A;
  private JLabel OBJ_105;
  private XRiTextField ARG6N;
  private XRiTextField ARG32A;
  private JLabel OBJ_107;
  private JPanel panel3;
  private XRiComboBox WSOLDE;
  private JLabel SOLD1;
  private JLabel OBJ_109;
  private XRiTextField WSDEB;
  private XRiTextField WSFIN;
  private JLabel OBJ_110;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem TRIAGE;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_28;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
