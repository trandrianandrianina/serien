
package ri.serien.libecranrpg.rcgm.RCGM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RCGM06FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] WSOLDE_Value = { "", "1", "2", "3", "4", };
  private String[] WSOLDE_Title = { "", "Tous", "Solde différent de zéro", "Débiteurs", "Créditeurs", };
  private String[] ARG23N_Value = { "", "2", "1", "9", };
  private String[] ARG23N_Title = { "", "Message en rouge", "Message / compte ou saisie", "Désactivé", };
  
  public RCGM06FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ARG23N.setValeurs(ARG23N_Value, ARG23N_Title);
    WSOLDE.setValeurs(WSOLDE_Value, WSOLDE_Title);
    SCAN23.setValeurs(" ", SCAN23_GRP);
    SCAN23_B.setValeurs("*");
    SCAN23_C.setValeurs("N");
    TIDX5.setValeurs("5", "RBC");
    TIDX4.setValeurs("4", "RBC");
    TIDX6.setValeurs("6", "RBC");
    TIDX11.setValeurs("11", "RBC");
    TIDX9.setValeurs("9", "RBC");
    TIDX12.setValeurs("12", "RBC");
    TIDX14.setValeurs("14", "RBC");
    TIDX8.setValeurs("8", "RBC");
    TIDX23.setValeurs("23", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX30.setValeurs("30", "RBC");
    TIDX3.setValeurs("3", "RBC");
    TIDX15.setValeurs("15", "RBC");
    TIDX20.setValeurs("20", "RBC");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WCSNDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCSNDX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_94.setSelected(lexique.HostFieldGetData("SCAN23").equalsIgnoreCase("S"));
    // OBJ_62.setSelected(lexique.HostFieldGetData("SCAN5").equalsIgnoreCase("S"));
    // OBJ_59.setSelected(lexique.HostFieldGetData("SCAN4").equalsIgnoreCase("S"));
    
    DATPR.setVisible(lexique.isTrue("91"));
    l_DATPR.setVisible(DATPR.isVisible());
    
    // duplication
    barre_tete2.setVisible(lexique.isTrue("56"));
    
    // Titre
    // setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Recherche comptes @LIBPG@"));
    setTitle(p_bpresentation.getText());
    
    

    
    p_bpresentation.setCodeEtablissement(INDSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDSOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OBJ_62.isSelected()) {
    // lexique.HostFieldPutData("SCAN5", 0, "S");
    // }
    // else {
    // lexique.HostFieldPutData("SCAN5", 0, " ");
    // }
    // if (OBJ_59.isSelected()) {
    // lexique.HostFieldPutData("SCAN4", 0, "S");
    // }
    // else {
    // lexique.HostFieldPutData("SCAN4", 0, " ");
    // }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void SCAN23StateChanged(ChangeEvent e) {
    ARG23N.setEnabled(!SCAN23.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40 = new JLabel();
    INDSOC = new XRiTextField();
    OBJ_42 = new JLabel();
    INDNCG = new XRiTextField();
    OBJ_44 = new JLabel();
    INDNCA = new XRiTextField();
    l_DATPR = new JLabel();
    DATPR = new XRiTextField();
    p_tete_droite = new JPanel();
    barre_tete2 = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_45 = new JLabel();
    WISOC3 = new XRiTextField();
    WINCG3 = new XRiTextField();
    WINCA3 = new XRiTextField();
    p_tete_droite2 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel7 = new JPanel();
    panel1 = new JPanel();
    TIDX12 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    TIDX11 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    ARG4A = new XRiTextField();
    ARG5A = new XRiTextField();
    ARG33A = new XRiTextField();
    OBJ_103 = new JLabel();
    TIDX15 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG32A = new XRiTextField();
    OBJ_107 = new JLabel();
    TIDX8 = new XRiRadioButton();
    TIDX14 = new XRiRadioButton();
    ARG11A = new XRiTextField();
    ARG10A = new XRiTextField();
    ARG15A = new XRiTextField();
    ARG16A = new XRiTextField();
    ARG17A = new XRiTextField();
    ARG18A = new XRiTextField();
    ARG19A = new XRiTextField();
    ARG3N = new XRiTextField();
    ARG6N = new XRiTextField();
    ARG14A = new XRiTextField();
    ARG7A = new XRiTextField();
    ARG8A = new XRiTextField();
    ARG9A = new XRiTextField();
    ARG12A = new XRiTextField();
    ARG13A = new XRiTextField();
    OBJ_105 = new JLabel();
    OBJ_112 = new JLabel();
    WCSNDX = new RiZoneSortie();
    panel3 = new JPanel();
    WSOLDE = new XRiComboBox();
    SOLD1 = new JLabel();
    OBJ_109 = new JLabel();
    WSDEB = new XRiTextField();
    WSFIN = new XRiTextField();
    OBJ_110 = new JLabel();
    panel2 = new JPanel();
    TIDX20 = new XRiRadioButton();
    TIDX21 = new XRiRadioButton();
    TIDX23 = new XRiRadioButton();
    ARG22A = new XRiTextField();
    ARG21N = new XRiTextField();
    ARG20A = new XRiTextField();
    OBJ_94 = new JCheckBox();
    panel4 = new JPanel();
    TIDX30 = new XRiRadioButton();
    ARG30D = new XRiCalendrier();
    panel5 = new JPanel();
    SCAN23 = new XRiRadioButton();
    SCAN23_B = new XRiRadioButton();
    SCAN23_C = new XRiRadioButton();
    ARG23N = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    RBC_GRP = new ButtonGroup();
    SCAN23_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Recherche de comptes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche.setMinimumSize(new Dimension(800, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40 ----
          OBJ_40.setText("Soci\u00e9t\u00e9");
          OBJ_40.setName("OBJ_40");

          //---- INDSOC ----
          INDSOC.setComponentPopupMenu(BTD);
          INDSOC.setName("INDSOC");

          //---- OBJ_42 ----
          OBJ_42.setText("Compte g\u00e9n\u00e9ral");
          OBJ_42.setName("OBJ_42");

          //---- INDNCG ----
          INDNCG.setToolTipText("Saisissez num\u00e9ro de compte collectif ou d\u00e9but de compte g\u00e9n\u00e9ral");
          INDNCG.setComponentPopupMenu(BTD);
          INDNCG.setName("INDNCG");

          //---- OBJ_44 ----
          OBJ_44.setText("Auxiliaire");
          OBJ_44.setName("OBJ_44");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(BTD);
          INDNCA.setName("INDNCA");

          //---- l_DATPR ----
          l_DATPR.setText("Date pour pr\u00e9visionnel");
          l_DATPR.setName("l_DATPR");

          //---- DATPR ----
          DATPR.setName("DATPR");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(l_DATPR, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DATPR, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DATPR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_40)
                  .addComponent(OBJ_42)
                  .addComponent(OBJ_44)
                  .addComponent(l_DATPR)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_tete2 ========
      {
        barre_tete2.setMinimumSize(new Dimension(111, 34));
        barre_tete2.setPreferredSize(new Dimension(111, 34));
        barre_tete2.setName("barre_tete2");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche2.setMinimumSize(new Dimension(800, 32));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- OBJ_41 ----
          OBJ_41.setText("Par duplication de");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_43 ----
          OBJ_43.setText("Compte g\u00e9n\u00e9ral");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_45 ----
          OBJ_45.setText("Auxiliaire");
          OBJ_45.setName("OBJ_45");

          //---- WISOC3 ----
          WISOC3.setComponentPopupMenu(BTD);
          WISOC3.setToolTipText("Par duplication");
          WISOC3.setName("WISOC3");

          //---- WINCG3 ----
          WINCG3.setToolTipText("Par duplication");
          WINCG3.setComponentPopupMenu(BTD);
          WINCG3.setName("WINCG3");

          //---- WINCA3 ----
          WINCA3.setComponentPopupMenu(BTD);
          WINCA3.setToolTipText("Par duplication");
          WINCA3.setName("WINCA3");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WISOC3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WINCG3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WINCA3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addComponent(WISOC3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WINCG3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WINCA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gauche2Layout.createParallelGroup()
                  .addComponent(OBJ_41)
                  .addComponent(OBJ_43)
                  .addComponent(OBJ_45)))
          );
        }
        barre_tete2.add(p_tete_gauche2);

        //======== p_tete_droite2 ========
        {
          p_tete_droite2.setOpaque(false);
          p_tete_droite2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite2.setPreferredSize(new Dimension(150, 0));
          p_tete_droite2.setMinimumSize(new Dimension(150, 0));
          p_tete_droite2.setName("p_tete_droite2");
          p_tete_droite2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete2.add(p_tete_droite2);
      }
      p_nord.add(barre_tete2);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("recherche PCG");
              riSousMenu_bt6.setToolTipText("recherche plan comptable g\u00e9n\u00e9ral");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("recherche d'\u00e9critures");
              riSousMenu_bt7.setToolTipText("recherche d'\u00e9critures");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("M\u00e9morisation curseur");
              riSousMenu_bt14.setToolTipText("M\u00e9morisation position curseur");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(null, "Recherche sur tiers", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- TIDX12 ----
              TIDX12.setText("Code r\u00e9glement / \u00e9ch\u00e9ance");
              TIDX12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX12.setName("TIDX12");
              panel1.add(TIDX12);
              TIDX12.setBounds(25, 347, 190, 20);

              //---- TIDX9 ----
              TIDX9.setText("N\u00b0 Ident.(Pays/Num\u00e9ro)");
              TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX9.setName("TIDX9");
              panel1.add(TIDX9);
              TIDX9.setBounds(25, 279, 190, 20);

              //---- TIDX11 ----
              TIDX11.setText("Num\u00e9ro de t\u00e9l\u00e9phone");
              TIDX11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX11.setName("TIDX11");
              panel1.add(TIDX11);
              TIDX11.setBounds(25, 313, 190, 20);

              //---- TIDX6 ----
              TIDX6.setText("Code pays / Code postal");
              TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX6.setName("TIDX6");
              panel1.add(TIDX6);
              TIDX6.setBounds(25, 143, 190, 20);

              //---- TIDX4 ----
              TIDX4.setText("Clef de classement 1");
              TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX4.setName("TIDX4");
              panel1.add(TIDX4);
              TIDX4.setBounds(25, 41, 190, 20);

              //---- TIDX5 ----
              TIDX5.setText("Clef de classement 2");
              TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX5.setName("TIDX5");
              panel1.add(TIDX5);
              TIDX5.setBounds(25, 75, 190, 20);

              //---- ARG4A ----
              ARG4A.setComponentPopupMenu(BTD);
              ARG4A.setName("ARG4A");
              panel1.add(ARG4A);
              ARG4A.setBounds(216, 37, 160, ARG4A.getPreferredSize().height);

              //---- ARG5A ----
              ARG5A.setComponentPopupMenu(BTD);
              ARG5A.setName("ARG5A");
              panel1.add(ARG5A);
              ARG5A.setBounds(215, 71, 160, ARG5A.getPreferredSize().height);

              //---- ARG33A ----
              ARG33A.setComponentPopupMenu(BTD);
              ARG33A.setName("ARG33A");
              panel1.add(ARG33A);
              ARG33A.setBounds(215, 105, 160, ARG33A.getPreferredSize().height);

              //---- OBJ_103 ----
              OBJ_103.setText("Recherche phon\u00e9tique");
              OBJ_103.setName("OBJ_103");
              panel1.add(OBJ_103);
              OBJ_103.setBounds(45, 109, 175, 20);

              //---- TIDX15 ----
              TIDX15.setText("Crit\u00e8res de s\u00e9lections");
              TIDX15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX15.setName("TIDX15");
              panel1.add(TIDX15);
              TIDX15.setBounds(25, 415, 190, 20);

              //---- TIDX3 ----
              TIDX3.setText("Num\u00e9ro auxiliaire");
              TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX3.setName("TIDX3");
              panel1.add(TIDX3);
              TIDX3.setBounds(25, 245, 190, 20);

              //---- ARG32A ----
              ARG32A.setComponentPopupMenu(BTD);
              ARG32A.setName("ARG32A");
              panel1.add(ARG32A);
              ARG32A.setBounds(216, 173, 160, ARG32A.getPreferredSize().height);

              //---- OBJ_107 ----
              OBJ_107.setText("Recherche sur ville");
              OBJ_107.setName("OBJ_107");
              panel1.add(OBJ_107);
              OBJ_107.setBounds(45, 177, 175, 20);

              //---- TIDX8 ----
              TIDX8.setText("Repr\u00e9sentant");
              TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX8.setName("TIDX8");
              panel1.add(TIDX8);
              TIDX8.setBounds(25, 211, 190, 20);

              //---- TIDX14 ----
              TIDX14.setText("Code devise");
              TIDX14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX14.setName("TIDX14");
              panel1.add(TIDX14);
              TIDX14.setBounds(25, 381, 190, 20);

              //---- ARG11A ----
              ARG11A.setComponentPopupMenu(BTD);
              ARG11A.setName("ARG11A");
              panel1.add(ARG11A);
              ARG11A.setBounds(215, 309, 100, ARG11A.getPreferredSize().height);

              //---- ARG10A ----
              ARG10A.setComponentPopupMenu(BTD);
              ARG10A.setName("ARG10A");
              panel1.add(ARG10A);
              ARG10A.setBounds(250, 275, 84, ARG10A.getPreferredSize().height);

              //---- ARG15A ----
              ARG15A.setComponentPopupMenu(BTD);
              ARG15A.setName("ARG15A");
              panel1.add(ARG15A);
              ARG15A.setBounds(40, 437, 52, ARG15A.getPreferredSize().height);

              //---- ARG16A ----
              ARG16A.setComponentPopupMenu(BTD);
              ARG16A.setName("ARG16A");
              panel1.add(ARG16A);
              ARG16A.setBounds(100, 437, 52, ARG16A.getPreferredSize().height);

              //---- ARG17A ----
              ARG17A.setComponentPopupMenu(BTD);
              ARG17A.setName("ARG17A");
              panel1.add(ARG17A);
              ARG17A.setBounds(155, 437, 52, ARG17A.getPreferredSize().height);

              //---- ARG18A ----
              ARG18A.setComponentPopupMenu(BTD);
              ARG18A.setName("ARG18A");
              panel1.add(ARG18A);
              ARG18A.setBounds(210, 437, 52, ARG18A.getPreferredSize().height);

              //---- ARG19A ----
              ARG19A.setComponentPopupMenu(BTD);
              ARG19A.setName("ARG19A");
              panel1.add(ARG19A);
              ARG19A.setBounds(270, 437, 52, ARG19A.getPreferredSize().height);

              //---- ARG3N ----
              ARG3N.setComponentPopupMenu(BTD);
              ARG3N.setName("ARG3N");
              panel1.add(ARG3N);
              ARG3N.setBounds(215, 241, 60, ARG3N.getPreferredSize().height);

              //---- ARG6N ----
              ARG6N.setComponentPopupMenu(BTD);
              ARG6N.setName("ARG6N");
              panel1.add(ARG6N);
              ARG6N.setBounds(270, 139, 52, ARG6N.getPreferredSize().height);

              //---- ARG14A ----
              ARG14A.setComponentPopupMenu(BTD2);
              ARG14A.setName("ARG14A");
              panel1.add(ARG14A);
              ARG14A.setBounds(215, 377, 35, ARG14A.getPreferredSize().height);

              //---- ARG7A ----
              ARG7A.setComponentPopupMenu(BTD);
              ARG7A.setName("ARG7A");
              panel1.add(ARG7A);
              ARG7A.setBounds(215, 139, 36, ARG7A.getPreferredSize().height);

              //---- ARG8A ----
              ARG8A.setComponentPopupMenu(BTD2);
              ARG8A.setName("ARG8A");
              panel1.add(ARG8A);
              ARG8A.setBounds(216, 207, 30, ARG8A.getPreferredSize().height);

              //---- ARG9A ----
              ARG9A.setComponentPopupMenu(BTD);
              ARG9A.setName("ARG9A");
              panel1.add(ARG9A);
              ARG9A.setBounds(215, 275, 30, ARG9A.getPreferredSize().height);

              //---- ARG12A ----
              ARG12A.setComponentPopupMenu(BTD);
              ARG12A.setName("ARG12A");
              panel1.add(ARG12A);
              ARG12A.setBounds(215, 343, 30, ARG12A.getPreferredSize().height);

              //---- ARG13A ----
              ARG13A.setComponentPopupMenu(BTD);
              ARG13A.setName("ARG13A");
              panel1.add(ARG13A);
              ARG13A.setBounds(269, 343, 30, ARG13A.getPreferredSize().height);

              //---- OBJ_105 ----
              OBJ_105.setText("/");
              OBJ_105.setName("OBJ_105");
              panel1.add(OBJ_105);
              OBJ_105.setBounds(260, 139, 10, 28);

              //---- OBJ_112 ----
              OBJ_112.setText("/");
              OBJ_112.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_112.setName("OBJ_112");
              panel1.add(OBJ_112);
              OBJ_112.setBounds(252, 343, 10, 28);

              //---- WCSNDX ----
              WCSNDX.setText("@WCSNDX@");
              WCSNDX.setName("WCSNDX");
              panel1.add(WCSNDX);
              WCSNDX.setBounds(380, 107, 100, WCSNDX.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }

            //======== panel3 ========
            {
              panel3.setBackground(new Color(214, 217, 223));
              panel3.setBorder(new TitledBorder(null, "Solde", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- WSOLDE ----
              WSOLDE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WSOLDE.setName("WSOLDE");
              panel3.add(WSOLDE);
              WSOLDE.setBounds(180, 32, 204, WSOLDE.getPreferredSize().height);

              //---- SOLD1 ----
              SOLD1.setText("Affichage du solde");
              SOLD1.setComponentPopupMenu(BTD);
              SOLD1.setName("SOLD1");
              panel3.add(SOLD1);
              SOLD1.setBounds(30, 35, 145, 20);

              //---- OBJ_109 ----
              OBJ_109.setText("Solde compris entre");
              OBJ_109.setName("OBJ_109");
              panel3.add(OBJ_109);
              OBJ_109.setBounds(30, 70, 145, 20);

              //---- WSDEB ----
              WSDEB.setComponentPopupMenu(BTD);
              WSDEB.setName("WSDEB");
              panel3.add(WSDEB);
              WSDEB.setBounds(180, 66, 96, WSDEB.getPreferredSize().height);

              //---- WSFIN ----
              WSFIN.setComponentPopupMenu(BTD);
              WSFIN.setName("WSFIN");
              panel3.add(WSFIN);
              WSFIN.setBounds(180, 101, 96, WSFIN.getPreferredSize().height);

              //---- OBJ_110 ----
              OBJ_110.setText("et");
              OBJ_110.setName("OBJ_110");
              panel3.add(OBJ_110);
              OBJ_110.setBounds(30, 105, 145, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- TIDX20 ----
              TIDX20.setText("Firme");
              TIDX20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX20.setName("TIDX20");
              panel2.add(TIDX20);
              TIDX20.setBounds(25, 19, 90, TIDX20.getPreferredSize().height);

              //---- TIDX21 ----
              TIDX21.setText("Centrale");
              TIDX21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX21.setName("TIDX21");
              panel2.add(TIDX21);
              TIDX21.setBounds(25, 54, 105, TIDX21.getPreferredSize().height);

              //---- TIDX23 ----
              TIDX23.setText("Contact");
              TIDX23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX23.setName("TIDX23");
              panel2.add(TIDX23);
              TIDX23.setBounds(25, 89, 100, 20);

              //---- ARG22A ----
              ARG22A.setComponentPopupMenu(BTD);
              ARG22A.setName("ARG22A");
              panel2.add(ARG22A);
              ARG22A.setBounds(180, 85, 140, ARG22A.getPreferredSize().height);

              //---- ARG21N ----
              ARG21N.setComponentPopupMenu(BTD);
              ARG21N.setName("ARG21N");
              panel2.add(ARG21N);
              ARG21N.setBounds(180, 49, 70, ARG21N.getPreferredSize().height);

              //---- ARG20A ----
              ARG20A.setComponentPopupMenu(BTD2);
              ARG20A.setName("ARG20A");
              panel2.add(ARG20A);
              ARG20A.setBounds(180, 14, 60, ARG20A.getPreferredSize().height);

              //---- OBJ_94 ----
              OBJ_94.setText("");
              OBJ_94.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_94.setName("OBJ_94");
              panel2.add(OBJ_94);
              OBJ_94.setBounds(325, 89, 25, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- TIDX30 ----
              TIDX30.setText("Date de cr\u00e9ation");
              TIDX30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX30.setName("TIDX30");
              panel4.add(TIDX30);
              TIDX30.setBounds(26, 20, 140, 20);

              //---- ARG30D ----
              ARG30D.setComponentPopupMenu(BTD);
              ARG30D.setName("ARG30D");
              panel4.add(ARG30D);
              ARG30D.setBounds(180, 15, 105, ARG30D.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }

            //======== panel5 ========
            {
              panel5.setBorder(new TitledBorder(null, "S\u00e9lection d'\u00e9tat", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- SCAN23 ----
              SCAN23.setText("Aucune");
              SCAN23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN23.setName("SCAN23");
              SCAN23.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  SCAN23StateChanged(e);
                }
              });
              panel5.add(SCAN23);
              SCAN23.setBounds(26, 30, 74, 20);

              //---- SCAN23_B ----
              SCAN23_B.setText("Interne");
              SCAN23_B.setName("SCAN23_B");
              panel5.add(SCAN23_B);
              SCAN23_B.setBounds(105, 31, 70, 20);

              //---- SCAN23_C ----
              SCAN23_C.setText("Exclus");
              SCAN23_C.setToolTipText("Exclusion de l'\u00e9tat s\u00e9lectionn\u00e9");
              SCAN23_C.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN23_C.setName("SCAN23_C");
              panel5.add(SCAN23_C);
              SCAN23_C.setBounds(180, 30, 67, 20);

              //---- ARG23N ----
              ARG23N.setToolTipText("Etat du compte");
              ARG23N.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG23N.setName("ARG23N");
              panel5.add(ARG23N);
              ARG23N.setBounds(26, 57, 210, ARG23N.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }

            GroupLayout panel7Layout = new GroupLayout(panel7);
            panel7.setLayout(panel7Layout);
            panel7Layout.setHorizontalGroup(
              panel7Layout.createParallelGroup()
                .addGroup(panel7Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 492, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addGroup(panel7Layout.createParallelGroup()
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)))
            );
            panel7Layout.setVerticalGroup(
              panel7Layout.createParallelGroup()
                .addGroup(panel7Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel7Layout.createParallelGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 480, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel7Layout.createSequentialGroup()
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_20);
    }

    //---- RBC_GRP ----
    RBC_GRP.add(TIDX12);
    RBC_GRP.add(TIDX9);
    RBC_GRP.add(TIDX11);
    RBC_GRP.add(TIDX6);
    RBC_GRP.add(TIDX4);
    RBC_GRP.add(TIDX5);
    RBC_GRP.add(TIDX15);
    RBC_GRP.add(TIDX3);
    RBC_GRP.add(TIDX8);
    RBC_GRP.add(TIDX14);
    RBC_GRP.add(TIDX20);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX23);
    RBC_GRP.add(TIDX30);

    //---- SCAN23_GRP ----
    SCAN23_GRP.add(SCAN23);
    SCAN23_GRP.add(SCAN23_B);
    SCAN23_GRP.add(SCAN23_C);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40;
  private XRiTextField INDSOC;
  private JLabel OBJ_42;
  private XRiTextField INDNCG;
  private JLabel OBJ_44;
  private XRiTextField INDNCA;
  private JLabel l_DATPR;
  private XRiTextField DATPR;
  private JPanel p_tete_droite;
  private JMenuBar barre_tete2;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_41;
  private JLabel OBJ_43;
  private JLabel OBJ_45;
  private XRiTextField WISOC3;
  private XRiTextField WINCG3;
  private XRiTextField WINCA3;
  private JPanel p_tete_droite2;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel7;
  private JPanel panel1;
  private XRiRadioButton TIDX12;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX11;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG4A;
  private XRiTextField ARG5A;
  private XRiTextField ARG33A;
  private JLabel OBJ_103;
  private XRiRadioButton TIDX15;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG32A;
  private JLabel OBJ_107;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX14;
  private XRiTextField ARG11A;
  private XRiTextField ARG10A;
  private XRiTextField ARG15A;
  private XRiTextField ARG16A;
  private XRiTextField ARG17A;
  private XRiTextField ARG18A;
  private XRiTextField ARG19A;
  private XRiTextField ARG3N;
  private XRiTextField ARG6N;
  private XRiTextField ARG14A;
  private XRiTextField ARG7A;
  private XRiTextField ARG8A;
  private XRiTextField ARG9A;
  private XRiTextField ARG12A;
  private XRiTextField ARG13A;
  private JLabel OBJ_105;
  private JLabel OBJ_112;
  private RiZoneSortie WCSNDX;
  private JPanel panel3;
  private XRiComboBox WSOLDE;
  private JLabel SOLD1;
  private JLabel OBJ_109;
  private XRiTextField WSDEB;
  private XRiTextField WSFIN;
  private JLabel OBJ_110;
  private JPanel panel2;
  private XRiRadioButton TIDX20;
  private XRiRadioButton TIDX21;
  private XRiRadioButton TIDX23;
  private XRiTextField ARG22A;
  private XRiTextField ARG21N;
  private XRiTextField ARG20A;
  private JCheckBox OBJ_94;
  private JPanel panel4;
  private XRiRadioButton TIDX30;
  private XRiCalendrier ARG30D;
  private JPanel panel5;
  private XRiRadioButton SCAN23;
  private XRiRadioButton SCAN23_B;
  private XRiRadioButton SCAN23_C;
  private XRiComboBox ARG23N;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private ButtonGroup RBC_GRP;
  private ButtonGroup SCAN23_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
