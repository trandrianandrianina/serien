
package ri.serien.libecranrpg.sgtm.SGTM74FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGTM74FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public ODialog dialog_JO = null;
  
  public SGTM74FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void btCodeJournalActionPerformed(ActionEvent e) {
    lexique.addVariableGlobale("CODE_SOC", z_etablissement_.getText());
    lexique.addVariableGlobale("ZONE_JO", "CODJO");
    if (dialog_JO == null) {
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this, false));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new SNPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    btCodeJournal = new JButton();
    CODJO = new XRiTextField();
    WLIB = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlSociete = new SNPanelTitre();
    z_etablissement_ = new XRiTextField();
    DGNOM = new XRiTextField();
    lbPeriodeEnCours = new SNLabelChamp();
    lbEtablissement = new SNLabelChamp();
    DGPERX = new XRiTextField();
    MESCLO = new XRiTextField();
    bouton_etablissement = new SNBoutonRecherche();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      pnlBandeau.add(p_bpresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8re de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- btCodeJournal ----
          btCodeJournal.setText("Journal");
          btCodeJournal.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btCodeJournal.setToolTipText("Recherche d'un journal");
          btCodeJournal.setFont(new Font("sansserif", Font.PLAIN, 14));
          btCodeJournal.setMaximumSize(new Dimension(75, 30));
          btCodeJournal.setMinimumSize(new Dimension(75, 30));
          btCodeJournal.setPreferredSize(new Dimension(75, 30));
          btCodeJournal.setName("btCodeJournal");
          btCodeJournal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btCodeJournalActionPerformed(e);
            }
          });
          pnlCritereSelection.add(btCodeJournal, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CODJO ----
          CODJO.setMaximumSize(new Dimension(36, 30));
          CODJO.setMinimumSize(new Dimension(36, 30));
          CODJO.setPreferredSize(new Dimension(36, 30));
          CODJO.setFont(new Font("sansserif", Font.PLAIN, 14));
          CODJO.setName("CODJO");
          pnlCritereSelection.add(CODJO, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WLIB ----
          WLIB.setMaximumSize(new Dimension(440, 30));
          WLIB.setMinimumSize(new Dimension(440, 30));
          WLIB.setPreferredSize(new Dimension(440, 30));
          WLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
          WLIB.setName("WLIB");
          pnlCritereSelection.add(WLIB, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlSociete ========
        {
          pnlSociete.setTitre("Soci\u00e9t\u00e9");
          pnlSociete.setName("pnlSociete");
          pnlSociete.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlSociete.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlSociete.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlSociete.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlSociete.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("WSOC");
          z_etablissement_.setMaximumSize(new Dimension(48, 30));
          z_etablissement_.setMinimumSize(new Dimension(48, 30));
          z_etablissement_.setPreferredSize(new Dimension(48, 30));
          z_etablissement_.setName("z_etablissement_");
          pnlSociete.add(z_etablissement_, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- DGNOM ----
          DGNOM.setMaximumSize(new Dimension(320, 30));
          DGNOM.setMinimumSize(new Dimension(320, 30));
          DGNOM.setPreferredSize(new Dimension(320, 30));
          DGNOM.setFont(new Font("sansserif", Font.PLAIN, 14));
          DGNOM.setName("DGNOM");
          pnlSociete.add(DGNOM, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode");
          lbPeriodeEnCours.setMaximumSize(new Dimension(110, 30));
          lbPeriodeEnCours.setMinimumSize(new Dimension(110, 30));
          lbPeriodeEnCours.setPreferredSize(new Dimension(110, 30));
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlSociete.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbEtablissement ----
          lbEtablissement.setText("Code soci\u00e9t\u00e9");
          lbEtablissement.setMaximumSize(new Dimension(110, 30));
          lbEtablissement.setMinimumSize(new Dimension(110, 30));
          lbEtablissement.setPreferredSize(new Dimension(110, 30));
          lbEtablissement.setName("lbEtablissement");
          pnlSociete.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- DGPERX ----
          DGPERX.setMaximumSize(new Dimension(72, 30));
          DGPERX.setMinimumSize(new Dimension(72, 30));
          DGPERX.setPreferredSize(new Dimension(72, 30));
          DGPERX.setFont(new Font("sansserif", Font.PLAIN, 14));
          DGPERX.setName("DGPERX");
          pnlSociete.add(DGPERX, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- MESCLO ----
          MESCLO.setMaximumSize(new Dimension(230, 30));
          MESCLO.setMinimumSize(new Dimension(230, 30));
          MESCLO.setPreferredSize(new Dimension(230, 30));
          MESCLO.setFont(new Font("sansserif", Font.PLAIN, 14));
          MESCLO.setName("MESCLO");
          pnlSociete.add(MESCLO, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          pnlSociete.add(bouton_etablissement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        }
        pnlDroite.add(pnlSociete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlBandeau;
  private SNBandeauTitre p_bpresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private JButton btCodeJournal;
  private XRiTextField CODJO;
  private XRiTextField WLIB;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlSociete;
  private XRiTextField z_etablissement_;
  private XRiTextField DGNOM;
  private SNLabelChamp lbPeriodeEnCours;
  private SNLabelChamp lbEtablissement;
  private XRiTextField DGPERX;
  private XRiTextField MESCLO;
  private SNBoutonRecherche bouton_etablissement;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
