
package ri.serien.libecranrpg.vprm.VPRM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VPRM11FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VPRM11FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    PRNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRNOM@")).trim());
    WZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP1@")).trim());
    WZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP2@")).trim());
    WZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP3@")).trim());
    WZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP4@")).trim());
    WZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP5@")).trim());
    WZP6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP6@")).trim());
    WZP7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP7@")).trim());
    WZP8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP8@")).trim());
    WZP9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP9@")).trim());
    WZP10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP10@")).trim());
    WZP11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP11@")).trim());
    WZP12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP12@")).trim());
    WZP13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP13@")).trim());
    WZP14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP14@")).trim());
    WZP15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP15@")).trim());
    WZP16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP16@")).trim());
    WZP17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP17@")).trim());
    WZP18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP18@")).trim());
    ERZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    ERZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP2@")).trim());
    ERZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP3@")).trim());
    ERZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP4@")).trim());
    ERZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP5@")).trim());
    ERZP6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP6@")).trim());
    ERZP7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP7@")).trim());
    ERZP8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP8@")).trim());
    ERZP9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP9@")).trim());
    ERZP10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP10@")).trim());
    ERZP11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP11@")).trim());
    ERZP12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP12@")).trim());
    ERZP13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP13@")).trim());
    ERZP14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP14@")).trim());
    ERZP15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP15@")).trim());
    ERZP16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP16@")).trim());
    ERZP17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP17@")).trim());
    ERZP18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP18@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_64 = new JLabel();
    PRPRO = new JTextField();
    PRLIV = new JTextField();
    PRNOM = new RiZoneSortie();
    OBJ_24 = new JPanel();
    EPZP1 = new XRiTextField();
    WZP1 = new JLabel();
    WZP2 = new JLabel();
    WZP3 = new JLabel();
    WZP4 = new JLabel();
    WZP5 = new JLabel();
    WZP6 = new JLabel();
    WZP7 = new JLabel();
    WZP8 = new JLabel();
    WZP9 = new JLabel();
    WZP10 = new JLabel();
    WZP11 = new JLabel();
    WZP12 = new JLabel();
    WZP13 = new JLabel();
    WZP14 = new JLabel();
    WZP15 = new JLabel();
    WZP16 = new JLabel();
    WZP17 = new JLabel();
    WZP18 = new JLabel();
    EPZP2 = new XRiTextField();
    EPZP3 = new XRiTextField();
    EPZP4 = new XRiTextField();
    EPZP5 = new XRiTextField();
    EPZP6 = new XRiTextField();
    EPZP7 = new XRiTextField();
    EPZP8 = new XRiTextField();
    EPZP9 = new XRiTextField();
    EPZP10 = new XRiTextField();
    EPZP11 = new XRiTextField();
    EPZP12 = new XRiTextField();
    EPZP13 = new XRiTextField();
    EPZP14 = new XRiTextField();
    EPZP15 = new XRiTextField();
    EPZP16 = new XRiTextField();
    EPZP17 = new XRiTextField();
    EPZP18 = new XRiTextField();
    ERZP1 = new RiZoneSortie();
    ERZP2 = new RiZoneSortie();
    ERZP3 = new RiZoneSortie();
    ERZP4 = new RiZoneSortie();
    ERZP5 = new RiZoneSortie();
    ERZP6 = new RiZoneSortie();
    ERZP7 = new RiZoneSortie();
    ERZP8 = new RiZoneSortie();
    ERZP9 = new RiZoneSortie();
    ERZP10 = new RiZoneSortie();
    ERZP11 = new RiZoneSortie();
    ERZP12 = new RiZoneSortie();
    ERZP13 = new RiZoneSortie();
    ERZP14 = new RiZoneSortie();
    ERZP15 = new RiZoneSortie();
    ERZP16 = new RiZoneSortie();
    ERZP17 = new RiZoneSortie();
    ERZP18 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(920, 620));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_64 ----
          OBJ_64.setText("Num\u00e9ro");
          OBJ_64.setName("OBJ_64");
          panel1.add(OBJ_64);
          OBJ_64.setBounds(20, 12, 70, 25);

          //---- PRPRO ----
          PRPRO.setName("PRPRO");
          panel1.add(PRPRO);
          PRPRO.setBounds(100, 10, 60, PRPRO.getPreferredSize().height);

          //---- PRLIV ----
          PRLIV.setName("PRLIV");
          panel1.add(PRLIV);
          PRLIV.setBounds(160, 10, 40, PRLIV.getPreferredSize().height);

          //---- PRNOM ----
          PRNOM.setText("@PRNOM@");
          PRNOM.setName("PRNOM");
          panel1.add(PRNOM);
          PRNOM.setBounds(235, 13, 310, 23);

          //======== OBJ_24 ========
          {
            OBJ_24.setOpaque(false);
            OBJ_24.setName("OBJ_24");
            OBJ_24.setLayout(null);

            //---- EPZP1 ----
            EPZP1.setComponentPopupMenu(BTD);
            EPZP1.setName("EPZP1");
            OBJ_24.add(EPZP1);
            EPZP1.setBounds(222, 5, 310, EPZP1.getPreferredSize().height);

            //---- WZP1 ----
            WZP1.setText("@WZP1@");
            WZP1.setName("WZP1");
            OBJ_24.add(WZP1);
            WZP1.setBounds(5, 4, 209, 28);

            //---- WZP2 ----
            WZP2.setText("@WZP2@");
            WZP2.setName("WZP2");
            OBJ_24.add(WZP2);
            WZP2.setBounds(5, 33, 209, 28);

            //---- WZP3 ----
            WZP3.setText("@WZP3@");
            WZP3.setName("WZP3");
            OBJ_24.add(WZP3);
            WZP3.setBounds(5, 62, 209, 28);

            //---- WZP4 ----
            WZP4.setText("@WZP4@");
            WZP4.setName("WZP4");
            OBJ_24.add(WZP4);
            WZP4.setBounds(5, 91, 209, 28);

            //---- WZP5 ----
            WZP5.setText("@WZP5@");
            WZP5.setName("WZP5");
            OBJ_24.add(WZP5);
            WZP5.setBounds(5, 120, 209, 28);

            //---- WZP6 ----
            WZP6.setText("@WZP6@");
            WZP6.setName("WZP6");
            OBJ_24.add(WZP6);
            WZP6.setBounds(5, 149, 209, 28);

            //---- WZP7 ----
            WZP7.setText("@WZP7@");
            WZP7.setName("WZP7");
            OBJ_24.add(WZP7);
            WZP7.setBounds(5, 178, 209, 28);

            //---- WZP8 ----
            WZP8.setText("@WZP8@");
            WZP8.setName("WZP8");
            OBJ_24.add(WZP8);
            WZP8.setBounds(5, 207, 209, 28);

            //---- WZP9 ----
            WZP9.setText("@WZP9@");
            WZP9.setName("WZP9");
            OBJ_24.add(WZP9);
            WZP9.setBounds(5, 236, 209, 28);

            //---- WZP10 ----
            WZP10.setText("@WZP10@");
            WZP10.setName("WZP10");
            OBJ_24.add(WZP10);
            WZP10.setBounds(5, 265, 209, 28);

            //---- WZP11 ----
            WZP11.setText("@WZP11@");
            WZP11.setName("WZP11");
            OBJ_24.add(WZP11);
            WZP11.setBounds(5, 294, 209, 28);

            //---- WZP12 ----
            WZP12.setText("@WZP12@");
            WZP12.setName("WZP12");
            OBJ_24.add(WZP12);
            WZP12.setBounds(5, 323, 209, 28);

            //---- WZP13 ----
            WZP13.setText("@WZP13@");
            WZP13.setName("WZP13");
            OBJ_24.add(WZP13);
            WZP13.setBounds(5, 352, 209, 28);

            //---- WZP14 ----
            WZP14.setText("@WZP14@");
            WZP14.setName("WZP14");
            OBJ_24.add(WZP14);
            WZP14.setBounds(5, 381, 209, 28);

            //---- WZP15 ----
            WZP15.setText("@WZP15@");
            WZP15.setName("WZP15");
            OBJ_24.add(WZP15);
            WZP15.setBounds(5, 410, 209, 28);

            //---- WZP16 ----
            WZP16.setText("@WZP16@");
            WZP16.setName("WZP16");
            OBJ_24.add(WZP16);
            WZP16.setBounds(5, 439, 209, 28);

            //---- WZP17 ----
            WZP17.setText("@WZP17@");
            WZP17.setName("WZP17");
            OBJ_24.add(WZP17);
            WZP17.setBounds(5, 468, 209, 28);

            //---- WZP18 ----
            WZP18.setText("@WZP18@");
            WZP18.setName("WZP18");
            OBJ_24.add(WZP18);
            WZP18.setBounds(5, 497, 209, 28);

            //---- EPZP2 ----
            EPZP2.setComponentPopupMenu(BTD);
            EPZP2.setName("EPZP2");
            OBJ_24.add(EPZP2);
            EPZP2.setBounds(222, 34, 310, EPZP2.getPreferredSize().height);

            //---- EPZP3 ----
            EPZP3.setComponentPopupMenu(BTD);
            EPZP3.setName("EPZP3");
            OBJ_24.add(EPZP3);
            EPZP3.setBounds(222, 63, 310, EPZP3.getPreferredSize().height);

            //---- EPZP4 ----
            EPZP4.setComponentPopupMenu(BTD);
            EPZP4.setName("EPZP4");
            OBJ_24.add(EPZP4);
            EPZP4.setBounds(222, 92, 310, EPZP4.getPreferredSize().height);

            //---- EPZP5 ----
            EPZP5.setComponentPopupMenu(BTD);
            EPZP5.setName("EPZP5");
            OBJ_24.add(EPZP5);
            EPZP5.setBounds(222, 121, 310, EPZP5.getPreferredSize().height);

            //---- EPZP6 ----
            EPZP6.setComponentPopupMenu(BTD);
            EPZP6.setName("EPZP6");
            OBJ_24.add(EPZP6);
            EPZP6.setBounds(222, 150, 310, EPZP6.getPreferredSize().height);

            //---- EPZP7 ----
            EPZP7.setComponentPopupMenu(BTD);
            EPZP7.setName("EPZP7");
            OBJ_24.add(EPZP7);
            EPZP7.setBounds(222, 179, 310, EPZP7.getPreferredSize().height);

            //---- EPZP8 ----
            EPZP8.setComponentPopupMenu(BTD);
            EPZP8.setName("EPZP8");
            OBJ_24.add(EPZP8);
            EPZP8.setBounds(222, 208, 310, EPZP8.getPreferredSize().height);

            //---- EPZP9 ----
            EPZP9.setComponentPopupMenu(BTD);
            EPZP9.setName("EPZP9");
            OBJ_24.add(EPZP9);
            EPZP9.setBounds(222, 237, 310, EPZP9.getPreferredSize().height);

            //---- EPZP10 ----
            EPZP10.setComponentPopupMenu(BTD);
            EPZP10.setName("EPZP10");
            OBJ_24.add(EPZP10);
            EPZP10.setBounds(222, 266, 310, EPZP10.getPreferredSize().height);

            //---- EPZP11 ----
            EPZP11.setComponentPopupMenu(BTD);
            EPZP11.setName("EPZP11");
            OBJ_24.add(EPZP11);
            EPZP11.setBounds(222, 295, 310, EPZP11.getPreferredSize().height);

            //---- EPZP12 ----
            EPZP12.setComponentPopupMenu(BTD);
            EPZP12.setName("EPZP12");
            OBJ_24.add(EPZP12);
            EPZP12.setBounds(222, 324, 310, EPZP12.getPreferredSize().height);

            //---- EPZP13 ----
            EPZP13.setComponentPopupMenu(BTD);
            EPZP13.setName("EPZP13");
            OBJ_24.add(EPZP13);
            EPZP13.setBounds(222, 353, 310, EPZP13.getPreferredSize().height);

            //---- EPZP14 ----
            EPZP14.setComponentPopupMenu(BTD);
            EPZP14.setName("EPZP14");
            OBJ_24.add(EPZP14);
            EPZP14.setBounds(222, 382, 310, EPZP14.getPreferredSize().height);

            //---- EPZP15 ----
            EPZP15.setComponentPopupMenu(BTD);
            EPZP15.setName("EPZP15");
            OBJ_24.add(EPZP15);
            EPZP15.setBounds(222, 411, 310, EPZP15.getPreferredSize().height);

            //---- EPZP16 ----
            EPZP16.setComponentPopupMenu(BTD);
            EPZP16.setName("EPZP16");
            OBJ_24.add(EPZP16);
            EPZP16.setBounds(222, 440, 310, EPZP16.getPreferredSize().height);

            //---- EPZP17 ----
            EPZP17.setComponentPopupMenu(BTD);
            EPZP17.setName("EPZP17");
            OBJ_24.add(EPZP17);
            EPZP17.setBounds(222, 469, 310, EPZP17.getPreferredSize().height);

            //---- EPZP18 ----
            EPZP18.setComponentPopupMenu(BTD);
            EPZP18.setName("EPZP18");
            OBJ_24.add(EPZP18);
            EPZP18.setBounds(222, 498, 310, EPZP18.getPreferredSize().height);

            //---- ERZP1 ----
            ERZP1.setText("@ERZP1@");
            ERZP1.setName("ERZP1");
            OBJ_24.add(ERZP1);
            ERZP1.setBounds(545, 7, 150, ERZP1.getPreferredSize().height);

            //---- ERZP2 ----
            ERZP2.setText("@ERZP2@");
            ERZP2.setName("ERZP2");
            OBJ_24.add(ERZP2);
            ERZP2.setBounds(545, 36, 150, ERZP2.getPreferredSize().height);

            //---- ERZP3 ----
            ERZP3.setText("@ERZP3@");
            ERZP3.setName("ERZP3");
            OBJ_24.add(ERZP3);
            ERZP3.setBounds(545, 65, 150, ERZP3.getPreferredSize().height);

            //---- ERZP4 ----
            ERZP4.setText("@ERZP4@");
            ERZP4.setName("ERZP4");
            OBJ_24.add(ERZP4);
            ERZP4.setBounds(545, 94, 150, ERZP4.getPreferredSize().height);

            //---- ERZP5 ----
            ERZP5.setText("@ERZP5@");
            ERZP5.setName("ERZP5");
            OBJ_24.add(ERZP5);
            ERZP5.setBounds(545, 123, 150, ERZP5.getPreferredSize().height);

            //---- ERZP6 ----
            ERZP6.setText("@ERZP6@");
            ERZP6.setName("ERZP6");
            OBJ_24.add(ERZP6);
            ERZP6.setBounds(545, 152, 150, ERZP6.getPreferredSize().height);

            //---- ERZP7 ----
            ERZP7.setText("@ERZP7@");
            ERZP7.setName("ERZP7");
            OBJ_24.add(ERZP7);
            ERZP7.setBounds(545, 181, 150, ERZP7.getPreferredSize().height);

            //---- ERZP8 ----
            ERZP8.setText("@ERZP8@");
            ERZP8.setName("ERZP8");
            OBJ_24.add(ERZP8);
            ERZP8.setBounds(545, 210, 150, ERZP8.getPreferredSize().height);

            //---- ERZP9 ----
            ERZP9.setText("@ERZP9@");
            ERZP9.setName("ERZP9");
            OBJ_24.add(ERZP9);
            ERZP9.setBounds(545, 239, 150, ERZP9.getPreferredSize().height);

            //---- ERZP10 ----
            ERZP10.setText("@ERZP10@");
            ERZP10.setName("ERZP10");
            OBJ_24.add(ERZP10);
            ERZP10.setBounds(545, 268, 150, ERZP10.getPreferredSize().height);

            //---- ERZP11 ----
            ERZP11.setText("@ERZP11@");
            ERZP11.setName("ERZP11");
            OBJ_24.add(ERZP11);
            ERZP11.setBounds(545, 297, 150, ERZP11.getPreferredSize().height);

            //---- ERZP12 ----
            ERZP12.setText("@ERZP12@");
            ERZP12.setName("ERZP12");
            OBJ_24.add(ERZP12);
            ERZP12.setBounds(545, 326, 150, ERZP12.getPreferredSize().height);

            //---- ERZP13 ----
            ERZP13.setText("@ERZP13@");
            ERZP13.setName("ERZP13");
            OBJ_24.add(ERZP13);
            ERZP13.setBounds(545, 355, 150, ERZP13.getPreferredSize().height);

            //---- ERZP14 ----
            ERZP14.setText("@ERZP14@");
            ERZP14.setName("ERZP14");
            OBJ_24.add(ERZP14);
            ERZP14.setBounds(545, 384, 150, ERZP14.getPreferredSize().height);

            //---- ERZP15 ----
            ERZP15.setText("@ERZP15@");
            ERZP15.setName("ERZP15");
            OBJ_24.add(ERZP15);
            ERZP15.setBounds(545, 413, 150, ERZP15.getPreferredSize().height);

            //---- ERZP16 ----
            ERZP16.setText("@ERZP16@");
            ERZP16.setName("ERZP16");
            OBJ_24.add(ERZP16);
            ERZP16.setBounds(545, 442, 150, ERZP16.getPreferredSize().height);

            //---- ERZP17 ----
            ERZP17.setText("@ERZP17@");
            ERZP17.setName("ERZP17");
            OBJ_24.add(ERZP17);
            ERZP17.setBounds(545, 471, 150, ERZP17.getPreferredSize().height);

            //---- ERZP18 ----
            ERZP18.setText("@ERZP18@");
            ERZP18.setName("ERZP18");
            OBJ_24.add(ERZP18);
            ERZP18.setBounds(545, 500, 150, ERZP18.getPreferredSize().height);
          }
          panel1.add(OBJ_24);
          OBJ_24.setBounds(10, 50, 715, 540);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 735, 605);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_64;
  private JTextField PRPRO;
  private JTextField PRLIV;
  private RiZoneSortie PRNOM;
  private JPanel OBJ_24;
  private XRiTextField EPZP1;
  private JLabel WZP1;
  private JLabel WZP2;
  private JLabel WZP3;
  private JLabel WZP4;
  private JLabel WZP5;
  private JLabel WZP6;
  private JLabel WZP7;
  private JLabel WZP8;
  private JLabel WZP9;
  private JLabel WZP10;
  private JLabel WZP11;
  private JLabel WZP12;
  private JLabel WZP13;
  private JLabel WZP14;
  private JLabel WZP15;
  private JLabel WZP16;
  private JLabel WZP17;
  private JLabel WZP18;
  private XRiTextField EPZP2;
  private XRiTextField EPZP3;
  private XRiTextField EPZP4;
  private XRiTextField EPZP5;
  private XRiTextField EPZP6;
  private XRiTextField EPZP7;
  private XRiTextField EPZP8;
  private XRiTextField EPZP9;
  private XRiTextField EPZP10;
  private XRiTextField EPZP11;
  private XRiTextField EPZP12;
  private XRiTextField EPZP13;
  private XRiTextField EPZP14;
  private XRiTextField EPZP15;
  private XRiTextField EPZP16;
  private XRiTextField EPZP17;
  private XRiTextField EPZP18;
  private RiZoneSortie ERZP1;
  private RiZoneSortie ERZP2;
  private RiZoneSortie ERZP3;
  private RiZoneSortie ERZP4;
  private RiZoneSortie ERZP5;
  private RiZoneSortie ERZP6;
  private RiZoneSortie ERZP7;
  private RiZoneSortie ERZP8;
  private RiZoneSortie ERZP9;
  private RiZoneSortie ERZP10;
  private RiZoneSortie ERZP11;
  private RiZoneSortie ERZP12;
  private RiZoneSortie ERZP13;
  private RiZoneSortie ERZP14;
  private RiZoneSortie ERZP15;
  private RiZoneSortie ERZP16;
  private RiZoneSortie ERZP17;
  private RiZoneSortie ERZP18;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
