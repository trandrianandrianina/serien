
package ri.serien.libecranrpg.vprm.VPRM04FM;
// Nom Fichier: b_none_0_VPRM04FM_FMTB0_FMTA2_FMTF1_12.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VPRM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  @SuppressWarnings("unchecked")
  public VPRM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DOPP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOPP1@")).trim());
    AFOBS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AFOBS@")).trim());
    LIBGF1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGF1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    panel2.setRightDecoration(TCI1);
    panel3.setRightDecoration(TCI2);
    panel4.setRightDecoration(TCI3);
    panel5.setRightDecoration(TCI4);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affaire commerciale"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void TCI1ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI1"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    AFNUM = new XRiTextField();
    panel2 = new JXTitledPanel();
    OBJ_76 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    WDDAX = new XRiCalendrier();
    WDPAX = new XRiCalendrier();
    AFEXE = new XRiTextField();
    WTDA = new XRiTextField();
    WTPA = new XRiTextField();
    panel5 = new JXTitledPanel();
    DOPP1 = new RiZoneSortie();
    AFME1 = new XRiTextField();
    AFPO1 = new XRiTextField();
    OBJ_83 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_85 = new JLabel();
    panel4 = new JXTitledPanel();
    AFOBS = new RiZoneSortie();
    panel3 = new JXTitledPanel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    LIBGF1 = new RiZoneSortie();
    AFAR1 = new XRiTextField();
    OBJ_72 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_78 = new JLabel();
    xRiCheckBox1 = new XRiCheckBox();
    AFLIB = new XRiTextField();
    RETEL = new XRiTextField();
    REPAC = new XRiTextField();
    AFDTDX = new XRiCalendrier();
    AFDTFX = new XRiCalendrier();
    AFOBJ = new XRiTextField();
    AFLIB2 = new XRiTextField();
    AFREP = new XRiTextField();
    RPLIB1 = new XRiTextField();
    AFETA = new XRiTextField();
    LIBETA = new XRiTextField();
    TCI1 = new SNBoutonDetail();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI4 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1060, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Autres vues");
            riSousMenu_bt6.setToolTipText("Autres vues");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Affaires commerciales");
            riSousMenu_bt7.setToolTipText("Affichage ou non des affaires commerciales d\u00e9sactiv\u00e9es");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Fonctions sur fiches");
            riSousMenu_bt8.setToolTipText("Fonctions sur fiches");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- AFNUM ----
          AFNUM.setName("AFNUM");
          panel1.add(AFNUM);
          AFNUM.setBounds(100, 15, 76, AFNUM.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new DropShadowBorder());
            panel2.setTitle("Actions");
            panel2.setName("panel2");
            Container panel2ContentContainer = panel2.getContentContainer();
            panel2ContentContainer.setLayout(null);

            //---- OBJ_76 ----
            OBJ_76.setText("Derni\u00e8re");
            OBJ_76.setName("OBJ_76");
            panel2ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(15, 10, 135, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("Prochaine");
            OBJ_79.setName("OBJ_79");
            panel2ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(15, 39, 135, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("Exclusion actions");
            OBJ_80.setName("OBJ_80");
            panel2ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(15, 68, 135, 20);

            //---- WDDAX ----
            WDDAX.setName("WDDAX");
            panel2ContentContainer.add(WDDAX);
            WDDAX.setBounds(165, 6, 105, WDDAX.getPreferredSize().height);

            //---- WDPAX ----
            WDPAX.setName("WDPAX");
            panel2ContentContainer.add(WDPAX);
            WDPAX.setBounds(165, 35, 105, WDPAX.getPreferredSize().height);

            //---- AFEXE ----
            AFEXE.setName("AFEXE");
            panel2ContentContainer.add(AFEXE);
            AFEXE.setBounds(165, 64, 24, AFEXE.getPreferredSize().height);

            //---- WTDA ----
            WTDA.setName("WTDA");
            panel2ContentContainer.add(WTDA);
            WTDA.setBounds(285, 6, 44, WTDA.getPreferredSize().height);

            //---- WTPA ----
            WTPA.setName("WTPA");
            panel2ContentContainer.add(WTPA);
            WTPA.setBounds(285, 35, 44, WTPA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2ContentContainer.setMinimumSize(preferredSize);
              panel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(5, 205, 425, 125);

          //======== panel5 ========
          {
            panel5.setBorder(new DropShadowBorder());
            panel5.setTitle("Opportunit\u00e9s");
            panel5.setName("panel5");
            Container panel5ContentContainer = panel5.getContentContainer();
            panel5ContentContainer.setLayout(null);

            //---- DOPP1 ----
            DOPP1.setText("@DOPP1@");
            DOPP1.setName("DOPP1");
            panel5ContentContainer.add(DOPP1);
            DOPP1.setBounds(15, 6, 364, DOPP1.getPreferredSize().height);

            //---- AFME1 ----
            AFME1.setName("AFME1");
            panel5ContentContainer.add(AFME1);
            AFME1.setBounds(200, 35, 104, AFME1.getPreferredSize().height);

            //---- AFPO1 ----
            AFPO1.setName("AFPO1");
            panel5ContentContainer.add(AFPO1);
            AFPO1.setBounds(200, 65, 74, 28);

            //---- OBJ_83 ----
            OBJ_83.setText("%");
            OBJ_83.setName("OBJ_83");
            panel5ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(280, 69, 45, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("Valeur");
            OBJ_84.setName("OBJ_84");
            panel5ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(15, 39, 100, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("taux de r\u00e9ussite");
            OBJ_85.setName("OBJ_85");
            panel5ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(15, 69, 100, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel5ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5ContentContainer.setMinimumSize(preferredSize);
              panel5ContentContainer.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel5);
          panel5.setBounds(440, 345, 425, 125);

          //======== panel4 ========
          {
            panel4.setBorder(new DropShadowBorder());
            panel4.setTitle("Notes");
            panel4.setName("panel4");
            Container panel4ContentContainer = panel4.getContentContainer();
            panel4ContentContainer.setLayout(null);

            //---- AFOBS ----
            AFOBS.setText("@AFOBS@");
            AFOBS.setName("AFOBS");
            panel4ContentContainer.add(AFOBS);
            AFOBS.setBounds(10, 30, 400, AFOBS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4ContentContainer.setMinimumSize(preferredSize);
              panel4ContentContainer.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel4);
          panel4.setBounds(5, 345, 425, 125);

          //======== panel3 ========
          {
            panel3.setBorder(new DropShadowBorder());
            panel3.setTitle("Int\u00e9r\u00eat");
            panel3.setName("panel3");
            Container panel3ContentContainer = panel3.getContentContainer();
            panel3ContentContainer.setLayout(null);

            //---- OBJ_81 ----
            OBJ_81.setText("Groupe/famille");
            OBJ_81.setName("OBJ_81");
            panel3ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(15, 8, 100, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Article");
            OBJ_82.setName("OBJ_82");
            panel3ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(15, 39, 80, 20);

            //---- LIBGF1 ----
            LIBGF1.setText("@LIBGF1@");
            LIBGF1.setName("LIBGF1");
            panel3ContentContainer.add(LIBGF1);
            LIBGF1.setBounds(200, 6, 164, LIBGF1.getPreferredSize().height);

            //---- AFAR1 ----
            AFAR1.setName("AFAR1");
            panel3ContentContainer.add(AFAR1);
            AFAR1.setBounds(200, 35, 214, AFAR1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3ContentContainer.setMinimumSize(preferredSize);
              panel3ContentContainer.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(440, 205, 425, 125);

          //---- OBJ_72 ----
          OBJ_72.setText("Affaire");
          OBJ_72.setName("OBJ_72");
          panel1.add(OBJ_72);
          OBJ_72.setBounds(5, 20, 90, 20);

          //---- OBJ_91 ----
          OBJ_91.setText("Contact");
          OBJ_91.setName("OBJ_91");
          panel1.add(OBJ_91);
          OBJ_91.setBounds(5, 55, 90, 20);

          //---- OBJ_74 ----
          OBJ_74.setText("Date de d\u00e9but");
          OBJ_74.setName("OBJ_74");
          panel1.add(OBJ_74);
          OBJ_74.setBounds(5, 125, 90, 20);

          //---- OBJ_73 ----
          OBJ_73.setText("T\u00e9l\u00e9phone");
          OBJ_73.setName("OBJ_73");
          panel1.add(OBJ_73);
          OBJ_73.setBounds(5, 90, 90, 20);

          //---- OBJ_75 ----
          OBJ_75.setText("Date de fin");
          OBJ_75.setName("OBJ_75");
          panel1.add(OBJ_75);
          OBJ_75.setBounds(5, 165, 90, 20);

          //---- OBJ_77 ----
          OBJ_77.setText("Objet");
          OBJ_77.setName("OBJ_77");
          panel1.add(OBJ_77);
          OBJ_77.setBounds(455, 20, 130, 20);

          //---- OBJ_92 ----
          OBJ_92.setText("Repr\u00e9sentant");
          OBJ_92.setName("OBJ_92");
          panel1.add(OBJ_92);
          OBJ_92.setBounds(455, 55, 130, 20);

          //---- OBJ_78 ----
          OBJ_78.setText("Etat");
          OBJ_78.setName("OBJ_78");
          panel1.add(OBJ_78);
          OBJ_78.setBounds(455, 90, 130, 20);

          //---- xRiCheckBox1 ----
          xRiCheckBox1.setText("D\u00e9sactiv\u00e9e");
          xRiCheckBox1.setName("xRiCheckBox1");
          panel1.add(xRiCheckBox1);
          xRiCheckBox1.setBounds(455, 130, 130, xRiCheckBox1.getPreferredSize().height);

          //---- AFLIB ----
          AFLIB.setName("AFLIB");
          panel1.add(AFLIB);
          AFLIB.setBounds(180, 15, 256, AFLIB.getPreferredSize().height);

          //---- RETEL ----
          RETEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
          RETEL.setName("RETEL");
          panel1.add(RETEL);
          RETEL.setBounds(100, 85, 210, RETEL.getPreferredSize().height);

          //---- REPAC ----
          REPAC.setName("REPAC");
          panel1.add(REPAC);
          REPAC.setBounds(100, 50, 256, REPAC.getPreferredSize().height);

          //---- AFDTDX ----
          AFDTDX.setName("AFDTDX");
          panel1.add(AFDTDX);
          AFDTDX.setBounds(100, 125, 105, AFDTDX.getPreferredSize().height);

          //---- AFDTFX ----
          AFDTFX.setName("AFDTFX");
          panel1.add(AFDTFX);
          AFDTFX.setBounds(100, 160, 105, AFDTFX.getPreferredSize().height);

          //---- AFOBJ ----
          AFOBJ.setName("AFOBJ");
          panel1.add(AFOBJ);
          AFOBJ.setBounds(610, 15, 56, AFOBJ.getPreferredSize().height);

          //---- AFLIB2 ----
          AFLIB2.setName("AFLIB2");
          panel1.add(AFLIB2);
          AFLIB2.setBounds(675, 15, 176, AFLIB2.getPreferredSize().height);

          //---- AFREP ----
          AFREP.setName("AFREP");
          panel1.add(AFREP);
          AFREP.setBounds(610, 50, 34, AFREP.getPreferredSize().height);

          //---- RPLIB1 ----
          RPLIB1.setName("RPLIB1");
          panel1.add(RPLIB1);
          RPLIB1.setBounds(675, 50, 176, RPLIB1.getPreferredSize().height);

          //---- AFETA ----
          AFETA.setName("AFETA");
          panel1.add(AFETA);
          AFETA.setBounds(610, 85, 24, AFETA.getPreferredSize().height);

          //---- LIBETA ----
          LIBETA.setName("LIBETA");
          panel1.add(LIBETA);
          LIBETA.setBounds(675, 85, 176, LIBETA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 875, 480);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- TCI1 ----
    TCI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI1.setBorder(null);
    TCI1.setName("TCI1");
    TCI1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI1ActionPerformed(e);
      }
    });

    //---- TCI2 ----
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setBorder(null);
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setBorder(null);
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI4 ----
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setBorder(null);
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField AFNUM;
  private JXTitledPanel panel2;
  private JLabel OBJ_76;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private XRiCalendrier WDDAX;
  private XRiCalendrier WDPAX;
  private XRiTextField AFEXE;
  private XRiTextField WTDA;
  private XRiTextField WTPA;
  private JXTitledPanel panel5;
  private RiZoneSortie DOPP1;
  private XRiTextField AFME1;
  private XRiTextField AFPO1;
  private JLabel OBJ_83;
  private JLabel OBJ_84;
  private JLabel OBJ_85;
  private JXTitledPanel panel4;
  private RiZoneSortie AFOBS;
  private JXTitledPanel panel3;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private RiZoneSortie LIBGF1;
  private XRiTextField AFAR1;
  private JLabel OBJ_72;
  private JLabel OBJ_91;
  private JLabel OBJ_74;
  private JLabel OBJ_73;
  private JLabel OBJ_75;
  private JLabel OBJ_77;
  private JLabel OBJ_92;
  private JLabel OBJ_78;
  private XRiCheckBox xRiCheckBox1;
  private XRiTextField AFLIB;
  private XRiTextField RETEL;
  private XRiTextField REPAC;
  private XRiCalendrier AFDTDX;
  private XRiCalendrier AFDTFX;
  private XRiTextField AFOBJ;
  private XRiTextField AFLIB2;
  private XRiTextField AFREP;
  private XRiTextField RPLIB1;
  private XRiTextField AFETA;
  private XRiTextField LIBETA;
  private SNBoutonDetail TCI1;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
