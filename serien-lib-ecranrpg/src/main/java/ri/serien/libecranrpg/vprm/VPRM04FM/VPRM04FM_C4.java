
package ri.serien.libecranrpg.vprm.VPRM04FM;
// Nom Fichier: pop_VPRM04FM_FMTC4_FMTF1_99.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VPRM04FM_C4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VPRM04FM_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DOPP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOPP1@")).trim());
    DOPP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOPP2@")).trim());
    DOPP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOPP3@")).trim());
    DOPP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOPP4@")).trim());
    DOPP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOPP5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Opportunités");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    DOPP1 = new RiZoneSortie();
    AFME1 = new XRiTextField();
    AFPO1 = new XRiTextField();
    label1 = new JLabel();
    separator1 = compFactory.createSeparator("Description de l'opportunit\u00e9");
    separator2 = compFactory.createSeparator("Valeur");
    separator3 = compFactory.createSeparator("R\u00e9ussite");
    DOPP2 = new RiZoneSortie();
    AFME2 = new XRiTextField();
    AFPO2 = new XRiTextField();
    label2 = new JLabel();
    DOPP3 = new RiZoneSortie();
    AFME3 = new XRiTextField();
    AFPO3 = new XRiTextField();
    label3 = new JLabel();
    DOPP4 = new RiZoneSortie();
    AFME4 = new XRiTextField();
    AFPO4 = new XRiTextField();
    label4 = new JLabel();
    DOPP5 = new RiZoneSortie();
    AFME5 = new XRiTextField();
    AFPO5 = new XRiTextField();
    label5 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_25 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 270));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- DOPP1 ----
          DOPP1.setText("@DOPP1@");
          DOPP1.setName("DOPP1");
          panel1.add(DOPP1);
          DOPP1.setBounds(20, 55, 364, DOPP1.getPreferredSize().height);

          //---- AFME1 ----
          AFME1.setName("AFME1");
          panel1.add(AFME1);
          AFME1.setBounds(400, 55, 84, AFME1.getPreferredSize().height);

          //---- AFPO1 ----
          AFPO1.setName("AFPO1");
          panel1.add(AFPO1);
          AFPO1.setBounds(495, 55, 60, AFPO1.getPreferredSize().height);

          //---- label1 ----
          label1.setText("%");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(560, 55, 25, 28);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(20, 35, 365, 20);

          //---- separator2 ----
          separator2.setName("separator2");
          panel1.add(separator2);
          separator2.setBounds(400, 35, 84, 20);

          //---- separator3 ----
          separator3.setName("separator3");
          panel1.add(separator3);
          separator3.setBounds(495, 35, 80, 20);

          //---- DOPP2 ----
          DOPP2.setText("@DOPP2@");
          DOPP2.setName("DOPP2");
          panel1.add(DOPP2);
          DOPP2.setBounds(20, 88, 364, 24);

          //---- AFME2 ----
          AFME2.setName("AFME2");
          panel1.add(AFME2);
          AFME2.setBounds(400, 88, 84, 28);

          //---- AFPO2 ----
          AFPO2.setName("AFPO2");
          panel1.add(AFPO2);
          AFPO2.setBounds(495, 88, 60, 28);

          //---- label2 ----
          label2.setText("%");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(560, 88, 25, 28);

          //---- DOPP3 ----
          DOPP3.setText("@DOPP3@");
          DOPP3.setName("DOPP3");
          panel1.add(DOPP3);
          DOPP3.setBounds(20, 121, 364, 24);

          //---- AFME3 ----
          AFME3.setName("AFME3");
          panel1.add(AFME3);
          AFME3.setBounds(400, 121, 84, 28);

          //---- AFPO3 ----
          AFPO3.setName("AFPO3");
          panel1.add(AFPO3);
          AFPO3.setBounds(495, 121, 60, 28);

          //---- label3 ----
          label3.setText("%");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(560, 121, 25, 28);

          //---- DOPP4 ----
          DOPP4.setText("@DOPP4@");
          DOPP4.setName("DOPP4");
          panel1.add(DOPP4);
          DOPP4.setBounds(20, 154, 364, 24);

          //---- AFME4 ----
          AFME4.setName("AFME4");
          panel1.add(AFME4);
          AFME4.setBounds(400, 154, 84, 28);

          //---- AFPO4 ----
          AFPO4.setName("AFPO4");
          panel1.add(AFPO4);
          AFPO4.setBounds(495, 154, 60, 28);

          //---- label4 ----
          label4.setText("%");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(560, 154, 25, 28);

          //---- DOPP5 ----
          DOPP5.setText("@DOPP5@");
          DOPP5.setName("DOPP5");
          panel1.add(DOPP5);
          DOPP5.setBounds(20, 187, 364, 24);

          //---- AFME5 ----
          AFME5.setName("AFME5");
          panel1.add(AFME5);
          AFME5.setBounds(400, 187, 84, 28);

          //---- AFPO5 ----
          AFPO5.setName("AFPO5");
          panel1.add(AFPO5);
          AFPO5.setBounds(495, 187, 60, 28);

          //---- label5 ----
          label5.setText("%");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(560, 187, 25, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 620, 255);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie DOPP1;
  private XRiTextField AFME1;
  private XRiTextField AFPO1;
  private JLabel label1;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private RiZoneSortie DOPP2;
  private XRiTextField AFME2;
  private XRiTextField AFPO2;
  private JLabel label2;
  private RiZoneSortie DOPP3;
  private XRiTextField AFME3;
  private XRiTextField AFPO3;
  private JLabel label3;
  private RiZoneSortie DOPP4;
  private XRiTextField AFME4;
  private XRiTextField AFPO4;
  private JLabel label4;
  private RiZoneSortie DOPP5;
  private XRiTextField AFME5;
  private XRiTextField AFPO5;
  private JLabel label5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_25;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
