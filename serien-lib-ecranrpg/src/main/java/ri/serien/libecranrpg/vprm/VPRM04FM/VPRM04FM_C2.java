
package ri.serien.libecranrpg.vprm.VPRM04FM;
// Nom Fichier: pop_VPRM04FM_FMTC2_FMTF1_10.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VPRM04FM_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] tableZones = { "AFGF1", "AFGF2", "AFGF3", "AFGF4" };
  
  public VPRM04FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBGF1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGF1@")).trim());
    LIBAR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAR1@")).trim());
    LIBGF2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGF2@")).trim());
    LIBAR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAR2@")).trim());
    LIBGF3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGF3@")).trim());
    LIBAR3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAR3@")).trim());
    LIBGF4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGF4@")).trim());
    LIBAR4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAR4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Intérêts pour");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    int ligne = lexique.getIndexTableau(BTD.getInvoker().getName(), tableZones) + 16;
    lexique.HostCursorPut(ligne, 5);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    int ligne = lexique.getIndexTableau(BTD.getInvoker().getName(), tableZones) + 16;
    lexique.HostCursorPut(ligne, 7);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    AFGF1 = new XRiTextField();
    LIBGF1 = new RiZoneSortie();
    AFAR1 = new XRiTextField();
    LIBAR1 = new RiZoneSortie();
    AFQT1 = new XRiTextField();
    AFUN1 = new XRiTextField();
    separator1 = compFactory.createSeparator("Groupe / famille");
    separator2 = compFactory.createSeparator("Article");
    separator3 = compFactory.createSeparator("Quantit\u00e9");
    AFGF2 = new XRiTextField();
    LIBGF2 = new RiZoneSortie();
    AFAR2 = new XRiTextField();
    LIBAR2 = new RiZoneSortie();
    AFQT2 = new XRiTextField();
    AFUN2 = new XRiTextField();
    AFGF3 = new XRiTextField();
    LIBGF3 = new RiZoneSortie();
    AFAR3 = new XRiTextField();
    LIBAR3 = new RiZoneSortie();
    AFQT3 = new XRiTextField();
    AFUN3 = new XRiTextField();
    AFGF4 = new XRiTextField();
    LIBGF4 = new RiZoneSortie();
    AFAR4 = new XRiTextField();
    LIBAR4 = new RiZoneSortie();
    AFQT4 = new XRiTextField();
    AFUN4 = new XRiTextField();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles groupes");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles familles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1050, 215));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- AFGF1 ----
          AFGF1.setComponentPopupMenu(BTD);
          AFGF1.setName("AFGF1");
          panel1.add(AFGF1);
          AFGF1.setBounds(10, 45, 44, AFGF1.getPreferredSize().height);

          //---- LIBGF1 ----
          LIBGF1.setText("@LIBGF1@");
          LIBGF1.setName("LIBGF1");
          panel1.add(LIBGF1);
          LIBGF1.setBounds(55, 47, 164, LIBGF1.getPreferredSize().height);

          //---- AFAR1 ----
          AFAR1.setName("AFAR1");
          panel1.add(AFAR1);
          AFAR1.setBounds(240, 45, 214, AFAR1.getPreferredSize().height);

          //---- LIBAR1 ----
          LIBAR1.setText("@LIBAR1@");
          LIBAR1.setName("LIBAR1");
          panel1.add(LIBAR1);
          LIBAR1.setBounds(455, 47, 244, LIBAR1.getPreferredSize().height);

          //---- AFQT1 ----
          AFQT1.setName("AFQT1");
          panel1.add(AFQT1);
          AFQT1.setBounds(740, 45, 52, AFQT1.getPreferredSize().height);

          //---- AFUN1 ----
          AFUN1.setName("AFUN1");
          panel1.add(AFUN1);
          AFUN1.setBounds(795, 45, 34, AFUN1.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(10, 25, 210, 19);

          //---- separator2 ----
          separator2.setName("separator2");
          panel1.add(separator2);
          separator2.setBounds(240, 25, 460, 19);

          //---- separator3 ----
          separator3.setName("separator3");
          panel1.add(separator3);
          separator3.setBounds(740, 25, 85, 19);

          //---- AFGF2 ----
          AFGF2.setComponentPopupMenu(BTD);
          AFGF2.setName("AFGF2");
          panel1.add(AFGF2);
          AFGF2.setBounds(10, 78, 44, 28);

          //---- LIBGF2 ----
          LIBGF2.setText("@LIBGF2@");
          LIBGF2.setName("LIBGF2");
          panel1.add(LIBGF2);
          LIBGF2.setBounds(55, 80, 164, 24);

          //---- AFAR2 ----
          AFAR2.setName("AFAR2");
          panel1.add(AFAR2);
          AFAR2.setBounds(240, 78, 214, 28);

          //---- LIBAR2 ----
          LIBAR2.setText("@LIBAR2@");
          LIBAR2.setName("LIBAR2");
          panel1.add(LIBAR2);
          LIBAR2.setBounds(455, 80, 244, 24);

          //---- AFQT2 ----
          AFQT2.setName("AFQT2");
          panel1.add(AFQT2);
          AFQT2.setBounds(740, 78, 52, 28);

          //---- AFUN2 ----
          AFUN2.setName("AFUN2");
          panel1.add(AFUN2);
          AFUN2.setBounds(795, 78, 34, 28);

          //---- AFGF3 ----
          AFGF3.setComponentPopupMenu(BTD);
          AFGF3.setName("AFGF3");
          panel1.add(AFGF3);
          AFGF3.setBounds(10, 111, 44, 28);

          //---- LIBGF3 ----
          LIBGF3.setText("@LIBGF3@");
          LIBGF3.setName("LIBGF3");
          panel1.add(LIBGF3);
          LIBGF3.setBounds(55, 113, 164, 24);

          //---- AFAR3 ----
          AFAR3.setName("AFAR3");
          panel1.add(AFAR3);
          AFAR3.setBounds(240, 111, 214, 28);

          //---- LIBAR3 ----
          LIBAR3.setText("@LIBAR3@");
          LIBAR3.setName("LIBAR3");
          panel1.add(LIBAR3);
          LIBAR3.setBounds(455, 113, 244, 24);

          //---- AFQT3 ----
          AFQT3.setName("AFQT3");
          panel1.add(AFQT3);
          AFQT3.setBounds(740, 111, 52, 28);

          //---- AFUN3 ----
          AFUN3.setName("AFUN3");
          panel1.add(AFUN3);
          AFUN3.setBounds(795, 111, 34, 28);

          //---- AFGF4 ----
          AFGF4.setComponentPopupMenu(BTD);
          AFGF4.setName("AFGF4");
          panel1.add(AFGF4);
          AFGF4.setBounds(10, 144, 44, 28);

          //---- LIBGF4 ----
          LIBGF4.setText("@LIBGF4@");
          LIBGF4.setName("LIBGF4");
          panel1.add(LIBGF4);
          LIBGF4.setBounds(55, 146, 164, 24);

          //---- AFAR4 ----
          AFAR4.setName("AFAR4");
          panel1.add(AFAR4);
          AFAR4.setBounds(240, 144, 214, 28);

          //---- LIBAR4 ----
          LIBAR4.setText("@LIBAR4@");
          LIBAR4.setName("LIBAR4");
          panel1.add(LIBAR4);
          LIBAR4.setBounds(455, 146, 244, 24);

          //---- AFQT4 ----
          AFQT4.setName("AFQT4");
          panel1.add(AFQT4);
          AFQT4.setBounds(740, 144, 52, 28);

          //---- AFUN4 ----
          AFUN4.setName("AFUN4");
          panel1.add(AFUN4);
          AFUN4.setBounds(795, 144, 34, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 865, 195);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField AFGF1;
  private RiZoneSortie LIBGF1;
  private XRiTextField AFAR1;
  private RiZoneSortie LIBAR1;
  private XRiTextField AFQT1;
  private XRiTextField AFUN1;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private XRiTextField AFGF2;
  private RiZoneSortie LIBGF2;
  private XRiTextField AFAR2;
  private RiZoneSortie LIBAR2;
  private XRiTextField AFQT2;
  private XRiTextField AFUN2;
  private XRiTextField AFGF3;
  private RiZoneSortie LIBGF3;
  private XRiTextField AFAR3;
  private RiZoneSortie LIBAR3;
  private XRiTextField AFQT3;
  private XRiTextField AFUN3;
  private XRiTextField AFGF4;
  private RiZoneSortie LIBGF4;
  private XRiTextField AFAR4;
  private RiZoneSortie LIBAR4;
  private XRiTextField AFQT4;
  private XRiTextField AFUN4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
