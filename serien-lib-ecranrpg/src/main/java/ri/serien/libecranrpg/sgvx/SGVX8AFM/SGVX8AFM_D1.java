
package ri.serien.libecranrpg.sgvx.SGVX8AFM;
// Nom Fichier: pop_SGVX8AFM_FMTD1_FMTF1_131.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVX8AFM_D1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] WNMC_Value = { "", "0", "1", };
  
  public SGVX8AFM_D1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    WNMC.setValeurs(WNMC_Value, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WUNS.setEnabled(lexique.isPresent("WUNS"));
    WUNV.setEnabled(lexique.isPresent("WUNV"));
    PRVFIN.setEnabled(lexique.isPresent("PRVFIN"));
    PRVDEB.setEnabled(lexique.isPresent("PRVDEB"));
    // WNMC.setSelectedIndex(getIndice("WNMC", WNMC_Value));
    // WNMC.setVisible( lexique.isPresent("WNMC"));
    // WNMC.setEnabled( lexique.isPresent("WNMC"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_15.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("RECHERCHE"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // lexique.HostFieldPutData("WNMC", 0, WNMC_Value[WNMC.getSelectedIndex()]);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvx8a"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel3 = new JPanel();
    panel1 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_15 = new JButton();
    panel4 = new JPanel();
    panel2 = new JPanel();
    OBJ_19 = new JLabel();
    PRVDEB = new XRiTextField();
    OBJ_22 = new JLabel();
    PRVFIN = new XRiTextField();
    WUNV = new XRiTextField();
    OBJ_16 = new JLabel();
    OBJ_17 = new JLabel();
    WUNS = new XRiTextField();
    WNMC = new XRiComboBox();
    OBJ_18 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_8 = new JLabel();

    //======== this ========
    setPreferredSize(new Dimension(375, 225));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel3 ========
    {
      panel3.setName("panel3");
      panel3.setLayout(null);

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("Ok");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_14ActionPerformed(e);
          }
        });
        panel1.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        //---- OBJ_15 ----
        OBJ_15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_15.setToolTipText("Retour");
        OBJ_15.setName("OBJ_15");
        OBJ_15.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_15ActionPerformed(e);
          }
        });
        panel1.add(OBJ_15);
        OBJ_15.setBounds(60, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      panel3.add(panel1);
      panel1.setBounds(250, 0, 120, 50);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel3.getComponentCount(); i++) {
          Rectangle bounds = panel3.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel3.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel3.setMinimumSize(preferredSize);
        panel3.setPreferredSize(preferredSize);
      }
    }
    add(panel3, BorderLayout.SOUTH);

    //======== panel4 ========
    {
      panel4.setName("panel4");
      panel4.setLayout(null);

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder("Autres crit\u00e8res"));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- OBJ_19 ----
        OBJ_19.setText("Limite(s) prix tarif");
        OBJ_19.setName("OBJ_19");
        panel2.add(OBJ_19);
        OBJ_19.setBounds(30, 41, 101, 16);

        //---- PRVDEB ----
        PRVDEB.setComponentPopupMenu(BTD);
        PRVDEB.setName("PRVDEB");
        panel2.add(PRVDEB);
        PRVDEB.setBounds(160, 35, 66, PRVDEB.getPreferredSize().height);

        //---- OBJ_22 ----
        OBJ_22.setText("/");
        OBJ_22.setName("OBJ_22");
        panel2.add(OBJ_22);
        OBJ_22.setBounds(240, 41, 12, 16);

        //---- PRVFIN ----
        PRVFIN.setComponentPopupMenu(BTD);
        PRVFIN.setName("PRVFIN");
        panel2.add(PRVFIN);
        PRVFIN.setBounds(270, 35, 66, PRVFIN.getPreferredSize().height);

        //---- WUNV ----
        WUNV.setComponentPopupMenu(BTD);
        WUNV.setName("WUNV");
        panel2.add(WUNV);
        WUNV.setBounds(160, 65, 30, WUNV.getPreferredSize().height);

        //---- OBJ_16 ----
        OBJ_16.setText("Unit\u00e9 de Vente");
        OBJ_16.setName("OBJ_16");
        panel2.add(OBJ_16);
        OBJ_16.setBounds(30, 71, 91, 16);

        //---- OBJ_17 ----
        OBJ_17.setText("Unit\u00e9 de Stock");
        OBJ_17.setName("OBJ_17");
        panel2.add(OBJ_17);
        OBJ_17.setBounds(30, 101, 90, 16);

        //---- WUNS ----
        WUNS.setComponentPopupMenu(BTD);
        WUNS.setName("WUNS");
        panel2.add(WUNS);
        WUNS.setBounds(160, 95, 30, WUNS.getPreferredSize().height);

        //---- WNMC ----
        WNMC.setModel(new DefaultComboBoxModel(new String[] {
          "Tous les articles",
          "Articles sauf nomenclatures",
          "Articles nomenclatures"
        }));
        WNMC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WNMC.setName("WNMC");
        panel2.add(WNMC);
        WNMC.setBounds(160, 125, 157, WNMC.getPreferredSize().height);

        //---- OBJ_18 ----
        OBJ_18.setText("Nomenclature");
        OBJ_18.setName("OBJ_18");
        panel2.add(OBJ_18);
        OBJ_18.setBounds(30, 130, 87, 16);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      panel4.add(panel2);
      panel2.setBounds(5, 5, 365, 170);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel4.getComponentCount(); i++) {
          Rectangle bounds = panel4.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel4.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel4.setMinimumSize(preferredSize);
        panel4.setPreferredSize(preferredSize);
      }
    }
    add(panel4, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //---- OBJ_8 ----
    OBJ_8.setText("Autres crit\u00e8res");
    OBJ_8.setName("OBJ_8");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel3;
  private JPanel panel1;
  private JButton BT_ENTER;
  private JButton OBJ_15;
  private JPanel panel4;
  private JPanel panel2;
  private JLabel OBJ_19;
  private XRiTextField PRVDEB;
  private JLabel OBJ_22;
  private XRiTextField PRVFIN;
  private XRiTextField WUNV;
  private JLabel OBJ_16;
  private JLabel OBJ_17;
  private XRiTextField WUNS;
  private XRiComboBox WNMC;
  private JLabel OBJ_18;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  private JLabel OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
