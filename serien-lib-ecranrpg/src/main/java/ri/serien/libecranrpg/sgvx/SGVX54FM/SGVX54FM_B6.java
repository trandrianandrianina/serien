
package ri.serien.libecranrpg.sgvx.SGVX54FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Indicateur 17 si plusieurs magasin
 * 
 * [GVM3415] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Général -> Par fournisseur
 * Indicateur:10001001 (91 et 95)
 * Titre:Chiffrage général des stocks par fournisseur
 * 
 * [GVM3425] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Par magasin -> Par fournisseur
 * Indicateurs:00001001 (95)
 * Titre:Chiffrage général des stocks par magasin et par fournisseur
 * 
 * [GVM3465] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (général) -> Par fournisseur
 * Indicateur:110001001(91,92 et 95)
 * Titre: Chiffrage général des lots de stock par fournisseur
 * 
 * [GVM3475] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (par magasin) -> Par fournisseur
 * Indicateur:100001001 (90 et 95)
 * Titre:Chiffrage des lots de stocks par magasin et par fournisseur
 * 
 * [GAM3415] Gestion des achats -> Stocks -> Chiffrage stocks -> Général -> Par fournisseur
 * Indicateur:10001001 (91 et 95)
 * Titre:Chiffrage général des stocks par fournisseur
 * 
 * [GAM3425] Gestion des achats -> Stocks -> Chiffrage stocks -> Par magasin -> Par fournisseur
 * Indicateurs:00001001 (95)
 * Titre:Chiffrage général des stocks par magasin et par fournisseur
 * 
 * [GAM3455] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (général) -> Par fournisseur
 * Indicateur:110001001 (90,91 et 95)
 * Titre:Chiffrage des lots de stocks par fournisseur
 * 
 * [GAM3465] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (par magasin) -> Par fournisseur
 * Indicateur:100001001(90 et 95)
 * Titre:Chiffrage des lots de stocks par magasin et par fournisseur
 * 
 * [GAM3951] Gestion des achats -> Stocks -> Editions diverses -> Stocks fournisseurs -> Calcul stock/achats/conso. mensuel
 * Indicateur:00001001 (95) autre indicateurs:87,75,66,58,10 et 11
 * Titre: Calcul des stocks sur les achats et par les consommation mensuel (par magasin)
 * 
 * [GPM5411] Gestion de la production -> Stocks -> Chiffrages de stocks -> Général -> Par Famille
 * Indicateur:10011001 (91,94 et 95)
 * Titre: Chiffrage général des stocks par famille
 * 
 * [GPM5412] Gestion de la production -> Stocks -> Chiffrages de stocks -> Général -> Par Article
 * Indicateur:10001001 (91 et 95)
 * Titre: Chiffrage général des stocks par article
 * 
 * [GPM5413] Gestion de la production -> Stocks -> Chiffrages de stocks -> Général -> Par Mot de Classement 2
 * Indicateur:10101001 (91,93 et 95)
 * Titre: Chiffrage général des stocks par le 2ème mot de classement
 * 
 * [GPM5414] Gestion de la production -> Stocks -> Chiffrages de stocks -> Général -> Par Sous-Famille
 * Indicateur:11001001 (91,92 et 95)
 * Titre: Chiffrage général des stocks par la sous-famille
 * 
 * [GPM5421] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Famille
 * Indicateur:00011001 (94 et 95)
 * Titre:Chiffrage général des stocks par magasin et par famille
 * 
 * [GPM5422] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Article
 * Indicateur:00001001 (95)
 * Titre:Chiffrage général des stocks par magasin et par article
 * 
 * [GPM5423] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Mot de Classement 2
 * Indicateur:00101001 (93 et 95)
 * Titre:Chiffrage général des stocks par magasin et par le 2ème mot de classement
 * 
 * [GPM5424] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Sous-Famille
 * Indicateur:01001001 (92 et 95)
 * Titre:Chiffrage général des stocks par magasin et par la sous-famille
 * 
 */
public class SGVX54FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORTER = "Exporter";
  private Message LOCTP = null;
  
  public SGVX54FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    REPON1.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'E', true);
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Gere les erreur
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Indicateur
    // Indicateur si c'est une sélection de plage de fournisseur
    Boolean isPlageFournisseur = lexique.isTrue("(N87)");
    Boolean isMagasinVisible = lexique.isTrue("(N91)");
    
    Boolean isCalculStock = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N94) and 95 and 87");
    Boolean isGeneralMagasinFournisseur = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N93) AND (N94) and 95");
    Boolean isLotMagasinFournisseur = lexique.isTrue("90 AND (N91) AND (N92) AND (N94) and 95");
    Boolean isGeneralMagasinSousFamille = lexique.isTrue("(N90) AND (N91) AND 92 AND (N94) and 95");
    Boolean isGeneralFournisseur = lexique.isTrue("(N90) AND 91 AND (N92) AND (N94) and 95");
    Boolean isGeneralMagasinFamille = lexique.isTrue("(N90) AND (N91) AND (N92) AND 94 and 95");
    Boolean isLotFournisseur = lexique.isTrue("90 AND 91 AND (N92) AND (N94) and 95");
    Boolean isGeneralLotFournisseur = lexique.isTrue("(N90) AND 91 AND 92 AND (N94) and 95");
    Boolean isGeneralMotClassement = lexique.isTrue("(N90) AND 91 AND (N92) AND 93 AND (N94) and 95");
    Boolean isGeneralFamille = lexique.isTrue("(N90) AND 91 AND (N92) AND (N93) AND 94 and 95");
    Boolean isGeneralArticle = lexique.isTrue("(N90) AND 91 AND (N92) AND (N93) AND (N94) and 95");
    Boolean isGeneralSousFamille = lexique.isTrue("(N90) AND 91 AND 92 AND (N93) AND (N94) and 95");
    Boolean isGeneralMagasinArticle = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N93) AND (N94) and 95");
    Boolean isGeneralMagasinMotClassement = lexique.isTrue("(N90) AND (N91) AND (N92) AND 93 AND (N94) and 95");
    
    // Titre
    if (isCalculStock) {
      bpPresentation.setText("Calcul des stocks sur les achats et par les consommation mensuel (par magasin)");
    }
    if (isGeneralMagasinFournisseur) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par fournisseur");
    }
    if (isLotMagasinFournisseur) {
      bpPresentation.setText("Chiffrage des lots de stocks par magasin et par fournisseur");
    }
    if (isGeneralMagasinSousFamille) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par sous-famille");
    }
    if (isGeneralFournisseur) {
      bpPresentation.setText("Chiffrage général des stocks par fournisseur");
    }
    if (isGeneralMagasinFamille) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par famille");
    }
    if (isLotFournisseur) {
      bpPresentation.setText("Chiffrage des lots de stocks par fournisseur");
    }
    if (isGeneralLotFournisseur) {
      bpPresentation.setText("Chiffrage général des lots de stock par fournisseur");
    }
    if (isGeneralMotClassement) {
      bpPresentation.setText("Chiffrage général des stocks par le 2ème mot de classement");
    }
    if (isGeneralFamille) {
      bpPresentation.setText("Chiffrage général des stocks par famille");
    }
    if (isGeneralArticle && lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5412")) {
      bpPresentation.setText("Chiffrage général des stocks par article");
    }
    if (isGeneralSousFamille) {
      bpPresentation.setText("Chiffrage général des stocks par sous-famille");
    }
    if (isGeneralMagasinArticle && lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5422")) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par article");
    }
    if (isGeneralMagasinMotClassement) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par le 2ème mot de classement");
    }
    
    // Visibilité
    snMagasin1.setVisible(isMagasinVisible);
    lbMagasin1.setVisible(isMagasinVisible);
    snMagasin2.setVisible(isMagasinVisible);
    lbMagasin2.setVisible(isMagasinVisible);
    snMagasin3.setVisible(isMagasinVisible);
    lbMagasin3.setVisible(isMagasinVisible);
    snMagasin4.setVisible(isMagasinVisible);
    lbMagasin4.setVisible(isMagasinVisible);
    snMagasin5.setVisible(isMagasinVisible);
    lbMagasin5.setVisible(isMagasinVisible);
    snMagasin6.setVisible(isMagasinVisible);
    lbMagasin6.setVisible(isMagasinVisible);
    pnlOptionEdition.setVisible(REPON1.isVisible());
    
    snFournisseurFin.setVisible(isPlageFournisseur);
    lbFin.setVisible(isPlageFournisseur);
    if (!isPlageFournisseur) {
      lbFournisseurDebut.setText("Fournisseur");
    }
    else {
      lbFournisseurDebut.setText("Fournisseur début");
    }
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation des composants
    chargerListeFournisseur();
    chargerListeComposantsMagasin();
    changerBouton();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    // Gestion de la selection complete
    if (snMagasin1.isVisible() || snMagasin2.isVisible() || snMagasin3.isVisible() || snMagasin4.isVisible() || snMagasin5.isVisible()
        || snMagasin6.isVisible()) {
      if (snMagasin1.getIdSelection() == null && snMagasin2.getIdSelection() == null && snMagasin3.getIdSelection() == null
          && snMagasin4.getIdSelection() == null && snMagasin5.getIdSelection() == null && snMagasin6.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUM", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUM", 0, "  ");
        snMagasin1.renseignerChampRPG(lexique, "MA01");
        snMagasin2.renseignerChampRPG(lexique, "MA02");
        snMagasin3.renseignerChampRPG(lexique, "MA03");
        snMagasin4.renseignerChampRPG(lexique, "MA04");
        snMagasin5.renseignerChampRPG(lexique, "MA05");
        snMagasin6.renseignerChampRPG(lexique, "MA06");
      }
    }
    if (snFournisseurDebut.getIdSelection() == null && snFournisseurFin.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
      snFournisseurDebut.renseignerChampRPG(lexique, "FRSDEB");
      if (snFournisseurFin.isVisible()) {
        snFournisseurFin.renseignerChampRPG(lexique, "FRSFIN");
      }
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composant fournisseur
   */
  private void chargerListeFournisseur() {
    snFournisseurDebut.setSession(getSession());
    snFournisseurDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurDebut.charger(true);
    snFournisseurDebut.setSelectionParChampRPG(lexique, "FRSDEB");
    snFournisseurFin.setSession(getSession());
    snFournisseurFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurFin.charger(true);
    snFournisseurFin.setSelectionParChampRPG(lexique, "FRSFIN");
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * snMagasin1 : "Tous" autorisé, "Aucun" interdit
   * snMagasin2-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    if (lexique.isTrue("91")) {
      return;
    }
    snMagasin2.setVisible(false);
    snMagasin3.setVisible(false);
    snMagasin4.setVisible(false);
    snMagasin5.setVisible(false);
    snMagasin6.setVisible(false);
    
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(false);
    snMagasin1.setAucunAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On vérifie que le nombre de magasins n'est pas supérieur au nombre de composant magasin présent.
    // On charge un nombre de composants égal au nombre de choix possibles ou au nombre maximal de composants disponibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On affiche les composants snMagasin2-6 si ils ne sont pas en "Aucun" (null)
      listeComposant.get(i).setVisible(!(listeComposant.get(i).getIdSelection() == null));
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
  }
  
  private void changerBouton() {
    if (lexique.isTrue("17")) {
      snBarreBouton.activerBouton(EnumBouton.ANNULER, true);
      snBarreBouton.activerBouton(EnumBouton.QUITTER, false);
      snEtablissement.setEnabled(false);
    }
    else {
      snBarreBouton.activerBouton(EnumBouton.ANNULER, false);
      snBarreBouton.activerBouton(EnumBouton.QUITTER, true);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeFournisseur();
      chargerListeComposantsMagasin();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    lbFournisseurDebut = new SNLabelChamp();
    snFournisseurDebut = new SNFournisseur();
    lbFin = new SNLabelChamp();
    snFournisseurFin = new SNFournisseur();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlOptionEdition = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin1 ----
            lbMagasin1.setText("Magasin 1");
            lbMagasin1.setMaximumSize(new Dimension(175, 30));
            lbMagasin1.setMinimumSize(new Dimension(175, 30));
            lbMagasin1.setPreferredSize(new Dimension(175, 30));
            lbMagasin1.setName("lbMagasin1");
            pnlCritereDeSelection.add(lbMagasin1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin1.setEnabled(false);
            snMagasin1.setName("snMagasin1");
            pnlCritereDeSelection.add(snMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin 2");
            lbMagasin2.setMaximumSize(new Dimension(175, 30));
            lbMagasin2.setMinimumSize(new Dimension(175, 30));
            lbMagasin2.setPreferredSize(new Dimension(175, 30));
            lbMagasin2.setName("lbMagasin2");
            pnlCritereDeSelection.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin2 ----
            snMagasin2.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin2.setEnabled(false);
            snMagasin2.setName("snMagasin2");
            pnlCritereDeSelection.add(snMagasin2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Magasin 3");
            lbMagasin3.setMaximumSize(new Dimension(175, 30));
            lbMagasin3.setMinimumSize(new Dimension(175, 30));
            lbMagasin3.setPreferredSize(new Dimension(175, 30));
            lbMagasin3.setName("lbMagasin3");
            pnlCritereDeSelection.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin3 ----
            snMagasin3.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin3.setEnabled(false);
            snMagasin3.setName("snMagasin3");
            pnlCritereDeSelection.add(snMagasin3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin4 ----
            lbMagasin4.setText("Magasin 4");
            lbMagasin4.setMaximumSize(new Dimension(175, 30));
            lbMagasin4.setMinimumSize(new Dimension(175, 30));
            lbMagasin4.setPreferredSize(new Dimension(175, 30));
            lbMagasin4.setName("lbMagasin4");
            pnlCritereDeSelection.add(lbMagasin4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin4 ----
            snMagasin4.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin4.setEnabled(false);
            snMagasin4.setName("snMagasin4");
            pnlCritereDeSelection.add(snMagasin4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin5 ----
            lbMagasin5.setText("Magasin 5");
            lbMagasin5.setMaximumSize(new Dimension(175, 30));
            lbMagasin5.setMinimumSize(new Dimension(175, 30));
            lbMagasin5.setPreferredSize(new Dimension(175, 30));
            lbMagasin5.setName("lbMagasin5");
            pnlCritereDeSelection.add(lbMagasin5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin5 ----
            snMagasin5.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin5.setEnabled(false);
            snMagasin5.setName("snMagasin5");
            pnlCritereDeSelection.add(snMagasin5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin6 ----
            lbMagasin6.setText("Magasin 6");
            lbMagasin6.setMaximumSize(new Dimension(175, 30));
            lbMagasin6.setMinimumSize(new Dimension(175, 30));
            lbMagasin6.setPreferredSize(new Dimension(175, 30));
            lbMagasin6.setName("lbMagasin6");
            pnlCritereDeSelection.add(lbMagasin6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin6 ----
            snMagasin6.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin6.setEnabled(false);
            snMagasin6.setName("snMagasin6");
            pnlCritereDeSelection.add(snMagasin6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurDebut ----
            lbFournisseurDebut.setVerticalAlignment(SwingConstants.CENTER);
            lbFournisseurDebut.setText("texte dans le code");
            lbFournisseurDebut.setMaximumSize(new Dimension(175, 30));
            lbFournisseurDebut.setMinimumSize(new Dimension(175, 30));
            lbFournisseurDebut.setPreferredSize(new Dimension(175, 30));
            lbFournisseurDebut.setName("lbFournisseurDebut");
            pnlCritereDeSelection.add(lbFournisseurDebut, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseurDebut ----
            snFournisseurDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFournisseurDebut.setPreferredSize(new Dimension(350, 30));
            snFournisseurDebut.setMinimumSize(new Dimension(350, 30));
            snFournisseurDebut.setMaximumSize(new Dimension(350, 30));
            snFournisseurDebut.setName("snFournisseurDebut");
            pnlCritereDeSelection.add(snFournisseurDebut, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFin ----
            lbFin.setText("Fournisseur fin");
            lbFin.setMaximumSize(new Dimension(175, 30));
            lbFin.setMinimumSize(new Dimension(175, 30));
            lbFin.setPreferredSize(new Dimension(175, 30));
            lbFin.setName("lbFin");
            pnlCritereDeSelection.add(lbFin, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snFournisseurFin ----
            snFournisseurFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFournisseurFin.setPreferredSize(new Dimension(350, 30));
            snFournisseurFin.setMinimumSize(new Dimension(350, 30));
            snFournisseurFin.setMaximumSize(new Dimension(350, 30));
            snFournisseurFin.setName("snFournisseurFin");
            pnlCritereDeSelection.add(snFournisseurFin, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Option d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- REPON1 ----
            REPON1.setText("Lignes d\u00e9tail articles");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setPreferredSize(new Dimension(144, 30));
            REPON1.setMinimumSize(new Dimension(144, 30));
            REPON1.setName("REPON1");
            pnlOptionEdition.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNLabelChamp lbFournisseurDebut;
  private SNFournisseur snFournisseurDebut;
  private SNLabelChamp lbFin;
  private SNFournisseur snFournisseurFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox REPON1;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
