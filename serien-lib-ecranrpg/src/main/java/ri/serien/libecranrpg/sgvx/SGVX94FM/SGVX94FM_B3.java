
package ri.serien.libecranrpg.sgvx.SGVX94FM;

// Nom Fichier: b_SGVX94FM_FMTB3_FMTF1_405.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX94FM_B3 extends SNPanelEcranRPG implements ioFrame {
   
  
  public SGVX94FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    PAT001.setValeursSelection("1", "");
    PAT002.setValeursSelection("1", "");
    PAT003.setValeursSelection("1", "");
    PAT004.setValeursSelection("1", "");
    PAT005.setValeursSelection("1", "");
    PAT006.setValeursSelection("1", "");
    PAT007.setValeursSelection("1", "");
    PAT008.setValeursSelection("1", "");
    PAT009.setValeursSelection("1", "");
    PAT010.setValeursSelection("1", "");
    PAT011.setValeursSelection("1", "");
    PAT012.setValeursSelection("1", "");
    PAT013.setValeursSelection("1", "");
    PAT014.setValeursSelection("1", "");
    PAT015.setValeursSelection("1", "");
    PAT016.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    planification.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SAUV@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    PAT001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS001@ @PAN001@")).trim());
    PAT002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS002@ @PAN002@")).trim());
    PAT003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS003@ @PAN003@")).trim());
    PAT004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS004@ @PAN004@")).trim());
    PAT005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS005@ @PAN005@")).trim());
    PAE001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE001@")).trim());
    PAE002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE002@")).trim());
    PAE003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE003@")).trim());
    PAE004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE004@")).trim());
    PAE005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE005@")).trim());
    PAT006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS006@ @PAN006@")).trim());
    PAT007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS007@ @PAN007@")).trim());
    PAT008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS008@ @PAN008@")).trim());
    PAT009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS009@ @PAN009@")).trim());
    PAT010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS010@ @PAN010@")).trim());
    PAE006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE006@")).trim());
    PAE007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE007@")).trim());
    PAE008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE008@")).trim());
    PAE009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE009@")).trim());
    PAE010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE010@")).trim());
    PAT011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS011@ @PAN011@")).trim());
    PAT012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS012@ @PAN012@")).trim());
    PAT013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS013@ @PAN013@")).trim());
    PAT014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS014@ @PAN014@")).trim());
    PAT015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS015@ @PAN015@")).trim());
    PAE011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE011@")).trim());
    PAE012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE012@")).trim());
    PAE013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE013@")).trim());
    PAE014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE014@")).trim());
    PAE015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE015@")).trim());
    PAE016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE016@")).trim());
    PAT016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS016@ @PAN016@")).trim());
  }
  
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    planification.setVisible(lexique.isTrue("96"));
    panel1.setVisible(!WTETB.isSelected());
    
    PAE001.setVisible(PAT001.isVisible());
    PAE002.setVisible(PAT002.isVisible());
    PAE003.setVisible(PAT003.isVisible());
    PAE004.setVisible(PAT004.isVisible());
    PAE005.setVisible(PAT005.isVisible());
    PAE006.setVisible(PAT006.isVisible());
    PAE007.setVisible(PAT007.isVisible());
    PAE008.setVisible(PAT008.isVisible());
    PAE009.setVisible(PAT009.isVisible());
    PAE010.setVisible(PAT010.isVisible());
    PAE011.setVisible(PAT011.isVisible());
    PAE012.setVisible(PAT012.isVisible());
    PAE013.setVisible(PAT013.isVisible());
    PAE014.setVisible(PAT014.isVisible());
    PAE015.setVisible(PAT015.isVisible());
    PAE016.setVisible(PAT016.isVisible());
    
    // TODO Icones
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTETBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTETB", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER");
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    planification = new JLabel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    PAT001 = new XRiCheckBox();
    PAT002 = new XRiCheckBox();
    PAT003 = new XRiCheckBox();
    PAT004 = new XRiCheckBox();
    PAT005 = new XRiCheckBox();
    PAM001 = new XRiTextField();
    PAM002 = new XRiTextField();
    PAM003 = new XRiTextField();
    PAM004 = new XRiTextField();
    PAM005 = new XRiTextField();
    PAE001 = new RiZoneSortie();
    PAE002 = new RiZoneSortie();
    PAE003 = new RiZoneSortie();
    PAE004 = new RiZoneSortie();
    PAE005 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    PAT006 = new XRiCheckBox();
    PAT007 = new XRiCheckBox();
    PAT008 = new XRiCheckBox();
    PAT009 = new XRiCheckBox();
    PAT010 = new XRiCheckBox();
    PAM006 = new XRiTextField();
    PAM007 = new XRiTextField();
    PAM008 = new XRiTextField();
    PAM009 = new XRiTextField();
    PAM010 = new XRiTextField();
    PAE006 = new RiZoneSortie();
    PAE007 = new RiZoneSortie();
    PAE008 = new RiZoneSortie();
    PAE009 = new RiZoneSortie();
    PAE010 = new RiZoneSortie();
    PAT011 = new XRiCheckBox();
    PAT012 = new XRiCheckBox();
    PAT013 = new XRiCheckBox();
    PAT014 = new XRiCheckBox();
    PAT015 = new XRiCheckBox();
    PAM011 = new XRiTextField();
    PAM012 = new XRiTextField();
    PAM013 = new XRiTextField();
    PAM014 = new XRiTextField();
    PAM015 = new XRiTextField();
    PAE011 = new RiZoneSortie();
    PAE012 = new RiZoneSortie();
    PAE013 = new RiZoneSortie();
    PAE014 = new RiZoneSortie();
    PAE015 = new RiZoneSortie();
    PAE016 = new RiZoneSortie();
    PAT016 = new XRiCheckBox();
    PAM016 = new XRiTextField();
    WTETB = new JButton();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- planification ----
          planification.setText("@SAUV@");
          planification.setFont(planification.getFont().deriveFont(planification.getFont().getStyle() | Font.BOLD));
          planification.setForeground(new Color(255, 0, 51));
          planification.setName("planification");
          p_tete_gauche.add(planification);
          planification.setBounds(5, 5, 674, 20);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 630));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Choix de l'\u00e9tablissement"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- PAT001 ----
            PAT001.setText("@PAS001@ @PAN001@");
            PAT001.setName("PAT001");
            panel1.add(PAT001);
            PAT001.setBounds(20, 40, 321, 27);

            //---- PAT002 ----
            PAT002.setText("@PAS002@ @PAN002@");
            PAT002.setName("PAT002");
            panel1.add(PAT002);
            PAT002.setBounds(20, 70, 321, 27);

            //---- PAT003 ----
            PAT003.setText("@PAS003@ @PAN003@");
            PAT003.setName("PAT003");
            panel1.add(PAT003);
            PAT003.setBounds(20, 100, 321, 27);

            //---- PAT004 ----
            PAT004.setText("@PAS004@ @PAN004@");
            PAT004.setName("PAT004");
            panel1.add(PAT004);
            PAT004.setBounds(20, 130, 321, 27);

            //---- PAT005 ----
            PAT005.setText("@PAS005@ @PAN005@");
            PAT005.setName("PAT005");
            panel1.add(PAT005);
            PAT005.setBounds(20, 160, 321, 27);

            //---- PAM001 ----
            PAM001.setName("PAM001");
            panel1.add(PAM001);
            PAM001.setBounds(360, 40, 60, PAM001.getPreferredSize().height);

            //---- PAM002 ----
            PAM002.setName("PAM002");
            panel1.add(PAM002);
            PAM002.setBounds(360, 70, 60, PAM002.getPreferredSize().height);

            //---- PAM003 ----
            PAM003.setName("PAM003");
            panel1.add(PAM003);
            PAM003.setBounds(360, 100, 60, PAM003.getPreferredSize().height);

            //---- PAM004 ----
            PAM004.setName("PAM004");
            panel1.add(PAM004);
            PAM004.setBounds(360, 130, 60, PAM004.getPreferredSize().height);

            //---- PAM005 ----
            PAM005.setName("PAM005");
            panel1.add(PAM005);
            PAM005.setBounds(360, 160, 60, PAM005.getPreferredSize().height);

            //---- PAE001 ----
            PAE001.setText("@PAE001@");
            PAE001.setName("PAE001");
            panel1.add(PAE001);
            PAE001.setBounds(475, 40, 175, 28);

            //---- PAE002 ----
            PAE002.setText("@PAE002@");
            PAE002.setName("PAE002");
            panel1.add(PAE002);
            PAE002.setBounds(475, 70, 175, 28);

            //---- PAE003 ----
            PAE003.setText("@PAE003@");
            PAE003.setName("PAE003");
            panel1.add(PAE003);
            PAE003.setBounds(475, 100, 175, 28);

            //---- PAE004 ----
            PAE004.setText("@PAE004@");
            PAE004.setName("PAE004");
            panel1.add(PAE004);
            PAE004.setBounds(475, 130, 175, 28);

            //---- PAE005 ----
            PAE005.setText("@PAE005@");
            PAE005.setName("PAE005");
            panel1.add(PAE005);
            PAE005.setBounds(475, 160, 175, 28);

            //---- label1 ----
            label1.setText("Etablissement");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(20, 20, 321, 25);

            //---- label2 ----
            label2.setText("Purge");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(360, 20, 60, 25);

            //---- label3 ----
            label3.setText("En cours");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(475, 20, 175, 25);

            //---- PAT006 ----
            PAT006.setText("@PAS006@ @PAN006@");
            PAT006.setName("PAT006");
            panel1.add(PAT006);
            PAT006.setBounds(20, 190, 321, 27);

            //---- PAT007 ----
            PAT007.setText("@PAS007@ @PAN007@");
            PAT007.setName("PAT007");
            panel1.add(PAT007);
            PAT007.setBounds(20, 220, 321, 27);

            //---- PAT008 ----
            PAT008.setText("@PAS008@ @PAN008@");
            PAT008.setName("PAT008");
            panel1.add(PAT008);
            PAT008.setBounds(20, 250, 321, 27);

            //---- PAT009 ----
            PAT009.setText("@PAS009@ @PAN009@");
            PAT009.setName("PAT009");
            panel1.add(PAT009);
            PAT009.setBounds(20, 280, 321, 27);

            //---- PAT010 ----
            PAT010.setText("@PAS010@ @PAN010@");
            PAT010.setName("PAT010");
            panel1.add(PAT010);
            PAT010.setBounds(20, 310, 321, 27);

            //---- PAM006 ----
            PAM006.setName("PAM006");
            panel1.add(PAM006);
            PAM006.setBounds(360, 190, 60, PAM006.getPreferredSize().height);

            //---- PAM007 ----
            PAM007.setName("PAM007");
            panel1.add(PAM007);
            PAM007.setBounds(360, 220, 60, PAM007.getPreferredSize().height);

            //---- PAM008 ----
            PAM008.setName("PAM008");
            panel1.add(PAM008);
            PAM008.setBounds(360, 250, 60, PAM008.getPreferredSize().height);

            //---- PAM009 ----
            PAM009.setName("PAM009");
            panel1.add(PAM009);
            PAM009.setBounds(360, 280, 60, PAM009.getPreferredSize().height);

            //---- PAM010 ----
            PAM010.setName("PAM010");
            panel1.add(PAM010);
            PAM010.setBounds(360, 310, 60, PAM010.getPreferredSize().height);

            //---- PAE006 ----
            PAE006.setText("@PAE006@");
            PAE006.setName("PAE006");
            panel1.add(PAE006);
            PAE006.setBounds(475, 190, 175, 28);

            //---- PAE007 ----
            PAE007.setText("@PAE007@");
            PAE007.setName("PAE007");
            panel1.add(PAE007);
            PAE007.setBounds(475, 220, 175, 28);

            //---- PAE008 ----
            PAE008.setText("@PAE008@");
            PAE008.setName("PAE008");
            panel1.add(PAE008);
            PAE008.setBounds(475, 250, 175, 28);

            //---- PAE009 ----
            PAE009.setText("@PAE009@");
            PAE009.setName("PAE009");
            panel1.add(PAE009);
            PAE009.setBounds(475, 280, 175, 28);

            //---- PAE010 ----
            PAE010.setText("@PAE010@");
            PAE010.setName("PAE010");
            panel1.add(PAE010);
            PAE010.setBounds(475, 310, 175, 28);

            //---- PAT011 ----
            PAT011.setText("@PAS011@ @PAN011@");
            PAT011.setName("PAT011");
            panel1.add(PAT011);
            PAT011.setBounds(20, 340, 321, 27);

            //---- PAT012 ----
            PAT012.setText("@PAS012@ @PAN012@");
            PAT012.setName("PAT012");
            panel1.add(PAT012);
            PAT012.setBounds(20, 370, 321, 27);

            //---- PAT013 ----
            PAT013.setText("@PAS013@ @PAN013@");
            PAT013.setName("PAT013");
            panel1.add(PAT013);
            PAT013.setBounds(20, 400, 321, 27);

            //---- PAT014 ----
            PAT014.setText("@PAS014@ @PAN014@");
            PAT014.setName("PAT014");
            panel1.add(PAT014);
            PAT014.setBounds(20, 430, 321, 27);

            //---- PAT015 ----
            PAT015.setText("@PAS015@ @PAN015@");
            PAT015.setName("PAT015");
            panel1.add(PAT015);
            PAT015.setBounds(20, 460, 321, 27);

            //---- PAM011 ----
            PAM011.setName("PAM011");
            panel1.add(PAM011);
            PAM011.setBounds(360, 340, 60, PAM011.getPreferredSize().height);

            //---- PAM012 ----
            PAM012.setName("PAM012");
            panel1.add(PAM012);
            PAM012.setBounds(360, 370, 60, PAM012.getPreferredSize().height);

            //---- PAM013 ----
            PAM013.setName("PAM013");
            panel1.add(PAM013);
            PAM013.setBounds(360, 400, 60, PAM013.getPreferredSize().height);

            //---- PAM014 ----
            PAM014.setName("PAM014");
            panel1.add(PAM014);
            PAM014.setBounds(360, 430, 60, PAM014.getPreferredSize().height);

            //---- PAM015 ----
            PAM015.setName("PAM015");
            panel1.add(PAM015);
            PAM015.setBounds(360, 460, 60, PAM015.getPreferredSize().height);

            //---- PAE011 ----
            PAE011.setText("@PAE011@");
            PAE011.setName("PAE011");
            panel1.add(PAE011);
            PAE011.setBounds(475, 340, 175, 28);

            //---- PAE012 ----
            PAE012.setText("@PAE012@");
            PAE012.setName("PAE012");
            panel1.add(PAE012);
            PAE012.setBounds(475, 370, 175, 28);

            //---- PAE013 ----
            PAE013.setText("@PAE013@");
            PAE013.setName("PAE013");
            panel1.add(PAE013);
            PAE013.setBounds(475, 400, 175, 28);

            //---- PAE014 ----
            PAE014.setText("@PAE014@");
            PAE014.setName("PAE014");
            panel1.add(PAE014);
            PAE014.setBounds(475, 430, 175, 28);

            //---- PAE015 ----
            PAE015.setText("@PAE015@");
            PAE015.setName("PAE015");
            panel1.add(PAE015);
            PAE015.setBounds(475, 460, 175, 28);

            //---- PAE016 ----
            PAE016.setText("@PAE016@");
            PAE016.setName("PAE016");
            panel1.add(PAE016);
            PAE016.setBounds(475, 490, 175, 28);

            //---- PAT016 ----
            PAT016.setText("@PAS016@ @PAN016@");
            PAT016.setName("PAT016");
            panel1.add(PAT016);
            PAT016.setBounds(20, 490, 321, 27);

            //---- PAM016 ----
            PAM016.setName("PAM016");
            panel1.add(PAM016);
            PAM016.setBounds(360, 490, 60, PAM016.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- WTETB ----
          WTETB.setText("S\u00e9lection de toutes les soci\u00e9t\u00e9s");
          WTETB.setName("WTETB");
          WTETB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTETBActionPerformed(e);
            }
          });

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 674, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(WTETB, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 534, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(WTETB)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
        // //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JPopupMenu BTD;
    private JMenuItem OBJ_11;
    private JMenuItem OBJ_12;
    private JPanel p_nord;
    private SNBandeauTitre p_bpresentation;
    private JMenuBar barre_tete;
    private JPanel p_tete_gauche;
    private JLabel planification;
    private JPanel p_tete_droite;
    private JLabel lb_loctp_;
    private JPanel p_sud;
    private JPanel p_menus;
    private JPanel menus_bas;
    private RiMenu navig_erreurs;
    private RiMenu_bt bouton_erreurs;
    private RiMenu navig_valid;
    private RiMenu_bt bouton_valider;
    private RiMenu navig_retour;
    private RiMenu_bt bouton_retour;
    private JScrollPane scroll_droite;
    private JPanel menus_haut;
    private RiMenu riMenu2;
    private RiMenu_bt riMenu_bt2;
    private RiSousMenu riSousMenu6;
    private RiSousMenu_bt riSousMenu_bt_export;
    private SNPanelDegradeGris p_centrage;
    private JPanel p_contenu;
    private JPanel panel1;
    private XRiCheckBox PAT001;
    private XRiCheckBox PAT002;
    private XRiCheckBox PAT003;
    private XRiCheckBox PAT004;
    private XRiCheckBox PAT005;
    private XRiTextField PAM001;
    private XRiTextField PAM002;
    private XRiTextField PAM003;
    private XRiTextField PAM004;
    private XRiTextField PAM005;
    private RiZoneSortie PAE001;
    private RiZoneSortie PAE002;
    private RiZoneSortie PAE003;
    private RiZoneSortie PAE004;
    private RiZoneSortie PAE005;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private XRiCheckBox PAT006;
    private XRiCheckBox PAT007;
    private XRiCheckBox PAT008;
    private XRiCheckBox PAT009;
    private XRiCheckBox PAT010;
    private XRiTextField PAM006;
    private XRiTextField PAM007;
    private XRiTextField PAM008;
    private XRiTextField PAM009;
    private XRiTextField PAM010;
    private RiZoneSortie PAE006;
    private RiZoneSortie PAE007;
    private RiZoneSortie PAE008;
    private RiZoneSortie PAE009;
    private RiZoneSortie PAE010;
    private XRiCheckBox PAT011;
    private XRiCheckBox PAT012;
    private XRiCheckBox PAT013;
    private XRiCheckBox PAT014;
    private XRiCheckBox PAT015;
    private XRiTextField PAM011;
    private XRiTextField PAM012;
    private XRiTextField PAM013;
    private XRiTextField PAM014;
    private XRiTextField PAM015;
    private RiZoneSortie PAE011;
    private RiZoneSortie PAE012;
    private RiZoneSortie PAE013;
    private RiZoneSortie PAE014;
    private RiZoneSortie PAE015;
    private RiZoneSortie PAE016;
    private XRiCheckBox PAT016;
    private XRiTextField PAM016;
    private JButton WTETB;
    // JFormDesigner - End of variables declaration //GEN-END:variables
}
