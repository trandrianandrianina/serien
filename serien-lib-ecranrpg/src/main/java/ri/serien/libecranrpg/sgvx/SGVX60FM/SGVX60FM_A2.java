
package ri.serien.libecranrpg.sgvx.SGVX60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGVX60FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top= null;
  private String[] _MOI1_Title = { "Mois", "Entrées", "Sorties", };
  private String[][] _MOI1_Data =
      { { "MOI1", "E1X01", "S1X01", }, { "MOI2", "E1X02", "S1X02", }, { "MOI3", "E1X03", "S1X03", }, { "MOI4", "E1X04", "S1X04", },
          { "MOI5", "E1X05", "S1X05", }, { "MOI6", "E1X06", "S1X06", }, { "MOI7", "E1X07", "S1X07", }, { "MOI8", "E1X08", "S1X08", },
          { "MOI9", "E1X09", "S1X09", }, { "MOI10", "E1X10", "S1X10", }, { "MOI11", "E1X11", "S1X11", }, { "MOI12", "E1X12", "S1X12", }, };
  private int[] _MOI1_Width = { 50, 79, 80, };
  
  public SGVX60FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    setTitle("Historique des 12 derniers mois");
    
    // Ajout
    initDiverses();
    MOI1.setAspectTable(null, _MOI1_Title, _MOI1_Data, _MOI1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB1@")).trim());
    MOYSJ.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOYSJ@")).trim());
    TOTS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTS@")).trim());
    MOYSM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOYSM@")).trim());
    MOYEJ.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOYEJ@")).trim());
    MOYEM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOYEM@")).trim());
    TOTE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    WMAG.setEnabled(lexique.isPresent("WMAG"));
    TOTE.setVisible(lexique.isPresent("TOTE"));
    MOYEM.setVisible(lexique.isPresent("MOYEM"));
    MOYEJ.setVisible(lexique.isPresent("MOYEJ"));
    MOYSM.setVisible(lexique.isPresent("MOYSM"));
    TOTS.setVisible(lexique.isPresent("TOTS"));
    MOYSJ.setVisible(lexique.isPresent("MOYSJ"));
    OBJ_36.setVisible(lexique.isPresent("MALIB1"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    MOI1 = new XRiTable();
    OBJ_36 = new RiZoneSortie();
    MOYSJ = new RiZoneSortie();
    TOTS = new RiZoneSortie();
    MOYSM = new RiZoneSortie();
    MOYEJ = new RiZoneSortie();
    MOYEM = new RiZoneSortie();
    TOTE = new RiZoneSortie();
    OBJ_17 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_26 = new JLabel();
    WMAG = new XRiTextField();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(530, 440));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Tous les magasins");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Historique des 12 derniers mois"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

            //---- MOI1 ----
            MOI1.setName("MOI1");
            SCROLLPANE_LIST2.setViewportView(MOI1);
          }
          panel2.add(SCROLLPANE_LIST2);
          SCROLLPANE_LIST2.setBounds(20, 70, 285, 225);

          //---- OBJ_36 ----
          OBJ_36.setText("@MALIB1@");
          OBJ_36.setName("OBJ_36");
          panel2.add(OBJ_36);
          OBJ_36.setBounds(120, 37, 185, OBJ_36.getPreferredSize().height);

          //---- MOYSJ ----
          MOYSJ.setComponentPopupMenu(BTD);
          MOYSJ.setHorizontalAlignment(SwingConstants.RIGHT);
          MOYSJ.setText("@MOYSJ@");
          MOYSJ.setName("MOYSJ");
          panel2.add(MOYSJ);
          MOYSJ.setBounds(205, 335, 98, MOYSJ.getPreferredSize().height);

          //---- TOTS ----
          TOTS.setComponentPopupMenu(BTD);
          TOTS.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTS.setText("@TOTS@");
          TOTS.setName("TOTS");
          panel2.add(TOTS);
          TOTS.setBounds(205, 305, 98, TOTS.getPreferredSize().height);

          //---- MOYSM ----
          MOYSM.setComponentPopupMenu(BTD);
          MOYSM.setHorizontalAlignment(SwingConstants.RIGHT);
          MOYSM.setText("@MOYSM@");
          MOYSM.setName("MOYSM");
          panel2.add(MOYSM);
          MOYSM.setBounds(205, 365, 98, MOYSM.getPreferredSize().height);

          //---- MOYEJ ----
          MOYEJ.setComponentPopupMenu(BTD);
          MOYEJ.setHorizontalAlignment(SwingConstants.RIGHT);
          MOYEJ.setText("@MOYEJ@");
          MOYEJ.setName("MOYEJ");
          panel2.add(MOYEJ);
          MOYEJ.setBounds(100, 335, 98, MOYEJ.getPreferredSize().height);

          //---- MOYEM ----
          MOYEM.setComponentPopupMenu(BTD);
          MOYEM.setHorizontalAlignment(SwingConstants.RIGHT);
          MOYEM.setText("@MOYEM@");
          MOYEM.setName("MOYEM");
          panel2.add(MOYEM);
          MOYEM.setBounds(100, 365, 98, MOYEM.getPreferredSize().height);

          //---- TOTE ----
          TOTE.setComponentPopupMenu(BTD);
          TOTE.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTE.setText("@TOTE@");
          TOTE.setName("TOTE");
          panel2.add(TOTE);
          TOTE.setBounds(100, 305, 98, TOTE.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("Magasin");
          OBJ_17.setName("OBJ_17");
          panel2.add(OBJ_17);
          OBJ_17.setBounds(20, 39, 60, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("Moy / mois");
          OBJ_28.setName("OBJ_28");
          panel2.add(OBJ_28);
          OBJ_28.setBounds(25, 367, 70, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("Moy / jour");
          OBJ_27.setName("OBJ_27");
          panel2.add(OBJ_27);
          OBJ_27.setBounds(25, 337, 70, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Total");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(25, 307, 70, 20);

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setNextFocusableComponent(bouton_valider);
          WMAG.setName("WMAG");
          panel2.add(WMAG);
          WMAG.setBounds(80, 35, 34, WMAG.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable MOI1;
  private RiZoneSortie OBJ_36;
  private RiZoneSortie MOYSJ;
  private RiZoneSortie TOTS;
  private RiZoneSortie MOYSM;
  private RiZoneSortie MOYEJ;
  private RiZoneSortie MOYEM;
  private RiZoneSortie TOTE;
  private JLabel OBJ_17;
  private JLabel OBJ_28;
  private JLabel OBJ_27;
  private JLabel OBJ_26;
  private XRiTextField WMAG;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
