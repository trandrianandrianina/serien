
package ri.serien.libecranrpg.sgvx.SGVXDVFM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SGVXDVFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public SGVXDVFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CONF.setValeurs("OUI", CONF_GRP);
    CONF_NON.setValeurs("NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbTitre.setText(
        lexique.TranslationTable(interpreteurD.analyseExpression("Vous avez demandé le dévérouillage du bordereau @IBON@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    lbTitre.setMessage(Message.getMessageMoyen("Vous avez demandé le dévérouillage du bordereau " + lexique.HostFieldGetData("IBON")));
    lbAvertissement.setMessage(Message.getMessageImportant("Attention : Ceci est une opération grave"));
    lbDemande1.setMessage(Message.getMessageNormal("Ce bordereau est peut-être en cours de traitement sur un autre écran"));
    lbDemande2
        .setMessage(Message.getMessageNormal("(si tel est le cas abandonnez " + "l'opération ou contactez votre assistance Série M)"));
    lbConfirmer.setMessage(Message.getMessageMoyen("Confirmez vous cette demande ?"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Déverrouillage"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new SNPanelEcranRPG();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbTitre = new SNMessage();
    lbAvertissement = new SNMessage();
    lbDemande1 = new SNMessage();
    lbDemande2 = new SNMessage();
    pnlReponse = new SNPanel();
    lbConfirmer = new SNLabelChamp();
    CONF = new XRiRadioButton();
    CONF_NON = new XRiRadioButton();
    CONF_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(710, 280));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      p_principal.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbTitre ----
        lbTitre.setText("Vous avez demand\u00e9 le d\u00e9v\u00e9rouillage du bordereau @IBON@");
        lbTitre.setMaximumSize(new Dimension(250, 30));
        lbTitre.setMinimumSize(new Dimension(350, 30));
        lbTitre.setPreferredSize(new Dimension(400, 30));
        lbTitre.setName("lbTitre");
        pnlContenu.add(lbTitre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbAvertissement ----
        lbAvertissement.setText("Attention : Ceci est une op\u00e9ration grave");
        lbAvertissement.setMinimumSize(new Dimension(400, 30));
        lbAvertissement.setMaximumSize(new Dimension(400, 30));
        lbAvertissement.setPreferredSize(new Dimension(400, 30));
        lbAvertissement.setName("lbAvertissement");
        pnlContenu.add(lbAvertissement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDemande1 ----
        lbDemande1.setText("ce bordereau est peut-\u00eatre en cours de traitement sur un autre \u00e9cran");
        lbDemande1.setName("lbDemande1");
        pnlContenu.add(lbDemande1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDemande2 ----
        lbDemande2.setText("(si tel est le cas abandonnez l'op\u00e9ration ou contactez votre assistance S\u00e9rie M)");
        lbDemande2.setName("lbDemande2");
        pnlContenu.add(lbDemande2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlReponse ========
        {
          pnlReponse.setName("pnlReponse");
          pnlReponse.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlReponse.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlReponse.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlReponse.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlReponse.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbConfirmer ----
          lbConfirmer.setText("Confirmez vous cette demande ?");
          lbConfirmer.setPreferredSize(new Dimension(300, 30));
          lbConfirmer.setMinimumSize(new Dimension(300, 30));
          lbConfirmer.setMaximumSize(new Dimension(300, 30));
          lbConfirmer.setName("lbConfirmer");
          pnlReponse.add(lbConfirmer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CONF ----
          CONF.setText("Oui");
          CONF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CONF.setFont(new Font("sansserif", Font.PLAIN, 14));
          CONF.setPreferredSize(new Dimension(60, 30));
          CONF.setMinimumSize(new Dimension(60, 30));
          CONF.setMaximumSize(new Dimension(60, 30));
          CONF.setName("CONF");
          pnlReponse.add(CONF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CONF_NON ----
          CONF_NON.setText("Non");
          CONF_NON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CONF_NON.setFont(new Font("sansserif", Font.PLAIN, 14));
          CONF_NON.setPreferredSize(new Dimension(60, 30));
          CONF_NON.setMinimumSize(new Dimension(60, 30));
          CONF_NON.setMaximumSize(new Dimension(60, 30));
          CONF_NON.setName("CONF_NON");
          pnlReponse.add(CONF_NON, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlReponse, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_principal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ---- CONF_GRP ----
    CONF_GRP.add(CONF);
    CONF_GRP.add(CONF_NON);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelEcranRPG p_principal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNMessage lbTitre;
  private SNMessage lbAvertissement;
  private SNMessage lbDemande1;
  private SNMessage lbDemande2;
  private SNPanel pnlReponse;
  private SNLabelChamp lbConfirmer;
  private XRiRadioButton CONF;
  private XRiRadioButton CONF_NON;
  private ButtonGroup CONF_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
