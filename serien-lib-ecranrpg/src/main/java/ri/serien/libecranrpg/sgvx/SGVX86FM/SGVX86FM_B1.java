
package ri.serien.libecranrpg.sgvx.SGVX86FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX86FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] WLC_Value = { "", "1", "2", "0", };
  private String[] WSX_Value = { "", "1", "2", "0", };
  private String[] WSM_Value = { "", "1", "2", "0", };
  
  public SGVX86FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WLC.setValeurs(WLC_Value, null);
    WSX.setValeurs(WSX_Value, null);
    WSM.setValeurs(WSM_Value, null);
    WCD.setValeursSelection("X", " ");
    WCC.setValeursSelection("X", " ");
    WCB.setValeursSelection("X", " ");
    WCA.setValeursSelection("X", " ");
    WCN.setValeursSelection("X", " ");
    WTOUFR.setValeursSelection("**", "  ");
    WTOUA.setValeursSelection("**", "  ");
    WTOUFA.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // WCD.setEnabled( lexique.isPresent("WCD"));
    // WCD.setSelected(lexique.HostFieldGetData("WCD").equalsIgnoreCase("X"));
    // WCC.setEnabled( lexique.isPresent("WCC"));
    // WCC.setSelected(lexique.HostFieldGetData("WCC").equalsIgnoreCase("X"));
    // WCB.setEnabled( lexique.isPresent("WCB"));
    // WCB.setSelected(lexique.HostFieldGetData("WCB").equalsIgnoreCase("X"));
    // WCA.setEnabled( lexique.isPresent("WCA"));
    // WCA.setSelected(lexique.HostFieldGetData("WCA").equalsIgnoreCase("X"));
    // WCN.setEnabled( lexique.isPresent("WCN"));
    // WCN.setSelected(lexique.HostFieldGetData("WCN").equalsIgnoreCase("X"));
    
    MA12.setEnabled(lexique.isPresent("MA12"));
    MA11.setEnabled(lexique.isPresent("MA11"));
    MA10.setEnabled(lexique.isPresent("MA10"));
    MA09.setEnabled(lexique.isPresent("MA09"));
    MA08.setEnabled(lexique.isPresent("MA08"));
    MA07.setEnabled(lexique.isPresent("MA07"));
    MA06.setEnabled(lexique.isPresent("MA06"));
    MA05.setEnabled(lexique.isPresent("MA05"));
    MA04.setEnabled(lexique.isPresent("MA04"));
    MA03.setEnabled(lexique.isPresent("MA03"));
    MA02.setEnabled(lexique.isPresent("MA02"));
    MA01.setEnabled(lexique.isPresent("MA01"));
    
    WLCSC.setEnabled(lexique.isPresent("WLCSC"));
    WSXSC.setEnabled(lexique.isPresent("WSXSC"));
    WSMSC.setEnabled(lexique.isPresent("WSMSC"));
    WLCSA.setEnabled(lexique.isPresent("WLCSA"));
    WSXSA.setEnabled(lexique.isPresent("WSXSA"));
    WSMSA.setEnabled(lexique.isPresent("WSMSA"));
    
    FRFIN.setEnabled(lexique.isPresent("FRFIN"));
    FRDEB.setEnabled(lexique.isPresent("FRDEB"));
    EFIN.setEnabled(lexique.isPresent("EFIN"));
    EDEB.setEnabled(lexique.isPresent("EDEB"));
    // WTOUFR.setEnabled( lexique.isPresent("WTOUFR"));
    // WTOUFR.setSelected(lexique.HostFieldGetData("WTOUFR").equalsIgnoreCase("**"));
    // WTOUA.setEnabled( lexique.isPresent("WTOUA"));
    // WTOUA.setSelected(lexique.HostFieldGetData("WTOUA").equalsIgnoreCase("**"));
    // WTOUFA.setEnabled( lexique.isPresent("WTOUFA"));
    // WTOUFA.setSelected(lexique.HostFieldGetData("WTOUFA").equalsIgnoreCase("**"));
    // WTOUM.setEnabled( lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    EFINA.setEnabled(lexique.isPresent("EFINA"));
    EDEBA.setEnabled(lexique.isPresent("EDEBA"));
    
    P_SEL0.setVisible(!WTOUFR.isSelected());
    P_SEL2.setVisible(!WTOUFA.isSelected());
    P_SEL1.setVisible(!WTOUM.isSelected());
    P_SEL3.setVisible(!WTOUA.isSelected());
    
    // WLC.setEnabled( lexique.isPresent("WLC"));
    // WSX.setEnabled( lexique.isPresent("WSX"));
    // WSM.setEnabled( lexique.isPresent("WSM"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WCD.isSelected())
    // lexique.HostFieldPutData("WCD", 0, "X");
    // else
    // lexique.HostFieldPutData("WCD", 0, " ");
    // if (WCC.isSelected())
    // lexique.HostFieldPutData("WCC", 0, "X");
    // else
    // lexique.HostFieldPutData("WCC", 0, " ");
    // if (WCB.isSelected())
    // lexique.HostFieldPutData("WCB", 0, "X");
    // else
    // lexique.HostFieldPutData("WCB", 0, " ");
    // if (WCA.isSelected())
    // lexique.HostFieldPutData("WCA", 0, "X");
    // else
    // lexique.HostFieldPutData("WCA", 0, " ");
    // if (WCN.isSelected())
    // lexique.HostFieldPutData("WCN", 0, "X");
    // else
    // lexique.HostFieldPutData("WCN", 0, " ");
    // if (WTOUFR.isSelected())
    // lexique.HostFieldPutData("WTOUFR", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUFR", 0, " ");
    // if (WTOUA.isSelected())
    // lexique.HostFieldPutData("WTOUA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUA", 0, " ");
    // if (WTOUFA.isSelected())
    // lexique.HostFieldPutData("WTOUFA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUFA", 0, " ");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // lexique.HostFieldPutData("WLC", 0, WLC_Value[WLC.getSelectedIndex()]);
    // lexique.HostFieldPutData("WSX", 0, WSX_Value[WSX.getSelectedIndex()]);
    // lexique.HostFieldPutData("WSM", 0, WSM_Value[WSM.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOUFAActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOUAActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
  }
  
  private void WTOUFRActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_42 = new JXTitledSeparator();
    OBJ_71 = new JXTitledSeparator();
    OBJ_63 = new JXTitledSeparator();
    OBJ_47 = new JXTitledSeparator();
    OBJ_38 = new JXTitledSeparator();
    P_SEL3 = new JPanel();
    EDEBA = new XRiTextField();
    EFINA = new XRiTextField();
    OBJ_73 = new JLabel();
    OBJ_77 = new JLabel();
    WSM = new XRiComboBox();
    WSX = new XRiComboBox();
    WLC = new XRiComboBox();
    P_SEL1 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    P_SEL2 = new JPanel();
    OBJ_67 = new JLabel();
    OBJ_69 = new JLabel();
    EDEB = new XRiTextField();
    EFIN = new XRiTextField();
    OBJ_41 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_96 = new JLabel();
    FRDEB = new XRiTextField();
    FRFIN = new XRiTextField();
    OBJ_97 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_106 = new JLabel();
    WTOUM = new XRiCheckBox();
    WTOUFA = new XRiCheckBox();
    WTOUA = new XRiCheckBox();
    WTOUFR = new XRiCheckBox();
    OBJ_104 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_102 = new JLabel();
    WSMSA = new XRiTextField();
    WSXSA = new XRiTextField();
    WLCSA = new XRiTextField();
    WSMSC = new XRiTextField();
    WSXSC = new XRiTextField();
    WLCSC = new XRiTextField();
    WCN = new XRiCheckBox();
    WCA = new XRiCheckBox();
    WCB = new XRiCheckBox();
    WCC = new XRiCheckBox();
    WCD = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Mise \u00e0 jour des stocks minimums");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(755, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_42 ----
          OBJ_42.setTitle("");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_71 ----
          OBJ_71.setTitle("Plage d'articles");
          OBJ_71.setName("OBJ_71");

          //---- OBJ_63 ----
          OBJ_63.setTitle("Plage de familles");
          OBJ_63.setName("OBJ_63");

          //---- OBJ_47 ----
          OBJ_47.setTitle("Plage de magasins");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_38 ----
          OBJ_38.setTitle("Fournisseur principal");
          OBJ_38.setName("OBJ_38");

          //======== P_SEL3 ========
          {
            P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL3.setOpaque(false);
            P_SEL3.setName("P_SEL3");
            P_SEL3.setLayout(null);

            //---- EDEBA ----
            EDEBA.setComponentPopupMenu(BTD);
            EDEBA.setName("EDEBA");
            P_SEL3.add(EDEBA);
            EDEBA.setBounds(190, 10, 214, EDEBA.getPreferredSize().height);

            //---- EFINA ----
            EFINA.setComponentPopupMenu(BTD);
            EFINA.setName("EFINA");
            P_SEL3.add(EFINA);
            EFINA.setBounds(190, 40, 214, EFINA.getPreferredSize().height);

            //---- OBJ_73 ----
            OBJ_73.setText("Code de d\u00e9but");
            OBJ_73.setName("OBJ_73");
            P_SEL3.add(OBJ_73);
            OBJ_73.setBounds(15, 14, 93, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Code de fin");
            OBJ_77.setName("OBJ_77");
            P_SEL3.add(OBJ_77);
            OBJ_77.setBounds(15, 44, 73, 20);
          }

          //---- WSM ----
          WSM.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Maj nb de semaines",
            "Mise \u00e0 jour saisie",
            "Remise \u00e0 z\u00e9ro"
          }));
          WSM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSM.setName("WSM");

          //---- WSX ----
          WSX.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Maj nb de semaines",
            "Mise \u00e0 jour saisie",
            "Remise \u00e0 z\u00e9ro"
          }));
          WSX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSX.setName("WSX");

          //---- WLC ----
          WLC.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Maj nb de semaines",
            "Mise \u00e0 jour saisie",
            "Remise \u00e0 z\u00e9ro"
          }));
          WLC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WLC.setName("WLC");

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- MA01 ----
            MA01.setComponentPopupMenu(BTD);
            MA01.setName("MA01");
            P_SEL1.add(MA01);
            MA01.setBounds(6, 10, 34, MA01.getPreferredSize().height);

            //---- MA02 ----
            MA02.setComponentPopupMenu(BTD);
            MA02.setName("MA02");
            P_SEL1.add(MA02);
            MA02.setBounds(42, 10, 34, MA02.getPreferredSize().height);

            //---- MA03 ----
            MA03.setComponentPopupMenu(BTD);
            MA03.setName("MA03");
            P_SEL1.add(MA03);
            MA03.setBounds(78, 10, 34, MA03.getPreferredSize().height);

            //---- MA04 ----
            MA04.setComponentPopupMenu(BTD);
            MA04.setName("MA04");
            P_SEL1.add(MA04);
            MA04.setBounds(114, 10, 34, MA04.getPreferredSize().height);

            //---- MA05 ----
            MA05.setComponentPopupMenu(BTD);
            MA05.setName("MA05");
            P_SEL1.add(MA05);
            MA05.setBounds(150, 10, 34, MA05.getPreferredSize().height);

            //---- MA06 ----
            MA06.setComponentPopupMenu(BTD);
            MA06.setName("MA06");
            P_SEL1.add(MA06);
            MA06.setBounds(186, 10, 34, MA06.getPreferredSize().height);

            //---- MA07 ----
            MA07.setComponentPopupMenu(BTD);
            MA07.setName("MA07");
            P_SEL1.add(MA07);
            MA07.setBounds(222, 10, 34, MA07.getPreferredSize().height);

            //---- MA08 ----
            MA08.setComponentPopupMenu(BTD);
            MA08.setName("MA08");
            P_SEL1.add(MA08);
            MA08.setBounds(258, 10, 34, MA08.getPreferredSize().height);

            //---- MA09 ----
            MA09.setComponentPopupMenu(BTD);
            MA09.setName("MA09");
            P_SEL1.add(MA09);
            MA09.setBounds(294, 10, 34, MA09.getPreferredSize().height);

            //---- MA10 ----
            MA10.setComponentPopupMenu(BTD);
            MA10.setName("MA10");
            P_SEL1.add(MA10);
            MA10.setBounds(330, 10, 34, MA10.getPreferredSize().height);

            //---- MA11 ----
            MA11.setComponentPopupMenu(BTD);
            MA11.setName("MA11");
            P_SEL1.add(MA11);
            MA11.setBounds(366, 10, 34, MA11.getPreferredSize().height);

            //---- MA12 ----
            MA12.setComponentPopupMenu(BTD);
            MA12.setName("MA12");
            P_SEL1.add(MA12);
            MA12.setBounds(402, 10, 34, MA12.getPreferredSize().height);
          }

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- OBJ_67 ----
            OBJ_67.setText("Code de d\u00e9but");
            OBJ_67.setName("OBJ_67");
            P_SEL2.add(OBJ_67);
            OBJ_67.setBounds(15, 14, 88, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("Code de fin");
            OBJ_69.setName("OBJ_69");
            P_SEL2.add(OBJ_69);
            OBJ_69.setBounds(250, 14, 67, 20);

            //---- EDEB ----
            EDEB.setComponentPopupMenu(BTD);
            EDEB.setName("EDEB");
            P_SEL2.add(EDEB);
            EDEB.setBounds(150, 10, 44, EDEB.getPreferredSize().height);

            //---- EFIN ----
            EFIN.setComponentPopupMenu(BTD);
            EFIN.setName("EFIN");
            P_SEL2.add(EFIN);
            EFIN.setBounds(360, 10, 44, EFIN.getPreferredSize().height);
          }

          //---- OBJ_41 ----
          OBJ_41.setTitle("Classes \u00e0 traiter");
          OBJ_41.setName("OBJ_41");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_96 ----
            OBJ_96.setText("N\u00b0 de d\u00e9but");
            OBJ_96.setName("OBJ_96");
            P_SEL0.add(OBJ_96);
            OBJ_96.setBounds(15, 11, 75, 20);

            //---- FRDEB ----
            FRDEB.setComponentPopupMenu(BTD);
            FRDEB.setName("FRDEB");
            P_SEL0.add(FRDEB);
            FRDEB.setBounds(90, 7, 68, FRDEB.getPreferredSize().height);

            //---- FRFIN ----
            FRFIN.setComponentPopupMenu(BTD);
            FRFIN.setName("FRFIN");
            P_SEL0.add(FRFIN);
            FRFIN.setBounds(230, 7, 68, FRFIN.getPreferredSize().height);

            //---- OBJ_97 ----
            OBJ_97.setText("N\u00b0 de fin");
            OBJ_97.setName("OBJ_97");
            P_SEL0.add(OBJ_97);
            OBJ_97.setBounds(175, 11, 55, 20);
          }

          //---- OBJ_107 ----
          OBJ_107.setText("Nbr de semaines de conso");
          OBJ_107.setName("OBJ_107");

          //---- OBJ_106 ----
          OBJ_106.setText("Nbr semaines d'analyse");
          OBJ_106.setName("OBJ_106");

          //---- WTOUM ----
          WTOUM.setText("S\u00e9lection compl\u00e8te");
          WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUM.setName("WTOUM");
          WTOUM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUMActionPerformed(e);
            }
          });

          //---- WTOUFA ----
          WTOUFA.setText("S\u00e9lection compl\u00e8te");
          WTOUFA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUFA.setName("WTOUFA");
          WTOUFA.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUFAActionPerformed(e);
            }
          });

          //---- WTOUA ----
          WTOUA.setText("S\u00e9lection compl\u00e8te");
          WTOUA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUA.setName("WTOUA");
          WTOUA.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUAActionPerformed(e);
            }
          });

          //---- WTOUFR ----
          WTOUFR.setText("S\u00e9lection compl\u00e8te");
          WTOUFR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUFR.setName("WTOUFR");
          WTOUFR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUFRActionPerformed(e);
            }
          });

          //---- OBJ_104 ----
          OBJ_104.setText("Lot de commande");
          OBJ_104.setName("OBJ_104");

          //---- OBJ_103 ----
          OBJ_103.setText("Stock maxi");
          OBJ_103.setName("OBJ_103");

          //---- OBJ_102 ----
          OBJ_102.setText("Stock mini");
          OBJ_102.setName("OBJ_102");

          //---- WSMSA ----
          WSMSA.setComponentPopupMenu(BTD);
          WSMSA.setName("WSMSA");

          //---- WSXSA ----
          WSXSA.setComponentPopupMenu(BTD);
          WSXSA.setName("WSXSA");

          //---- WLCSA ----
          WLCSA.setComponentPopupMenu(BTD);
          WLCSA.setName("WLCSA");

          //---- WSMSC ----
          WSMSC.setComponentPopupMenu(BTD);
          WSMSC.setName("WSMSC");

          //---- WSXSC ----
          WSXSC.setComponentPopupMenu(BTD);
          WSXSC.setName("WSXSC");

          //---- WLCSC ----
          WLCSC.setComponentPopupMenu(BTD);
          WLCSC.setName("WLCSC");

          //---- WCN ----
          WCN.setText("N");
          WCN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCN.setName("WCN");

          //---- WCA ----
          WCA.setText("A");
          WCA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCA.setName("WCA");

          //---- WCB ----
          WCB.setText("B");
          WCB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCB.setName("WCB");

          //---- WCC ----
          WCC.setText("C");
          WCC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCC.setName("WCC");

          //---- WCD ----
          WCD.setText("D");
          WCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WCD.setName("WCD");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUFA, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUA, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUFR, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 308, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(WCN, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(WCA, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(WCB, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(WCC, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(WCD, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(85, 85, 85)
                .addComponent(WSM, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_106, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(WSMSA, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(WSMSC, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_103, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(85, 85, 85)
                .addComponent(WSX, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(WSXSA, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                .addGap(147, 147, 147)
                .addComponent(WSXSC, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(WLC, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(WLCSA, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                .addGap(147, 147, 147)
                .addComponent(WLCSC, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOUFA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(27, 27, 27)
                    .addComponent(WTOUA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(WTOUFR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(WCN, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WCA, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WCB, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WCC, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WCD, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))))
                .addGap(11, 11, 11)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(OBJ_102))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WSM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_106, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(WSMSA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(WSMSC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_103))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(WSX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WSXSA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WSXSC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_104))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(WLC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLCSA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WLCSC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_71;
  private JXTitledSeparator OBJ_63;
  private JXTitledSeparator OBJ_47;
  private JXTitledSeparator OBJ_38;
  private JPanel P_SEL3;
  private XRiTextField EDEBA;
  private XRiTextField EFINA;
  private JLabel OBJ_73;
  private JLabel OBJ_77;
  private XRiComboBox WSM;
  private XRiComboBox WSX;
  private XRiComboBox WLC;
  private JPanel P_SEL1;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPanel P_SEL2;
  private JLabel OBJ_67;
  private JLabel OBJ_69;
  private XRiTextField EDEB;
  private XRiTextField EFIN;
  private JXTitledSeparator OBJ_41;
  private JPanel P_SEL0;
  private JLabel OBJ_96;
  private XRiTextField FRDEB;
  private XRiTextField FRFIN;
  private JLabel OBJ_97;
  private JLabel OBJ_107;
  private JLabel OBJ_106;
  private XRiCheckBox WTOUM;
  private XRiCheckBox WTOUFA;
  private XRiCheckBox WTOUA;
  private XRiCheckBox WTOUFR;
  private JLabel OBJ_104;
  private JLabel OBJ_103;
  private JLabel OBJ_102;
  private XRiTextField WSMSA;
  private XRiTextField WSXSA;
  private XRiTextField WLCSA;
  private XRiTextField WSMSC;
  private XRiTextField WSXSC;
  private XRiTextField WLCSC;
  private XRiCheckBox WCN;
  private XRiCheckBox WCA;
  private XRiCheckBox WCB;
  private XRiCheckBox WCC;
  private XRiCheckBox WCD;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
