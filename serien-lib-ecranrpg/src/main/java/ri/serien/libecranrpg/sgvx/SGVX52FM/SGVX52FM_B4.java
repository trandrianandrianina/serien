
package ri.serien.libecranrpg.sgvx.SGVX52FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX52FM_B4 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  // Variables
  private boolean modePlanning = false;
  private boolean modeFamilles = false;
  
  public SGVX52FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    
    WTOU.setValeursSelection("**", " ");
    WARTDE.setValeursSelection("OUI", "NON");
    WARTNG.setValeursSelection("OUI", "NON");
    EDTDET.setValeursSelection("OUI", "NON");
    OPT01.setValeursSelection("1", " ");
    OPT02.setValeursSelection("1", " ");
    OPT03.setValeursSelection("1", " ");
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    modeFamilles = lexique.isTrue("94");
    modePlanning = lexique.isTrue("97");
    
    rafraichirEtablissement();
    
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Message planning
    lbPlanning.setVisible(modePlanning);
    if (modePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
    }
    
    boolean is91 = lexique.isTrue("91");
    pnlEdition.setVisible(is91);
    OPT01.setVisible(is91);
    OPT02.setVisible(is91);
    OPT03.setVisible(is91);
    
    lexique.setNomFichierTableur(
        lexique.HostFieldGetData("TITPG1").trim().replace('/', '_') + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    rafraichirEtablissement();
    
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Message planning
    lbPlanning.setVisible(modePlanning);
    if (modePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
      // boites à choix date paramétrée
      cbDatePlanning.removeAllItems();
      cbDatePlanning.addItem("");
      cbDatePlanning.addItem("Date du jour");
      cbDatePlanning.addItem("Début du mois en cours");
      cbDatePlanning.addItem("Début du mois précédent");
      cbDatePlanning.addItem("Fin du mois précédent");
      cbDatePlanning2.removeAllItems();
      cbDatePlanning2.addItem("");
      cbDatePlanning2.addItem("Date du jour");
      cbDatePlanning2.addItem("Début du mois en cours");
      cbDatePlanning2.addItem("Début du mois précédent");
      cbDatePlanning2.addItem("Fin du mois précédent");
      if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).startsWith("*")) {
        rbDate.setSelected(false);
        rbDateParametree.setSelected(true);
      }
      else {
        rbDate.setSelected(true);
        rbDateParametree.setSelected(false);
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*DAT")) {
        cbDatePlanning.setSelectedItem("Date du jour");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*DME")) {
        cbDatePlanning.setSelectedItem("Début du mois en cours");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*DMP")) {
        cbDatePlanning.setSelectedItem("Début du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*FMP")) {
        cbDatePlanning.setSelectedItem("Fin du mois précédent");
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*DAT")) {
        cbDatePlanning2.setSelectedItem("Date du jour");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*DME")) {
        cbDatePlanning2.setSelectedItem("Début du mois en cours");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*DMP")) {
        cbDatePlanning2.setSelectedItem("Début du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*FMP")) {
        cbDatePlanning2.setSelectedItem("Fin du mois précédent");
      }
    }
    
    rafraichirMagasin();
    
    lbFamilles.setVisible(modeFamilles);
    snFamille1.setVisible(modeFamilles);
    snFamille2.setVisible(modeFamilles);
    lbSousFamilles.setVisible(!modeFamilles);
    snSousFamille1.setVisible(!modeFamilles);
    snSousFamille2.setVisible(!modeFamilles);
    
    if (modeFamilles) {
      WTOU.setText("Toutes les familles");
      rafraichirFamille1();
      rafraichirFamille2();
    }
    else {
      WTOU.setText("Toutes les sous-familles");
      rafraichirSousFamille1();
      rafraichirSousFamille2();
    }
    
    // Mode planning
    lbDate.setVisible(!modePlanning);
    rbDate.setVisible(modePlanning);
    rbDateParametree.setVisible(modePlanning);
    pnlDateParametree.setVisible(modePlanning);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "MA01");
    
    if (modeFamilles) {
      snFamille1.renseignerChampRPG(lexique, "EFAMD");
      snFamille2.renseignerChampRPG(lexique, "EFAMF");
      snFamille1.setEnabled(false);
      snFamille2.setEnabled(false);
    }
    else {
      snSousFamille1.renseignerChampRPG(lexique, "SFAMD");
      snSousFamille2.renseignerChampRPG(lexique, "SFAMF");
    }
    
    if (modePlanning) {
      switch (cbDatePlanning.getSelectedIndex()) {
        case 1:
          lexique.HostFieldPutData("DATDEB", 0, "*DAT");
          break;
        case 2:
          lexique.HostFieldPutData("DATDEB", 0, "*DME");
          break;
        case 3:
          lexique.HostFieldPutData("DATDEB", 0, "*DMP");
          break;
        case 4:
          lexique.HostFieldPutData("DATDEB", 0, "*FMP");
          break;
        
        default:
          break;
      }
      switch (cbDatePlanning2.getSelectedIndex()) {
        case 1:
          lexique.HostFieldPutData("DATFIN", 0, "*DAT");
          break;
        case 2:
          lexique.HostFieldPutData("DATFIN", 0, "*DME");
          break;
        case 3:
          lexique.HostFieldPutData("DATFIN", 0, "*DMP");
          break;
        case 4:
          lexique.HostFieldPutData("DATFIN", 0, "*FMP");
          break;
        
        default:
          break;
      }
    }
  }
  
  /**
   * Initialise le composant établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    snEtablissement.setEnabled(false);
  }
  
  /**
   * 
   * Initialise le composant magasin.
   *
   */
  private void rafraichirMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "MA01");
    snMagasin.setEnabled(false);
  }
  
  private void rafraichirFamille1() {
    snFamille1.setSession(getSession());
    snFamille1.setIdEtablissement(snEtablissement.getIdSelection());
    snFamille1.charger(false);
    snFamille1.setSelectionParChampRPG(lexique, "EFAMD");
    snFamille1.setEnabled(false);
  }
  
  private void rafraichirFamille2() {
    snFamille2.setSession(getSession());
    snFamille2.setIdEtablissement(snEtablissement.getIdSelection());
    snFamille2.charger(false);
    snFamille2.setSelectionParChampRPG(lexique, "EFAMF");
    snFamille2.setEnabled(false);
  }
  
  private void rafraichirSousFamille1() {
    snSousFamille1.setSession(getSession());
    snSousFamille1.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamille1.charger(false);
    snSousFamille1.setSelectionParChampRPG(lexique, "SFAMD");
    snSousFamille1.setEnabled(false);
  }
  
  private void rafraichirSousFamille2() {
    snSousFamille2.setSession(getSession());
    snSousFamille2.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamille2.charger(false);
    snSousFamille2.setSelectionParChampRPG(lexique, "SFAMF");
    snSousFamille2.setEnabled(false);
  }
  
  private void modifierParametrageDate() {
    if (rbDate.isSelected()) {
      DATDEB.setEnabled(true);
      DATFIN.setEnabled(true);
      DATDEB.setDate(null);
      DATFIN.setDate(null);
      cbDatePlanning.setSelectedIndex(0);
      cbDatePlanning.setEnabled(false);
      cbDatePlanning2.setSelectedIndex(0);
      cbDatePlanning2.setEnabled(false);
    }
    else if (rbDateParametree.isSelected()) {
      lexique.HostFieldPutData("DATDEB", 0, "");
      lexique.HostFieldPutData("DATFIN", 0, "");
      DATDEB.setDate(null);
      DATDEB.setEnabled(false);
      DATFIN.setDate(null);
      DATFIN.setEnabled(false);
      cbDatePlanning.setEnabled(true);
      cbDatePlanning2.setEnabled(true);
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void PERDEBInputMethodTextChanged(InputMethodEvent e) {
    try {
      cbDatePlanning.setSelectedIndex(0);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void PERFINInputMethodTextChanged(InputMethodEvent e) {
    try {
      cbDatePlanning2.setSelectedIndex(0);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbDateItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbDateParametreeItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCriteresSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    WTOU = new XRiCheckBox();
    lbFamilles = new SNLabelChamp();
    snFamille1 = new SNFamille();
    snFamille2 = new SNFamille();
    lbSousFamilles = new SNLabelChamp();
    snSousFamille1 = new SNSousFamille();
    snSousFamille2 = new SNSousFamille();
    pnlLibelleDate = new SNPanel();
    rbDate = new SNRadioButton();
    lbDate = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    lbDu = new SNLabelChamp();
    DATDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    DATFIN = new XRiCalendrier();
    rbDateParametree = new SNRadioButton();
    pnlDateParametree = new SNPanel();
    cbDatePlanning = new SNComboBox();
    lbAu2 = new SNLabelChamp();
    cbDatePlanning2 = new SNComboBox();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEnCours = new SNTexte();
    pnlOptionsEdition = new SNPanelTitre();
    WARTNG = new XRiCheckBox();
    WARTDE = new XRiCheckBox();
    EDTDET = new XRiCheckBox();
    pnlEdition = new SNPanelTitre();
    OPT01 = new XRiCheckBox();
    OPT02 = new XRiCheckBox();
    OPT03 = new XRiCheckBox();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      p_nord.add(bpPresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbPlanning ----
        lbPlanning.setText("Label Planning");
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCriteresSelection ========
          {
            pnlCriteresSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresSelection.setName("pnlCriteresSelection");
            pnlCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCriteresSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlCriteresSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlCriteresSelection.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WTOU ----
            WTOU.setText("Toutes les familles");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTOU.setMaximumSize(new Dimension(142, 30));
            WTOU.setMinimumSize(new Dimension(142, 30));
            WTOU.setPreferredSize(new Dimension(142, 30));
            WTOU.setName("WTOU");
            pnlCriteresSelection.add(WTOU, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilles ----
            lbFamilles.setText("Plage de familles");
            lbFamilles.setName("lbFamilles");
            pnlCriteresSelection.add(lbFamilles, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamille1 ----
            snFamille1.setName("snFamille1");
            pnlCriteresSelection.add(snFamille1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snFamille2 ----
            snFamille2.setName("snFamille2");
            pnlCriteresSelection.add(snFamille2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilles ----
            lbSousFamilles.setText("Plage de sous-familles");
            lbSousFamilles.setName("lbSousFamilles");
            pnlCriteresSelection.add(lbSousFamilles, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamille1 ----
            snSousFamille1.setName("snSousFamille1");
            pnlCriteresSelection.add(snSousFamille1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snSousFamille2 ----
            snSousFamille2.setName("snSousFamille2");
            pnlCriteresSelection.add(snSousFamille2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlLibelleDate ========
            {
              pnlLibelleDate.setName("pnlLibelleDate");
              pnlLibelleDate.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- rbDate ----
              rbDate.setText("P\u00e9riode \u00e0 \u00e9diter");
              rbDate.setPreferredSize(new Dimension(150, 30));
              rbDate.setMinimumSize(new Dimension(150, 30));
              rbDate.setMaximumSize(new Dimension(250, 30));
              rbDate.setName("rbDate");
              rbDate.addItemListener(e -> rbDateItemStateChanged(e));
              pnlLibelleDate.add(rbDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDate ----
              lbDate.setText("P\u00e9riode \u00e0 \u00e9diter");
              lbDate.setName("lbDate");
              pnlLibelleDate.add(lbDate, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresSelection.add(pnlLibelleDate, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setOpaque(false);
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbDu ----
              lbDu.setText("du");
              lbDu.setPreferredSize(new Dimension(30, 30));
              lbDu.setMinimumSize(new Dimension(30, 30));
              lbDu.setMaximumSize(new Dimension(30, 30));
              lbDu.setName("lbDu");
              pnlPeriodeAEditer.add(lbDu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DATDEB ----
              DATDEB.setPreferredSize(new Dimension(125, 30));
              DATDEB.setMinimumSize(new Dimension(150, 30));
              DATDEB.setMaximumSize(new Dimension(150, 30));
              DATDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATDEB.setName("DATDEB");
              DATDEB.addInputMethodListener(new InputMethodListener() {
                @Override
                public void caretPositionChanged(InputMethodEvent e) {
                }
                
                @Override
                public void inputMethodTextChanged(InputMethodEvent e) {
                  PERDEBInputMethodTextChanged(e);
                }
              });
              pnlPeriodeAEditer.add(DATDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setPreferredSize(new Dimension(30, 30));
              lbAu.setMinimumSize(new Dimension(30, 30));
              lbAu.setMaximumSize(new Dimension(30, 30));
              lbAu.setName("lbAu");
              pnlPeriodeAEditer.add(lbAu, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DATFIN ----
              DATFIN.setPreferredSize(new Dimension(125, 30));
              DATFIN.setMinimumSize(new Dimension(150, 30));
              DATFIN.setMaximumSize(new Dimension(150, 30));
              DATFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATFIN.setName("DATFIN");
              DATFIN.addInputMethodListener(new InputMethodListener() {
                @Override
                public void caretPositionChanged(InputMethodEvent e) {
                }
                
                @Override
                public void inputMethodTextChanged(InputMethodEvent e) {
                  PERFINInputMethodTextChanged(e);
                }
              });
              pnlPeriodeAEditer.add(DATFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbDateParametree ----
            rbDateParametree.setText("ou date param\u00e9r\u00e9e");
            rbDateParametree.setPreferredSize(new Dimension(200, 30));
            rbDateParametree.setMinimumSize(new Dimension(200, 30));
            rbDateParametree.setMaximumSize(new Dimension(250, 30));
            rbDateParametree.setName("rbDateParametree");
            rbDateParametree.addItemListener(e -> rbDateParametreeItemStateChanged(e));
            pnlCriteresSelection.add(rbDateParametree, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlDateParametree ========
            {
              pnlDateParametree.setName("pnlDateParametree");
              pnlDateParametree.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- cbDatePlanning ----
              cbDatePlanning.setName("cbDatePlanning");
              pnlDateParametree.add(cbDatePlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu2 ----
              lbAu2.setText("\u00e0");
              lbAu2.setPreferredSize(new Dimension(30, 30));
              lbAu2.setMinimumSize(new Dimension(30, 30));
              lbAu2.setMaximumSize(new Dimension(30, 30));
              lbAu2.setName("lbAu2");
              pnlDateParametree.add(lbAu2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbDatePlanning2 ----
              cbDatePlanning2.setName("cbDatePlanning2");
              pnlDateParametree.add(cbDatePlanning2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCriteresSelection.add(pnlDateParametree, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionne ========
          {
            pnlEtablissementSelectionne.setTitre("Etablissement");
            pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
            pnlEtablissementSelectionne.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
            pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionne.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfDateEnCours ----
            tfDateEnCours.setText("@WENCX@");
            tfDateEnCours.setMaximumSize(new Dimension(272, 20));
            tfDateEnCours.setEditable(false);
            tfDateEnCours.setPreferredSize(new Dimension(272, 30));
            tfDateEnCours.setMinimumSize(new Dimension(272, 30));
            tfDateEnCours.setEnabled(false);
            tfDateEnCours.setName("tfDateEnCours");
            pnlEtablissementSelectionne.add(tfDateEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlOptionsEdition ========
          {
            pnlOptionsEdition.setOpaque(false);
            pnlOptionsEdition.setTitre("Options d'\u00e9dition");
            pnlOptionsEdition.setName("pnlOptionsEdition");
            pnlOptionsEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionsEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- WARTNG ----
            WARTNG.setText("Edition des articles non g\u00e9r\u00e9s / p\u00e9riode");
            WARTNG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WARTNG.setFont(new Font("sansserif", Font.PLAIN, 14));
            WARTNG.setMaximumSize(new Dimension(269, 30));
            WARTNG.setMinimumSize(new Dimension(269, 30));
            WARTNG.setPreferredSize(new Dimension(269, 30));
            WARTNG.setName("WARTNG");
            pnlOptionsEdition.add(WARTNG, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WARTDE ----
            WARTDE.setText("S\u00e9lection des articles d\u00e9sactiv\u00e9s");
            WARTDE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WARTDE.setFont(new Font("sansserif", Font.PLAIN, 14));
            WARTDE.setMaximumSize(new Dimension(269, 30));
            WARTDE.setMinimumSize(new Dimension(269, 30));
            WARTDE.setPreferredSize(new Dimension(269, 30));
            WARTDE.setName("WARTDE");
            pnlOptionsEdition.add(WARTDE, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EDTDET ----
            EDTDET.setText("Edition d\u00e9taill\u00e9e");
            EDTDET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTDET.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTDET.setMaximumSize(new Dimension(269, 30));
            EDTDET.setMinimumSize(new Dimension(269, 30));
            EDTDET.setPreferredSize(new Dimension(269, 30));
            EDTDET.setName("EDTDET");
            pnlOptionsEdition.add(EDTDET, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionsEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlEdition ========
          {
            pnlEdition.setOpaque(false);
            pnlEdition.setTitre("A editer seulement pour");
            pnlEdition.setName("pnlEdition");
            pnlEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OPT01 ----
            OPT01.setText("Les achats");
            OPT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT01.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT01.setMaximumSize(new Dimension(269, 30));
            OPT01.setMinimumSize(new Dimension(269, 30));
            OPT01.setPreferredSize(new Dimension(269, 30));
            OPT01.setName("OPT01");
            pnlEdition.add(OPT01, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT02 ----
            OPT02.setText("Les ventes");
            OPT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT02.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT02.setMaximumSize(new Dimension(269, 30));
            OPT02.setMinimumSize(new Dimension(269, 30));
            OPT02.setPreferredSize(new Dimension(269, 30));
            OPT02.setName("OPT02");
            pnlEdition.add(OPT02, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT03 ----
            OPT03.setText("Les bordereaux de stock");
            OPT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT03.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT03.setMaximumSize(new Dimension(269, 30));
            OPT03.setMinimumSize(new Dimension(269, 30));
            OPT03.setPreferredSize(new Dimension(269, 30));
            OPT03.setName("OPT03");
            pnlEdition.add(OPT03, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEdition, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbDate);
    buttonGroup1.add(rbDateParametree);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre bpPresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNMessage lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private XRiCheckBox WTOU;
  private SNLabelChamp lbFamilles;
  private SNFamille snFamille1;
  private SNFamille snFamille2;
  private SNLabelChamp lbSousFamilles;
  private SNSousFamille snSousFamille1;
  private SNSousFamille snSousFamille2;
  private SNPanel pnlLibelleDate;
  private SNRadioButton rbDate;
  private SNLabelChamp lbDate;
  private SNPanel pnlPeriodeAEditer;
  private SNLabelChamp lbDu;
  private XRiCalendrier DATDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier DATFIN;
  private SNRadioButton rbDateParametree;
  private SNPanel pnlDateParametree;
  private SNComboBox cbDatePlanning;
  private SNLabelChamp lbAu2;
  private SNComboBox cbDatePlanning2;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEnCours;
  private SNPanelTitre pnlOptionsEdition;
  private XRiCheckBox WARTNG;
  private XRiCheckBox WARTDE;
  private XRiCheckBox EDTDET;
  private SNPanelTitre pnlEdition;
  private XRiCheckBox OPT01;
  private XRiCheckBox OPT02;
  private XRiCheckBox OPT03;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
