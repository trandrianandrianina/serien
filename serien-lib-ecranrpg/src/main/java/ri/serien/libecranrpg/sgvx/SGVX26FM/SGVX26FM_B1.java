
package ri.serien.libecranrpg.sgvx.SGVX26FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX26FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX26FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    UTYP3.setValeurs("3", "RB");
    UTYP2.setValeurs("2", "RB");
    UTYP1.setValeurs("1", "RB");
    REPON1.setValeursSelection("OUI", "NON");
    REPON2.setValeursSelection("OUI", "NON");
    TOTFAM.setValeursSelection("OUI", "NON");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // TOTFAM.setSelected(lexique.HostFieldGetData("TOTFAM").equalsIgnoreCase("OUI"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // UTYP3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    // UTYP2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // UTYP1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // REPON2.setSelected(lexique.HostFieldGetData("REPON2").equalsIgnoreCase("OUI"));
    
    UFOU_CHK.setSelected(lexique.HostFieldGetData("UFOU").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!UFOU_CHK.isSelected());
    
    WMAG_CHK.setSelected(lexique.HostFieldGetData("WMAG").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WMAG_CHK.isSelected());
    P_SEL2.setVisible(!WTOU.isSelected());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TOTFAM.isSelected())
    // lexique.HostFieldPutData("TOTFAM", 0, "OUI");
    // else
    // lexique.HostFieldPutData("TOTFAM", 0, "NON");
    if (UFOU_CHK.isSelected()) {
      lexique.HostFieldPutData("UFOU", 0, "**");
    }
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    if (WMAG_CHK.isSelected()) {
      lexique.HostFieldPutData("WMAG", 0, "**");
      // if (UTYP3.isSelected())
      // lexique.HostFieldPutData("RB", 0, "3");
      // if (REPON1.isSelected())
      // lexique.HostFieldPutData("REPON1", 0, "OUI");
      // else
      // lexique.HostFieldPutData("REPON1", 0, "NON");
      // if (UTYP2.isSelected())
      // lexique.HostFieldPutData("RB", 0, "2");
      // if (UTYP1.isSelected())
      // lexique.HostFieldPutData("RB", 0, "1");
      // if (REPON2.isSelected())
      // lexique.HostFieldPutData("REPON2", 0, "OUI");
      // else
      // lexique.HostFieldPutData("REPON2", 0, "NON");
    }
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WMAG_CHKActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!WMAG_CHK.isSelected()) {
      WMAG.setText("");
    }
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void UFOU_CHKActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
    if (!UFOU_CHK.isSelected()) {
      UFOU.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_52 = new JXTitledSeparator();
    OBJ_42 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    OBJ_58 = new JLabel();
    OBJ_66 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    P_SEL0 = new JPanel();
    OBJ_48 = new JLabel();
    WMAG = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_76 = new JLabel();
    WMAG_CHK = new JCheckBox();
    WTOU = new XRiCheckBox();
    TOTFAM = new XRiCheckBox();
    WPROD = new XRiTextField();
    panel1 = new JPanel();
    OBJ_53 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    OBJ_37 = new JXTitledSeparator();
    P_SEL1 = new JPanel();
    OBJ_50 = new JLabel();
    UFOU = new XRiTextField();
    REPON2 = new XRiCheckBox();
    UTYP1 = new XRiRadioButton();
    UTYP2 = new XRiRadioButton();
    REPON1 = new XRiCheckBox();
    UTYP3 = new XRiRadioButton();
    UFOU_CHK = new JCheckBox();
    OBJ_71 = new JLabel();
    WDEL = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_52 ----
          OBJ_52.setTitle("Plage familles");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_42 ----
          OBJ_42.setTitle("Magasin");
          OBJ_42.setName("OBJ_42");

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- OBJ_58 ----
            OBJ_58.setText("Code de d\u00e9but");
            OBJ_58.setName("OBJ_58");
            P_SEL2.add(OBJ_58);
            OBJ_58.setBounds(10, 15, 105, 18);

            //---- OBJ_66 ----
            OBJ_66.setText("Code de fin");
            OBJ_66.setName("OBJ_66");
            P_SEL2.add(OBJ_66);
            OBJ_66.setBounds(10, 45, 105, 18);

            //---- FAMDEB ----
            FAMDEB.setComponentPopupMenu(BTD);
            FAMDEB.setName("FAMDEB");
            P_SEL2.add(FAMDEB);
            FAMDEB.setBounds(210, 10, 40, FAMDEB.getPreferredSize().height);

            //---- FAMFIN ----
            FAMFIN.setComponentPopupMenu(BTD);
            FAMFIN.setName("FAMFIN");
            P_SEL2.add(FAMFIN);
            FAMFIN.setBounds(210, 40, 40, FAMFIN.getPreferredSize().height);
          }

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_48 ----
            OBJ_48.setText("Code magasin");
            OBJ_48.setName("OBJ_48");
            P_SEL0.add(OBJ_48);
            OBJ_48.setBounds(10, 15, 105, 18);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            P_SEL0.add(WMAG);
            WMAG.setBounds(210, 10, 30, WMAG.getPreferredSize().height);
          }

          //---- OBJ_77 ----
          OBJ_77.setText("@LIBGP@");
          OBJ_77.setName("OBJ_77");

          //---- OBJ_76 ----
          OBJ_76.setText("Gestionnaire de produit");
          OBJ_76.setName("OBJ_76");

          //---- WMAG_CHK ----
          WMAG_CHK.setText("S\u00e9lection compl\u00e8te");
          WMAG_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WMAG_CHK.setName("WMAG_CHK");
          WMAG_CHK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WMAG_CHKActionPerformed(e);
            }
          });

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- TOTFAM ----
          TOTFAM.setText("Totalisation");
          TOTFAM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOTFAM.setName("TOTFAM");

          //---- WPROD ----
          WPROD.setComponentPopupMenu(BTD);
          WPROD.setName("WPROD");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_53 ----
            OBJ_53.setTitle("Type d'\u00e9dition");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(10, 110, 365, OBJ_53.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setTitle("Fournisseur");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(10, 10, 365, OBJ_43.getPreferredSize().height);

            //---- OBJ_37 ----
            OBJ_37.setTitle("");
            OBJ_37.setName("OBJ_37");
            panel1.add(OBJ_37);
            OBJ_37.setBounds(10, 235, 370, OBJ_37.getPreferredSize().height);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_50 ----
              OBJ_50.setText("Code fournisseur");
              OBJ_50.setName("OBJ_50");
              P_SEL1.add(OBJ_50);
              OBJ_50.setBounds(10, 15, 140, 18);

              //---- UFOU ----
              UFOU.setComponentPopupMenu(BTD);
              UFOU.setName("UFOU");
              P_SEL1.add(UFOU);
              UFOU.setBounds(205, 10, 80, UFOU.getPreferredSize().height);
            }
            panel1.add(P_SEL1);
            P_SEL1.setBounds(30, 55, 295, 46);

            //---- REPON2 ----
            REPON2.setText("G\u00e9n\u00e9ration des r\u00e9appros");
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setName("REPON2");
            panel1.add(REPON2);
            REPON2.setBounds(30, 295, 222, REPON2.getPreferredSize().height);

            //---- UTYP1 ----
            UTYP1.setText("Articles \u00e0 r\u00e9approvisionner seuls");
            UTYP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP1.setName("UTYP1");
            panel1.add(UTYP1);
            UTYP1.setBounds(30, 145, 220, UTYP1.getPreferredSize().height);

            //---- UTYP2 ----
            UTYP2.setText("Tous les articles");
            UTYP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP2.setName("UTYP2");
            panel1.add(UTYP2);
            UTYP2.setBounds(30, 175, 201, UTYP2.getPreferredSize().height);

            //---- REPON1 ----
            REPON1.setText("Calcul du stock minimum");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setName("REPON1");
            panel1.add(REPON1);
            REPON1.setBounds(30, 265, 171, REPON1.getPreferredSize().height);

            //---- UTYP3 ----
            UTYP3.setText("Articles en sur stocks");
            UTYP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UTYP3.setName("UTYP3");
            panel1.add(UTYP3);
            UTYP3.setBounds(30, 205, 153, UTYP3.getPreferredSize().height);

            //---- UFOU_CHK ----
            UFOU_CHK.setText("S\u00e9lection compl\u00e8te");
            UFOU_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UFOU_CHK.setName("UFOU_CHK");
            UFOU_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                UFOU_CHKActionPerformed(e);
              }
            });
            panel1.add(UFOU_CHK);
            UFOU_CHK.setBounds(30, 30, 135, UFOU_CHK.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("D\u00e9lai");
            OBJ_71.setName("OBJ_71");
            panel1.add(OBJ_71);
            OBJ_71.setBounds(245, 205, 36, 18);

            //---- WDEL ----
            WDEL.setComponentPopupMenu(BTD);
            WDEL.setName("WDEL");
            panel1.add(WDEL);
            WDEL.setBounds(285, 200, 30, WDEL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 735, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(82, 82, 82)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(WMAG_CHK, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(p_contenuLayout.createParallelGroup()
                          .addComponent(TOTFAM, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
                        .addGap(68, 68, 68)
                        .addComponent(WPROD, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)))
                    .addGap(2, 2, 2)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(WMAG_CHK)
                    .addGap(7, 7, 7)
                    .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                    .addGap(9, 9, 9)
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(TOTFAM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WPROD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(UTYP1);
    RB_GRP.add(UTYP2);
    RB_GRP.add(UTYP3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_52;
  private JXTitledSeparator OBJ_42;
  private JPanel P_SEL2;
  private JLabel OBJ_58;
  private JLabel OBJ_66;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JPanel P_SEL0;
  private JLabel OBJ_48;
  private XRiTextField WMAG;
  private JLabel OBJ_77;
  private JLabel OBJ_76;
  private JCheckBox WMAG_CHK;
  private XRiCheckBox WTOU;
  private XRiCheckBox TOTFAM;
  private XRiTextField WPROD;
  private JPanel panel1;
  private JXTitledSeparator OBJ_53;
  private JXTitledSeparator OBJ_43;
  private JXTitledSeparator OBJ_37;
  private JPanel P_SEL1;
  private JLabel OBJ_50;
  private XRiTextField UFOU;
  private XRiCheckBox REPON2;
  private XRiRadioButton UTYP1;
  private XRiRadioButton UTYP2;
  private XRiCheckBox REPON1;
  private XRiRadioButton UTYP3;
  private JCheckBox UFOU_CHK;
  private JLabel OBJ_71;
  private XRiTextField WDEL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
