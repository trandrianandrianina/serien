
package ri.serien.libecranrpg.sgvx.SGVXVFFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;

/**
 * @author Stéphane Vénéri
 */
public class SGVXVFFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXVFFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@IND60@")).trim());
    VFUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VFUSR@")).trim());
    WTIMH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTIMH@")).trim());
    WTIMM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTIMM@")).trim());
    VFTRV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VFTRV@")).trim());
    VFNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VFNUM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    riMenu2.setVisible(lexique.isTrue("17"));
    riSousMenu6.setVisible(lexique.isTrue("17"));
    label7.setVisible(lexique.isTrue("17"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    separator1 = compFactory.createSeparator("@IND60@");
    label1 = new JLabel();
    VFUSR = new RiZoneSortie();
    label2 = new JLabel();
    WDATX = new XRiCalendrier();
    label3 = new JLabel();
    WTIMH = new RiZoneSortie();
    label4 = new JLabel();
    WTIMM = new RiZoneSortie();
    label5 = new JLabel();
    VFTRV = new RiZoneSortie();
    label6 = new JLabel();
    VFNUM = new RiZoneSortie();
    label7 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(700, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("D\u00e9verrouiller");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(10, 10, 500, separator1.getPreferredSize().height);

          //---- label1 ----
          label1.setText("V\u00e9rrouill\u00e9 par");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(20, 38, 85, 20);

          //---- VFUSR ----
          VFUSR.setText("@VFUSR@");
          VFUSR.setName("VFUSR");
          panel1.add(VFUSR);
          VFUSR.setBounds(110, 36, 114, VFUSR.getPreferredSize().height);

          //---- label2 ----
          label2.setText("le");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(20, 69, 25, 20);

          //---- WDATX ----
          WDATX.setName("WDATX");
          panel1.add(WDATX);
          WDATX.setBounds(110, 65, 105, WDATX.getPreferredSize().height);

          //---- label3 ----
          label3.setText("\u00e0");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(240, 71, 25, label3.getPreferredSize().height);

          //---- WTIMH ----
          WTIMH.setText("@WTIMH@");
          WTIMH.setName("WTIMH");
          panel1.add(WTIMH);
          WTIMH.setBounds(265, 67, 28, WTIMH.getPreferredSize().height);

          //---- label4 ----
          label4.setText("h");
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(295, 71, 20, label4.getPreferredSize().height);

          //---- WTIMM ----
          WTIMM.setText("@WTIMM@");
          WTIMM.setName("WTIMM");
          panel1.add(WTIMM);
          WTIMM.setBounds(315, 67, 28, WTIMM.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Travail");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(20, 100, 85, 20);

          //---- VFTRV ----
          VFTRV.setText("@VFTRV@");
          VFTRV.setName("VFTRV");
          panel1.add(VFTRV);
          VFTRV.setBounds(110, 98, 114, VFTRV.getPreferredSize().height);

          //---- label6 ----
          label6.setText("num\u00e9ro");
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(20, 129, 85, 20);

          //---- VFNUM ----
          VFNUM.setText("@VFNUM@");
          VFNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          VFNUM.setName("VFNUM");
          panel1.add(VFNUM);
          VFNUM.setBounds(110, 127, 60, VFNUM.getPreferredSize().height);

          //---- label7 ----
          label7.setText("Si l'utilisateur ci dessus est sorti de la fiche, vous pouvez la d\u00e9verrouiller");
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setForeground(new Color(204, 0, 0));
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(10, 160, 500, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 520, 190);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JComponent separator1;
  private JLabel label1;
  private RiZoneSortie VFUSR;
  private JLabel label2;
  private XRiCalendrier WDATX;
  private JLabel label3;
  private RiZoneSortie WTIMH;
  private JLabel label4;
  private RiZoneSortie WTIMM;
  private JLabel label5;
  private RiZoneSortie VFTRV;
  private JLabel label6;
  private RiZoneSortie VFNUM;
  private JLabel label7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
