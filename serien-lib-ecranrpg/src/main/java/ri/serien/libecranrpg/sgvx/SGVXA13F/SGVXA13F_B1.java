
package ri.serien.libecranrpg.sgvx.SGVXA13F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class SGVXA13F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXA13F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    WLS05.setValeursSelection("X", " ");
    WLS04.setValeursSelection("X", " ");
    WLS03.setValeursSelection("X", " ");
    WLS02.setValeursSelection("X", " ");
    WLS01.setValeursSelection("X", " ");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    WLS01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LST01@")).trim());
    WLS02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LST02@")).trim());
    WLS03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LST03@")).trim());
    WLS04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LST04@")).trim());
    WLS05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LST05@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    OBJ_20 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    WLS01 = new XRiCheckBox();
    WLS02 = new XRiCheckBox();
    WLS03 = new XRiCheckBox();
    WLS04 = new XRiCheckBox();
    WLS05 = new XRiCheckBox();
    OBJ_41 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_42 = new JLabel();
    DEBAD1 = new XRiTextField();
    DEBAD2 = new XRiTextField();
    DEBAD3 = new XRiTextField();
    DEBAD4 = new XRiTextField();
    FINAD1 = new XRiTextField();
    FINAD2 = new XRiTextField();
    FINAD3 = new XRiTextField();
    FINAD4 = new XRiTextField();
    WMAG = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 200));
        menus_haut.setPreferredSize(new Dimension(160, 200));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt_export ----
          riSousMenu_bt_export.setText("Exportation tableur");
          riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
          riSousMenu_bt_export.setName("riSousMenu_bt_export");
          riSousMenu_bt_export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt_exportActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt_export);
        }
        menus_haut.add(riSousMenu6);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel1.add(sep_etablissement);
            sep_etablissement.setBounds(30, 30, 520, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel1.add(z_dgnom_);
            z_dgnom_.setBounds(200, 55, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel1.add(z_wencx_);
            z_wencx_.setBounds(200, 85, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel1.add(z_etablissement_);
            z_etablissement_.setBounds(45, 70, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel1.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(90, 70), bouton_etablissement.getPreferredSize()));

            //---- OBJ_19 ----
            OBJ_19.setTitle("Type de zones \u00e0 \u00e9diter");
            OBJ_19.setName("OBJ_19");
            panel1.add(OBJ_19);
            OBJ_19.setBounds(30, 310, 520, OBJ_19.getPreferredSize().height);

            //---- OBJ_20 ----
            OBJ_20.setTitle("Adresse de stockage \u00e0 cr\u00e9er");
            OBJ_20.setName("OBJ_20");
            panel1.add(OBJ_20);
            OBJ_20.setBounds(30, 205, 520, OBJ_20.getPreferredSize().height);

            //---- OBJ_21 ----
            OBJ_21.setTitle("");
            OBJ_21.setName("OBJ_21");
            panel1.add(OBJ_21);
            OBJ_21.setBounds(30, 130, 520, OBJ_21.getPreferredSize().height);

            //---- WLS01 ----
            WLS01.setText("@LST01@");
            WLS01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WLS01.setName("WLS01");
            panel1.add(WLS01);
            WLS01.setBounds(45, 345, 153, 28);

            //---- WLS02 ----
            WLS02.setText("@LST02@");
            WLS02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WLS02.setName("WLS02");
            panel1.add(WLS02);
            WLS02.setBounds(200, 345, 153, 28);

            //---- WLS03 ----
            WLS03.setText("@LST03@");
            WLS03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WLS03.setName("WLS03");
            panel1.add(WLS03);
            WLS03.setBounds(345, 345, 153, 28);

            //---- WLS04 ----
            WLS04.setText("@LST04@");
            WLS04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WLS04.setName("WLS04");
            panel1.add(WLS04);
            WLS04.setBounds(45, 380, 153, 28);

            //---- WLS05 ----
            WLS05.setText("@LST05@");
            WLS05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WLS05.setName("WLS05");
            panel1.add(WLS05);
            WLS05.setBounds(200, 380, 153, 28);

            //---- OBJ_41 ----
            OBJ_41.setText("Adresse de d\u00e9but");
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(45, 235, 110, 28);

            //---- OBJ_40 ----
            OBJ_40.setText("Magasin \u00e0 traiter");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(45, 150, 121, 28);

            //---- OBJ_42 ----
            OBJ_42.setText("Adresse de fin");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(45, 265, 112, 28);

            //---- DEBAD1 ----
            DEBAD1.setComponentPopupMenu(BTD);
            DEBAD1.setName("DEBAD1");
            panel1.add(DEBAD1);
            DEBAD1.setBounds(200, 235, 40, DEBAD1.getPreferredSize().height);

            //---- DEBAD2 ----
            DEBAD2.setComponentPopupMenu(BTD);
            DEBAD2.setName("DEBAD2");
            panel1.add(DEBAD2);
            DEBAD2.setBounds(275, 235, 40, DEBAD2.getPreferredSize().height);

            //---- DEBAD3 ----
            DEBAD3.setComponentPopupMenu(BTD);
            DEBAD3.setName("DEBAD3");
            panel1.add(DEBAD3);
            DEBAD3.setBounds(345, 235, 40, DEBAD3.getPreferredSize().height);

            //---- DEBAD4 ----
            DEBAD4.setComponentPopupMenu(BTD);
            DEBAD4.setName("DEBAD4");
            panel1.add(DEBAD4);
            DEBAD4.setBounds(420, 235, 40, DEBAD4.getPreferredSize().height);

            //---- FINAD1 ----
            FINAD1.setComponentPopupMenu(BTD);
            FINAD1.setName("FINAD1");
            panel1.add(FINAD1);
            FINAD1.setBounds(200, 265, 40, FINAD1.getPreferredSize().height);

            //---- FINAD2 ----
            FINAD2.setComponentPopupMenu(BTD);
            FINAD2.setName("FINAD2");
            panel1.add(FINAD2);
            FINAD2.setBounds(275, 265, 40, FINAD2.getPreferredSize().height);

            //---- FINAD3 ----
            FINAD3.setComponentPopupMenu(BTD);
            FINAD3.setName("FINAD3");
            panel1.add(FINAD3);
            FINAD3.setBounds(345, 265, 40, FINAD3.getPreferredSize().height);

            //---- FINAD4 ----
            FINAD4.setComponentPopupMenu(BTD);
            FINAD4.setName("FINAD4");
            panel1.add(FINAD4);
            FINAD4.setBounds(420, 265, 40, FINAD4.getPreferredSize().height);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            panel1.add(WMAG);
            WMAG.setBounds(200, 150, 30, WMAG.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_20;
  private JXTitledSeparator OBJ_21;
  private XRiCheckBox WLS01;
  private XRiCheckBox WLS02;
  private XRiCheckBox WLS03;
  private XRiCheckBox WLS04;
  private XRiCheckBox WLS05;
  private JLabel OBJ_41;
  private JLabel OBJ_40;
  private JLabel OBJ_42;
  private XRiTextField DEBAD1;
  private XRiTextField DEBAD2;
  private XRiTextField DEBAD3;
  private XRiTextField DEBAD4;
  private XRiTextField FINAD1;
  private XRiTextField FINAD2;
  private XRiTextField FINAD3;
  private XRiTextField FINAD4;
  private XRiTextField WMAG;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
