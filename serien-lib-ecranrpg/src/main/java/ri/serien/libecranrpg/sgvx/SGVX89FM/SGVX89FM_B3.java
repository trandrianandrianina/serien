
package ri.serien.libecranrpg.sgvx.SGVX89FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX89FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] REA_Value = { "", "0", "1", "2", "3", "4", "5", "6", "7", "8", };
  private String[] AGE_Value = { "", "0", "1", "2", "3", "4", "5", "6", "7", "8", };
  
  public SGVX89FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    AGE06.setValeurs(AGE_Value, null);
    AGE05.setValeurs(AGE_Value, null);
    AGE04.setValeurs(AGE_Value, null);
    AGE03.setValeurs(AGE_Value, null);
    AGE02.setValeurs(AGE_Value, null);
    AGE01.setValeurs(AGE_Value, null);
    REA06.setValeurs(REA_Value, null);
    REA05.setValeurs(REA_Value, null);
    REA04.setValeurs(REA_Value, null);
    REA02.setValeurs(REA_Value, null);
    REA03.setValeurs(REA_Value, null);
    REA01.setValeurs(REA_Value, null);
    WTOU1.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
    FDSNGE.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    // riBoutonInfo1.gererAffichageV03F(lexique.isTrue("19"),lexique.getConvertInfosFiche(lexique.HostFieldGetData("V03F").trim()));
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    P_SEL0.setVisible(!WTOU1.isSelected());
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    // WTOU1.setEnabled(false);
    P_SEL1.setVisible(!WTOU2.isSelected());
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    // WTOU2.setEnabled(false);
    
    // CODMAG.setVisible( lexique.isPresent("CODMAG"));
    OBJ_43.setVisible(CODMAG.isVisible());
    OBJ_19.setVisible(CODMAG.isVisible());
    // FDSNGE.setVisible( lexique.isPresent("FDSNGE"));
    // FDSNGE.setSelected(lexique.HostFieldGetData("FDSNGE").equalsIgnoreCase("OUI"));
    
    // REA01.setSelectedIndex(getIndice("REA01", REA_Value));
    // REA02.setSelectedIndex(getIndice("REA02", REA_Value));
    // REA03.setSelectedIndex(getIndice("REA03", REA_Value));
    // REA04.setSelectedIndex(getIndice("REA04", REA_Value));
    // REA05.setSelectedIndex(getIndice("REA05", REA_Value));
    // REA06.setSelectedIndex(getIndice("REA06", REA_Value));
    // REA01.setEnabled( lexique.isPresent("REA01"));
    // REA02.setEnabled( lexique.isPresent("REA02"));
    // REA03.setEnabled( lexique.isPresent("REA03"));
    // REA04.setEnabled( lexique.isPresent("REA04"));
    // REA05.setEnabled( lexique.isPresent("REA05"));
    // REA06.setEnabled( lexique.isPresent("REA06"));
    // AGE01.setSelectedIndex(getIndice("AGE01", AGE_Value));
    // AGE02.setSelectedIndex(getIndice("AGE02", AGE_Value));
    // AGE03.setSelectedIndex(getIndice("AGE03", AGE_Value));
    // AGE04.setSelectedIndex(getIndice("AGE04", AGE_Value));
    // AGE05.setSelectedIndex(getIndice("AGE05", AGE_Value));
    // AGE06.setSelectedIndex(getIndice("AGE06", AGE_Value));
    // AGE01.setEnabled( lexique.isPresent("AGE01"));
    // AGE02.setEnabled( lexique.isPresent("AGE02"));
    // AGE03.setEnabled( lexique.isPresent("AGE03"));
    // AGE04.setEnabled( lexique.isPresent("AGE04"));
    // AGE05.setEnabled( lexique.isPresent("AGE05"));
    // AGE06.setEnabled( lexique.isPresent("AGE06"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    // riMenu_bt3.setIcon(lexique.getImage("images/outils.png", true));
    // riMenu_bt4.setIcon(lexique.getImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // lexique.HostFieldPutData("REA06", 0, REA_Value[REA06.getSelectedIndex()]);
    // lexique.HostFieldPutData("REA05", 0, REA_Value[REA05.getSelectedIndex()]);
    // lexique.HostFieldPutData("REA04", 0, REA_Value[REA04.getSelectedIndex()]);
    // lexique.HostFieldPutData("REA03", 0, REA_Value[REA03.getSelectedIndex()]);
    // lexique.HostFieldPutData("REA02", 0, REA_Value[REA02.getSelectedIndex()]);
    // lexique.HostFieldPutData("REA01", 0, REA_Value[REA01.getSelectedIndex()]);
    
    // lexique.HostFieldPutData("AGE06", 0, AGE_Value[AGE06.getSelectedIndex()]);
    // lexique.HostFieldPutData("AGE05", 0, AGE_Value[AGE05.getSelectedIndex()]);
    // lexique.HostFieldPutData("AGE04", 0, AGE_Value[AGE04.getSelectedIndex()]);
    // lexique.HostFieldPutData("AGE03", 0, AGE_Value[AGE03.getSelectedIndex()]);
    // lexique.HostFieldPutData("AGE02", 0, AGE_Value[AGE02.getSelectedIndex()]);
    // lexique.HostFieldPutData("AGE01", 0, AGE_Value[AGE01.getSelectedIndex()]);
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvx"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_74 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_32 = new JXTitledSeparator();
    OBJ_24 = new JXTitledSeparator();
    OBJ_19 = new JXTitledSeparator();
    FDSNGE = new XRiCheckBox();
    OBJ_42 = new JLabel();
    OBJ_43 = new JLabel();
    CODFRS = new XRiTextField();
    CODMAG = new XRiTextField();
    WTOU2 = new XRiCheckBox();
    P_SEL1 = new JPanel();
    EDEBA = new XRiTextField();
    EFINA = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_39 = new JLabel();
    WTOU1 = new XRiCheckBox();
    P_SEL0 = new JPanel();
    OBJ_27 = new JLabel();
    OBJ_30 = new JLabel();
    EDEB = new XRiTextField();
    EFIN = new XRiTextField();
    OBJ_38 = new JXTitledSeparator();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    REA01 = new XRiComboBox();
    REA03 = new XRiComboBox();
    REA02 = new XRiComboBox();
    REA04 = new XRiComboBox();
    REA05 = new XRiComboBox();
    REA06 = new XRiComboBox();
    AGE01 = new XRiComboBox();
    AGE02 = new XRiComboBox();
    AGE03 = new XRiComboBox();
    AGE04 = new XRiComboBox();
    AGE05 = new XRiComboBox();
    AGE06 = new XRiComboBox();
    OBJ_40 = new JXTitledSeparator();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 200));
        menus_haut.setPreferredSize(new Dimension(160, 200));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt_export ----
          riSousMenu_bt_export.setText("Exportation tableur");
          riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
          riSousMenu_bt_export.setName("riSousMenu_bt_export");
          riSousMenu_bt_export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt_exportActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt_export);
        }
        menus_haut.add(riSousMenu6);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1100, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_74 ----
          OBJ_74.setText("Classe D");
          OBJ_74.setName("OBJ_74");

          //---- OBJ_75 ----
          OBJ_75.setText("Classe B");
          OBJ_75.setName("OBJ_75");

          //---- OBJ_76 ----
          OBJ_76.setText("Classe N");
          OBJ_76.setName("OBJ_76");

          //---- OBJ_77 ----
          OBJ_77.setText("Classe C");
          OBJ_77.setName("OBJ_77");

          //---- OBJ_78 ----
          OBJ_78.setText("Classe R");
          OBJ_78.setName("OBJ_78");

          //---- OBJ_79 ----
          OBJ_79.setText("Classe A");
          OBJ_79.setName("OBJ_79");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setEnabled(false);
          bouton_etablissement.setName("bouton_etablissement");

          //---- OBJ_32 ----
          OBJ_32.setTitle("Plage articles");
          OBJ_32.setName("OBJ_32");

          //---- OBJ_24 ----
          OBJ_24.setTitle("Plage de famille");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_19 ----
          OBJ_19.setTitle("");
          OBJ_19.setName("OBJ_19");

          //---- FDSNGE ----
          FDSNGE.setText("Mise \u00e0 jour article fin de s\u00e9rie en produit non g\u00e9r\u00e9");
          FDSNGE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FDSNGE.setName("FDSNGE");

          //---- OBJ_42 ----
          OBJ_42.setText("Fournisseur \u00e0 traiter");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_43 ----
          OBJ_43.setText("Code magasin");
          OBJ_43.setName("OBJ_43");

          //---- CODFRS ----
          CODFRS.setComponentPopupMenu(BTD);
          CODFRS.setName("CODFRS");

          //---- CODMAG ----
          CODMAG.setComponentPopupMenu(BTD);
          CODMAG.setName("CODMAG");

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU2.setName("WTOU2");

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- EDEBA ----
            EDEBA.setComponentPopupMenu(BTD);
            EDEBA.setName("EDEBA");
            P_SEL1.add(EDEBA);
            EDEBA.setBounds(125, 5, 210, EDEBA.getPreferredSize().height);

            //---- EFINA ----
            EFINA.setComponentPopupMenu(BTD);
            EFINA.setName("EFINA");
            P_SEL1.add(EFINA);
            EFINA.setBounds(125, 35, 210, EFINA.getPreferredSize().height);

            //---- OBJ_36 ----
            OBJ_36.setText("Code d\u00e9but");
            OBJ_36.setName("OBJ_36");
            P_SEL1.add(OBJ_36);
            OBJ_36.setBounds(11, 9, 93, 20);

            //---- OBJ_39 ----
            OBJ_39.setText("Code fin");
            OBJ_39.setName("OBJ_39");
            P_SEL1.add(OBJ_39);
            OBJ_39.setBounds(11, 39, 75, 20);
          }

          //---- WTOU1 ----
          WTOU1.setText("S\u00e9lection compl\u00e8te");
          WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU1.setName("WTOU1");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_27 ----
            OBJ_27.setText("Code d\u00e9but");
            OBJ_27.setName("OBJ_27");
            P_SEL0.add(OBJ_27);
            OBJ_27.setBounds(15, 9, 93, 20);

            //---- OBJ_30 ----
            OBJ_30.setText("Code fin");
            OBJ_30.setName("OBJ_30");
            P_SEL0.add(OBJ_30);
            OBJ_30.setBounds(15, 37, 75, 20);

            //---- EDEB ----
            EDEB.setComponentPopupMenu(BTD);
            EDEB.setName("EDEB");
            P_SEL0.add(EDEB);
            EDEB.setBounds(125, 5, 40, EDEB.getPreferredSize().height);

            //---- EFIN ----
            EFIN.setComponentPopupMenu(BTD);
            EFIN.setName("EFIN");
            P_SEL0.add(EFIN);
            EFIN.setBounds(125, 33, 40, EFIN.getPreferredSize().height);
          }

          //---- OBJ_38 ----
          OBJ_38.setTitle("Type r\u00e9approvisionnement");
          OBJ_38.setName("OBJ_38");

          //---- label1 ----
          label1.setText("Si\u00e8ge");
          label1.setName("label1");

          //---- label2 ----
          label2.setText("Si\u00e8ge");
          label2.setName("label2");

          //---- label3 ----
          label3.setText("Agence");
          label3.setName("label3");

          //---- label4 ----
          label4.setText("Agence");
          label4.setName("label4");

          //---- REA01 ----
          REA01.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          REA01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REA01.setName("REA01");

          //---- REA03 ----
          REA03.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          REA03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REA03.setName("REA03");

          //---- REA02 ----
          REA02.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          REA02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REA02.setName("REA02");

          //---- REA04 ----
          REA04.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          REA04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REA04.setName("REA04");

          //---- REA05 ----
          REA05.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          REA05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REA05.setName("REA05");

          //---- REA06 ----
          REA06.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          REA06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REA06.setName("REA06");

          //---- AGE01 ----
          AGE01.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          AGE01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AGE01.setName("AGE01");

          //---- AGE02 ----
          AGE02.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          AGE02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AGE02.setName("AGE02");

          //---- AGE03 ----
          AGE03.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          AGE03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AGE03.setName("AGE03");

          //---- AGE04 ----
          AGE04.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          AGE04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AGE04.setName("AGE04");

          //---- AGE05 ----
          AGE05.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          AGE05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AGE05.setName("AGE05");

          //---- AGE06 ----
          AGE06.setModel(new DefaultComboBoxModel(new String[] {
            "Pas de mise \u00e0 jour",
            "Rupture sur stock mini",
            "Sur conso moyenne",
            "R\u00e9appro. manuel",
            "sur conso.pr\u00e9vues",
            "Conso. moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plaf. couvert.)",
            "Compl\u00e9ment stock max",
            "Plaf. couverture/Mag",
            "Produit non g\u00e9r\u00e9"
          }));
          AGE06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AGE06.setName("AGE06");

          //---- OBJ_40 ----
          OBJ_40.setTitle("");
          OBJ_40.setName("OBJ_40");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(CODMAG, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 387, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CODFRS, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(FDSNGE, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(124, 124, 124)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(105, 105, 105)
                .addComponent(label3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(185, 185, 185)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(105, 105, 105)
                .addComponent(label4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(REA03, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(REA02, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(REA01, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(AGE01, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AGE02, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AGE03, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(REA04, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(REA06, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(REA05, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(AGE04, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AGE05, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AGE06, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CODMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CODFRS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(FDSNGE, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(label1)
                  .addComponent(label3)
                  .addComponent(label2)
                  .addComponent(label4))
                .addGap(4, 4, 4)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(REA03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(REA02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(REA01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AGE01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(AGE02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(AGE03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(REA04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addComponent(REA06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(REA05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(AGE04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(AGE05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(AGE06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_74;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JLabel OBJ_77;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_32;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_19;
  private XRiCheckBox FDSNGE;
  private JLabel OBJ_42;
  private JLabel OBJ_43;
  private XRiTextField CODFRS;
  private XRiTextField CODMAG;
  private XRiCheckBox WTOU2;
  private JPanel P_SEL1;
  private XRiTextField EDEBA;
  private XRiTextField EFINA;
  private JLabel OBJ_36;
  private JLabel OBJ_39;
  private XRiCheckBox WTOU1;
  private JPanel P_SEL0;
  private JLabel OBJ_27;
  private JLabel OBJ_30;
  private XRiTextField EDEB;
  private XRiTextField EFIN;
  private JXTitledSeparator OBJ_38;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiComboBox REA01;
  private XRiComboBox REA03;
  private XRiComboBox REA02;
  private XRiComboBox REA04;
  private XRiComboBox REA05;
  private XRiComboBox REA06;
  private XRiComboBox AGE01;
  private XRiComboBox AGE02;
  private XRiComboBox AGE03;
  private XRiComboBox AGE04;
  private XRiComboBox AGE05;
  private XRiComboBox AGE06;
  private JXTitledSeparator OBJ_40;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
