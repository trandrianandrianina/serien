
package ri.serien.libecranrpg.sgvx.SGVX34FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX34FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] REPON2_Value = { "NON", "OUI", "TIR", };
  
  public SGVX34FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPON2.setValeurs(REPON2_Value, null);
    EFAM_tous.setValeursSelection("**", "");
    EFOUR.setValeursSelection("**", "");
    EDTSTN.setValeursSelection("1", "0");
    REPON1.setValeursSelection("OUI", "NON");
    EDTMIN.setValeursSelection("O", "");
    EDTSMN.setValeursSelection("1", "0");
    EDTCND.setValeursSelection("O", "");
    ESFAM_tous.setValeursSelection("**", "");
    WTOUM.setValeursSelection("**", "");
    WLB1.setValeursSelection("X", "");
    WLB2.setValeursSelection("X", "");
    WLB3.setValeursSelection("X", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    TZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP2@")).trim());
    TZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP3@")).trim());
    TZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP4@")).trim());
    TZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP5@")).trim());
    TZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    
    EFAM_tous.setSelected(lexique.HostFieldGetData("EFAM").trim().equalsIgnoreCase("**"));
    ESFAM_tous.setSelected(lexique.HostFieldGetData("ESFAM").trim().equalsIgnoreCase("**"));
    
    OBJ_66.setVisible(lexique.isPresent("DATINV"));
    P_SEL0.setVisible(!WTOUM.isSelected());
    P_SEL1.setVisible(!EFOUR.isSelected());
    P_SEL3.setVisible(!ESFAM_tous.isSelected());
    P_SEL2.setVisible(!EFAM_tous.isSelected());
    OBJ_42.setVisible(lexique.isPresent("DATINV"));
    // REPON2.setEnabled( lexique.isPresent("REPON2"));
    
    OBJ_45.setVisible(lexique.isPresent("ADS1"));
    OBJ_69.setVisible(lexique.isPresent("EFOUR"));
    OBJ_43.setVisible(lexique.isPresent("WTOUM"));
    panel1.setVisible(lexique.isTrue("95"));
    panel2.setVisible(!panel1.isVisible());
    panel6.setVisible(panel1.isVisible());
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (EFAM_tous.isSelected()) {
      lexique.HostFieldPutData("EFAM", 0, "**");
    }
    if (ESFAM_tous.isSelected()) {
      lexique.HostFieldPutData("ESFAM", 0, "**");
    }
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void EFOURActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void EFAMActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
    ESFAM_tous.setSelected(false);
    lexique.HostFieldPutData("ESFAM", 0, "");
  }
  
  private void ESFAMActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
    EFAM_tous.setSelected(false);
    lexique.HostFieldPutData("EFAM", 0, "");
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    panel6 = new JPanel();
    OBJ_44 = new JXTitledSeparator();
    WMAG = new XRiTextField();
    label1 = new JLabel();
    panel2 = new JPanel();
    P_SEL0 = new JPanel();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    WTOUM = new XRiCheckBox();
    OBJ_43 = new JXTitledSeparator();
    panel1 = new JPanel();
    OBJ_42 = new JXTitledSeparator();
    OBJ_66 = new JLabel();
    DATINV = new XRiCalendrier();
    OBJ_45 = new JXTitledSeparator();
    ADS1 = new XRiTextField();
    ADS2 = new XRiTextField();
    ADS3 = new XRiTextField();
    ADS4 = new XRiTextField();
    OBJ_84 = new JXTitledSeparator();
    P_SEL3 = new JPanel();
    OBJ_91 = new JLabel();
    ESFAM = new XRiTextField();
    ESFAM_tous = new XRiCheckBox();
    OBJ_37 = new JXTitledSeparator();
    OBJ_101 = new JXTitledSeparator();
    REPON2 = new XRiComboBox();
    OBJ_108 = new JLabel();
    EDTCND = new XRiCheckBox();
    EDTSMN = new XRiCheckBox();
    EDTMIN = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    EDTSTN = new XRiCheckBox();
    OBJ_114 = new JLabel();
    WLB1 = new XRiCheckBox();
    WLB2 = new XRiCheckBox();
    WLB3 = new XRiCheckBox();
    panel3 = new JPanel();
    OBJ_69 = new JXTitledSeparator();
    P_SEL1 = new JPanel();
    EFOUR_doublon_48 = new XRiTextField();
    OBJ_72 = new JLabel();
    EFOUR = new XRiCheckBox();
    panel4 = new JPanel();
    OBJ_70 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    EFAM = new XRiTextField();
    OBJ_75 = new JLabel();
    EFAM_tous = new XRiCheckBox();
    panel5 = new JPanel();
    OBJ_83 = new JXTitledSeparator();
    TZP2 = new JLabel();
    TZP3 = new JLabel();
    TZP4 = new JLabel();
    TZP5 = new JLabel();
    TZP1 = new JLabel();
    ZP1 = new XRiTextField();
    ZP2 = new XRiTextField();
    ZP3 = new XRiTextField();
    ZP4 = new XRiTextField();
    ZP5 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    MA12_doublon_77 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 590));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 590));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_44 ----
            OBJ_44.setTitle(" ");
            OBJ_44.setName("OBJ_44");
            panel6.add(OBJ_44);
            OBJ_44.setBounds(5, 0, 805, OBJ_44.getPreferredSize().height);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            panel6.add(WMAG);
            WMAG.setBounds(180, 35, 34, WMAG.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Magasin s\u00e9lectionn\u00e9");
            label1.setName("label1");
            panel6.add(label1);
            label1.setBounds(25, 37, 130, 25);
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(141, 10, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(175, 10, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(209, 10, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(243, 10, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(277, 10, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(311, 10, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(345, 10, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(379, 10, 34, MA12.getPreferredSize().height);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(5, 10, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(39, 10, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(73, 10, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(107, 10, 34, MA04.getPreferredSize().height);
            }
            panel2.add(P_SEL0);
            P_SEL0.setBounds(175, 25, 420, 45);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            panel2.add(WTOUM);
            WTOUM.setBounds(20, 38, 150, 18);

            //---- OBJ_43 ----
            OBJ_43.setTitle("Codes magasins");
            OBJ_43.setName("OBJ_43");
            panel2.add(OBJ_43);
            OBJ_43.setBounds(5, 0, 805, OBJ_43.getPreferredSize().height);
          }

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_42 ----
            OBJ_42.setTitle("Bordereau d'inventaire");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(5, 0, 390, OBJ_42.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("G\u00e9n\u00e9r\u00e9 le");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(21, 30, 75, 18);

            //---- DATINV ----
            DATINV.setComponentPopupMenu(BTD);
            DATINV.setName("DATINV");
            panel1.add(DATINV);
            DATINV.setBounds(100, 25, 105, DATINV.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setTitle("Adresse de stockage");
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(415, 0, 395, OBJ_45.getPreferredSize().height);

            //---- ADS1 ----
            ADS1.setComponentPopupMenu(BTD);
            ADS1.setName("ADS1");
            panel1.add(ADS1);
            ADS1.setBounds(430, 25, 40, ADS1.getPreferredSize().height);

            //---- ADS2 ----
            ADS2.setComponentPopupMenu(BTD);
            ADS2.setName("ADS2");
            panel1.add(ADS2);
            ADS2.setBounds(475, 25, 40, ADS2.getPreferredSize().height);

            //---- ADS3 ----
            ADS3.setComponentPopupMenu(BTD);
            ADS3.setName("ADS3");
            panel1.add(ADS3);
            ADS3.setBounds(520, 25, 40, ADS3.getPreferredSize().height);

            //---- ADS4 ----
            ADS4.setComponentPopupMenu(BTD);
            ADS4.setName("ADS4");
            panel1.add(ADS4);
            ADS4.setBounds(565, 25, 40, ADS4.getPreferredSize().height);
          }

          //---- OBJ_84 ----
          OBJ_84.setTitle("ou Sous-famille");
          OBJ_84.setName("OBJ_84");

          //======== P_SEL3 ========
          {
            P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL3.setOpaque(false);
            P_SEL3.setName("P_SEL3");
            P_SEL3.setLayout(null);

            //---- OBJ_91 ----
            OBJ_91.setText("Code");
            OBJ_91.setName("OBJ_91");
            P_SEL3.add(OBJ_91);
            OBJ_91.setBounds(10, 14, 45, 20);

            //---- ESFAM ----
            ESFAM.setComponentPopupMenu(BTD);
            ESFAM.setName("ESFAM");
            P_SEL3.add(ESFAM);
            ESFAM.setBounds(55, 10, 60, ESFAM.getPreferredSize().height);
          }

          //---- ESFAM_tous ----
          ESFAM_tous.setText("S\u00e9lection compl\u00e8te");
          ESFAM_tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ESFAM_tous.setName("ESFAM_tous");
          ESFAM_tous.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ESFAMActionPerformed(e);
            }
          });

          //---- OBJ_37 ----
          OBJ_37.setTitle("");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_101 ----
          OBJ_101.setTitle("");
          OBJ_101.setName("OBJ_101");

          //---- REPON2 ----
          REPON2.setModel(new DefaultComboBoxModel(new String[] {
            "Pas d'interligne",
            "Interligne simple",
            "Interligne tirets"
          }));
          REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON2.setName("REPON2");

          //---- OBJ_108 ----
          OBJ_108.setText("Num\u00e9ro libell\u00e9 compl\u00e9mentaire d'article  \u00e0 \u00e9diter");
          OBJ_108.setName("OBJ_108");

          //---- EDTCND ----
          EDTCND.setText("Edition du conditionnement");
          EDTCND.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTCND.setName("EDTCND");

          //---- EDTSMN ----
          EDTSMN.setText("Edition si stock mini est nul");
          EDTSMN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTSMN.setName("EDTSMN");

          //---- EDTMIN ----
          EDTMIN.setText("Edition du stock mini");
          EDTMIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTMIN.setName("EDTMIN");

          //---- REPON1 ----
          REPON1.setText("Edition du stock th\u00e9orique");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- EDTSTN ----
          EDTSTN.setText("Edition si stock nul");
          EDTSTN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTSTN.setName("EDTSTN");

          //---- OBJ_114 ----
          OBJ_114.setText("Pr\u00e9sentation \u00e9dition");
          OBJ_114.setName("OBJ_114");

          //---- WLB1 ----
          WLB1.setComponentPopupMenu(BTD);
          WLB1.setText("2");
          WLB1.setName("WLB1");

          //---- WLB2 ----
          WLB2.setComponentPopupMenu(BTD);
          WLB2.setText("3");
          WLB2.setName("WLB2");

          //---- WLB3 ----
          WLB3.setComponentPopupMenu(BTD);
          WLB3.setText("4");
          WLB3.setName("WLB3");

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_69 ----
            OBJ_69.setTitle("Fournisseur");
            OBJ_69.setName("OBJ_69");
            panel3.add(OBJ_69);
            OBJ_69.setBounds(5, 0, 390, OBJ_69.getPreferredSize().height);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- EFOUR_doublon_48 ----
              EFOUR_doublon_48.setComponentPopupMenu(BTD);
              EFOUR_doublon_48.setName("EFOUR_doublon_48");
              P_SEL1.add(EFOUR_doublon_48);
              EFOUR_doublon_48.setBounds(60, 10, 80, EFOUR_doublon_48.getPreferredSize().height);

              //---- OBJ_72 ----
              OBJ_72.setText("Code");
              OBJ_72.setName("OBJ_72");
              P_SEL1.add(OBJ_72);
              OBJ_72.setBounds(15, 15, 45, 18);
            }
            panel3.add(P_SEL1);
            P_SEL1.setBounds(175, 25, 155, 46);

            //---- EFOUR ----
            EFOUR.setText("S\u00e9lection compl\u00e8te");
            EFOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EFOUR.setName("EFOUR");
            EFOUR.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EFOURActionPerformed(e);
              }
            });
            panel3.add(EFOUR);
            EFOUR.setBounds(20, 37, 155, 22);
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_70 ----
            OBJ_70.setTitle("Famille");
            OBJ_70.setName("OBJ_70");
            panel4.add(OBJ_70);
            OBJ_70.setBounds(5, 0, 395, OBJ_70.getPreferredSize().height);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- EFAM ----
              EFAM.setComponentPopupMenu(BTD);
              EFAM.setName("EFAM");
              P_SEL2.add(EFAM);
              EFAM.setBounds(55, 10, 45, EFAM.getPreferredSize().height);

              //---- OBJ_75 ----
              OBJ_75.setText("Code");
              OBJ_75.setName("OBJ_75");
              P_SEL2.add(OBJ_75);
              OBJ_75.setBounds(10, 15, 45, 18);
            }
            panel4.add(P_SEL2);
            P_SEL2.setBounds(180, 25, 130, 45);

            //---- EFAM_tous ----
            EFAM_tous.setText("S\u00e9lection compl\u00e8te");
            EFAM_tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EFAM_tous.setName("EFAM_tous");
            EFAM_tous.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EFAMActionPerformed(e);
              }
            });
            panel4.add(EFAM_tous);
            EFAM_tous.setBounds(20, 38, 154, EFAM_tous.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- OBJ_83 ----
            OBJ_83.setTitle("Zones personnalis\u00e9es disponibles");
            OBJ_83.setName("OBJ_83");
            panel5.add(OBJ_83);
            OBJ_83.setBounds(5, 0, 390, OBJ_83.getPreferredSize().height);

            //---- TZP2 ----
            TZP2.setText("@TZP2@");
            TZP2.setComponentPopupMenu(BTD);
            TZP2.setName("TZP2");
            panel5.add(TZP2);
            TZP2.setBounds(105, 39, 35, 20);

            //---- TZP3 ----
            TZP3.setText("@TZP3@");
            TZP3.setComponentPopupMenu(BTD);
            TZP3.setName("TZP3");
            panel5.add(TZP3);
            TZP3.setBounds(190, 39, 35, 20);

            //---- TZP4 ----
            TZP4.setText("@TZP4@");
            TZP4.setComponentPopupMenu(BTD);
            TZP4.setName("TZP4");
            panel5.add(TZP4);
            TZP4.setBounds(20, 69, 35, 20);

            //---- TZP5 ----
            TZP5.setText("@TZP5@");
            TZP5.setComponentPopupMenu(BTD);
            TZP5.setName("TZP5");
            panel5.add(TZP5);
            TZP5.setBounds(105, 69, 35, 20);

            //---- TZP1 ----
            TZP1.setText("@TZP1@");
            TZP1.setComponentPopupMenu(BTD);
            TZP1.setName("TZP1");
            panel5.add(TZP1);
            TZP1.setBounds(20, 39, 35, 20);

            //---- ZP1 ----
            ZP1.setComponentPopupMenu(BTD);
            ZP1.setName("ZP1");
            panel5.add(ZP1);
            ZP1.setBounds(60, 35, 34, ZP1.getPreferredSize().height);

            //---- ZP2 ----
            ZP2.setComponentPopupMenu(BTD);
            ZP2.setName("ZP2");
            panel5.add(ZP2);
            ZP2.setBounds(145, 35, 34, ZP2.getPreferredSize().height);

            //---- ZP3 ----
            ZP3.setComponentPopupMenu(BTD);
            ZP3.setName("ZP3");
            panel5.add(ZP3);
            ZP3.setBounds(230, 35, 34, ZP3.getPreferredSize().height);

            //---- ZP4 ----
            ZP4.setComponentPopupMenu(BTD);
            ZP4.setName("ZP4");
            panel5.add(ZP4);
            ZP4.setBounds(60, 65, 34, ZP4.getPreferredSize().height);

            //---- ZP5 ----
            ZP5.setComponentPopupMenu(BTD);
            ZP5.setName("ZP5");
            panel5.add(ZP5);
            ZP5.setBounds(145, 65, 34, ZP5.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 820, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 820, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 820, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(ESFAM_tous, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_101, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(EDTMIN, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(EDTCND, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(EDTSTN, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(EDTSMN, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_108, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WLB1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(WLB2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(WLB3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(9, 9, 9)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(ESFAM_tous, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))))
                .addComponent(OBJ_101, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EDTMIN, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EDTCND, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(EDTSTN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EDTSMN, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_108, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLB3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- MA12_doublon_77 ----
    MA12_doublon_77.setComponentPopupMenu(BTD);
    MA12_doublon_77.setName("MA12_doublon_77");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JPanel panel6;
  private JXTitledSeparator OBJ_44;
  private XRiTextField WMAG;
  private JLabel label1;
  private JPanel panel2;
  private JPanel P_SEL0;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiCheckBox WTOUM;
  private JXTitledSeparator OBJ_43;
  private JPanel panel1;
  private JXTitledSeparator OBJ_42;
  private JLabel OBJ_66;
  private XRiCalendrier DATINV;
  private JXTitledSeparator OBJ_45;
  private XRiTextField ADS1;
  private XRiTextField ADS2;
  private XRiTextField ADS3;
  private XRiTextField ADS4;
  private JXTitledSeparator OBJ_84;
  private JPanel P_SEL3;
  private JLabel OBJ_91;
  private XRiTextField ESFAM;
  private XRiCheckBox ESFAM_tous;
  private JXTitledSeparator OBJ_37;
  private JXTitledSeparator OBJ_101;
  private XRiComboBox REPON2;
  private JLabel OBJ_108;
  private XRiCheckBox EDTCND;
  private XRiCheckBox EDTSMN;
  private XRiCheckBox EDTMIN;
  private XRiCheckBox REPON1;
  private XRiCheckBox EDTSTN;
  private JLabel OBJ_114;
  private XRiCheckBox WLB1;
  private XRiCheckBox WLB2;
  private XRiCheckBox WLB3;
  private JPanel panel3;
  private JXTitledSeparator OBJ_69;
  private JPanel P_SEL1;
  private XRiTextField EFOUR_doublon_48;
  private JLabel OBJ_72;
  private XRiCheckBox EFOUR;
  private JPanel panel4;
  private JXTitledSeparator OBJ_70;
  private JPanel P_SEL2;
  private XRiTextField EFAM;
  private JLabel OBJ_75;
  private XRiCheckBox EFAM_tous;
  private JPanel panel5;
  private JXTitledSeparator OBJ_83;
  private JLabel TZP2;
  private JLabel TZP3;
  private JLabel TZP4;
  private JLabel TZP5;
  private JLabel TZP1;
  private XRiTextField ZP1;
  private XRiTextField ZP2;
  private XRiTextField ZP3;
  private XRiTextField ZP4;
  private XRiTextField ZP5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private XRiTextField MA12_doublon_77;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
