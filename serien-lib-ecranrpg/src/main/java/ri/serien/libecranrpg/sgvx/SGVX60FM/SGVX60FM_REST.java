
package ri.serien.libecranrpg.sgvx.SGVX60FM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVX60FM_REST extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX60FM_REST(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    setTitle("Restrictions magasins");
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    bouton_erreurs.setIcon(lexique.chargerImage("images/alertes.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    WMATYP = new XRiTextField();
    WMASMA = new XRiTextField();
    WMAAMA = new XRiTextField();

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      popupMenu1.add(menuItem1);
    }

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(505, 165));
      p_principal.setMinimumSize(new Dimension(505, 165));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label1 ----
          label1.setText("Type");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(25, 23, 95, 22);

          //---- label2 ----
          label2.setText("Zone g\u00e9ographique");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(25, 63, 140, 22);

          //---- label3 ----
          label3.setText("Activit\u00e9");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(25, 103, 95, 22);

          //---- WMATYP ----
          WMATYP.setComponentPopupMenu(null);
          WMATYP.setName("WMATYP");
          panel1.add(WMATYP);
          WMATYP.setBounds(200, 20, 34, WMATYP.getPreferredSize().height);

          //---- WMASMA ----
          WMASMA.setComponentPopupMenu(null);
          WMASMA.setName("WMASMA");
          panel1.add(WMASMA);
          WMASMA.setBounds(200, 60, 34, WMASMA.getPreferredSize().height);

          //---- WMAAMA ----
          WMAAMA.setComponentPopupMenu(null);
          WMAAMA.setName("WMAAMA");
          panel1.add(WMAAMA);
          WMAAMA.setBounds(200, 100, 34, WMAAMA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 305, 145);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField WMATYP;
  private XRiTextField WMASMA;
  private XRiTextField WMAAMA;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
