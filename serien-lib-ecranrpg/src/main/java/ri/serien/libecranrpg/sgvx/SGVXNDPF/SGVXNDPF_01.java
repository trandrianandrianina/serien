
package ri.serien.libecranrpg.sgvx.SGVXNDPF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SGVXNDPF_01 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXNDPF_01(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WREP.setValeurs("OUI", WREP_GRP);
    WREP_NON.setValeurs("NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WFAM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFAM@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFALIB@")).trim());
    WGRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WGRP@")).trim());
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WGRLIB@")).trim());
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFANDP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // OBJ_13.setEnabled( lexique.isPresent("WREP"));
    // OBJ_13.setSelected(lexique.HostFieldGetData("WREP").equalsIgnoreCase("NON"));
    // OBJ_12.setEnabled( lexique.isPresent("WREP"));
    // OBJ_12.setSelected(lexique.HostFieldGetData("WREP").equalsIgnoreCase("OUI"));
    OBJ_18.setVisible(lexique.isPresent("WFANDP"));
    OBJ_14.setVisible(lexique.isPresent("WFALIB"));
    OBJ_16.setVisible(lexique.isPresent("WGRLIB"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OBJ_13.isSelected())
    // lexique.HostFieldPutData("WREP", 0, "NON");
    // if (OBJ_12.isSelected())
    // lexique.HostFieldPutData("WREP", 0, "OUI");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_9 = new JLabel();
    WFAM = new RiZoneSortie();
    OBJ_14 = new RiZoneSortie();
    OBJ_15 = new JLabel();
    WGRP = new RiZoneSortie();
    OBJ_16 = new RiZoneSortie();
    OBJ_17 = new JLabel();
    OBJ_18 = new RiZoneSortie();
    WREP = new XRiRadioButton();
    WREP_NON = new XRiRadioButton();
    WREP_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(720, 290));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_9 ----
          OBJ_9.setText("Vous venez de modifier la nomenclature douani\u00e8re de la famille");
          OBJ_9.setFont(OBJ_9.getFont().deriveFont(OBJ_9.getFont().getSize() + 3f));
          OBJ_9.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_9.setName("OBJ_9");
          panel1.add(OBJ_9);
          OBJ_9.setBounds(25, 15, 455, 20);

          //---- WFAM ----
          WFAM.setText("@WFAM@");
          WFAM.setName("WFAM");
          panel1.add(WFAM);
          WFAM.setBounds(80, 40, 44, 24);

          //---- OBJ_14 ----
          OBJ_14.setText("@WFALIB@");
          OBJ_14.setName("OBJ_14");
          panel1.add(OBJ_14);
          OBJ_14.setBounds(130, 40, 300, 24);

          //---- OBJ_15 ----
          OBJ_15.setText("D\u00e9sirez vous mettre \u00e0 jour toutes les familles du groupe");
          OBJ_15.setFont(OBJ_15.getFont().deriveFont(OBJ_15.getFont().getSize() + 3f));
          OBJ_15.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_15.setName("OBJ_15");
          panel1.add(OBJ_15);
          OBJ_15.setBounds(25, 73, 455, 20);

          //---- WGRP ----
          WGRP.setText("@WGRP@");
          WGRP.setName("WGRP");
          panel1.add(WGRP);
          WGRP.setBounds(80, 100, 24, WGRP.getPreferredSize().height);

          //---- OBJ_16 ----
          OBJ_16.setText("@WGRLIB@");
          OBJ_16.setName("OBJ_16");
          panel1.add(OBJ_16);
          OBJ_16.setBounds(110, 100, 320, 24);

          //---- OBJ_17 ----
          OBJ_17.setText("avec la nouvelle nomenclature ?");
          OBJ_17.setFont(OBJ_17.getFont().deriveFont(OBJ_17.getFont().getSize() + 3f));
          OBJ_17.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(25, 131, 455, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("@WFANDP@");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(186, 158, 133, 24);

          //---- WREP ----
          WREP.setText("OUI");
          WREP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WREP.setName("WREP");
          panel1.add(WREP);
          WREP.setBounds(160, 220, 66, 20);

          //---- WREP_NON ----
          WREP_NON.setText("NON");
          WREP_NON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WREP_NON.setName("WREP_NON");
          panel1.add(WREP_NON);
          WREP_NON.setBounds(275, 220, 66, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 519, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(16, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 261, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(14, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- WREP_GRP ----
    WREP_GRP.add(WREP);
    WREP_GRP.add(WREP_NON);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_9;
  private RiZoneSortie WFAM;
  private RiZoneSortie OBJ_14;
  private JLabel OBJ_15;
  private RiZoneSortie WGRP;
  private RiZoneSortie OBJ_16;
  private JLabel OBJ_17;
  private RiZoneSortie OBJ_18;
  private XRiRadioButton WREP;
  private XRiRadioButton WREP_NON;
  private ButtonGroup WREP_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
