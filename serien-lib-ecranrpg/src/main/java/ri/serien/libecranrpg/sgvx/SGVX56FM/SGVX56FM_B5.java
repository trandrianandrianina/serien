
package ri.serien.libecranrpg.sgvx.SGVX56FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX56FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX56FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    OPT4.setValeurs("4", "RB");
    OPT2.setValeurs("2", "RB");
    OPT3.setValeurs("3", "RB");
    OPT1.setValeurs("1", "RB");
    EDTMIN.setValeursSelection("1", " ");
    MVTTRS.setValeursSelection("X", " ");
    SEL4.setValeursSelection("X", " ");
    SEL3.setValeursSelection("X", " ");
    SEL1.setValeursSelection("X", " ");
    SEL2.setValeursSelection("X", " ");
    WTOU.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("OUI", "NON");
    WTOUM.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    MA01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA01@")).trim());
    MA02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA02@")).trim());
    MA03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA03@")).trim());
    MA04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA04@")).trim());
    MA05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA05@")).trim());
    MA06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA06@")).trim());
    MA07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA07@")).trim());
    MA08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA08@")).trim());
    MA09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA09@")).trim());
    MA10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA10@")).trim());
    MA11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA11@")).trim());
    MA12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA12@")).trim());
    OBJ_60.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBELLE1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    panel3.setVisible((lexique.HostFieldGetData("RB").equalsIgnoreCase("4")) & lexique.isPresent("PERDEB"));
    P_SEL7.setVisible((!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**")) && lexique.isTrue("93"));
    P_SEL6.setVisible((!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"))
        && (!lexique.isTrue("93") && !lexique.isTrue("94") && !lexique.isTrue("95") && !lexique.isTrue("96")));
    P_SEL5.setVisible((!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**")) && lexique.isTrue("94"));
    P_SEL8.setVisible((!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**")) && lexique.isTrue("95"));
    P_SEL9.setVisible((!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**")) && lexique.isTrue("96"));
    P_SEL0.setVisible(!(lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**") || lexique.isTrue("91")));
    OBJ_44.setVisible(lexique.isPresent("MA01"));
    
    if (lexique.isTrue("96")) {
      OBJ_60.setTitle("Plage de sous-familles");
    }
    if (lexique.isTrue("95")) {
      OBJ_60.setTitle("Plage de fournisseurs");
    }
    if (lexique.isTrue("94")) {
      OBJ_60.setTitle("Plage de familles");
    }
    if (lexique.isTrue("93")) {
      OBJ_60.setTitle("Plage 2e clés articles");
    }
    if (!lexique.isTrue("93") && !lexique.isTrue("94") && !lexique.isTrue("95") && !lexique.isTrue("96")) {
      OBJ_60.setTitle("Plage d'articles");
    }
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
  }
  
  private void OPT4MouseClicked(MouseEvent e) {
    panel3.setVisible(OPT4.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_44 = new JXTitledSeparator();
    WTOUM = new XRiCheckBox();
    P_SEL0 = new JPanel();
    MA01 = new RiZoneSortie();
    MA02 = new RiZoneSortie();
    MA03 = new RiZoneSortie();
    MA04 = new RiZoneSortie();
    MA05 = new RiZoneSortie();
    MA06 = new RiZoneSortie();
    MA07 = new RiZoneSortie();
    MA08 = new RiZoneSortie();
    MA09 = new RiZoneSortie();
    MA10 = new RiZoneSortie();
    MA11 = new RiZoneSortie();
    MA12 = new RiZoneSortie();
    REPON1 = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    OBJ_60 = new JXTitledSeparator();
    P_SEL8 = new JPanel();
    OBJ_80 = new JLabel();
    FRSDEB = new XRiTextField();
    OBJ_85 = new JLabel();
    FRSFIN = new XRiTextField();
    P_SEL5 = new JPanel();
    OBJ_79 = new JLabel();
    EDEB = new XRiTextField();
    OBJ_84 = new JLabel();
    EFIN = new XRiTextField();
    P_SEL6 = new JPanel();
    EDEBA = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_83 = new JLabel();
    EFINA = new XRiTextField();
    P_SEL7 = new JPanel();
    OBJ_78 = new JLabel();
    DEBIAK = new XRiTextField();
    OBJ_82 = new JLabel();
    FINIAK = new XRiTextField();
    P_SEL9 = new JPanel();
    OBJ_81 = new JLabel();
    DEBIAE = new XRiTextField();
    OBJ_86 = new JLabel();
    FINIAE = new XRiTextField();
    OBJ_107 = new JXTitledSeparator();
    OBJ_94 = new JXTitledSeparator();
    panel1 = new JPanel();
    SEL2 = new XRiCheckBox();
    SEL1 = new XRiCheckBox();
    SEL3 = new XRiCheckBox();
    SEL4 = new XRiCheckBox();
    OBJ_89 = new JXTitledSeparator();
    panel2 = new JPanel();
    OPT1 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT4 = new XRiRadioButton();
    OBJ_108 = new JLabel();
    panel3 = new JPanel();
    OBJ_111 = new JLabel();
    PERDEB = new XRiCalendrier();
    OBJ_113 = new JLabel();
    PERFIN = new XRiCalendrier();
    OBJ_39 = new JXTitledSeparator();
    OBJ_96 = new JLabel();
    WABC = new XRiTextField();
    WABC1 = new XRiTextField();
    WABC2 = new XRiTextField();
    WABC3 = new XRiTextField();
    SEL5 = new XRiTextField();
    EDTMIN = new XRiCheckBox();
    MVTTRS = new XRiCheckBox();
    OBJ_97 = new JLabel();
    WREA1 = new XRiTextField();
    WREA2 = new XRiTextField();
    WREA3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1010, 690));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Choix du papier");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(810, 580));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel4.add(sep_etablissement);
            sep_etablissement.setBounds(30, 10, 735, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel4.add(z_dgnom_);
            z_dgnom_.setBounds(190, 35, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel4.add(z_wencx_);
            z_wencx_.setBounds(190, 65, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel4.add(z_etablissement_);
            z_etablissement_.setBounds(45, 55, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel4.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(90, 50), bouton_etablissement.getPreferredSize()));

            //---- OBJ_44 ----
            OBJ_44.setTitle("Codes magasins");
            OBJ_44.setName("OBJ_44");
            panel4.add(OBJ_44);
            OBJ_44.setBounds(30, 105, 735, OBJ_44.getPreferredSize().height);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setComponentPopupMenu(null);
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            panel4.add(WTOUM);
            WTOUM.setBounds(45, 145, 145, WTOUM.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(null);
              MA01.setText("@MA01@");
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(7, 10, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(null);
              MA02.setText("@MA02@");
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(44, 10, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(null);
              MA03.setText("@MA03@");
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(80, 10, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(null);
              MA04.setText("@MA04@");
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(116, 10, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(null);
              MA05.setText("@MA05@");
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(152, 10, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(null);
              MA06.setText("@MA06@");
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(189, 10, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(null);
              MA07.setText("@MA07@");
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(225, 10, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(null);
              MA08.setText("@MA08@");
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(261, 10, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(null);
              MA09.setText("@MA09@");
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(298, 10, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(null);
              MA10.setText("@MA10@");
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(334, 10, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(null);
              MA11.setText("@MA11@");
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(370, 10, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(null);
              MA12.setText("@MA12@");
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(407, 10, 34, MA12.getPreferredSize().height);
            }
            panel4.add(P_SEL0);
            P_SEL0.setBounds(190, 130, 450, 45);

            //---- REPON1 ----
            REPON1.setText("Totalisation");
            REPON1.setComponentPopupMenu(null);
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setName("REPON1");
            panel4.add(REPON1);
            REPON1.setBounds(45, 265, 96, REPON1.getPreferredSize().height);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setComponentPopupMenu(null);
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel4.add(WTOU);
            WTOU.setBounds(45, 230, 145, WTOU.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setTitle("@LIBELLE1@");
            OBJ_60.setName("OBJ_60");
            panel4.add(OBJ_60);
            OBJ_60.setBounds(30, 190, 735, OBJ_60.getPreferredSize().height);

            //======== P_SEL8 ========
            {
              P_SEL8.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL8.setOpaque(false);
              P_SEL8.setName("P_SEL8");
              P_SEL8.setLayout(null);

              //---- OBJ_80 ----
              OBJ_80.setText("Code de d\u00e9but");
              OBJ_80.setName("OBJ_80");
              P_SEL8.add(OBJ_80);
              OBJ_80.setBounds(15, 15, 100, 18);

              //---- FRSDEB ----
              FRSDEB.setComponentPopupMenu(BTD);
              FRSDEB.setName("FRSDEB");
              P_SEL8.add(FRSDEB);
              FRSDEB.setBounds(120, 14, 80, FRSDEB.getPreferredSize().height);

              //---- OBJ_85 ----
              OBJ_85.setText("Code de fin");
              OBJ_85.setName("OBJ_85");
              P_SEL8.add(OBJ_85);
              OBJ_85.setBounds(275, 15, 80, 18);

              //---- FRSFIN ----
              FRSFIN.setComponentPopupMenu(BTD);
              FRSFIN.setName("FRSFIN");
              P_SEL8.add(FRSFIN);
              FRSFIN.setBounds(360, 14, 80, FRSFIN.getPreferredSize().height);

              //======== P_SEL5 ========
              {
                P_SEL5.setBorder(new BevelBorder(BevelBorder.LOWERED));
                P_SEL5.setOpaque(false);
                P_SEL5.setName("P_SEL5");
                P_SEL5.setLayout(null);

                //---- OBJ_79 ----
                OBJ_79.setText("Famille de d\u00e9but");
                OBJ_79.setName("OBJ_79");
                P_SEL5.add(OBJ_79);
                OBJ_79.setBounds(15, 15, 120, 18);

                //---- EDEB ----
                EDEB.setComponentPopupMenu(BTD);
                EDEB.setName("EDEB");
                P_SEL5.add(EDEB);
                EDEB.setBounds(135, 14, 40, EDEB.getPreferredSize().height);

                //---- OBJ_84 ----
                OBJ_84.setText("Famille de fin");
                OBJ_84.setName("OBJ_84");
                P_SEL5.add(OBJ_84);
                OBJ_84.setBounds(275, 15, 95, 18);

                //---- EFIN ----
                EFIN.setComponentPopupMenu(BTD);
                EFIN.setName("EFIN");
                P_SEL5.add(EFIN);
                EFIN.setBounds(370, 14, 40, EFIN.getPreferredSize().height);

                //======== P_SEL6 ========
                {
                  P_SEL6.setBorder(new BevelBorder(BevelBorder.LOWERED));
                  P_SEL6.setOpaque(false);
                  P_SEL6.setName("P_SEL6");
                  P_SEL6.setLayout(null);

                  //---- EDEBA ----
                  EDEBA.setComponentPopupMenu(BTD);
                  EDEBA.setName("EDEBA");
                  P_SEL6.add(EDEBA);
                  EDEBA.setBounds(55, 14, 210, EDEBA.getPreferredSize().height);

                  //---- OBJ_77 ----
                  OBJ_77.setText("De");
                  OBJ_77.setName("OBJ_77");
                  P_SEL6.add(OBJ_77);
                  OBJ_77.setBounds(15, 15, 30, 18);

                  //---- OBJ_83 ----
                  OBJ_83.setText("\u00e0");
                  OBJ_83.setHorizontalAlignment(SwingConstants.CENTER);
                  OBJ_83.setName("OBJ_83");
                  P_SEL6.add(OBJ_83);
                  OBJ_83.setBounds(280, 15, 25, 18);

                  //---- EFINA ----
                  EFINA.setComponentPopupMenu(BTD);
                  EFINA.setName("EFINA");
                  P_SEL6.add(EFINA);
                  EFINA.setBounds(325, 14, 210, EFINA.getPreferredSize().height);

                  //======== P_SEL7 ========
                  {
                    P_SEL7.setBorder(new BevelBorder(BevelBorder.LOWERED));
                    P_SEL7.setOpaque(false);
                    P_SEL7.setName("P_SEL7");
                    P_SEL7.setLayout(null);

                    //---- OBJ_78 ----
                    OBJ_78.setText("De");
                    OBJ_78.setName("OBJ_78");
                    P_SEL7.add(OBJ_78);
                    OBJ_78.setBounds(15, 15, 30, 18);

                    //---- DEBIAK ----
                    DEBIAK.setComponentPopupMenu(BTD);
                    DEBIAK.setName("DEBIAK");
                    P_SEL7.add(DEBIAK);
                    DEBIAK.setBounds(55, 14, 160, DEBIAK.getPreferredSize().height);

                    //---- OBJ_82 ----
                    OBJ_82.setText("\u00e0");
                    OBJ_82.setHorizontalAlignment(SwingConstants.CENTER);
                    OBJ_82.setName("OBJ_82");
                    P_SEL7.add(OBJ_82);
                    OBJ_82.setBounds(240, 15, 30, 18);

                    //---- FINIAK ----
                    FINIAK.setComponentPopupMenu(BTD);
                    FINIAK.setName("FINIAK");
                    P_SEL7.add(FINIAK);
                    FINIAK.setBounds(300, 14, 160, FINIAK.getPreferredSize().height);
                  }
                  P_SEL6.add(P_SEL7);
                  P_SEL7.setBounds(0, 0, 560, 48);
                }
                P_SEL5.add(P_SEL6);
                P_SEL6.setBounds(0, 0, 560, 48);
              }
              P_SEL8.add(P_SEL5);
              P_SEL5.setBounds(0, 0, 560, 48);
            }
            panel4.add(P_SEL8);
            P_SEL8.setBounds(190, 215, 560, 48);

            //======== P_SEL9 ========
            {
              P_SEL9.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL9.setOpaque(false);
              P_SEL9.setName("P_SEL9");
              P_SEL9.setLayout(null);

              //---- OBJ_81 ----
              OBJ_81.setText("Code de d\u00e9but");
              OBJ_81.setName("OBJ_81");
              P_SEL9.add(OBJ_81);
              OBJ_81.setBounds(15, 15, 105, 18);

              //---- DEBIAE ----
              DEBIAE.setComponentPopupMenu(BTD);
              DEBIAE.setName("DEBIAE");
              P_SEL9.add(DEBIAE);
              DEBIAE.setBounds(130, 10, 60, DEBIAE.getPreferredSize().height);

              //---- OBJ_86 ----
              OBJ_86.setText("Code de fin");
              OBJ_86.setName("OBJ_86");
              P_SEL9.add(OBJ_86);
              OBJ_86.setBounds(275, 15, 85, 18);

              //---- FINIAE ----
              FINIAE.setComponentPopupMenu(BTD);
              FINIAE.setName("FINIAE");
              P_SEL9.add(FINIAE);
              FINIAE.setBounds(370, 10, 60, FINIAE.getPreferredSize().height);
            }
            panel4.add(P_SEL9);
            P_SEL9.setBounds(190, 215, 560, 48);

            //---- OBJ_107 ----
            OBJ_107.setTitle("");
            OBJ_107.setName("OBJ_107");
            panel4.add(OBJ_107);
            OBJ_107.setBounds(30, 515, 735, OBJ_107.getPreferredSize().height);

            //---- OBJ_94 ----
            OBJ_94.setTitle("S\u00e9lections compl\u00e9mentaires");
            OBJ_94.setName("OBJ_94");
            panel4.add(OBJ_94);
            OBJ_94.setBounds(30, 375, 296, OBJ_94.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- SEL2 ----
              SEL2.setText("Articles avec stock mini");
              SEL2.setComponentPopupMenu(null);
              SEL2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SEL2.setName("SEL2");
              panel1.add(SEL2);
              SEL2.setBounds(5, 28, 210, 20);

              //---- SEL1 ----
              SEL1.setText("Articles au stock \u00e0 z\u00e9ro");
              SEL1.setComponentPopupMenu(null);
              SEL1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SEL1.setName("SEL1");
              panel1.add(SEL1);
              SEL1.setBounds(5, 5, 210, 20);

              //---- SEL3 ----
              SEL3.setText("Articles sans stock mini");
              SEL3.setComponentPopupMenu(null);
              SEL3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SEL3.setName("SEL3");
              panel1.add(SEL3);
              SEL3.setBounds(5, 51, 210, 20);

              //---- SEL4 ----
              SEL4.setText("Articles stock n\u00e9gatif");
              SEL4.setComponentPopupMenu(null);
              SEL4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SEL4.setName("SEL4");
              panel1.add(SEL4);
              SEL4.setBounds(5, 74, 210, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel1);
            panel1.setBounds(40, 405, 275, 105);

            //---- OBJ_89 ----
            OBJ_89.setTitle("");
            OBJ_89.setName("OBJ_89");
            panel4.add(OBJ_89);
            OBJ_89.setBounds(30, 300, 735, OBJ_89.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OPT1 ----
              OPT1.setText("Tous les articles (en rupture ou non)");
              OPT1.setToolTipText("Etat de stock pour tous les articles");
              OPT1.setComponentPopupMenu(null);
              OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT1.setName("OPT1");
              OPT1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OPT4MouseClicked(e);
                }
              });
              panel2.add(OPT1);
              OPT1.setBounds(10, 5, 290, OPT1.getPreferredSize().height);

              //---- OPT3 ----
              OPT3.setText("Articles en rupture / disponible");
              OPT3.setToolTipText("Rupture sur stock th\u00e9orique + attendu - commandes clients");
              OPT3.setComponentPopupMenu(null);
              OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT3.setName("OPT3");
              OPT3.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OPT4MouseClicked(e);
                }
              });
              panel2.add(OPT3);
              OPT3.setBounds(10, 30, 290, OPT3.getPreferredSize().height);

              //---- OPT2 ----
              OPT2.setText("Articles en rupture / existant");
              OPT2.setToolTipText("Rupture sur stock th\u00e9orique + attendu");
              OPT2.setComponentPopupMenu(null);
              OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT2.setName("OPT2");
              OPT2.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OPT4MouseClicked(e);
                }
              });
              panel2.add(OPT2);
              OPT2.setBounds(325, 5, 275, OPT2.getPreferredSize().height);

              //---- OPT4 ----
              OPT4.setText("Position / consommation ant\u00e9rieure");
              OPT4.setToolTipText("Proposition de r\u00e9appro du disponible - consommation sur p\u00e9riode demand\u00e9e");
              OPT4.setComponentPopupMenu(null);
              OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT4.setName("OPT4");
              OPT4.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OPT4MouseClicked(e);
                }
              });
              panel2.add(OPT4);
              OPT4.setBounds(325, 30, 275, OPT4.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel2);
            panel2.setBounds(35, 310, 605, 55);

            //---- OBJ_108 ----
            OBJ_108.setText("Type de produit");
            OBJ_108.setName("OBJ_108");
            panel4.add(OBJ_108);
            OBJ_108.setBounds(45, 529, 143, 20);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_111 ----
              OBJ_111.setText("Du");
              OBJ_111.setName("OBJ_111");
              panel3.add(OBJ_111);
              OBJ_111.setBounds(15, 35, 30, 18);

              //---- PERDEB ----
              PERDEB.setComponentPopupMenu(null);
              PERDEB.setName("PERDEB");
              panel3.add(PERDEB);
              PERDEB.setBounds(45, 34, 105, PERDEB.getPreferredSize().height);

              //---- OBJ_113 ----
              OBJ_113.setText("au");
              OBJ_113.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_113.setName("OBJ_113");
              panel3.add(OBJ_113);
              OBJ_113.setBounds(160, 35, 30, 18);

              //---- PERFIN ----
              PERFIN.setComponentPopupMenu(null);
              PERFIN.setName("PERFIN");
              panel3.add(PERFIN);
              PERFIN.setBounds(200, 34, 105, PERFIN.getPreferredSize().height);

              //---- OBJ_39 ----
              OBJ_39.setTitle("P\u00e9riode de consommation");
              OBJ_39.setName("OBJ_39");
              panel3.add(OBJ_39);
              OBJ_39.setBounds(5, 5, 415, OBJ_39.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel3);
            panel3.setBounds(345, 370, 430, 84);

            //---- OBJ_96 ----
            OBJ_96.setText("Codes ABC");
            OBJ_96.setName("OBJ_96");
            panel4.add(OBJ_96);
            OBJ_96.setBounds(260, 529, 95, 20);

            //---- WABC ----
            WABC.setComponentPopupMenu(BTD);
            WABC.setName("WABC");
            panel4.add(WABC);
            WABC.setBounds(355, 525, 20, WABC.getPreferredSize().height);

            //---- WABC1 ----
            WABC1.setComponentPopupMenu(BTD);
            WABC1.setName("WABC1");
            panel4.add(WABC1);
            WABC1.setBounds(375, 525, 20, WABC1.getPreferredSize().height);

            //---- WABC2 ----
            WABC2.setComponentPopupMenu(BTD);
            WABC2.setName("WABC2");
            panel4.add(WABC2);
            WABC2.setBounds(395, 525, 20, WABC2.getPreferredSize().height);

            //---- WABC3 ----
            WABC3.setComponentPopupMenu(BTD);
            WABC3.setName("WABC3");
            panel4.add(WABC3);
            WABC3.setBounds(415, 525, 20, WABC3.getPreferredSize().height);

            //---- SEL5 ----
            SEL5.setComponentPopupMenu(BTD);
            SEL5.setName("SEL5");
            panel4.add(SEL5);
            SEL5.setBounds(200, 525, 20, SEL5.getPreferredSize().height);

            //---- EDTMIN ----
            EDTMIN.setText("Calcul rupture sans valeur stock mini");
            EDTMIN.setComponentPopupMenu(null);
            EDTMIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTMIN.setName("EDTMIN");
            panel4.add(EDTMIN);
            EDTMIN.setBounds(355, 485, 270, 20);

            //---- MVTTRS ----
            MVTTRS.setText("Transfert non pris en compte");
            MVTTRS.setComponentPopupMenu(null);
            MVTTRS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MVTTRS.setName("MVTTRS");
            panel4.add(MVTTRS);
            MVTTRS.setBounds(355, 460, 270, 20);

            //---- OBJ_97 ----
            OBJ_97.setText("Type de r\u00e9appro");
            OBJ_97.setName("OBJ_97");
            panel4.add(OBJ_97);
            OBJ_97.setBounds(515, 529, 135, 20);

            //---- WREA1 ----
            WREA1.setComponentPopupMenu(BTD);
            WREA1.setName("WREA1");
            panel4.add(WREA1);
            WREA1.setBounds(650, 525, 24, WREA1.getPreferredSize().height);

            //---- WREA2 ----
            WREA2.setComponentPopupMenu(BTD);
            WREA2.setName("WREA2");
            panel4.add(WREA2);
            WREA2.setBounds(677, 525, 24, WREA2.getPreferredSize().height);

            //---- WREA3 ----
            WREA3.setComponentPopupMenu(BTD);
            WREA3.setName("WREA3");
            panel4.add(WREA3);
            WREA3.setBounds(704, 525, 24, WREA3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(5, 5, 800, 565);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(OPT1);
    RB_GRP.add(OPT3);
    RB_GRP.add(OPT2);
    RB_GRP.add(OPT4);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel4;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_44;
  private XRiCheckBox WTOUM;
  private JPanel P_SEL0;
  private RiZoneSortie MA01;
  private RiZoneSortie MA02;
  private RiZoneSortie MA03;
  private RiZoneSortie MA04;
  private RiZoneSortie MA05;
  private RiZoneSortie MA06;
  private RiZoneSortie MA07;
  private RiZoneSortie MA08;
  private RiZoneSortie MA09;
  private RiZoneSortie MA10;
  private RiZoneSortie MA11;
  private RiZoneSortie MA12;
  private XRiCheckBox REPON1;
  private XRiCheckBox WTOU;
  private JXTitledSeparator OBJ_60;
  private JPanel P_SEL8;
  private JLabel OBJ_80;
  private XRiTextField FRSDEB;
  private JLabel OBJ_85;
  private XRiTextField FRSFIN;
  private JPanel P_SEL5;
  private JLabel OBJ_79;
  private XRiTextField EDEB;
  private JLabel OBJ_84;
  private XRiTextField EFIN;
  private JPanel P_SEL6;
  private XRiTextField EDEBA;
  private JLabel OBJ_77;
  private JLabel OBJ_83;
  private XRiTextField EFINA;
  private JPanel P_SEL7;
  private JLabel OBJ_78;
  private XRiTextField DEBIAK;
  private JLabel OBJ_82;
  private XRiTextField FINIAK;
  private JPanel P_SEL9;
  private JLabel OBJ_81;
  private XRiTextField DEBIAE;
  private JLabel OBJ_86;
  private XRiTextField FINIAE;
  private JXTitledSeparator OBJ_107;
  private JXTitledSeparator OBJ_94;
  private JPanel panel1;
  private XRiCheckBox SEL2;
  private XRiCheckBox SEL1;
  private XRiCheckBox SEL3;
  private XRiCheckBox SEL4;
  private JXTitledSeparator OBJ_89;
  private JPanel panel2;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT3;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT4;
  private JLabel OBJ_108;
  private JPanel panel3;
  private JLabel OBJ_111;
  private XRiCalendrier PERDEB;
  private JLabel OBJ_113;
  private XRiCalendrier PERFIN;
  private JXTitledSeparator OBJ_39;
  private JLabel OBJ_96;
  private XRiTextField WABC;
  private XRiTextField WABC1;
  private XRiTextField WABC2;
  private XRiTextField WABC3;
  private XRiTextField SEL5;
  private XRiCheckBox EDTMIN;
  private XRiCheckBox MVTTRS;
  private JLabel OBJ_97;
  private XRiTextField WREA1;
  private XRiTextField WREA2;
  private XRiTextField WREA3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
