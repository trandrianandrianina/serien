
package ri.serien.libecranrpg.sgvx.SGVXA7FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class SGVXA7FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXA7FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    bouton_etablissement = new SNBoutonRecherche();
    z_etablissement_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_dgnom_ = new RiZoneSortie();
    sep_etablissement = new JXTitledSeparator();
    OBJ_36 = new JXTitledSeparator();
    OBJ_37 = new JXTitledSeparator();
    OBJ_38 = new JXTitledSeparator();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_53 = new JLabel();
    ADD1 = new XRiTextField();
    ADD2 = new XRiTextField();
    ADD3 = new XRiTextField();
    ADD4 = new XRiTextField();
    ADF1 = new XRiTextField();
    ADF2 = new XRiTextField();
    ADF3 = new XRiTextField();
    ADF4 = new XRiTextField();
    WMAG = new XRiTextField();
    UTYP = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_55 = new JXTitledSeparator();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 200));
        menus_haut.setPreferredSize(new Dimension(160, 200));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt_export ----
          riSousMenu_bt_export.setText("Exportation tableur");
          riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
          riSousMenu_bt_export.setName("riSousMenu_bt_export");
          riSousMenu_bt_export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt_exportActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt_export);
        }
        menus_haut.add(riSousMenu6);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(650, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel1.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(90, 70), bouton_etablissement.getPreferredSize()));

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel1.add(z_etablissement_);
            z_etablissement_.setBounds(45, 75, 40, z_etablissement_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel1.add(z_wencx_);
            z_wencx_.setBounds(200, 85, 260, z_wencx_.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel1.add(z_dgnom_);
            z_dgnom_.setBounds(200, 55, 260, z_dgnom_.getPreferredSize().height);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel1.add(sep_etablissement);
            sep_etablissement.setBounds(30, 30, 575, sep_etablissement.getPreferredSize().height);

            //---- OBJ_36 ----
            OBJ_36.setTitle("Adresses de stockage \u00e0 s\u00e9lectionner");
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(30, 295, 575, OBJ_36.getPreferredSize().height);

            //---- OBJ_37 ----
            OBJ_37.setTitle("Option");
            OBJ_37.setName("OBJ_37");
            panel1.add(OBJ_37);
            OBJ_37.setBounds(30, 210, 575, OBJ_37.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setTitle("Magasin");
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(30, 125, 575, OBJ_38.getPreferredSize().height);

            //---- OBJ_54 ----
            OBJ_54.setText("Zones de stock \u00e0 \u00e9diter");
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(45, 240, 150, 28);

            //---- OBJ_56 ----
            OBJ_56.setText("Adresse de d\u00e9but");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(45, 330, 150, 28);

            //---- OBJ_57 ----
            OBJ_57.setText("Adresse de fin");
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(45, 365, 150, 28);

            //---- OBJ_53 ----
            OBJ_53.setText("Code magasin");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(45, 160, 150, 28);

            //---- ADD1 ----
            ADD1.setComponentPopupMenu(BTD);
            ADD1.setName("ADD1");
            panel1.add(ADD1);
            ADD1.setBounds(200, 330, 40, ADD1.getPreferredSize().height);

            //---- ADD2 ----
            ADD2.setComponentPopupMenu(BTD);
            ADD2.setName("ADD2");
            panel1.add(ADD2);
            ADD2.setBounds(246, 330, 40, ADD2.getPreferredSize().height);

            //---- ADD3 ----
            ADD3.setComponentPopupMenu(BTD);
            ADD3.setName("ADD3");
            panel1.add(ADD3);
            ADD3.setBounds(292, 330, 40, ADD3.getPreferredSize().height);

            //---- ADD4 ----
            ADD4.setComponentPopupMenu(BTD);
            ADD4.setName("ADD4");
            panel1.add(ADD4);
            ADD4.setBounds(338, 330, 40, ADD4.getPreferredSize().height);

            //---- ADF1 ----
            ADF1.setComponentPopupMenu(BTD);
            ADF1.setName("ADF1");
            panel1.add(ADF1);
            ADF1.setBounds(200, 365, 40, ADF1.getPreferredSize().height);

            //---- ADF2 ----
            ADF2.setComponentPopupMenu(BTD);
            ADF2.setName("ADF2");
            panel1.add(ADF2);
            ADF2.setBounds(246, 365, 40, ADF2.getPreferredSize().height);

            //---- ADF3 ----
            ADF3.setComponentPopupMenu(BTD);
            ADF3.setName("ADF3");
            panel1.add(ADF3);
            ADF3.setBounds(292, 365, 40, ADF3.getPreferredSize().height);

            //---- ADF4 ----
            ADF4.setComponentPopupMenu(BTD);
            ADF4.setName("ADF4");
            panel1.add(ADF4);
            ADF4.setBounds(338, 365, 40, ADF4.getPreferredSize().height);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            panel1.add(WMAG);
            WMAG.setBounds(200, 160, 30, WMAG.getPreferredSize().height);

            //---- UTYP ----
            UTYP.setComponentPopupMenu(BTD);
            UTYP.setName("UTYP");
            panel1.add(UTYP);
            UTYP.setBounds(200, 240, 24, UTYP.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 624, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_55 ----
    OBJ_55.setTitle("");
    OBJ_55.setName("OBJ_55");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_etablissement_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_dgnom_;
  private JXTitledSeparator sep_etablissement;
  private JXTitledSeparator OBJ_36;
  private JXTitledSeparator OBJ_37;
  private JXTitledSeparator OBJ_38;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JLabel OBJ_53;
  private XRiTextField ADD1;
  private XRiTextField ADD2;
  private XRiTextField ADD3;
  private XRiTextField ADD4;
  private XRiTextField ADF1;
  private XRiTextField ADF2;
  private XRiTextField ADF3;
  private XRiTextField ADF4;
  private XRiTextField WMAG;
  private XRiTextField UTYP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JXTitledSeparator OBJ_55;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
