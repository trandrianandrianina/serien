
package ri.serien.libecranrpg.sgvx.SGVX47FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX47FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX47FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TCMP.setValeurs("X");
    TCMAP.setValeurs("X");
    PRSAI.setValeursSelection("X", " ");
    PRCAL.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRAT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    MAGAS_ck.setSelected(lexique.HostFieldGetData("MAGAS").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!MAGAS_ck.isSelected());
    
    panel1.setVisible(lexique.isPresent("WZP"));
    if (panel1.isVisible()) {
      WZP_ck.setSelected(lexique.HostFieldGetData("WZP").equalsIgnoreCase("**"));
      P_SEL4.setVisible(!WZP_ck.isSelected());
    }
    
    FAMDEB_ck.setSelected(lexique.HostFieldGetData("FAMDEB").trim().equalsIgnoreCase("**"));
    P_SEL3.setVisible(!FAMDEB_ck.isSelected());
    
    FOUDEB_ck.setSelected(lexique.HostFieldGetData("FOUDEB").trim().equalsIgnoreCase("**"));
    P_SEL2.setVisible(!FOUDEB_ck.isSelected());
    
    ARTDEB_ck.setSelected(lexique.HostFieldGetData("ARTDEB").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!ARTDEB_ck.isSelected());
    
    NBRMA.setVisible(lexique.isPresent("NBRMA"));
    OBJ_60.setVisible(lexique.isPresent("NBRMA"));
    OBJ_52.setVisible(lexique.isPresent("NBRMA"));
    
    // TCMP.setSelected(lexique.HostFieldGetData("TCMP").equalsIgnoreCase("X"));
    // TCMAP.setSelected(lexique.HostFieldGetData("TCMAP").equalsIgnoreCase("X"));
    
    // PRSAI.setSelected(lexique.HostFieldGetData("PRSAI").equalsIgnoreCase("X"));
    // PRCAL.setSelected(lexique.HostFieldGetData("PRCAL").equalsIgnoreCase("X"));
    OBJ_79.setEnabled(PRCAL.isEnabled());
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (FOUDEB_ck.isSelected()) {
      lexique.HostFieldPutData("FOUDEB", 0, "**");
    }
    
    if (FAMDEB_ck.isSelected()) {
      lexique.HostFieldPutData("FAMDEB", 0, "**");
    }
    
    if (WZP_ck.isSelected()) {
      lexique.HostFieldPutData("WZP", 0, "**");
    }
    
    if (ARTDEB_ck.isSelected()) {
      lexique.HostFieldPutData("ARTDEB", 0, "**");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WZPActionPerformed(ActionEvent e) {
    P_SEL4.setVisible(!P_SEL4.isVisible());
    if (!WZP_ck.isSelected()) {
      WZP.setText("");
    }
  }
  
  private void FAMDEBActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
    if (!FAMDEB_ck.isSelected()) {
      FAMDEB.setText("");
    }
  }
  
  private void FOUDEBActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
    if (!FOUDEB_ck.isSelected()) {
      FOUDEB.setText("");
    }
  }
  
  private void ARTDEB_ckActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!ARTDEB_ck.isSelected()) {
      ARTDEB.setText("");
    }
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_30 = new JXTitledSeparator();
    OBJ_32 = new JXTitledSeparator();
    OBJ_26 = new JXTitledSeparator();
    panel1 = new JPanel();
    OBJ_28 = new JXTitledSeparator();
    P_SEL4 = new JPanel();
    OBJ_77 = new JLabel();
    WZP = new XRiTextField();
    OBJ_54 = new RiZoneSortie();
    WZP_ck = new JCheckBox();
    OBJ_31 = new JXTitledSeparator();
    OBJ_34 = new JXTitledSeparator();
    OBJ_48 = new JXTitledSeparator();
    OBJ_55 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    P_SEL2 = new JPanel();
    FOUDEB = new XRiTextField();
    FOUFIN = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    P_SEL3 = new JPanel();
    OBJ_40 = new JLabel();
    OBJ_53 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    PRCAL = new XRiCheckBox();
    TCMAP = new XRiRadioButton();
    TCMP = new XRiRadioButton();
    PRSAI = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_76 = new JLabel();
    MAGAS = new XRiTextField();
    OBJ_79 = new JLabel();
    MAGAS_ck = new JCheckBox();
    ARTDEB_ck = new JCheckBox();
    FAMDEB_ck = new JCheckBox();
    FOUDEB_ck = new JCheckBox();
    OBJ_47 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_59 = new JLabel();
    DATDEP = new XRiTextField();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    NBRMG = new XRiTextField();
    NBRMA = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 840, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setEnabled(false);
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //---- OBJ_30 ----
          OBJ_30.setTitle("Provenance coefficient");
          OBJ_30.setName("OBJ_30");
          p_contenu.add(OBJ_30);
          OBJ_30.setBounds(35, 310, 285, OBJ_30.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setTitle("Plage articles");
          OBJ_32.setName("OBJ_32");
          p_contenu.add(OBJ_32);
          OBJ_32.setBounds(340, 200, 535, OBJ_32.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setTitle("Codes magasins");
          OBJ_26.setName("OBJ_26");
          p_contenu.add(OBJ_26);
          OBJ_26.setBounds(35, 120, 285, OBJ_26.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_28 ----
            OBJ_28.setTitle("ZP articles");
            OBJ_28.setName("OBJ_28");
            panel1.add(OBJ_28);
            OBJ_28.setBounds(0, 0, 535, OBJ_28.getPreferredSize().height);

            //======== P_SEL4 ========
            {
              P_SEL4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL4.setOpaque(false);
              P_SEL4.setName("P_SEL4");
              P_SEL4.setLayout(null);

              //---- OBJ_77 ----
              OBJ_77.setText("Code");
              OBJ_77.setName("OBJ_77");
              P_SEL4.add(OBJ_77);
              OBJ_77.setBounds(15, 14, 36, 18);

              //---- WZP ----
              WZP.setComponentPopupMenu(BTD);
              WZP.setName("WZP");
              P_SEL4.add(WZP);
              WZP.setBounds(60, 10, 34, WZP.getPreferredSize().height);

              //---- OBJ_54 ----
              OBJ_54.setText("@LIBRAT@");
              OBJ_54.setName("OBJ_54");
              P_SEL4.add(OBJ_54);
              OBJ_54.setBounds(105, 10, 260, OBJ_54.getPreferredSize().height);
            }
            panel1.add(P_SEL4);
            P_SEL4.setBounds(145, 20, 375, 45);

            //---- WZP_ck ----
            WZP_ck.setText("S\u00e9lection compl\u00e8te");
            WZP_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WZP_ck.setName("WZP_ck");
            WZP_ck.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WZPActionPerformed(e);
              }
            });
            panel1.add(WZP_ck);
            WZP_ck.setBounds(5, 33, 130, WZP_ck.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(340, 120, panel1.getPreferredSize().width, 70);

          //---- OBJ_31 ----
          OBJ_31.setTitle("P\u00e9riode \u00e0 g\u00e9n\u00e9rer");
          OBJ_31.setName("OBJ_31");
          p_contenu.add(OBJ_31);
          OBJ_31.setBounds(35, 430, 285, OBJ_31.getPreferredSize().height);

          //---- OBJ_34 ----
          OBJ_34.setTitle("Familles");
          OBJ_34.setName("OBJ_34");
          p_contenu.add(OBJ_34);
          OBJ_34.setBounds(340, 310, 535, OBJ_34.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setTitle("Type de calcul");
          OBJ_48.setName("OBJ_48");
          p_contenu.add(OBJ_48);
          OBJ_48.setBounds(35, 200, 285, OBJ_48.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setTitle("Fournisseurs");
          OBJ_55.setName("OBJ_55");
          p_contenu.add(OBJ_55);
          OBJ_55.setBounds(340, 430, 535, OBJ_55.getPreferredSize().height);

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- ARTDEB ----
            ARTDEB.setComponentPopupMenu(BTD);
            ARTDEB.setName("ARTDEB");
            P_SEL0.add(ARTDEB);
            ARTDEB.setBounds(110, 10, 214, ARTDEB.getPreferredSize().height);

            //---- ARTFIN ----
            ARTFIN.setComponentPopupMenu(BTD);
            ARTFIN.setName("ARTFIN");
            P_SEL0.add(ARTFIN);
            ARTFIN.setBounds(110, 40, 214, ARTFIN.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Code de d\u00e9but");
            OBJ_45.setName("OBJ_45");
            P_SEL0.add(OBJ_45);
            OBJ_45.setBounds(15, 15, 85, 18);

            //---- OBJ_46 ----
            OBJ_46.setText("Code de fin");
            OBJ_46.setName("OBJ_46");
            P_SEL0.add(OBJ_46);
            OBJ_46.setBounds(15, 45, 75, 18);
          }
          p_contenu.add(P_SEL0);
          P_SEL0.setBounds(485, 220, 335, 75);

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- FOUDEB ----
            FOUDEB.setComponentPopupMenu(BTD);
            FOUDEB.setName("FOUDEB");
            P_SEL2.add(FOUDEB);
            FOUDEB.setBounds(110, 10, 84, FOUDEB.getPreferredSize().height);

            //---- FOUFIN ----
            FOUFIN.setComponentPopupMenu(BTD);
            FOUFIN.setName("FOUFIN");
            P_SEL2.add(FOUFIN);
            FOUFIN.setBounds(110, 40, 84, FOUFIN.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("D\u00e9but");
            OBJ_57.setName("OBJ_57");
            P_SEL2.add(OBJ_57);
            OBJ_57.setBounds(15, 15, 39, 18);

            //---- OBJ_58 ----
            OBJ_58.setText("Fin");
            OBJ_58.setName("OBJ_58");
            P_SEL2.add(OBJ_58);
            OBJ_58.setBounds(15, 45, 39, 18);
          }
          p_contenu.add(P_SEL2);
          P_SEL2.setBounds(485, 450, 335, 75);

          //======== P_SEL3 ========
          {
            P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL3.setOpaque(false);
            P_SEL3.setName("P_SEL3");
            P_SEL3.setLayout(null);

            //---- OBJ_40 ----
            OBJ_40.setText("D\u00e9but");
            OBJ_40.setName("OBJ_40");
            P_SEL3.add(OBJ_40);
            OBJ_40.setBounds(15, 15, 39, 18);

            //---- OBJ_53 ----
            OBJ_53.setText("Fin");
            OBJ_53.setName("OBJ_53");
            P_SEL3.add(OBJ_53);
            OBJ_53.setBounds(15, 45, 39, 18);

            //---- FAMDEB ----
            FAMDEB.setComponentPopupMenu(BTD);
            FAMDEB.setName("FAMDEB");
            P_SEL3.add(FAMDEB);
            FAMDEB.setBounds(110, 10, 44, FAMDEB.getPreferredSize().height);

            //---- FAMFIN ----
            FAMFIN.setComponentPopupMenu(BTD);
            FAMFIN.setName("FAMFIN");
            P_SEL3.add(FAMFIN);
            FAMFIN.setBounds(110, 40, 44, FAMFIN.getPreferredSize().height);
          }
          p_contenu.add(P_SEL3);
          P_SEL3.setBounds(485, 330, 335, 75);

          //---- PRCAL ----
          PRCAL.setText("Calcul sur progression des derniers mois");
          PRCAL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRCAL.setName("PRCAL");
          p_contenu.add(PRCAL);
          PRCAL.setBounds(50, 375, 268, PRCAL.getPreferredSize().height);

          //---- TCMAP ----
          TCMAP.setText("Coefficient / mois de l'ann\u00e9e pr\u00e9c\u00e9dente");
          TCMAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TCMAP.setName("TCMAP");
          p_contenu.add(TCMAP);
          TCMAP.setBounds(50, 230, 264, TCMAP.getPreferredSize().height);

          //---- TCMP ----
          TCMP.setText("Coefficient sur mois pr\u00e9c\u00e9dents");
          TCMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TCMP.setName("TCMP");
          p_contenu.add(TCMP);
          TCMP.setBounds(50, 260, 264, TCMP.getPreferredSize().height);

          //---- PRSAI ----
          PRSAI.setText("Saisie du type de calcul de pr\u00e9visions");
          PRSAI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PRSAI.setName("PRSAI");
          p_contenu.add(PRSAI);
          PRSAI.setBounds(50, 345, 250, PRSAI.getPreferredSize().height);

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_76 ----
            OBJ_76.setText("Code");
            OBJ_76.setName("OBJ_76");
            P_SEL1.add(OBJ_76);
            OBJ_76.setBounds(13, 15, 36, 18);

            //---- MAGAS ----
            MAGAS.setComponentPopupMenu(BTD);
            MAGAS.setName("MAGAS");
            P_SEL1.add(MAGAS);
            MAGAS.setBounds(60, 10, 34, MAGAS.getPreferredSize().height);
          }
          p_contenu.add(P_SEL1);
          P_SEL1.setBounds(205, 140, 99, 45);

          //---- OBJ_79 ----
          OBJ_79.setText("(par rapport \u00e0 l'ann\u00e9e pr\u00e9c\u00e9dente)");
          OBJ_79.setName("OBJ_79");
          p_contenu.add(OBJ_79);
          OBJ_79.setBounds(70, 395, 207, 18);

          //---- MAGAS_ck ----
          MAGAS_ck.setText("S\u00e9lection compl\u00e8te");
          MAGAS_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MAGAS_ck.setName("MAGAS_ck");
          p_contenu.add(MAGAS_ck);
          MAGAS_ck.setBounds(50, 155, 141, MAGAS_ck.getPreferredSize().height);

          //---- ARTDEB_ck ----
          ARTDEB_ck.setText("S\u00e9lection compl\u00e8te");
          ARTDEB_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARTDEB_ck.setName("ARTDEB_ck");
          ARTDEB_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ARTDEB_ckActionPerformed(e);
            }
          });
          p_contenu.add(ARTDEB_ck);
          ARTDEB_ck.setBounds(345, 250, 130, ARTDEB_ck.getPreferredSize().height);

          //---- FAMDEB_ck ----
          FAMDEB_ck.setText("S\u00e9lection compl\u00e8te");
          FAMDEB_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FAMDEB_ck.setName("FAMDEB_ck");
          FAMDEB_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              FAMDEBActionPerformed(e);
            }
          });
          p_contenu.add(FAMDEB_ck);
          FAMDEB_ck.setBounds(345, 360, 130, FAMDEB_ck.getPreferredSize().height);

          //---- FOUDEB_ck ----
          FOUDEB_ck.setText("S\u00e9lection compl\u00e8te");
          FOUDEB_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FOUDEB_ck.setName("FOUDEB_ck");
          FOUDEB_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              FOUDEBActionPerformed(e);
            }
          });
          p_contenu.add(FOUDEB_ck);
          FOUDEB_ck.setBounds(345, 480, 130, FOUDEB_ck.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("Mois de d\u00e9part");
          OBJ_47.setName("OBJ_47");
          p_contenu.add(OBJ_47);
          OBJ_47.setBounds(50, 464, 94, 20);

          //---- OBJ_52 ----
          OBJ_52.setText("Analyser");
          OBJ_52.setName("OBJ_52");
          p_contenu.add(OBJ_52);
          OBJ_52.setBounds(50, 525, 57, 18);

          //---- OBJ_59 ----
          OBJ_59.setText("G\u00e9n\u00e9rer");
          OBJ_59.setName("OBJ_59");
          p_contenu.add(OBJ_59);
          OBJ_59.setBounds(50, 495, 57, 18);

          //---- DATDEP ----
          DATDEP.setComponentPopupMenu(BTD);
          DATDEP.setName("DATDEP");
          p_contenu.add(DATDEP);
          DATDEP.setBounds(205, 460, 45, DATDEP.getPreferredSize().height);

          //---- OBJ_60 ----
          OBJ_60.setText("mois");
          OBJ_60.setName("OBJ_60");
          p_contenu.add(OBJ_60);
          OBJ_60.setBounds(260, 525, 33, 18);

          //---- OBJ_61 ----
          OBJ_61.setText("mois");
          OBJ_61.setName("OBJ_61");
          p_contenu.add(OBJ_61);
          OBJ_61.setBounds(260, 495, 33, 18);

          //---- NBRMG ----
          NBRMG.setComponentPopupMenu(BTD);
          NBRMG.setName("NBRMG");
          p_contenu.add(NBRMG);
          NBRMG.setBounds(205, 490, 28, NBRMG.getPreferredSize().height);

          //---- NBRMA ----
          NBRMA.setComponentPopupMenu(BTD);
          NBRMA.setName("NBRMA");
          p_contenu.add(NBRMA);
          NBRMA.setBounds(205, 520, 28, NBRMA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TCMAP);
    RB_GRP.add(TCMP);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_30;
  private JXTitledSeparator OBJ_32;
  private JXTitledSeparator OBJ_26;
  private JPanel panel1;
  private JXTitledSeparator OBJ_28;
  private JPanel P_SEL4;
  private JLabel OBJ_77;
  private XRiTextField WZP;
  private RiZoneSortie OBJ_54;
  private JCheckBox WZP_ck;
  private JXTitledSeparator OBJ_31;
  private JXTitledSeparator OBJ_34;
  private JXTitledSeparator OBJ_48;
  private JXTitledSeparator OBJ_55;
  private JPanel P_SEL0;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private JPanel P_SEL2;
  private XRiTextField FOUDEB;
  private XRiTextField FOUFIN;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JPanel P_SEL3;
  private JLabel OBJ_40;
  private JLabel OBJ_53;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private XRiCheckBox PRCAL;
  private XRiRadioButton TCMAP;
  private XRiRadioButton TCMP;
  private XRiCheckBox PRSAI;
  private JPanel P_SEL1;
  private JLabel OBJ_76;
  private XRiTextField MAGAS;
  private JLabel OBJ_79;
  private JCheckBox MAGAS_ck;
  private JCheckBox ARTDEB_ck;
  private JCheckBox FAMDEB_ck;
  private JCheckBox FOUDEB_ck;
  private JLabel OBJ_47;
  private JLabel OBJ_52;
  private JLabel OBJ_59;
  private XRiTextField DATDEP;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private XRiTextField NBRMG;
  private XRiTextField NBRMA;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
