
package ri.serien.libecranrpg.sgvx.SGVX25FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM3239] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Inventaires -> Edition de bordereaux chiffrés
 * Indicateur : 11010001 (91,92,94)
 * Titre : Edition des bordereaux d'inventaire chiffrés
 * 
 * [GVM323A] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Inventaires -> Edition de bordereaux non chiffrés
 * Indicateur: 11000001 (91,92)
 * Titre : Edition des bordereaux d'inventaire non chiffrés
 * 
 * [GVM327] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Edition de bordereaux chiffrés
 * Indicateur: 10010001 (91,94)
 * Titre : Edition des bordereaux de stock chiffrés
 * 
 * [GVM328] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Edition de bordereaux non chiffrés
 * Indicateur: 10010001 (91)
 * Titre : Edition des bordereaux de stock non chiffrés
 * 
 * [GAM3238] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires -> Edition de bordereaux chiffrés
 * Indicateur : 11010001 (91,92,94)
 * Titre : Edition des bordereaux d'inventaire chiffrés
 * 
 * [GAM3239] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires -> Edition de bordereaux non chiffrés
 * Indicateur: 11000001 (91,92)
 * Titre : Edition des bordereaux d'inventaire non chiffrés
 * 
 * [GAM327] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Edition de bordereaux chiffrés
 * Indicateur: 10010001 (91,94)
 * Titre : Edition des bordereaux de stock chiffrés
 * 
 * [GAM328] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Edition de bordereaux non chiffrés
 * Indicateur: 10010001 (91)
 * Titre : Edition des bordereaux de stock non chiffrés
 */
public class SGVX25FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] UTRI_Value = { "", "S", "F", };
  private String[] UTYP_Value = { "", "E", "R", "I", "D", "T", "S", };
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  public SGVX25FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    FORBOR.setValeursSelection("B", " ");
    UTRI.setValeurs(UTRI_Value, null);
    UTYP.setValeurs(UTYP_Value, null);
    REPON1.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbDateEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gére les erreurs automatiquement
    gererLesErreurs("19");
    
    // Indicateur
    Boolean is91 = lexique.isTrue("91");
    Boolean is92 = lexique.isTrue("92");
    Boolean is94 = lexique.isTrue("94");
    
    // Titre
    if (is91) {
      bpPresentation.setText("Edition des bordereaux de stock non chiffrés");
    }
    if (is91 && is92) {
      bpPresentation.setText("Edition des bordereaux d'inventaire non chiffrés");
    }
    if (is91 && is94) {
      bpPresentation.setText("Edition des bordereaux de stock chiffrés");
    }
    if (is91 && is92 && is94) {
      bpPresentation.setText("Edition des bordereaux d'inventaire chiffrés");
    }
    
    // Visibilité des composants
    lbDateEnCours.setVisible(lexique.isPresent("WENCX"));
    lbBordereau.setVisible(lexique.isPresent("UTYP"));
    UTYP.setVisible(lexique.isPresent("UTYP"));
    FORBOR.setVisible(lexique.isPresent("FORBOR"));
    lbTri.setVisible(lexique.isPresent("UTRI"));
    UTRI.setVisible(lexique.isPresent("UTRI"));
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (NUMDEB.getText().isEmpty() && NUMFIN.getText().isEmpty()) {
      lexique.HostFieldPutData("WTOU1", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU1", 0, "");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbBordereauAEditer = new SNLabelChamp();
    pnlNumeroBordereaux = new SNPanel();
    NUMDEB = new XRiTextField();
    lbNumeroFin = new SNLabelChamp();
    NUMFIN = new XRiTextField();
    lbBordereau = new SNLabelChamp();
    UTYP = new XRiComboBox();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    lbDateEnCours = new SNTexte();
    pnlOptions = new SNPanelTitre();
    lbTri = new SNLabelChamp();
    UTRI = new XRiComboBox();
    lbNombreExemplaire = new SNLabelChamp();
    NBREX = new XRiTextField();
    REPON1 = new XRiCheckBox();
    FORBOR = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setMinimumSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbBordereauAEditer ----
          lbBordereauAEditer.setText("Bordereaux \u00e0 \u00e9diter de");
          lbBordereauAEditer.setName("lbBordereauAEditer");
          pnlCritereSelection.add(lbBordereauAEditer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlNumeroBordereaux ========
          {
            pnlNumeroBordereaux.setOpaque(false);
            pnlNumeroBordereaux.setName("pnlNumeroBordereaux");
            pnlNumeroBordereaux.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlNumeroBordereaux.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlNumeroBordereaux.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlNumeroBordereaux.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlNumeroBordereaux.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- NUMDEB ----
            NUMDEB.setMinimumSize(new Dimension(70, 30));
            NUMDEB.setPreferredSize(new Dimension(70, 30));
            NUMDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            NUMDEB.setName("NUMDEB");
            pnlNumeroBordereaux.add(NUMDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbNumeroFin ----
            lbNumeroFin.setText("\u00e0");
            lbNumeroFin.setMinimumSize(new Dimension(8, 30));
            lbNumeroFin.setPreferredSize(new Dimension(8, 30));
            lbNumeroFin.setMaximumSize(new Dimension(8, 30));
            lbNumeroFin.setName("lbNumeroFin");
            pnlNumeroBordereaux.add(lbNumeroFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- NUMFIN ----
            NUMFIN.setPreferredSize(new Dimension(70, 30));
            NUMFIN.setMinimumSize(new Dimension(70, 30));
            NUMFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            NUMFIN.setName("NUMFIN");
            pnlNumeroBordereaux.add(NUMFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCritereSelection.add(pnlNumeroBordereaux, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbBordereau ----
          lbBordereau.setText("Type de bordereaux");
          lbBordereau.setPreferredSize(new Dimension(150, 19));
          lbBordereau.setName("lbBordereau");
          pnlCritereSelection.add(lbBordereau, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- UTYP ----
          UTYP.setModel(new DefaultComboBoxModel(new String[] {
            "Tous les types",
            "Entr\u00e9e en stocks",
            "D\u00e9pr\u00e9ciation de stocks",
            "Inventaires",
            "Divers mouvements",
            "Transferts (E --> R)",
            "Sorties de stock"
          }));
          UTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UTYP.setFont(new Font("sansserif", Font.PLAIN, 14));
          UTYP.setMinimumSize(new Dimension(200, 30));
          UTYP.setPreferredSize(new Dimension(200, 30));
          UTYP.setMaximumSize(new Dimension(200, 30));
          UTYP.setName("UTYP");
          pnlCritereSelection.add(UTYP, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlEtablissementSelectionne ========
        {
          pnlEtablissementSelectionne.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlEtablissementSelectionne.setTitre("Etablissement");
          pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
          pnlEtablissementSelectionne.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissementSelectionne.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbDateEnCours ----
          lbDateEnCours.setText("@WENCX@");
          lbDateEnCours.setEditable(false);
          lbDateEnCours.setPreferredSize(new Dimension(260, 30));
          lbDateEnCours.setMinimumSize(new Dimension(260, 30));
          lbDateEnCours.setEnabled(false);
          lbDateEnCours.setName("lbDateEnCours");
          pnlEtablissementSelectionne.add(lbDateEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptions ========
        {
          pnlOptions.setTitre("Options d'\u00e9dition");
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbTri ----
          lbTri.setText("Tri \u00e9dition");
          lbTri.setPreferredSize(new Dimension(150, 19));
          lbTri.setName("lbTri");
          pnlOptions.add(lbTri, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- UTRI ----
          UTRI.setModel(new DefaultComboBoxModel(new String[] {
            "Num\u00e9ro de ligne",
            "Adresse de stockage",
            "Groupe / famille"
          }));
          UTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UTRI.setFont(new Font("sansserif", Font.PLAIN, 14));
          UTRI.setPreferredSize(new Dimension(175, 30));
          UTRI.setMinimumSize(new Dimension(175, 30));
          UTRI.setMaximumSize(new Dimension(175, 30));
          UTRI.setName("UTRI");
          pnlOptions.add(UTRI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbNombreExemplaire ----
          lbNombreExemplaire.setText("Nombre d'exemplaires");
          lbNombreExemplaire.setPreferredSize(new Dimension(150, 19));
          lbNombreExemplaire.setName("lbNombreExemplaire");
          pnlOptions.add(lbNombreExemplaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NBREX ----
          NBREX.setPreferredSize(new Dimension(24, 30));
          NBREX.setMinimumSize(new Dimension(24, 30));
          NBREX.setFont(new Font("sansserif", Font.PLAIN, 14));
          NBREX.setName("NBREX");
          pnlOptions.add(NBREX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- REPON1 ----
          REPON1.setText("R\u00e9\u00e9dition des bordereaux");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
          REPON1.setMinimumSize(new Dimension(190, 30));
          REPON1.setPreferredSize(new Dimension(190, 30));
          REPON1.setName("REPON1");
          pnlOptions.add(REPON1, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- FORBOR ----
          FORBOR.setText("Formulaire de type bon");
          FORBOR.setFont(new Font("sansserif", Font.PLAIN, 14));
          FORBOR.setPreferredSize(new Dimension(45, 30));
          FORBOR.setMinimumSize(new Dimension(45, 30));
          FORBOR.setName("FORBOR");
          pnlOptions.add(FORBOR, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbBordereauAEditer;
  private SNPanel pnlNumeroBordereaux;
  private XRiTextField NUMDEB;
  private SNLabelChamp lbNumeroFin;
  private XRiTextField NUMFIN;
  private SNLabelChamp lbBordereau;
  private XRiComboBox UTYP;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte lbDateEnCours;
  private SNPanelTitre pnlOptions;
  private SNLabelChamp lbTri;
  private XRiComboBox UTRI;
  private SNLabelChamp lbNombreExemplaire;
  private XRiTextField NBREX;
  private XRiCheckBox REPON1;
  private XRiCheckBox FORBOR;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
