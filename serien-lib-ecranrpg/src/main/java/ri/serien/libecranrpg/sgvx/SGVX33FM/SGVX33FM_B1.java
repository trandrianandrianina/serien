
package ri.serien.libecranrpg.sgvx.SGVX33FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX33FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX33FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTART.setValeursSelection("**", "  ");
    WTFAM.setValeursSelection("**", "  ");
    WTCLI.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNMA@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBD@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBF@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBD@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBF@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNMA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    WMAG_chk.setSelected(lexique.HostFieldGetData("WMAG").trim().equalsIgnoreCase("**"));
    panel1.setVisible(!WMAG_chk.isSelected());
    
    // WTCLI.setSelected(lexique.HostFieldGetData("WTCLI").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTCLI.isSelected());
    
    // WTFAM.setSelected(lexique.HostFieldGetData("WTFAM").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!WTFAM.isSelected());
    
    // WTART.setSelected(lexique.HostFieldGetData("WTART").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!WTART.isSelected());
    /*
        OBJ_53.setVisible(  lexique.isPresent("WNOM"));
        OBJ_35.setVisible( lexique.isPresent("WNMA"));
        OBJ_43.setVisible(  lexique.isPresent("A1LIBF"));
        OBJ_42.setVisible(  lexique.isPresent("A1LIBD"));
        OBJ_37.setVisible(  lexique.isPresent("FALIBF"));
        OBJ_36.setVisible(  lexique.isPresent("FALIBD"));
    */
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (WMAG_chk.isSelected()) {
      lexique.HostFieldPutData("WMAG", 0, "**");
    }
    
    // if (WTCLI.isSelected())
    // lexique.HostFieldPutData("WTCLI", 0, "**");
    // else
    // lexique.HostFieldPutData("WTCLI", 0, " ");
    // if (WTART.isSelected())
    // lexique.HostFieldPutData("WTART", 0, "**");
    // else
    // lexique.HostFieldPutData("WTART", 0, " ");
    // if (WTFAM.isSelected())
    // lexique.HostFieldPutData("WTFAM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTFAM", 0, " ");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTCLIActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTFAMActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTARTActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WMAG_chkActionPerformed(ActionEvent e) {
    panel1.setVisible(!WMAG_chk.isSelected());
    if (!WMAG_chk.isSelected()) {
      WMAG.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_24 = new JXTitledSeparator();
    OBJ_38 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    panel1 = new JPanel();
    WMAG = new XRiTextField();
    label1 = new JLabel();
    label2 = new RiZoneSortie();
    OBJ_33 = new JXTitledSeparator();
    WMAG_chk = new JCheckBox();
    WTCLI = new XRiCheckBox();
    P_SEL0 = new JPanel();
    OBJ_53 = new RiZoneSortie();
    OBJ_52 = new JLabel();
    WCLI = new XRiTextField();
    WTFAM = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_36 = new RiZoneSortie();
    OBJ_37 = new RiZoneSortie();
    OBJ_32 = new JLabel();
    OBJ_26 = new JLabel();
    WFDEB = new XRiTextField();
    WFFIN = new XRiTextField();
    P_SEL2 = new JPanel();
    OBJ_42 = new RiZoneSortie();
    OBJ_43 = new RiZoneSortie();
    WARTD = new XRiTextField();
    WARTF = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_40 = new JLabel();
    WTART = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_29 = new JXTitledSeparator();
    OBJ_35 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout)p_centrage.getLayout()).rowHeights = new int[] {496, 0};

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(855, 430));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_24 ----
          OBJ_24.setTitle("Plage de codes groupe / famille");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_38 ----
          OBJ_38.setTitle("Plage de codes articles");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Code client");
          OBJ_22.setName("OBJ_22");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel1.setName("panel1");

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");

            //---- label1 ----
            label1.setText("Magasin");
            label1.setName("label1");

            //---- label2 ----
            label2.setText("@WNMA@");
            label2.setName("label2");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addComponent(label1)
                  .addGap(28, 28, 28)
                  .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 314, GroupLayout.PREFERRED_SIZE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addComponent(label1))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //---- OBJ_33 ----
          OBJ_33.setTitle("Code magasin");
          OBJ_33.setName("OBJ_33");

          //---- WMAG_chk ----
          WMAG_chk.setText("S\u00e9lection compl\u00e8te");
          WMAG_chk.setVisible(false);
          WMAG_chk.setName("WMAG_chk");
          WMAG_chk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WMAG_chkActionPerformed(e);
            }
          });

          //---- WTCLI ----
          WTCLI.setText("S\u00e9lection compl\u00e8te");
          WTCLI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTCLI.setName("WTCLI");
          WTCLI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTCLIActionPerformed(e);
            }
          });

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");

            //---- OBJ_53 ----
            OBJ_53.setText("@WNOM@");
            OBJ_53.setName("OBJ_53");

            //---- OBJ_52 ----
            OBJ_52.setText("Client");
            OBJ_52.setName("OBJ_52");

            //---- WCLI ----
            WCLI.setComponentPopupMenu(BTD);
            WCLI.setName("WCLI");

            GroupLayout P_SEL0Layout = new GroupLayout(P_SEL0);
            P_SEL0.setLayout(P_SEL0Layout);
            P_SEL0Layout.setHorizontalGroup(
              P_SEL0Layout.createParallelGroup()
                .addGroup(P_SEL0Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                  .addGap(23, 23, 23)
                  .addComponent(WCLI, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                  .addGap(16, 16, 16)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 314, GroupLayout.PREFERRED_SIZE))
            );
            P_SEL0Layout.setVerticalGroup(
              P_SEL0Layout.createParallelGroup()
                .addGroup(P_SEL0Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_52))
                .addGroup(P_SEL0Layout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addComponent(WCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_SEL0Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //---- WTFAM ----
          WTFAM.setText("S\u00e9lection compl\u00e8te");
          WTFAM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTFAM.setName("WTFAM");
          WTFAM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTFAMActionPerformed(e);
            }
          });

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");

            //---- OBJ_36 ----
            OBJ_36.setText("@FALIBD@");
            OBJ_36.setName("OBJ_36");

            //---- OBJ_37 ----
            OBJ_37.setText("@FALIBF@");
            OBJ_37.setName("OBJ_37");

            //---- OBJ_32 ----
            OBJ_32.setText("Code fin");
            OBJ_32.setName("OBJ_32");

            //---- OBJ_26 ----
            OBJ_26.setText("Code d\u00e9but");
            OBJ_26.setName("OBJ_26");

            //---- WFDEB ----
            WFDEB.setComponentPopupMenu(BTD);
            WFDEB.setName("WFDEB");

            //---- WFFIN ----
            WFFIN.setComponentPopupMenu(BTD);
            WFFIN.setName("WFFIN");

            GroupLayout P_SEL1Layout = new GroupLayout(P_SEL1);
            P_SEL1.setLayout(P_SEL1Layout);
            P_SEL1Layout.setHorizontalGroup(
              P_SEL1Layout.createParallelGroup()
                .addGroup(P_SEL1Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(P_SEL1Layout.createParallelGroup()
                    .addGroup(P_SEL1Layout.createSequentialGroup()
                      .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(WFDEB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 314, GroupLayout.PREFERRED_SIZE))
                    .addGroup(P_SEL1Layout.createSequentialGroup()
                      .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(WFFIN, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 314, GroupLayout.PREFERRED_SIZE))))
            );
            P_SEL1Layout.setVerticalGroup(
              P_SEL1Layout.createParallelGroup()
                .addGroup(P_SEL1Layout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addGroup(P_SEL1Layout.createParallelGroup()
                    .addGroup(P_SEL1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(WFDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(P_SEL1Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2)
                  .addGroup(P_SEL1Layout.createParallelGroup()
                    .addGroup(P_SEL1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(WFFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(P_SEL1Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");

            //---- OBJ_42 ----
            OBJ_42.setText("@A1LIBD@");
            OBJ_42.setName("OBJ_42");

            //---- OBJ_43 ----
            OBJ_43.setText("@A1LIBF@");
            OBJ_43.setName("OBJ_43");

            //---- WARTD ----
            WARTD.setComponentPopupMenu(BTD);
            WARTD.setName("WARTD");

            //---- WARTF ----
            WARTF.setComponentPopupMenu(BTD);
            WARTF.setName("WARTF");

            //---- OBJ_41 ----
            OBJ_41.setText("Code fin");
            OBJ_41.setName("OBJ_41");

            //---- OBJ_40 ----
            OBJ_40.setText("Code d\u00e9but");
            OBJ_40.setName("OBJ_40");

            GroupLayout P_SEL2Layout = new GroupLayout(P_SEL2);
            P_SEL2.setLayout(P_SEL2Layout);
            P_SEL2Layout.setHorizontalGroup(
              P_SEL2Layout.createParallelGroup()
                .addGroup(P_SEL2Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(P_SEL2Layout.createParallelGroup()
                    .addGroup(P_SEL2Layout.createSequentialGroup()
                      .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(WARTD, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE))
                    .addGroup(P_SEL2Layout.createSequentialGroup()
                      .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(WARTF, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE))))
            );
            P_SEL2Layout.setVerticalGroup(
              P_SEL2Layout.createParallelGroup()
                .addGroup(P_SEL2Layout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addGroup(P_SEL2Layout.createParallelGroup()
                    .addGroup(P_SEL2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(WARTD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(P_SEL2Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2)
                  .addGroup(P_SEL2Layout.createParallelGroup()
                    .addGroup(P_SEL2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(WARTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(P_SEL2Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          //---- WTART ----
          WTART.setText("S\u00e9lection compl\u00e8te");
          WTART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTART.setName("WTART");
          WTART.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTARTActionPerformed(e);
            }
          });

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(82, 82, 82)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(WMAG_chk)
                    .addGap(155, 155, 155)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(WTCLI, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(WTFAM, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(WTART, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addComponent(WMAG_chk))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(WTCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(WTFAM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WTART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_29 ----
    OBJ_29.setTitle("");
    OBJ_29.setName("OBJ_29");

    //---- OBJ_35 ----
    OBJ_35.setText("@WNMA@");
    OBJ_35.setName("OBJ_35");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_38;
  private JXTitledSeparator OBJ_22;
  private JPanel panel1;
  private XRiTextField WMAG;
  private JLabel label1;
  private RiZoneSortie label2;
  private JXTitledSeparator OBJ_33;
  private JCheckBox WMAG_chk;
  private XRiCheckBox WTCLI;
  private JPanel P_SEL0;
  private RiZoneSortie OBJ_53;
  private JLabel OBJ_52;
  private XRiTextField WCLI;
  private XRiCheckBox WTFAM;
  private JPanel P_SEL1;
  private RiZoneSortie OBJ_36;
  private RiZoneSortie OBJ_37;
  private JLabel OBJ_32;
  private JLabel OBJ_26;
  private XRiTextField WFDEB;
  private XRiTextField WFFIN;
  private JPanel P_SEL2;
  private RiZoneSortie OBJ_42;
  private RiZoneSortie OBJ_43;
  private XRiTextField WARTD;
  private XRiTextField WARTF;
  private JLabel OBJ_41;
  private JLabel OBJ_40;
  private XRiCheckBox WTART;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JXTitledSeparator OBJ_29;
  private JLabel OBJ_35;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
