
package ri.serien.libecranrpg.sgvx.SGVX94FM;

// Nom Fichier: b_SGVX94FM_FMTB3_FMTF1_405.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX94FM_B4 extends SNPanelEcranRPG implements ioFrame {
   
  
  public SGVX94FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    planification.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SAUV@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    CTT001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS001@ @CTN001@")).trim());
    PAT002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS002@ @CTN002@")).trim());
    PAT003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS003@ @CTN003@")).trim());
    PAT004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS004@ @CTN004@")).trim());
    PAT005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS005@ @CTN005@")).trim());
    PAT006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS006@ @CTN006@")).trim());
    PAT007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS007@ @CTN007@")).trim());
    PAT008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS008@ @CTN008@")).trim());
    PAT009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS009@ @CTN009@")).trim());
    PAT010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS010@ @CTN010@")).trim());
    PAT011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS011@ @CTN011@")).trim());
    PAT012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS012@ @CTN012@")).trim());
    PAT013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS013@ @CTN013@")).trim());
    PAT014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS014@ @CTN014@")).trim());
    PAT015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS015@ @CTN015@")).trim());
    PAT016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS016@ @CTN016@")).trim());
  }
  
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    planification.setVisible(lexique.isTrue("96"));
    panel1.setVisible(!WTETB.isSelected());
    
    // TODO Icones
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTETBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTETB", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    planification = new JLabel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CTT001 = new JLabel();
    PAT002 = new JLabel();
    PAT003 = new JLabel();
    PAT004 = new JLabel();
    PAT005 = new JLabel();
    CTM001 = new XRiTextField();
    CTM002 = new XRiTextField();
    CTM003 = new XRiTextField();
    CTM004 = new XRiTextField();
    CTM005 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    PAT006 = new JLabel();
    PAT007 = new JLabel();
    PAT008 = new JLabel();
    PAT009 = new JLabel();
    PAT010 = new JLabel();
    CTM006 = new XRiTextField();
    CTM007 = new XRiTextField();
    CTM008 = new XRiTextField();
    CTM009 = new XRiTextField();
    CTM010 = new XRiTextField();
    PAT011 = new JLabel();
    PAT012 = new JLabel();
    PAT013 = new JLabel();
    PAT014 = new JLabel();
    PAT015 = new JLabel();
    CTM011 = new XRiTextField();
    CTM012 = new XRiTextField();
    CTM013 = new XRiTextField();
    CTM014 = new XRiTextField();
    CTM015 = new XRiTextField();
    PAT016 = new JLabel();
    CTM016 = new XRiTextField();
    WTETB = new JButton();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- planification ----
          planification.setText("@SAUV@");
          planification.setFont(planification.getFont().deriveFont(planification.getFont().getStyle() | Font.BOLD));
          planification.setForeground(new Color(255, 0, 51));
          planification.setName("planification");
          p_tete_gauche.add(planification);
          planification.setBounds(5, 5, 674, 20);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(500, 630));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(500, 597));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Choix de l'\u00e9tablissement"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- CTT001 ----
            CTT001.setText("@CTS001@ @CTN001@");
            CTT001.setName("CTT001");
            panel1.add(CTT001);
            CTT001.setBounds(20, 40, 321, 27);

            //---- PAT002 ----
            PAT002.setText("@CTS002@ @CTN002@");
            PAT002.setName("PAT002");
            panel1.add(PAT002);
            PAT002.setBounds(20, 70, 321, 27);

            //---- PAT003 ----
            PAT003.setText("@CTS003@ @CTN003@");
            PAT003.setName("PAT003");
            panel1.add(PAT003);
            PAT003.setBounds(20, 100, 321, 27);

            //---- PAT004 ----
            PAT004.setText("@CTS004@ @CTN004@");
            PAT004.setName("PAT004");
            panel1.add(PAT004);
            PAT004.setBounds(20, 130, 321, 27);

            //---- PAT005 ----
            PAT005.setText("@CTS005@ @CTN005@");
            PAT005.setName("PAT005");
            panel1.add(PAT005);
            PAT005.setBounds(20, 160, 321, 27);

            //---- CTM001 ----
            CTM001.setName("CTM001");
            panel1.add(CTM001);
            CTM001.setBounds(360, 40, 60, CTM001.getPreferredSize().height);

            //---- CTM002 ----
            CTM002.setName("CTM002");
            panel1.add(CTM002);
            CTM002.setBounds(360, 70, 60, CTM002.getPreferredSize().height);

            //---- CTM003 ----
            CTM003.setName("CTM003");
            panel1.add(CTM003);
            CTM003.setBounds(360, 100, 60, CTM003.getPreferredSize().height);

            //---- CTM004 ----
            CTM004.setName("CTM004");
            panel1.add(CTM004);
            CTM004.setBounds(360, 130, 60, CTM004.getPreferredSize().height);

            //---- CTM005 ----
            CTM005.setName("CTM005");
            panel1.add(CTM005);
            CTM005.setBounds(360, 160, 60, CTM005.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Etablissement");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(20, 20, 321, 25);

            //---- label2 ----
            label2.setText("Purge");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(360, 20, 60, 25);

            //---- PAT006 ----
            PAT006.setText("@CTS006@ @CTN006@");
            PAT006.setName("PAT006");
            panel1.add(PAT006);
            PAT006.setBounds(20, 190, 321, 27);

            //---- PAT007 ----
            PAT007.setText("@CTS007@ @CTN007@");
            PAT007.setName("PAT007");
            panel1.add(PAT007);
            PAT007.setBounds(20, 220, 321, 27);

            //---- PAT008 ----
            PAT008.setText("@CTS008@ @CTN008@");
            PAT008.setName("PAT008");
            panel1.add(PAT008);
            PAT008.setBounds(20, 250, 321, 27);

            //---- PAT009 ----
            PAT009.setText("@CTS009@ @CTN009@");
            PAT009.setName("PAT009");
            panel1.add(PAT009);
            PAT009.setBounds(20, 280, 321, 27);

            //---- PAT010 ----
            PAT010.setText("@CTS010@ @CTN010@");
            PAT010.setName("PAT010");
            panel1.add(PAT010);
            PAT010.setBounds(20, 310, 321, 27);

            //---- CTM006 ----
            CTM006.setName("CTM006");
            panel1.add(CTM006);
            CTM006.setBounds(360, 190, 60, CTM006.getPreferredSize().height);

            //---- CTM007 ----
            CTM007.setName("CTM007");
            panel1.add(CTM007);
            CTM007.setBounds(360, 220, 60, CTM007.getPreferredSize().height);

            //---- CTM008 ----
            CTM008.setName("CTM008");
            panel1.add(CTM008);
            CTM008.setBounds(360, 250, 60, CTM008.getPreferredSize().height);

            //---- CTM009 ----
            CTM009.setName("CTM009");
            panel1.add(CTM009);
            CTM009.setBounds(360, 280, 60, CTM009.getPreferredSize().height);

            //---- CTM010 ----
            CTM010.setName("CTM010");
            panel1.add(CTM010);
            CTM010.setBounds(360, 310, 60, CTM010.getPreferredSize().height);

            //---- PAT011 ----
            PAT011.setText("@CTS011@ @CTN011@");
            PAT011.setName("PAT011");
            panel1.add(PAT011);
            PAT011.setBounds(20, 340, 321, 27);

            //---- PAT012 ----
            PAT012.setText("@CTS012@ @CTN012@");
            PAT012.setName("PAT012");
            panel1.add(PAT012);
            PAT012.setBounds(20, 370, 321, 27);

            //---- PAT013 ----
            PAT013.setText("@CTS013@ @CTN013@");
            PAT013.setName("PAT013");
            panel1.add(PAT013);
            PAT013.setBounds(20, 400, 321, 27);

            //---- PAT014 ----
            PAT014.setText("@CTS014@ @CTN014@");
            PAT014.setName("PAT014");
            panel1.add(PAT014);
            PAT014.setBounds(20, 430, 321, 27);

            //---- PAT015 ----
            PAT015.setText("@CTS015@ @CTN015@");
            PAT015.setName("PAT015");
            panel1.add(PAT015);
            PAT015.setBounds(20, 460, 321, 27);

            //---- CTM011 ----
            CTM011.setName("CTM011");
            panel1.add(CTM011);
            CTM011.setBounds(360, 340, 60, CTM011.getPreferredSize().height);

            //---- CTM012 ----
            CTM012.setName("CTM012");
            panel1.add(CTM012);
            CTM012.setBounds(360, 370, 60, CTM012.getPreferredSize().height);

            //---- CTM013 ----
            CTM013.setName("CTM013");
            panel1.add(CTM013);
            CTM013.setBounds(360, 400, 60, CTM013.getPreferredSize().height);

            //---- CTM014 ----
            CTM014.setName("CTM014");
            panel1.add(CTM014);
            CTM014.setBounds(360, 430, 60, CTM014.getPreferredSize().height);

            //---- CTM015 ----
            CTM015.setName("CTM015");
            panel1.add(CTM015);
            CTM015.setBounds(360, 460, 60, CTM015.getPreferredSize().height);

            //---- PAT016 ----
            PAT016.setText("@CTS016@ @CTN016@");
            PAT016.setName("PAT016");
            panel1.add(PAT016);
            PAT016.setBounds(20, 490, 321, 27);

            //---- CTM016 ----
            CTM016.setName("CTM016");
            panel1.add(CTM016);
            CTM016.setBounds(360, 490, 60, CTM016.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- WTETB ----
          WTETB.setText("S\u00e9lection de toutes les soci\u00e9t\u00e9s");
          WTETB.setName("WTETB");
          WTETB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTETBActionPerformed(e);
            }
          });

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                  .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                    .addGap(0, 214, Short.MAX_VALUE)
                    .addComponent(WTETB, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 534, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(WTETB)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
        // //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY
    // //GEN-BEGIN:variables
    private JPopupMenu BTD;
    private JMenuItem OBJ_11;
    private JMenuItem OBJ_12;
    private JPanel p_nord;
    private SNBandeauTitre p_bpresentation;
    private JMenuBar barre_tete;
    private JPanel p_tete_gauche;
    private JLabel planification;
    private JPanel p_tete_droite;
    private JLabel lb_loctp_;
    private JPanel p_sud;
    private JPanel p_menus;
    private JPanel menus_bas;
    private RiMenu navig_erreurs;
    private RiMenu_bt bouton_erreurs;
    private RiMenu navig_valid;
    private RiMenu_bt bouton_valider;
    private RiMenu navig_retour;
    private RiMenu_bt bouton_retour;
    private JScrollPane scroll_droite;
    private JPanel menus_haut;
    private RiMenu riMenu2;
    private RiMenu_bt riMenu_bt2;
    private RiSousMenu riSousMenu6;
    private RiSousMenu_bt riSousMenu_bt_export;
    private SNPanelDegradeGris p_centrage;
    private JPanel p_contenu;
    private JPanel panel1;
    private JLabel CTT001;
    private JLabel PAT002;
    private JLabel PAT003;
    private JLabel PAT004;
    private JLabel PAT005;
    private XRiTextField CTM001;
    private XRiTextField CTM002;
    private XRiTextField CTM003;
    private XRiTextField CTM004;
    private XRiTextField CTM005;
    private JLabel label1;
    private JLabel label2;
    private JLabel PAT006;
    private JLabel PAT007;
    private JLabel PAT008;
    private JLabel PAT009;
    private JLabel PAT010;
    private XRiTextField CTM006;
    private XRiTextField CTM007;
    private XRiTextField CTM008;
    private XRiTextField CTM009;
    private XRiTextField CTM010;
    private JLabel PAT011;
    private JLabel PAT012;
    private JLabel PAT013;
    private JLabel PAT014;
    private JLabel PAT015;
    private XRiTextField CTM011;
    private XRiTextField CTM012;
    private XRiTextField CTM013;
    private XRiTextField CTM014;
    private XRiTextField CTM015;
    private JLabel PAT016;
    private XRiTextField CTM016;
    private JButton WTETB;
    // JFormDesigner - End of variables declaration //GEN-END:variables
}
