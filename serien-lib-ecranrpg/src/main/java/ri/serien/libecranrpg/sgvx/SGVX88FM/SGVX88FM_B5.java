
package ri.serien.libecranrpg.sgvx.SGVX88FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX88FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX88FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TRIFRS.setValeursSelection("X", " ");
    TRIABC.setValeursSelection("X", " ");
    WTOU2.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
    EDTDES.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    ABCFIN.setEnabled(lexique.isPresent("ABCFIN"));
    ABCDEB.setEnabled(lexique.isPresent("ABCDEB"));
    MA12.setVisible(lexique.isPresent("MA12"));
    MA11.setVisible(lexique.isPresent("MA11"));
    MA10.setVisible(lexique.isPresent("MA10"));
    MA09.setVisible(lexique.isPresent("MA09"));
    MA08.setVisible(lexique.isPresent("MA08"));
    MA07.setVisible(lexique.isPresent("MA07"));
    MA06.setVisible(lexique.isPresent("MA06"));
    MA05.setVisible(lexique.isPresent("MA05"));
    MA04.setVisible(lexique.isPresent("MA04"));
    MA03.setVisible(lexique.isPresent("MA03"));
    MA02.setVisible(lexique.isPresent("MA02"));
    MA01.setVisible(lexique.isPresent("MA01"));
    // TRIFRS.setEnabled( lexique.isPresent("TRIFRS"));
    // TRIFRS.setSelected(lexique.HostFieldGetData("TRIFRS").equalsIgnoreCase("X"));
    // TRIABC.setEnabled( lexique.isPresent("TRIABC"));
    // TRIABC.setSelected(lexique.HostFieldGetData("TRIABC").equalsIgnoreCase("X"));
    FINFRS.setEnabled(lexique.isPresent("FINFRS"));
    DEBFRS.setEnabled(lexique.isPresent("DEBFRS"));
    // DATCHF.setEnabled( lexique.isPresent("DATCHF"));
    OBJ_68.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    OBJ_65.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    // WTOU2.setVisible( lexique.isPresent("WTOU2"));
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    // WTOU.setVisible( lexique.isPresent("WTOU"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // WTOUM.setVisible( lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    EFINA.setVisible(lexique.isPresent("EFINA"));
    EDEBA.setVisible(lexique.isPresent("EDEBA"));
    // EDTDES.setVisible( lexique.isPresent("EDTDES"));
    // EDTDES.setSelected(lexique.HostFieldGetData("EDTDES").equalsIgnoreCase("OUI"));
    P_SEL2.setVisible(!lexique.HostFieldGetData("WTOU2").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TRIFRS.isSelected())
    // lexique.HostFieldPutData("TRIFRS", 0, "X");
    // else
    // lexique.HostFieldPutData("TRIFRS", 0, " ");
    // if (TRIABC.isSelected())
    // lexique.HostFieldPutData("TRIABC", 0, "X");
    // else
    // lexique.HostFieldPutData("TRIABC", 0, " ");
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU2", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (EDTDES.isSelected())
    // lexique.HostFieldPutData("EDTDES", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EDTDES", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_62 = new JXTitledSeparator();
    OBJ_40 = new JXTitledSeparator();
    OBJ_46 = new JXTitledSeparator();
    P_SEL1 = new JPanel();
    EDEBA = new XRiTextField();
    EFINA = new XRiTextField();
    OBJ_65 = new JLabel();
    OBJ_68 = new JLabel();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    P_SEL2 = new JPanel();
    DEBFRS = new XRiTextField();
    FINFRS = new XRiTextField();
    OBJ_80 = new JLabel();
    OBJ_82 = new JLabel();
    EDTDES = new XRiCheckBox();
    OBJ_75 = new JLabel();
    WTOUM = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    WTOU2 = new XRiCheckBox();
    OBJ_76 = new JLabel();
    TRIABC = new XRiCheckBox();
    TRIFRS = new XRiCheckBox();
    OBJ_86 = new JLabel();
    ABCDEB = new XRiTextField();
    ABCFIN = new XRiTextField();
    OBJ_85 = new JLabel();
    DATCHF = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(660, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_62 ----
          OBJ_62.setTitle("Plage des articles");
          OBJ_62.setName("OBJ_62");

          //---- OBJ_40 ----
          OBJ_40.setTitle("Plage codes Fournisseurs");
          OBJ_40.setName("OBJ_40");

          //---- OBJ_46 ----
          OBJ_46.setTitle("Code magasin");
          OBJ_46.setName("OBJ_46");

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- EDEBA ----
            EDEBA.setComponentPopupMenu(BTD);
            EDEBA.setName("EDEBA");
            P_SEL1.add(EDEBA);
            EDEBA.setBounds(170, 7, 214, EDEBA.getPreferredSize().height);

            //---- EFINA ----
            EFINA.setComponentPopupMenu(BTD);
            EFINA.setName("EFINA");
            P_SEL1.add(EFINA);
            EFINA.setBounds(170, 36, 214, EFINA.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("Code d\u00e9but");
            OBJ_65.setName("OBJ_65");
            P_SEL1.add(OBJ_65);
            OBJ_65.setBounds(15, 11, 95, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Code famille fin");
            OBJ_68.setName("OBJ_68");
            P_SEL1.add(OBJ_68);
            OBJ_68.setBounds(15, 40, 100, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL1.setMinimumSize(preferredSize);
              P_SEL1.setPreferredSize(preferredSize);
            }
          }

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- MA01 ----
            MA01.setComponentPopupMenu(BTD);
            MA01.setName("MA01");
            P_SEL0.add(MA01);
            MA01.setBounds(9, 8, 34, MA01.getPreferredSize().height);

            //---- MA02 ----
            MA02.setComponentPopupMenu(BTD);
            MA02.setName("MA02");
            P_SEL0.add(MA02);
            MA02.setBounds(40, 8, 34, MA02.getPreferredSize().height);

            //---- MA03 ----
            MA03.setComponentPopupMenu(BTD);
            MA03.setName("MA03");
            P_SEL0.add(MA03);
            MA03.setBounds(71, 8, 34, MA03.getPreferredSize().height);

            //---- MA04 ----
            MA04.setComponentPopupMenu(BTD);
            MA04.setName("MA04");
            P_SEL0.add(MA04);
            MA04.setBounds(102, 8, 34, MA04.getPreferredSize().height);

            //---- MA05 ----
            MA05.setComponentPopupMenu(BTD);
            MA05.setName("MA05");
            P_SEL0.add(MA05);
            MA05.setBounds(133, 8, 34, MA05.getPreferredSize().height);

            //---- MA06 ----
            MA06.setComponentPopupMenu(BTD);
            MA06.setName("MA06");
            P_SEL0.add(MA06);
            MA06.setBounds(164, 8, 34, MA06.getPreferredSize().height);

            //---- MA07 ----
            MA07.setComponentPopupMenu(BTD);
            MA07.setName("MA07");
            P_SEL0.add(MA07);
            MA07.setBounds(195, 8, 34, MA07.getPreferredSize().height);

            //---- MA08 ----
            MA08.setComponentPopupMenu(BTD);
            MA08.setName("MA08");
            P_SEL0.add(MA08);
            MA08.setBounds(226, 8, 34, MA08.getPreferredSize().height);

            //---- MA09 ----
            MA09.setComponentPopupMenu(BTD);
            MA09.setName("MA09");
            P_SEL0.add(MA09);
            MA09.setBounds(257, 8, 34, MA09.getPreferredSize().height);

            //---- MA10 ----
            MA10.setComponentPopupMenu(BTD);
            MA10.setName("MA10");
            P_SEL0.add(MA10);
            MA10.setBounds(288, 8, 34, MA10.getPreferredSize().height);

            //---- MA11 ----
            MA11.setComponentPopupMenu(BTD);
            MA11.setName("MA11");
            P_SEL0.add(MA11);
            MA11.setBounds(319, 8, 34, MA11.getPreferredSize().height);

            //---- MA12 ----
            MA12.setComponentPopupMenu(BTD);
            MA12.setName("MA12");
            P_SEL0.add(MA12);
            MA12.setBounds(350, 8, 34, MA12.getPreferredSize().height);
          }

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- DEBFRS ----
            DEBFRS.setComponentPopupMenu(BTD);
            DEBFRS.setName("DEBFRS");
            P_SEL2.add(DEBFRS);
            DEBFRS.setBounds(75, 5, 68, DEBFRS.getPreferredSize().height);

            //---- FINFRS ----
            FINFRS.setComponentPopupMenu(BTD);
            FINFRS.setName("FINFRS");
            P_SEL2.add(FINFRS);
            FINFRS.setBounds(240, 5, 68, FINFRS.getPreferredSize().height);

            //---- OBJ_80 ----
            OBJ_80.setText("D\u00e9but");
            OBJ_80.setName("OBJ_80");
            P_SEL2.add(OBJ_80);
            OBJ_80.setBounds(15, 10, 39, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Fin");
            OBJ_82.setName("OBJ_82");
            P_SEL2.add(OBJ_82);
            OBJ_82.setBounds(195, 9, 21, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL2.getComponentCount(); i++) {
                Rectangle bounds = P_SEL2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL2.setMinimumSize(preferredSize);
              P_SEL2.setPreferredSize(preferredSize);
            }
          }

          //---- EDTDES ----
          EDTDES.setText("Edition des articles d\u00e9sactiv\u00e9s");
          EDTDES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTDES.setName("EDTDES");

          //---- OBJ_75 ----
          OBJ_75.setText("Chiffrage au PUMP au");
          OBJ_75.setName("OBJ_75");

          //---- WTOUM ----
          WTOUM.setText("S\u00e9lection compl\u00e8te");
          WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUM.setName("WTOUM");
          WTOUM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUMActionPerformed(e);
            }
          });

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU2.setName("WTOU2");
          WTOU2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU2ActionPerformed(e);
            }
          });

          //---- OBJ_76 ----
          OBJ_76.setText("Choix code ABC");
          OBJ_76.setName("OBJ_76");

          //---- TRIABC ----
          TRIABC.setText("Tri");
          TRIABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIABC.setName("TRIABC");

          //---- TRIFRS ----
          TRIFRS.setText("Tri");
          TRIFRS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIFRS.setName("TRIFRS");

          //---- OBJ_86 ----
          OBJ_86.setText("De");
          OBJ_86.setName("OBJ_86");

          //---- ABCDEB ----
          ABCDEB.setComponentPopupMenu(BTD);
          ABCDEB.setName("ABCDEB");

          //---- ABCFIN ----
          ABCFIN.setComponentPopupMenu(BTD);
          ABCFIN.setName("ABCFIN");

          //---- OBJ_85 ----
          OBJ_85.setText("\u00e0");
          OBJ_85.setName("OBJ_85");

          //---- DATCHF ----
          DATCHF.setName("DATCHF");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(DATCHF, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(ABCDEB, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(ABCFIN, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(TRIABC, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(TRIFRS, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(EDTDES, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DATCHF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ABCDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ABCFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(TRIABC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(TRIFRS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20)
                .addComponent(EDTDES, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_62;
  private JXTitledSeparator OBJ_40;
  private JXTitledSeparator OBJ_46;
  private JPanel P_SEL1;
  private XRiTextField EDEBA;
  private XRiTextField EFINA;
  private JLabel OBJ_65;
  private JLabel OBJ_68;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPanel P_SEL2;
  private XRiTextField DEBFRS;
  private XRiTextField FINFRS;
  private JLabel OBJ_80;
  private JLabel OBJ_82;
  private XRiCheckBox EDTDES;
  private JLabel OBJ_75;
  private XRiCheckBox WTOUM;
  private XRiCheckBox WTOU;
  private XRiCheckBox WTOU2;
  private JLabel OBJ_76;
  private XRiCheckBox TRIABC;
  private XRiCheckBox TRIFRS;
  private JLabel OBJ_86;
  private XRiTextField ABCDEB;
  private XRiTextField ABCFIN;
  private JLabel OBJ_85;
  private XRiCalendrier DATCHF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
