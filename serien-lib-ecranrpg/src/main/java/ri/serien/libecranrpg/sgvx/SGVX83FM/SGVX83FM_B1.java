
package ri.serien.libecranrpg.sgvx.SGVX83FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX83FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX83FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOUFR.setValeursSelection("**", "  ");
    WTOUA.setValeursSelection("**", "  ");
    WTOUFA.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
    WFOR.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    FRFIN.setEnabled(lexique.isPresent("FRFIN"));
    FRDEB.setEnabled(lexique.isPresent("FRDEB"));
    // WTOUFR.setVisible( lexique.isPresent("WTOUFR"));
    // WTOUFR.setEnabled( lexique.isPresent("WTOUFR"));
    // WTOUFR.setSelected(lexique.HostFieldGetData("WTOUFR").equalsIgnoreCase("**"));
    // WTOUA.setVisible( lexique.isPresent("WTOUA"));
    // WTOUA.setEnabled( lexique.isPresent("WTOUA"));
    // WTOUA.setSelected(lexique.HostFieldGetData("WTOUA").equalsIgnoreCase("**"));
    // WTOUFA.setVisible( lexique.isPresent("WTOUFA"));
    // WTOUFA.setEnabled( lexique.isPresent("WTOUFA"));
    // WTOUFA.setSelected(lexique.HostFieldGetData("WTOUFA").equalsIgnoreCase("**"));
    // WTOUM.setVisible( lexique.isPresent("WTOUM"));
    // WTOUM.setEnabled( lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    EFINA.setEnabled(lexique.isPresent("EFINA"));
    EDEBA.setEnabled(lexique.isPresent("EDEBA"));
    // WFOR.setSelected(lexique.HostFieldGetData("WFOR").equalsIgnoreCase("X"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**"));
    P_SEL3.setVisible(!lexique.HostFieldGetData("WTOUFR").trim().equalsIgnoreCase("**"));
    FRFIN.setEnabled(lexique.isPresent("FRFIN"));
    P_SEL2.setVisible(!lexique.HostFieldGetData("WTOUA").trim().equalsIgnoreCase("**"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOUFA").trim().equalsIgnoreCase("**"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOUFR.isSelected())
    // lexique.HostFieldPutData("WTOUFR", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUFR", 0, " ");
    // if (WTOUA.isSelected())
    // lexique.HostFieldPutData("WTOUA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUA", 0, " ");
    // if (WTOUFA.isSelected())
    // lexique.HostFieldPutData("WTOUFA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUFA", 0, " ");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (WFOR.isSelected())
    // lexique.HostFieldPutData("WFOR", 0, "X");
    // else
    // lexique.HostFieldPutData("WFOR", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUFAActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOUAActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOUFRActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_38 = new JXTitledSeparator();
    OBJ_45 = new JXTitledSeparator();
    OBJ_52 = new JXTitledSeparator();
    OBJ_23 = new JXTitledSeparator();
    WFOR = new XRiCheckBox();
    panel1 = new JPanel();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    WTOUM = new XRiCheckBox();
    panel2 = new JPanel();
    WTOUFA = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    EDEB = new XRiTextField();
    EFIN = new XRiTextField();
    panel3 = new JPanel();
    WTOUA = new XRiCheckBox();
    P_SEL2 = new JPanel();
    EDEBA = new XRiTextField();
    EFINA = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_50 = new JLabel();
    panel4 = new JPanel();
    WTOUFR = new XRiCheckBox();
    P_SEL3 = new JPanel();
    OBJ_55 = new JLabel();
    OBJ_57 = new JLabel();
    FRDEB = new XRiTextField();
    FRFIN = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(665, 565));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_38 ----
          OBJ_38.setTitle("Plage de famille");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_45 ----
          OBJ_45.setTitle("Plage articles");
          OBJ_45.setName("OBJ_45");

          //---- OBJ_52 ----
          OBJ_52.setTitle("Fournisseur principal");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_23 ----
          OBJ_23.setTitle("Code(s) Magasin(s) \u00e0 traiter");
          OBJ_23.setName("OBJ_23");

          //---- WFOR ----
          WFOR.setText("Nbr vente < nb vente mini -> Stk mini = 0");
          WFOR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WFOR.setName("WFOR");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(10, 9, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(42, 9, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(74, 9, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(106, 9, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(138, 9, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(170, 9, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(202, 9, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(234, 9, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(266, 9, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(298, 9, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(330, 9, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(362, 9, 33, MA12.getPreferredSize().height);
            }
            panel1.add(P_SEL0);
            P_SEL0.setBounds(160, 5, 405, 44);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            panel1.add(WTOUM);
            WTOUM.setBounds(5, 17, 140, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WTOUFA ----
            WTOUFA.setText("S\u00e9lection compl\u00e8te");
            WTOUFA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUFA.setName("WTOUFA");
            WTOUFA.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUFAActionPerformed(e);
              }
            });
            panel2.add(WTOUFA);
            WTOUFA.setBounds(5, 27, 140, 20);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_41 ----
              OBJ_41.setText("Code de d\u00e9but");
              OBJ_41.setName("OBJ_41");
              P_SEL1.add(OBJ_41);
              OBJ_41.setBounds(15, 9, 93, 20);

              //---- OBJ_43 ----
              OBJ_43.setText("Code de fin");
              OBJ_43.setName("OBJ_43");
              P_SEL1.add(OBJ_43);
              OBJ_43.setBounds(15, 38, 93, 20);

              //---- EDEB ----
              EDEB.setComponentPopupMenu(BTD);
              EDEB.setName("EDEB");
              P_SEL1.add(EDEB);
              EDEB.setBounds(155, 5, 44, EDEB.getPreferredSize().height);

              //---- EFIN ----
              EFIN.setComponentPopupMenu(BTD);
              EFIN.setName("EFIN");
              P_SEL1.add(EFIN);
              EFIN.setBounds(155, 34, 44, EFIN.getPreferredSize().height);
            }
            panel2.add(P_SEL1);
            P_SEL1.setBounds(160, 5, 405, 65);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WTOUA ----
            WTOUA.setText("S\u00e9lection compl\u00e8te");
            WTOUA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUA.setName("WTOUA");
            WTOUA.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUAActionPerformed(e);
              }
            });
            panel3.add(WTOUA);
            WTOUA.setBounds(5, 25, 140, 20);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- EDEBA ----
              EDEBA.setComponentPopupMenu(BTD);
              EDEBA.setName("EDEBA");
              P_SEL2.add(EDEBA);
              EDEBA.setBounds(155, 4, 214, EDEBA.getPreferredSize().height);

              //---- EFINA ----
              EFINA.setComponentPopupMenu(BTD);
              EFINA.setName("EFINA");
              P_SEL2.add(EFINA);
              EFINA.setBounds(155, 33, 214, EFINA.getPreferredSize().height);

              //---- OBJ_48 ----
              OBJ_48.setText("Code de d\u00e9but");
              OBJ_48.setName("OBJ_48");
              P_SEL2.add(OBJ_48);
              OBJ_48.setBounds(15, 8, 93, 20);

              //---- OBJ_50 ----
              OBJ_50.setText("Code de fin");
              OBJ_50.setName("OBJ_50");
              P_SEL2.add(OBJ_50);
              OBJ_50.setBounds(15, 35, 93, 20);
            }
            panel3.add(P_SEL2);
            P_SEL2.setBounds(160, 0, 405, 66);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- WTOUFR ----
            WTOUFR.setText("S\u00e9lection compl\u00e8te");
            WTOUFR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUFR.setName("WTOUFR");
            WTOUFR.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUFRActionPerformed(e);
              }
            });
            panel4.add(WTOUFR);
            WTOUFR.setBounds(5, 25, 140, 20);

            //======== P_SEL3 ========
            {
              P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL3.setOpaque(false);
              P_SEL3.setName("P_SEL3");
              P_SEL3.setLayout(null);

              //---- OBJ_55 ----
              OBJ_55.setText("Code de d\u00e9but");
              OBJ_55.setName("OBJ_55");
              P_SEL3.add(OBJ_55);
              OBJ_55.setBounds(15, 8, 93, 20);

              //---- OBJ_57 ----
              OBJ_57.setText("Code de fin");
              OBJ_57.setName("OBJ_57");
              P_SEL3.add(OBJ_57);
              OBJ_57.setBounds(15, 34, 93, 20);

              //---- FRDEB ----
              FRDEB.setComponentPopupMenu(BTD);
              FRDEB.setName("FRDEB");
              P_SEL3.add(FRDEB);
              FRDEB.setBounds(155, 4, 68, FRDEB.getPreferredSize().height);

              //---- FRFIN ----
              FRFIN.setComponentPopupMenu(BTD);
              FRFIN.setName("FRFIN");
              P_SEL3.add(FRFIN);
              FRFIN.setBounds(155, 33, 68, FRFIN.getPreferredSize().height);
            }
            panel4.add(P_SEL3);
            P_SEL3.setBounds(160, 5, 405, 65);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 595, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 595, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 595, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 595, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 595, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WFOR, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WFOR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_38;
  private JXTitledSeparator OBJ_45;
  private JXTitledSeparator OBJ_52;
  private JXTitledSeparator OBJ_23;
  private XRiCheckBox WFOR;
  private JPanel panel1;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private XRiCheckBox WTOUM;
  private JPanel panel2;
  private XRiCheckBox WTOUFA;
  private JPanel P_SEL1;
  private JLabel OBJ_41;
  private JLabel OBJ_43;
  private XRiTextField EDEB;
  private XRiTextField EFIN;
  private JPanel panel3;
  private XRiCheckBox WTOUA;
  private JPanel P_SEL2;
  private XRiTextField EDEBA;
  private XRiTextField EFINA;
  private JLabel OBJ_48;
  private JLabel OBJ_50;
  private JPanel panel4;
  private XRiCheckBox WTOUFR;
  private JPanel P_SEL3;
  private JLabel OBJ_55;
  private JLabel OBJ_57;
  private XRiTextField FRDEB;
  private XRiTextField FRFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
