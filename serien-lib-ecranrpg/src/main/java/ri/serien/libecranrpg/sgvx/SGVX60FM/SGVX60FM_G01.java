
package ri.serien.libecranrpg.sgvx.SGVX60FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class SGVX60FM_G01 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  private RiGraphe graphe2 = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  
  /**
   * Constructeur.
   */
  public SGVX60FM_G01(JPanel pPanel, Lexical pLexique, iData pInterpreteurD) {
    master = pPanel;
    lexique = pLexique;
    interpreteurD = pInterpreteurD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  public void setData() {
    // Graphe
    String[] libelle1 = new String[12];
    String[] libelle2 = new String[12];
    String[] donnee1 = new String[12];
    String[] donnee2 = new String[12];
    
    // Chargement des libellés
    for (int i = 0; i < libelle1.length; i++) {
      libelle1[i] = lexique.HostFieldGetData("MOI" + ((i + 1) < 10 ? +(i + 1) : (i + 1)));
    }
    for (int i = 0; i < libelle2.length; i++) {
      libelle2[i] = lexique.HostFieldGetData("MOI" + ((i + 13)));
    }
    
    // Chargement des données
    for (int i = 0; i < donnee1.length; i++) {
      if (lexique.HostFieldGetData("S1X" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim().equalsIgnoreCase("")) {
        donnee1[i] = "0.0";
      }
      else {
        donnee1[i] = lexique.HostFieldGetNumericData("S1X" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
      }
    }
    for (int i = 0; i < donnee2.length; i++) {
      if (lexique.HostFieldGetData("S1X" + ((i + 13))).trim().equalsIgnoreCase("")) {
        donnee2[i] = "0.0";
      }
      else {
        donnee2[i] = lexique.HostFieldGetNumericData("S1X" + ((i + 13))).trim();
      }
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle1.length][2];
    Object[][] data2 = new Object[libelle2.length][2];
    for (int i = 0; i < libelle1.length; i++) {
      data[i][0] = libelle1[i];
      data2[i][0] = libelle2[i];
      data[i][1] = Double.parseDouble(donnee1[i].trim());
      data2[i][1] = Double.parseDouble(donnee2[i].trim());
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Première année", false);
    
    graphe2.setDonnee(data2, "", false);
    graphe2.getGraphe("Deuxième année", false);
    
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    l_graphe2.setIcon(graphe2.getPicture(l_graphe2.getWidth(), l_graphe2.getHeight()));
    
    
    
    // TODO Icones
    button1.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    l_graphe2 = new JLabel();
    separator1 = compFactory.createSeparator(" ");
    button1 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      
      // ---- l_graphe2 ----
      l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe2.setComponentPopupMenu(null);
      l_graphe2.setBackground(new Color(214, 217, 223));
      l_graphe2.setName("l_graphe2");
      
      // ---- separator1 ----
      separator1.setName("separator1");
      
      // ---- button1 ----
      button1.setText("Retour");
      button1.setFont(button1.getFont().deriveFont(button1.getFont().getStyle() | Font.BOLD, button1.getFont().getSize() + 2f));
      button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      button1.setName("button1");
      button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button1ActionPerformed(e);
        }
      });
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(12, 12, 12).addComponent(separator1, GroupLayout.PREFERRED_SIZE, 1068,
              GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(15, 15, 15)
              .addGroup(P_CentreLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(button1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addGroup(P_CentreLayout.createSequentialGroup()
                      .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 523, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                      .addComponent(l_graphe2, GroupLayout.PREFERRED_SIZE, 523, GroupLayout.PREFERRED_SIZE)))));
      P_CentreLayout
          .setVerticalGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup().addGap(11, 11, 11)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addGroup(P_CentreLayout.createParallelGroup()
                      .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 554, GroupLayout.PREFERRED_SIZE)
                      .addComponent(l_graphe2, GroupLayout.PREFERRED_SIZE, 554, GroupLayout.PREFERRED_SIZE))
                  .addGap(31, 31, 31).addComponent(button1, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap()));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JLabel l_graphe2;
  private JComponent separator1;
  private JButton button1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
