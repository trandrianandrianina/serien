
package ri.serien.libecranrpg.sgvx.SGVX70FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX70FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORTER = "Exporter";
  
  public SGVX70FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WDET.setValeursSelection("OUI", "NON");
    WTOU2.setValeursSelection("**", "  ");
    UOPT8.setValeursSelection("X", " ");
    UOPT7.setValeursSelection("X", " ");
    UOPT6.setValeursSelection("X", " ");
    UOPT5.setValeursSelection("X", " ");
    UOPT4.setValeursSelection("X", " ");
    UOPT3.setValeursSelection("X", " ");
    UOPT2.setValeursSelection("X", " ");
    UOPT1.setValeursSelection("X", " ");
    
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEtablissement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    boolean isVuePeriode = lexique.isTrue("91");
    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    lbPeriode.setVisible(isVuePeriode);
    pnlPeriode.setVisible(isVuePeriode);
    lbBordereauDebut.setVisible(!isVuePeriode);
    pnlBordereaux.setVisible(!isVuePeriode);
    pnlTypes.setVisible(!WTOU2.isSelected());
    
    

    
    p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getCodeEtablissement());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    pnlTypes.setVisible(!pnlTypes.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbPeriode = new SNLabelChamp();
    pnlPeriode = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    lbBordereauDebut = new SNLabelChamp();
    pnlBordereaux = new SNPanel();
    NUMDEB = new XRiTextField();
    lbBordereauFin = new SNLabelChamp();
    NUMFIN = new XRiTextField();
    pnlTypesBordereaux = new SNPanelTitre();
    WTOU2 = new XRiCheckBox();
    pnlTypes = new SNPanel();
    UOPT1 = new XRiCheckBox();
    UOPT2 = new XRiCheckBox();
    UOPT3 = new XRiCheckBox();
    UOPT4 = new XRiCheckBox();
    UOPT5 = new XRiCheckBox();
    UOPT6 = new XRiCheckBox();
    UOPT7 = new XRiCheckBox();
    UOPT8 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionner = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEtablissement = new SNTexte();
    pnlOptions = new SNPanelTitre();
    WDET = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());

      //======== pnlColonne ========
      {
        pnlColonne.setPreferredSize(new Dimension(700, 520));
        pnlColonne.setBackground(new Color(239, 239, 222));
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlCritereSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlCritereSelection.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlCritereSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlCritereSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode \u00e0 \u00e9diter du");
            lbPeriode.setPreferredSize(new Dimension(175, 30));
            lbPeriode.setMinimumSize(new Dimension(175, 30));
            lbPeriode.setName("lbPeriode");
            pnlCritereSelection.add(lbPeriode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== pnlPeriode ========
            {
              pnlPeriode.setName("pnlPeriode");
              pnlPeriode.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlPeriode.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
              ((GridBagLayout)pnlPeriode.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlPeriode.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlPeriode.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- PERDEB ----
              PERDEB.setMaximumSize(new Dimension(120, 30));
              PERDEB.setMinimumSize(new Dimension(120, 30));
              PERDEB.setPreferredSize(new Dimension(120, 30));
              PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERDEB.setName("PERDEB");
              pnlPeriode.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbAu ----
              lbAu.setText("au");
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlPeriode.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- PERFIN ----
              PERFIN.setMaximumSize(new Dimension(120, 30));
              PERFIN.setMinimumSize(new Dimension(120, 30));
              PERFIN.setPreferredSize(new Dimension(120, 30));
              PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              PERFIN.setName("PERFIN");
              pnlPeriode.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlPeriode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbBordereauDebut ----
            lbBordereauDebut.setText("Bordereau de d\u00e9but");
            lbBordereauDebut.setPreferredSize(new Dimension(175, 30));
            lbBordereauDebut.setMinimumSize(new Dimension(175, 30));
            lbBordereauDebut.setName("lbBordereauDebut");
            pnlCritereSelection.add(lbBordereauDebut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //======== pnlBordereaux ========
            {
              pnlBordereaux.setName("pnlBordereaux");
              pnlBordereaux.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlBordereaux.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
              ((GridBagLayout)pnlBordereaux.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlBordereaux.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlBordereaux.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- NUMDEB ----
              NUMDEB.setPreferredSize(new Dimension(70, 30));
              NUMDEB.setMinimumSize(new Dimension(70, 30));
              NUMDEB.setMaximumSize(new Dimension(70, 30));
              NUMDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMDEB.setName("NUMDEB");
              pnlBordereaux.add(NUMDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbBordereauFin ----
              lbBordereauFin.setText("Bordereau de fin");
              lbBordereauFin.setName("lbBordereauFin");
              pnlBordereaux.add(lbBordereauFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- NUMFIN ----
              NUMFIN.setPreferredSize(new Dimension(70, 30));
              NUMFIN.setMinimumSize(new Dimension(70, 30));
              NUMFIN.setMaximumSize(new Dimension(70, 30));
              NUMFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMFIN.setName("NUMFIN");
              pnlBordereaux.add(NUMFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            }
            pnlCritereSelection.add(pnlBordereaux, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlTypesBordereaux ========
          {
            pnlTypesBordereaux.setOpaque(false);
            pnlTypesBordereaux.setTitre("Types de bordereaux");
            pnlTypesBordereaux.setName("pnlTypesBordereaux");
            pnlTypesBordereaux.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlTypesBordereaux.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlTypesBordereaux.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlTypesBordereaux.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)pnlTypesBordereaux.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- WTOU2 ----
            WTOU2.setText("S\u00e9lection compl\u00e8te");
            WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU2.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTOU2.setPreferredSize(new Dimension(200, 30));
            WTOU2.setMinimumSize(new Dimension(200, 30));
            WTOU2.setMaximumSize(new Dimension(200, 30));
            WTOU2.setName("WTOU2");
            WTOU2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU2ActionPerformed(e);
              }
            });
            pnlTypesBordereaux.add(WTOU2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== pnlTypes ========
            {
              pnlTypes.setName("pnlTypes");
              pnlTypes.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlTypes.getLayout()).columnWidths = new int[] {0, 0};
              ((GridBagLayout)pnlTypes.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
              ((GridBagLayout)pnlTypes.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
              ((GridBagLayout)pnlTypes.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

              //---- UOPT1 ----
              UOPT1.setText("Entr\u00e9e en stocks");
              UOPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT1.setPreferredSize(new Dimension(200, 30));
              UOPT1.setMinimumSize(new Dimension(200, 30));
              UOPT1.setMaximumSize(new Dimension(200, 30));
              UOPT1.setName("UOPT1");
              pnlTypes.add(UOPT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- UOPT2 ----
              UOPT2.setText("Sortie de stocks");
              UOPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT2.setPreferredSize(new Dimension(200, 30));
              UOPT2.setMinimumSize(new Dimension(200, 30));
              UOPT2.setMaximumSize(new Dimension(200, 30));
              UOPT2.setName("UOPT2");
              pnlTypes.add(UOPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- UOPT3 ----
              UOPT3.setText("Divers mouvements");
              UOPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT3.setPreferredSize(new Dimension(200, 30));
              UOPT3.setMinimumSize(new Dimension(200, 30));
              UOPT3.setMaximumSize(new Dimension(200, 30));
              UOPT3.setName("UOPT3");
              pnlTypes.add(UOPT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- UOPT4 ----
              UOPT4.setText("Transferts (E -> R)");
              UOPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT4.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT4.setPreferredSize(new Dimension(200, 30));
              UOPT4.setMinimumSize(new Dimension(200, 30));
              UOPT4.setMaximumSize(new Dimension(200, 30));
              UOPT4.setName("UOPT4");
              pnlTypes.add(UOPT4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- UOPT5 ----
              UOPT5.setText("Inventaires");
              UOPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT5.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT5.setPreferredSize(new Dimension(200, 30));
              UOPT5.setMinimumSize(new Dimension(200, 30));
              UOPT5.setMaximumSize(new Dimension(200, 30));
              UOPT5.setName("UOPT5");
              pnlTypes.add(UOPT5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- UOPT6 ----
              UOPT6.setText("Stock minimum");
              UOPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT6.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT6.setPreferredSize(new Dimension(200, 30));
              UOPT6.setMinimumSize(new Dimension(200, 30));
              UOPT6.setMaximumSize(new Dimension(200, 30));
              UOPT6.setName("UOPT6");
              pnlTypes.add(UOPT6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- UOPT7 ----
              UOPT7.setText("P.U.M.P");
              UOPT7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT7.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT7.setPreferredSize(new Dimension(200, 30));
              UOPT7.setMinimumSize(new Dimension(200, 30));
              UOPT7.setMaximumSize(new Dimension(200, 30));
              UOPT7.setName("UOPT7");
              pnlTypes.add(UOPT7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- UOPT8 ----
              UOPT8.setText("D\u00e9pr\u00e9ciations de stocks");
              UOPT8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UOPT8.setFont(new Font("sansserif", Font.PLAIN, 14));
              UOPT8.setPreferredSize(new Dimension(200, 30));
              UOPT8.setMinimumSize(new Dimension(200, 30));
              UOPT8.setMaximumSize(new Dimension(200, 30));
              UOPT8.setName("UOPT8");
              pnlTypes.add(UOPT8, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlTypesBordereaux.add(pnlTypes, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlTypesBordereaux, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //======== pnlEtablissementSelectionner ========
          {
            pnlEtablissementSelectionner.setTitre("Etablissement et magasin");
            pnlEtablissementSelectionner.setName("pnlEtablissementSelectionner");
            pnlEtablissementSelectionner.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlEtablissementSelectionner.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissementSelectionner.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissementSelectionner.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlEtablissementSelectionner.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionner.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissementSelectionner.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionner.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- tfDateEtablissement ----
            tfDateEtablissement.setText("@WENCX@");
            tfDateEtablissement.setEditable(false);
            tfDateEtablissement.setPreferredSize(new Dimension(264, 30));
            tfDateEtablissement.setMinimumSize(new Dimension(264, 30));
            tfDateEtablissement.setEnabled(false);
            tfDateEtablissement.setName("tfDateEtablissement");
            pnlEtablissementSelectionner.add(tfDateEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionner, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlOptions ========
          {
            pnlOptions.setOpaque(false);
            pnlOptions.setTitre("Options d'\u00e9dition");
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- WDET ----
            WDET.setText("D\u00e9tail des lignes articles");
            WDET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WDET.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDET.setPreferredSize(new Dimension(200, 30));
            WDET.setMinimumSize(new Dimension(200, 30));
            WDET.setMaximumSize(new Dimension(200, 30));
            WDET.setName("WDET");
            pnlOptions.add(WDET, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbPeriode;
  private SNPanel pnlPeriode;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNLabelChamp lbBordereauDebut;
  private SNPanel pnlBordereaux;
  private XRiTextField NUMDEB;
  private SNLabelChamp lbBordereauFin;
  private XRiTextField NUMFIN;
  private SNPanelTitre pnlTypesBordereaux;
  private XRiCheckBox WTOU2;
  private SNPanel pnlTypes;
  private XRiCheckBox UOPT1;
  private XRiCheckBox UOPT2;
  private XRiCheckBox UOPT3;
  private XRiCheckBox UOPT4;
  private XRiCheckBox UOPT5;
  private XRiCheckBox UOPT6;
  private XRiCheckBox UOPT7;
  private XRiCheckBox UOPT8;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionner;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEtablissement;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox WDET;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
