
package ri.serien.libecranrpg.sgvx.SGVX90FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX90FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX90FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    DPTDIF.setValeursSelection("OUI", "NON");
    MESWF.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SAUV@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGCPLX@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HHMNSS@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS001@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_39.setVisible(lexique.isPresent("ETS001"));
    // DPTDIF.setVisible( lexique.isPresent("DPTDIF"));
    // DPTDIF.setSelected(lexique.HostFieldGetData("DPTDIF").equalsIgnoreCase("OUI"));
    OBJ_61.setVisible(lexique.isPresent("HHMNSS"));
    // MESWF.setEnabled( lexique.isPresent("MESWF"));
    // MESWF.setSelected(lexique.HostFieldGetData("MESWF").equalsIgnoreCase("OUI"));
    OBJ_60.setVisible(lexique.isPresent("HHMNSS"));
    OBJ_42.setVisible(lexique.isPresent("DGCPLX"));
    OBJ_41.setVisible(lexique.isPresent("ETN001"));
    OBJ_64.setVisible(lexique.isPresent("SAUV"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (DPTDIF.isSelected())
    // lexique.HostFieldPutData("DPTDIF", 0, "OUI");
    // else
    // lexique.HostFieldPutData("DPTDIF", 0, "NON");
    // if (MESWF.isSelected())
    // lexique.HostFieldPutData("MESWF", 0, "OUI");
    // else
    // lexique.HostFieldPutData("MESWF", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_64 = new JLabel();
    OBJ_41 = new RiZoneSortie();
    OBJ_42 = new RiZoneSortie();
    OBJ_60 = new JLabel();
    OBJ_40 = new JLabel();
    MESWF = new XRiCheckBox();
    OBJ_61 = new RiZoneSortie();
    DPTDIF = new XRiCheckBox();
    OBJ_38 = new JLabel();
    OBJ_43 = new JLabel();
    DGDECX = new XRiTextField();
    OBJ_39 = new RiZoneSortie();
    panel1 = new JPanel();
    OBJ_49 = new JLabel();
    OBJ_52 = new JLabel();
    DGDE1X = new XRiTextField();
    DGFE1X = new XRiTextField();
    panel2 = new JPanel();
    DGDE2X = new XRiTextField();
    DGFE2X = new XRiTextField();
    OBJ_50 = new JLabel();
    OBJ_53 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(620, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_64 ----
          OBJ_64.setText("@SAUV@");
          OBJ_64.setName("OBJ_64");

          //---- OBJ_41 ----
          OBJ_41.setText("@ETN001@");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_42 ----
          OBJ_42.setText("@DGCPLX@");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_60 ----
          OBJ_60.setText("D\u00e9part diff\u00e9r\u00e9 \u00e0");
          OBJ_60.setName("OBJ_60");

          //---- OBJ_40 ----
          OBJ_40.setText("Nom ou raison sociale");
          OBJ_40.setName("OBJ_40");

          //---- MESWF ----
          MESWF.setText("Message WorkFlow");
          MESWF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MESWF.setName("MESWF");

          //---- OBJ_61 ----
          OBJ_61.setText("@HHMNSS@");
          OBJ_61.setName("OBJ_61");

          //---- DPTDIF ----
          DPTDIF.setText("D\u00e9part diff\u00e9r\u00e9");
          DPTDIF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DPTDIF.setName("DPTDIF");

          //---- OBJ_38 ----
          OBJ_38.setText("Etablissement");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_43 ----
          OBJ_43.setText("Arr\u00eat\u00e9 sur p\u00e9riode");
          OBJ_43.setName("OBJ_43");

          //---- DGDECX ----
          DGDECX.setComponentPopupMenu(BTD);
          DGDECX.setName("DGDECX");

          //---- OBJ_39 ----
          OBJ_39.setText("@ETS001@");
          OBJ_39.setName("OBJ_39");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Exercice en cours"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_49 ----
            OBJ_49.setText("D\u00e9but");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(20, 39, 65, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("Fin");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(20, 74, 65, 20);

            //---- DGDE1X ----
            DGDE1X.setName("DGDE1X");
            panel1.add(DGDE1X);
            DGDE1X.setBounds(85, 35, 40, DGDE1X.getPreferredSize().height);

            //---- DGFE1X ----
            DGFE1X.setName("DGFE1X");
            panel1.add(DGFE1X);
            DGFE1X.setBounds(85, 70, 40, DGFE1X.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Exercice suivant"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- DGDE2X ----
            DGDE2X.setName("DGDE2X");
            panel2.add(DGDE2X);
            DGDE2X.setBounds(85, 35, 40, DGDE2X.getPreferredSize().height);

            //---- DGFE2X ----
            DGFE2X.setName("DGFE2X");
            panel2.add(DGFE2X);
            DGFE2X.setBounds(85, 70, 40, DGFE2X.getPreferredSize().height);

            //---- OBJ_50 ----
            OBJ_50.setText("D\u00e9but");
            OBJ_50.setName("OBJ_50");
            panel2.add(OBJ_50);
            OBJ_50.setBounds(20, 39, 65, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Fin");
            OBJ_53.setName("OBJ_53");
            panel2.add(OBJ_53);
            OBJ_53.setBounds(20, 75, 65, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(224, 224, 224)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(DGDECX, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(DPTDIF, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(MESWF, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 520, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DGDECX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(DPTDIF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MESWF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(14, 14, 14)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_64;
  private RiZoneSortie OBJ_41;
  private RiZoneSortie OBJ_42;
  private JLabel OBJ_60;
  private JLabel OBJ_40;
  private XRiCheckBox MESWF;
  private RiZoneSortie OBJ_61;
  private XRiCheckBox DPTDIF;
  private JLabel OBJ_38;
  private JLabel OBJ_43;
  private XRiTextField DGDECX;
  private RiZoneSortie OBJ_39;
  private JPanel panel1;
  private JLabel OBJ_49;
  private JLabel OBJ_52;
  private XRiTextField DGDE1X;
  private XRiTextField DGFE1X;
  private JPanel panel2;
  private XRiTextField DGDE2X;
  private XRiTextField DGFE2X;
  private JLabel OBJ_50;
  private JLabel OBJ_53;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
