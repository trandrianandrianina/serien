
package ri.serien.libecranrpg.sgvx.SGVX49FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX49FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX49FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU3.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBDA@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBFA@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBTD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    // WTOU3.setSelected(lexique.HostFieldGetData("WTOU3").equalsIgnoreCase("**"));
    
    P_SEL1.setVisible(!WTOU1.isSelected());
    P_SEL0.setVisible(!WTOU3.isSelected());
    P_SEL2.setVisible(!WTOU2.isSelected());
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU2", 0, " ");
    // if (WTOU1.isSelected())
    // lexique.HostFieldPutData("WTOU1", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU1", 0, " ");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_29 = new JXTitledSeparator();
    OBJ_27 = new JXTitledSeparator();
    OBJ_31 = new JXTitledSeparator();
    OBJ_38 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    OBJ_63 = new RiZoneSortie();
    OBJ_65 = new RiZoneSortie();
    WDART = new XRiTextField();
    WFART = new XRiTextField();
    OBJ_62 = new JLabel();
    OBJ_64 = new JLabel();
    WTOU2 = new XRiCheckBox();
    panel1 = new JPanel();
    OBJ_66 = new JLabel();
    OBJ_67 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    panel2 = new JPanel();
    WTOU1 = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_61 = new RiZoneSortie();
    OBJ_37 = new JLabel();
    WTD = new XRiTextField();
    panel3 = new JPanel();
    WTOU3 = new XRiCheckBox();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_60 = new JXTitledSeparator();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Choix papier");
              riSousMenu_bt_export.setToolTipText("Choix papier");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setEnabled(false);
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_29 ----
          OBJ_29.setTitle("Plage articles");
          OBJ_29.setName("OBJ_29");

          //---- OBJ_27 ----
          OBJ_27.setTitle("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_31 ----
          OBJ_31.setTitle("Types de divers");
          OBJ_31.setName("OBJ_31");

          //---- OBJ_38 ----
          OBJ_38.setTitle("Codes magasins");
          OBJ_38.setName("OBJ_38");

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- OBJ_63 ----
            OBJ_63.setText("@WLIBDA@");
            OBJ_63.setName("OBJ_63");
            P_SEL2.add(OBJ_63);
            OBJ_63.setBounds(340, 7, 250, OBJ_63.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("@WLIBFA@");
            OBJ_65.setName("OBJ_65");
            P_SEL2.add(OBJ_65);
            OBJ_65.setBounds(340, 37, 250, OBJ_65.getPreferredSize().height);

            //---- WDART ----
            WDART.setComponentPopupMenu(BTD);
            WDART.setName("WDART");
            P_SEL2.add(WDART);
            WDART.setBounds(110, 5, 214, WDART.getPreferredSize().height);

            //---- WFART ----
            WFART.setComponentPopupMenu(BTD);
            WFART.setName("WFART");
            P_SEL2.add(WFART);
            WFART.setBounds(110, 35, 214, WFART.getPreferredSize().height);

            //---- OBJ_62 ----
            OBJ_62.setText("Code d\u00e9but");
            OBJ_62.setName("OBJ_62");
            P_SEL2.add(OBJ_62);
            OBJ_62.setBounds(15, 10, 75, 18);

            //---- OBJ_64 ----
            OBJ_64.setText("Code fin");
            OBJ_64.setName("OBJ_64");
            P_SEL2.add(OBJ_64);
            OBJ_64.setBounds(15, 40, 75, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL2.getComponentCount(); i++) {
                Rectangle bounds = P_SEL2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL2.setMinimumSize(preferredSize);
              P_SEL2.setPreferredSize(preferredSize);
            }
          }

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU2.setName("WTOU2");
          WTOU2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU2ActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_66 ----
            OBJ_66.setText("Date d\u00e9but");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(15, 10, 72, 18);

            //---- OBJ_67 ----
            OBJ_67.setText("Date fin");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(380, 10, 54, 18);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(170, 5, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(510, 5, 105, DATFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WTOU1 ----
            WTOU1.setText("S\u00e9lection compl\u00e8te");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });
            panel2.add(WTOU1);
            WTOU1.setBounds(10, 17, 141, WTOU1.getPreferredSize().height);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_61 ----
              OBJ_61.setText("@WLIBTD@");
              OBJ_61.setName("OBJ_61");
              P_SEL1.add(OBJ_61);
              OBJ_61.setBounds(175, 10, 310, OBJ_61.getPreferredSize().height);

              //---- OBJ_37 ----
              OBJ_37.setText("Type de divers");
              OBJ_37.setName("OBJ_37");
              P_SEL1.add(OBJ_37);
              OBJ_37.setBounds(15, 13, 95, 18);

              //---- WTD ----
              WTD.setComponentPopupMenu(BTD);
              WTD.setName("WTD");
              P_SEL1.add(WTD);
              WTD.setBounds(110, 8, 44, WTD.getPreferredSize().height);
            }
            panel2.add(P_SEL1);
            P_SEL1.setBounds(165, 5, 600, 43);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WTOU3 ----
            WTOU3.setText("S\u00e9lection compl\u00e8te");
            WTOU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU3.setName("WTOU3");
            panel3.add(WTOU3);
            WTOU3.setBounds(5, 17, 141, WTOU3.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(25, 8, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(71, 8, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(117, 8, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(163, 8, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(209, 8, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(255, 8, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(301, 8, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(347, 8, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(393, 8, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(439, 8, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(485, 8, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(531, 8, 34, MA12.getPreferredSize().height);
            }
            panel3.add(P_SEL0);
            P_SEL0.setBounds(160, 5, 600, 43);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 765, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 775, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 601, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- OBJ_60 ----
    OBJ_60.setTitle("");
    OBJ_60.setName("OBJ_60");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_29;
  private JXTitledSeparator OBJ_27;
  private JXTitledSeparator OBJ_31;
  private JXTitledSeparator OBJ_38;
  private JPanel P_SEL2;
  private RiZoneSortie OBJ_63;
  private RiZoneSortie OBJ_65;
  private XRiTextField WDART;
  private XRiTextField WFART;
  private JLabel OBJ_62;
  private JLabel OBJ_64;
  private XRiCheckBox WTOU2;
  private JPanel panel1;
  private JLabel OBJ_66;
  private JLabel OBJ_67;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JPanel panel2;
  private XRiCheckBox WTOU1;
  private JPanel P_SEL1;
  private RiZoneSortie OBJ_61;
  private JLabel OBJ_37;
  private XRiTextField WTD;
  private JPanel panel3;
  private XRiCheckBox WTOU3;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private JXTitledSeparator OBJ_60;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
