
package ri.serien.libecranrpg.sgvx.SGVXA26F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/*
 * Created by JFormDesigner on Tue Jul 03 17:12:57 CEST 2012
 */

/**
 * @author Stéphane Vénéri
 */
public class SGVXA26F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXA26F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WEDT02.setValeursSelection("OUI", "NON");
    WEDT01.setValeursSelection("OUI", "NON");
    WRAZ.setValeursSelection("OUI", "NON");
    WEDTF2.setValeursSelection("OUI", "NON");
    WEDTF1.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    // DEPDEB.setVisible(lexique.isPresent("DEPDEB"));
    // DEPFIN.setVisible(lexique.isPresent("DEPFIN"));
    // WCTR.setVisible(lexique.isPresent("WCTR"));
    // WPFC.setVisible(lexique.isPresent("WPFC"));
    // NBCOL.setVisible(lexique.isPresent("NBCOL"));
    // NUMPRE.setVisible(lexique.isPresent("NUMPRE"));
    // WMTHT.setVisible(lexique.isPresent("WMTHT"));
    // CLIFIN.setVisible(lexique.isPresent("CLIFIN"));
    // CLIDEB.setVisible(lexique.isPresent("CLIDEB"));
    // DATFIN.setVisible(lexique.isPresent("DATFIN"));
    // DATDEB.setVisible(lexique.isPresent("DATDEB"));
    // WZTR.setVisible(lexique.isPresent("WZTR"));
    // WEDT02.setEnabled(lexique.isPresent("WEDT02"));
    // WEDT02.setSelected(lexique.HostFieldGetData("WEDT02").equalsIgnoreCase("OUI"));
    // WEDT01.setEnabled(lexique.isPresent("WEDT01"));
    // WEDT01.setSelected(lexique.HostFieldGetData("WEDT01").equalsIgnoreCase("OUI"));
    // CLICLK.setVisible(lexique.isPresent("CLICLK"));
    // WRAZ.setEnabled(lexique.isPresent("WRAZ"));
    // WRAZ.setSelected(lexique.HostFieldGetData("WRAZ").equalsIgnoreCase("OUI"));
    // WEDTF2.setEnabled(lexique.isPresent("WEDTF2"));
    // WEDTF2.setSelected(lexique.HostFieldGetData("WEDTF2").equalsIgnoreCase("OUI"));
    // TYPNOM.setEnabled(lexique.isPresent("TYPNOM"));
    // TYPCLI.setEnabled(lexique.isPresent("TYPCLI"));
    // TYPDAT.setEnabled(lexique.isPresent("TYPDAT"));
    // WEDTF1.setEnabled(lexique.isPresent("WEDTF1"));
    // WEDTF1.setSelected(lexique.HostFieldGetData("WEDTF1").equalsIgnoreCase("OUI"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WEDT02.isSelected())
    // lexique.HostFieldPutData("WEDT02", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WEDT02", 0, "NON");
    // if (WEDT01.isSelected())
    // lexique.HostFieldPutData("WEDT01", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WEDT01", 0, "NON");
    // if (WRAZ.isSelected())
    // lexique.HostFieldPutData("WRAZ", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WRAZ", 0, "NON");
    // if (WEDTF2.isSelected())
    // lexique.HostFieldPutData("WEDTF2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WEDTF2", 0, "NON");
    // if (WEDTF1.isSelected())
    // lexique.HostFieldPutData("WEDTF1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WEDTF1", 0, "NON");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvxa26"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_22 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_23 = new JXTitledSeparator();
    WEDTF1 = new XRiCheckBox();
    TYPDAT = new XRiComboBox();
    TYPCLI = new XRiComboBox();
    TYPNOM = new XRiComboBox();
    WEDTF2 = new XRiCheckBox();
    WRAZ = new XRiCheckBox();
    CLICLK = new XRiTextField();
    WEDT01 = new XRiCheckBox();
    WEDT02 = new XRiCheckBox();
    OBJ_60 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_53 = new JLabel();
    WZTR = new XRiTextField();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    CLIDEB = new XRiTextField();
    CLIFIN = new XRiTextField();
    WMTHT = new XRiTextField();
    NUMPRE = new XRiTextField();
    NBCOL = new XRiTextField();
    WPFC = new XRiTextField();
    WCTR = new XRiTextField();
    DEPFIN = new XRiTextField();
    DEPDEB = new XRiTextField();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_61 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(980, 610));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 510));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setForeground(Color.black);
          p_contenu.setMinimumSize(new Dimension(780, 510));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_22 ----
          OBJ_22.setTitle("Crit\u00e8res s\u00e9lection nouvelle pr\u00e9paration");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_21 ----
          OBJ_21.setTitle("");
          OBJ_21.setName("OBJ_21");

          //---- OBJ_23 ----
          OBJ_23.setTitle("");
          OBJ_23.setName("OBJ_23");

          //---- WEDTF1 ----
          WEDTF1.setText("R\u00e9\u00e9dition pr\u00e9paration globale");
          WEDTF1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WEDTF1.setName("WEDTF1");

          //---- TYPDAT ----
          TYPDAT.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "P",
            "S"
          }));
          TYPDAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYPDAT.setName("TYPDAT");

          //---- TYPCLI ----
          TYPCLI.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "F",
            "L",
            "C"
          }));
          TYPCLI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYPCLI.setName("TYPCLI");

          //---- TYPNOM ----
          TYPNOM.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "F",
            "L",
            "C"
          }));
          TYPNOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYPNOM.setName("TYPNOM");

          //---- WEDTF2 ----
          WEDTF2.setText("R\u00e9\u00e9dition pr\u00e9paration d\u00e9taill\u00e9e");
          WEDTF2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WEDTF2.setName("WEDTF2");

          //---- WRAZ ----
          WRAZ.setText("RAZ traitement pr\u00e9c\u00e9dent");
          WRAZ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WRAZ.setName("WRAZ");

          //---- CLICLK ----
          CLICLK.setComponentPopupMenu(null);
          CLICLK.setName("CLICLK");

          //---- WEDT01 ----
          WEDT01.setText("Edition pr\u00e9paration globale");
          WEDT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WEDT01.setName("WEDT01");

          //---- WEDT02 ----
          WEDT02.setText("Edition pr\u00e9paration d\u00e9taill\u00e9e");
          WEDT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WEDT02.setName("WEDT02");

          //---- OBJ_60 ----
          OBJ_60.setText("Montant maxi commande");
          OBJ_60.setName("OBJ_60");

          //---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro de pr\u00e9paration");
          OBJ_49.setName("OBJ_49");

          //---- OBJ_56 ----
          OBJ_56.setText("Nombre cartons maxi");
          OBJ_56.setName("OBJ_56");

          //---- OBJ_55 ----
          OBJ_55.setText("Zone g\u00e9ographique");
          OBJ_55.setName("OBJ_55");

          //---- OBJ_51 ----
          OBJ_51.setText("Code alphab\u00e9tique");
          OBJ_51.setName("OBJ_51");

          //---- OBJ_57 ----
          OBJ_57.setText("Zone Pr\u00e9paration");
          OBJ_57.setName("OBJ_57");

          //---- OBJ_54 ----
          OBJ_54.setText("Code transporteur");
          OBJ_54.setName("OBJ_54");

          //---- OBJ_52 ----
          OBJ_52.setText("Num\u00e9ros clients");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_50 ----
          OBJ_50.setText("Livraisons du");
          OBJ_50.setName("OBJ_50");

          //---- OBJ_53 ----
          OBJ_53.setText("D\u00e9partement");
          OBJ_53.setName("OBJ_53");

          //---- WZTR ----
          WZTR.setComponentPopupMenu(BTD);
          WZTR.setName("WZTR");

          //---- DATDEB ----
          DATDEB.setComponentPopupMenu(null);
          DATDEB.setName("DATDEB");

          //---- DATFIN ----
          DATFIN.setComponentPopupMenu(null);
          DATFIN.setName("DATFIN");

          //---- CLIDEB ----
          CLIDEB.setComponentPopupMenu(BTD);
          CLIDEB.setName("CLIDEB");

          //---- CLIFIN ----
          CLIFIN.setComponentPopupMenu(BTD);
          CLIFIN.setName("CLIFIN");

          //---- WMTHT ----
          WMTHT.setComponentPopupMenu(null);
          WMTHT.setName("WMTHT");

          //---- NUMPRE ----
          NUMPRE.setComponentPopupMenu(null);
          NUMPRE.setName("NUMPRE");

          //---- NBCOL ----
          NBCOL.setComponentPopupMenu(null);
          NBCOL.setName("NBCOL");

          //---- WPFC ----
          WPFC.setComponentPopupMenu(null);
          WPFC.setName("WPFC");

          //---- WCTR ----
          WCTR.setComponentPopupMenu(BTD);
          WCTR.setName("WCTR");

          //---- DEPFIN ----
          DEPFIN.setComponentPopupMenu(BTD);
          DEPFIN.setName("DEPFIN");

          //---- DEPDEB ----
          DEPDEB.setComponentPopupMenu(BTD);
          DEPDEB.setName("DEPDEB");

          //---- OBJ_58 ----
          OBJ_58.setText("au");
          OBJ_58.setName("OBJ_58");

          //---- OBJ_59 ----
          OBJ_59.setText("au");
          OBJ_59.setName("OBJ_59");

          //---- OBJ_61 ----
          OBJ_61.setText("/");
          OBJ_61.setName("OBJ_61");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(135, 135, 135)
                    .addComponent(NUMPRE, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
                .addGap(80, 80, 80)
                .addComponent(WRAZ, GroupLayout.PREFERRED_SIZE, 234, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WEDTF1, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(WEDTF2, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53)
                .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(TYPDAT, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(CLIDEB, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(CLIFIN, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(TYPCLI, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(CLICLK, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addGap(120, 120, 120)
                .addComponent(TYPNOM, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(DEPDEB, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DEPFIN, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addComponent(WCTR, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(WZTR, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NBCOL, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(228, 228, 228)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(150, 150, 150)
                    .addComponent(WMTHT, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WEDT01, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
                .addGap(88, 88, 88)
                .addComponent(WEDT02, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(WPFC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(NUMPRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_49))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(WRAZ, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WEDTF1, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WEDTF2, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_50))
                  .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_58))
                  .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TYPDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_52))
                  .addComponent(CLIDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_59))
                  .addComponent(CLIFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TYPCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_51))
                  .addComponent(CLICLK, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TYPNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_53))
                  .addComponent(DEPDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_61))
                  .addComponent(DEPFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_54))
                  .addComponent(WCTR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_55))
                  .addComponent(WZTR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_56))
                  .addComponent(NBCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WMTHT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_60)))
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WEDT01, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WEDT02, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_57))
                  .addComponent(WPFC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_23;
  private XRiCheckBox WEDTF1;
  private XRiComboBox TYPDAT;
  private XRiComboBox TYPCLI;
  private XRiComboBox TYPNOM;
  private XRiCheckBox WEDTF2;
  private XRiCheckBox WRAZ;
  private XRiTextField CLICLK;
  private XRiCheckBox WEDT01;
  private XRiCheckBox WEDT02;
  private JLabel OBJ_60;
  private JLabel OBJ_49;
  private JLabel OBJ_56;
  private JLabel OBJ_55;
  private JLabel OBJ_51;
  private JLabel OBJ_57;
  private JLabel OBJ_54;
  private JLabel OBJ_52;
  private JLabel OBJ_50;
  private JLabel OBJ_53;
  private XRiTextField WZTR;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private XRiTextField CLIDEB;
  private XRiTextField CLIFIN;
  private XRiTextField WMTHT;
  private XRiTextField NUMPRE;
  private XRiTextField NBCOL;
  private XRiTextField WPFC;
  private XRiTextField WCTR;
  private XRiTextField DEPFIN;
  private XRiTextField DEPDEB;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JLabel OBJ_61;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
