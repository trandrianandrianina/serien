
package ri.serien.libecranrpg.sgvx.SGVX97FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX97FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX97FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WARTDT.setValeursSelection("O", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMAG@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGRP@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFAM@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSFA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    MA01.setEnabled(lexique.isPresent("MA01"));
    MA01_ck.setEnabled(lexique.isPresent("MA01"));
    MA01_ck.setSelected(lexique.HostFieldGetData("MA01").equalsIgnoreCase("**"));
    panel5.setVisible(!MA01_ck.isSelected());
    
    WGROUP.setEnabled(lexique.isPresent("WGROUP"));
    WGROUP_ck.setEnabled(lexique.isPresent("WGROUP"));
    WGROUP_ck.setSelected(lexique.HostFieldGetData("WGROUP").equalsIgnoreCase("*"));
    panel6.setVisible(!WGROUP_ck.isSelected());
    
    WFAM.setEnabled(lexique.isPresent("WFAM"));
    WFAM_ck.setEnabled(lexique.isPresent("WFAM"));
    WFAM_ck.setSelected(lexique.HostFieldGetData("WFAM").trim().equalsIgnoreCase("**"));
    panel7.setVisible(!WFAM_ck.isSelected());
    
    WSFAM.setEnabled(lexique.isPresent("WSFAM"));
    WSFAM_ck.setEnabled(lexique.isPresent("WSFAM"));
    WSFAM_ck.setSelected(lexique.HostFieldGetData("WSFAM").trim().equalsIgnoreCase("**"));
    panel8.setVisible(!WSFAM_ck.isSelected());
    
    // DATFIN.setEnabled( lexique.isPresent("DATFIN"));
    // DATDEB.setEnabled( lexique.isPresent("DATDEB"));
    // WARTDT.setEnabled( lexique.isPresent("WARTDT"));
    // WARTDT.setSelected(lexique.HostFieldGetData("WARTDT").trim().equalsIgnoreCase("O"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WARTDT.isSelected())
    // lexique.HostFieldPutData("WARTDT", 0, "O");
    // else
    // lexique.HostFieldPutData("WARTDT", 0, " ");
    if (WSFAM_ck.isSelected()) {
      lexique.HostFieldPutData("WSFAM", 0, "**");
    }
    // else
    // lexique.HostFieldPutData("WSFAM", 0, " ");
    if (WFAM_ck.isSelected()) {
      lexique.HostFieldPutData("WFAM", 0, "**");
    }
    // else
    // lexique.HostFieldPutData("WFAM", 0, " ");
    if (WGROUP_ck.isSelected()) {
      lexique.HostFieldPutData("WGROUP", 0, "*");
    }
    // else
    // lexique.HostFieldPutData("WGROUP", 0, " ");
    if (MA01_ck.isSelected()) {
      lexique.HostFieldPutData("MA01", 0, "**");
      // else
      // lexique.HostFieldPutData("MA01", 0, " ");
    }
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void MA01ActionPerformed(ActionEvent e) {
    panel5.setVisible(!panel5.isVisible());
    if (panel5.isVisible()) {
      MA01.setText("");
    }
  }
  
  private void WGROUPActionPerformed(ActionEvent e) {
    panel6.setVisible(!panel6.isVisible());
    if (panel6.isVisible()) {
      WGROUP.setText("");
    }
  }
  
  private void WFAMActionPerformed(ActionEvent e) {
    panel7.setVisible(!panel7.isVisible());
    if (panel7.isVisible()) {
      WFAM.setText("");
    }
  }
  
  private void WSFAMActionPerformed(ActionEvent e) {
    panel8.setVisible(!panel8.isVisible());
    if (panel8.isVisible()) {
      WSFAM.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_37 = new JXTitledSeparator();
    OBJ_39 = new JXTitledSeparator();
    MA01_ck = new JCheckBox();
    WGROUP_ck = new JCheckBox();
    WFAM_ck = new JCheckBox();
    WSFAM_ck = new JCheckBox();
    OBJ_56 = new JLabel();
    WARTDT = new XRiCheckBox();
    OBJ_58 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    panel5 = new JPanel();
    OBJ_44 = new JLabel();
    MA01 = new XRiTextField();
    OBJ_46 = new RiZoneSortie();
    panel6 = new JPanel();
    OBJ_47 = new JLabel();
    WGROUP = new XRiTextField();
    OBJ_49 = new RiZoneSortie();
    panel7 = new JPanel();
    OBJ_50 = new JLabel();
    WFAM = new XRiTextField();
    OBJ_52 = new RiZoneSortie();
    panel8 = new JPanel();
    OBJ_53 = new JLabel();
    WSFAM = new XRiTextField();
    OBJ_55 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_37 ----
          OBJ_37.setTitle("Codes");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_39 ----
          OBJ_39.setTitle("");
          OBJ_39.setName("OBJ_39");

          //---- MA01_ck ----
          MA01_ck.setText("S\u00e9lection compl\u00e8te");
          MA01_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MA01_ck.setName("MA01_ck");
          MA01_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              MA01ActionPerformed(e);
            }
          });

          //---- WGROUP_ck ----
          WGROUP_ck.setText("S\u00e9lection compl\u00e8te");
          WGROUP_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WGROUP_ck.setName("WGROUP_ck");
          WGROUP_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WGROUPActionPerformed(e);
            }
          });

          //---- WFAM_ck ----
          WFAM_ck.setText("S\u00e9lection compl\u00e8te");
          WFAM_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WFAM_ck.setName("WFAM_ck");
          WFAM_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WFAMActionPerformed(e);
            }
          });

          //---- WSFAM_ck ----
          WSFAM_ck.setText("S\u00e9lection compl\u00e8te");
          WSFAM_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSFAM_ck.setName("WSFAM_ck");
          WSFAM_ck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WSFAMActionPerformed(e);
            }
          });

          //---- OBJ_56 ----
          OBJ_56.setText("P\u00e9riode \u00e0 \u00e9diter du");
          OBJ_56.setName("OBJ_56");

          //---- WARTDT ----
          WARTDT.setText("D\u00e9tail par article");
          WARTDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WARTDT.setName("WARTDT");

          //---- OBJ_58 ----
          OBJ_58.setText("au");
          OBJ_58.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_58.setName("OBJ_58");

          //---- DATDEB ----
          DATDEB.setName("DATDEB");

          //---- DATFIN ----
          DATFIN.setName("DATFIN");

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- OBJ_44 ----
            OBJ_44.setText("Magasin");
            OBJ_44.setName("OBJ_44");
            panel5.add(OBJ_44);
            OBJ_44.setBounds(10, 5, 70, 20);

            //---- MA01 ----
            MA01.setComponentPopupMenu(BTD);
            MA01.setName("MA01");
            panel5.add(MA01);
            MA01.setBounds(90, 1, 34, MA01.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("@LIBMAG@");
            OBJ_46.setName("OBJ_46");
            panel5.add(OBJ_46);
            OBJ_46.setBounds(155, 5, 250, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_47 ----
            OBJ_47.setText("Groupe");
            OBJ_47.setName("OBJ_47");
            panel6.add(OBJ_47);
            OBJ_47.setBounds(10, 5, 53, 20);

            //---- WGROUP ----
            WGROUP.setComponentPopupMenu(BTD);
            WGROUP.setName("WGROUP");
            panel6.add(WGROUP);
            WGROUP.setBounds(90, 1, 24, WGROUP.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("@LIBGRP@");
            OBJ_49.setName("OBJ_49");
            panel6.add(OBJ_49);
            OBJ_49.setBounds(155, 5, 250, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- OBJ_50 ----
            OBJ_50.setText("Famille");
            OBJ_50.setName("OBJ_50");
            panel7.add(OBJ_50);
            OBJ_50.setBounds(5, 5, 47, 20);

            //---- WFAM ----
            WFAM.setComponentPopupMenu(BTD);
            WFAM.setName("WFAM");
            panel7.add(WFAM);
            WFAM.setBounds(90, 1, 44, WFAM.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("@LIBFAM@");
            OBJ_52.setName("OBJ_52");
            panel7.add(OBJ_52);
            OBJ_52.setBounds(155, 5, 250, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }

          //======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setBackground(new Color(214, 217, 223));
            panel8.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel8.setName("panel8");
            panel8.setLayout(null);

            //---- OBJ_53 ----
            OBJ_53.setText("Sous Famille");
            OBJ_53.setName("OBJ_53");
            panel8.add(OBJ_53);
            OBJ_53.setBounds(5, 5, 81, 20);

            //---- WSFAM ----
            WSFAM.setComponentPopupMenu(BTD);
            WSFAM.setName("WSFAM");
            panel8.add(WSFAM);
            WSFAM.setBounds(90, 1, 64, WSFAM.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("@LIBSFA@");
            OBJ_55.setName("OBJ_55");
            panel8.add(OBJ_55);
            OBJ_55.setBounds(155, 5, 250, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel8.getComponentCount(); i++) {
                Rectangle bounds = panel8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8.setMinimumSize(preferredSize);
              panel8.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(82, 82, 82)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                    .addGap(26, 26, 26)
                    .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(WARTDT, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(WSFAM_ck)
                      .addComponent(WFAM_ck)
                      .addComponent(WGROUP_ck)
                      .addComponent(MA01_ck))
                    .addGap(31, 31, 31)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(39, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MA01_ck, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WGROUP_ck, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WFAM_ck, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WSFAM_ck, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(WARTDT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_37;
  private JXTitledSeparator OBJ_39;
  private JCheckBox MA01_ck;
  private JCheckBox WGROUP_ck;
  private JCheckBox WFAM_ck;
  private JCheckBox WSFAM_ck;
  private JLabel OBJ_56;
  private XRiCheckBox WARTDT;
  private JLabel OBJ_58;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JPanel panel5;
  private JLabel OBJ_44;
  private XRiTextField MA01;
  private RiZoneSortie OBJ_46;
  private JPanel panel6;
  private JLabel OBJ_47;
  private XRiTextField WGROUP;
  private RiZoneSortie OBJ_49;
  private JPanel panel7;
  private JLabel OBJ_50;
  private XRiTextField WFAM;
  private RiZoneSortie OBJ_52;
  private JPanel panel8;
  private JLabel OBJ_53;
  private XRiTextField WSFAM;
  private RiZoneSortie OBJ_55;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
