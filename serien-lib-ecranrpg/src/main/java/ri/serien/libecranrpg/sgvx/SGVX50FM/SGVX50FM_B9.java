
package ri.serien.libecranrpg.sgvx.SGVX50FM;
// Nom Fichier: pop_none_15_SGVX50FM_FMTB9_290.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class SGVX50FM_B9 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVX50FM_B9(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    initDiverses();
    EDTADS.setValeursSelection("X", " ");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setTitle(interpreteurD.analyseExpression("Option supplémentaire"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    EDTADS = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(400, 128));
    setPreferredSize(new Dimension(400, 128));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(238, 238, 210));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

      //---- EDTADS ----
      EDTADS.setText("Edition des adresses de stockage");
      EDTADS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      EDTADS.setMaximumSize(new Dimension(205, 30));
      EDTADS.setMinimumSize(new Dimension(235, 30));
      EDTADS.setPreferredSize(new Dimension(235, 30));
      EDTADS.setFont(new Font("sansserif", Font.PLAIN, 14));
      EDTADS.setName("EDTADS");
      pnlContenu.add(EDTADS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private XRiCheckBox EDTADS;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
