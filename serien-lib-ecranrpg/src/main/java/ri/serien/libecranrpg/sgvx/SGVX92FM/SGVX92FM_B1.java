/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sgvx.SGVX92FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Mise à jour stock mini et idéal.
 * [GAM3294] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Mise à jour fiche stock -> Mise à jour stock mini et
 * idéal.
 */
public class SGVX92FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private boolean executerEvenement = true;
  
  // Mode de calcul de la moyenne des ventes mensuelles
  // 1 = N derniers mois avant le mois actuel
  // 2 = N mois suivants de l'année n-1
  private String[] SMMCAL_Value = { "1", "2" };
  
  /**
   * Constructeur.
   */
  public SGVX92FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    SMMCAL.setValeurs(SMMCAL_Value, null);
    SMCESS.setValeursSelection("1", "");
    
    // Configurer les boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpTitre.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    OBJ_LIBGRD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGRD@")).trim());
    OBJ_LIBGRF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGRF@")).trim());
    OBJ_LIBFAD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFAD@")).trim());
    OBJ_LIBFAF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFAF@")).trim());
    OBJ_LIBSFD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSFD@")).trim());
    OBJ_LIBSFF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSFF@")).trim());
    OBJ_LIBARD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBARD@")).trim());
    OBJ_LIBARF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBARF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    executerEvenement = false;
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Afficher le message d'erreur s'il y en a un
    if (lexique.isTrue("19")) {
      String message = lexique.HostFieldGetData("V03F");
      if (message != null && !message.isEmpty()) {
        DialogueInformation.afficher(message);
      }
    }
    
    // Afficher le message d'information lorsqu'on est en paramètrage d'une planification
    boolean modeSauvegardePlanning = lexique.isTrue("96");
    lbPlanning.setVisible(modeSauvegardePlanning);
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Renseigner le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "MAG");
    
    // Renseigner le fournisseur
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "FRDEB");
    
    // Changer le logo du bandeau en fonction de l'établissement
    bpTitre.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Sélectionner la seconde puce si une valeur est renseignée dans le délai et que le coefficient est vide
    if (lexique.HostFieldGetData("SMCOEM").trim().isEmpty() && !lexique.HostFieldGetData("SMDELS").trim().isEmpty()) {
      rbDelaiStockMini.setSelected(true);
    }
    else {
      rbCoefficientStockMini.setSelected(true);
    }
    rafraichirStockMini();
    
    executerEvenement = true;
  }
  
  private void rafraichirStockMini() {
    if (rbCoefficientStockMini.isSelected()) {
      SMCOEM.setEnabled(true);
      SMDELS.setEnabled(false);
      SMDELS.setText("");
    }
    else if (rbDelaiStockMini.isSelected()) {
      SMCOEM.setEnabled(true);
      SMCOEM.setText("");
      SMDELS.setEnabled(true);
    }
    else {
      SMCOEM.setEnabled(false);
      SMCOEM.setText("");
      SMDELS.setEnabled(false);
      SMDELS.setText("");
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "MAG");
    snFournisseur.renseignerChampRPG(lexique, "FRDEB");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!executerEvenement) {
        return;
      }
      
      // Changer le logo du bandeau en fonction de l'établissement
      bpTitre.setCodeEtablissement(snEtablissement.getCodeSelection());
      
      // Renseigner l'établissement du fournisseur
      snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
      snFournisseur.charger(false);
      
      // Effacer le magasin
      snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      snMagasin.charger(true);
      
      // Effacer le fournisseur
      snFournisseur.setSelection(null);
      
      // Effacer la plage de groupes
      EDEBG.setText("");
      OBJ_LIBGRD.setText("");
      EFING.setText("");
      OBJ_LIBGRF.setText("");
      
      // Effacer la plage de familles
      EDEB.setText("");
      OBJ_LIBFAD.setText("");
      EFIN.setText("");
      OBJ_LIBFAF.setText("");
      
      // Effacer la plage de sous-familles
      EDEBS.setText("");
      OBJ_LIBSFD.setText("");
      EFINS.setText("");
      OBJ_LIBSFF.setText("");
      
      // Effacer la plage d'articles
      EDEBA.setText("");
      OBJ_LIBARD.setText("");
      EFINA.setText("");
      OBJ_LIBARF.setText("");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void rbCoefficientStockMiniItemStateChanged(ItemEvent e) {
    try {
      rafraichirStockMini();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbDelaiStockMiniItemStateChanged(ItemEvent e) {
    try {
      rafraichirStockMini();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpTitre = new SNBandeauTitre();
    pnlContenu = new JPanel();
    lbPlanning = new JLabel();
    pnlContenu2 = new JPanel();
    pnlCalculMoyenneVente = new JPanel();
    lbModeCalcul = new SNLabelChamp();
    SMMCAL = new XRiComboBox();
    lbPeriode = new SNLabelChamp();
    pnlPeriode = new JPanel();
    SMNBM = new XRiTextField();
    lbUnitePeriode = new SNLabelChamp();
    SMCESS = new XRiCheckBox();
    pnlEtablissementMagasin = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlContenu3 = new JPanel();
    pnlCalculStockMini = new JPanel();
    sNLabel1 = new SNLabelChamp();
    panel1 = new JPanel();
    rbCoefficientStockMini = new JRadioButton();
    SMCOEM = new XRiTextField();
    panel2 = new JPanel();
    rbDelaiStockMini = new JRadioButton();
    SMDELS = new XRiTextField();
    lbUniteDelaiStockMini = new SNLabelChamp();
    pnlCalculStockIdeal = new JPanel();
    lbCoefficientStockIdeal = new SNLabelChamp();
    SMCOEI = new XRiTextField();
    pnlSelectionArticle = new JPanel();
    lbFournisseurPrincipal = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbPlageGroupe = new SNLabelChamp();
    pnlPlageGroupe = new JPanel();
    EDEBG = new XRiTextField();
    OBJ_LIBGRD = new RiZoneSortie();
    lbPlageGroupe2 = new SNLabelChamp();
    EFING = new XRiTextField();
    OBJ_LIBGRF = new RiZoneSortie();
    lbPlageFamille = new SNLabelChamp();
    pnlPlageFamille = new JPanel();
    EDEB = new XRiTextField();
    OBJ_LIBFAD = new RiZoneSortie();
    lbPlageFamille2 = new SNLabelChamp();
    EFIN = new XRiTextField();
    OBJ_LIBFAF = new RiZoneSortie();
    lbPlageSousFamille = new SNLabelChamp();
    pnlPlageSousFamille = new JPanel();
    EDEBS = new XRiTextField();
    OBJ_LIBSFD = new RiZoneSortie();
    lbPlageSousFamille2 = new SNLabelChamp();
    EFINS = new XRiTextField();
    OBJ_LIBSFF = new RiZoneSortie();
    lbPlageArticle = new SNLabelChamp();
    pnlPlageArticle = new JPanel();
    EDEBA = new XRiTextField();
    OBJ_LIBARD = new RiZoneSortie();
    EFINA = new XRiTextField();
    OBJ_LIBARF = new RiZoneSortie();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpTitre ----
    bpTitre.setText("@TITPG1@ @TITPG2@");
    bpTitre.setName("bpTitre");
    add(bpTitre, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(100, 24));
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setOpaque(false);
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {175, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

      //---- lbPlanning ----
      lbPlanning.setText("Param\u00e8trage d'une t\u00e2che planifi\u00e9e");
      lbPlanning.setFont(new Font("sansserif", Font.BOLD, 14));
      lbPlanning.setForeground(Color.red);
      lbPlanning.setMaximumSize(new Dimension(300, 30));
      lbPlanning.setMinimumSize(new Dimension(300, 30));
      lbPlanning.setPreferredSize(new Dimension(300, 30));
      lbPlanning.setName("lbPlanning");
      pnlContenu.add(lbPlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlContenu2 ========
      {
        pnlContenu2.setOpaque(false);
        pnlContenu2.setName("pnlContenu2");
        pnlContenu2.setLayout(new GridLayout(1, 2, 5, 5));

        //======== pnlCalculMoyenneVente ========
        {
          pnlCalculMoyenneVente.setOpaque(false);
          pnlCalculMoyenneVente.setBorder(new TitledBorder("Calcul de la moyenne des ventes par mois"));
          pnlCalculMoyenneVente.setName("pnlCalculMoyenneVente");
          pnlCalculMoyenneVente.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCalculMoyenneVente.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCalculMoyenneVente.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCalculMoyenneVente.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCalculMoyenneVente.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbModeCalcul ----
          lbModeCalcul.setText("Mode de calcul de la moyenne");
          lbModeCalcul.setPreferredSize(new Dimension(200, 30));
          lbModeCalcul.setMinimumSize(new Dimension(200, 30));
          lbModeCalcul.setName("lbModeCalcul");
          pnlCalculMoyenneVente.add(lbModeCalcul, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- SMMCAL ----
          SMMCAL.setComponentPopupMenu(BTD);
          SMMCAL.setMinimumSize(new Dimension(300, 30));
          SMMCAL.setMaximumSize(new Dimension(300, 30));
          SMMCAL.setPreferredSize(new Dimension(300, 30));
          SMMCAL.setModel(new DefaultComboBoxModel(new String[] {
            "N derniers mois avant le mois actuel",
            "N mois suivants de l'ann\u00e9e n-1"
          }));
          SMMCAL.setFont(new Font("sansserif", Font.PLAIN, 14));
          SMMCAL.setName("SMMCAL");
          pnlCalculMoyenneVente.add(SMMCAL, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode concern\u00e9e");
          lbPeriode.setName("lbPeriode");
          pnlCalculMoyenneVente.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlPeriode ========
          {
            pnlPeriode.setOpaque(false);
            pnlPeriode.setName("pnlPeriode");
            pnlPeriode.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlPeriode.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlPeriode.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlPeriode.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlPeriode.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- SMNBM ----
            SMNBM.setComponentPopupMenu(null);
            SMNBM.setMinimumSize(new Dimension(50, 30));
            SMNBM.setMaximumSize(new Dimension(50, 30));
            SMNBM.setPreferredSize(new Dimension(50, 30));
            SMNBM.setFont(new Font("sansserif", Font.PLAIN, 14));
            SMNBM.setName("SMNBM");
            pnlPeriode.add(SMNBM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbUnitePeriode ----
            lbUnitePeriode.setText("mois");
            lbUnitePeriode.setHorizontalAlignment(SwingConstants.LEADING);
            lbUnitePeriode.setName("lbUnitePeriode");
            pnlPeriode.add(lbUnitePeriode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCalculMoyenneVente.add(pnlPeriode, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- SMCESS ----
          SMCESS.setText("Prise en compte des cessions entre magasins");
          SMCESS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SMCESS.setFont(new Font("sansserif", Font.PLAIN, 14));
          SMCESS.setMaximumSize(new Dimension(300, 30));
          SMCESS.setMinimumSize(new Dimension(300, 30));
          SMCESS.setPreferredSize(new Dimension(300, 30));
          SMCESS.setName("SMCESS");
          pnlCalculMoyenneVente.add(SMCESS, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu2.add(pnlCalculMoyenneVente);

        //======== pnlEtablissementMagasin ========
        {
          pnlEtablissementMagasin.setOpaque(false);
          pnlEtablissementMagasin.setBorder(new TitledBorder("S\u00e9lection du magasin"));
          pnlEtablissementMagasin.setName("pnlEtablissementMagasin");
          pnlEtablissementMagasin.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissementMagasin.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissementMagasin.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissementMagasin.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissementMagasin.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissementMagasin.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlEtablissementMagasin.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlEtablissementMagasin.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasin ----
          snMagasin.setName("snMagasin");
          pnlEtablissementMagasin.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu2.add(pnlEtablissementMagasin);
      }
      pnlContenu.add(pnlContenu2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlContenu3 ========
      {
        pnlContenu3.setOpaque(false);
        pnlContenu3.setName("pnlContenu3");
        pnlContenu3.setLayout(new GridLayout(1, 2, 5, 5));

        //======== pnlCalculStockMini ========
        {
          pnlCalculStockMini.setOpaque(false);
          pnlCalculStockMini.setBorder(new TitledBorder("Calcul du stock mini"));
          pnlCalculStockMini.setName("pnlCalculStockMini");
          pnlCalculStockMini.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCalculStockMini.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCalculStockMini.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCalculStockMini.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCalculStockMini.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- sNLabel1 ----
          sNLabel1.setText("Calcul du stock mini");
          sNLabel1.setMaximumSize(new Dimension(200, 30));
          sNLabel1.setMinimumSize(new Dimension(200, 30));
          sNLabel1.setPreferredSize(new Dimension(200, 30));
          sNLabel1.setName("sNLabel1");
          pnlCalculStockMini.add(sNLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(new GridBagLayout());
            ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- rbCoefficientStockMini ----
            rbCoefficientStockMini.setText("Coefficient multiplicateur");
            rbCoefficientStockMini.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbCoefficientStockMini.setMaximumSize(new Dimension(176, 30));
            rbCoefficientStockMini.setMinimumSize(new Dimension(176, 30));
            rbCoefficientStockMini.setPreferredSize(new Dimension(176, 30));
            rbCoefficientStockMini.setName("rbCoefficientStockMini");
            rbCoefficientStockMini.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                rbCoefficientStockMiniItemStateChanged(e);
              }
            });
            panel1.add(rbCoefficientStockMini, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- SMCOEM ----
            SMCOEM.setComponentPopupMenu(null);
            SMCOEM.setMinimumSize(new Dimension(50, 30));
            SMCOEM.setMaximumSize(new Dimension(50, 30));
            SMCOEM.setPreferredSize(new Dimension(50, 30));
            SMCOEM.setFont(new Font("sansserif", Font.PLAIN, 14));
            SMCOEM.setName("SMCOEM");
            panel1.add(SMCOEM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCalculStockMini.add(panel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(new GridBagLayout());
            ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- rbDelaiStockMini ----
            rbDelaiStockMini.setText("D\u00e9lai");
            rbDelaiStockMini.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbDelaiStockMini.setName("rbDelaiStockMini");
            rbDelaiStockMini.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                rbDelaiStockMiniItemStateChanged(e);
              }
            });
            panel2.add(rbDelaiStockMini, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- SMDELS ----
            SMDELS.setComponentPopupMenu(null);
            SMDELS.setMinimumSize(new Dimension(50, 30));
            SMDELS.setMaximumSize(new Dimension(50, 30));
            SMDELS.setPreferredSize(new Dimension(50, 30));
            SMDELS.setFont(new Font("sansserif", Font.PLAIN, 14));
            SMDELS.setName("SMDELS");
            panel2.add(SMDELS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbUniteDelaiStockMini ----
            lbUniteDelaiStockMini.setText("semaine(s)");
            lbUniteDelaiStockMini.setHorizontalAlignment(SwingConstants.LEADING);
            lbUniteDelaiStockMini.setName("lbUniteDelaiStockMini");
            panel2.add(lbUniteDelaiStockMini, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCalculStockMini.add(panel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu3.add(pnlCalculStockMini);

        //======== pnlCalculStockIdeal ========
        {
          pnlCalculStockIdeal.setOpaque(false);
          pnlCalculStockIdeal.setBorder(new TitledBorder("Calcul du stock id\u00e9al"));
          pnlCalculStockIdeal.setName("pnlCalculStockIdeal");
          pnlCalculStockIdeal.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCalculStockIdeal.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCalculStockIdeal.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlCalculStockIdeal.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCalculStockIdeal.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbCoefficientStockIdeal ----
          lbCoefficientStockIdeal.setText("Coefficient multiplicateur");
          lbCoefficientStockIdeal.setPreferredSize(new Dimension(200, 30));
          lbCoefficientStockIdeal.setMinimumSize(new Dimension(200, 30));
          lbCoefficientStockIdeal.setMaximumSize(new Dimension(200, 30));
          lbCoefficientStockIdeal.setName("lbCoefficientStockIdeal");
          pnlCalculStockIdeal.add(lbCoefficientStockIdeal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- SMCOEI ----
          SMCOEI.setComponentPopupMenu(BTD);
          SMCOEI.setMinimumSize(new Dimension(50, 30));
          SMCOEI.setMaximumSize(new Dimension(50, 30));
          SMCOEI.setPreferredSize(new Dimension(50, 30));
          SMCOEI.setFont(new Font("sansserif", Font.PLAIN, 14));
          SMCOEI.setName("SMCOEI");
          pnlCalculStockIdeal.add(SMCOEI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu3.add(pnlCalculStockIdeal);
      }
      pnlContenu.add(pnlContenu3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlSelectionArticle ========
      {
        pnlSelectionArticle.setOpaque(false);
        pnlSelectionArticle.setBorder(new TitledBorder("S\u00e9lection des articles \u00e0 mettre \u00e0 jour"));
        pnlSelectionArticle.setName("pnlSelectionArticle");
        pnlSelectionArticle.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlSelectionArticle.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlSelectionArticle.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlSelectionArticle.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlSelectionArticle.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbFournisseurPrincipal ----
        lbFournisseurPrincipal.setText("Par le fournisseur principal");
        lbFournisseurPrincipal.setMaximumSize(new Dimension(200, 30));
        lbFournisseurPrincipal.setMinimumSize(new Dimension(200, 30));
        lbFournisseurPrincipal.setPreferredSize(new Dimension(200, 30));
        lbFournisseurPrincipal.setName("lbFournisseurPrincipal");
        pnlSelectionArticle.add(lbFournisseurPrincipal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snFournisseur ----
        snFournisseur.setName("snFournisseur");
        pnlSelectionArticle.add(snFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPlageGroupe ----
        lbPlageGroupe.setText("Par plage de groupes");
        lbPlageGroupe.setMaximumSize(new Dimension(200, 30));
        lbPlageGroupe.setMinimumSize(new Dimension(200, 30));
        lbPlageGroupe.setPreferredSize(new Dimension(200, 30));
        lbPlageGroupe.setName("lbPlageGroupe");
        pnlSelectionArticle.add(lbPlageGroupe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== pnlPlageGroupe ========
        {
          pnlPlageGroupe.setOpaque(false);
          pnlPlageGroupe.setName("pnlPlageGroupe");
          pnlPlageGroupe.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageGroupe.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlPlageGroupe.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPlageGroupe.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlPlageGroupe.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- EDEBG ----
          EDEBG.setComponentPopupMenu(BTD);
          EDEBG.setMinimumSize(new Dimension(50, 30));
          EDEBG.setMaximumSize(new Dimension(50, 30));
          EDEBG.setPreferredSize(new Dimension(50, 30));
          EDEBG.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDEBG.setName("EDEBG");
          pnlPlageGroupe.add(EDEBG, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- OBJ_LIBGRD ----
          OBJ_LIBGRD.setText("@LIBGRD@");
          OBJ_LIBGRD.setPreferredSize(new Dimension(300, 30));
          OBJ_LIBGRD.setMinimumSize(new Dimension(300, 30));
          OBJ_LIBGRD.setMaximumSize(new Dimension(300, 30));
          OBJ_LIBGRD.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBGRD.setName("OBJ_LIBGRD");
          pnlPlageGroupe.add(OBJ_LIBGRD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbPlageGroupe2 ----
          lbPlageGroupe2.setText("\u00e0");
          lbPlageGroupe2.setPreferredSize(new Dimension(30, 30));
          lbPlageGroupe2.setMinimumSize(new Dimension(30, 30));
          lbPlageGroupe2.setMaximumSize(new Dimension(30, 30));
          lbPlageGroupe2.setHorizontalAlignment(SwingConstants.CENTER);
          lbPlageGroupe2.setName("lbPlageGroupe2");
          pnlPlageGroupe.add(lbPlageGroupe2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- EFING ----
          EFING.setComponentPopupMenu(BTD);
          EFING.setMinimumSize(new Dimension(50, 30));
          EFING.setMaximumSize(new Dimension(50, 30));
          EFING.setPreferredSize(new Dimension(50, 30));
          EFING.setFont(new Font("sansserif", Font.PLAIN, 14));
          EFING.setName("EFING");
          pnlPlageGroupe.add(EFING, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- OBJ_LIBGRF ----
          OBJ_LIBGRF.setText("@LIBGRF@");
          OBJ_LIBGRF.setPreferredSize(new Dimension(300, 30));
          OBJ_LIBGRF.setMinimumSize(new Dimension(300, 30));
          OBJ_LIBGRF.setMaximumSize(new Dimension(300, 30));
          OBJ_LIBGRF.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBGRF.setInheritsPopupMenu(false);
          OBJ_LIBGRF.setName("OBJ_LIBGRF");
          pnlPlageGroupe.add(OBJ_LIBGRF, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSelectionArticle.add(pnlPlageGroupe, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPlageFamille ----
        lbPlageFamille.setText("Par plage de familles");
        lbPlageFamille.setMinimumSize(new Dimension(200, 30));
        lbPlageFamille.setMaximumSize(new Dimension(200, 30));
        lbPlageFamille.setPreferredSize(new Dimension(200, 30));
        lbPlageFamille.setName("lbPlageFamille");
        pnlSelectionArticle.add(lbPlageFamille, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== pnlPlageFamille ========
        {
          pnlPlageFamille.setOpaque(false);
          pnlPlageFamille.setName("pnlPlageFamille");
          pnlPlageFamille.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageFamille.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlPlageFamille.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPlageFamille.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlPlageFamille.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- EDEB ----
          EDEB.setComponentPopupMenu(BTD);
          EDEB.setMinimumSize(new Dimension(50, 30));
          EDEB.setMaximumSize(new Dimension(50, 30));
          EDEB.setPreferredSize(new Dimension(50, 30));
          EDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDEB.setName("EDEB");
          pnlPlageFamille.add(EDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- OBJ_LIBFAD ----
          OBJ_LIBFAD.setText("@LIBFAD@");
          OBJ_LIBFAD.setPreferredSize(new Dimension(300, 30));
          OBJ_LIBFAD.setMinimumSize(new Dimension(300, 30));
          OBJ_LIBFAD.setMaximumSize(new Dimension(300, 30));
          OBJ_LIBFAD.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBFAD.setName("OBJ_LIBFAD");
          pnlPlageFamille.add(OBJ_LIBFAD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbPlageFamille2 ----
          lbPlageFamille2.setText("\u00e0");
          lbPlageFamille2.setMaximumSize(new Dimension(30, 30));
          lbPlageFamille2.setMinimumSize(new Dimension(30, 30));
          lbPlageFamille2.setPreferredSize(new Dimension(30, 30));
          lbPlageFamille2.setHorizontalAlignment(SwingConstants.CENTER);
          lbPlageFamille2.setName("lbPlageFamille2");
          pnlPlageFamille.add(lbPlageFamille2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- EFIN ----
          EFIN.setComponentPopupMenu(BTD);
          EFIN.setMinimumSize(new Dimension(50, 30));
          EFIN.setMaximumSize(new Dimension(50, 30));
          EFIN.setPreferredSize(new Dimension(50, 30));
          EFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
          EFIN.setName("EFIN");
          pnlPlageFamille.add(EFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- OBJ_LIBFAF ----
          OBJ_LIBFAF.setText("@LIBFAF@");
          OBJ_LIBFAF.setPreferredSize(new Dimension(300, 30));
          OBJ_LIBFAF.setMinimumSize(new Dimension(300, 30));
          OBJ_LIBFAF.setMaximumSize(new Dimension(300, 30));
          OBJ_LIBFAF.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBFAF.setName("OBJ_LIBFAF");
          pnlPlageFamille.add(OBJ_LIBFAF, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSelectionArticle.add(pnlPlageFamille, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPlageSousFamille ----
        lbPlageSousFamille.setText("Par plage de sous-familles");
        lbPlageSousFamille.setMaximumSize(new Dimension(200, 30));
        lbPlageSousFamille.setMinimumSize(new Dimension(200, 30));
        lbPlageSousFamille.setPreferredSize(new Dimension(200, 30));
        lbPlageSousFamille.setName("lbPlageSousFamille");
        pnlSelectionArticle.add(lbPlageSousFamille, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== pnlPlageSousFamille ========
        {
          pnlPlageSousFamille.setOpaque(false);
          pnlPlageSousFamille.setName("pnlPlageSousFamille");
          pnlPlageSousFamille.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageSousFamille.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlPlageSousFamille.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPlageSousFamille.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlPlageSousFamille.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- EDEBS ----
          EDEBS.setMinimumSize(new Dimension(50, 30));
          EDEBS.setMaximumSize(new Dimension(50, 30));
          EDEBS.setPreferredSize(new Dimension(50, 30));
          EDEBS.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDEBS.setComponentPopupMenu(BTD);
          EDEBS.setName("EDEBS");
          pnlPlageSousFamille.add(EDEBS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- OBJ_LIBSFD ----
          OBJ_LIBSFD.setText("@LIBSFD@");
          OBJ_LIBSFD.setPreferredSize(new Dimension(300, 30));
          OBJ_LIBSFD.setMinimumSize(new Dimension(300, 30));
          OBJ_LIBSFD.setMaximumSize(new Dimension(300, 30));
          OBJ_LIBSFD.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBSFD.setName("OBJ_LIBSFD");
          pnlPlageSousFamille.add(OBJ_LIBSFD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbPlageSousFamille2 ----
          lbPlageSousFamille2.setText("\u00e0");
          lbPlageSousFamille2.setMaximumSize(new Dimension(30, 30));
          lbPlageSousFamille2.setMinimumSize(new Dimension(30, 30));
          lbPlageSousFamille2.setPreferredSize(new Dimension(30, 30));
          lbPlageSousFamille2.setHorizontalAlignment(SwingConstants.CENTER);
          lbPlageSousFamille2.setName("lbPlageSousFamille2");
          pnlPlageSousFamille.add(lbPlageSousFamille2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- EFINS ----
          EFINS.setComponentPopupMenu(BTD);
          EFINS.setMinimumSize(new Dimension(50, 30));
          EFINS.setMaximumSize(new Dimension(50, 30));
          EFINS.setPreferredSize(new Dimension(50, 30));
          EFINS.setFont(new Font("sansserif", Font.PLAIN, 14));
          EFINS.setName("EFINS");
          pnlPlageSousFamille.add(EFINS, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- OBJ_LIBSFF ----
          OBJ_LIBSFF.setText("@LIBSFF@");
          OBJ_LIBSFF.setPreferredSize(new Dimension(300, 30));
          OBJ_LIBSFF.setMinimumSize(new Dimension(300, 30));
          OBJ_LIBSFF.setMaximumSize(new Dimension(300, 30));
          OBJ_LIBSFF.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBSFF.setName("OBJ_LIBSFF");
          pnlPlageSousFamille.add(OBJ_LIBSFF, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSelectionArticle.add(pnlPlageSousFamille, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPlageArticle ----
        lbPlageArticle.setText("Par plage d'articles");
        lbPlageArticle.setMaximumSize(new Dimension(200, 30));
        lbPlageArticle.setMinimumSize(new Dimension(200, 30));
        lbPlageArticle.setPreferredSize(new Dimension(200, 30));
        lbPlageArticle.setName("lbPlageArticle");
        pnlSelectionArticle.add(lbPlageArticle, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlPlageArticle ========
        {
          pnlPlageArticle.setOpaque(false);
          pnlPlageArticle.setName("pnlPlageArticle");
          pnlPlageArticle.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPlageArticle.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageArticle.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPlageArticle.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlPlageArticle.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- EDEBA ----
          EDEBA.setComponentPopupMenu(BTD);
          EDEBA.setMinimumSize(new Dimension(200, 30));
          EDEBA.setMaximumSize(new Dimension(200, 30));
          EDEBA.setPreferredSize(new Dimension(200, 30));
          EDEBA.setName("EDEBA");
          pnlPlageArticle.add(EDEBA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_LIBARD ----
          OBJ_LIBARD.setText("@LIBARD@");
          OBJ_LIBARD.setPreferredSize(new Dimension(400, 30));
          OBJ_LIBARD.setMinimumSize(new Dimension(400, 30));
          OBJ_LIBARD.setMaximumSize(new Dimension(400, 30));
          OBJ_LIBARD.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBARD.setName("OBJ_LIBARD");
          pnlPlageArticle.add(OBJ_LIBARD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- EFINA ----
          EFINA.setComponentPopupMenu(BTD);
          EFINA.setMinimumSize(new Dimension(200, 30));
          EFINA.setMaximumSize(new Dimension(200, 30));
          EFINA.setPreferredSize(new Dimension(200, 30));
          EFINA.setName("EFINA");
          pnlPlageArticle.add(EFINA, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- OBJ_LIBARF ----
          OBJ_LIBARF.setText("@LIBARF@");
          OBJ_LIBARF.setPreferredSize(new Dimension(400, 30));
          OBJ_LIBARF.setMinimumSize(new Dimension(400, 30));
          OBJ_LIBARF.setMaximumSize(new Dimension(400, 30));
          OBJ_LIBARF.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_LIBARF.setName("OBJ_LIBARF");
          pnlPlageArticle.add(OBJ_LIBARF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSelectionArticle.add(pnlPlageArticle, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSelectionArticle, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- bgStockMini ----
    ButtonGroup bgStockMini = new ButtonGroup();
    bgStockMini.add(rbCoefficientStockMini);
    bgStockMini.add(rbDelaiStockMini);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpTitre;
  private JPanel pnlContenu;
  private JLabel lbPlanning;
  private JPanel pnlContenu2;
  private JPanel pnlCalculMoyenneVente;
  private SNLabelChamp lbModeCalcul;
  private XRiComboBox SMMCAL;
  private SNLabelChamp lbPeriode;
  private JPanel pnlPeriode;
  private XRiTextField SMNBM;
  private SNLabelChamp lbUnitePeriode;
  private XRiCheckBox SMCESS;
  private JPanel pnlEtablissementMagasin;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private JPanel pnlContenu3;
  private JPanel pnlCalculStockMini;
  private SNLabelChamp sNLabel1;
  private JPanel panel1;
  private JRadioButton rbCoefficientStockMini;
  private XRiTextField SMCOEM;
  private JPanel panel2;
  private JRadioButton rbDelaiStockMini;
  private XRiTextField SMDELS;
  private SNLabelChamp lbUniteDelaiStockMini;
  private JPanel pnlCalculStockIdeal;
  private SNLabelChamp lbCoefficientStockIdeal;
  private XRiTextField SMCOEI;
  private JPanel pnlSelectionArticle;
  private SNLabelChamp lbFournisseurPrincipal;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbPlageGroupe;
  private JPanel pnlPlageGroupe;
  private XRiTextField EDEBG;
  private RiZoneSortie OBJ_LIBGRD;
  private SNLabelChamp lbPlageGroupe2;
  private XRiTextField EFING;
  private RiZoneSortie OBJ_LIBGRF;
  private SNLabelChamp lbPlageFamille;
  private JPanel pnlPlageFamille;
  private XRiTextField EDEB;
  private RiZoneSortie OBJ_LIBFAD;
  private SNLabelChamp lbPlageFamille2;
  private XRiTextField EFIN;
  private RiZoneSortie OBJ_LIBFAF;
  private SNLabelChamp lbPlageSousFamille;
  private JPanel pnlPlageSousFamille;
  private XRiTextField EDEBS;
  private RiZoneSortie OBJ_LIBSFD;
  private SNLabelChamp lbPlageSousFamille2;
  private XRiTextField EFINS;
  private RiZoneSortie OBJ_LIBSFF;
  private SNLabelChamp lbPlageArticle;
  private JPanel pnlPlageArticle;
  private XRiTextField EDEBA;
  private RiZoneSortie OBJ_LIBARD;
  private XRiTextField EFINA;
  private RiZoneSortie OBJ_LIBARF;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
