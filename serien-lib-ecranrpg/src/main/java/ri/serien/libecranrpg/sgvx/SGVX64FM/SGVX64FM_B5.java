
package ri.serien.libecranrpg.sgvx.SGVX64FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX64FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX64FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    UCH2.setValeurs("X");
    UCH1.setValeurs("X");
    NWABC.setValeursSelection("1", " ");
    REPON3.setValeursSelection("OUI", "NON");
    REPON4.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    WTOU.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
    REPON5.setValeursSelection("O", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_58.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    OBJ_42.setVisible(lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    P_SEL0.setVisible((!WTOUM.isSelected()) & (lexique.isTrue("N91")));
    
    WTOU.setEnabled(false);
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    P_SEL4.setVisible(!WTOU.isSelected() && lexique.isTrue("94"));
    P_SEL3.setVisible(!WTOU.isSelected() && (!lexique.isTrue("93") && !lexique.isTrue("94") && !lexique.isTrue("95")));
    P_SEL1.setVisible(!WTOU.isSelected() && lexique.isTrue("93"));
    P_SEL2.setVisible(!WTOU.isSelected() && lexique.isTrue("95"));
    
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    
    // REPON5.setSelected(lexique.HostFieldGetData("REPON5").equalsIgnoreCase("O"));
    OBJ_84.setVisible(NBSEL.isVisible());
    OBJ_84.setEnabled(lexique.isPresent("NBSEL"));
    OBJ_86.setVisible(NBSEL.isVisible());
    OBJ_86.setEnabled(lexique.isPresent("NBSEL"));
    
    // REPON3.setSelected(lexique.HostFieldGetData("REPON3").equalsIgnoreCase("OUI"));
    // REPON4.setSelected(lexique.HostFieldGetData("REPON4").equalsIgnoreCase("OUI"));
    // UCH1.setSelected(lexique.HostFieldGetData("UCH1").equalsIgnoreCase("X"));
    // UCH2.setSelected(lexique.HostFieldGetData("UCH2").equalsIgnoreCase("X"));
    // NWABC.setSelected(lexique.HostFieldGetData("NWABC").equalsIgnoreCase("1"));
    
    if (lexique.isTrue("93")) {
      OBJ_58.setTitle("Plage de sous-famille");
    }
    if (lexique.isTrue("95")) {
      OBJ_58.setTitle("Plage de Fournisseurs");
    }
    if (lexique.isTrue("94")) {
      OBJ_58.setTitle("Plage de famille");
    }
    if (!lexique.isTrue("93") && !lexique.isTrue("94") && !lexique.isTrue("95")) {
      OBJ_58.setTitle("Plage Article");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (NWABC.isSelected())
    // lexique.HostFieldPutData("NWABC", 0, "1");
    // else
    // lexique.HostFieldPutData("NWABC", 0, " ");
    // if (UCH1.isSelected())
    // lexique.HostFieldPutData("UCH1", 0, "X");
    // else
    // lexique.HostFieldPutData("UCH1", 0, " ");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (UCH2.isSelected())
    // lexique.HostFieldPutData("UCH2", 0, "X");
    // else
    // lexique.HostFieldPutData("UCH2", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (REPON5.isSelected())
    // lexique.HostFieldPutData("REPON5", 0, "O");
    // else
    // lexique.HostFieldPutData("REPON5", 0, " ");
    // if (REPON4.isSelected())
    // lexique.HostFieldPutData("REPON4", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON4", 0, "NON");
    // if (REPON3.isSelected())
    // lexique.HostFieldPutData("REPON3", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON3", 0, "NON");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL4.setVisible(!P_SEL4.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_58 = new JXTitledSeparator();
    OBJ_42 = new JXTitledSeparator();
    OBJ_87 = new JXTitledSeparator();
    P_SEL4 = new JPanel();
    OBJ_61 = new JLabel();
    OBJ_76 = new JLabel();
    EDEB = new XRiTextField();
    EFIN = new XRiTextField();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    REPON5 = new XRiCheckBox();
    WTOUM = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    OBJ_84 = new JLabel();
    NBSEL = new XRiTextField();
    OBJ_86 = new JLabel();
    panel1 = new JPanel();
    OBJ_88 = new JLabel();
    OBJ_89 = new JLabel();
    REPON4 = new XRiCheckBox();
    REPON3 = new XRiCheckBox();
    OBJ_91 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    panel2 = new JPanel();
    OBJ_95 = new JXTitledSeparator();
    UCH1 = new XRiRadioButton();
    UCH2 = new XRiRadioButton();
    panel3 = new JPanel();
    OBJ_37 = new JXTitledSeparator();
    OBJ_100 = new JLabel();
    WABC = new XRiTextField();
    NWABC = new XRiCheckBox();
    P_SEL1 = new JPanel();
    ESFD = new XRiTextField();
    ESFF = new XRiTextField();
    OBJ_64 = new JLabel();
    OBJ_78 = new JLabel();
    P_SEL3 = new JPanel();
    EDEBA = new XRiTextField();
    EFINA = new XRiTextField();
    OBJ_62 = new JLabel();
    OBJ_75 = new JLabel();
    P_SEL2 = new JPanel();
    FRSDEB = new XRiTextField();
    FRSFIN = new XRiTextField();
    OBJ_63 = new JLabel();
    OBJ_77 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(630, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 560, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //---- OBJ_58 ----
          OBJ_58.setTitle("@PLAG@");
          OBJ_58.setName("OBJ_58");
          p_contenu.add(OBJ_58);
          OBJ_58.setBounds(35, 200, 560, OBJ_58.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setTitle("Code(s) magasin(s) \u00e0 traiter");
          OBJ_42.setName("OBJ_42");
          p_contenu.add(OBJ_42);
          OBJ_42.setBounds(35, 120, 560, OBJ_42.getPreferredSize().height);

          //---- OBJ_87 ----
          OBJ_87.setTitle("");
          OBJ_87.setName("OBJ_87");
          p_contenu.add(OBJ_87);
          OBJ_87.setBounds(35, 335, 560, OBJ_87.getPreferredSize().height);

          //======== P_SEL4 ========
          {
            P_SEL4.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL4.setOpaque(false);
            P_SEL4.setName("P_SEL4");
            P_SEL4.setLayout(null);

            //---- OBJ_61 ----
            OBJ_61.setText("Code famille d\u00e9but");
            OBJ_61.setName("OBJ_61");
            P_SEL4.add(OBJ_61);
            OBJ_61.setBounds(15, 9, 111, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Code famille fin");
            OBJ_76.setName("OBJ_76");
            P_SEL4.add(OBJ_76);
            OBJ_76.setBounds(15, 39, 111, 20);

            //---- EDEB ----
            EDEB.setName("EDEB");
            P_SEL4.add(EDEB);
            EDEB.setBounds(155, 5, 44, EDEB.getPreferredSize().height);

            //---- EFIN ----
            EFIN.setName("EFIN");
            P_SEL4.add(EFIN);
            EFIN.setBounds(155, 35, 44, EFIN.getPreferredSize().height);
          }
          p_contenu.add(P_SEL4);
          P_SEL4.setBounds(205, 225, 372, 69);

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- MA01 ----
            MA01.setName("MA01");
            P_SEL0.add(MA01);
            MA01.setBounds(5, 9, 34, MA01.getPreferredSize().height);

            //---- MA02 ----
            MA02.setName("MA02");
            P_SEL0.add(MA02);
            MA02.setBounds(35, 9, 34, MA02.getPreferredSize().height);

            //---- MA03 ----
            MA03.setName("MA03");
            P_SEL0.add(MA03);
            MA03.setBounds(65, 9, 34, MA03.getPreferredSize().height);

            //---- MA04 ----
            MA04.setName("MA04");
            P_SEL0.add(MA04);
            MA04.setBounds(95, 9, 34, MA04.getPreferredSize().height);

            //---- MA05 ----
            MA05.setName("MA05");
            P_SEL0.add(MA05);
            MA05.setBounds(125, 9, 34, MA05.getPreferredSize().height);

            //---- MA06 ----
            MA06.setName("MA06");
            P_SEL0.add(MA06);
            MA06.setBounds(155, 9, 34, MA06.getPreferredSize().height);

            //---- MA07 ----
            MA07.setName("MA07");
            P_SEL0.add(MA07);
            MA07.setBounds(185, 9, 34, MA07.getPreferredSize().height);

            //---- MA08 ----
            MA08.setName("MA08");
            P_SEL0.add(MA08);
            MA08.setBounds(215, 9, 34, MA08.getPreferredSize().height);

            //---- MA09 ----
            MA09.setName("MA09");
            P_SEL0.add(MA09);
            MA09.setBounds(245, 9, 34, MA09.getPreferredSize().height);

            //---- MA10 ----
            MA10.setName("MA10");
            P_SEL0.add(MA10);
            MA10.setBounds(275, 9, 34, MA10.getPreferredSize().height);

            //---- MA11 ----
            MA11.setName("MA11");
            P_SEL0.add(MA11);
            MA11.setBounds(305, 9, 34, MA11.getPreferredSize().height);

            //---- MA12 ----
            MA12.setName("MA12");
            P_SEL0.add(MA12);
            MA12.setBounds(335, 9, 34, MA12.getPreferredSize().height);
          }
          p_contenu.add(P_SEL0);
          P_SEL0.setBounds(205, 145, 372, 45);

          //---- REPON5 ----
          REPON5.setText("Tri valeur d\u00e9croissante");
          REPON5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON5.setName("REPON5");
          p_contenu.add(REPON5);
          REPON5.setBounds(50, 305, 172, 20);

          //---- WTOUM ----
          WTOUM.setText("S\u00e9lection compl\u00e8te");
          WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUM.setName("WTOUM");
          WTOUM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUMActionPerformed(e);
            }
          });
          p_contenu.add(WTOUM);
          WTOUM.setBounds(50, 150, 141, 20);

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });
          p_contenu.add(WTOU);
          WTOU.setBounds(50, 250, 141, 20);

          //---- REPON1 ----
          REPON1.setText("Totalisation");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");
          p_contenu.add(REPON1);
          REPON1.setBounds(50, 277, 117, 20);

          //---- OBJ_84 ----
          OBJ_84.setText("(nombre article");
          OBJ_84.setName("OBJ_84");
          p_contenu.add(OBJ_84);
          OBJ_84.setBounds(250, 305, 92, 20);

          //---- NBSEL ----
          NBSEL.setComponentPopupMenu(BTD);
          NBSEL.setName("NBSEL");
          p_contenu.add(NBSEL);
          NBSEL.setBounds(360, 300, 34, NBSEL.getPreferredSize().height);

          //---- OBJ_86 ----
          OBJ_86.setText(")");
          OBJ_86.setName("OBJ_86");
          p_contenu.add(OBJ_86);
          OBJ_86.setBounds(400, 305, 6, 20);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_88 ----
            OBJ_88.setText("P\u00e9riode \u00e0 \u00e9diter");
            OBJ_88.setName("OBJ_88");
            panel1.add(OBJ_88);
            OBJ_88.setBounds(20, 10, 108, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("du");
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(170, 10, 25, 20);

            //---- REPON4 ----
            REPON4.setText("Edition articles d\u00e9sactiv\u00e9s");
            REPON4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON4.setName("REPON4");
            panel1.add(REPON4);
            REPON4.setBounds(325, 40, 210, 20);

            //---- REPON3 ----
            REPON3.setText("Edition articles non g\u00e9r\u00e9s / P\u00e9riode");
            REPON3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON3.setName("REPON3");
            panel1.add(REPON3);
            REPON3.setBounds(15, 40, 235, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("au");
            OBJ_91.setName("OBJ_91");
            panel1.add(OBJ_91);
            OBJ_91.setBounds(370, 10, 25, 20);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(215, 6, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(425, 6, 105, DATFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(35, 340, 595, 70);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_95 ----
            OBJ_95.setTitle("Chiffrage");
            OBJ_95.setName("OBJ_95");
            panel2.add(OBJ_95);
            OBJ_95.setBounds(5, 10, 285, OBJ_95.getPreferredSize().height);

            //---- UCH1 ----
            UCH1.setText("Au P.U.M.P");
            UCH1.setName("UCH1");
            panel2.add(UCH1);
            UCH1.setBounds(20, 30, 94, UCH1.getPreferredSize().height);

            //---- UCH2 ----
            UCH2.setText("Au prix de revient");
            UCH2.setName("UCH2");
            panel2.add(UCH2);
            UCH2.setBounds(160, 30, 120, UCH2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(30, 405, 300, 60);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_37 ----
            OBJ_37.setTitle("");
            OBJ_37.setName("OBJ_37");
            panel3.add(OBJ_37);
            OBJ_37.setBounds(5, 16, 245, OBJ_37.getPreferredSize().height);

            //---- OBJ_100 ----
            OBJ_100.setText("Code A,B,C,D");
            OBJ_100.setName("OBJ_100");
            panel3.add(OBJ_100);
            OBJ_100.setBounds(20, 30, 85, 20);

            //---- WABC ----
            WABC.setComponentPopupMenu(BTD);
            WABC.setName("WABC");
            panel3.add(WABC);
            WABC.setBounds(110, 26, 24, WABC.getPreferredSize().height);

            //---- NWABC ----
            NWABC.setText("Exclusion");
            NWABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NWABC.setName("NWABC");
            panel3.add(NWABC);
            NWABC.setBounds(150, 30, 90, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(345, 405, 260, 60);

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- ESFD ----
            ESFD.setComponentPopupMenu(BTD);
            ESFD.setName("ESFD");
            P_SEL1.add(ESFD);
            ESFD.setBounds(155, 5, 64, ESFD.getPreferredSize().height);

            //---- ESFF ----
            ESFF.setComponentPopupMenu(BTD);
            ESFF.setName("ESFF");
            P_SEL1.add(ESFF);
            ESFF.setBounds(155, 35, 64, ESFF.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("Code sous-famille d\u00e9but");
            OBJ_64.setName("OBJ_64");
            P_SEL1.add(OBJ_64);
            OBJ_64.setBounds(15, 9, 145, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("Code sous-famille fin");
            OBJ_78.setName("OBJ_78");
            P_SEL1.add(OBJ_78);
            OBJ_78.setBounds(15, 39, 145, 20);
          }
          p_contenu.add(P_SEL1);
          P_SEL1.setBounds(205, 225, 372, 69);

          //======== P_SEL3 ========
          {
            P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL3.setOpaque(false);
            P_SEL3.setName("P_SEL3");
            P_SEL3.setLayout(null);

            //---- EDEBA ----
            EDEBA.setName("EDEBA");
            P_SEL3.add(EDEBA);
            EDEBA.setBounds(155, 5, 210, EDEBA.getPreferredSize().height);

            //---- EFINA ----
            EFINA.setName("EFINA");
            P_SEL3.add(EFINA);
            EFINA.setBounds(155, 35, 210, EFINA.getPreferredSize().height);

            //---- OBJ_62 ----
            OBJ_62.setText("D\u00e9but");
            OBJ_62.setName("OBJ_62");
            P_SEL3.add(OBJ_62);
            OBJ_62.setBounds(15, 9, 55, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("Fin");
            OBJ_75.setName("OBJ_75");
            P_SEL3.add(OBJ_75);
            OBJ_75.setBounds(15, 39, 55, 20);
          }
          p_contenu.add(P_SEL3);
          P_SEL3.setBounds(205, 225, 372, 69);

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- FRSDEB ----
            FRSDEB.setName("FRSDEB");
            P_SEL2.add(FRSDEB);
            FRSDEB.setBounds(155, 5, 68, FRSDEB.getPreferredSize().height);

            //---- FRSFIN ----
            FRSFIN.setName("FRSFIN");
            P_SEL2.add(FRSFIN);
            FRSFIN.setBounds(155, 35, 68, FRSFIN.getPreferredSize().height);

            //---- OBJ_63 ----
            OBJ_63.setText("Fournisseur d\u00e9but");
            OBJ_63.setName("OBJ_63");
            P_SEL2.add(OBJ_63);
            OBJ_63.setBounds(15, 9, 110, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Fournisseur fin");
            OBJ_77.setName("OBJ_77");
            P_SEL2.add(OBJ_77);
            OBJ_77.setBounds(15, 39, 110, 20);
          }
          p_contenu.add(P_SEL2);
          P_SEL2.setBounds(205, 225, 372, 69);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- UCH_GRP ----
    ButtonGroup UCH_GRP = new ButtonGroup();
    UCH_GRP.add(UCH1);
    UCH_GRP.add(UCH2);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_58;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_87;
  private JPanel P_SEL4;
  private JLabel OBJ_61;
  private JLabel OBJ_76;
  private XRiTextField EDEB;
  private XRiTextField EFIN;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private XRiCheckBox REPON5;
  private XRiCheckBox WTOUM;
  private XRiCheckBox WTOU;
  private XRiCheckBox REPON1;
  private JLabel OBJ_84;
  private XRiTextField NBSEL;
  private JLabel OBJ_86;
  private JPanel panel1;
  private JLabel OBJ_88;
  private JLabel OBJ_89;
  private XRiCheckBox REPON4;
  private XRiCheckBox REPON3;
  private JLabel OBJ_91;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JPanel panel2;
  private JXTitledSeparator OBJ_95;
  private XRiRadioButton UCH1;
  private XRiRadioButton UCH2;
  private JPanel panel3;
  private JXTitledSeparator OBJ_37;
  private JLabel OBJ_100;
  private XRiTextField WABC;
  private XRiCheckBox NWABC;
  private JPanel P_SEL1;
  private XRiTextField ESFD;
  private XRiTextField ESFF;
  private JLabel OBJ_64;
  private JLabel OBJ_78;
  private JPanel P_SEL3;
  private XRiTextField EDEBA;
  private XRiTextField EFINA;
  private JLabel OBJ_62;
  private JLabel OBJ_75;
  private JPanel P_SEL2;
  private XRiTextField FRSDEB;
  private XRiTextField FRSFIN;
  private JLabel OBJ_63;
  private JLabel OBJ_77;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
