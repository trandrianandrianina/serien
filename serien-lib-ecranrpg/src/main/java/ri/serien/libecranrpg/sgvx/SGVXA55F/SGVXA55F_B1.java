
package ri.serien.libecranrpg.sgvx.SGVXA55F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/*
 * Created by JFormDesigner on Tue Jul 03 15:02:26 CEST 2012
 */

/**
 * @author Stéphane Vénéri
 */
public class SGVXA55F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXA55F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TYPREA.setValeursSelection("1", "");
    TYPSTK.setValeursSelection("1", "");
    EDTDET.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    
    // icones
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    OBJ_20 = new JXTitledSeparator();
    TYPREA = new XRiCheckBox();
    OBJ_22 = new JLabel();
    WMAG = new XRiTextField();
    TYPSTK = new XRiCheckBox();
    OBJ_21 = new JXTitledSeparator();
    FAMDEB = new XRiTextField();
    OBJ_23 = new JLabel();
    OBJ_24 = new JLabel();
    FAMFIN = new XRiTextField();
    OBJ_25 = new JXTitledSeparator();
    OBJ_26 = new JLabel();
    ARTDEB = new XRiTextField();
    OBJ_28 = new JLabel();
    ARTFIN = new XRiTextField();
    OBJ_29 = new JXTitledSeparator();
    OBJ_30 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    OBJ_31 = new JLabel();
    OBJ_32 = new JXTitledSeparator();
    EDTDET = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_27 = new JXTitledSeparator();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 200));
        menus_haut.setPreferredSize(new Dimension(160, 200));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt_export ----
          riSousMenu_bt_export.setText("Exportation tableur");
          riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
          riSousMenu_bt_export.setName("riSousMenu_bt_export");
          riSousMenu_bt_export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt_exportActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt_export);
        }
        menus_haut.add(riSousMenu6);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(860, 440));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(630, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel1.add(sep_etablissement);
            sep_etablissement.setBounds(10, 10, 585, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel1.add(z_dgnom_);
            z_dgnom_.setBounds(140, 35, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel1.add(z_wencx_);
            z_wencx_.setBounds(140, 65, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel1.add(z_etablissement_);
            z_etablissement_.setBounds(25, 55, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel1.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(70, 50), bouton_etablissement.getPreferredSize()));

            //---- OBJ_19 ----
            OBJ_19.setTitle(" ");
            OBJ_19.setName("OBJ_19");
            panel1.add(OBJ_19);
            OBJ_19.setBounds(10, 175, 585, OBJ_19.getPreferredSize().height);

            //---- OBJ_20 ----
            OBJ_20.setTitle(" ");
            OBJ_20.setName("OBJ_20");
            panel1.add(OBJ_20);
            OBJ_20.setBounds(10, 105, 585, OBJ_20.getPreferredSize().height);

            //---- TYPREA ----
            TYPREA.setText("Zone de stock \u00e0 r\u00e9approvisionner");
            TYPREA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TYPREA.setName("TYPREA");
            panel1.add(TYPREA);
            TYPREA.setBounds(25, 195, 245, 28);

            //---- OBJ_22 ----
            OBJ_22.setText("Code magasin");
            OBJ_22.setName("OBJ_22");
            panel1.add(OBJ_22);
            OBJ_22.setBounds(25, 130, 91, 28);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            panel1.add(WMAG);
            WMAG.setBounds(140, 130, 34, WMAG.getPreferredSize().height);

            //---- TYPSTK ----
            TYPSTK.setText("Depuis une zone de stockage");
            TYPSTK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TYPSTK.setName("TYPSTK");
            panel1.add(TYPSTK);
            TYPSTK.setBounds(320, 195, 245, 28);

            //---- OBJ_21 ----
            OBJ_21.setTitle("Codes familles");
            OBJ_21.setName("OBJ_21");
            panel1.add(OBJ_21);
            OBJ_21.setBounds(10, 235, 585, OBJ_21.getPreferredSize().height);

            //---- FAMDEB ----
            FAMDEB.setComponentPopupMenu(BTD);
            FAMDEB.setName("FAMDEB");
            panel1.add(FAMDEB);
            FAMDEB.setBounds(140, 260, 40, FAMDEB.getPreferredSize().height);

            //---- OBJ_23 ----
            OBJ_23.setText("Code de d\u00e9but");
            OBJ_23.setName("OBJ_23");
            panel1.add(OBJ_23);
            OBJ_23.setBounds(25, 260, 91, 28);

            //---- OBJ_24 ----
            OBJ_24.setText("Code de fin");
            OBJ_24.setName("OBJ_24");
            panel1.add(OBJ_24);
            OBJ_24.setBounds(210, 260, 91, 28);

            //---- FAMFIN ----
            FAMFIN.setComponentPopupMenu(BTD);
            FAMFIN.setName("FAMFIN");
            panel1.add(FAMFIN);
            FAMFIN.setBounds(320, 260, 40, FAMFIN.getPreferredSize().height);

            //---- OBJ_25 ----
            OBJ_25.setTitle("R\u00e9f\u00e9rences articles");
            OBJ_25.setName("OBJ_25");
            panel1.add(OBJ_25);
            OBJ_25.setBounds(10, 300, 585, OBJ_25.getPreferredSize().height);

            //---- OBJ_26 ----
            OBJ_26.setText("Code de d\u00e9but");
            OBJ_26.setName("OBJ_26");
            panel1.add(OBJ_26);
            OBJ_26.setBounds(25, 330, 91, 28);

            //---- ARTDEB ----
            ARTDEB.setComponentPopupMenu(BTD);
            ARTDEB.setName("ARTDEB");
            panel1.add(ARTDEB);
            ARTDEB.setBounds(140, 330, 210, ARTDEB.getPreferredSize().height);

            //---- OBJ_28 ----
            OBJ_28.setText("Code de fin");
            OBJ_28.setName("OBJ_28");
            panel1.add(OBJ_28);
            OBJ_28.setBounds(25, 360, 91, 28);

            //---- ARTFIN ----
            ARTFIN.setComponentPopupMenu(BTD);
            ARTFIN.setName("ARTFIN");
            panel1.add(ARTFIN);
            ARTFIN.setBounds(140, 360, 210, ARTFIN.getPreferredSize().height);

            //---- OBJ_29 ----
            OBJ_29.setTitle(" ");
            OBJ_29.setName("OBJ_29");
            panel1.add(OBJ_29);
            OBJ_29.setBounds(10, 395, 585, OBJ_29.getPreferredSize().height);

            //---- OBJ_30 ----
            OBJ_30.setText("P\u00e9riode \u00e0 traiter du");
            OBJ_30.setName("OBJ_30");
            panel1.add(OBJ_30);
            OBJ_30.setBounds(25, 415, 115, 28);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(140, 415, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(320, 415, 105, DATFIN.getPreferredSize().height);

            //---- OBJ_31 ----
            OBJ_31.setText("au");
            OBJ_31.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_31.setName("OBJ_31");
            panel1.add(OBJ_31);
            OBJ_31.setBounds(240, 415, 75, 28);

            //---- OBJ_32 ----
            OBJ_32.setTitle(" ");
            OBJ_32.setName("OBJ_32");
            panel1.add(OBJ_32);
            OBJ_32.setBounds(10, 455, 585, OBJ_32.getPreferredSize().height);

            //---- EDTDET ----
            EDTDET.setText("Edition d\u00e9taill\u00e9e");
            EDTDET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTDET.setName("EDTDET");
            panel1.add(EDTDET);
            EDTDET.setBounds(25, 475, 245, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_27 ----
    OBJ_27.setTitle("");
    OBJ_27.setName("OBJ_27");
    // //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_20;
  private XRiCheckBox TYPREA;
  private JLabel OBJ_22;
  private XRiTextField WMAG;
  private XRiCheckBox TYPSTK;
  private JXTitledSeparator OBJ_21;
  private XRiTextField FAMDEB;
  private JLabel OBJ_23;
  private JLabel OBJ_24;
  private XRiTextField FAMFIN;
  private JXTitledSeparator OBJ_25;
  private JLabel OBJ_26;
  private XRiTextField ARTDEB;
  private JLabel OBJ_28;
  private XRiTextField ARTFIN;
  private JXTitledSeparator OBJ_29;
  private JLabel OBJ_30;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JLabel OBJ_31;
  private JXTitledSeparator OBJ_32;
  private XRiCheckBox EDTDET;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JXTitledSeparator OBJ_27;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
