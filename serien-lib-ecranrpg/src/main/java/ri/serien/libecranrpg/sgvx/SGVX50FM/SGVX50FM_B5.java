
package ri.serien.libecranrpg.sgvx.SGVX50FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * 
 * [GVM3293] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Compte rendu d'inventaire -> Par magasin et clé de
 * classement 2
 * Indicateur :01100011 (92,93,97)
 * Titre : Compte rendu d'inventaire par magasin et par le 2ème mot de classement
 * 
 * [GVM3294] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Compte rendu d'inventaire -> Par magasin et sous-famille
 * Indicateur :01000111 (92,96,97)
 * Titre : Compte rendu d'inventaire par magasin et par sous-famille
 * 
 * [GVM3311] Gestion des ventes -> Stocks -> Etats des stocks -> Général -> Par famille
 * Indicateur :10010001 (91,94)
 * Titre : Etat général des stocks par famille
 * 
 * [GVM3312] Gestion des ventes -> Stocks -> Etats des stocks -> Général -> Par article
 * Indicateur :10000001 (91)
 * Titre : Etat général des stocks par article
 * 
 * [GVM3314] Gestion des ventes -> Stocks -> Etats des stocks -> Général -> Par sous-famille
 * Indicateur :10000101 (91,96)
 * Titre : Etat général des stocks par sous-famille
 * 
 * [GVM3321] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par famille
 * Indicateur :00010001 (94)
 * Titre : Etat des stocks par magasin et par famille
 * 
 * [GVM3322] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par article
 * Indicateur :00000001
 * Titre : Etat général des stocks par magasin et par article
 * 
 * [GVM3323] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par mot de classement 2
 * Indicateur :00100001 (93)
 * Titre : Etats des stocks par magasin et par le 2ème mot de classement
 * 
 * [GVM3324] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par sous-famille
 * Indicateur :00000101 (96)
 * Titre : Etat des stocks par magasin et par sous-famille
 * 
 * [GVM3326] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par article valorisé
 * Indicateur :00000001
 * Titre : Etat des stocks par magasin et par article
 * 
 * [GVM3351] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire -> Par famille
 * Indicateur :11010001 (91,92,94)
 * Titre : Compte rendu d'inventaire par article
 * 
 * [GVM3352] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire -> Par article
 * Indicateur :11000001 (91,92)
 * Titre : Compte rendu d'inventaire par article
 * 
 * [GVM3353] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire -> Par mot de classement 2
 * Indicateur :11100001 (91,92,93)
 * Titre : Compte rendu d'inventaire par le 2ème mot de classement
 * 
 * [GVM3354] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire -> Par sous-famille
 * Indicateur :11000101 (91,92,96)
 * Titre : Compte rendu d'inventaire par sous-famille
 * 
 * [GVM3361] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par famille
 * Indicateur :01010001 (92,94)
 * Titre : Compte rendu d'inventaire par magasin et par famille
 * 
 * [GVM3362] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par article
 * Indicateur :01000001 (92)
 * Titre : Compte rendu d'inventaire par magasin et par article
 * 
 * [GVM3364] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par sous-famille
 * Indicateur :01000101 (92,96)
 * Titre : Compte rendu d'inventaire par magasin et par sous-famille
 * 
 * [GVM3365] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par article valorisé
 * Indicateur :01000001 (92)
 * Titre : Compte rendu d'inventaire par magasin et par article
 * Bouton: Quitter
 * 
 * [GAM3243] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu
 * inventaire/mag/famille
 * Indicateur :01010011 (92,94,97)
 * Titre : Compte rendu d'inventaire par magasin et par famille
 * 
 * [GAM3244] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu
 * inventaire/mag/article
 * Indicateur :01000011(92,97)
 * Titre : Compte rendu d'inventaire par magasin et par article
 * 
 * [GAM3245] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu inventaire/mag/cl2
 * Indicateur :01100011 (92,93,97)
 * Titre : Compte rendu d'inventaire par magasin et par le 2ème mot de classement
 * 
 * [GAM3246] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu
 * inventaire/mag/ss-fam.
 * Indicateur :01000111 (92,96,97)
 * Titre : Compte rendu d'inventaire par magasin et par sous-famille
 * 
 * [GAM3311] Gestion des achats -> Stocks -> Editions stocks -> Général -> Par famille
 * Indicateur :10010001 (91,94)
 * Titre : Etats général des stocks par famille
 * 
 * [GAM3312] Gestion des achats -> Stocks -> Editions stocks -> Général -> Par article
 * Indicateur :10000001 (91)
 * Titre : Etats général des stocks par article
 * 
 * [GAM3313] Gestion des achats -> Stocks -> Editions stocks -> Général -> Par mot de classement 2
 * Indicateur :10100001 (91,93)
 * Titre : Etat général des stocks par le 2ème mot de classement
 * 
 * [GAM3314] Gestion des achats -> Stocks -> Editions stocks -> Général -> Par fournisseur
 * Indicateur :10001001 (91,95)
 * Titre : Etats général des stocks par fournisseur
 * 
 * [GAM3315] Gestion des achats -> Stocks -> Editions stocks -> Général -> Par sous-famille
 * Indicateur :10000101 (91,96)
 * Titre : Etat général des stocks par sous-famille
 * 
 * [GAM3321] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par famille
 * Indicateur :00010001 (94)
 * Titre : Etats des stocks par magasin et par famille
 * 
 * [GAM3322] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par article
 * Indicateur :00000001
 * Titre : Etats des stocks par magasin et par article
 * 
 * [GAM3323] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par mot de classement 2
 * Indicateur :00100001 (93)
 * Titre : Etat des stocks par magasin et par le 2ème mot de classement
 * 
 * [GAM3325] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par sous-famille
 * Indicateur :00000101 (96)
 * Titre : Etats des stocks par magasin et pas sous-famille
 * 
 * [GAM3351] Gestion des achats -> Stocks -> Editions stocks -> Ecarts sur inventaire -> Par famille
 * Indicateur :11010001 (91,92,94)
 * Titre : Compte rendu d'inventaire par famille
 * 
 * [GAM3352] Gestion des achats -> Stocks -> Editions stocks -> Ecarts sur inventaire -> Par article
 * Indicateur :11000001 (91,92)
 * Titre : Compte rendu d'inventaire par article
 * 
 * [GAM3353] Gestion des achats -> Stocks -> Editions stocks -> Ecarts sur inventaire -> Par mot de classement 2
 * Indicateur :11100001 (91,92,93)
 * Titre : Compte rendu d'inventaire par le 2ème mot de classement
 * 
 * [GAM3361] Gestion des achats -> Stocks -> Editions stocks -> Ecarts sur inventaire (par magasin) -> Par famille
 * Indicateur :01010001 (92,94)
 * Titre : Compte rendu d'inventaire par magasin et par famille
 * 
 * [GAM3362] Gestion des achats -> Stocks -> Editions stocks -> Ecarts sur inventaire (par magasin) -> Par article
 * Indicateur :01000001 (92)
 * Titre : Compte rendu d'inventaire par magasin et par article
 * 
 * [GAM3363] Gestion des achats -> Stocks -> Editions stocks -> Ecarts sur inventaire (par magasin) -> Par mot de classement 2
 * Indicateur :01100001 (92,93)
 * Titre : Compte rendu d'inventaire par magasin et par le 2ème mot de classement
 * 
 * [GPM5311] Gestion de la production -> Stocks -> Editions stocks -> Général -> Par Famille
 * Indicateur :10010001 (91,94)
 * Titre : Etats général des stocks par famille
 * 
 * [GPM5312] Gestion de la production -> Stocks -> Editions stocks -> Général -> Par Article
 * Indicateur :10000001 (91)
 * Titre : Etats général des stocks par article
 * 
 * [GPM5313] Gestion de la production -> Stocks -> Editions stocks -> Général -> Par Mot de Classement 2
 * Indicateur :10100001 (91,93)
 * Titre : Etat général des stocks par le 2ème mot de classement
 * 
 * [GPM5314] Gestion de la production -> Stocks -> Editions stocks -> Général -> Par Fournisseur
 * Indicateur :10001001 (91,95)
 * Titre : Etats général des stock par fournisseur
 * 
 * [GPM5315] Gestion de la production -> Stocks -> Editions stocks -> Général -> Par Sous-Famille
 * Indicateur :10000101 (91,95)
 * Titre : Etats général des stocks par sous-famille
 * 
 * [GPM5321] Gestion de la production -> Stocks -> Editions stocks -> Par Magasin -> Par Famille
 * Indicateur :00010001 (94)
 * Titre : Etats des stocks par magasin et par famille
 * 
 * [GPM5322] Gestion de la production -> Stocks -> Editions stocks -> Par Magasin -> Par Article
 * Indicateur :00000001
 * Titre : Etats des stocks par magasin et par article
 * 
 * [GPM5323] Gestion de la production -> Stocks -> Editions stocks -> Par Magasin -> Par Mot de Classement 2
 * Indicateur :00100001 (93)
 * Titre : Etat des stocks par magasin et par le 2ème mot de classement
 * 
 * [GPM5324] Gestion de la production -> Stocks -> Editions stocks -> Par Magasin -> Par Fournisseur
 * Indicateur :00001001 (95)
 * Titre : Etats des stocks par magasin et par fournisseur
 * 
 * [GPM5325] Gestion de la production -> Stocks -> Editions stocks -> Par Magasin -> Par Sous-Famille
 * Indicateur :00000101 (96)
 * Titre : Etats des stocks par magasin et sous-famille
 */
public class SGVX50FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_CHOIX_PAPIER = "Choisir papier";
  private Message LOCTP = null;
  
  public SGVX50FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    NWABC.setValeursSelection("1", " ");
    REPON6.setValeursSelection("O", " ");
    ARTTOT.setValeursSelection("1", " ");
    REPON5.setValeursSelection("OUI", "NON");
    REPON2.setValeursSelection("OUI", "NON");
    REPON4.setValeursSelection("OUI", "NON");
    REPON3.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snSousFamilleDebut.lierComposantFin(snSousFamilleFin);
    
    snBarreBouton.ajouterBouton(BOUTON_CHOIX_PAPIER, 'E', true);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Titre
    chargerTitre();
    
    if (lexique.isPresent("REPON2")) {
      pnlPeriode.setTitre("Période à éditer : la date de fin est la veille du jour de l'inventaire");
    }
    else {
      pnlPeriode.setTitre("Période à éditer");
    }
    // Indicateur
    Boolean is91 = lexique.isTrue("91");
    Boolean isAdresseStockage = lexique.isTrue("92 and 97");
    
    // Visibilité des composant magasin
    if (!is91) {
      snMagasin6.setVisible(true);
      snMagasin5.setVisible(true);
      snMagasin4.setVisible(true);
      snMagasin3.setVisible(true);
      snMagasin2.setVisible(true);
      snMagasin1.setVisible(true);
    }
    else {
      snMagasin6.setVisible(false);
      snMagasin5.setVisible(false);
      snMagasin4.setVisible(false);
      snMagasin3.setVisible(false);
      snMagasin2.setVisible(false);
      snMagasin1.setVisible(false);
    }
    
    // Visibilité des labels
    lbMagasin6.setVisible(snMagasin6.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin1.setVisible(snMagasin1.isVisible());
    
    pnlAdressesStockages.setVisible(isAdresseStockage);
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Renseigne l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation des composants
    initialisationVisibiliteComposants();
    initialisationDesCriteresDeSelection();
    chargerListeComposantsMagasin();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin1.renseignerChampRPG(lexique, "MA01");
    snMagasin2.renseignerChampRPG(lexique, "MA02");
    snMagasin3.renseignerChampRPG(lexique, "MA03");
    snMagasin4.renseignerChampRPG(lexique, "MA04");
    snMagasin5.renseignerChampRPG(lexique, "MA05");
    snMagasin6.renseignerChampRPG(lexique, "MA06");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_CHOIX_PAPIER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le titre suivant le bon point de menu
   */
  private void chargerTitre() {
    Boolean is91 = lexique.isTrue("91");
    Boolean is92 = lexique.isTrue("92");
    Boolean is93 = lexique.isTrue("93");
    Boolean is94 = lexique.isTrue("94");
    Boolean is95 = lexique.isTrue("95");
    Boolean is96 = lexique.isTrue("96");
    Boolean is97 = lexique.isTrue("97");
    
    if (!is91 && !is92 && !is93 && !is94 && !is95 && !is96 && !is97) {
      bpPresentation.setText("Etat des stocks par magasin et par article");
    }
    if (is91) {
      bpPresentation.setText("Etat général des stocks par article");
    }
    if (is92) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par article");
    }
    if (is93) {
      bpPresentation.setText("Etat des stocks par magasin et par le 2ème mot de classement");
    }
    if (is94) {
      bpPresentation.setText("Etat des stocks par magasin et par famille");
    }
    if (is95) {
      bpPresentation.setText("Etat des stocks par magasin et par fournisseur");
    }
    if (is96) {
      bpPresentation.setText("Etat des stocks par magasin et par sous-famille");
    }
    if (is91 && is92 && !is93) {
      bpPresentation.setText("Compte rendu d'inventaire par article");
    }
    if (is91 && is93) {
      bpPresentation.setText("Etat général des stocks par le 2ème mot de classement");
    }
    if (is91 && is94) {
      bpPresentation.setText("Etat général des stocks par famille");
    }
    if (is91 && is95) {
      bpPresentation.setText("Etat général des stocks par fournisseur");
    }
    if (is91 && is96) {
      bpPresentation.setText("Etat général des stocks par sous-famille");
    }
    if (is92 && is93 && !is97) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par le 2ème mot de classement");
    }
    if (is92 && is94 && !is97) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par famille");
    }
    if (is92 && is96 && !is97) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par sous-famille");
    }
    if (is92 && is97 && !is93) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par article");
    }
    if (is91 && is92 && is93) {
      bpPresentation.setText("Compte rendu d'inventaire par le 2ème mot de classement");
    }
    if (is91 && is92 && is94) {
      bpPresentation.setText("Compte rendu d'inventaire par famille");
    }
    if (is91 && is92 && is96) {
      bpPresentation.setText("Compte rendu d'inventaire par sous-famille");
    }
    if (is92 && is93 && is97) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par le 2ème mot de classement");
    }
    if (is92 && is94 && is97) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par famille");
    }
    if (is92 && is96 && is97) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par sous-famille");
    }
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * snMagasin1 : "Tous" autorisé, "Aucun" interdit
   * snMagasin2-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On vérifie que le nombre de magasins n'est pas supérieur au nombre de composant magasin présent.
    // On charge un nombre de composants égal au nombre de choix possibles ou au nombre maximal de composants disponibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    for (int i = 0; i < listeComposant.size(); i++) {
      listeComposant.get(i).setVisible(false);
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On affiche les composants snMagasin2-6 si ils ne sont pas en "Aucun" (null)
      listeComposant.get(i).setVisible(!(listeComposant.get(i).getIdSelection() == null));
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
    
  }
  
  /**
   * Permet d'initialiser les critères de sélection suivant quelle pts de menu sélectionner
   */
  private void initialisationDesCriteresDeSelection() {
    if (lexique.isTrue("(N93) AND (N94) AND (N95) AND (N96)")) {
      lbTexteFin.setText("Article de fin");
      lbTexteDebut.setText("Article de début");
      snArticleDebut.setSession(getSession());
      snArticleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snArticleDebut.charger(false);
      snArticleDebut.setSelectionParChampRPG(lexique, "EDEBA");
      snArticleFin.setSession(getSession());
      snArticleFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snArticleFin.charger(false);
      snArticleFin.setSelectionParChampRPG(lexique, "EFINA");
    }
    else if (lexique.isTrue("93")) {
      lbTexteFin.setText("2eme clé article de fin");
      lbTexteDebut.setText("2eme clé article début");
      tfLibelleDebut.setText(lexique.HostFieldGetData("DEBIAK"));
      tfLibelleFin.setText(lexique.HostFieldGetData("FINIAK"));
    }
    else if (lexique.isTrue("94")) {
      lbTexteFin.setText("Famille de fin");
      lbTexteDebut.setText("Famille de début");
      snFamilleDebut.setSession(getSession());
      snFamilleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snFamilleDebut.setTousAutorise(true);
      snFamilleDebut.charger(false);
      snFamilleDebut.setSelectionParChampRPG(lexique, "EDEB");
      snFamilleFin.setSession(getSession());
      snFamilleFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snFamilleFin.setTousAutorise(true);
      snFamilleFin.charger(false);
      snFamilleFin.setSelectionParChampRPG(lexique, "EFIN");
    }
    else if (lexique.isTrue("95")) {
      lbTexteFin.setText("Fournisseur de fin");
      lbTexteDebut.setText("Fournisseur de début");
      snFournisseurDebut.setSession(getSession());
      snFournisseurDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snFournisseurDebut.charger(false);
      snFournisseurDebut.setSelectionParChampRPG(lexique, "FRSDEB");
      snFournisseurFin.setSession(getSession());
      snFournisseurFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snFournisseurFin.charger(false);
      snFournisseurFin.setSelectionParChampRPG(lexique, "FRSFIN");
    }
    else if (lexique.isTrue("96")) {
      lbTexteFin.setText("Sous famille de fin");
      lbTexteDebut.setText("Sous famille de début");
      snSousFamilleDebut.setSession(getSession());
      snSousFamilleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snSousFamilleDebut.setTousAutorise(true);
      snSousFamilleDebut.charger(false);
      snSousFamilleDebut.setSelectionParChampRPG(lexique, "DEBIAE");
      snSousFamilleFin.setSession(getSession());
      snSousFamilleFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
      snSousFamilleFin.setTousAutorise(true);
      snSousFamilleFin.charger(false);
      snSousFamilleFin.setSelectionParChampRPG(lexique, "FINIAE");
    }
  }
  
  /**
   * Gére la visibilité des composant suivant la situation
   */
  private void initialisationVisibiliteComposants() {
    if (lexique.isTrue("(N93) AND (N94) AND (N95) AND (N96)")) {
      
      tfLibelleDebut.setVisible(false);
      tfLibelleFin.setVisible(false);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      snSousFamilleDebut.setVisible(false);
      snSousFamilleFin.setVisible(false);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      snArticleDebut.setVisible(true);
      snArticleFin.setVisible(true);
    }
    else if (lexique.isTrue("93")) {
      
      tfLibelleDebut.setVisible(true);
      tfLibelleFin.setVisible(true);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      snSousFamilleDebut.setVisible(false);
      snSousFamilleFin.setVisible(false);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else if (lexique.isTrue("94")) {
      
      tfLibelleDebut.setVisible(false);
      tfLibelleFin.setVisible(false);
      snFamilleDebut.setVisible(true);
      snFamilleFin.setVisible(true);
      snSousFamilleDebut.setVisible(false);
      snSousFamilleFin.setVisible(false);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else if (lexique.isTrue("95")) {
      
      tfLibelleDebut.setVisible(false);
      tfLibelleFin.setVisible(false);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      snSousFamilleDebut.setVisible(false);
      snSousFamilleFin.setVisible(false);
      snFournisseurDebut.setVisible(true);
      snFournisseurFin.setVisible(true);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else if (lexique.isTrue("96")) {
      
      tfLibelleDebut.setVisible(false);
      tfLibelleFin.setVisible(false);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      snSousFamilleDebut.setVisible(true);
      snSousFamilleFin.setVisible(true);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
  }
  
  private void btRechercheActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbTexteDebut = new SNLabelChamp();
    pnlSelectionCritereSelection = new SNPanel();
    pnlInfoSelectionCritereSelection = new SNPanel();
    tfLibelleDebut = new SNTexte();
    tfLibelleFin = new SNTexte();
    snArticleDebut = new SNArticle();
    snArticleFin = new SNArticle();
    snFamilleDebut = new SNFamille();
    snFamilleFin = new SNFamille();
    snFournisseurDebut = new SNFournisseur();
    snFournisseurFin = new SNFournisseur();
    snSousFamilleDebut = new SNSousFamille();
    snSousFamilleFin = new SNSousFamille();
    lbTexteFin = new SNLabelChamp();
    REPON1 = new XRiCheckBox();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    pnlPeriode = new SNPanelTitre();
    lbDebut = new SNLabelChamp();
    DATDEB = new XRiCalendrier();
    lbFin = new SNLabelChamp();
    DATFIN = new XRiCalendrier();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlOptions = new SNPanelTitre();
    REPON3 = new XRiCheckBox();
    REPON5 = new XRiCheckBox();
    REPON4 = new XRiCheckBox();
    REPON2 = new XRiCheckBox();
    ARTTOT = new XRiCheckBox();
    REPON6 = new XRiCheckBox();
    NWABC = new XRiCheckBox();
    pnlCodeSection = new SNPanel();
    lbSelection = new SNLabelChamp();
    WABC = new XRiTextField();
    pnlAdressesStockages = new SNPanelTitre();
    WADS1 = new XRiTextField();
    WADS2 = new XRiTextField();
    WADS3 = new XRiTextField();
    WADS4 = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 690));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setMinimumSize(new Dimension(120, 30));
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbTexteDebut ----
            lbTexteDebut.setText("text");
            lbTexteDebut.setName("lbTexteDebut");
            pnlCritereDeSelection.add(lbTexteDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlSelectionCritereSelection ========
            {
              pnlSelectionCritereSelection.setName("pnlSelectionCritereSelection");
              pnlSelectionCritereSelection.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSelectionCritereSelection.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlSelectionCritereSelection.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSelectionCritereSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ======== pnlInfoSelectionCritereSelection ========
              {
                pnlInfoSelectionCritereSelection.setName("pnlInfoSelectionCritereSelection");
                pnlInfoSelectionCritereSelection.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlInfoSelectionCritereSelection.getLayout()).columnWidths = new int[] { 0, 0 };
                ((GridBagLayout) pnlInfoSelectionCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlInfoSelectionCritereSelection.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
                ((GridBagLayout) pnlInfoSelectionCritereSelection.getLayout()).rowWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- tfLibelleDebut ----
                tfLibelleDebut.setEnabled(false);
                tfLibelleDebut.setPreferredSize(new Dimension(300, 30));
                tfLibelleDebut.setMinimumSize(new Dimension(300, 30));
                tfLibelleDebut.setMaximumSize(new Dimension(300, 30));
                tfLibelleDebut.setName("tfLibelleDebut");
                pnlInfoSelectionCritereSelection.add(tfLibelleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- tfLibelleFin ----
                tfLibelleFin.setEnabled(false);
                tfLibelleFin.setPreferredSize(new Dimension(300, 30));
                tfLibelleFin.setMinimumSize(new Dimension(300, 30));
                tfLibelleFin.setMaximumSize(new Dimension(300, 30));
                tfLibelleFin.setName("tfLibelleFin");
                pnlInfoSelectionCritereSelection.add(tfLibelleFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snArticleDebut ----
                snArticleDebut.setPreferredSize(new Dimension(300, 30));
                snArticleDebut.setMinimumSize(new Dimension(300, 30));
                snArticleDebut.setMaximumSize(new Dimension(300, 30));
                snArticleDebut.setEnabled(false);
                snArticleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
                snArticleDebut.setName("snArticleDebut");
                pnlInfoSelectionCritereSelection.add(snArticleDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snArticleFin ----
                snArticleFin.setPreferredSize(new Dimension(300, 30));
                snArticleFin.setMinimumSize(new Dimension(300, 30));
                snArticleFin.setMaximumSize(new Dimension(300, 30));
                snArticleFin.setEnabled(false);
                snArticleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
                snArticleFin.setName("snArticleFin");
                pnlInfoSelectionCritereSelection.add(snArticleFin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snFamilleDebut ----
                snFamilleDebut.setPreferredSize(new Dimension(300, 30));
                snFamilleDebut.setMinimumSize(new Dimension(300, 30));
                snFamilleDebut.setMaximumSize(new Dimension(300, 30));
                snFamilleDebut.setEnabled(false);
                snFamilleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
                snFamilleDebut.setName("snFamilleDebut");
                pnlInfoSelectionCritereSelection.add(snFamilleDebut, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snFamilleFin ----
                snFamilleFin.setPreferredSize(new Dimension(300, 30));
                snFamilleFin.setMinimumSize(new Dimension(300, 30));
                snFamilleFin.setMaximumSize(new Dimension(300, 30));
                snFamilleFin.setEnabled(false);
                snFamilleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
                snFamilleFin.setName("snFamilleFin");
                pnlInfoSelectionCritereSelection.add(snFamilleFin, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snFournisseurDebut ----
                snFournisseurDebut.setPreferredSize(new Dimension(300, 30));
                snFournisseurDebut.setMinimumSize(new Dimension(300, 30));
                snFournisseurDebut.setMaximumSize(new Dimension(300, 30));
                snFournisseurDebut.setEnabled(false);
                snFournisseurDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
                snFournisseurDebut.setName("snFournisseurDebut");
                pnlInfoSelectionCritereSelection.add(snFournisseurDebut, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snFournisseurFin ----
                snFournisseurFin.setPreferredSize(new Dimension(300, 30));
                snFournisseurFin.setMinimumSize(new Dimension(300, 30));
                snFournisseurFin.setMaximumSize(new Dimension(300, 30));
                snFournisseurFin.setEnabled(false);
                snFournisseurFin.setFont(new Font("sansserif", Font.PLAIN, 14));
                snFournisseurFin.setName("snFournisseurFin");
                pnlInfoSelectionCritereSelection.add(snFournisseurFin, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snSousFamilleDebut ----
                snSousFamilleDebut.setPreferredSize(new Dimension(300, 30));
                snSousFamilleDebut.setMinimumSize(new Dimension(300, 30));
                snSousFamilleDebut.setMaximumSize(new Dimension(300, 30));
                snSousFamilleDebut.setEnabled(false);
                snSousFamilleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
                snSousFamilleDebut.setName("snSousFamilleDebut");
                pnlInfoSelectionCritereSelection.add(snSousFamilleDebut, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snSousFamilleFin ----
                snSousFamilleFin.setPreferredSize(new Dimension(300, 30));
                snSousFamilleFin.setMinimumSize(new Dimension(300, 30));
                snSousFamilleFin.setMaximumSize(new Dimension(300, 30));
                snSousFamilleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
                snSousFamilleFin.setEnabled(false);
                snSousFamilleFin.setName("snSousFamilleFin");
                pnlInfoSelectionCritereSelection.add(snSousFamilleFin, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlSelectionCritereSelection.add(pnlInfoSelectionCritereSelection, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlSelectionCritereSelection, new GridBagConstraints(1, 0, 2, 2, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTexteFin ----
            lbTexteFin.setText("text");
            lbTexteFin.setName("lbTexteFin");
            pnlCritereDeSelection.add(lbTexteFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPON1 ----
            REPON1.setText("Totalisation");
            REPON1.setComponentPopupMenu(null);
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setPreferredSize(new Dimension(95, 30));
            REPON1.setMinimumSize(new Dimension(95, 30));
            REPON1.setName("REPON1");
            pnlCritereDeSelection.add(REPON1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbMagasin1 ----
            lbMagasin1.setText("Magasin 1");
            lbMagasin1.setName("lbMagasin1");
            pnlCritereDeSelection.add(lbMagasin1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setEnabled(false);
            snMagasin1.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin1.setName("snMagasin1");
            pnlCritereDeSelection.add(snMagasin1, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin 2");
            lbMagasin2.setName("lbMagasin2");
            pnlCritereDeSelection.add(lbMagasin2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin2 ----
            snMagasin2.setEnabled(false);
            snMagasin2.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin2.setName("snMagasin2");
            pnlCritereDeSelection.add(snMagasin2, new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Magasin 3");
            lbMagasin3.setName("lbMagasin3");
            pnlCritereDeSelection.add(lbMagasin3, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin3 ----
            snMagasin3.setEnabled(false);
            snMagasin3.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin3.setName("snMagasin3");
            pnlCritereDeSelection.add(snMagasin3, new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin4 ----
            lbMagasin4.setText("Magasin 4");
            lbMagasin4.setName("lbMagasin4");
            pnlCritereDeSelection.add(lbMagasin4, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin4 ----
            snMagasin4.setEnabled(false);
            snMagasin4.setName("snMagasin4");
            pnlCritereDeSelection.add(snMagasin4, new GridBagConstraints(1, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin5 ----
            lbMagasin5.setText("Magasin 5");
            lbMagasin5.setName("lbMagasin5");
            pnlCritereDeSelection.add(lbMagasin5, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin5 ----
            snMagasin5.setEnabled(false);
            snMagasin5.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin5.setName("snMagasin5");
            pnlCritereDeSelection.add(snMagasin5, new GridBagConstraints(1, 7, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin6 ----
            lbMagasin6.setText("Magasin 6");
            lbMagasin6.setName("lbMagasin6");
            pnlCritereDeSelection.add(lbMagasin6, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin6 ----
            snMagasin6.setEnabled(false);
            snMagasin6.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin6.setName("snMagasin6");
            pnlCritereDeSelection.add(snMagasin6, new GridBagConstraints(1, 8, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlPeriode ========
          {
            pnlPeriode.setTitre("P\u00e9riode \u00e0 \u00e9diter : la date de fin est la veille du jour de l'inventaire");
            pnlPeriode.setName("pnlPeriode");
            pnlPeriode.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPeriode.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlPeriode.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPeriode.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPeriode.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbDebut ----
            lbDebut.setText("Date de d\u00e9but");
            lbDebut.setName("lbDebut");
            pnlPeriode.add(lbDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- DATDEB ----
            DATDEB.setPreferredSize(new Dimension(110, 30));
            DATDEB.setMinimumSize(new Dimension(110, 30));
            DATDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATDEB.setName("DATDEB");
            pnlPeriode.add(DATDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbFin ----
            lbFin.setText("Date de fin");
            lbFin.setName("lbFin");
            pnlPeriode.add(lbFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- DATFIN ----
            DATFIN.setMinimumSize(new Dimension(110, 30));
            DATFIN.setPreferredSize(new Dimension(110, 30));
            DATFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATFIN.setName("DATFIN");
            pnlPeriode.add(DATFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptions ========
          {
            pnlOptions.setTitre("Options d'\u00e9dition");
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- REPON3 ----
            REPON3.setText("Edition des articles non g\u00e9r\u00e9s sur p\u00e9riode");
            REPON3.setComponentPopupMenu(null);
            REPON3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON3.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON3.setName("REPON3");
            pnlOptions.add(REPON3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPON5 ----
            REPON5.setText("Edition en double quantit\u00e9");
            REPON5.setComponentPopupMenu(null);
            REPON5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON5.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON5.setPreferredSize(new Dimension(187, 30));
            REPON5.setMinimumSize(new Dimension(187, 30));
            REPON5.setName("REPON5");
            pnlOptions.add(REPON5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON4 ----
            REPON4.setText("Edition des articles d\u00e9sactiv\u00e9s");
            REPON4.setComponentPopupMenu(null);
            REPON4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON4.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON4.setPreferredSize(new Dimension(212, 30));
            REPON4.setMinimumSize(new Dimension(212, 30));
            REPON4.setName("REPON4");
            pnlOptions.add(REPON4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPON2 ----
            REPON2.setText("Ecarts sur inventaire seulement");
            REPON2.setComponentPopupMenu(null);
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON2.setMinimumSize(new Dimension(222, 30));
            REPON2.setPreferredSize(new Dimension(222, 30));
            REPON2.setName("REPON2");
            pnlOptions.add(REPON2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ARTTOT ----
            ARTTOT.setText("Totaux articles seulement");
            ARTTOT.setComponentPopupMenu(null);
            ARTTOT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARTTOT.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARTTOT.setMinimumSize(new Dimension(185, 30));
            ARTTOT.setPreferredSize(new Dimension(185, 30));
            ARTTOT.setName("ARTTOT");
            pnlOptions.add(ARTTOT, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPON6 ----
            REPON6.setText("Edition simplifi\u00e9e");
            REPON6.setComponentPopupMenu(null);
            REPON6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON6.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON6.setPreferredSize(new Dimension(128, 30));
            REPON6.setMinimumSize(new Dimension(128, 30));
            REPON6.setName("REPON6");
            pnlOptions.add(REPON6, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- NWABC ----
            NWABC.setText("Exclusion");
            NWABC.setComponentPopupMenu(null);
            NWABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NWABC.setFont(new Font("sansserif", Font.PLAIN, 14));
            NWABC.setMinimumSize(new Dimension(82, 30));
            NWABC.setPreferredSize(new Dimension(82, 30));
            NWABC.setName("NWABC");
            pnlOptions.add(NWABC, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlCodeSection ========
            {
              pnlCodeSection.setName("pnlCodeSection");
              pnlCodeSection.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCodeSection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCodeSection.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCodeSection.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlCodeSection.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbSelection ----
              lbSelection.setText("S\u00e9lection code A,B,C,D");
              lbSelection.setName("lbSelection");
              pnlCodeSection.add(lbSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WABC ----
              WABC.setComponentPopupMenu(null);
              WABC.setFont(new Font("sansserif", Font.PLAIN, 14));
              WABC.setPreferredSize(new Dimension(24, 30));
              WABC.setMinimumSize(new Dimension(24, 30));
              WABC.setName("WABC");
              pnlCodeSection.add(WABC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOptions.add(pnlCodeSection, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlAdressesStockages ========
            {
              pnlAdressesStockages.setTitre("Adresse de stockage");
              pnlAdressesStockages.setName("pnlAdressesStockages");
              pnlAdressesStockages.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlAdressesStockages.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlAdressesStockages.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlAdressesStockages.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlAdressesStockages.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WADS1 ----
              WADS1.setComponentPopupMenu(null);
              WADS1.setFont(new Font("sansserif", Font.PLAIN, 14));
              WADS1.setMinimumSize(new Dimension(44, 30));
              WADS1.setPreferredSize(new Dimension(44, 30));
              WADS1.setName("WADS1");
              pnlAdressesStockages.add(WADS1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WADS2 ----
              WADS2.setComponentPopupMenu(null);
              WADS2.setFont(new Font("sansserif", Font.PLAIN, 14));
              WADS2.setPreferredSize(new Dimension(44, 30));
              WADS2.setMinimumSize(new Dimension(44, 30));
              WADS2.setName("WADS2");
              pnlAdressesStockages.add(WADS2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WADS3 ----
              WADS3.setComponentPopupMenu(null);
              WADS3.setFont(new Font("sansserif", Font.PLAIN, 14));
              WADS3.setPreferredSize(new Dimension(44, 30));
              WADS3.setMinimumSize(new Dimension(44, 30));
              WADS3.setName("WADS3");
              pnlAdressesStockages.add(WADS3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WADS4 ----
              WADS4.setComponentPopupMenu(null);
              WADS4.setFont(new Font("sansserif", Font.PLAIN, 14));
              WADS4.setPreferredSize(new Dimension(44, 30));
              WADS4.setMinimumSize(new Dimension(44, 30));
              WADS4.setName("WADS4");
              pnlAdressesStockages.add(WADS4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOptions.add(pnlAdressesStockages, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbTexteDebut;
  private SNPanel pnlSelectionCritereSelection;
  private SNPanel pnlInfoSelectionCritereSelection;
  private SNTexte tfLibelleDebut;
  private SNTexte tfLibelleFin;
  private SNArticle snArticleDebut;
  private SNArticle snArticleFin;
  private SNFamille snFamilleDebut;
  private SNFamille snFamilleFin;
  private SNFournisseur snFournisseurDebut;
  private SNFournisseur snFournisseurFin;
  private SNSousFamille snSousFamilleDebut;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbTexteFin;
  private XRiCheckBox REPON1;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNPanelTitre pnlPeriode;
  private SNLabelChamp lbDebut;
  private XRiCalendrier DATDEB;
  private SNLabelChamp lbFin;
  private XRiCalendrier DATFIN;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox REPON3;
  private XRiCheckBox REPON5;
  private XRiCheckBox REPON4;
  private XRiCheckBox REPON2;
  private XRiCheckBox ARTTOT;
  private XRiCheckBox REPON6;
  private XRiCheckBox NWABC;
  private SNPanel pnlCodeSection;
  private SNLabelChamp lbSelection;
  private XRiTextField WABC;
  private SNPanelTitre pnlAdressesStockages;
  private XRiTextField WADS1;
  private XRiTextField WADS2;
  private XRiTextField WADS3;
  private XRiTextField WADS4;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
