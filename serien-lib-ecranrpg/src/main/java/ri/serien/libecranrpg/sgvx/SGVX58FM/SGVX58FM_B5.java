
package ri.serien.libecranrpg.sgvx.SGVX58FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX58FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX58FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPON1.setValeursSelection("OUI", "NON");
    REPON4.setValeursSelection("OUI", "NON");
    WTOUM.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    P_SEL5.setVisible(lexique.isTrue("N91"));
    
    // Maj du titre
    if (lexique.isTrue("93")) {
      OBJ_23.setTitle("Plage 2ème clé article");
      P_SEL1.setVisible(false);
      P_SEL2.setVisible(false);
      P_SEL3.setVisible(true);
      P_SEL4.setVisible(false);
    }
    else if (lexique.isTrue("94")) {
      OBJ_23.setTitle("Plage de familles");
      P_SEL1.setVisible(false);
      P_SEL2.setVisible(true);
      P_SEL3.setVisible(false);
      P_SEL4.setVisible(false);
    }
    else if (lexique.isTrue("94")) {
      OBJ_23.setTitle("Plage de fournisseurs");
      P_SEL1.setVisible(false);
      P_SEL2.setVisible(false);
      P_SEL3.setVisible(false);
      P_SEL4.setVisible(true);
    }
    else if (lexique.isTrue("(N95) AND (N94) AND (N95)")) {
      OBJ_23.setTitle("Plage articles");
      P_SEL1.setVisible(true);
      P_SEL2.setVisible(false);
      P_SEL3.setVisible(false);
      P_SEL4.setVisible(false);
    }
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (REPON4.isSelected())
    // lexique.HostFieldPutData("REPON4", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON4", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    if (lexique.isTrue("93")) {
      P_SEL3.setVisible(!P_SEL3.isVisible());
    }
    else if (lexique.isTrue("94")) {
      P_SEL2.setVisible(!P_SEL2.isVisible());
    }
    else if (lexique.isTrue("94")) {
      P_SEL4.setVisible(!P_SEL4.isVisible());
    }
    else if (lexique.isTrue("(N95) AND (N94) AND (N95)")) {
      P_SEL1.setVisible(!P_SEL1.isVisible());
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    panel5 = new JPanel();
    OBJ_29 = new JXTitledSeparator();
    OBJ_41 = new JXTitledSeparator();
    OBJ_67 = new JLabel();
    DATFIN = new XRiCalendrier();
    REPON4 = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    OBJ_23 = new JXTitledSeparator();
    P_SEL1 = new JPanel();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    EDEBA = new XRiTextField();
    EFINA = new XRiTextField();
    WTOU = new XRiCheckBox();
    P_SEL5 = new JPanel();
    OBJ_45 = new JXTitledSeparator();
    WTOUM = new XRiCheckBox();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    P_SEL2 = new JPanel();
    OBJ_36 = new JLabel();
    OBJ_33 = new JLabel();
    EDEB = new XRiTextField();
    EFIN = new XRiTextField();
    P_SEL3 = new JPanel();
    DEBIAK = new XRiTextField();
    FINIAK = new XRiTextField();
    OBJ_40 = new JLabel();
    OBJ_35 = new JLabel();
    P_SEL4 = new JPanel();
    FRSDEB = new XRiTextField();
    FRSFIN = new XRiTextField();
    OBJ_39 = new JLabel();
    OBJ_34 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(695, 430));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 625, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- OBJ_29 ----
            OBJ_29.setTitle("");
            OBJ_29.setName("OBJ_29");
            panel5.add(OBJ_29);
            OBJ_29.setBounds(355, 11, 280, 13);

            //---- OBJ_41 ----
            OBJ_41.setTitle("Etat des stocks");
            OBJ_41.setName("OBJ_41");
            panel5.add(OBJ_41);
            OBJ_41.setBounds(10, 10, 315, 14);

            //---- OBJ_67 ----
            OBJ_67.setText("Date");
            OBJ_67.setName("OBJ_67");
            panel5.add(OBJ_67);
            OBJ_67.setBounds(25, 45, 40, 18);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel5.add(DATFIN);
            DATFIN.setBounds(180, 40, 105, DATFIN.getPreferredSize().height);

            //---- REPON4 ----
            REPON4.setText("Edition des articles d\u00e9sactiv\u00e9s");
            REPON4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON4.setName("REPON4");
            panel5.add(REPON4);
            REPON4.setBounds(375, 45, 200, REPON4.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel5);
          panel5.setBounds(25, 320, 640, 95);

          //---- REPON1 ----
          REPON1.setText("Totalisation");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");
          p_contenu.add(REPON1);
          REPON1.setBounds(50, 295, 100, REPON1.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setTitle("Plage articles");
          OBJ_23.setName("OBJ_23");
          p_contenu.add(OBJ_23);
          OBJ_23.setBounds(35, 205, 625, OBJ_23.getPreferredSize().height);

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_37 ----
            OBJ_37.setText("Code de d\u00e9but");
            OBJ_37.setName("OBJ_37");
            P_SEL1.add(OBJ_37);
            OBJ_37.setBounds(15, 10, 128, 18);

            //---- OBJ_38 ----
            OBJ_38.setText("Code de fin");
            OBJ_38.setName("OBJ_38");
            P_SEL1.add(OBJ_38);
            OBJ_38.setBounds(15, 35, 81, 18);

            //---- EDEBA ----
            EDEBA.setComponentPopupMenu(BTD);
            EDEBA.setName("EDEBA");
            P_SEL1.add(EDEBA);
            EDEBA.setBounds(190, 5, 214, EDEBA.getPreferredSize().height);

            //---- EFINA ----
            EFINA.setComponentPopupMenu(BTD);
            EFINA.setName("EFINA");
            P_SEL1.add(EFINA);
            EFINA.setBounds(190, 30, 214, EFINA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL1.setMinimumSize(preferredSize);
              P_SEL1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL1);
          P_SEL1.setBounds(205, 220, 442, P_SEL1.getPreferredSize().height);

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });
          p_contenu.add(WTOU);
          WTOU.setBounds(new Rectangle(new Point(55, 241), WTOU.getPreferredSize()));

          //======== P_SEL5 ========
          {
            P_SEL5.setOpaque(false);
            P_SEL5.setName("P_SEL5");
            P_SEL5.setLayout(null);

            //---- OBJ_45 ----
            OBJ_45.setTitle("Codes magasins");
            OBJ_45.setName("OBJ_45");
            P_SEL5.add(OBJ_45);
            OBJ_45.setBounds(15, 5, 625, OBJ_45.getPreferredSize().height);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            P_SEL5.add(WTOUM);
            WTOUM.setBounds(30, 40, 135, WTOUM.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(7, 9, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(42, 9, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(77, 9, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(112, 9, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(147, 9, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(182, 9, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(217, 9, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(252, 9, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(287, 9, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(322, 9, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(357, 9, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(392, 9, 34, MA12.getPreferredSize().height);
            }
            P_SEL5.add(P_SEL0);
            P_SEL0.setBounds(185, 30, 442, 43);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL5.getComponentCount(); i++) {
                Rectangle bounds = P_SEL5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL5.setMinimumSize(preferredSize);
              P_SEL5.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL5);
          P_SEL5.setBounds(20, 120, 645, 80);

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- OBJ_36 ----
            OBJ_36.setText("Code famille d\u00e9but");
            OBJ_36.setName("OBJ_36");
            P_SEL2.add(OBJ_36);
            OBJ_36.setBounds(15, 10, 120, 18);

            //---- OBJ_33 ----
            OBJ_33.setText("Code famille fin");
            OBJ_33.setName("OBJ_33");
            P_SEL2.add(OBJ_33);
            OBJ_33.setBounds(15, 35, 90, 18);

            //---- EDEB ----
            EDEB.setComponentPopupMenu(BTD);
            EDEB.setName("EDEB");
            P_SEL2.add(EDEB);
            EDEB.setBounds(205, 5, 44, EDEB.getPreferredSize().height);

            //---- EFIN ----
            EFIN.setComponentPopupMenu(BTD);
            EFIN.setName("EFIN");
            P_SEL2.add(EFIN);
            EFIN.setBounds(205, 30, 44, EFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL2.getComponentCount(); i++) {
                Rectangle bounds = P_SEL2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL2.setMinimumSize(preferredSize);
              P_SEL2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL2);
          P_SEL2.setBounds(200, 225, 442, P_SEL2.getPreferredSize().height);

          //======== P_SEL3 ========
          {
            P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL3.setBackground(new Color(214, 217, 223));
            P_SEL3.setOpaque(false);
            P_SEL3.setName("P_SEL3");
            P_SEL3.setLayout(null);

            //---- DEBIAK ----
            DEBIAK.setComponentPopupMenu(BTD);
            DEBIAK.setName("DEBIAK");
            P_SEL3.add(DEBIAK);
            DEBIAK.setBounds(60, 15, 164, DEBIAK.getPreferredSize().height);

            //---- FINIAK ----
            FINIAK.setComponentPopupMenu(BTD);
            FINIAK.setName("FINIAK");
            P_SEL3.add(FINIAK);
            FINIAK.setBounds(270, 15, 164, FINIAK.getPreferredSize().height);

            //---- OBJ_40 ----
            OBJ_40.setText("De");
            OBJ_40.setName("OBJ_40");
            P_SEL3.add(OBJ_40);
            OBJ_40.setBounds(15, 20, 29, 18);

            //---- OBJ_35 ----
            OBJ_35.setText("\u00e0");
            OBJ_35.setName("OBJ_35");
            P_SEL3.add(OBJ_35);
            OBJ_35.setBounds(245, 20, 14, 19);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL3.getComponentCount(); i++) {
                Rectangle bounds = P_SEL3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL3.setMinimumSize(preferredSize);
              P_SEL3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL3);
          P_SEL3.setBounds(200, 220, 442, 55);

          //======== P_SEL4 ========
          {
            P_SEL4.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL4.setOpaque(false);
            P_SEL4.setName("P_SEL4");
            P_SEL4.setLayout(null);

            //---- FRSDEB ----
            FRSDEB.setComponentPopupMenu(BTD);
            FRSDEB.setName("FRSDEB");
            P_SEL4.add(FRSDEB);
            FRSDEB.setBounds(107, 15, 68, FRSDEB.getPreferredSize().height);

            //---- FRSFIN ----
            FRSFIN.setComponentPopupMenu(BTD);
            FRSFIN.setName("FRSFIN");
            P_SEL4.add(FRSFIN);
            FRSFIN.setBounds(300, 15, 68, FRSFIN.getPreferredSize().height);

            //---- OBJ_39 ----
            OBJ_39.setText("De");
            OBJ_39.setName("OBJ_39");
            P_SEL4.add(OBJ_39);
            OBJ_39.setBounds(15, 20, 24, 18);

            //---- OBJ_34 ----
            OBJ_34.setText("\u00e0");
            OBJ_34.setName("OBJ_34");
            P_SEL4.add(OBJ_34);
            OBJ_34.setBounds(235, 20, 17, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL4.getComponentCount(); i++) {
                Rectangle bounds = P_SEL4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL4.setMinimumSize(preferredSize);
              P_SEL4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL4);
          P_SEL4.setBounds(195, 220, 442, 60);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JPanel panel5;
  private JXTitledSeparator OBJ_29;
  private JXTitledSeparator OBJ_41;
  private JLabel OBJ_67;
  private XRiCalendrier DATFIN;
  private XRiCheckBox REPON4;
  private XRiCheckBox REPON1;
  private JXTitledSeparator OBJ_23;
  private JPanel P_SEL1;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private XRiTextField EDEBA;
  private XRiTextField EFINA;
  private XRiCheckBox WTOU;
  private JPanel P_SEL5;
  private JXTitledSeparator OBJ_45;
  private XRiCheckBox WTOUM;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPanel P_SEL2;
  private JLabel OBJ_36;
  private JLabel OBJ_33;
  private XRiTextField EDEB;
  private XRiTextField EFIN;
  private JPanel P_SEL3;
  private XRiTextField DEBIAK;
  private XRiTextField FINIAK;
  private JLabel OBJ_40;
  private JLabel OBJ_35;
  private JPanel P_SEL4;
  private XRiTextField FRSDEB;
  private XRiTextField FRSFIN;
  private JLabel OBJ_39;
  private JLabel OBJ_34;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
