
package ri.serien.libecranrpg.sgvx.SGVXCVFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class SGVXCVFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXCVFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Les établissements suivants doivent être purgés pour les factures de vente de @WAN@")).trim());
    WETB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB01@")).trim());
    WNBR01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR01@")).trim());
    WETB02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB02@")).trim());
    WNBR02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR02@")).trim());
    WETB03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB03@")).trim());
    WNBR03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR03@")).trim());
    WETB04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB04@")).trim());
    WNBR04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR04@")).trim());
    WETB05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB05@")).trim());
    WNBR05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR05@")).trim());
    WETB06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB06@")).trim());
    WNBR06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR06@")).trim());
    WETB07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB07@")).trim());
    WNBR07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR07@")).trim());
    WETB08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB08@")).trim());
    WNBR08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR08@")).trim());
    WETB09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB09@")).trim());
    WNBR09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR09@")).trim());
    WETB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB10@")).trim());
    WNBR10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR10@")).trim());
    WETB11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB11@")).trim());
    WNBR11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR11@")).trim());
    WETB12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB12@")).trim());
    WNBR12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR12@")).trim());
    WETB13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB13@")).trim());
    WNBR13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR13@")).trim());
    WETB14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB14@")).trim());
    WNBR14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR14@")).trim());
    WETB15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB15@")).trim());
    WNBR15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR15@")).trim());
    WETB16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB16@")).trim());
    WNBR16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR16@")).trim());
    WETB17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB17@")).trim());
    WNBR17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR17@")).trim());
    WETB18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB18@")).trim());
    WNBR18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR18@")).trim());
    WETB19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB19@")).trim());
    WNBR19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR19@")).trim());
    WETB20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB20@")).trim());
    WNBR20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR20@")).trim());
    WETB21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB21@")).trim());
    WNBR21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR21@")).trim());
    WETB22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB22@")).trim());
    WNBR22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR22@")).trim());
    WETB23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB23@")).trim());
    WNBR23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR23@")).trim());
    WETB24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB24@")).trim());
    WNBR24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR24@")).trim());
    WETB25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB25@")).trim());
    WNBR25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR25@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix multi-paramètres"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    WETB01 = new RiZoneSortie();
    WNBR01 = new RiZoneSortie();
    WETB02 = new RiZoneSortie();
    WNBR02 = new RiZoneSortie();
    WETB03 = new RiZoneSortie();
    WNBR03 = new RiZoneSortie();
    WETB04 = new RiZoneSortie();
    WNBR04 = new RiZoneSortie();
    WETB05 = new RiZoneSortie();
    WNBR05 = new RiZoneSortie();
    WETB06 = new RiZoneSortie();
    WNBR06 = new RiZoneSortie();
    WETB07 = new RiZoneSortie();
    WNBR07 = new RiZoneSortie();
    WETB08 = new RiZoneSortie();
    WNBR08 = new RiZoneSortie();
    WETB09 = new RiZoneSortie();
    WNBR09 = new RiZoneSortie();
    WETB10 = new RiZoneSortie();
    WNBR10 = new RiZoneSortie();
    WETB11 = new RiZoneSortie();
    WNBR11 = new RiZoneSortie();
    WETB12 = new RiZoneSortie();
    WNBR12 = new RiZoneSortie();
    WETB13 = new RiZoneSortie();
    WNBR13 = new RiZoneSortie();
    WETB14 = new RiZoneSortie();
    WNBR14 = new RiZoneSortie();
    WETB15 = new RiZoneSortie();
    WNBR15 = new RiZoneSortie();
    WETB16 = new RiZoneSortie();
    WNBR16 = new RiZoneSortie();
    WETB17 = new RiZoneSortie();
    WNBR17 = new RiZoneSortie();
    WETB18 = new RiZoneSortie();
    WNBR18 = new RiZoneSortie();
    WETB19 = new RiZoneSortie();
    WNBR19 = new RiZoneSortie();
    WETB20 = new RiZoneSortie();
    WNBR20 = new RiZoneSortie();
    WETB21 = new RiZoneSortie();
    WNBR21 = new RiZoneSortie();
    WETB22 = new RiZoneSortie();
    WNBR22 = new RiZoneSortie();
    WETB23 = new RiZoneSortie();
    WNBR23 = new RiZoneSortie();
    WETB24 = new RiZoneSortie();
    WNBR24 = new RiZoneSortie();
    WETB25 = new RiZoneSortie();
    WNBR25 = new RiZoneSortie();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(920, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label1 ----
          label1.setText("ATTENTION");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setBackground(Color.red);
          label1.setForeground(Color.red);
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(10, 10, 710, 21);

          //---- label2 ----
          label2.setText("Les \u00e9tablissements suivants doivent \u00eatre purg\u00e9s pour les factures de vente de @WAN@");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 3f));
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(10, 50, 710, 21);

          //---- WETB01 ----
          WETB01.setText("@WETB01@");
          WETB01.setName("WETB01");
          panel1.add(WETB01);
          WETB01.setBounds(35, 110, 40, WETB01.getPreferredSize().height);

          //---- WNBR01 ----
          WNBR01.setText("@WNBR01@");
          WNBR01.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR01.setName("WNBR01");
          panel1.add(WNBR01);
          WNBR01.setBounds(80, 110, 70, WNBR01.getPreferredSize().height);

          //---- WETB02 ----
          WETB02.setText("@WETB02@");
          WETB02.setName("WETB02");
          panel1.add(WETB02);
          WETB02.setBounds(215, 110, 40, WETB02.getPreferredSize().height);

          //---- WNBR02 ----
          WNBR02.setText("@WNBR02@");
          WNBR02.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR02.setName("WNBR02");
          panel1.add(WNBR02);
          WNBR02.setBounds(263, 110, 70, WNBR02.getPreferredSize().height);

          //---- WETB03 ----
          WETB03.setText("@WETB03@");
          WETB03.setName("WETB03");
          panel1.add(WETB03);
          WETB03.setBounds(400, 110, 40, WETB03.getPreferredSize().height);

          //---- WNBR03 ----
          WNBR03.setText("@WNBR03@");
          WNBR03.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR03.setName("WNBR03");
          panel1.add(WNBR03);
          WNBR03.setBounds(446, 110, 70, WNBR03.getPreferredSize().height);

          //---- WETB04 ----
          WETB04.setText("@WETB04@");
          WETB04.setName("WETB04");
          panel1.add(WETB04);
          WETB04.setBounds(585, 110, 40, WETB04.getPreferredSize().height);

          //---- WNBR04 ----
          WNBR04.setText("@WNBR04@");
          WNBR04.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR04.setName("WNBR04");
          panel1.add(WNBR04);
          WNBR04.setBounds(629, 110, 70, WNBR04.getPreferredSize().height);

          //---- WETB05 ----
          WETB05.setText("@WETB05@");
          WETB05.setName("WETB05");
          panel1.add(WETB05);
          WETB05.setBounds(35, 140, 40, WETB05.getPreferredSize().height);

          //---- WNBR05 ----
          WNBR05.setText("@WNBR05@");
          WNBR05.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR05.setName("WNBR05");
          panel1.add(WNBR05);
          WNBR05.setBounds(80, 140, 70, WNBR05.getPreferredSize().height);

          //---- WETB06 ----
          WETB06.setText("@WETB06@");
          WETB06.setName("WETB06");
          panel1.add(WETB06);
          WETB06.setBounds(215, 140, 40, WETB06.getPreferredSize().height);

          //---- WNBR06 ----
          WNBR06.setText("@WNBR06@");
          WNBR06.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR06.setName("WNBR06");
          panel1.add(WNBR06);
          WNBR06.setBounds(260, 140, 70, WNBR06.getPreferredSize().height);

          //---- WETB07 ----
          WETB07.setText("@WETB07@");
          WETB07.setName("WETB07");
          panel1.add(WETB07);
          WETB07.setBounds(400, 140, 40, WETB07.getPreferredSize().height);

          //---- WNBR07 ----
          WNBR07.setText("@WNBR07@");
          WNBR07.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR07.setName("WNBR07");
          panel1.add(WNBR07);
          WNBR07.setBounds(445, 140, 70, WNBR07.getPreferredSize().height);

          //---- WETB08 ----
          WETB08.setText("@WETB08@");
          WETB08.setName("WETB08");
          panel1.add(WETB08);
          WETB08.setBounds(585, 140, 40, WETB08.getPreferredSize().height);

          //---- WNBR08 ----
          WNBR08.setText("@WNBR08@");
          WNBR08.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR08.setName("WNBR08");
          panel1.add(WNBR08);
          WNBR08.setBounds(630, 140, 70, WNBR08.getPreferredSize().height);

          //---- WETB09 ----
          WETB09.setText("@WETB09@");
          WETB09.setName("WETB09");
          panel1.add(WETB09);
          WETB09.setBounds(35, 170, 40, WETB09.getPreferredSize().height);

          //---- WNBR09 ----
          WNBR09.setText("@WNBR09@");
          WNBR09.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR09.setName("WNBR09");
          panel1.add(WNBR09);
          WNBR09.setBounds(80, 170, 70, WNBR09.getPreferredSize().height);

          //---- WETB10 ----
          WETB10.setText("@WETB10@");
          WETB10.setName("WETB10");
          panel1.add(WETB10);
          WETB10.setBounds(215, 170, 40, WETB10.getPreferredSize().height);

          //---- WNBR10 ----
          WNBR10.setText("@WNBR10@");
          WNBR10.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR10.setName("WNBR10");
          panel1.add(WNBR10);
          WNBR10.setBounds(260, 170, 70, WNBR10.getPreferredSize().height);

          //---- WETB11 ----
          WETB11.setText("@WETB11@");
          WETB11.setName("WETB11");
          panel1.add(WETB11);
          WETB11.setBounds(400, 170, 40, WETB11.getPreferredSize().height);

          //---- WNBR11 ----
          WNBR11.setText("@WNBR11@");
          WNBR11.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR11.setName("WNBR11");
          panel1.add(WNBR11);
          WNBR11.setBounds(445, 170, 70, WNBR11.getPreferredSize().height);

          //---- WETB12 ----
          WETB12.setText("@WETB12@");
          WETB12.setName("WETB12");
          panel1.add(WETB12);
          WETB12.setBounds(585, 170, 40, WETB12.getPreferredSize().height);

          //---- WNBR12 ----
          WNBR12.setText("@WNBR12@");
          WNBR12.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR12.setName("WNBR12");
          panel1.add(WNBR12);
          WNBR12.setBounds(630, 170, 70, WNBR12.getPreferredSize().height);

          //---- WETB13 ----
          WETB13.setText("@WETB13@");
          WETB13.setName("WETB13");
          panel1.add(WETB13);
          WETB13.setBounds(35, 200, 40, WETB13.getPreferredSize().height);

          //---- WNBR13 ----
          WNBR13.setText("@WNBR13@");
          WNBR13.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR13.setName("WNBR13");
          panel1.add(WNBR13);
          WNBR13.setBounds(80, 200, 70, WNBR13.getPreferredSize().height);

          //---- WETB14 ----
          WETB14.setText("@WETB14@");
          WETB14.setName("WETB14");
          panel1.add(WETB14);
          WETB14.setBounds(215, 200, 40, WETB14.getPreferredSize().height);

          //---- WNBR14 ----
          WNBR14.setText("@WNBR14@");
          WNBR14.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR14.setName("WNBR14");
          panel1.add(WNBR14);
          WNBR14.setBounds(260, 200, 70, WNBR14.getPreferredSize().height);

          //---- WETB15 ----
          WETB15.setText("@WETB15@");
          WETB15.setName("WETB15");
          panel1.add(WETB15);
          WETB15.setBounds(400, 200, 40, WETB15.getPreferredSize().height);

          //---- WNBR15 ----
          WNBR15.setText("@WNBR15@");
          WNBR15.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR15.setName("WNBR15");
          panel1.add(WNBR15);
          WNBR15.setBounds(445, 200, 70, WNBR15.getPreferredSize().height);

          //---- WETB16 ----
          WETB16.setText("@WETB16@");
          WETB16.setName("WETB16");
          panel1.add(WETB16);
          WETB16.setBounds(585, 200, 40, WETB16.getPreferredSize().height);

          //---- WNBR16 ----
          WNBR16.setText("@WNBR16@");
          WNBR16.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR16.setName("WNBR16");
          panel1.add(WNBR16);
          WNBR16.setBounds(630, 200, 70, WNBR16.getPreferredSize().height);

          //---- WETB17 ----
          WETB17.setText("@WETB17@");
          WETB17.setName("WETB17");
          panel1.add(WETB17);
          WETB17.setBounds(35, 230, 40, WETB17.getPreferredSize().height);

          //---- WNBR17 ----
          WNBR17.setText("@WNBR17@");
          WNBR17.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR17.setName("WNBR17");
          panel1.add(WNBR17);
          WNBR17.setBounds(80, 230, 70, WNBR17.getPreferredSize().height);

          //---- WETB18 ----
          WETB18.setText("@WETB18@");
          WETB18.setName("WETB18");
          panel1.add(WETB18);
          WETB18.setBounds(215, 230, 40, WETB18.getPreferredSize().height);

          //---- WNBR18 ----
          WNBR18.setText("@WNBR18@");
          WNBR18.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR18.setName("WNBR18");
          panel1.add(WNBR18);
          WNBR18.setBounds(260, 230, 70, WNBR18.getPreferredSize().height);

          //---- WETB19 ----
          WETB19.setText("@WETB19@");
          WETB19.setName("WETB19");
          panel1.add(WETB19);
          WETB19.setBounds(400, 230, 40, WETB19.getPreferredSize().height);

          //---- WNBR19 ----
          WNBR19.setText("@WNBR19@");
          WNBR19.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR19.setName("WNBR19");
          panel1.add(WNBR19);
          WNBR19.setBounds(445, 230, 70, WNBR19.getPreferredSize().height);

          //---- WETB20 ----
          WETB20.setText("@WETB20@");
          WETB20.setName("WETB20");
          panel1.add(WETB20);
          WETB20.setBounds(585, 230, 40, WETB20.getPreferredSize().height);

          //---- WNBR20 ----
          WNBR20.setText("@WNBR20@");
          WNBR20.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR20.setName("WNBR20");
          panel1.add(WNBR20);
          WNBR20.setBounds(630, 230, 70, WNBR20.getPreferredSize().height);

          //---- WETB21 ----
          WETB21.setText("@WETB21@");
          WETB21.setName("WETB21");
          panel1.add(WETB21);
          WETB21.setBounds(35, 260, 40, WETB21.getPreferredSize().height);

          //---- WNBR21 ----
          WNBR21.setText("@WNBR21@");
          WNBR21.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR21.setName("WNBR21");
          panel1.add(WNBR21);
          WNBR21.setBounds(80, 260, 70, WNBR21.getPreferredSize().height);

          //---- WETB22 ----
          WETB22.setText("@WETB22@");
          WETB22.setName("WETB22");
          panel1.add(WETB22);
          WETB22.setBounds(215, 260, 40, WETB22.getPreferredSize().height);

          //---- WNBR22 ----
          WNBR22.setText("@WNBR22@");
          WNBR22.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR22.setName("WNBR22");
          panel1.add(WNBR22);
          WNBR22.setBounds(260, 260, 70, WNBR22.getPreferredSize().height);

          //---- WETB23 ----
          WETB23.setText("@WETB23@");
          WETB23.setName("WETB23");
          panel1.add(WETB23);
          WETB23.setBounds(400, 260, 40, WETB23.getPreferredSize().height);

          //---- WNBR23 ----
          WNBR23.setText("@WNBR23@");
          WNBR23.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR23.setName("WNBR23");
          panel1.add(WNBR23);
          WNBR23.setBounds(445, 260, 70, WNBR23.getPreferredSize().height);

          //---- WETB24 ----
          WETB24.setText("@WETB24@");
          WETB24.setName("WETB24");
          panel1.add(WETB24);
          WETB24.setBounds(585, 260, 40, WETB24.getPreferredSize().height);

          //---- WNBR24 ----
          WNBR24.setText("@WNBR24@");
          WNBR24.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR24.setName("WNBR24");
          panel1.add(WNBR24);
          WNBR24.setBounds(630, 260, 70, WNBR24.getPreferredSize().height);

          //---- WETB25 ----
          WETB25.setText("@WETB25@");
          WETB25.setName("WETB25");
          panel1.add(WETB25);
          WETB25.setBounds(35, 290, 40, WETB25.getPreferredSize().height);

          //---- WNBR25 ----
          WNBR25.setText("@WNBR25@");
          WNBR25.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR25.setName("WNBR25");
          panel1.add(WNBR25);
          WNBR25.setBounds(80, 290, 70, WNBR25.getPreferredSize().height);

          //---- label3 ----
          label3.setText("Etb.");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(35, 90, 40, label3.getPreferredSize().height);

          //---- label4 ----
          label4.setText("Etb.");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(215, 90, 40, label4.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Etb.");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(400, 90, 40, label5.getPreferredSize().height);

          //---- label6 ----
          label6.setText("Etb.");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(585, 90, 40, label6.getPreferredSize().height);

          //---- label7 ----
          label7.setText("Nombre");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(80, 90, 70, label7.getPreferredSize().height);

          //---- label8 ----
          label8.setText("Nombre");
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(265, 90, 70, label8.getPreferredSize().height);

          //---- label9 ----
          label9.setText("Nombre");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(445, 90, 70, label9.getPreferredSize().height);

          //---- label10 ----
          label10.setText("Nombre");
          label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setName("label10");
          panel1.add(label10);
          label10.setBounds(630, 90, 70, label10.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private RiZoneSortie WETB01;
  private RiZoneSortie WNBR01;
  private RiZoneSortie WETB02;
  private RiZoneSortie WNBR02;
  private RiZoneSortie WETB03;
  private RiZoneSortie WNBR03;
  private RiZoneSortie WETB04;
  private RiZoneSortie WNBR04;
  private RiZoneSortie WETB05;
  private RiZoneSortie WNBR05;
  private RiZoneSortie WETB06;
  private RiZoneSortie WNBR06;
  private RiZoneSortie WETB07;
  private RiZoneSortie WNBR07;
  private RiZoneSortie WETB08;
  private RiZoneSortie WNBR08;
  private RiZoneSortie WETB09;
  private RiZoneSortie WNBR09;
  private RiZoneSortie WETB10;
  private RiZoneSortie WNBR10;
  private RiZoneSortie WETB11;
  private RiZoneSortie WNBR11;
  private RiZoneSortie WETB12;
  private RiZoneSortie WNBR12;
  private RiZoneSortie WETB13;
  private RiZoneSortie WNBR13;
  private RiZoneSortie WETB14;
  private RiZoneSortie WNBR14;
  private RiZoneSortie WETB15;
  private RiZoneSortie WNBR15;
  private RiZoneSortie WETB16;
  private RiZoneSortie WNBR16;
  private RiZoneSortie WETB17;
  private RiZoneSortie WNBR17;
  private RiZoneSortie WETB18;
  private RiZoneSortie WNBR18;
  private RiZoneSortie WETB19;
  private RiZoneSortie WNBR19;
  private RiZoneSortie WETB20;
  private RiZoneSortie WNBR20;
  private RiZoneSortie WETB21;
  private RiZoneSortie WNBR21;
  private RiZoneSortie WETB22;
  private RiZoneSortie WNBR22;
  private RiZoneSortie WETB23;
  private RiZoneSortie WNBR23;
  private RiZoneSortie WETB24;
  private RiZoneSortie WNBR24;
  private RiZoneSortie WETB25;
  private RiZoneSortie WNBR25;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
