
package ri.serien.libecranrpg.sgvx.SGVX8AFM;
// Nom Fichier: pop_SGVX8AFM_FMTD2_644.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVX8AFM_D2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX8AFM_D2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WMO4.setEnabled(lexique.isPresent("WMO4"));
    WMO3.setEnabled(lexique.isPresent("WMO3"));
    WMO2.setEnabled(lexique.isPresent("WMO2"));
    WMO1.setEnabled(lexique.isPresent("WMO1"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_27.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvx8a"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel3 = new JPanel();
    panel2 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_27 = new JButton();
    panel4 = new JPanel();
    panel1 = new JPanel();
    OBJ_20 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_18 = new JLabel();
    WMO1 = new XRiTextField();
    WMO2 = new XRiTextField();
    WMO3 = new XRiTextField();
    WMO4 = new XRiTextField();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_17 = new JLabel();

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== panel3 ========
    {
      panel3.setName("panel3");
      panel3.setLayout(null);

      //======== panel2 ========
      {
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("Ok");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_26ActionPerformed(e);
          }
        });
        panel2.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        //---- OBJ_27 ----
        OBJ_27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_27.setToolTipText("Retour");
        OBJ_27.setName("OBJ_27");
        OBJ_27.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_27ActionPerformed(e);
          }
        });
        panel2.add(OBJ_27);
        OBJ_27.setBounds(60, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      panel3.add(panel2);
      panel2.setBounds(280, 0, 120, 50);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel3.getComponentCount(); i++) {
          Rectangle bounds = panel3.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel3.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel3.setMinimumSize(preferredSize);
        panel3.setPreferredSize(preferredSize);
      }
    }
    add(panel3, BorderLayout.SOUTH);

    //======== panel4 ========
    {
      panel4.setName("panel4");
      panel4.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Scan libell\u00e9s"));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_20 ----
        OBJ_20.setText("Scan sur 2\u00e8me d\u00e9signation");
        OBJ_20.setName("OBJ_20");
        panel1.add(OBJ_20);
        OBJ_20.setBounds(15, 62, 165, 20);

        //---- OBJ_22 ----
        OBJ_22.setText("Scan sur 3\u00e8me d\u00e9signation");
        OBJ_22.setName("OBJ_22");
        panel1.add(OBJ_22);
        OBJ_22.setBounds(15, 90, 165, 20);

        //---- OBJ_24 ----
        OBJ_24.setText("Scan sur 4\u00e8me d\u00e9signation");
        OBJ_24.setName("OBJ_24");
        panel1.add(OBJ_24);
        OBJ_24.setBounds(15, 118, 165, 20);

        //---- OBJ_18 ----
        OBJ_18.setText("Scan sur 1\u00e8re d\u00e9signation");
        OBJ_18.setName("OBJ_18");
        panel1.add(OBJ_18);
        OBJ_18.setBounds(15, 34, 158, 20);

        //---- WMO1 ----
        WMO1.setComponentPopupMenu(BTD);
        WMO1.setName("WMO1");
        panel1.add(WMO1);
        WMO1.setBounds(205, 30, 160, WMO1.getPreferredSize().height);

        //---- WMO2 ----
        WMO2.setComponentPopupMenu(BTD);
        WMO2.setName("WMO2");
        panel1.add(WMO2);
        WMO2.setBounds(205, 58, 160, WMO2.getPreferredSize().height);

        //---- WMO3 ----
        WMO3.setComponentPopupMenu(BTD);
        WMO3.setName("WMO3");
        panel1.add(WMO3);
        WMO3.setBounds(205, 86, 160, WMO3.getPreferredSize().height);

        //---- WMO4 ----
        WMO4.setComponentPopupMenu(BTD);
        WMO4.setName("WMO4");
        panel1.add(WMO4);
        WMO4.setBounds(205, 114, 160, WMO4.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      panel4.add(panel1);
      panel1.setBounds(5, 5, 390, 155);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel4.getComponentCount(); i++) {
          Rectangle bounds = panel4.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel4.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel4.setMinimumSize(preferredSize);
        panel4.setPreferredSize(preferredSize);
      }
    }
    add(panel4, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_17 ----
    OBJ_17.setText("Scan libell\u00e9s");
    OBJ_17.setName("OBJ_17");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel3;
  private JPanel panel2;
  private JButton BT_ENTER;
  private JButton OBJ_27;
  private JPanel panel4;
  private JPanel panel1;
  private JLabel OBJ_20;
  private JLabel OBJ_22;
  private JLabel OBJ_24;
  private JLabel OBJ_18;
  private XRiTextField WMO1;
  private XRiTextField WMO2;
  private XRiTextField WMO3;
  private XRiTextField WMO4;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JLabel OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
