
package ri.serien.libecranrpg.sgvx.SGVXA18F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVXA18F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVXA18F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TYP5.setValeursSelection("1", " ");
    TYP4.setValeursSelection("1", " ");
    TYP3.setValeursSelection("1", " ");
    TYP2.setValeursSelection("1", " ");
    TYP1.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    TYP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@1@ @LST1@")).trim());
    TYP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@2@ @LST2@")).trim());
    TYP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@3@ @LST3@")).trim());
    TYP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@4@ @LST4@")).trim());
    TYP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@5@ @LST5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    


    
    
    ADF4.setEnabled(lexique.isPresent("ADF4"));
    ADF3.setEnabled(lexique.isPresent("ADF3"));
    ADF2.setEnabled(lexique.isPresent("ADF2"));
    ADF1.setEnabled(lexique.isPresent("ADF1"));
    ADD4.setEnabled(lexique.isPresent("ADD4"));
    ADD3.setEnabled(lexique.isPresent("ADD3"));
    ADD2.setEnabled(lexique.isPresent("ADD2"));
    ADD1.setEnabled(lexique.isPresent("ADD1"));
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Renseigner le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // Renseigner l'article de débutWMAG
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "ARTDEB");
    
    // Renseigner l'article de fin
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "ARTFIN");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snArticleDebut.renseignerChampRPG(lexique, "ARTDEB");
    snArticleFin.renseignerChampRPG(lexique, "ARTFIN");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F10");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      snMagasin.charger(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_contenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    z_wencx_ = new XRiTextField();
    pnlAdressesStockage = new SNPanelTitre();
    lbAdressesDebut = new SNLabelChamp();
    lbAdressesFin = new SNLabelChamp();
    ADD1 = new XRiTextField();
    ADD2 = new XRiTextField();
    ADD3 = new XRiTextField();
    ADD4 = new XRiTextField();
    ADF1 = new XRiTextField();
    ADF2 = new XRiTextField();
    ADF3 = new XRiTextField();
    ADF4 = new XRiTextField();
    pnlMagasin = new SNPanelTitre();
    lbWMAG = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbPanelAdressesStockage = new SNLabelTitre();
    lbPanelArticles = new SNLabelTitre();
    pnlArticles = new SNPanelTitre();
    lbArticleDebut = new SNLabelChamp();
    lbArticleFin = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    snArticleFin = new SNArticle();
    lbPanelTypeZones = new SNLabelTitre();
    pnlTypeZones = new SNPanelTitre();
    TYP1 = new XRiCheckBox();
    TYP2 = new XRiCheckBox();
    TYP3 = new XRiCheckBox();
    TYP4 = new XRiCheckBox();
    TYP5 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(239, 239, 222));
        p_contenu.setOpaque(true);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setOpaque(false);
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setMinimumSize(new Dimension(400, 30));
          snEtablissement.setPreferredSize(new Dimension(400, 30));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 10), 0, 0));

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setPreferredSize(new Dimension(220, 30));
          z_wencx_.setMinimumSize(new Dimension(220, 30));
          z_wencx_.setFont(new Font("sansserif", Font.PLAIN, 14));
          z_wencx_.setEnabled(false);
          z_wencx_.setName("z_wencx_");
          pnlEtablissement.add(z_wencx_, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlAdressesStockage ========
        {
          pnlAdressesStockage.setName("pnlAdressesStockage");
          pnlAdressesStockage.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlAdressesStockage.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlAdressesStockage.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlAdressesStockage.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlAdressesStockage.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbAdressesDebut ----
          lbAdressesDebut.setText("Adresse de d\u00e9but");
          lbAdressesDebut.setName("lbAdressesDebut");
          pnlAdressesStockage.add(lbAdressesDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbAdressesFin ----
          lbAdressesFin.setText("Adresse de fin");
          lbAdressesFin.setName("lbAdressesFin");
          pnlAdressesStockage.add(lbAdressesFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- ADD1 ----
          ADD1.setComponentPopupMenu(BTD);
          ADD1.setPreferredSize(new Dimension(50, 30));
          ADD1.setMinimumSize(new Dimension(50, 30));
          ADD1.setName("ADD1");
          pnlAdressesStockage.add(ADD1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ADD2 ----
          ADD2.setComponentPopupMenu(BTD);
          ADD2.setPreferredSize(new Dimension(50, 30));
          ADD2.setMinimumSize(new Dimension(50, 30));
          ADD2.setName("ADD2");
          pnlAdressesStockage.add(ADD2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ADD3 ----
          ADD3.setComponentPopupMenu(BTD);
          ADD3.setPreferredSize(new Dimension(50, 30));
          ADD3.setMinimumSize(new Dimension(50, 30));
          ADD3.setName("ADD3");
          pnlAdressesStockage.add(ADD3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ADD4 ----
          ADD4.setComponentPopupMenu(BTD);
          ADD4.setPreferredSize(new Dimension(50, 30));
          ADD4.setMinimumSize(new Dimension(50, 30));
          ADD4.setName("ADD4");
          pnlAdressesStockage.add(ADD4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ADF1 ----
          ADF1.setComponentPopupMenu(BTD);
          ADF1.setPreferredSize(new Dimension(50, 30));
          ADF1.setMinimumSize(new Dimension(50, 30));
          ADF1.setName("ADF1");
          pnlAdressesStockage.add(ADF1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- ADF2 ----
          ADF2.setComponentPopupMenu(BTD);
          ADF2.setPreferredSize(new Dimension(50, 30));
          ADF2.setMinimumSize(new Dimension(50, 30));
          ADF2.setName("ADF2");
          pnlAdressesStockage.add(ADF2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- ADF3 ----
          ADF3.setComponentPopupMenu(BTD);
          ADF3.setPreferredSize(new Dimension(50, 30));
          ADF3.setMinimumSize(new Dimension(50, 30));
          ADF3.setName("ADF3");
          pnlAdressesStockage.add(ADF3, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- ADF4 ----
          ADF4.setComponentPopupMenu(BTD);
          ADF4.setPreferredSize(new Dimension(50, 30));
          ADF4.setMinimumSize(new Dimension(50, 30));
          ADF4.setName("ADF4");
          pnlAdressesStockage.add(ADF4, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(pnlAdressesStockage, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlMagasin ========
        {
          pnlMagasin.setOpaque(false);
          pnlMagasin.setName("pnlMagasin");
          pnlMagasin.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlMagasin.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlMagasin.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlMagasin.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlMagasin.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbWMAG ----
          lbWMAG.setText("Magasin \u00e0 traiter");
          lbWMAG.setName("lbWMAG");
          pnlMagasin.add(lbWMAG, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasin ----
          snMagasin.setName("snMagasin");
          pnlMagasin.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        p_contenu.add(pnlMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- lbPanelAdressesStockage ----
        lbPanelAdressesStockage.setText("Adresse de stockage \u00e0 s\u00e9lectionner");
        lbPanelAdressesStockage.setName("lbPanelAdressesStockage");
        p_contenu.add(lbPanelAdressesStockage, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- lbPanelArticles ----
        lbPanelArticles.setText("Plage article");
        lbPanelArticles.setName("lbPanelArticles");
        p_contenu.add(lbPanelArticles, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlArticles ========
        {
          pnlArticles.setName("pnlArticles");
          pnlArticles.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlArticles.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlArticles.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlArticles.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlArticles.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbArticleDebut ----
          lbArticleDebut.setText("Article d\u00e9but");
          lbArticleDebut.setName("lbArticleDebut");
          pnlArticles.add(lbArticleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbArticleFin ----
          lbArticleFin.setText("Article fin");
          lbArticleFin.setName("lbArticleFin");
          pnlArticles.add(lbArticleFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snArticleDebut ----
          snArticleDebut.setName("snArticleDebut");
          pnlArticles.add(snArticleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- snArticleFin ----
          snArticleFin.setName("snArticleFin");
          pnlArticles.add(snArticleFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(pnlArticles, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- lbPanelTypeZones ----
        lbPanelTypeZones.setText("Type de zones \u00e0 \u00e9diter");
        lbPanelTypeZones.setName("lbPanelTypeZones");
        p_contenu.add(lbPanelTypeZones, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlTypeZones ========
        {
          pnlTypeZones.setName("pnlTypeZones");
          pnlTypeZones.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlTypeZones.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlTypeZones.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlTypeZones.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlTypeZones.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- TYP1 ----
          TYP1.setText("@1@ @LST1@");
          TYP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYP1.setMinimumSize(new Dimension(150, 30));
          TYP1.setPreferredSize(new Dimension(150, 30));
          TYP1.setFont(new Font("sansserif", Font.PLAIN, 14));
          TYP1.setMaximumSize(new Dimension(200, 30));
          TYP1.setName("TYP1");
          pnlTypeZones.add(TYP1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TYP2 ----
          TYP2.setText("@2@ @LST2@");
          TYP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYP2.setMinimumSize(new Dimension(150, 30));
          TYP2.setPreferredSize(new Dimension(150, 30));
          TYP2.setFont(new Font("sansserif", Font.PLAIN, 14));
          TYP2.setMaximumSize(new Dimension(200, 30));
          TYP2.setName("TYP2");
          pnlTypeZones.add(TYP2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- TYP3 ----
          TYP3.setText("@3@ @LST3@");
          TYP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYP3.setMinimumSize(new Dimension(150, 30));
          TYP3.setPreferredSize(new Dimension(150, 30));
          TYP3.setFont(new Font("sansserif", Font.PLAIN, 14));
          TYP3.setMaximumSize(new Dimension(200, 30));
          TYP3.setName("TYP3");
          pnlTypeZones.add(TYP3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TYP4 ----
          TYP4.setText("@4@ @LST4@");
          TYP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYP4.setMinimumSize(new Dimension(150, 30));
          TYP4.setPreferredSize(new Dimension(150, 30));
          TYP4.setFont(new Font("sansserif", Font.PLAIN, 14));
          TYP4.setMaximumSize(new Dimension(200, 30));
          TYP4.setName("TYP4");
          pnlTypeZones.add(TYP4, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- TYP5 ----
          TYP5.setText("@5@ @LST5@");
          TYP5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TYP5.setMinimumSize(new Dimension(150, 30));
          TYP5.setPreferredSize(new Dimension(150, 30));
          TYP5.setFont(new Font("sansserif", Font.PLAIN, 14));
          TYP5.setMaximumSize(new Dimension(200, 30));
          TYP5.setName("TYP5");
          pnlTypeZones.add(TYP5, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        }
        p_contenu.add(pnlTypeZones, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelContenu p_contenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private XRiTextField z_wencx_;
  private SNPanelTitre pnlAdressesStockage;
  private SNLabelChamp lbAdressesDebut;
  private SNLabelChamp lbAdressesFin;
  private XRiTextField ADD1;
  private XRiTextField ADD2;
  private XRiTextField ADD3;
  private XRiTextField ADD4;
  private XRiTextField ADF1;
  private XRiTextField ADF2;
  private XRiTextField ADF3;
  private XRiTextField ADF4;
  private SNPanelTitre pnlMagasin;
  private SNLabelChamp lbWMAG;
  private SNMagasin snMagasin;
  private SNLabelTitre lbPanelAdressesStockage;
  private SNLabelTitre lbPanelArticles;
  private SNPanelTitre pnlArticles;
  private SNLabelChamp lbArticleDebut;
  private SNLabelChamp lbArticleFin;
  private SNArticle snArticleDebut;
  private SNArticle snArticleFin;
  private SNLabelTitre lbPanelTypeZones;
  private SNPanelTitre pnlTypeZones;
  private XRiCheckBox TYP1;
  private XRiCheckBox TYP2;
  private XRiCheckBox TYP3;
  private XRiCheckBox TYP4;
  private XRiCheckBox TYP5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
