
package ri.serien.libecranrpg.sgvx.SGVX5FFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX5FFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TYPREA_Value = { " ", "0", "1", "2", "3", "4", "5", "6", "7", "8" };
  
  public SGVX5FFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    TYPREA.setValeurs(TYPREA_Value, null);
    DCDE.setValeursSelection("1", "");
    DATT.setValeursSelection("1", "");
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    if (WMAG.getText().trim().equalsIgnoreCase("**")) {
      WMAG.setText("");
    }
    else {
      WMAG.setText("**");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_51 = new JXTitledSeparator();
    OBJ_53 = new JXTitledSeparator();
    WMAG = new XRiTextField();
    WTOUM = new XRiCheckBox();
    OBJ_81 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    OBJ_83 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    CLIDEB = new XRiTextField();
    CLIFIN = new XRiTextField();
    FRSDEB = new XRiTextField();
    FRSFIN = new XRiTextField();
    TYPREA = new XRiComboBox();
    NUMZP = new XRiTextField();
    ZPDEB = new XRiTextField();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_94 = new JLabel();
    TOP1 = new XRiRadioButton();
    TOP2 = new XRiRadioButton();
    TOP3 = new XRiRadioButton();
    TOP4 = new XRiRadioButton();
    TOP5 = new XRiRadioButton();
    TOP6 = new XRiRadioButton();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    QTDIS = new XRiTextField();
    MOIDIS = new XRiTextField();
    MINDIS = new XRiTextField();
    OBJ_101 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    DATT = new XRiCheckBox();
    OBJ_54 = new JXTitledSeparator();
    OBJ_93 = new JLabel();
    DATDEB2 = new XRiCalendrier();
    OBJ_105 = new JLabel();
    DATFIN2 = new XRiCalendrier();
    DCDE = new XRiCheckBox();
    OBJ_55 = new JXTitledSeparator();
    OBJ_106 = new JLabel();
    OBJ_56 = new JXTitledSeparator();
    OBJ_107 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_112 = new JLabel();
    ZPFIN = new XRiTextField();
    OBJ_113 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel1.add(sep_etablissement);
            sep_etablissement.setBounds(10, 10, 730, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel1.add(z_dgnom_);
            z_dgnom_.setBounds(190, 40, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel1.add(z_wencx_);
            z_wencx_.setBounds(190, 70, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel1.add(z_etablissement_);
            z_etablissement_.setBounds(35, 57, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel1.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(80, 55), bouton_etablissement.getPreferredSize()));

            //---- OBJ_51 ----
            OBJ_51.setTitle("Article");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(10, 115, 730, OBJ_51.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setTitle("Disponible");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(10, 260, 730, OBJ_53.getPreferredSize().height);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            panel1.add(WMAG);
            WMAG.setBounds(225, 135, 34, WMAG.getPreferredSize().height);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            panel1.add(WTOUM);
            WTOUM.setBounds(310, 139, 150, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Dates de livraison");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(380, 460, 120, 20);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(510, 456, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(650, 456, 105, DATFIN.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("Fournisseurs");
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(60, 405, 90, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("Clients");
            OBJ_84.setName("OBJ_84");
            panel1.add(OBJ_84);
            OBJ_84.setBounds(60, 460, 90, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("Familles");
            OBJ_85.setName("OBJ_85");
            panel1.add(OBJ_85);
            OBJ_85.setBounds(35, 169, 155, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Articles");
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(35, 199, 155, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("Type de r\u00e9approvisionnement");
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(35, 228, 180, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("Num\u00e9ro de ZP ligne");
            OBJ_88.setName("OBJ_88");
            panel1.add(OBJ_88);
            OBJ_88.setBounds(35, 520, 200, 20);

            //---- ARTDEB ----
            ARTDEB.setComponentPopupMenu(BTD);
            ARTDEB.setName("ARTDEB");
            panel1.add(ARTDEB);
            ARTDEB.setBounds(225, 195, 210, ARTDEB.getPreferredSize().height);

            //---- ARTFIN ----
            ARTFIN.setComponentPopupMenu(BTD);
            ARTFIN.setName("ARTFIN");
            panel1.add(ARTFIN);
            ARTFIN.setBounds(485, 195, 210, ARTFIN.getPreferredSize().height);

            //---- FAMDEB ----
            FAMDEB.setComponentPopupMenu(BTD);
            FAMDEB.setName("FAMDEB");
            panel1.add(FAMDEB);
            FAMDEB.setBounds(225, 165, 36, FAMDEB.getPreferredSize().height);

            //---- FAMFIN ----
            FAMFIN.setComponentPopupMenu(BTD);
            FAMFIN.setName("FAMFIN");
            panel1.add(FAMFIN);
            FAMFIN.setBounds(310, 165, 36, FAMFIN.getPreferredSize().height);

            //---- CLIDEB ----
            CLIDEB.setComponentPopupMenu(BTD);
            CLIDEB.setName("CLIDEB");
            panel1.add(CLIDEB);
            CLIDEB.setBounds(150, 456, 60, 28);

            //---- CLIFIN ----
            CLIFIN.setComponentPopupMenu(BTD);
            CLIFIN.setName("CLIFIN");
            panel1.add(CLIFIN);
            CLIFIN.setBounds(245, 456, 60, 28);

            //---- FRSDEB ----
            FRSDEB.setComponentPopupMenu(BTD);
            FRSDEB.setName("FRSDEB");
            panel1.add(FRSDEB);
            FRSDEB.setBounds(150, 401, 68, 28);

            //---- FRSFIN ----
            FRSFIN.setComponentPopupMenu(BTD);
            FRSFIN.setName("FRSFIN");
            panel1.add(FRSFIN);
            FRSFIN.setBounds(245, 401, 68, 28);

            //---- TYPREA ----
            TYPREA.setComponentPopupMenu(BTD);
            TYPREA.setModel(new DefaultComboBoxModel(new String[] {
              "Tous",
              "Rupture sur stock minimum",
              "Sur consomation moyenne",
              "R\u00e9approvisionnement manuel",
              "Sur consomation pr\u00e9vue",
              "Consomation moyenne plafonn\u00e9e",
              "G\u00e9r\u00e9 (plafond couvert)",
              "Compl\u00e9ment stock maxi",
              "Plafond couverture sur magasin",
              "Produit non g\u00e9r\u00e9"
            }));
            TYPREA.setName("TYPREA");
            panel1.add(TYPREA);
            TYPREA.setBounds(225, 225, 260, TYPREA.getPreferredSize().height);

            //---- NUMZP ----
            NUMZP.setComponentPopupMenu(BTD);
            NUMZP.setName("NUMZP");
            panel1.add(NUMZP);
            NUMZP.setBounds(245, 516, 34, NUMZP.getPreferredSize().height);

            //---- ZPDEB ----
            ZPDEB.setComponentPopupMenu(BTD);
            ZPDEB.setName("ZPDEB");
            panel1.add(ZPDEB);
            ZPDEB.setBounds(510, 516, 34, ZPDEB.getPreferredSize().height);

            //---- OBJ_89 ----
            OBJ_89.setText("\u00e0");
            OBJ_89.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(219, 405, 25, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("\u00e0");
            OBJ_90.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_90.setName("OBJ_90");
            panel1.add(OBJ_90);
            OBJ_90.setBounds(219, 460, 25, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("\u00e0");
            OBJ_91.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_91.setName("OBJ_91");
            panel1.add(OBJ_91);
            OBJ_91.setBounds(273, 169, 25, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("\u00e0");
            OBJ_92.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_92.setName("OBJ_92");
            panel1.add(OBJ_92);
            OBJ_92.setBounds(440, 199, 25, 20);

            //---- OBJ_94 ----
            OBJ_94.setText("S\u00e9lection");
            OBJ_94.setName("OBJ_94");
            panel1.add(OBJ_94);
            OBJ_94.setBounds(380, 520, 75, 20);

            //---- TOP1 ----
            TOP1.setComponentPopupMenu(BTD);
            TOP1.setText("inf\u00e9rieure");
            TOP1.setName("TOP1");
            panel1.add(TOP1);
            TOP1.setBounds(150, 285, 95, TOP1.getPreferredSize().height);

            //---- TOP2 ----
            TOP2.setComponentPopupMenu(BTD);
            TOP2.setText("sup\u00e9rieure ou \u00e9gale");
            TOP2.setName("TOP2");
            panel1.add(TOP2);
            TOP2.setBounds(335, 285, 145, TOP2.getPreferredSize().height);

            //---- TOP3 ----
            TOP3.setComponentPopupMenu(BTD);
            TOP3.setText("inf\u00e9rieure");
            TOP3.setName("TOP3");
            panel1.add(TOP3);
            TOP3.setBounds(150, 317, 95, TOP3.getPreferredSize().height);

            //---- TOP4 ----
            TOP4.setComponentPopupMenu(BTD);
            TOP4.setText("sup\u00e9rieure ou \u00e9gale");
            TOP4.setName("TOP4");
            panel1.add(TOP4);
            TOP4.setBounds(335, 317, 145, TOP4.getPreferredSize().height);

            //---- TOP5 ----
            TOP5.setComponentPopupMenu(BTD);
            TOP5.setText("inf\u00e9rieure");
            TOP5.setName("TOP5");
            panel1.add(TOP5);
            TOP5.setBounds(150, 349, 95, TOP5.getPreferredSize().height);

            //---- TOP6 ----
            TOP6.setComponentPopupMenu(BTD);
            TOP6.setText("sup\u00e9rieure ou \u00e9gale");
            TOP6.setName("TOP6");
            panel1.add(TOP6);
            TOP6.setBounds(335, 349, 145, TOP6.getPreferredSize().height);

            //---- OBJ_98 ----
            OBJ_98.setText("ou");
            OBJ_98.setName("OBJ_98");
            panel1.add(OBJ_98);
            OBJ_98.setBounds(285, 284, 25, 20);

            //---- OBJ_99 ----
            OBJ_99.setText("ou");
            OBJ_99.setName("OBJ_99");
            panel1.add(OBJ_99);
            OBJ_99.setBounds(285, 316, 25, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("ou");
            OBJ_100.setName("OBJ_100");
            panel1.add(OBJ_100);
            OBJ_100.setBounds(285, 348, 25, 20);

            //---- QTDIS ----
            QTDIS.setComponentPopupMenu(BTD);
            QTDIS.setName("QTDIS");
            panel1.add(QTDIS);
            QTDIS.setBounds(530, 280, 54, QTDIS.getPreferredSize().height);

            //---- MOIDIS ----
            MOIDIS.setComponentPopupMenu(BTD);
            MOIDIS.setName("MOIDIS");
            panel1.add(MOIDIS);
            MOIDIS.setBounds(530, 312, 36, MOIDIS.getPreferredSize().height);

            //---- MINDIS ----
            MINDIS.setComponentPopupMenu(BTD);
            MINDIS.setName("MINDIS");
            panel1.add(MINDIS);
            MINDIS.setBounds(530, 344, 36, MINDIS.getPreferredSize().height);

            //---- OBJ_101 ----
            OBJ_101.setText("unit\u00e9s");
            OBJ_101.setName("OBJ_101");
            panel1.add(OBJ_101);
            OBJ_101.setBounds(590, 284, 160, 20);

            //---- OBJ_102 ----
            OBJ_102.setText("fois la conso. mensuelle");
            OBJ_102.setName("OBJ_102");
            panel1.add(OBJ_102);
            OBJ_102.setBounds(590, 316, 160, 20);

            //---- OBJ_103 ----
            OBJ_103.setText("fois le stock minimum");
            OBJ_103.setName("OBJ_103");
            panel1.add(OBJ_103);
            OBJ_103.setBounds(590, 348, 160, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("Magasin");
            OBJ_104.setName("OBJ_104");
            panel1.add(OBJ_104);
            OBJ_104.setBounds(35, 139, 155, 20);

            //---- DATT ----
            DATT.setName("DATT");
            panel1.add(DATT);
            DATT.setBounds(new Rectangle(new Point(35, 405), DATT.getPreferredSize()));

            //---- OBJ_54 ----
            OBJ_54.setTitle("D\u00e9tail de l'attendu");
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(10, 380, 730, OBJ_54.getPreferredSize().height);

            //---- OBJ_93 ----
            OBJ_93.setText("Dates de livraison");
            OBJ_93.setName("OBJ_93");
            panel1.add(OBJ_93);
            OBJ_93.setBounds(380, 405, 120, 20);

            //---- DATDEB2 ----
            DATDEB2.setName("DATDEB2");
            panel1.add(DATDEB2);
            DATDEB2.setBounds(510, 401, 105, DATDEB2.getPreferredSize().height);

            //---- OBJ_105 ----
            OBJ_105.setText("au");
            OBJ_105.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_105.setName("OBJ_105");
            panel1.add(OBJ_105);
            OBJ_105.setBounds(620, 405, 25, 20);

            //---- DATFIN2 ----
            DATFIN2.setName("DATFIN2");
            panel1.add(DATFIN2);
            DATFIN2.setBounds(650, 401, 105, DATFIN2.getPreferredSize().height);

            //---- DCDE ----
            DCDE.setName("DCDE");
            panel1.add(DCDE);
            DCDE.setBounds(new Rectangle(new Point(35, 460), DCDE.getPreferredSize()));

            //---- OBJ_55 ----
            OBJ_55.setTitle("D\u00e9tail du command\u00e9");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(10, 435, 730, OBJ_55.getPreferredSize().height);

            //---- OBJ_106 ----
            OBJ_106.setText("au");
            OBJ_106.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_106.setName("OBJ_106");
            panel1.add(OBJ_106);
            OBJ_106.setBounds(620, 460, 25, 20);

            //---- OBJ_56 ----
            OBJ_56.setTitle(" ");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(10, 495, 730, OBJ_56.getPreferredSize().height);

            //---- OBJ_107 ----
            OBJ_107.setText("Quantit\u00e9");
            OBJ_107.setName("OBJ_107");
            panel1.add(OBJ_107);
            OBJ_107.setBounds(35, 284, 105, 20);

            //---- OBJ_108 ----
            OBJ_108.setText("Quantit\u00e9");
            OBJ_108.setName("OBJ_108");
            panel1.add(OBJ_108);
            OBJ_108.setBounds(35, 316, 105, 20);

            //---- OBJ_109 ----
            OBJ_109.setText("Quantit\u00e9");
            OBJ_109.setName("OBJ_109");
            panel1.add(OBJ_109);
            OBJ_109.setBounds(35, 348, 105, 20);

            //---- OBJ_110 ----
            OBJ_110.setText("\u00e0");
            OBJ_110.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_110.setName("OBJ_110");
            panel1.add(OBJ_110);
            OBJ_110.setBounds(485, 284, 25, 20);

            //---- OBJ_111 ----
            OBJ_111.setText("\u00e0");
            OBJ_111.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_111.setName("OBJ_111");
            panel1.add(OBJ_111);
            OBJ_111.setBounds(485, 316, 25, 20);

            //---- OBJ_112 ----
            OBJ_112.setText("\u00e0");
            OBJ_112.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_112.setName("OBJ_112");
            panel1.add(OBJ_112);
            OBJ_112.setBounds(485, 348, 25, 20);

            //---- ZPFIN ----
            ZPFIN.setComponentPopupMenu(BTD);
            ZPFIN.setName("ZPFIN");
            panel1.add(ZPFIN);
            ZPFIN.setBounds(619, 516, 34, ZPFIN.getPreferredSize().height);

            //---- OBJ_113 ----
            OBJ_113.setText("\u00e0");
            OBJ_113.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_113.setName("OBJ_113");
            panel1.add(OBJ_113);
            OBJ_113.setBounds(569, 520, 25, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(TOP1);
    buttonGroup1.add(TOP2);

    //---- buttonGroup2 ----
    ButtonGroup buttonGroup2 = new ButtonGroup();
    buttonGroup2.add(TOP3);
    buttonGroup2.add(TOP4);

    //---- buttonGroup3 ----
    ButtonGroup buttonGroup3 = new ButtonGroup();
    buttonGroup3.add(TOP5);
    buttonGroup3.add(TOP6);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_51;
  private JXTitledSeparator OBJ_53;
  private XRiTextField WMAG;
  private XRiCheckBox WTOUM;
  private JLabel OBJ_81;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JLabel OBJ_83;
  private JLabel OBJ_84;
  private JLabel OBJ_85;
  private JLabel OBJ_86;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private XRiTextField CLIDEB;
  private XRiTextField CLIFIN;
  private XRiTextField FRSDEB;
  private XRiTextField FRSFIN;
  private XRiComboBox TYPREA;
  private XRiTextField NUMZP;
  private XRiTextField ZPDEB;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private JLabel OBJ_94;
  private XRiRadioButton TOP1;
  private XRiRadioButton TOP2;
  private XRiRadioButton TOP3;
  private XRiRadioButton TOP4;
  private XRiRadioButton TOP5;
  private XRiRadioButton TOP6;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private XRiTextField QTDIS;
  private XRiTextField MOIDIS;
  private XRiTextField MINDIS;
  private JLabel OBJ_101;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private XRiCheckBox DATT;
  private JXTitledSeparator OBJ_54;
  private JLabel OBJ_93;
  private XRiCalendrier DATDEB2;
  private JLabel OBJ_105;
  private XRiCalendrier DATFIN2;
  private XRiCheckBox DCDE;
  private JXTitledSeparator OBJ_55;
  private JLabel OBJ_106;
  private JXTitledSeparator OBJ_56;
  private JLabel OBJ_107;
  private JLabel OBJ_108;
  private JLabel OBJ_109;
  private JLabel OBJ_110;
  private JLabel OBJ_111;
  private JLabel OBJ_112;
  private XRiTextField ZPFIN;
  private JLabel OBJ_113;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
