
package ri.serien.libecranrpg.sgvx.SGVXJ04F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVXJ04F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] TARNAT_Value = { "TTC", "HT" };
  
  public SGVXJ04F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WSIM.setValeursSelection("1", "");
    WEDT.setValeursSelection("1", "");
    
    // Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    WENCX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    WENCX.setEnabled(false);
    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Magasins
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    

    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  // Traitement des boutons de la barre
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    p_sud = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbDateInventaire = new SNLabelChamp();
    DATVAL = new XRiCalendrier();
    pnlOptions = new SNPanelTitre();
    WSIM = new XRiCheckBox();
    WEDT = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    WENCX = new SNTexte();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      p_nord.add(bpPresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setBackground(new Color(239, 239, 222));
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //---- snBarreBouton ----
      snBarreBouton.setBackground(new Color(239, 239, 222));
      snBarreBouton.setName("snBarreBouton");
      p_sud.add(snBarreBouton, BorderLayout.SOUTH);

      //======== pnlContenu ========
      {
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridLayout());

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //======== pnlSelection ========
          {
            pnlSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlSelection.setName("pnlSelection");
            pnlSelection.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelection.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setInheritsPopupMenu(false);
            lbMagasin.setName("lbMagasin");
            pnlSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlSelection.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbDateInventaire ----
            lbDateInventaire.setText("Date de traitement");
            lbDateInventaire.setName("lbDateInventaire");
            pnlSelection.add(lbDateInventaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- DATVAL ----
            DATVAL.setMaximumSize(new Dimension(110, 28));
            DATVAL.setMinimumSize(new Dimension(110, 28));
            DATVAL.setPreferredSize(new Dimension(110, 28));
            DATVAL.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATVAL.setName("DATVAL");
            pnlSelection.add(DATVAL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlOptions ========
          {
            pnlOptions.setTitre("Options");
            pnlOptions.setMaximumSize(new Dimension(730, 120));
            pnlOptions.setMinimumSize(new Dimension(730, 120));
            pnlOptions.setPreferredSize(new Dimension(730, 120));
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- WSIM ----
            WSIM.setText("Contr\u00f4le des fichiers sans g\u00e9n\u00e9ration des commandes d'achat");
            WSIM.setMaximumSize(new Dimension(700, 30));
            WSIM.setMinimumSize(new Dimension(700, 30));
            WSIM.setPreferredSize(new Dimension(700, 30));
            WSIM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSIM.setAutoscrolls(true);
            WSIM.setName("WSIM");
            pnlOptions.add(WSIM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- WEDT ----
            WEDT.setText("Edition des commandes d'achats");
            WEDT.setMaximumSize(new Dimension(700, 30));
            WEDT.setMinimumSize(new Dimension(700, 30));
            WEDT.setPreferredSize(new Dimension(700, 30));
            WEDT.setFont(new Font("sansserif", Font.PLAIN, 14));
            WEDT.setName("WEDT");
            pnlOptions.add(WEDT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setMaximumSize(new Dimension(800, 190));
            pnlEtablissement.setMinimumSize(new Dimension(800, 190));
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setInheritsPopupMenu(false);
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setInheritsPopupMenu(false);
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- WENCX ----
            WENCX.setText("@WENCX@");
            WENCX.setPreferredSize(new Dimension(260, 30));
            WENCX.setMinimumSize(new Dimension(260, 30));
            WENCX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WENCX.setMaximumSize(new Dimension(260, 30));
            WENCX.setName("WENCX");
            pnlEtablissement.add(WENCX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlDroite);
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre bpPresentation;
  private SNPanel p_sud;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbDateInventaire;
  private XRiCalendrier DATVAL;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox WSIM;
  private XRiCheckBox WEDT;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte WENCX;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
