
package ri.serien.libecranrpg.sgvx.SGVX72FM;
// Nom Fichier: b_SGVX72FM_FMTB2_FMTF1_120.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVX72FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX72FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    UOPT2.setValeursSelection("X", " ");
    UOPT1.setValeursSelection("X", " ");
    REPON1.setValeursSelection("OUI", "NON");
    GENV1.setValeursSelection("O", "N");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSYS1@")).trim());
    LB1SY1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB1SY1@")).trim());
    LB1SY2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB1SY2@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WSY1.setEnabled(lexique.isPresent("WSY1"));
    WETB.setVisible(lexique.isPresent("WETB"));
    WARTM.setEnabled(lexique.isPresent("WARTM"));
    // UOPT2.setSelected(lexique.HostFieldGetData("UOPT2").equalsIgnoreCase("X"));
    // UOPT1.setSelected(lexique.HostFieldGetData("UOPT1").equalsIgnoreCase("X"));
    // REPON1.setEnabled( lexique.isPresent("REPON1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    // GENV1.setEnabled( lexique.isPresent("GENV1"));
    // GENV1.setSelected(lexique.HostFieldGetData("GENV1").equalsIgnoreCase("O"));
    OBJ_49.setVisible(lexique.isPresent("WENCX"));
    OBJ_35.setVisible(lexique.isPresent("DGNOM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WETB.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (UOPT2.isSelected())
    // lexique.HostFieldPutData("UOPT2", 0, "X");
    // else
    // lexique.HostFieldPutData("UOPT2", 0, " ");
    // if (UOPT1.isSelected())
    // lexique.HostFieldPutData("UOPT1", 0, "X");
    // else
    // lexique.HostFieldPutData("UOPT1", 0, " ");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (GENV1.isSelected())
    // lexique.HostFieldPutData("GENV1", 0, "O");
    // else
    // lexique.HostFieldPutData("GENV1", 0, "N");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvx72"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_touche")
    lexique.HostCursorPut("UOPT1");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche=("F4")
    // ScriptCall("G_touche")
    lexique.HostCursorPut("UOPT2");
    lexique.HostScreenSendKey(this, ("F4"), false);
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    OBJ_30 = new JXTitledSeparator();
    OBJ_31 = new JXTitledSeparator();
    OBJ_34 = new JXTitledSeparator();
    OBJ_33 = new JXTitledSeparator();
    OBJ_35 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_53 = new JLabel();
    GENV1 = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    LB1SY1 = new JLabel();
    LB1SY2 = new JLabel();
    UOPT1 = new XRiCheckBox();
    UOPT2 = new XRiCheckBox();
    WARTM = new XRiTextField();
    OBJ_42 = new JButton();
    OBJ_46 = new JButton();
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_52 = new JLabel();
    WETB = new XRiTextField();
    WSY1 = new XRiTextField();
    SY1DEB = new XRiTextField();
    SY1FIN = new XRiTextField();
    SY2DEB = new XRiTextField();
    SY2FIN = new XRiTextField();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    BT_ChgSoc = new JButton();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap(881, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addComponent(bt_Fonctions)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_30 ----
      OBJ_30.setTitle("G\u00e9n\u00e9ration");
      OBJ_30.setName("OBJ_30");

      //---- OBJ_31 ----
      OBJ_31.setTitle("Article");
      OBJ_31.setName("OBJ_31");

      //---- OBJ_34 ----
      OBJ_34.setTitle("Etablissement s\u00e9lectionn\u00e9");
      OBJ_34.setName("OBJ_34");

      //---- OBJ_33 ----
      OBJ_33.setTitle(" ");
      OBJ_33.setName("OBJ_33");

      //---- OBJ_35 ----
      OBJ_35.setText("@DGNOM@");
      OBJ_35.setName("OBJ_35");

      //---- OBJ_49 ----
      OBJ_49.setText("@WENCX@");
      OBJ_49.setName("OBJ_49");

      //---- OBJ_51 ----
      OBJ_51.setText("@WLIB@");
      OBJ_51.setName("OBJ_51");

      //---- OBJ_53 ----
      OBJ_53.setText("@LSYS1@");
      OBJ_53.setName("OBJ_53");

      //---- GENV1 ----
      GENV1.setText("G\u00e9n\u00e9ration des art. syst\u00e8me var1");
      GENV1.setComponentPopupMenu(BTD);
      GENV1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      GENV1.setName("GENV1");

      //---- REPON1 ----
      REPON1.setText("Saisie des incompatibilit\u00e9s");
      REPON1.setComponentPopupMenu(BTD);
      REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      REPON1.setName("REPON1");

      //---- LB1SY1 ----
      LB1SY1.setText("@LB1SY1@");
      LB1SY1.setComponentPopupMenu(BTD);
      LB1SY1.setName("LB1SY1");

      //---- LB1SY2 ----
      LB1SY2.setText("@LB1SY2@");
      LB1SY2.setComponentPopupMenu(BTD);
      LB1SY2.setName("LB1SY2");

      //---- UOPT1 ----
      UOPT1.setText("S\u00e9lection liste");
      UOPT1.setComponentPopupMenu(BTD);
      UOPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      UOPT1.setName("UOPT1");

      //---- UOPT2 ----
      UOPT2.setText("S\u00e9lection liste");
      UOPT2.setComponentPopupMenu(BTD);
      UOPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      UOPT2.setName("UOPT2");

      //---- WARTM ----
      WARTM.setComponentPopupMenu(BTD);
      WARTM.setName("WARTM");

      //---- OBJ_42 ----
      OBJ_42.setText("Liste");
      OBJ_42.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_42.setName("OBJ_42");
      OBJ_42.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_42ActionPerformed(e);
        }
      });

      //---- OBJ_46 ----
      OBJ_46.setText("Liste");
      OBJ_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_46.setName("OBJ_46");
      OBJ_46.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_46ActionPerformed(e);
        }
      });

      //---- OBJ_56 ----
      OBJ_56.setText("Syst\u00e8me");
      OBJ_56.setName("OBJ_56");

      //---- OBJ_58 ----
      OBJ_58.setText("Syst\u00e8me");
      OBJ_58.setName("OBJ_58");

      //---- OBJ_50 ----
      OBJ_50.setText("Radical");
      OBJ_50.setName("OBJ_50");

      //---- OBJ_52 ----
      OBJ_52.setText("Variable");
      OBJ_52.setName("OBJ_52");

      //---- WETB ----
      WETB.setComponentPopupMenu(BTD);
      WETB.setName("WETB");

      //---- WSY1 ----
      WSY1.setComponentPopupMenu(BTD);
      WSY1.setName("WSY1");

      //---- SY1DEB ----
      SY1DEB.setComponentPopupMenu(BTD);
      SY1DEB.setName("SY1DEB");

      //---- SY1FIN ----
      SY1FIN.setComponentPopupMenu(BTD);
      SY1FIN.setName("SY1FIN");

      //---- SY2DEB ----
      SY2DEB.setComponentPopupMenu(BTD);
      SY2DEB.setName("SY2DEB");

      //---- SY2FIN ----
      SY2FIN.setComponentPopupMenu(BTD);
      SY2FIN.setName("SY2FIN");

      //---- OBJ_54 ----
      OBJ_54.setText("D\u00e9but");
      OBJ_54.setName("OBJ_54");

      //---- OBJ_55 ----
      OBJ_55.setText("Fin");
      OBJ_55.setName("OBJ_55");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(20, 20, 20)
            .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(83, 83, 83)
            .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(2, 2, 2)
            .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
            .addGap(53, 53, 53)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 406, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(20, 20, 20)
            .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 543, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(83, 83, 83)
            .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
            .addGap(28, 28, 28)
            .addComponent(WARTM, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(83, 83, 83)
            .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
            .addGap(28, 28, 28)
            .addComponent(WSY1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
            .addGap(95, 95, 95)
            .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(20, 20, 20)
            .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 543, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(250, 250, 250)
            .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(52, 52, 52)
            .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5)
            .addComponent(LB1SY1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addComponent(SY1DEB, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(SY1FIN, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addComponent(UOPT1, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
            .addGap(22, 22, 22)
            .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(52, 52, 52)
            .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addComponent(LB1SY2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addComponent(SY2DEB, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(SY2FIN, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addComponent(UOPT2, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
            .addGap(22, 22, 22)
            .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(52, 52, 52)
            .addComponent(GENV1, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(25, 25, 25)
            .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 544, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(52, 52, 52)
            .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(9, 9, 9)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGap(10, 10, 10)
            .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(12, 12, 12)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WARTM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WSY1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGap(13, 13, 13)
            .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
            .addGap(13, 13, 13)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(LB1SY1, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(SY1DEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(SY1FIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(UOPT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(OBJ_42))
            .addGap(1, 1, 1)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(LB1SY2, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(SY2DEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(SY2FIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(UOPT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(OBJ_46))
            .addGap(11, 11, 11)
            .addComponent(GENV1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_30;
  private JXTitledSeparator OBJ_31;
  private JXTitledSeparator OBJ_34;
  private JXTitledSeparator OBJ_33;
  private JLabel OBJ_35;
  private JLabel OBJ_49;
  private JLabel OBJ_51;
  private JLabel OBJ_53;
  private XRiCheckBox GENV1;
  private XRiCheckBox REPON1;
  private JLabel LB1SY1;
  private JLabel LB1SY2;
  private XRiCheckBox UOPT1;
  private XRiCheckBox UOPT2;
  private XRiTextField WARTM;
  private JButton OBJ_42;
  private JButton OBJ_46;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private JLabel OBJ_50;
  private JLabel OBJ_52;
  private XRiTextField WETB;
  private XRiTextField WSY1;
  private XRiTextField SY1DEB;
  private XRiTextField SY1FIN;
  private XRiTextField SY2DEB;
  private XRiTextField SY2FIN;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JButton BT_ChgSoc;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
