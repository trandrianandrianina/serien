
package ri.serien.libecranrpg.sgvx.SGVX63FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX63FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX63FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TRICAF.setValeursSelection("1", " ");
    TRIFRS.setValeursSelection("1", " ");
    TRIART.setValeursSelection("1", " ");
    CACPRV.setValeursSelection("1", " ");
    CACPRX.setValeursSelection("1", " ");
    TRIGFA.setValeursSelection("1", " ");
    WTOUG.setValeursSelection("**", "  ");
    WTOUA.setValeursSelection("**", "  ");
    WTOUF.setValeursSelection("**", "  ");
    DETART.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBD@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // WTOUF.setSelected(lexique.HostFieldGetData("WTOUF").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!WTOUF.isSelected() & WTOUF.isVisible());
    TRIFRS.setEnabled(lexique.HostFieldGetData("TRICAF").trim().equalsIgnoreCase(""));
    // TRIFRS.setSelected(lexique.HostFieldGetData("TRIFRS").equalsIgnoreCase("1"));
    TRICAF.setEnabled(lexique.HostFieldGetData("TRIFRS").equalsIgnoreCase(""));
    // TRICAF.setSelected(lexique.HostFieldGetData("TRICAF").equalsIgnoreCase("1"));
    // WTOUA.setSelected(lexique.HostFieldGetData("WTOUA").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTOUA.isSelected() & WTOUA.isVisible());
    // DETART.setSelected(lexique.HostFieldGetData("DETART").equalsIgnoreCase("OUI"));
    // WTOUG.setSelected(lexique.HostFieldGetData("WTOUG").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!WTOUG.isSelected() & WTOUG.isVisible());
    OBJ_65.setVisible(lexique.isPresent("FALIBF"));
    OBJ_64.setVisible(lexique.isPresent("FALIBD"));
    // TRIART.setEnabled(lexique.HostFieldGetData("TRIGFA").trim().equalsIgnoreCase("") & lexique.isPresent("TRIART"));
    // TRIART.setSelected(lexique.HostFieldGetData("TRIART").equalsIgnoreCase("1"));
    // CACPRV.setEnabled(lexique.HostFieldGetData("CACPRX").trim().equalsIgnoreCase("") & lexique.isPresent("CACPRV"));
    // CACPRV.setSelected(lexique.HostFieldGetData("CACPRV").equalsIgnoreCase("1"));
    // CACPRX.setEnabled(lexique.HostFieldGetData("CACPRV").equalsIgnoreCase("") & lexique.isPresent("CACPRX"));
    // CACPRX.setSelected(lexique.HostFieldGetData("CACPRX").equalsIgnoreCase("1"));
    // TRIGFA.setEnabled(lexique.HostFieldGetData("TRIART").equalsIgnoreCase("") & lexique.isPresent("TRIGFA"));
    // TRIGFA.setSelected(lexique.HostFieldGetData("TRIGFA").equalsIgnoreCase("1"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TRIART.isSelected())
    // lexique.HostFieldPutData("TRIART", 0, "1");
    // else
    // lexique.HostFieldPutData("TRIART", 0, " ");
    // if (CACPRV.isSelected())
    // lexique.HostFieldPutData("CACPRV", 0, "1");
    // else
    // lexique.HostFieldPutData("CACPRV", 0, " ");
    // if (TRIFRS.isSelected())
    // lexique.HostFieldPutData("TRIFRS", 0, "1");
    // else
    // lexique.HostFieldPutData("TRIFRS", 0, " ");
    // if (CACPRX.isSelected())
    // lexique.HostFieldPutData("CACPRX", 0, "1");
    // else
    // lexique.HostFieldPutData("CACPRX", 0, " ");
    // if (TRIGFA.isSelected())
    // lexique.HostFieldPutData("TRIGFA", 0, "1");
    // else
    // lexique.HostFieldPutData("TRIGFA", 0, " ");
    // if (WTOUG.isSelected())
    // lexique.HostFieldPutData("WTOUG", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUG", 0, " ");
    // if (WTOUA.isSelected())
    // lexique.HostFieldPutData("WTOUA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUA", 0, " ");
    // if (WTOUF.isSelected())
    // lexique.HostFieldPutData("WTOUF", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUF", 0, " ");
    // if (DETART.isSelected())
    // lexique.HostFieldPutData("DETART", 0, "OUI");
    // else
    // lexique.HostFieldPutData("DETART", 0, "NON");
    // if (TRICAF.isSelected())
    // lexique.HostFieldPutData("TRICAF", 0, "1");
    // else
    // lexique.HostFieldPutData("TRICAF", 0, " ");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUFActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOUAActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUGActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void TRIGFAActionPerformed(ActionEvent e) {
    TRIART.setEnabled(!TRIGFA.isSelected());
  }
  
  private void CACPRXActionPerformed(ActionEvent e) {
    CACPRV.setEnabled(!CACPRX.isSelected());
  }
  
  private void CACPRVActionPerformed(ActionEvent e) {
    CACPRX.setEnabled(!CACPRV.isSelected());
  }
  
  private void TRIARTActionPerformed(ActionEvent e) {
    TRIGFA.setEnabled(!TRIART.isSelected());
  }
  
  private void TRIFRSActionPerformed(ActionEvent e) {
    TRICAF.setEnabled(!TRIFRS.isSelected());
  }
  
  private void TRICAFActionPerformed(ActionEvent e) {
    TRIFRS.setEnabled(!TRICAF.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_25 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_30 = new JXTitledSeparator();
    OBJ_24 = new JXTitledSeparator();
    OBJ_67 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    OBJ_64 = new RiZoneSortie();
    OBJ_65 = new RiZoneSortie();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    GFADEB = new XRiTextField();
    GFAFIN = new XRiTextField();
    P_SEL0 = new JPanel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    P_SEL1 = new JPanel();
    OBJ_32 = new JLabel();
    OBJ_40 = new JLabel();
    FRSDEB = new XRiTextField();
    FRSFIN = new XRiTextField();
    OBJ_43 = new JXTitledSeparator();
    DETART = new XRiCheckBox();
    WTOUF = new XRiCheckBox();
    WTOUA = new XRiCheckBox();
    WTOUG = new XRiCheckBox();
    TRIGFA = new XRiCheckBox();
    CACPRX = new XRiCheckBox();
    OBJ_61 = new JLabel();
    CACPRV = new XRiCheckBox();
    TRIART = new XRiCheckBox();
    PDEB1 = new XRiCalendrier();
    PFIN1 = new XRiCalendrier();
    OBJ_60 = new JLabel();
    OBJ_29 = new JXTitledSeparator();
    TRIFRS = new XRiCheckBox();
    TRICAF = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_25 ----
          OBJ_25.setTitle("Plage d'articles");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_21 ----
          OBJ_21.setTitle("Plage de groupes-familles");
          OBJ_21.setName("OBJ_21");

          //---- OBJ_30 ----
          OBJ_30.setTitle("Num\u00e9ro du fournisseur");
          OBJ_30.setName("OBJ_30");

          //---- OBJ_24 ----
          OBJ_24.setTitle("Crit\u00e8res de tri");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_67 ----
          OBJ_67.setTitle("Chiffre d'affaires achats calcul\u00e9 \u00e0 partir du");
          OBJ_67.setName("OBJ_67");

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- OBJ_64 ----
            OBJ_64.setText("@FALIBD@");
            OBJ_64.setName("OBJ_64");
            P_SEL2.add(OBJ_64);
            OBJ_64.setBounds(201, 7, 260, OBJ_64.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("@FALIBF@");
            OBJ_65.setName("OBJ_65");
            P_SEL2.add(OBJ_65);
            OBJ_65.setBounds(201, 32, 260, OBJ_65.getPreferredSize().height);

            //---- OBJ_62 ----
            OBJ_62.setText("Groupe-famille de d\u00e9but");
            OBJ_62.setName("OBJ_62");
            P_SEL2.add(OBJ_62);
            OBJ_62.setBounds(15, 9, 133, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("Groupe-famille de fin");
            OBJ_63.setName("OBJ_63");
            P_SEL2.add(OBJ_63);
            OBJ_63.setBounds(15, 34, 130, 20);

            //---- GFADEB ----
            GFADEB.setComponentPopupMenu(BTD);
            GFADEB.setName("GFADEB");
            P_SEL2.add(GFADEB);
            GFADEB.setBounds(156, 5, 44, GFADEB.getPreferredSize().height);

            //---- GFAFIN ----
            GFAFIN.setComponentPopupMenu(BTD);
            GFAFIN.setName("GFAFIN");
            P_SEL2.add(GFAFIN);
            GFAFIN.setBounds(156, 30, 44, GFAFIN.getPreferredSize().height);
          }

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- ARTDEB ----
            ARTDEB.setComponentPopupMenu(BTD);
            ARTDEB.setName("ARTDEB");
            P_SEL0.add(ARTDEB);
            ARTDEB.setBounds(112, 5, 210, ARTDEB.getPreferredSize().height);

            //---- ARTFIN ----
            ARTFIN.setComponentPopupMenu(BTD);
            ARTFIN.setName("ARTFIN");
            P_SEL0.add(ARTFIN);
            ARTFIN.setBounds(112, 30, 210, ARTFIN.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("Code de d\u00e9but");
            OBJ_41.setName("OBJ_41");
            P_SEL0.add(OBJ_41);
            OBJ_41.setBounds(15, 10, 90, 18);

            //---- OBJ_42 ----
            OBJ_42.setText("Code de fin");
            OBJ_42.setName("OBJ_42");
            P_SEL0.add(OBJ_42);
            OBJ_42.setBounds(15, 35, 75, 18);
          }

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_32 ----
            OBJ_32.setText("Num\u00e9ro fournisseur de d\u00e9but");
            OBJ_32.setName("OBJ_32");
            P_SEL1.add(OBJ_32);
            OBJ_32.setBounds(15, 10, 170, 18);

            //---- OBJ_40 ----
            OBJ_40.setText("Num\u00e9ro fournisseur de fin");
            OBJ_40.setName("OBJ_40");
            P_SEL1.add(OBJ_40);
            OBJ_40.setBounds(15, 35, 159, 18);

            //---- FRSDEB ----
            FRSDEB.setComponentPopupMenu(BTD);
            FRSDEB.setName("FRSDEB");
            P_SEL1.add(FRSDEB);
            FRSDEB.setBounds(189, 5, 68, FRSDEB.getPreferredSize().height);

            //---- FRSFIN ----
            FRSFIN.setComponentPopupMenu(BTD);
            FRSFIN.setName("FRSFIN");
            P_SEL1.add(FRSFIN);
            FRSFIN.setBounds(189, 30, 68, FRSFIN.getPreferredSize().height);
          }

          //---- OBJ_43 ----
          OBJ_43.setTitle("");
          OBJ_43.setName("OBJ_43");

          //---- DETART ----
          DETART.setText("Edition d\u00e9tail article");
          DETART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DETART.setName("DETART");

          //---- WTOUF ----
          WTOUF.setText("S\u00e9lection compl\u00e8te");
          WTOUF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUF.setName("WTOUF");
          WTOUF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUFActionPerformed(e);
            }
          });

          //---- WTOUA ----
          WTOUA.setText("S\u00e9lection compl\u00e8te");
          WTOUA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUA.setName("WTOUA");
          WTOUA.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUAActionPerformed(e);
            }
          });

          //---- WTOUG ----
          WTOUG.setText("S\u00e9lection compl\u00e8te");
          WTOUG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUG.setName("WTOUG");
          WTOUG.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUGActionPerformed(e);
            }
          });

          //---- TRIGFA ----
          TRIGFA.setText("Groupe-famille");
          TRIGFA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIGFA.setName("TRIGFA");
          TRIGFA.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              TRIGFAActionPerformed(e);
            }
          });

          //---- CACPRX ----
          CACPRX.setText("Prix d'achat");
          CACPRX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CACPRX.setName("CACPRX");
          CACPRX.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              CACPRXActionPerformed(e);
            }
          });

          //---- OBJ_61 ----
          OBJ_61.setText("P\u00e9riode d'analyse:");
          OBJ_61.setName("OBJ_61");

          //---- CACPRV ----
          CACPRV.setText("Prix de revient");
          CACPRV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CACPRV.setName("CACPRV");
          CACPRV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              CACPRVActionPerformed(e);
            }
          });

          //---- TRIART ----
          TRIART.setText("Code article");
          TRIART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIART.setName("TRIART");
          TRIART.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              TRIARTActionPerformed(e);
            }
          });

          //---- PDEB1 ----
          PDEB1.setName("PDEB1");

          //---- PFIN1 ----
          PFIN1.setName("PFIN1");

          //---- OBJ_60 ----
          OBJ_60.setText("au");
          OBJ_60.setName("OBJ_60");

          //---- OBJ_29 ----
          OBJ_29.setTitle("Crit\u00e8res de tri");
          OBJ_29.setName("OBJ_29");

          //---- TRIFRS ----
          TRIFRS.setText("N\u00b0 fournisseur");
          TRIFRS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIFRS.setName("TRIFRS");
          TRIFRS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              TRIFRSActionPerformed(e);
            }
          });

          //---- TRICAF ----
          TRICAF.setText("Chiffre affaires d\u00e9croissant");
          TRICAF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRICAF.setName("TRICAF");
          TRICAF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              TRICAFActionPerformed(e);
            }
          });

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUF, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(TRIFRS, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(TRICAF, GroupLayout.PREFERRED_SIZE, 178, GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addComponent(PDEB1, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(PFIN1, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WTOUA, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DETART, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUG, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(TRIART, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53)
                .addComponent(TRIGFA, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
                .addGap(102, 102, 102)
                .addComponent(CACPRV, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66)
                .addComponent(CACPRX, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(18, 18, 18)
                    .addComponent(WTOUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TRIFRS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TRICAF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PDEB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PFIN1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(23, 23, 23)
                    .addComponent(WTOUA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(DETART, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(23, 23, 23)
                    .addComponent(WTOUG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(TRIART, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TRIGFA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(CACPRV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(CACPRX, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_30;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_67;
  private JPanel P_SEL2;
  private RiZoneSortie OBJ_64;
  private RiZoneSortie OBJ_65;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private XRiTextField GFADEB;
  private XRiTextField GFAFIN;
  private JPanel P_SEL0;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private JPanel P_SEL1;
  private JLabel OBJ_32;
  private JLabel OBJ_40;
  private XRiTextField FRSDEB;
  private XRiTextField FRSFIN;
  private JXTitledSeparator OBJ_43;
  private XRiCheckBox DETART;
  private XRiCheckBox WTOUF;
  private XRiCheckBox WTOUA;
  private XRiCheckBox WTOUG;
  private XRiCheckBox TRIGFA;
  private XRiCheckBox CACPRX;
  private JLabel OBJ_61;
  private XRiCheckBox CACPRV;
  private XRiCheckBox TRIART;
  private XRiCalendrier PDEB1;
  private XRiCalendrier PFIN1;
  private JLabel OBJ_60;
  private JXTitledSeparator OBJ_29;
  private XRiCheckBox TRIFRS;
  private XRiCheckBox TRICAF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
