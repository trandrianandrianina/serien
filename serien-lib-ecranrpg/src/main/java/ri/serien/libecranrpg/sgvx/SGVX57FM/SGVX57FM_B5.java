
package ri.serien.libecranrpg.sgvx.SGVX57FM;
// Nom Fichier: b_SGVX57FM_FMTB5_FMTF1_326.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVX57FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX57FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CPRI4.setValeurs("4", "RB");
    CPRI5.setValeurs("5", "RB");
    CPRI3.setValeurs("3", "RB");
    CPRI1.setValeurs("1", "RB");
    CPRI2.setValeurs("2", "RB");
    WTOUM.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("OUI", "NON");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_26.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBELLE1@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_34.setVisible(lexique.isPresent("DEBIAK"));
    OBJ_33.setVisible(lexique.isPresent("EDEBA"));
    OBJ_37.setVisible(lexique.isPresent("DEBIAK"));
    OBJ_36.setVisible(lexique.isPresent("EDEBA"));
    // MA12.setVisible( lexique.isPresent("MA12"));
    // MA11.setVisible( lexique.isPresent("MA11"));
    // MA10.setVisible( lexique.isPresent("MA10"));
    // MA09.setVisible( lexique.isPresent("MA09"));
    // MA08.setVisible( lexique.isPresent("MA08"));
    // MA07.setVisible( lexique.isPresent("MA07"));
    // MA06.setVisible( lexique.isPresent("MA06"));
    // MA05.setVisible( lexique.isPresent("MA05"));
    // MA04.setVisible( lexique.isPresent("MA04"));
    // MA03.setVisible( lexique.isPresent("MA03"));
    // MA02.setVisible( lexique.isPresent("MA02"));
    // MA01.setVisible( lexique.isPresent("MA01"));
    // EDEB.setVisible( lexique.isPresent("EDEB"));
    // EFIN.setVisible( lexique.isPresent("EFIN"));
    // WETB.setEnabled( lexique.isPresent("WETB"));
    // DATREC.setVisible( lexique.isPresent("DATREC"));
    // DATCHF.setVisible( lexique.isPresent("DATCHF"));
    OBJ_35.setVisible(lexique.isPresent("EDEB"));
    // CPRI4.setVisible( lexique.isPresent("RB"));
    // CPRI4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    OBJ_38.setVisible(lexique.isPresent("EDEB"));
    // CPRI5.setVisible( lexique.isPresent("RB"));
    // CPRI5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // CPRI3.setVisible( lexique.isPresent("RB"));
    // CPRI3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // CPRI1.setVisible( lexique.isPresent("RB"));
    // CPRI1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // CPRI2.setVisible( lexique.isPresent("RB"));
    // CPRI2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // WTOUM.setVisible( lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // REPON1.setVisible( lexique.isPresent("REPON1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    // DEBIAK.setVisible( lexique.isPresent("DEBIAK"));
    // FINIAK.setVisible( lexique.isPresent("FINIAK"));
    // EFINA.setVisible( lexique.isPresent("EFINA"));
    // EDEBA.setVisible( lexique.isPresent("EDEBA"));
    OBJ_46.setVisible(lexique.isPresent("WENCX"));
    OBJ_44.setVisible(lexique.isPresent("DGNOM"));
    P_SEL3.setVisible((lexique.isPresent("WTOU")) && (!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**")));
    P_SEL2.setVisible((lexique.isPresent("WTOUM")) && (!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**")));
    P_SEL1.setVisible((lexique.isPresent("WTOUM")) && (!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**")));
    P_SEL0.setVisible((lexique.isPresent("WTOUM")) && (!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**")));
    OBJ_48.setVisible(lexique.isPresent("WTOUM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WETB.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (CPRI4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (CPRI5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (CPRI3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (CPRI1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (CPRI2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvx57"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    l_LOCTP = new JLabel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    OBJ_26 = new JXTitledSeparator();
    OBJ_48 = new JXTitledSeparator();
    OBJ_42 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    OBJ_29 = new JXTitledSeparator();
    OBJ_41 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    P_SEL3 = new JPanel();
    OBJ_38 = new JLabel();
    OBJ_35 = new JLabel();
    EFIN = new XRiTextField();
    EDEB = new XRiTextField();
    OBJ_44 = new JLabel();
    OBJ_46 = new JLabel();
    REPON1 = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    WTOUM = new XRiCheckBox();
    CPRI2 = new XRiRadioButton();
    CPRI1 = new XRiRadioButton();
    CPRI3 = new XRiRadioButton();
    CPRI5 = new XRiRadioButton();
    CPRI4 = new XRiRadioButton();
    DATCHF = new XRiCalendrier();
    DATREC = new XRiCalendrier();
    WETB = new XRiTextField();
    OBJ_71 = new JLabel();
    OBJ_73 = new JLabel();
    BT_ChgSoc = new JButton();
    P_SEL1 = new JPanel();
    EDEBA = new XRiTextField();
    EFINA = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_36 = new JLabel();
    P_SEL2 = new JPanel();
    DEBIAK = new XRiTextField();
    FINIAK = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_33 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- l_LOCTP ----
        l_LOCTP.setText("@LOCTP@");
        l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
        l_LOCTP.setName("l_LOCTP");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 782, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(bt_Fonctions))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_26 ----
      OBJ_26.setTitle("@LIBELLE1@");
      OBJ_26.setName("OBJ_26");

      //---- OBJ_48 ----
      OBJ_48.setTitle("Codes magasins");
      OBJ_48.setName("OBJ_48");

      //---- OBJ_42 ----
      OBJ_42.setTitle(" ");
      OBJ_42.setName("OBJ_42");

      //---- OBJ_43 ----
      OBJ_43.setTitle("Etablissement s\u00e9lectionn\u00e9");
      OBJ_43.setName("OBJ_43");

      //---- OBJ_29 ----
      OBJ_29.setTitle("Chiffrage des stocks");
      OBJ_29.setName("OBJ_29");

      //---- OBJ_41 ----
      OBJ_41.setTitle("Derni\u00e8re r\u00e9ception avant le");
      OBJ_41.setName("OBJ_41");

      //======== P_SEL0 ========
      {
        P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
        P_SEL0.setName("P_SEL0");
        P_SEL0.setLayout(null);

        //---- MA01 ----
        MA01.setComponentPopupMenu(BTD);
        MA01.setName("MA01");
        P_SEL0.add(MA01);
        MA01.setBounds(7, 9, 30, MA01.getPreferredSize().height);

        //---- MA02 ----
        MA02.setComponentPopupMenu(BTD);
        MA02.setName("MA02");
        P_SEL0.add(MA02);
        MA02.setBounds(44, 9, 30, MA02.getPreferredSize().height);

        //---- MA03 ----
        MA03.setComponentPopupMenu(BTD);
        MA03.setName("MA03");
        P_SEL0.add(MA03);
        MA03.setBounds(80, 9, 30, MA03.getPreferredSize().height);

        //---- MA04 ----
        MA04.setComponentPopupMenu(BTD);
        MA04.setName("MA04");
        P_SEL0.add(MA04);
        MA04.setBounds(116, 9, 30, MA04.getPreferredSize().height);

        //---- MA05 ----
        MA05.setComponentPopupMenu(BTD);
        MA05.setName("MA05");
        P_SEL0.add(MA05);
        MA05.setBounds(152, 9, 30, MA05.getPreferredSize().height);

        //---- MA06 ----
        MA06.setComponentPopupMenu(BTD);
        MA06.setName("MA06");
        P_SEL0.add(MA06);
        MA06.setBounds(189, 9, 30, MA06.getPreferredSize().height);

        //---- MA07 ----
        MA07.setComponentPopupMenu(BTD);
        MA07.setName("MA07");
        P_SEL0.add(MA07);
        MA07.setBounds(225, 9, 30, MA07.getPreferredSize().height);

        //---- MA08 ----
        MA08.setComponentPopupMenu(BTD);
        MA08.setName("MA08");
        P_SEL0.add(MA08);
        MA08.setBounds(261, 9, 30, MA08.getPreferredSize().height);

        //---- MA09 ----
        MA09.setComponentPopupMenu(BTD);
        MA09.setName("MA09");
        P_SEL0.add(MA09);
        MA09.setBounds(298, 9, 30, MA09.getPreferredSize().height);

        //---- MA10 ----
        MA10.setComponentPopupMenu(BTD);
        MA10.setName("MA10");
        P_SEL0.add(MA10);
        MA10.setBounds(334, 9, 30, MA10.getPreferredSize().height);

        //---- MA11 ----
        MA11.setComponentPopupMenu(BTD);
        MA11.setName("MA11");
        P_SEL0.add(MA11);
        MA11.setBounds(370, 9, 30, MA11.getPreferredSize().height);

        //---- MA12 ----
        MA12.setComponentPopupMenu(BTD);
        MA12.setName("MA12");
        P_SEL0.add(MA12);
        MA12.setBounds(407, 9, 30, MA12.getPreferredSize().height);
      }

      //======== P_SEL3 ========
      {
        P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
        P_SEL3.setName("P_SEL3");
        P_SEL3.setLayout(null);

        //---- OBJ_38 ----
        OBJ_38.setText("Famille D\u00e9but");
        OBJ_38.setName("OBJ_38");
        P_SEL3.add(OBJ_38);
        OBJ_38.setBounds(10, 15, 87, 18);

        //---- OBJ_35 ----
        OBJ_35.setText("Famille Fin");
        OBJ_35.setName("OBJ_35");
        P_SEL3.add(OBJ_35);
        OBJ_35.setBounds(160, 15, 69, 18);

        //---- EFIN ----
        EFIN.setComponentPopupMenu(BTD);
        EFIN.setName("EFIN");
        P_SEL3.add(EFIN);
        EFIN.setBounds(230, 10, 40, EFIN.getPreferredSize().height);

        //---- EDEB ----
        EDEB.setComponentPopupMenu(BTD);
        EDEB.setName("EDEB");
        P_SEL3.add(EDEB);
        EDEB.setBounds(95, 10, 40, EDEB.getPreferredSize().height);
      }

      //---- OBJ_44 ----
      OBJ_44.setText("@DGNOM@");
      OBJ_44.setName("OBJ_44");

      //---- OBJ_46 ----
      OBJ_46.setText("@WENCX@");
      OBJ_46.setName("OBJ_46");

      //---- REPON1 ----
      REPON1.setText("Lignes d\u00e9tail articles");
      REPON1.setComponentPopupMenu(BTD);
      REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      REPON1.setName("REPON1");

      //---- WTOU ----
      WTOU.setText("S\u00e9lection compl\u00e8te");
      WTOU.setComponentPopupMenu(BTD);
      WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      WTOU.setName("WTOU");
      WTOU.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          WTOUActionPerformed(e);
        }
      });

      //---- WTOUM ----
      WTOUM.setText("S\u00e9lection compl\u00e8te");
      WTOUM.setComponentPopupMenu(BTD);
      WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      WTOUM.setName("WTOUM");
      WTOUM.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          WTOUMActionPerformed(e);
        }
      });

      //---- CPRI2 ----
      CPRI2.setText("Prix derni\u00e8re entr\u00e9e");
      CPRI2.setComponentPopupMenu(BTD);
      CPRI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      CPRI2.setName("CPRI2");

      //---- CPRI1 ----
      CPRI1.setText("Prix de revient");
      CPRI1.setComponentPopupMenu(BTD);
      CPRI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      CPRI1.setName("CPRI1");

      //---- CPRI3 ----
      CPRI3.setText("Prix d'inventaire");
      CPRI3.setComponentPopupMenu(BTD);
      CPRI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      CPRI3.setName("CPRI3");

      //---- CPRI5 ----
      CPRI5.setText("Prix F.I.F.O");
      CPRI5.setComponentPopupMenu(BTD);
      CPRI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      CPRI5.setName("CPRI5");

      //---- CPRI4 ----
      CPRI4.setText("P.U.M.P");
      CPRI4.setComponentPopupMenu(BTD);
      CPRI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      CPRI4.setName("CPRI4");

      //---- DATCHF ----
      DATCHF.setComponentPopupMenu(BTD);
      DATCHF.setName("DATCHF");

      //---- DATREC ----
      DATREC.setComponentPopupMenu(BTD);
      DATREC.setName("DATREC");

      //---- WETB ----
      WETB.setComponentPopupMenu(BTD);
      WETB.setName("WETB");

      //---- OBJ_71 ----
      OBJ_71.setText("Date");
      OBJ_71.setName("OBJ_71");

      //---- OBJ_73 ----
      OBJ_73.setText("Date");
      OBJ_73.setName("OBJ_73");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      //======== P_SEL1 ========
      {
        P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
        P_SEL1.setName("P_SEL1");
        P_SEL1.setLayout(null);

        //---- EDEBA ----
        EDEBA.setComponentPopupMenu(BTD);
        EDEBA.setName("EDEBA");
        P_SEL1.add(EDEBA);
        EDEBA.setBounds(35, 10, 210, EDEBA.getPreferredSize().height);

        //---- EFINA ----
        EFINA.setComponentPopupMenu(BTD);
        EFINA.setName("EFINA");
        P_SEL1.add(EFINA);
        EFINA.setBounds(275, 10, 210, EFINA.getPreferredSize().height);

        //---- OBJ_34 ----
        OBJ_34.setText("\u00e0");
        OBJ_34.setName("OBJ_34");
        P_SEL1.add(OBJ_34);
        OBJ_34.setBounds(255, 15, 12, 18);

        //---- OBJ_36 ----
        OBJ_36.setText("De");
        OBJ_36.setName("OBJ_36");
        P_SEL1.add(OBJ_36);
        OBJ_36.setBounds(10, 15, 21, 18);
      }

      //======== P_SEL2 ========
      {
        P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
        P_SEL2.setName("P_SEL2");
        P_SEL2.setLayout(null);

        //---- DEBIAK ----
        DEBIAK.setComponentPopupMenu(BTD);
        DEBIAK.setName("DEBIAK");
        P_SEL2.add(DEBIAK);
        DEBIAK.setBounds(35, 10, 160, DEBIAK.getPreferredSize().height);

        //---- FINIAK ----
        FINIAK.setComponentPopupMenu(BTD);
        FINIAK.setName("FINIAK");
        P_SEL2.add(FINIAK);
        FINIAK.setBounds(220, 10, 160, FINIAK.getPreferredSize().height);

        //---- OBJ_37 ----
        OBJ_37.setText("De");
        OBJ_37.setName("OBJ_37");
        P_SEL2.add(OBJ_37);
        OBJ_37.setBounds(10, 15, 21, 18);

        //---- OBJ_33 ----
        OBJ_33.setText("\u00e0");
        OBJ_33.setName("OBJ_33");
        P_SEL2.add(OBJ_33);
        OBJ_33.setBounds(200, 15, 12, 18);
      }

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 690, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5)
            .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
            .addGap(52, 52, 52)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 690, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
            .addGap(19, 19, 19)
            .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 442, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 690, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
            .addGap(19, 19, 19)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
              .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
              .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 390, GroupLayout.PREFERRED_SIZE)))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
            .addGap(35, 35, 35)
            .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(DATCHF, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
            .addGap(175, 175, 175)
            .addComponent(CPRI1, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
            .addGap(60, 60, 60)
            .addComponent(CPRI2, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(DATREC, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
            .addGap(175, 175, 175)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(CPRI3, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
              .addComponent(CPRI4, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(415, 415, 415)
            .addComponent(CPRI5, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
            .addGap(12, 12, 12)
            .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
            .addGap(12, 12, 12)
            .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(P_SEL3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
              .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
              .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(16, 16, 16)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(DATCHF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(CPRI1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            .addGap(7, 7, 7)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(CPRI2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(DATREC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(CPRI3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(CPRI4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            .addGap(12, 12, 12)
            .addComponent(CPRI5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(CPRI2);
    RB_GRP.add(CPRI1);
    RB_GRP.add(CPRI3);
    RB_GRP.add(CPRI5);
    RB_GRP.add(CPRI4);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel l_LOCTP;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_26;
  private JXTitledSeparator OBJ_48;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_43;
  private JXTitledSeparator OBJ_29;
  private JXTitledSeparator OBJ_41;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPanel P_SEL3;
  private JLabel OBJ_38;
  private JLabel OBJ_35;
  private XRiTextField EFIN;
  private XRiTextField EDEB;
  private JLabel OBJ_44;
  private JLabel OBJ_46;
  private XRiCheckBox REPON1;
  private XRiCheckBox WTOU;
  private XRiCheckBox WTOUM;
  private XRiRadioButton CPRI2;
  private XRiRadioButton CPRI1;
  private XRiRadioButton CPRI3;
  private XRiRadioButton CPRI5;
  private XRiRadioButton CPRI4;
  private XRiCalendrier DATCHF;
  private XRiCalendrier DATREC;
  private XRiTextField WETB;
  private JLabel OBJ_71;
  private JLabel OBJ_73;
  private JButton BT_ChgSoc;
  private JPanel P_SEL1;
  private XRiTextField EDEBA;
  private XRiTextField EFINA;
  private JLabel OBJ_34;
  private JLabel OBJ_36;
  private JPanel P_SEL2;
  private XRiTextField DEBIAK;
  private XRiTextField FINIAK;
  private JLabel OBJ_37;
  private JLabel OBJ_33;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
