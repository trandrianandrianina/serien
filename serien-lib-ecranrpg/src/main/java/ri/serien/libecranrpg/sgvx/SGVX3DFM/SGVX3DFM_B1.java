
package ri.serien.libecranrpg.sgvx.SGVX3DFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SGVX3DFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public SGVX3DFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    CDEINT.setValeursSelection("1", " ");
    LIGAFF.setValeursSelection("OUI", "NON");
    OPT2.setValeursSelection("1", " ");
    OPT1.setValeursSelection("1", " ");
    WTVTE.setValeursSelection("**", "  ");
    WTRI.setValeursSelection("OUI", "NON");
    
    WDU.setValeurs("E", WDU_GRP);
    WDU_E.setValeurs("N");
    WDU_T.setValeurs(" ");
    
    WAS.setValeurs("E", WAS_GRP);
    WAS_E.setValeurs("N");
    WAS_T.setValeurs(" ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    NOMFRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMFRS@")).trim());
    AHLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AHLIBR@")).trim());
    NOMCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMCLI@")).trim());
    VDLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VDLIBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTVTEActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!WTVTE.isSelected());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!WTOU.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_72 = new JXTitledSeparator();
    pnlPeriode = new JPanel();
    OBJ_73 = new JLabel();
    OBJ_75 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    OBJ_50 = new JXTitledSeparator();
    pnlFournisseur = new JPanel();
    lbNumeroFRS = new JLabel();
    DEBFRS = new XRiTextField();
    DEBCOL = new XRiTextField();
    lbRechercheFRS = new JLabel();
    WRFRS = new XRiTextField();
    NOMFRS = new RiZoneSortie();
    OBJ_40 = new JXTitledSeparator();
    pnlCommandeAchat = new JPanel();
    P_SEL0 = new JPanel();
    OBJ_43 = new JLabel();
    OBJ_46 = new JLabel();
    NUMDEB = new XRiTextField();
    NUMFIN = new XRiTextField();
    SUFDEB = new XRiTextField();
    SUFFIN = new XRiTextField();
    WTOU = new XRiCheckBox();
    lbAcheteur = new JLabel();
    WACH = new XRiTextField();
    AHLIBR = new RiZoneSortie();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    CDEINT = new XRiCheckBox();
    LIGAFF = new XRiCheckBox();
    OBJ_64 = new JXTitledSeparator();
    pnlClient = new JPanel();
    lbRechercheClient = new JLabel();
    WRCLI = new XRiTextField();
    lbNumeroClient = new JLabel();
    DEBCLI = new XRiTextField();
    NOMCLI = new RiZoneSortie();
    OBJ_54 = new JXTitledSeparator();
    pnlCommandeVente = new JPanel();
    P_SEL1 = new JPanel();
    OBJ_57 = new JLabel();
    OBJ_60 = new JLabel();
    VTEDEB = new XRiTextField();
    VTEFIN = new XRiTextField();
    VTEDES = new XRiTextField();
    VTEFIS = new XRiTextField();
    WTVTE = new XRiCheckBox();
    WTRI = new XRiCheckBox();
    lbVendeur = new JLabel();
    WVDE = new XRiTextField();
    VDLIBR = new RiZoneSortie();
    pnlDirectUsine = new JPanel();
    lbDirectUsine = new JLabel();
    WDU = new XRiRadioButton();
    WDU_E = new XRiRadioButton();
    WDU_T = new XRiRadioButton();
    pnlArticlesSpeciaux = new JPanel();
    lbArtSpeciaux = new JLabel();
    WAS = new XRiRadioButton();
    WAS_E = new XRiRadioButton();
    WAS_T = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    WDU_GRP = new ButtonGroup();
    WAS_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 570));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_72 ----
          OBJ_72.setTitle("P\u00e9riode \u00e0 traiter");
          OBJ_72.setName("OBJ_72");

          //======== pnlPeriode ========
          {
            pnlPeriode.setOpaque(false);
            pnlPeriode.setName("pnlPeriode");
            pnlPeriode.setLayout(null);

            //---- OBJ_73 ----
            OBJ_73.setText("du");
            OBJ_73.setName("OBJ_73");
            pnlPeriode.add(OBJ_73);
            OBJ_73.setBounds(15, 10, 35, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("au");
            OBJ_75.setName("OBJ_75");
            pnlPeriode.add(OBJ_75);
            OBJ_75.setBounds(195, 10, 18, 20);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            pnlPeriode.add(DATDEB);
            DATDEB.setBounds(60, 6, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            pnlPeriode.add(DATFIN);
            DATFIN.setBounds(230, 6, 105, DATFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlPeriode.getComponentCount(); i++) {
                Rectangle bounds = pnlPeriode.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlPeriode.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlPeriode.setMinimumSize(preferredSize);
              pnlPeriode.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_50 ----
          OBJ_50.setTitle("Fournisseur");
          OBJ_50.setName("OBJ_50");

          //======== pnlFournisseur ========
          {
            pnlFournisseur.setOpaque(false);
            pnlFournisseur.setName("pnlFournisseur");
            pnlFournisseur.setLayout(null);

            //---- lbNumeroFRS ----
            lbNumeroFRS.setText("ou num\u00e9ro");
            lbNumeroFRS.setName("lbNumeroFRS");
            pnlFournisseur.add(lbNumeroFRS);
            lbNumeroFRS.setBounds(390, 5, 80, 28);

            //---- DEBFRS ----
            DEBFRS.setComponentPopupMenu(BTD);
            DEBFRS.setName("DEBFRS");
            pnlFournisseur.add(DEBFRS);
            DEBFRS.setBounds(490, 5, 58, DEBFRS.getPreferredSize().height);

            //---- DEBCOL ----
            DEBCOL.setComponentPopupMenu(BTD);
            DEBCOL.setName("DEBCOL");
            pnlFournisseur.add(DEBCOL);
            DEBCOL.setBounds(470, 5, 20, DEBCOL.getPreferredSize().height);

            //---- lbRechercheFRS ----
            lbRechercheFRS.setText("Recherche");
            lbRechercheFRS.setName("lbRechercheFRS");
            pnlFournisseur.add(lbRechercheFRS);
            lbRechercheFRS.setBounds(15, 5, 80, 28);

            //---- WRFRS ----
            WRFRS.setComponentPopupMenu(BTD);
            WRFRS.setName("WRFRS");
            pnlFournisseur.add(WRFRS);
            WRFRS.setBounds(175, 5, 150, WRFRS.getPreferredSize().height);

            //---- NOMFRS ----
            NOMFRS.setText("@NOMFRS@");
            NOMFRS.setName("NOMFRS");
            pnlFournisseur.add(NOMFRS);
            NOMFRS.setBounds(550, 7, 310, NOMFRS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlFournisseur.getComponentCount(); i++) {
                Rectangle bounds = pnlFournisseur.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlFournisseur.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlFournisseur.setMinimumSize(preferredSize);
              pnlFournisseur.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_40 ----
          OBJ_40.setTitle("Commande d'achat");
          OBJ_40.setName("OBJ_40");

          //======== pnlCommandeAchat ========
          {
            pnlCommandeAchat.setOpaque(false);
            pnlCommandeAchat.setName("pnlCommandeAchat");
            pnlCommandeAchat.setLayout(null);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_43 ----
              OBJ_43.setText("Num\u00e9ro de d\u00e9but");
              OBJ_43.setName("OBJ_43");
              P_SEL0.add(OBJ_43);
              OBJ_43.setBounds(10, 13, 110, 20);

              //---- OBJ_46 ----
              OBJ_46.setText("Num\u00e9ro de fin");
              OBJ_46.setName("OBJ_46");
              P_SEL0.add(OBJ_46);
              OBJ_46.setBounds(220, 13, 110, 20);

              //---- NUMDEB ----
              NUMDEB.setComponentPopupMenu(BTD);
              NUMDEB.setName("NUMDEB");
              P_SEL0.add(NUMDEB);
              NUMDEB.setBounds(124, 9, 58, NUMDEB.getPreferredSize().height);

              //---- NUMFIN ----
              NUMFIN.setComponentPopupMenu(BTD);
              NUMFIN.setName("NUMFIN");
              P_SEL0.add(NUMFIN);
              NUMFIN.setBounds(330, 9, 58, NUMFIN.getPreferredSize().height);

              //---- SUFDEB ----
              SUFDEB.setComponentPopupMenu(BTD);
              SUFDEB.setName("SUFDEB");
              P_SEL0.add(SUFDEB);
              SUFDEB.setBounds(185, 9, 20, SUFDEB.getPreferredSize().height);

              //---- SUFFIN ----
              SUFFIN.setComponentPopupMenu(BTD);
              SUFFIN.setName("SUFFIN");
              P_SEL0.add(SUFFIN);
              SUFFIN.setBounds(390, 9, 20, SUFFIN.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL0.setMinimumSize(preferredSize);
                P_SEL0.setPreferredSize(preferredSize);
              }
            }
            pnlCommandeAchat.add(P_SEL0);
            P_SEL0.setBounds(175, 10, 420, 45);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            pnlCommandeAchat.add(WTOU);
            WTOU.setBounds(15, 23, 141, 18);

            //---- lbAcheteur ----
            lbAcheteur.setText("Acheteur");
            lbAcheteur.setName("lbAcheteur");
            pnlCommandeAchat.add(lbAcheteur);
            lbAcheteur.setBounds(15, 59, 80, 28);

            //---- WACH ----
            WACH.setComponentPopupMenu(BTD);
            WACH.setName("WACH");
            pnlCommandeAchat.add(WACH);
            WACH.setBounds(175, 59, 40, WACH.getPreferredSize().height);

            //---- AHLIBR ----
            AHLIBR.setText("@AHLIBR@");
            AHLIBR.setName("AHLIBR");
            pnlCommandeAchat.add(AHLIBR);
            AHLIBR.setBounds(220, 61, 260, AHLIBR.getPreferredSize().height);

            //---- OPT1 ----
            OPT1.setText("Commandes valid\u00e9es");
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            pnlCommandeAchat.add(OPT1);
            OPT1.setBounds(640, 5, 160, 20);

            //---- OPT2 ----
            OPT2.setText("Commandes r\u00e9ceptionn\u00e9es");
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            pnlCommandeAchat.add(OPT2);
            OPT2.setBounds(640, 45, 191, 20);

            //---- CDEINT ----
            CDEINT.setText("Commandes internes uniquement");
            CDEINT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CDEINT.setName("CDEINT");
            pnlCommandeAchat.add(CDEINT);
            CDEINT.setBounds(640, 65, 225, 20);

            //---- LIGAFF ----
            LIGAFF.setText("Lignes affect\u00e9es seulement");
            LIGAFF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LIGAFF.setName("LIGAFF");
            pnlCommandeAchat.add(LIGAFF);
            LIGAFF.setBounds(640, 25, 188, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlCommandeAchat.getComponentCount(); i++) {
                Rectangle bounds = pnlCommandeAchat.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlCommandeAchat.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlCommandeAchat.setMinimumSize(preferredSize);
              pnlCommandeAchat.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_64 ----
          OBJ_64.setTitle("Client");
          OBJ_64.setName("OBJ_64");

          //======== pnlClient ========
          {
            pnlClient.setOpaque(false);
            pnlClient.setName("pnlClient");
            pnlClient.setLayout(null);

            //---- lbRechercheClient ----
            lbRechercheClient.setText("Recherche");
            lbRechercheClient.setName("lbRechercheClient");
            pnlClient.add(lbRechercheClient);
            lbRechercheClient.setBounds(15, 5, 80, 28);

            //---- WRCLI ----
            WRCLI.setComponentPopupMenu(BTD);
            WRCLI.setName("WRCLI");
            pnlClient.add(WRCLI);
            WRCLI.setBounds(175, 5, 150, WRCLI.getPreferredSize().height);

            //---- lbNumeroClient ----
            lbNumeroClient.setText("ou num\u00e9ro");
            lbNumeroClient.setName("lbNumeroClient");
            pnlClient.add(lbNumeroClient);
            lbNumeroClient.setBounds(390, 5, 80, 28);

            //---- DEBCLI ----
            DEBCLI.setComponentPopupMenu(BTD);
            DEBCLI.setName("DEBCLI");
            pnlClient.add(DEBCLI);
            DEBCLI.setBounds(470, 5, 58, DEBCLI.getPreferredSize().height);

            //---- NOMCLI ----
            NOMCLI.setText("@NOMCLI@");
            NOMCLI.setName("NOMCLI");
            pnlClient.add(NOMCLI);
            NOMCLI.setBounds(550, 7, 310, NOMCLI.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlClient.getComponentCount(); i++) {
                Rectangle bounds = pnlClient.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlClient.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlClient.setMinimumSize(preferredSize);
              pnlClient.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_54 ----
          OBJ_54.setTitle("Commande de vente");
          OBJ_54.setName("OBJ_54");

          //======== pnlCommandeVente ========
          {
            pnlCommandeVente.setOpaque(false);
            pnlCommandeVente.setName("pnlCommandeVente");
            pnlCommandeVente.setLayout(null);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_57 ----
              OBJ_57.setText("Num\u00e9ro de d\u00e9but");
              OBJ_57.setName("OBJ_57");
              P_SEL1.add(OBJ_57);
              OBJ_57.setBounds(15, 14, 110, 20);

              //---- OBJ_60 ----
              OBJ_60.setText("Num\u00e9ro de fin");
              OBJ_60.setName("OBJ_60");
              P_SEL1.add(OBJ_60);
              OBJ_60.setBounds(235, 14, 94, 20);

              //---- VTEDEB ----
              VTEDEB.setComponentPopupMenu(BTD);
              VTEDEB.setName("VTEDEB");
              P_SEL1.add(VTEDEB);
              VTEDEB.setBounds(120, 10, 58, VTEDEB.getPreferredSize().height);

              //---- VTEFIN ----
              VTEFIN.setComponentPopupMenu(BTD);
              VTEFIN.setName("VTEFIN");
              P_SEL1.add(VTEFIN);
              VTEFIN.setBounds(330, 10, 58, VTEFIN.getPreferredSize().height);

              //---- VTEDES ----
              VTEDES.setComponentPopupMenu(BTD);
              VTEDES.setName("VTEDES");
              P_SEL1.add(VTEDES);
              VTEDES.setBounds(185, 10, 20, VTEDES.getPreferredSize().height);

              //---- VTEFIS ----
              VTEFIS.setComponentPopupMenu(BTD);
              VTEFIS.setName("VTEFIS");
              P_SEL1.add(VTEFIS);
              VTEFIS.setBounds(390, 10, 20, VTEFIS.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL1.setMinimumSize(preferredSize);
                P_SEL1.setPreferredSize(preferredSize);
              }
            }
            pnlCommandeVente.add(P_SEL1);
            P_SEL1.setBounds(175, 10, 420, 45);

            //---- WTVTE ----
            WTVTE.setText("S\u00e9lection compl\u00e8te");
            WTVTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTVTE.setName("WTVTE");
            WTVTE.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTVTEActionPerformed(e);
              }
            });
            pnlCommandeVente.add(WTVTE);
            WTVTE.setBounds(15, 23, 141, 18);

            //---- WTRI ----
            WTRI.setText("Edition tri\u00e9e par num\u00e9ro client");
            WTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTRI.setName("WTRI");
            pnlCommandeVente.add(WTRI);
            WTRI.setBounds(640, 22, 200, 20);

            //---- lbVendeur ----
            lbVendeur.setText("Vendeur");
            lbVendeur.setName("lbVendeur");
            pnlCommandeVente.add(lbVendeur);
            lbVendeur.setBounds(15, 65, 80, 28);

            //---- WVDE ----
            WVDE.setComponentPopupMenu(BTD);
            WVDE.setName("WVDE");
            pnlCommandeVente.add(WVDE);
            WVDE.setBounds(175, 65, 40, WVDE.getPreferredSize().height);

            //---- VDLIBR ----
            VDLIBR.setText("@VDLIBR@");
            VDLIBR.setName("VDLIBR");
            pnlCommandeVente.add(VDLIBR);
            VDLIBR.setBounds(220, 67, 260, VDLIBR.getPreferredSize().height);

            //======== pnlDirectUsine ========
            {
              pnlDirectUsine.setBorder(new LineBorder(Color.lightGray));
              pnlDirectUsine.setOpaque(false);
              pnlDirectUsine.setName("pnlDirectUsine");
              pnlDirectUsine.setLayout(null);

              //---- lbDirectUsine ----
              lbDirectUsine.setText("Direct usine");
              lbDirectUsine.setName("lbDirectUsine");
              pnlDirectUsine.add(lbDirectUsine);
              lbDirectUsine.setBounds(5, 5, 100, 28);

              //---- WDU ----
              WDU.setText("Inclus");
              WDU.setComponentPopupMenu(null);
              WDU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WDU.setName("WDU");
              pnlDirectUsine.add(WDU);
              WDU.setBounds(110, 6, 65, 26);

              //---- WDU_E ----
              WDU_E.setText("Exclu");
              WDU_E.setComponentPopupMenu(null);
              WDU_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WDU_E.setName("WDU_E");
              pnlDirectUsine.add(WDU_E);
              WDU_E.setBounds(175, 6, 65, 26);

              //---- WDU_T ----
              WDU_T.setText("Tous");
              WDU_T.setComponentPopupMenu(null);
              WDU_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WDU_T.setName("WDU_T");
              pnlDirectUsine.add(WDU_T);
              WDU_T.setBounds(235, 6, 65, 26);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < pnlDirectUsine.getComponentCount(); i++) {
                  Rectangle bounds = pnlDirectUsine.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = pnlDirectUsine.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                pnlDirectUsine.setMinimumSize(preferredSize);
                pnlDirectUsine.setPreferredSize(preferredSize);
              }
            }
            pnlCommandeVente.add(pnlDirectUsine);
            pnlDirectUsine.setBounds(175, 100, 300, 40);

            //======== pnlArticlesSpeciaux ========
            {
              pnlArticlesSpeciaux.setBorder(new LineBorder(Color.lightGray));
              pnlArticlesSpeciaux.setOpaque(false);
              pnlArticlesSpeciaux.setName("pnlArticlesSpeciaux");
              pnlArticlesSpeciaux.setLayout(null);

              //---- lbArtSpeciaux ----
              lbArtSpeciaux.setText("Articles sp\u00e9ciaux");
              lbArtSpeciaux.setName("lbArtSpeciaux");
              pnlArticlesSpeciaux.add(lbArtSpeciaux);
              lbArtSpeciaux.setBounds(5, 5, 100, 28);

              //---- WAS ----
              WAS.setText("Inclus");
              WAS.setComponentPopupMenu(null);
              WAS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WAS.setName("WAS");
              pnlArticlesSpeciaux.add(WAS);
              WAS.setBounds(110, 6, 65, 26);

              //---- WAS_E ----
              WAS_E.setText("Exclu");
              WAS_E.setComponentPopupMenu(null);
              WAS_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WAS_E.setName("WAS_E");
              pnlArticlesSpeciaux.add(WAS_E);
              WAS_E.setBounds(175, 6, 65, 26);

              //---- WAS_T ----
              WAS_T.setText("Tous");
              WAS_T.setComponentPopupMenu(null);
              WAS_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WAS_T.setName("WAS_T");
              pnlArticlesSpeciaux.add(WAS_T);
              WAS_T.setBounds(235, 6, 65, 26);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < pnlArticlesSpeciaux.getComponentCount(); i++) {
                  Rectangle bounds = pnlArticlesSpeciaux.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = pnlArticlesSpeciaux.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                pnlArticlesSpeciaux.setMinimumSize(preferredSize);
                pnlArticlesSpeciaux.setPreferredSize(preferredSize);
              }
            }
            pnlCommandeVente.add(pnlArticlesSpeciaux);
            pnlArticlesSpeciaux.setBounds(520, 100, 300, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlCommandeVente.getComponentCount(); i++) {
                Rectangle bounds = pnlCommandeVente.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlCommandeVente.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlCommandeVente.setMinimumSize(preferredSize);
              pnlCommandeVente.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(pnlPeriode, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)
                  .addComponent(pnlFournisseur, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)
                  .addComponent(pnlCommandeAchat, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(pnlClient, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)
                  .addComponent(pnlCommandeVente, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(pnlPeriode, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(pnlFournisseur, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(pnlCommandeAchat, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(pnlClient, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(pnlCommandeVente, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- WDU_GRP ----
    WDU_GRP.add(WDU);
    WDU_GRP.add(WDU_E);
    WDU_GRP.add(WDU_T);

    //---- WAS_GRP ----
    WAS_GRP.add(WAS);
    WAS_GRP.add(WAS_E);
    WAS_GRP.add(WAS_T);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_72;
  private JPanel pnlPeriode;
  private JLabel OBJ_73;
  private JLabel OBJ_75;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JXTitledSeparator OBJ_50;
  private JPanel pnlFournisseur;
  private JLabel lbNumeroFRS;
  private XRiTextField DEBFRS;
  private XRiTextField DEBCOL;
  private JLabel lbRechercheFRS;
  private XRiTextField WRFRS;
  private RiZoneSortie NOMFRS;
  private JXTitledSeparator OBJ_40;
  private JPanel pnlCommandeAchat;
  private JPanel P_SEL0;
  private JLabel OBJ_43;
  private JLabel OBJ_46;
  private XRiTextField NUMDEB;
  private XRiTextField NUMFIN;
  private XRiTextField SUFDEB;
  private XRiTextField SUFFIN;
  private XRiCheckBox WTOU;
  private JLabel lbAcheteur;
  private XRiTextField WACH;
  private RiZoneSortie AHLIBR;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private XRiCheckBox CDEINT;
  private XRiCheckBox LIGAFF;
  private JXTitledSeparator OBJ_64;
  private JPanel pnlClient;
  private JLabel lbRechercheClient;
  private XRiTextField WRCLI;
  private JLabel lbNumeroClient;
  private XRiTextField DEBCLI;
  private RiZoneSortie NOMCLI;
  private JXTitledSeparator OBJ_54;
  private JPanel pnlCommandeVente;
  private JPanel P_SEL1;
  private JLabel OBJ_57;
  private JLabel OBJ_60;
  private XRiTextField VTEDEB;
  private XRiTextField VTEFIN;
  private XRiTextField VTEDES;
  private XRiTextField VTEFIS;
  private XRiCheckBox WTVTE;
  private XRiCheckBox WTRI;
  private JLabel lbVendeur;
  private XRiTextField WVDE;
  private RiZoneSortie VDLIBR;
  private JPanel pnlDirectUsine;
  private JLabel lbDirectUsine;
  private XRiRadioButton WDU;
  private XRiRadioButton WDU_E;
  private XRiRadioButton WDU_T;
  private JPanel pnlArticlesSpeciaux;
  private JLabel lbArtSpeciaux;
  private XRiRadioButton WAS;
  private XRiRadioButton WAS_E;
  private XRiRadioButton WAS_T;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private ButtonGroup WDU_GRP;
  private ButtonGroup WAS_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
