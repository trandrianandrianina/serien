
package ri.serien.libecranrpg.sgvx.SGVX60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGVX60FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _MOI1_Title = { "Mois", "Sortie", };
  private String[][] _MOI1_Data = { { "MOI1", "S1X01", }, { "MOI2", "S1X02", }, { "MOI3", "S1X03", }, { "MOI4", "S1X04", },
      { "MOI5", "S1X05", }, { "MOI6", "S1X06", }, { "MOI7", "S1X07", }, { "MOI8", "S1X08", }, { "MOI9", "S1X09", }, { "MOI10", "S1X10", },
      { "MOI11", "S1X11", }, { "MOI12", "S1X12", }, };
  private int[] _MOI1_Width = { 40, 85, };
  private String[] _MOI13_Title = { "Mois", "Sortie", };
  private String[][] _MOI13_Data = { { "MOI13", "S1X13", }, { "MOI14", "S1X14", }, { "MOI15", "S1X15", }, { "MOI16", "S1X16", },
      { "MOI17", "S1X17", }, { "MOI18", "S1X18", }, { "MOI19", "S1X19", }, { "MOI20", "S1X20", }, { "MOI21", "S1X21", },
      { "MOI22", "S1X22", }, { "MOI23", "S1X23", }, { "MOI24", "S1X24", }, };
  private int[] _MOI13_Width = { 40, 85, };
  private ODialog dialog_REST = null;
  
  public SGVX60FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    MOI1.setAspectTable(null, _MOI1_Title, _MOI1_Data, _MOI1_Width, false, null, null, null, null);
    MOI13.setAspectTable(null, _MOI13_Title, _MOI13_Data, _MOI13_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TOT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT1@")).trim());
    TOT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT2@")).trim());
    TOTAUX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTAUX@")).trim());
    MOYM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOYM@")).trim());
    P60X01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P60X01@")).trim());
    P60X02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P60X02@")).trim());
    P60X03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P60X03@")).trim());
    P60X04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P60X04@")).trim());
    P60X05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P60X05@")).trim());
    P60X06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P60X06@")).trim());
    S2PDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S2PDX@")).trim());
    S2PFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S2PFX@")).trim());
    S1PDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S1PDX@")).trim());
    S1PFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S1PFX@")).trim());
    S0PDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S0PDX@")).trim());
    S0PFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S0PFX@")).trim());
    S30PDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S30PDX@")).trim());
    S30PFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S30PFX@")).trim());
    S20PDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S20PDX@")).trim());
    S20PFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S20PFX@")).trim());
    S10PDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S10PDX@")).trim());
    S10PFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S10PFX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    // majTable( MOI13, MOI13.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    S10PFX.setVisible(lexique.isPresent("S10PFX"));
    S10PDX.setVisible(lexique.isPresent("S10PDX"));
    S20PFX.setVisible(lexique.isPresent("S20PFX"));
    S20PDX.setVisible(lexique.isPresent("S20PDX"));
    S30PFX.setVisible(lexique.isPresent("S30PFX"));
    S30PDX.setVisible(lexique.isPresent("S30PDX"));
    S0PFX.setVisible(lexique.isPresent("S0PFX"));
    S0PDX.setVisible(lexique.isPresent("S0PDX"));
    S1PFX.setVisible(lexique.isPresent("S1PFX"));
    S1PDX.setVisible(lexique.isPresent("S1PDX"));
    S2PFX.setVisible(lexique.isPresent("S2PFX"));
    S2PDX.setVisible(lexique.isPresent("S2PDX"));
    
    // Titre
    if (lexique.isTrue("91")) {
      setTitle(interpreteurD.analyseExpression("Historique consommation regroupé"));
    }
    else {
      setTitle(interpreteurD.analyseExpression("Historique consommation"));
    }
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/stats.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    new SGVX60FM_G01(this, lexique, interpreteurD);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    new SGVX60FM_G02(this, lexique, interpreteurD);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    if (dialog_REST == null) {
      dialog_REST = new ODialog((Window) getTopLevelAncestor(), new SGVX60FM_REST(this));
    }
    dialog_REST.affichePopupPerso();
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    OBJ_57 = new JLabel();
    OBJ_37 = new JLabel();
    TOT1 = new RiZoneSortie();
    TOT2 = new RiZoneSortie();
    TOTAUX = new RiZoneSortie();
    MOYM = new RiZoneSortie();
    OBJ_48 = new JLabel();
    OBJ_50 = new JLabel();
    SCROLLPANE_OBJ_15 = new JScrollPane();
    MOI1 = new XRiTable();
    SCROLLPANE_OBJ_18 = new JScrollPane();
    MOI13 = new XRiTable();
    panel6 = new JPanel();
    T01 = new JLabel();
    T02 = new JLabel();
    T03 = new JLabel();
    T04 = new JLabel();
    T05 = new JLabel();
    T06 = new JLabel();
    P60X01 = new RiZoneSortie();
    P60X02 = new RiZoneSortie();
    P60X03 = new RiZoneSortie();
    P60X04 = new RiZoneSortie();
    P60X05 = new RiZoneSortie();
    P60X06 = new RiZoneSortie();
    S2PDX = new RiZoneSortie();
    S2PFX = new RiZoneSortie();
    S1PDX = new RiZoneSortie();
    S1PFX = new RiZoneSortie();
    S0PDX = new RiZoneSortie();
    S0PFX = new RiZoneSortie();
    S30PDX = new RiZoneSortie();
    S30PFX = new RiZoneSortie();
    S20PDX = new RiZoneSortie();
    S20PFX = new RiZoneSortie();
    S10PDX = new RiZoneSortie();
    S10PFX = new RiZoneSortie();
    OBJ_25 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_55 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(905, 400));
    setPreferredSize(new Dimension(905, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Historique corrig\u00e9");
              riSousMenu_bt8.setToolTipText("Historique corrig\u00e9");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Restrictions");
              riSousMenu_bt10.setToolTipText("Restrictions");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder("Historique"));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- OBJ_57 ----
          OBJ_57.setText("Total des 24 derniers mois");
          OBJ_57.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_57.setName("OBJ_57");
          panel4.add(OBJ_57);
          OBJ_57.setBounds(80, 298, 185, 18);

          //---- OBJ_37 ----
          OBJ_37.setText("Moyenne mensuelle");
          OBJ_37.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_37.setName("OBJ_37");
          panel4.add(OBJ_37);
          OBJ_37.setBounds(123, 332, 142, 20);

          //---- TOT1 ----
          TOT1.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT1.setText("@TOT1@");
          TOT1.setName("TOT1");
          panel4.add(TOT1);
          TOT1.setBounds(95, 260, 95, TOT1.getPreferredSize().height);

          //---- TOT2 ----
          TOT2.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT2.setText("@TOT2@");
          TOT2.setName("TOT2");
          panel4.add(TOT2);
          TOT2.setBounds(275, 260, 95, TOT2.getPreferredSize().height);

          //---- TOTAUX ----
          TOTAUX.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTAUX.setText("@TOTAUX@");
          TOTAUX.setName("TOTAUX");
          panel4.add(TOTAUX);
          TOTAUX.setBounds(275, 295, 95, TOTAUX.getPreferredSize().height);

          //---- MOYM ----
          MOYM.setHorizontalAlignment(SwingConstants.RIGHT);
          MOYM.setText("@MOYM@");
          MOYM.setName("MOYM");
          panel4.add(MOYM);
          MOYM.setBounds(275, 330, 95, MOYM.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("Total");
          OBJ_48.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_48.setName("OBJ_48");
          panel4.add(OBJ_48);
          OBJ_48.setBounds(35, 261, 50, 23);

          //---- OBJ_50 ----
          OBJ_50.setText("Total");
          OBJ_50.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_50.setName("OBJ_50");
          panel4.add(OBJ_50);
          OBJ_50.setBounds(215, 261, 50, 23);

          //======== SCROLLPANE_OBJ_15 ========
          {
            SCROLLPANE_OBJ_15.setName("SCROLLPANE_OBJ_15");

            //---- MOI1 ----
            MOI1.setFont(new Font("Courier New", Font.PLAIN, 12));
            MOI1.setName("MOI1");
            SCROLLPANE_OBJ_15.setViewportView(MOI1);
          }
          panel4.add(SCROLLPANE_OBJ_15);
          SCROLLPANE_OBJ_15.setBounds(15, 30, 175, 225);

          //======== SCROLLPANE_OBJ_18 ========
          {
            SCROLLPANE_OBJ_18.setName("SCROLLPANE_OBJ_18");

            //---- MOI13 ----
            MOI13.setName("MOI13");
            SCROLLPANE_OBJ_18.setViewportView(MOI13);
          }
          panel4.add(SCROLLPANE_OBJ_18);
          SCROLLPANE_OBJ_18.setBounds(200, 30, 175, 225);
        }
        p_contenu.add(panel4);
        panel4.setBounds(5, 5, 390, 390);

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("R\u00e9capitulatif"));
          panel6.setOpaque(false);
          panel6.setName("panel6");

          //---- T01 ----
          T01.setText("Ant\u00e9p\u00e9nulti\u00e8me semestre");
          T01.setName("T01");

          //---- T02 ----
          T02.setText("P\u00e9nulti\u00e8me semestre");
          T02.setName("T02");

          //---- T03 ----
          T03.setText("Semestre en cours");
          T03.setName("T03");

          //---- T04 ----
          T04.setText("Trois derni\u00e8res d\u00e9cades");
          T04.setName("T04");

          //---- T05 ----
          T05.setText("Deux derni\u00e8res d\u00e9cades");
          T05.setName("T05");

          //---- T06 ----
          T06.setText("Derni\u00e8re d\u00e9cade");
          T06.setName("T06");

          //---- P60X01 ----
          P60X01.setHorizontalAlignment(SwingConstants.RIGHT);
          P60X01.setText("@P60X01@");
          P60X01.setName("P60X01");

          //---- P60X02 ----
          P60X02.setHorizontalAlignment(SwingConstants.RIGHT);
          P60X02.setText("@P60X02@");
          P60X02.setName("P60X02");

          //---- P60X03 ----
          P60X03.setHorizontalAlignment(SwingConstants.RIGHT);
          P60X03.setText("@P60X03@");
          P60X03.setName("P60X03");

          //---- P60X04 ----
          P60X04.setHorizontalAlignment(SwingConstants.RIGHT);
          P60X04.setText("@P60X04@");
          P60X04.setName("P60X04");

          //---- P60X05 ----
          P60X05.setHorizontalAlignment(SwingConstants.RIGHT);
          P60X05.setText("@P60X05@");
          P60X05.setName("P60X05");

          //---- P60X06 ----
          P60X06.setHorizontalAlignment(SwingConstants.RIGHT);
          P60X06.setText("@P60X06@");
          P60X06.setName("P60X06");

          //---- S2PDX ----
          S2PDX.setHorizontalAlignment(SwingConstants.CENTER);
          S2PDX.setInheritsPopupMenu(false);
          S2PDX.setText("@S2PDX@");
          S2PDX.setName("S2PDX");

          //---- S2PFX ----
          S2PFX.setHorizontalAlignment(SwingConstants.CENTER);
          S2PFX.setInheritsPopupMenu(false);
          S2PFX.setText("@S2PFX@");
          S2PFX.setName("S2PFX");

          //---- S1PDX ----
          S1PDX.setHorizontalAlignment(SwingConstants.CENTER);
          S1PDX.setInheritsPopupMenu(false);
          S1PDX.setText("@S1PDX@");
          S1PDX.setName("S1PDX");

          //---- S1PFX ----
          S1PFX.setHorizontalAlignment(SwingConstants.CENTER);
          S1PFX.setInheritsPopupMenu(false);
          S1PFX.setText("@S1PFX@");
          S1PFX.setName("S1PFX");

          //---- S0PDX ----
          S0PDX.setHorizontalAlignment(SwingConstants.CENTER);
          S0PDX.setInheritsPopupMenu(false);
          S0PDX.setText("@S0PDX@");
          S0PDX.setName("S0PDX");

          //---- S0PFX ----
          S0PFX.setHorizontalAlignment(SwingConstants.CENTER);
          S0PFX.setInheritsPopupMenu(false);
          S0PFX.setText("@S0PFX@");
          S0PFX.setName("S0PFX");

          //---- S30PDX ----
          S30PDX.setHorizontalAlignment(SwingConstants.CENTER);
          S30PDX.setInheritsPopupMenu(false);
          S30PDX.setText("@S30PDX@");
          S30PDX.setName("S30PDX");

          //---- S30PFX ----
          S30PFX.setHorizontalAlignment(SwingConstants.CENTER);
          S30PFX.setInheritsPopupMenu(false);
          S30PFX.setText("@S30PFX@");
          S30PFX.setName("S30PFX");

          //---- S20PDX ----
          S20PDX.setHorizontalAlignment(SwingConstants.CENTER);
          S20PDX.setInheritsPopupMenu(false);
          S20PDX.setText("@S20PDX@");
          S20PDX.setName("S20PDX");

          //---- S20PFX ----
          S20PFX.setHorizontalAlignment(SwingConstants.CENTER);
          S20PFX.setInheritsPopupMenu(false);
          S20PFX.setText("@S20PFX@");
          S20PFX.setName("S20PFX");

          //---- S10PDX ----
          S10PDX.setHorizontalAlignment(SwingConstants.CENTER);
          S10PDX.setInheritsPopupMenu(false);
          S10PDX.setText("@S10PDX@");
          S10PDX.setName("S10PDX");

          //---- S10PFX ----
          S10PFX.setHorizontalAlignment(SwingConstants.CENTER);
          S10PFX.setInheritsPopupMenu(false);
          S10PFX.setText("@S10PFX@");
          S10PFX.setName("S10PFX");

          //---- OBJ_25 ----
          OBJ_25.setText("\u00e0");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_30 ----
          OBJ_30.setText("\u00e0");
          OBJ_30.setName("OBJ_30");

          //---- OBJ_35 ----
          OBJ_35.setText("\u00e0");
          OBJ_35.setName("OBJ_35");

          //---- OBJ_41 ----
          OBJ_41.setText("\u00e0");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_46 ----
          OBJ_46.setText("\u00e0");
          OBJ_46.setName("OBJ_46");

          //---- OBJ_55 ----
          OBJ_55.setText("\u00e0");
          OBJ_55.setName("OBJ_55");

          GroupLayout panel6Layout = new GroupLayout(panel6);
          panel6.setLayout(panel6Layout);
          panel6Layout.setHorizontalGroup(
            panel6Layout.createParallelGroup()
              .addGroup(panel6Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(panel6Layout.createParallelGroup()
                  .addComponent(T01, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addComponent(S2PDX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(S2PFX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(P60X01, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
                  .addComponent(T02, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addComponent(S1PDX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(S1PFX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(P60X02, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
                  .addComponent(T03, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addComponent(S0PDX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(S0PFX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(P60X03, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
                  .addComponent(T04, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addComponent(S30PDX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(S30PFX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(P60X04, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
                  .addComponent(T05, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addComponent(S20PDX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(S20PFX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(P60X05, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
                  .addComponent(T06, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addComponent(S10PDX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(S10PFX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(P60X06, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))))
          );
          panel6Layout.setVerticalGroup(
            panel6Layout.createParallelGroup()
              .addGroup(panel6Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(T01)
                .addGap(6, 6, 6)
                .addGroup(panel6Layout.createParallelGroup()
                  .addComponent(S2PDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(S2PFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P60X01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(T02)
                .addGap(6, 6, 6)
                .addGroup(panel6Layout.createParallelGroup()
                  .addComponent(S1PDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(S1PFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P60X02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(T03)
                .addGap(6, 6, 6)
                .addGroup(panel6Layout.createParallelGroup()
                  .addComponent(S0PDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(S0PFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P60X03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(T04)
                .addGap(6, 6, 6)
                .addGroup(panel6Layout.createParallelGroup()
                  .addComponent(S30PDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(S30PFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P60X04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(T05)
                .addGap(6, 6, 6)
                .addGroup(panel6Layout.createParallelGroup()
                  .addComponent(S20PDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(S20PFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P60X05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(T06)
                .addGap(6, 6, 6)
                .addGroup(panel6Layout.createParallelGroup()
                  .addComponent(S10PDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(S10PFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P60X06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel6);
        panel6.setBounds(400, 5, 325, 390);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //======== riSousMenu6 ========
    {
      riSousMenu6.setName("riSousMenu6");

      //---- riSousMenu_bt6 ----
      riSousMenu_bt6.setText("Historique de conso");
      riSousMenu_bt6.setToolTipText("Historique de consommation");
      riSousMenu_bt6.setName("riSousMenu_bt6");
      riSousMenu_bt6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt6ActionPerformed(e);
        }
      });
      riSousMenu6.add(riSousMenu_bt6);
    }

    //======== riSousMenu7 ========
    {
      riSousMenu7.setName("riSousMenu7");

      //---- riSousMenu_bt7 ----
      riSousMenu_bt7.setText("Evolution conso");
      riSousMenu_bt7.setToolTipText("Evolution de la consommation");
      riSousMenu_bt7.setName("riSousMenu_bt7");
      riSousMenu_bt7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt7ActionPerformed(e);
        }
      });
      riSousMenu7.add(riSousMenu_bt7);
    }

    //======== riMenu3 ========
    {
      riMenu3.setName("riMenu3");

      //---- riMenu_bt3 ----
      riMenu_bt3.setText("Statistiques");
      riMenu_bt3.setName("riMenu_bt3");
      riMenu3.add(riMenu_bt3);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private JPanel p_contenu;
  private JPanel panel4;
  private JLabel OBJ_57;
  private JLabel OBJ_37;
  private RiZoneSortie TOT1;
  private RiZoneSortie TOT2;
  private RiZoneSortie TOTAUX;
  private RiZoneSortie MOYM;
  private JLabel OBJ_48;
  private JLabel OBJ_50;
  private JScrollPane SCROLLPANE_OBJ_15;
  private XRiTable MOI1;
  private JScrollPane SCROLLPANE_OBJ_18;
  private XRiTable MOI13;
  private JPanel panel6;
  private JLabel T01;
  private JLabel T02;
  private JLabel T03;
  private JLabel T04;
  private JLabel T05;
  private JLabel T06;
  private RiZoneSortie P60X01;
  private RiZoneSortie P60X02;
  private RiZoneSortie P60X03;
  private RiZoneSortie P60X04;
  private RiZoneSortie P60X05;
  private RiZoneSortie P60X06;
  private RiZoneSortie S2PDX;
  private RiZoneSortie S2PFX;
  private RiZoneSortie S1PDX;
  private RiZoneSortie S1PFX;
  private RiZoneSortie S0PDX;
  private RiZoneSortie S0PFX;
  private RiZoneSortie S30PDX;
  private RiZoneSortie S30PFX;
  private RiZoneSortie S20PDX;
  private RiZoneSortie S20PFX;
  private RiZoneSortie S10PDX;
  private RiZoneSortie S10PFX;
  private JLabel OBJ_25;
  private JLabel OBJ_30;
  private JLabel OBJ_35;
  private JLabel OBJ_41;
  private JLabel OBJ_46;
  private JLabel OBJ_55;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
