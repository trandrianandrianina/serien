
package ri.serien.libecranrpg.sgvx.SGVX02FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;

/**
 * @author Stéphane Vénéri
 */
public class SGVX02FM_MD extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVX02FM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // On vérifie quels choix sont autorisés
    riBouton1.setEnabled(!lexique.isTrue("22"));
    riBouton2.setEnabled(!lexique.isTrue("23"));
    riBouton3.setEnabled(!lexique.isTrue("24"));
    riBouton4.setEnabled(!lexique.isTrue("25"));
    riBouton6.setEnabled(lexique.isTrue("20")); // <- Edition Excel marche à l'inverse des autres indicateurs
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix du papier"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riBouton1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 1, "S");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 1, "V");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 1, "H");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 1, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_contenu = new JPanel();
    P_PnlOpts = new JPanel();
    riBouton1 = new SNBoutonLeger();
    riBouton2 = new SNBoutonLeger();
    riBouton3 = new SNBoutonLeger();
    riBouton4 = new SNBoutonLeger();
    panel1 = new JPanel();
    riBouton6 = new SNBoutonLeger();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(435, 325));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 200));
          menus_haut.setPreferredSize(new Dimension(160, 200));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt_export ----
            riSousMenu_bt_export.setText("Modifier la hauteur");
            riSousMenu_bt_export.setToolTipText("Modifier la hauteur");
            riSousMenu_bt_export.setName("riSousMenu_bt_export");
            riSousMenu_bt_export.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt_exportActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt_export);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(530, 320));
        p_contenu.setName("p_contenu");

        //======== P_PnlOpts ========
        {
          P_PnlOpts.setBorder(new TitledBorder("Sortie papier"));
          P_PnlOpts.setOpaque(false);
          P_PnlOpts.setName("P_PnlOpts");
          P_PnlOpts.setLayout(null);

          //---- riBouton1 ----
          riBouton1.setText("Standard zon\u00e9");
          riBouton1.setName("riBouton1");
          riBouton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton1ActionPerformed(e);
            }
          });
          P_PnlOpts.add(riBouton1);
          riBouton1.setBounds(30, 40, 175, riBouton1.getPreferredSize().height);

          //---- riBouton2 ----
          riBouton2.setText("12\" continu");
          riBouton2.setForeground(Color.black);
          riBouton2.setName("riBouton2");
          riBouton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton2ActionPerformed(e);
            }
          });
          P_PnlOpts.add(riBouton2);
          riBouton2.setBounds(30, 70, 175, riBouton2.getPreferredSize().height);

          //---- riBouton3 ----
          riBouton3.setText("Format A4 portrait");
          riBouton3.setForeground(Color.black);
          riBouton3.setName("riBouton3");
          riBouton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton3ActionPerformed(e);
            }
          });
          P_PnlOpts.add(riBouton3);
          riBouton3.setBounds(30, 100, 175, riBouton3.getPreferredSize().height);

          //---- riBouton4 ----
          riBouton4.setText("Format A4 paysage");
          riBouton4.setName("riBouton4");
          riBouton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton4ActionPerformed(e);
            }
          });
          P_PnlOpts.add(riBouton4);
          riBouton4.setBounds(30, 130, 175, riBouton4.getPreferredSize().height);
        }

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Autre type de sortie"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- riBouton6 ----
          riBouton6.setText("Exportation tableur");
          riBouton6.setName("riBouton6");
          riBouton6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton6ActionPerformed(e);
            }
          });
          panel1.add(riBouton6);
          riBouton6.setBounds(30, 40, 175, riBouton6.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(13, 13, 13)
              .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private JPanel p_contenu;
  private JPanel P_PnlOpts;
  private SNBoutonLeger riBouton1;
  private SNBoutonLeger riBouton2;
  private SNBoutonLeger riBouton3;
  private SNBoutonLeger riBouton4;
  private JPanel panel1;
  private SNBoutonLeger riBouton6;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
