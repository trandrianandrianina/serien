
package ri.serien.libecranrpg.sgvx.SGVX55FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX55FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX55FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    EDTDP.setValeursSelection("OUI", "NON");
    EDTDES.setValeursSelection("OUI", "NON");
    EDTRES.setValeursSelection("OUI", "NON");
    RAZNEG.setValeursSelection("OUI", "NON");
    NWABC.setValeursSelection("1", " ");
    WTOU.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    WABC.setEnabled(lexique.isPresent("WABC"));
    ZP5.setVisible(lexique.isPresent("ZP5"));
    ZP4.setVisible(lexique.isPresent("ZP4"));
    ZP3.setVisible(lexique.isPresent("ZP3"));
    ZP2.setVisible(lexique.isPresent("ZP2"));
    ZP1.setVisible(lexique.isPresent("ZP1"));
    
    P_SEL1.setVisible(!WTOU.isSelected());
    
    CPRI1.setSelected(!lexique.HostFieldGetData("CPRI1").trim().equalsIgnoreCase(""));
    CPRI2.setSelected(!lexique.HostFieldGetData("CPRI2").trim().equalsIgnoreCase(""));
    CPRI4.setSelected(!lexique.HostFieldGetData("CPRI4").trim().equalsIgnoreCase(""));
    CPRI5.setSelected(!lexique.HostFieldGetData("CPRI5").trim().equalsIgnoreCase(""));
    
    OBJ_43.setVisible(lexique.isPresent("WTOUM"));
    P_SEL0.setVisible(!WTOUM.isSelected() && lexique.isTrue("N91"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (CPRI1.isSelected()) {
      lexique.HostFieldPutData("CPRI1", 0, "1");
      lexique.HostFieldPutData("CPRI2", 0, "");
      lexique.HostFieldPutData("CPRI4", 0, "");
      lexique.HostFieldPutData("CPRI5", 0, "");
    }
    if (CPRI2.isSelected()) {
      lexique.HostFieldPutData("CPRI1", 0, "");
      lexique.HostFieldPutData("CPRI2", 0, "1");
      lexique.HostFieldPutData("CPRI4", 0, "");
      lexique.HostFieldPutData("CPRI5", 0, "");
    }
    if (CPRI4.isSelected()) {
      lexique.HostFieldPutData("CPRI1", 0, "");
      lexique.HostFieldPutData("CPRI2", 0, "");
      lexique.HostFieldPutData("CPRI4", 0, "1");
      lexique.HostFieldPutData("CPRI5", 0, "");
    }
    if (CPRI5.isSelected()) {
      lexique.HostFieldPutData("CPRI1", 0, "");
      lexique.HostFieldPutData("CPRI2", 0, "");
      lexique.HostFieldPutData("CPRI4", 0, "");
      lexique.HostFieldPutData("CPRI5", 0, "1");
    }
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_59 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    EDEB = new XRiTextField();
    EFIN = new XRiTextField();
    REPON1 = new XRiCheckBox();
    WTOUM = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    panel1 = new JPanel();
    OBJ_69 = new JXTitledSeparator();
    CPRI1 = new JRadioButton();
    CPRI2 = new JRadioButton();
    CPRI4 = new JRadioButton();
    CPRI5 = new JRadioButton();
    panel2 = new JPanel();
    OBJ_68 = new JXTitledSeparator();
    OBJ_72 = new JLabel();
    OBJ_76 = new JXTitledSeparator();
    ZP1 = new XRiTextField();
    ZP2 = new XRiTextField();
    ZP3 = new XRiTextField();
    ZP4 = new XRiTextField();
    ZP5 = new XRiTextField();
    OBJ_38 = new JXTitledSeparator();
    OBJ_89 = new JLabel();
    WABC = new XRiTextField();
    NWABC = new XRiCheckBox();
    DATCHF = new XRiCalendrier();
    panel3 = new JPanel();
    OBJ_37 = new JXTitledSeparator();
    RAZNEG = new XRiCheckBox();
    EDTRES = new XRiCheckBox();
    EDTDES = new XRiCheckBox();
    EDTDP = new XRiCheckBox();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_88 = new JXTitledSeparator();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_59 ----
          OBJ_59.setTitle("Plage de familles");
          OBJ_59.setName("OBJ_59");

          //---- OBJ_43 ----
          OBJ_43.setTitle("Codes magasins");
          OBJ_43.setName("OBJ_43");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- MA01 ----
            MA01.setComponentPopupMenu(BTD);
            MA01.setName("MA01");
            P_SEL0.add(MA01);
            MA01.setBounds(7, 9, 34, MA01.getPreferredSize().height);

            //---- MA02 ----
            MA02.setComponentPopupMenu(BTD);
            MA02.setName("MA02");
            P_SEL0.add(MA02);
            MA02.setBounds(43, 9, 34, MA02.getPreferredSize().height);

            //---- MA03 ----
            MA03.setComponentPopupMenu(BTD);
            MA03.setName("MA03");
            P_SEL0.add(MA03);
            MA03.setBounds(79, 9, 34, MA03.getPreferredSize().height);

            //---- MA04 ----
            MA04.setComponentPopupMenu(BTD);
            MA04.setName("MA04");
            P_SEL0.add(MA04);
            MA04.setBounds(115, 9, 34, MA04.getPreferredSize().height);

            //---- MA05 ----
            MA05.setComponentPopupMenu(BTD);
            MA05.setName("MA05");
            P_SEL0.add(MA05);
            MA05.setBounds(151, 9, 34, MA05.getPreferredSize().height);

            //---- MA06 ----
            MA06.setComponentPopupMenu(BTD);
            MA06.setName("MA06");
            P_SEL0.add(MA06);
            MA06.setBounds(187, 9, 34, MA06.getPreferredSize().height);

            //---- MA07 ----
            MA07.setComponentPopupMenu(BTD);
            MA07.setName("MA07");
            P_SEL0.add(MA07);
            MA07.setBounds(223, 9, 34, MA07.getPreferredSize().height);

            //---- MA08 ----
            MA08.setComponentPopupMenu(BTD);
            MA08.setName("MA08");
            P_SEL0.add(MA08);
            MA08.setBounds(259, 9, 34, MA08.getPreferredSize().height);

            //---- MA09 ----
            MA09.setComponentPopupMenu(BTD);
            MA09.setName("MA09");
            P_SEL0.add(MA09);
            MA09.setBounds(295, 9, 34, MA09.getPreferredSize().height);

            //---- MA10 ----
            MA10.setComponentPopupMenu(BTD);
            MA10.setName("MA10");
            P_SEL0.add(MA10);
            MA10.setBounds(331, 9, 34, MA10.getPreferredSize().height);

            //---- MA11 ----
            MA11.setComponentPopupMenu(BTD);
            MA11.setName("MA11");
            P_SEL0.add(MA11);
            MA11.setBounds(367, 9, 34, MA11.getPreferredSize().height);

            //---- MA12 ----
            MA12.setComponentPopupMenu(BTD);
            MA12.setName("MA12");
            P_SEL0.add(MA12);
            MA12.setBounds(403, 9, 34, MA12.getPreferredSize().height);
          }

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");

            //---- OBJ_65 ----
            OBJ_65.setText("Famille d\u00e9but");
            OBJ_65.setName("OBJ_65");

            //---- OBJ_66 ----
            OBJ_66.setText("Famille fin");
            OBJ_66.setName("OBJ_66");

            //---- EDEB ----
            EDEB.setComponentPopupMenu(BTD);
            EDEB.setName("EDEB");

            //---- EFIN ----
            EFIN.setComponentPopupMenu(BTD);
            EFIN.setName("EFIN");

            GroupLayout P_SEL1Layout = new GroupLayout(P_SEL1);
            P_SEL1.setLayout(P_SEL1Layout);
            P_SEL1Layout.setHorizontalGroup(
              P_SEL1Layout.createParallelGroup()
                .addGroup(P_SEL1Layout.createSequentialGroup()
                  .addGap(18, 18, 18)
                  .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                  .addGap(43, 43, 43)
                  .addComponent(EDEB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                  .addGap(51, 51, 51)
                  .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                  .addGap(56, 56, 56)
                  .addComponent(EFIN, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
            );
            P_SEL1Layout.setVerticalGroup(
              P_SEL1Layout.createParallelGroup()
                .addGroup(P_SEL1Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_SEL1Layout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addComponent(EDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_SEL1Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_SEL1Layout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addComponent(EFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //---- REPON1 ----
          REPON1.setText("Lignes d\u00e9tail articles");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- WTOUM ----
          WTOUM.setText("S\u00e9lection compl\u00e8te");
          WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUM.setName("WTOUM");
          WTOUM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUMActionPerformed(e);
            }
          });

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- OBJ_69 ----
            OBJ_69.setTitle("");
            OBJ_69.setName("OBJ_69");

            //---- CPRI1 ----
            CPRI1.setText("Prix de revient");
            CPRI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI1.setName("CPRI1");

            //---- CPRI2 ----
            CPRI2.setText("Prix derni\u00e8re entr\u00e9e");
            CPRI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI2.setName("CPRI2");

            //---- CPRI4 ----
            CPRI4.setText("P.U.M.P");
            CPRI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI4.setName("CPRI4");

            //---- CPRI5 ----
            CPRI5.setText("Prix F.I.F.O");
            CPRI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI5.setName("CPRI5");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(CPRI1, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(CPRI2, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(CPRI4, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(CPRI5, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(CPRI1)
                  .addGap(12, 12, 12)
                  .addComponent(CPRI2)
                  .addGap(12, 12, 12)
                  .addComponent(CPRI4)
                  .addGap(12, 12, 12)
                  .addComponent(CPRI5))
            );
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- OBJ_68 ----
            OBJ_68.setTitle("Chiffrage des stocks");
            OBJ_68.setName("OBJ_68");

            //---- OBJ_72 ----
            OBJ_72.setText("Date");
            OBJ_72.setName("OBJ_72");

            //---- OBJ_76 ----
            OBJ_76.setTitle("S\u00e9lection des articles / ZP");
            OBJ_76.setName("OBJ_76");

            //---- ZP1 ----
            ZP1.setComponentPopupMenu(BTD);
            ZP1.setName("ZP1");

            //---- ZP2 ----
            ZP2.setComponentPopupMenu(BTD);
            ZP2.setName("ZP2");

            //---- ZP3 ----
            ZP3.setComponentPopupMenu(BTD);
            ZP3.setName("ZP3");

            //---- ZP4 ----
            ZP4.setComponentPopupMenu(BTD);
            ZP4.setName("ZP4");

            //---- ZP5 ----
            ZP5.setComponentPopupMenu(BTD);
            ZP5.setName("ZP5");

            //---- OBJ_38 ----
            OBJ_38.setTitle("");
            OBJ_38.setName("OBJ_38");

            //---- OBJ_89 ----
            OBJ_89.setText("Code A,B,C,D");
            OBJ_89.setName("OBJ_89");

            //---- WABC ----
            WABC.setComponentPopupMenu(BTD);
            WABC.setName("WABC");

            //---- NWABC ----
            NWABC.setText("Exclusion");
            NWABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NWABC.setName("NWABC");

            //---- DATCHF ----
            DATCHF.setName("DATCHF");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                  .addGap(27, 27, 27)
                  .addComponent(DATCHF, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(ZP1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(ZP2, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(ZP3, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(ZP4, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(ZP5, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(WABC, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(NWABC, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DATCHF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(ZP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(WABC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(panel2Layout.createParallelGroup()
                        .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NWABC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))))
            );
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- OBJ_37 ----
            OBJ_37.setTitle("");
            OBJ_37.setName("OBJ_37");

            //---- RAZNEG ----
            RAZNEG.setText("Remise \u00e0 z\u00e9ro des n\u00e9gatifs");
            RAZNEG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RAZNEG.setName("RAZNEG");

            //---- EDTRES ----
            EDTRES.setText("Quantit\u00e9 r\u00e9serv\u00e9e");
            EDTRES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTRES.setName("EDTRES");

            //---- EDTDES ----
            EDTDES.setText("Edition articles d\u00e9sactiv\u00e9s");
            EDTDES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTDES.setName("EDTDES");

            //---- EDTDP ----
            EDTDP.setText("Seulement articles d\u00e9pr\u00e9ci\u00e9s");
            EDTDP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTDP.setName("EDTDP");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(RAZNEG, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(EDTRES, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(EDTDES, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(EDTDP, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(RAZNEG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(12, 12, 12)
                  .addComponent(EDTRES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(12, 12, 12)
                  .addComponent(EDTDES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(12, 12, 12)
                  .addComponent(EDTDP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Choix papier");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- OBJ_88 ----
    OBJ_88.setTitle("");
    OBJ_88.setName("OBJ_88");

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(CPRI1);
    RB_GRP.add(CPRI2);
    RB_GRP.add(CPRI4);
    RB_GRP.add(CPRI5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_59;
  private JXTitledSeparator OBJ_43;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPanel P_SEL1;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private XRiTextField EDEB;
  private XRiTextField EFIN;
  private XRiCheckBox REPON1;
  private XRiCheckBox WTOUM;
  private XRiCheckBox WTOU;
  private JPanel panel1;
  private JXTitledSeparator OBJ_69;
  private JRadioButton CPRI1;
  private JRadioButton CPRI2;
  private JRadioButton CPRI4;
  private JRadioButton CPRI5;
  private JPanel panel2;
  private JXTitledSeparator OBJ_68;
  private JLabel OBJ_72;
  private JXTitledSeparator OBJ_76;
  private XRiTextField ZP1;
  private XRiTextField ZP2;
  private XRiTextField ZP3;
  private XRiTextField ZP4;
  private XRiTextField ZP5;
  private JXTitledSeparator OBJ_38;
  private JLabel OBJ_89;
  private XRiTextField WABC;
  private XRiCheckBox NWABC;
  private XRiCalendrier DATCHF;
  private JPanel panel3;
  private JXTitledSeparator OBJ_37;
  private XRiCheckBox RAZNEG;
  private XRiCheckBox EDTRES;
  private XRiCheckBox EDTDES;
  private XRiCheckBox EDTDP;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private JXTitledSeparator OBJ_88;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
