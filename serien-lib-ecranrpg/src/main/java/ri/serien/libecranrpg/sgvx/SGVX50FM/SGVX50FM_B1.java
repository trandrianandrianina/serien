
package ri.serien.libecranrpg.sgvx.SGVX50FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Attention!! cette écran ne s'affiche seulement si l'établissement à plusieurs magasins
 * 
 * [GVM3321] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par famille
 * Indicateur :00010001 (94)
 * Titre : Etat des stocks par magasin et par famille
 * 
 * [GVM3322] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par article
 * Indicateur :00000001
 * Titre : Etat des stocks par magasin et par article
 * 
 * [GVM3323] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par mot de classement 2
 * Indicateur :00100001 (93)
 * Titre : Etat des stocks par magasin et par le 2ème mot de classement
 * 
 * [GVM3324] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par sous-famille
 * Indicateur :00000101 (96)
 * Titre : Etat des stocks par magasin et par sous-famille
 * 
 * [GVM3326] Gestion des ventes -> Stocks -> Etats des stocks -> Par magasin -> Par article valorisé
 * Indicateur :00000001
 * Titre : Etat des stocks par magasin et par article
 * 
 * [GVM3361] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par famille
 * Indicateur :01010001 (92,94)
 * Titre : Compte rendu d'inventaire par magasin et par famille
 * 
 * [GVM3362] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par article
 * Indicateur :01000001 (92)
 * Titre : Compte rendu d'inventaire par magasin et par article
 * 
 * [GVM3363] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par mot de classement 2
 * Indicateur :01100001 (92,93)
 * Titre : Compte rendu d'inventaire par magasin et par le 2ème mot de classement
 * 
 * [GVM3364] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par sous-famille
 * Indicateur :01000101 (92,96)
 * Titre : Compte rendu d'inventaire par magasin et par sous-famille
 * 
 * [GVM3365] Gestion des ventes -> Stocks -> Etats des stocks -> Compte rendu d'inventaire/magasin -> Par article valorisé
 * Indicateur :01000001 (92)
 * Titre : Compte rendu d'inventaire par magasin et par article
 * 
 * [GAM3243] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu
 * inventaire/mag/famille
 * Indicateur :01010011 (92,94,97)
 * Titre : Compte rendu d'inventaire par magasin et par famille
 * 
 * [GAM3244] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu
 * inventaire/mag/article
 * Indicateur :01000011 (92,97)
 * Titre : Compte rendu d'inventaire par magasin et par article
 * 
 * [GAM3245] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu inventaire/mag/cl2
 * Indicateur :01100011 (92,93,97)
 * Titre : Compte rendu d'inventaire par magasin et par le 2ème mot de classement
 * 
 * [GAM3246] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires tournants -> Compte rendu
 * inventaire/mag/ss-fam.
 * Indicateur :01000111 (92,96,97)
 * Titre : Compte rendu d'inventaire par magasin et par sous-famille
 * 
 * [GAM3321] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par famille
 * Indicateur :0001001 (94)
 * Titre : Etat des stocks par magasin et par famille
 * 
 * [GAM3322] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par article
 * Indicateur :0000001
 * Titre : Etat des stocks par magasin et par article
 * 
 * [GAM3323] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par mot de classement 2
 * Indicateur :0010001 (93)
 * Titre : Etat des stocks par magasin et par le 2ème mot de classement
 * 
 * [GAM3324] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par fournisseur
 * Indicateur :00001001 (95)
 * Titre : Etat des stocks par magasin et par fournisseur
 * 
 * [GAM3325] Gestion des achats -> Stocks -> Editions stocks -> Par magasin -> Par sous-famille
 * Indicateur :0000101 (96)
 * Titre : Etat des stocks par magasin et par sous-famille
 */
public class SGVX50FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_CHOIX_PAPIER = "Choisir papier";
  private Message LOCTP = null;
  
  public SGVX50FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    snBarreBouton.ajouterBouton(BOUTON_CHOIX_PAPIER, 'E', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Indicateur
    Boolean isStockMagasinArticle = lexique.isTrue("(N91) and (N92) and (N93) and (N94) and (N95) and (N96) and (N97)");
    Boolean isCompteRenduMagasinArticle = lexique.isTrue("92 and (N93)");
    Boolean isStockMagasinMotClassement = lexique.isTrue("93");
    Boolean isStockMagasinFamille = lexique.isTrue("94");
    Boolean isStockMagasinFournisseur = lexique.isTrue("95");
    Boolean isStockMagasinSousFamille = lexique.isTrue("96");
    Boolean isCompteRenduMagasinMotClassement = lexique.isTrue("92 and 93 and (N97)");
    Boolean isCompteRenduMagasinFamille = lexique.isTrue("92 and 94 and (N97)");
    Boolean isCompteRenduMagasinSousFamille = lexique.isTrue("92 and 96 and (N97)");
    
    // Titre
    if (isStockMagasinArticle) {
      bpPresentation.setText("Etat des stocks par magasin et par article");
    }
    if (isCompteRenduMagasinArticle) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par article");
    }
    if (isStockMagasinMotClassement) {
      bpPresentation.setText("Etat des stocks par magasin et par le 2ème mot de classement");
    }
    if (isStockMagasinFamille) {
      bpPresentation.setText("Etat des stocks par magasin et par famille");
    }
    if (isStockMagasinFournisseur) {
      bpPresentation.setText("Etat des stocks par magasin et par fournisseur");
    }
    if (isStockMagasinSousFamille) {
      bpPresentation.setText("Etat des stocks par magasin et par sous-famille");
    }
    if (isCompteRenduMagasinMotClassement) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par le 2ème mot de classement");
    }
    if (isCompteRenduMagasinFamille) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par famille");
    }
    if (isCompteRenduMagasinSousFamille) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par sous-famille");
    }
    if (isCompteRenduMagasinArticle) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par article");
    }
    if (isCompteRenduMagasinMotClassement) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par le 2ème mot de classement");
    }
    if (isCompteRenduMagasinFamille) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par famille");
    }
    if (isCompteRenduMagasinSousFamille) {
      bpPresentation.setText("Compte rendu d'inventaire par magasin et par sous-famille");
    }
    
    // Visibilité des composant magasin
    if (!lexique.isTrue("91")) {
      snMagasin6.setVisible(true);
      snMagasin5.setVisible(true);
      snMagasin4.setVisible(true);
      snMagasin3.setVisible(true);
      snMagasin2.setVisible(true);
      snMagasin1.setVisible(true);
    }
    else {
      snMagasin6.setVisible(false);
      snMagasin5.setVisible(false);
      snMagasin4.setVisible(false);
      snMagasin3.setVisible(false);
      snMagasin2.setVisible(false);
      snMagasin1.setVisible(false);
    }
    
    // Visibilité des labels
    lbMagasin6.setVisible(snMagasin6.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin1.setVisible(snMagasin1.isVisible());
    
    pnlCritereDeSelection.setVisible(snMagasin1.isVisible() || snMagasin2.isVisible() || snMagasin3.isVisible() || snMagasin4.isVisible()
        || snMagasin5.isVisible() || snMagasin6.isVisible());
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Renseigne l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation des composants
    chargerListeComposantsMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin1.renseignerChampRPG(lexique, "MA01");
    snMagasin2.renseignerChampRPG(lexique, "MA02");
    snMagasin3.renseignerChampRPG(lexique, "MA03");
    snMagasin4.renseignerChampRPG(lexique, "MA04");
    snMagasin5.renseignerChampRPG(lexique, "MA05");
    snMagasin6.renseignerChampRPG(lexique, "MA06");
    if (snMagasin1.getIdSelection() == null && snMagasin2.getIdSelection() == null && snMagasin3.getIdSelection() == null
        && snMagasin4.getIdSelection() == null && snMagasin5.getIdSelection() == null && snMagasin6.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOUM", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOUM", 0, "");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CHOIX_PAPIER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * snMagasin1 : "Tous" autorisé, "Aucun" interdit
   * snMagasin2-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On vérifie que le nombre de magasins n'est pas inférieur au nombre de composant magasin présent.
    // Dans le cas où le nombre de magasins est inférieur au nombre de composants présents.
    // on affiche un nombre de composants égal au nombre de choix possibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    for (int i = 0; i < listeComposant.size(); i++) {
      listeComposant.get(i).setVisible(false);
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On rend le composant snMagasin2 visibles si le composant snMagasin1 contient autre chose que "Tous".
      if (i == 0) {
        listeComposant.get(i).setVisible(!(snMagasin1.getIdSelection() == null));
      }
      // On rend les composants suivants visibles si le composant précédent contient autre chose que "Aucun" (null)
      else {
        listeComposant.get(i).setVisible(!(listeComposant.get(i - 1).getIdSelection() == null));
      }
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé.
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeComposantsMagasin();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // On observe le changement de valeur du composant snMagasin1.
  // On affiche ou non le composants snMagasin2 si snMagasin1 différent de "Aucun" (null).
  // On affiche le composant s'il est censé pouvoir être affiché.
  private void snMagasin1ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 2 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 2) {
      snMagasin2.setVisible(!(snMagasin1.getIdSelection() == null));
      lbMagasin2.setVisible(snMagasin2.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin1.getIdSelection() == null) {
      snMagasin2.setSelection(null);
    }
  }
  
  // On effectue la même observation pour chaque composant snMagasin.
  // On affiche le composant suivant selon les même conditions.
  private void snMagasin2ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 3 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 3) {
      snMagasin3.setVisible(!(snMagasin2.getIdSelection() == null));
      lbMagasin3.setVisible(snMagasin3.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin2.getIdSelection() == null) {
      snMagasin3.setSelection(null);
    }
  }
  
  private void snMagasin3ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 4 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 4) {
      snMagasin4.setVisible(!(snMagasin3.getIdSelection() == null));
      lbMagasin4.setVisible(snMagasin4.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin3.getIdSelection() == null) {
      snMagasin4.setSelection(null);
    }
  }
  
  private void snMagasin4ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 5 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 5) {
      snMagasin5.setVisible(!(snMagasin4.getIdSelection() == null));
      lbMagasin5.setVisible(snMagasin5.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin4.getIdSelection() == null) {
      snMagasin5.setSelection(null);
    }
  }
  
  private void snMagasin5ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 6 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 6) {
      snMagasin6.setVisible(!(snMagasin5.getIdSelection() == null));
      lbMagasin6.setVisible(snMagasin6.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin5.getIdSelection() == null) {
      snMagasin6.setSelection(null);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(915, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin1 ----
            lbMagasin1.setText("Magasin 1");
            lbMagasin1.setName("lbMagasin1");
            pnlCritereDeSelection.add(lbMagasin1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin1.setName("snMagasin1");
            snMagasin1.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin1ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin 2");
            lbMagasin2.setName("lbMagasin2");
            pnlCritereDeSelection.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin2 ----
            snMagasin2.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin2.setName("snMagasin2");
            snMagasin2.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin2ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Magasin 3");
            lbMagasin3.setName("lbMagasin3");
            pnlCritereDeSelection.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin3 ----
            snMagasin3.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin3.setName("snMagasin3");
            snMagasin3.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin3ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin4 ----
            lbMagasin4.setText("Magasin 4");
            lbMagasin4.setName("lbMagasin4");
            pnlCritereDeSelection.add(lbMagasin4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin4 ----
            snMagasin4.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin4.setName("snMagasin4");
            snMagasin4.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin4ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin5 ----
            lbMagasin5.setText("Magasin 5");
            lbMagasin5.setName("lbMagasin5");
            pnlCritereDeSelection.add(lbMagasin5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin5 ----
            snMagasin5.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin5.setName("snMagasin5");
            snMagasin5.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin5ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin6 ----
            lbMagasin6.setText("Magasin 6");
            lbMagasin6.setName("lbMagasin6");
            pnlCritereDeSelection.add(lbMagasin6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin6 ----
            snMagasin6.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin6.setName("snMagasin6");
            pnlCritereDeSelection.add(snMagasin6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
