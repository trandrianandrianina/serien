
package ri.serien.libecranrpg.sgvx.SGVX60FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class SGVX60FM_G02 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  
  public SGVX60FM_G02(JPanel pPanel, Lexical pLexique, iData pInterpreteurD) {
    master = pPanel;
    lexique = pLexique;
    interpreteurD = pInterpreteurD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  public void setData() {
    // Graphe
    String[] libelle1 = new String[24];
    String[] donnee1 = new String[24];
    
    // Chargement des libellés
    for (int i = 0; i < libelle1.length; i++) {
      libelle1[i] = lexique.HostFieldGetData("MOI" + ((i + 1) < 10 ? +(i + 1) : (i + 1)));
    }
    
    // Chargement des données
    for (int i = 0; i < donnee1.length; i++) {
      if (lexique.HostFieldGetData("S1X" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim().equalsIgnoreCase("")) {
        donnee1[i] = "0.0";
      }
      else {
        donnee1[i] = lexique.HostFieldGetNumericData("S1X" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
      }
      
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle1.length][2];
    
    for (int i = 0; i < libelle1.length; i++) {
      data[i][0] = libelle1[i];
      data[i][1] = Double.parseDouble(donnee1[i]);
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("", false);
    
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    
    // TODO Icones
    button1.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    button1 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      
      // ---- button1 ----
      button1.setText("Retour");
      button1.setFont(button1.getFont().deriveFont(button1.getFont().getStyle() | Font.BOLD, button1.getFont().getSize() + 2f));
      button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      button1.setName("button1");
      button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button1ActionPerformed(e);
        }
      });
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(12, 12, 12).addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 1068,
              GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(960, 960, 960).addComponent(button1, GroupLayout.PREFERRED_SIZE, 120,
              GroupLayout.PREFERRED_SIZE)));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(15, 15, 15)
              .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
              .addComponent(button1, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JButton button1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
