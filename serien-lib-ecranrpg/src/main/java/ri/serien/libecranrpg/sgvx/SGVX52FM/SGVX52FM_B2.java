
package ri.serien.libecranrpg.sgvx.SGVX52FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX52FM_B2 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  // Variables
  private boolean modePlanning = false;
  
  public SGVX52FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    modePlanning = lexique.isTrue("97");
    
    lexique.setNomFichierTableur(
        lexique.HostFieldGetData("TITPG1").trim().replace('/', '_') + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    rafraichirEtablissement();
    
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Message planning
    lbPlanning.setVisible(modePlanning);
    if (modePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
    }
    
    rafraichirMagasin();
    rafraichirArticle1();
    rafraichirArticle2();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "MA01");
    snArticle1.renseignerChampRPG(lexique, "EARTD");
    snArticle2.renseignerChampRPG(lexique, "EARTF");
  }
  
  /**
   * Initialise le composant établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  /**
   * 
   * Initialise le composant magasin.
   *
   */
  private void rafraichirMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "MA01");
  }
  
  private void rafraichirArticle1() {
    snArticle1.setSession(getSession());
    snArticle1.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle1.charger(false);
    snArticle1.setSelectionParChampRPG(lexique, "EARTD");
  }
  
  private void rafraichirArticle2() {
    snArticle2.setSession(getSession());
    snArticle2.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle2.charger(false);
    snArticle2.setSelectionParChampRPG(lexique, "EARTF");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTOUItemStateChanged(ItemEvent e) {
    try {
      if (WTOU.isSelected()) {
        snArticle1.setSelection(null);
        snArticle2.setSelection(null);
      }
      snArticle1.setEnabled(!WTOU.isSelected());
      snArticle2.setEnabled(!WTOU.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbPlanning = new SNMessage();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCriteresSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    WTOU = new XRiCheckBox();
    lbArticle = new SNLabelChamp();
    snArticle1 = new SNArticle();
    snArticle2 = new SNArticle();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfDateEnCours = new SNTexte();
    EARTF = new XRiTextField();
    EARTD = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      p_nord.add(bpPresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());

      //======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlMessage.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlMessage.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlMessage.getLayout()).columnWeights = new double[] {1.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlMessage.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbPlanning ----
        lbPlanning.setText("Label Planning");
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);

      //======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlCriteresSelection ========
          {
            pnlCriteresSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCriteresSelection.setName("pnlCriteresSelection");
            pnlCriteresSelection.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlCriteresSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlCriteresSelection.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)pnlCriteresSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlCriteresSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlCriteresSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlCriteresSelection.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- WTOU ----
            WTOU.setText("Tous les articles");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTOU.setMaximumSize(new Dimension(142, 30));
            WTOU.setMinimumSize(new Dimension(142, 30));
            WTOU.setPreferredSize(new Dimension(142, 30));
            WTOU.setName("WTOU");
            WTOU.addItemListener(e -> WTOUItemStateChanged(e));
            pnlCriteresSelection.add(WTOU, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbArticle ----
            lbArticle.setText("Plage d'articles");
            lbArticle.setName("lbArticle");
            pnlCriteresSelection.add(lbArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snArticle1 ----
            snArticle1.setName("snArticle1");
            pnlCriteresSelection.add(snArticle1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snArticle2 ----
            snArticle2.setName("snArticle2");
            pnlCriteresSelection.add(snArticle2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCriteresSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== pnlEtablissementSelectionne ========
          {
            pnlEtablissementSelectionne.setTitre("Etablissement");
            pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
            pnlEtablissementSelectionne.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
            pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionne.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- tfDateEnCours ----
            tfDateEnCours.setText("@WENCX@");
            tfDateEnCours.setMaximumSize(new Dimension(272, 20));
            tfDateEnCours.setEditable(false);
            tfDateEnCours.setPreferredSize(new Dimension(272, 30));
            tfDateEnCours.setMinimumSize(new Dimension(272, 30));
            tfDateEnCours.setEnabled(false);
            tfDateEnCours.setName("tfDateEnCours");
            pnlEtablissementSelectionne.add(tfDateEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- EARTF ----
    EARTF.setName("EARTF");

    //---- EARTD ----
    EARTD.setName("EARTD");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre bpPresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNMessage lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteresSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private XRiCheckBox WTOU;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle1;
  private SNArticle snArticle2;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfDateEnCours;
  private XRiTextField EARTF;
  private XRiTextField EARTD;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
