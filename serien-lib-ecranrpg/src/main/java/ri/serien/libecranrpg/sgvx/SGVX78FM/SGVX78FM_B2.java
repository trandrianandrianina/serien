
package ri.serien.libecranrpg.sgvx.SGVX78FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SGVX78FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private Message messageBordereau = null;
  private Message messageAttention = null;
  private Message messageOption = null;
  
  public SGVX78FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    // Ajout
    initDiverses();
    OPT2.setValeurs("2", "RB");
    OPT3.setValeurs("3", "RB");
    OPT1.setValeurs("1", "RB");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Options complémentaires"));
    
    // Initialisation des différent messages
    messageBordereau = messageBordereau.getMessageNormal(
        "Un ou plusieurs bordereaux traités et non purgés lors d'un précédent inventaire sont présents dans le système.");
    messageAttention = messageAttention.getMessageImportant("ATTENTION ! Cette option mal utilisée peut fausser vos stocks");
    lbMessage.setMessage(messageBordereau);
    lbAttention.setMessage(messageAttention);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbMessage = new SNMessage();
    sNLabelTitre1 = new SNLabelTitre();
    OPT3 = new XRiRadioButton();
    OPT1 = new XRiRadioButton();
    sNLabelTitre2 = new SNLabelTitre();
    OPT2 = new XRiRadioButton();
    lbAttention = new SNMessage();
    
    // ======== this ========
    setMinimumSize(new Dimension(700, 380));
    setPreferredSize(new Dimension(700, 380));
    setMaximumSize(new Dimension(700, 380));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ---- lbMessage ----
      lbMessage.setText("messsage");
      lbMessage.setName("lbMessage");
      pnlContenu.add(lbMessage,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- sNLabelTitre1 ----
      sNLabelTitre1.setText("Options pour ces bordereaux d\u00e9j\u00e0 trait\u00e9s");
      sNLabelTitre1.setName("sNLabelTitre1");
      pnlContenu.add(sNLabelTitre1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- OPT3 ----
      OPT3.setText("Suppression (option la plus fr\u00e9quente)");
      OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
      OPT3.setMinimumSize(new Dimension(300, 30));
      OPT3.setMaximumSize(new Dimension(300, 30));
      OPT3.setPreferredSize(new Dimension(300, 30));
      OPT3.setName("OPT3");
      pnlContenu.add(OPT3,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- OPT1 ----
      OPT1.setText("Aucun traitement (les anciens bordereaux sont conserv\u00e9s)");
      OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
      OPT1.setMinimumSize(new Dimension(300, 30));
      OPT1.setMaximumSize(new Dimension(300, 30));
      OPT1.setPreferredSize(new Dimension(300, 30));
      OPT1.setName("OPT1");
      pnlContenu.add(OPT1,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- sNLabelTitre2 ----
      sNLabelTitre2.setText("Option de forcage ou reprise apr\u00e8s incident");
      sNLabelTitre2.setName("sNLabelTitre2");
      pnlContenu.add(sNLabelTitre2,
          new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- OPT2 ----
      OPT2.setText("Regroupement forc\u00e9 avec les nouveaux bordereaux saisis");
      OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
      OPT2.setMinimumSize(new Dimension(300, 30));
      OPT2.setMaximumSize(new Dimension(300, 30));
      OPT2.setPreferredSize(new Dimension(300, 30));
      OPT2.setName("OPT2");
      pnlContenu.add(OPT2,
          new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbAttention ----
      lbAttention.setText("Attention");
      lbAttention.setName("lbAttention");
      pnlContenu.add(lbAttention,
          new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNMessage lbMessage;
  private SNLabelTitre sNLabelTitre1;
  private XRiRadioButton OPT3;
  private XRiRadioButton OPT1;
  private SNLabelTitre sNLabelTitre2;
  private XRiRadioButton OPT2;
  private SNMessage lbAttention;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
