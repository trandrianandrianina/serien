
package ri.serien.libecranrpg.sgvx.SGVX94FM;
// Nom Fichier: b_SGVX94FM_FMTB2_FMTF1_404.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX94FM_B2 extends SNPanelEcranRPG implements ioFrame {
   
  
  public SGVX94FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTETB.setValeursSelection("**", "");
    ETT001.setValeursSelection("1", "");
    ETT002.setValeursSelection("1", "");
    ETT003.setValeursSelection("1", "");
    ETT004.setValeursSelection("1", "");
    ETT005.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    planification.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SAUV@")).trim());
    ETT001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS001@ @ETN001@")).trim());
    ETT002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS002@ @ETN002@")).trim());
    ETT003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS003@ @ETN003@")).trim());
    ETT004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS004@ @ETN004@")).trim());
    ETT005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS005@ @ETN005@")).trim());
    ETE001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE001@")).trim());
    ETE002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE002@")).trim());
    ETE003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE003@")).trim());
    ETE004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE004@")).trim());
    ETE005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE005@")).trim());
  }
  
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    planification.setVisible(lexique.isTrue("96"));
    panel1.setVisible(!WTETB.isSelected());
    
    ETE001.setVisible(ETT001.isVisible());
    ETE002.setVisible(ETT002.isVisible());
    ETE003.setVisible(ETT003.isVisible());
    ETE004.setVisible(ETT004.isVisible());
    ETE005.setVisible(ETT005.isVisible());
    
    // TODO Icones
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTETBActionPerformed(ActionEvent e) {
    panel1.setVisible(!panel1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    planification = new JLabel();
    WTETB = new XRiCheckBox();
    panel1 = new JPanel();
    ETT001 = new XRiCheckBox();
    ETT002 = new XRiCheckBox();
    ETT003 = new XRiCheckBox();
    ETT004 = new XRiCheckBox();
    ETT005 = new XRiCheckBox();
    ETM001 = new XRiTextField();
    ETM002 = new XRiTextField();
    ETM003 = new XRiTextField();
    ETM004 = new XRiTextField();
    ETM005 = new XRiTextField();
    ETE001 = new RiZoneSortie();
    ETE002 = new RiZoneSortie();
    ETE003 = new RiZoneSortie();
    ETE004 = new RiZoneSortie();
    ETE005 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 330));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- planification ----
          planification.setText("@SAUV@");
          planification.setFont(planification.getFont().deriveFont(planification.getFont().getStyle() | Font.BOLD));
          planification.setHorizontalAlignment(SwingConstants.CENTER);
          planification.setForeground(new Color(255, 0, 51));
          planification.setName("planification");

          //---- WTETB ----
          WTETB.setText("Tous les \u00e9tablissements");
          WTETB.setName("WTETB");
          WTETB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTETBActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Choix de l'\u00e9tablissement"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- ETT001 ----
            ETT001.setText("@ETS001@ @ETN001@");
            ETT001.setName("ETT001");
            panel1.add(ETT001);
            ETT001.setBounds(20, 45, 321, 27);

            //---- ETT002 ----
            ETT002.setText("@ETS002@ @ETN002@");
            ETT002.setName("ETT002");
            panel1.add(ETT002);
            ETT002.setBounds(20, 80, 321, 27);

            //---- ETT003 ----
            ETT003.setText("@ETS003@ @ETN003@");
            ETT003.setName("ETT003");
            panel1.add(ETT003);
            ETT003.setBounds(20, 115, 321, 27);

            //---- ETT004 ----
            ETT004.setText("@ETS004@ @ETN004@");
            ETT004.setName("ETT004");
            panel1.add(ETT004);
            ETT004.setBounds(20, 155, 321, 27);

            //---- ETT005 ----
            ETT005.setText("@ETS005@ @ETN005@");
            ETT005.setName("ETT005");
            panel1.add(ETT005);
            ETT005.setBounds(20, 195, 321, 27);

            //---- ETM001 ----
            ETM001.setName("ETM001");
            panel1.add(ETM001);
            ETM001.setBounds(360, 44, 60, ETM001.getPreferredSize().height);

            //---- ETM002 ----
            ETM002.setName("ETM002");
            panel1.add(ETM002);
            ETM002.setBounds(360, 79, 60, ETM002.getPreferredSize().height);

            //---- ETM003 ----
            ETM003.setName("ETM003");
            panel1.add(ETM003);
            ETM003.setBounds(360, 114, 60, ETM003.getPreferredSize().height);

            //---- ETM004 ----
            ETM004.setName("ETM004");
            panel1.add(ETM004);
            ETM004.setBounds(360, 154, 60, ETM004.getPreferredSize().height);

            //---- ETM005 ----
            ETM005.setName("ETM005");
            panel1.add(ETM005);
            ETM005.setBounds(360, 194, 60, ETM005.getPreferredSize().height);

            //---- ETE001 ----
            ETE001.setText("@ETE001@");
            ETE001.setName("ETE001");
            panel1.add(ETE001);
            ETE001.setBounds(475, 44, 175, 28);

            //---- ETE002 ----
            ETE002.setText("@ETE002@");
            ETE002.setName("ETE002");
            panel1.add(ETE002);
            ETE002.setBounds(475, 79, 175, 28);

            //---- ETE003 ----
            ETE003.setText("@ETE003@");
            ETE003.setName("ETE003");
            panel1.add(ETE003);
            ETE003.setBounds(475, 114, 175, 28);

            //---- ETE004 ----
            ETE004.setText("@ETE004@");
            ETE004.setName("ETE004");
            panel1.add(ETE004);
            ETE004.setBounds(475, 154, 175, 28);

            //---- ETE005 ----
            ETE005.setText("@ETE005@");
            ETE005.setName("ETE005");
            panel1.add(ETE005);
            ETE005.setBounds(475, 194, 175, 28);

            //---- label1 ----
            label1.setText("Etablissement");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(20, 20, 321, 25);

            //---- label2 ----
            label2.setText("Purge");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(360, 20, 60, 25);

            //---- label3 ----
            label3.setText("En cours");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(475, 20, 175, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(planification, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(WTETB, GroupLayout.PREFERRED_SIZE, 321, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 674, GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planification, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(WTETB, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPopupMenu BTD;
    private JMenuItem OBJ_11;
    private JMenuItem OBJ_12;
    private JPanel p_nord;
    private SNBandeauTitre p_bpresentation;
    private JMenuBar barre_tete;
    private JPanel p_tete_gauche;
    private JPanel p_tete_droite;
    private JLabel lb_loctp_;
    private JPanel p_sud;
    private JPanel p_menus;
    private JPanel menus_bas;
    private RiMenu navig_erreurs;
    private RiMenu_bt bouton_erreurs;
    private RiMenu navig_valid;
    private RiMenu_bt bouton_valider;
    private RiMenu navig_retour;
    private RiMenu_bt bouton_retour;
    private JScrollPane scroll_droite;
    private JPanel menus_haut;
    private RiMenu riMenu2;
    private RiMenu_bt riMenu_bt2;
    private RiSousMenu riSousMenu6;
    private RiSousMenu_bt riSousMenu_bt_export;
    private SNPanelDegradeGris p_centrage;
    private JPanel p_contenu;
    private JLabel planification;
    private XRiCheckBox WTETB;
    private JPanel panel1;
    private XRiCheckBox ETT001;
    private XRiCheckBox ETT002;
    private XRiCheckBox ETT003;
    private XRiCheckBox ETT004;
    private XRiCheckBox ETT005;
    private XRiTextField ETM001;
    private XRiTextField ETM002;
    private XRiTextField ETM003;
    private XRiTextField ETM004;
    private XRiTextField ETM005;
    private RiZoneSortie ETE001;
    private RiZoneSortie ETE002;
    private RiZoneSortie ETE003;
    private RiZoneSortie ETE004;
    private RiZoneSortie ETE005;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
