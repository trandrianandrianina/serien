
package ri.serien.libecranrpg.sgvx.SGVX5CFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX5CFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX5CFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOUM.setValeursSelection("**", "  ");
    REPON2.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    UCH2.setEnabled(lexique.isPresent("UCH2"));
    UCH1.setEnabled(lexique.isPresent("UCH1"));
    ELIB4.setEnabled(lexique.isPresent("ELIB4"));
    ELIB3.setEnabled(lexique.isPresent("ELIB3"));
    ELIB2.setEnabled(lexique.isPresent("ELIB2"));
    ELIB1.setEnabled(lexique.isPresent("ELIB1"));
    MA12.setEnabled(lexique.isPresent("MA12"));
    MA11.setEnabled(lexique.isPresent("MA11"));
    MA10.setEnabled(lexique.isPresent("MA10"));
    MA09.setEnabled(lexique.isPresent("MA09"));
    MA08.setEnabled(lexique.isPresent("MA08"));
    MA07.setEnabled(lexique.isPresent("MA07"));
    MA06.setEnabled(lexique.isPresent("MA06"));
    MA05.setEnabled(lexique.isPresent("MA05"));
    MA04.setEnabled(lexique.isPresent("MA04"));
    MA03.setEnabled(lexique.isPresent("MA03"));
    MA02.setEnabled(lexique.isPresent("MA02"));
    MA01.setEnabled(lexique.isPresent("MA01"));
    BNFIN.setEnabled(lexique.isPresent("BNFIN"));
    BNDEB.setEnabled(lexique.isPresent("BNDEB"));
    EFIN.setEnabled(lexique.isPresent("EFIN"));
    EDEB.setEnabled(lexique.isPresent("EDEB"));
    // DTFIN2.setEnabled( lexique.isPresent("DTFIN2"));
    // DTDEB2.setEnabled( lexique.isPresent("DTDEB2"));
    // DTFIN1.setEnabled( lexique.isPresent("DTFIN1"));
    // DTDEB1.setEnabled( lexique.isPresent("DTDEB1"));
    // WTOUM.setVisible( lexique.isPresent("WTOUM"));
    OBJ_42.setVisible(lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    EFINA.setEnabled(lexique.isPresent("EFINA"));
    EDEBA.setEnabled(lexique.isPresent("EDEBA"));
    // REPON2.setEnabled( lexique.isPresent("REPON2"));
    // REPON2.setSelected(lexique.HostFieldGetData("REPON2").equalsIgnoreCase("OUI"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**") && !lexique.isTrue("91"));
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (REPON2.isSelected())
    // lexique.HostFieldPutData("REPON2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON2", 0, "NON");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_42 = new JXTitledSeparator();
    OBJ_37 = new JXTitledSeparator();
    OBJ_39 = new JXTitledSeparator();
    OBJ_41 = new JXTitledSeparator();
    OBJ_40 = new JXTitledSeparator();
    OBJ_38 = new JXTitledSeparator();
    panel1 = new JPanel();
    OBJ_86 = new JLabel();
    OBJ_87 = new JLabel();
    ELIB1 = new XRiTextField();
    OBJ_88 = new JLabel();
    ELIB2 = new XRiTextField();
    OBJ_89 = new JLabel();
    ELIB3 = new XRiTextField();
    OBJ_90 = new JLabel();
    ELIB4 = new XRiTextField();
    OBJ_91 = new JLabel();
    BNDEB = new XRiTextField();
    OBJ_92 = new JLabel();
    BNFIN = new XRiTextField();
    REPON2 = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_93 = new JLabel();
    UCH1 = new XRiTextField();
    OBJ_94 = new JLabel();
    UCH2 = new XRiTextField();
    panel3 = new JPanel();
    OBJ_84 = new JLabel();
    DTDEB1 = new XRiCalendrier();
    OBJ_83 = new JLabel();
    DTFIN1 = new XRiCalendrier();
    OBJ_82 = new JLabel();
    DTDEB2 = new XRiCalendrier();
    OBJ_85 = new JLabel();
    DTFIN2 = new XRiCalendrier();
    panel4 = new JPanel();
    EDEBA = new XRiTextField();
    OBJ_80 = new JLabel();
    OBJ_81 = new JLabel();
    EFINA = new XRiTextField();
    panel5 = new JPanel();
    EDEB = new XRiTextField();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    EFIN = new XRiTextField();
    panel6 = new JPanel();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    WTOUM = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 690));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 590));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(700, 590));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 20, 625, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(195, 45, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(195, 75, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 65, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 60), bouton_etablissement.getPreferredSize()));

          //---- OBJ_42 ----
          OBJ_42.setTitle("Code magasin");
          OBJ_42.setName("OBJ_42");
          p_contenu.add(OBJ_42);
          OBJ_42.setBounds(35, 110, 625, OBJ_42.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setTitle("");
          OBJ_37.setName("OBJ_37");
          p_contenu.add(OBJ_37);
          OBJ_37.setBounds(35, 480, 625, OBJ_37.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setTitle("P\u00e9riodes \u00e0 s\u00e9lectionner");
          OBJ_39.setName("OBJ_39");
          p_contenu.add(OBJ_39);
          OBJ_39.setBounds(35, 315, 625, OBJ_39.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setTitle("Plage de famille");
          OBJ_41.setName("OBJ_41");
          p_contenu.add(OBJ_41);
          OBJ_41.setBounds(35, 190, 625, OBJ_41.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setTitle("Plage articles");
          OBJ_40.setName("OBJ_40");
          p_contenu.add(OBJ_40);
          OBJ_40.setBounds(35, 250, 625, OBJ_40.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setTitle("Chiffrage stocks");
          OBJ_38.setName("OBJ_38");
          p_contenu.add(OBJ_38);
          OBJ_38.setBounds(35, 410, 625, OBJ_38.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_86 ----
            OBJ_86.setText("Libell\u00e9 articles \u00e0 \u00e9diter");
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(10, 14, 145, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("1");
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(180, 14, 10, 20);

            //---- ELIB1 ----
            ELIB1.setComponentPopupMenu(BTD);
            ELIB1.setName("ELIB1");
            panel1.add(ELIB1);
            ELIB1.setBounds(190, 10, 20, ELIB1.getPreferredSize().height);

            //---- OBJ_88 ----
            OBJ_88.setText("2");
            OBJ_88.setName("OBJ_88");
            panel1.add(OBJ_88);
            OBJ_88.setBounds(225, 14, 10, 20);

            //---- ELIB2 ----
            ELIB2.setComponentPopupMenu(BTD);
            ELIB2.setName("ELIB2");
            panel1.add(ELIB2);
            ELIB2.setBounds(240, 10, 20, ELIB2.getPreferredSize().height);

            //---- OBJ_89 ----
            OBJ_89.setText("3");
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(280, 14, 10, 20);

            //---- ELIB3 ----
            ELIB3.setComponentPopupMenu(BTD);
            ELIB3.setName("ELIB3");
            panel1.add(ELIB3);
            ELIB3.setBounds(295, 10, 20, ELIB3.getPreferredSize().height);

            //---- OBJ_90 ----
            OBJ_90.setText("4");
            OBJ_90.setName("OBJ_90");
            panel1.add(OBJ_90);
            OBJ_90.setBounds(330, 14, 10, 20);

            //---- ELIB4 ----
            ELIB4.setComponentPopupMenu(BTD);
            ELIB4.setName("ELIB4");
            panel1.add(ELIB4);
            ELIB4.setBounds(345, 10, 20, ELIB4.getPreferredSize().height);

            //---- OBJ_91 ----
            OBJ_91.setText("Ligne bloc notes");
            OBJ_91.setName("OBJ_91");
            panel1.add(OBJ_91);
            OBJ_91.setBounds(10, 44, 134, 20);

            //---- BNDEB ----
            BNDEB.setComponentPopupMenu(BTD);
            BNDEB.setName("BNDEB");
            panel1.add(BNDEB);
            BNDEB.setBounds(190, 40, 26, BNDEB.getPreferredSize().height);

            //---- OBJ_92 ----
            OBJ_92.setText("\u00e0");
            OBJ_92.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_92.setName("OBJ_92");
            panel1.add(OBJ_92);
            OBJ_92.setBounds(220, 44, 15, 20);

            //---- BNFIN ----
            BNFIN.setComponentPopupMenu(BTD);
            BNFIN.setName("BNFIN");
            panel1.add(BNFIN);
            BNFIN.setBounds(240, 40, 26, BNFIN.getPreferredSize().height);

            //---- REPON2 ----
            REPON2.setText("Edition articles d\u00e9sactiv\u00e9s");
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setName("REPON2");
            panel1.add(REPON2);
            REPON2.setBounds(385, 50, 182, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(45, 490, 595, 80);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_93 ----
            OBJ_93.setText("au P.U.M.P");
            OBJ_93.setName("OBJ_93");
            panel2.add(OBJ_93);
            OBJ_93.setBounds(20, 14, 80, 20);

            //---- UCH1 ----
            UCH1.setComponentPopupMenu(BTD);
            UCH1.setName("UCH1");
            panel2.add(UCH1);
            UCH1.setBounds(105, 10, 20, UCH1.getPreferredSize().height);

            //---- OBJ_94 ----
            OBJ_94.setText("au prix de revient");
            OBJ_94.setName("OBJ_94");
            panel2.add(OBJ_94);
            OBJ_94.setBounds(250, 14, 111, 20);

            //---- UCH2 ----
            UCH2.setComponentPopupMenu(BTD);
            UCH2.setName("UCH2");
            panel2.add(UCH2);
            UCH2.setBounds(365, 10, 20, UCH2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(30, 425, 405, 45);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_84 ----
            OBJ_84.setText("P\u00e9riode 1");
            OBJ_84.setName("OBJ_84");
            panel3.add(OBJ_84);
            OBJ_84.setBounds(10, 9, 75, 20);

            //---- DTDEB1 ----
            DTDEB1.setName("DTDEB1");
            panel3.add(DTDEB1);
            DTDEB1.setBounds(95, 5, 105, DTDEB1.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("/");
            OBJ_83.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_83.setName("OBJ_83");
            panel3.add(OBJ_83);
            OBJ_83.setBounds(215, 11, 13, OBJ_83.getPreferredSize().height);

            //---- DTFIN1 ----
            DTFIN1.setName("DTFIN1");
            panel3.add(DTFIN1);
            DTFIN1.setBounds(240, 5, 105, DTFIN1.getPreferredSize().height);

            //---- OBJ_82 ----
            OBJ_82.setText("P\u00e9riode 2");
            OBJ_82.setName("OBJ_82");
            panel3.add(OBJ_82);
            OBJ_82.setBounds(10, 39, 75, 20);

            //---- DTDEB2 ----
            DTDEB2.setName("DTDEB2");
            panel3.add(DTDEB2);
            DTDEB2.setBounds(95, 35, 105, DTDEB2.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("/");
            OBJ_85.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_85.setName("OBJ_85");
            panel3.add(OBJ_85);
            OBJ_85.setBounds(215, 41, 14, OBJ_85.getPreferredSize().height);

            //---- DTFIN2 ----
            DTFIN2.setName("DTFIN2");
            panel3.add(DTFIN2);
            DTFIN2.setBounds(240, 35, 105, DTFIN2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(40, 335, 410, 65);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- EDEBA ----
            EDEBA.setComponentPopupMenu(BTD);
            EDEBA.setName("EDEBA");
            panel4.add(EDEBA);
            EDEBA.setBounds(65, 10, 210, EDEBA.getPreferredSize().height);

            //---- OBJ_80 ----
            OBJ_80.setText("D\u00e9but");
            OBJ_80.setName("OBJ_80");
            panel4.add(OBJ_80);
            OBJ_80.setBounds(15, 14, 50, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Fin");
            OBJ_81.setName("OBJ_81");
            panel4.add(OBJ_81);
            OBJ_81.setBounds(320, 14, 35, 20);

            //---- EFINA ----
            EFINA.setComponentPopupMenu(BTD);
            EFINA.setName("EFINA");
            panel4.add(EFINA);
            EFINA.setBounds(355, 10, 210, EFINA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(35, 265, 570, 45);

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- EDEB ----
            EDEB.setComponentPopupMenu(BTD);
            EDEB.setName("EDEB");
            panel5.add(EDEB);
            EDEB.setBounds(145, 5, 40, EDEB.getPreferredSize().height);

            //---- OBJ_78 ----
            OBJ_78.setText("Code famille d\u00e9but");
            OBJ_78.setName("OBJ_78");
            panel5.add(OBJ_78);
            OBJ_78.setBounds(0, 9, 140, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("Code famille fin");
            OBJ_79.setName("OBJ_79");
            panel5.add(OBJ_79);
            OBJ_79.setBounds(215, 9, 125, 20);

            //---- EFIN ----
            EFIN.setComponentPopupMenu(BTD);
            EFIN.setName("EFIN");
            panel5.add(EFIN);
            EFIN.setBounds(340, 5, 40, EFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel5);
          panel5.setBounds(50, 210, 385, 40);

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(10, 8, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(42, 8, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(74, 8, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(106, 8, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(138, 8, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(170, 8, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(202, 8, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(234, 8, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(266, 8, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(298, 8, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(330, 8, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(362, 8, 34, MA12.getPreferredSize().height);
            }
            panel6.add(P_SEL0);
            P_SEL0.setBounds(150, 5, 405, 45);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            panel6.add(WTOUM);
            WTOUM.setBounds(5, 15, 141, WTOUM.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel6);
          panel6.setBounds(45, 130, 600, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_37;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_41;
  private JXTitledSeparator OBJ_40;
  private JXTitledSeparator OBJ_38;
  private JPanel panel1;
  private JLabel OBJ_86;
  private JLabel OBJ_87;
  private XRiTextField ELIB1;
  private JLabel OBJ_88;
  private XRiTextField ELIB2;
  private JLabel OBJ_89;
  private XRiTextField ELIB3;
  private JLabel OBJ_90;
  private XRiTextField ELIB4;
  private JLabel OBJ_91;
  private XRiTextField BNDEB;
  private JLabel OBJ_92;
  private XRiTextField BNFIN;
  private XRiCheckBox REPON2;
  private JPanel panel2;
  private JLabel OBJ_93;
  private XRiTextField UCH1;
  private JLabel OBJ_94;
  private XRiTextField UCH2;
  private JPanel panel3;
  private JLabel OBJ_84;
  private XRiCalendrier DTDEB1;
  private JLabel OBJ_83;
  private XRiCalendrier DTFIN1;
  private JLabel OBJ_82;
  private XRiCalendrier DTDEB2;
  private JLabel OBJ_85;
  private XRiCalendrier DTFIN2;
  private JPanel panel4;
  private XRiTextField EDEBA;
  private JLabel OBJ_80;
  private JLabel OBJ_81;
  private XRiTextField EFINA;
  private JPanel panel5;
  private XRiTextField EDEB;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private XRiTextField EFIN;
  private JPanel panel6;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private XRiCheckBox WTOUM;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
