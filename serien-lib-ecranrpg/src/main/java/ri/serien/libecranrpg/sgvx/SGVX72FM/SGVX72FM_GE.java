
package ri.serien.libecranrpg.sgvx.SGVX72FM;
// Nom Fichier: pop_SGVX72FM_FMTGE_126.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGVX72FM_GE extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX72FM_GE(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_4 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_5 = new JButton();

    //======== this ========
    setPreferredSize(new Dimension(315, 100));
    setName("this");
    setLayout(null);

    //---- OBJ_4 ----
    OBJ_4.setText("G\u00e9n\u00e9ration en cours");
    OBJ_4.setName("OBJ_4");
    add(OBJ_4);
    OBJ_4.setBounds(95, 40, 154, 20);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }
    add(P_PnlOpts);
    P_PnlOpts.setBounds(1036, 15, 55, 516);

    //---- OBJ_5 ----
    OBJ_5.setText("");
    OBJ_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(25, 28, 54, 44);

    setPreferredSize(new Dimension(315, 100));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_4;
  private JPanel P_PnlOpts;
  private JButton OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
