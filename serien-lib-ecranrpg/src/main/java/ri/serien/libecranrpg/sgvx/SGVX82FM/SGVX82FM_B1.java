
package ri.serien.libecranrpg.sgvx.SGVX82FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM3236] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Inventaires -> Génération bordereaux de saisie
 * Indicateur:00000001
 * Titre:Génération de bordereaux de saisie
 * 
 * [GAM3237] Gestion des achats -> Stocks -> Bordereaux de stocks/inventaires -> Inventaires -> Génération bordereaux de saisie
 * Indicateur:00000001
 * Titre:Génération de bordereaux de saisie
 */
public class SGVX82FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] REPON2_Value = { "NON", "OUI", "TIR", };
  private String[] TRIADS_Value = { "ART", "FAM", "ADS", };
  private Message LOCTP = null;
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  public SGVX82FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    REPON2.setValeurs(REPON2_Value, null);
    TRIADS.setValeurs(TRIADS_Value, null);
    ARTSTH.setValeursSelection("1", " ");
    WGENF.setValeursSelection("1", " ");
    EDTMIN.setValeursSelection("O", " ");
    REPON1.setValeursSelection("OUI", "NON");
    EDTCND.setValeursSelection("O", " ");
    GENQTE.setValeursSelection("O", " ");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
        
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    lbGenerationBordereau1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL1@")).trim());
    lbGenerationBordereau2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL2@")).trim());
    lbGenerationBordereau3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL3@")).trim());
    tfEnCours_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Visibilité
    lbSautLigne.setVisible(REPON2.isVisible());
    lbOptionGeneration.setVisible(TRIADS.isVisible());
    pnlSelectionFamille.setVisible(FAM_RB.isSelected());
    pnlCodeSousFamille.setVisible(SFAM_RB.isSelected());
    WGENF.setVisible(FAM_RB.isSelected());
    if (lexique.isTrue("40") && TRIADS.getSelectedIndex() == 2) {
      pnlTriAdresse.setVisible(true);
      pnlTriBordereaux.setVisible(true);
      WGEN1.setVisible(true);
      WGEN2.setVisible(true);
      WGEN3.setVisible(true);
    }
    else {
      pnlTriAdresse.setVisible(false);
      pnlTriBordereaux.setVisible(false);
      WGEN1.setVisible(false);
      WGEN2.setVisible(false);
      WGEN3.setVisible(false);
    }
    
    // Selection des tri par adresse
    if (!lexique.HostFieldGetData("WGEN1").trim().isEmpty()) {
      WGEN1.setSelected(true);
    }
    else if (!lexique.HostFieldGetData("WGEN2").trim().isEmpty()) {
      WGEN2.setSelected(true);
    }
    else if (!lexique.HostFieldGetData("WGEN3").trim().isEmpty()) {
      WGEN3.setSelected(true);
    }
    // Gestion des libelles
    lbGenerationBordereau1.setText(lexique.HostFieldGetData("LSL1").trim());
    lbGenerationBordereau2.setText(lexique.HostFieldGetData("LSL2").trim());
    lbGenerationBordereau3.setText(lexique.HostFieldGetData("LSL3").trim());
    WGEN1.setText("Trié par " + lexique.HostFieldGetData("LSL1").trim());
    WGEN2.setText("Trié par " + lexique.HostFieldGetData("LSL2").trim());
    WGEN3.setText("Trié par " + lexique.HostFieldGetData("LSL3").trim());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charge l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Charge les composants
    chargerComposantFamille();
    chargerComposantSousFamille();
    chargerListeComposantsMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Gestion selection complete
    // Magasin
    if (snMagasin1.getIdSelection() == null && snMagasin2.getIdSelection() == null && snMagasin3.getIdSelection() == null
        && snMagasin4.getIdSelection() == null && snMagasin5.getIdSelection() == null && snMagasin6.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOUM", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOUM", 0, "");
      snMagasin1.renseignerChampRPG(lexique, "MA01");
      snMagasin2.renseignerChampRPG(lexique, "MA02");
      snMagasin3.renseignerChampRPG(lexique, "MA03");
      snMagasin4.renseignerChampRPG(lexique, "MA04");
      snMagasin5.renseignerChampRPG(lexique, "MA05");
      snMagasin6.renseignerChampRPG(lexique, "MA06");
    }
    
    // Famille
    if (FAM_RB.isSelected()) {
      lexique.HostFieldPutData("ESFAM", 0, "");
      if (snFamilleDebut.getIdSelection() == null & snFamilleFin.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUF", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUF", 0, "");
        snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
        snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
      }
    }
    
    // Sous-famille
    if (SFAM_RB.isSelected()) {
      lexique.HostFieldPutData("WTOUF", 0, "");
      lexique.HostFieldPutData("FAMDEB", 0, "");
      lexique.HostFieldPutData("FAMFIN", 0, "");
      if (snSousFamille.getIdSelection() == null) {
        lexique.HostFieldPutData("ESFAM", 0, "**");
      }
      else {
        snSousFamille.renseignerChampRPG(lexique, "ESFAM");
      }
    }
    
    if (WGEN1.isSelected()) {
      lexique.HostFieldPutData("WGEN1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("WGEN1", 0, "");
    }
    if (WGEN2.isSelected()) {
      lexique.HostFieldPutData("WGEN2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("WGEN2", 0, "");
    }
    if (WGEN3.isSelected()) {
      lexique.HostFieldPutData("WGEN3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("WGEN3", 0, "");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void codeFamilleActionPerformed(ActionEvent e) {
    pnlSelectionFamille.setVisible(FAM_RB.isSelected());
    pnlCodeSousFamille.setVisible(!FAM_RB.isSelected());
    WGENF.setVisible(FAM_RB.isSelected());
    snSousFamille.setSelection(null);
  }
  
  private void codeSousFamilleActionPerformed(ActionEvent e) {
    pnlSelectionFamille.setVisible(!SFAM_RB.isSelected());
    pnlCodeSousFamille.setVisible(SFAM_RB.isSelected());
    WGENF.setVisible(FAM_RB.isSelected());
    snFamilleDebut.setSelection(null);
    snFamilleFin.setSelection(null);
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * snMagasin1 : "Tous" autorisé, "Aucun" interdit
   * snMagasin2-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On vérifie que le nombre de magasins n'est pas supérieur au nombre de composant magasin présent.
    // On charge un nombre de composants égal au nombre de choix possibles ou au nombre maximal de composants disponibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    for (int i = 0; i < listeComposant.size(); i++) {
      listeComposant.get(i).setVisible(false);
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On rend le composant snMagasin2 visibles si le composant snMagasin1 contient autre chose que "Tous".
      if (i == 0) {
        listeComposant.get(i).setVisible(!(snMagasin1.getIdSelection() == null));
      }
      // On rend les composants suivants visibles si le composant précédent contient autre chose que "Aucun" (null)
      else {
        listeComposant.get(i).setVisible(!(listeComposant.get(i - 1).getIdSelection() == null));
      }
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé.
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
  }
  
  /**
   * Initialise les composants famille
   */
  private void chargerComposantFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  /**
   * Initialise les composants sous-famille
   */
  private void chargerComposantSousFamille() {
    snSousFamille.setSession(getSession());
    snSousFamille.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamille.setTousAutorise(true);
    snSousFamille.charger(false);
    snSousFamille.setSelectionParChampRPG(lexique, "ESFAM");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeComposantsMagasin();
      chargerComposantFamille();
      chargerComposantSousFamille();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void TRIADSItemStateChanged(ItemEvent e) {
    if (lexique.isTrue("40") && TRIADS.getSelectedIndex() == 2) {
      pnlTriAdresse.setVisible(true);
      pnlTriBordereaux.setVisible(true);
      WGEN1.setVisible(true);
      WGEN2.setVisible(true);
      WGEN3.setVisible(true);
    }
    else {
      pnlTriAdresse.setVisible(false);
      pnlTriBordereaux.setVisible(false);
      WGEN1.setVisible(false);
      WGEN2.setVisible(false);
      WGEN3.setVisible(false);
    }
  }
  
  // On observe le changement de valeur du composant snMagasin1.
  // On affiche ou non le composants snMagasin2 si snMagasin1 différent de "Aucun" (null).
  // On affiche le composant s'il est censé pouvoir être affiché.
  private void snMagasin1ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 2 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 2) {
      snMagasin2.setVisible(!(snMagasin1.getIdSelection() == null));
      lbMagasin2.setVisible(snMagasin2.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin1.getIdSelection() == null) {
      snMagasin2.setSelection(null);
    }
  }
  
  // On effectue la même observation pour chaque composant snMagasin.
  // On affiche le composant suivant selon les même conditions.
  private void snMagasin2ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 3 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 3) {
      snMagasin3.setVisible(!(snMagasin2.getIdSelection() == null));
      lbMagasin3.setVisible(snMagasin3.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin2.getIdSelection() == null) {
      snMagasin3.setSelection(null);
    }
  }
  
  private void snMagasin3ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 4 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 4) {
      snMagasin4.setVisible(!(snMagasin3.getIdSelection() == null));
      lbMagasin4.setVisible(snMagasin4.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin3.getIdSelection() == null) {
      snMagasin4.setSelection(null);
    }
  }
  
  private void snMagasin4ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 5 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 5) {
      snMagasin5.setVisible(!(snMagasin4.getIdSelection() == null));
      lbMagasin5.setVisible(snMagasin5.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin4.getIdSelection() == null) {
      snMagasin5.setSelection(null);
    }
  }
  
  private void snMagasin5ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 6 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 6) {
      snMagasin6.setVisible(!(snMagasin5.getIdSelection() == null));
      lbMagasin6.setVisible(snMagasin6.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin5.getIdSelection() == null) {
      snMagasin6.setSelection(null);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    pnlBordereau = new SNPanel();
    lbBordereau = new SNLabelChamp();
    DATINV = new XRiCalendrier();
    GENQTE = new XRiCheckBox();
    FAM_RB = new JRadioButton();
    pnlSelectionFamille = new SNPanel();
    pnlBordereauFamille = new SNPanel();
    lbDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    SFAM_RB = new JRadioButton();
    pnlCodeSousFamille = new SNPanel();
    snSousFamille = new SNSousFamille();
    pnlMagasin = new SNPanel();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    pnlTriAdresse = new SNPanelTitre();
    lbGenerationBordereau1 = new SNLabelChamp();
    WADR1 = new XRiTextField();
    lbGenerationBordereau2 = new SNLabelChamp();
    WADR2 = new XRiTextField();
    lbGenerationBordereau3 = new SNLabelChamp();
    WADR3 = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfEnCours_ = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    ARTSTH = new XRiCheckBox();
    lbSautLigne = new SNLabelChamp();
    REPON2 = new XRiComboBox();
    lbOptionGeneration = new SNLabelChamp();
    TRIADS = new XRiComboBox();
    REPON1 = new XRiCheckBox();
    EDTCND = new XRiCheckBox();
    EDTMIN = new XRiCheckBox();
    WGENF = new XRiCheckBox();
    pnlTriBordereaux = new SNPanelTitre();
    WGEN1 = new JRadioButton();
    WGEN2 = new JRadioButton();
    WGEN3 = new JRadioButton();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("G\u00e9n\u00e9ration de bordereaux de saisie");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setPreferredSize(new Dimension(976, 175));
        pnlColonne.setMinimumSize(new Dimension(976, 175));
        pnlColonne.setMaximumSize(new Dimension(2147483647, 175));
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlBordereau ========
            {
              pnlBordereau.setName("pnlBordereau");
              pnlBordereau.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlBordereau.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlBordereau.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlBordereau.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlBordereau.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbBordereau ----
              lbBordereau.setText("Bordereau g\u00e9n\u00e9r\u00e9 au");
              lbBordereau.setName("lbBordereau");
              pnlBordereau.add(lbBordereau, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DATINV ----
              DATINV.setPreferredSize(new Dimension(110, 30));
              DATINV.setMinimumSize(new Dimension(110, 30));
              DATINV.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATINV.setMaximumSize(new Dimension(110, 30));
              DATINV.setName("DATINV");
              pnlBordereau.add(DATINV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- GENQTE ----
              GENQTE.setText("Sans g\u00e9n\u00e9ration quantit\u00e9");
              GENQTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              GENQTE.setFont(new Font("sansserif", Font.PLAIN, 14));
              GENQTE.setPreferredSize(new Dimension(200, 30));
              GENQTE.setMinimumSize(new Dimension(200, 30));
              GENQTE.setMaximumSize(new Dimension(200, 30));
              GENQTE.setName("GENQTE");
              pnlBordereau.add(GENQTE, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlBordereau, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- FAM_RB ----
            FAM_RB.setText("Codes familles \u00e0 traiter");
            FAM_RB.setFont(new Font("sansserif", Font.PLAIN, 14));
            FAM_RB.setPreferredSize(new Dimension(160, 30));
            FAM_RB.setMinimumSize(new Dimension(160, 30));
            FAM_RB.setName("FAM_RB");
            FAM_RB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                codeFamilleActionPerformed(e);
              }
            });
            pnlCritereDeSelection.add(FAM_RB, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSelectionFamille ========
            {
              pnlSelectionFamille.setBorder(null);
              pnlSelectionFamille.setOpaque(false);
              pnlSelectionFamille.setName("pnlSelectionFamille");
              pnlSelectionFamille.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSelectionFamille.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionFamille.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionFamille.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSelectionFamille.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ======== pnlBordereauFamille ========
              {
                pnlBordereauFamille.setOpaque(false);
                pnlBordereauFamille.setName("pnlBordereauFamille");
                pnlBordereauFamille.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlBordereauFamille.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlBordereauFamille.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlBordereauFamille.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlBordereauFamille.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbDebut ----
                lbDebut.setText("D\u00e9but");
                lbDebut.setMinimumSize(new Dimension(50, 30));
                lbDebut.setPreferredSize(new Dimension(50, 30));
                lbDebut.setMaximumSize(new Dimension(50, 30));
                lbDebut.setName("lbDebut");
                pnlBordereauFamille.add(lbDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- snFamilleDebut ----
                snFamilleDebut.setName("snFamilleDebut");
                pnlBordereauFamille.add(snFamilleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbFin ----
                lbFin.setText("Fin");
                lbFin.setPreferredSize(new Dimension(20, 30));
                lbFin.setMinimumSize(new Dimension(20, 30));
                lbFin.setMaximumSize(new Dimension(20, 30));
                lbFin.setName("lbFin");
                pnlBordereauFamille.add(lbFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- snFamilleFin ----
                snFamilleFin.setName("snFamilleFin");
                pnlBordereauFamille.add(snFamilleFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlSelectionFamille.add(pnlBordereauFamille, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlSelectionFamille, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- SFAM_RB ----
            SFAM_RB.setText("Code sous-famille \u00e0 traiter");
            SFAM_RB.setFont(new Font("sansserif", Font.PLAIN, 14));
            SFAM_RB.setMinimumSize(new Dimension(215, 30));
            SFAM_RB.setPreferredSize(new Dimension(215, 30));
            SFAM_RB.setName("SFAM_RB");
            SFAM_RB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                codeSousFamilleActionPerformed(e);
              }
            });
            pnlCritereDeSelection.add(SFAM_RB, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlCodeSousFamille ========
            {
              pnlCodeSousFamille.setName("pnlCodeSousFamille");
              pnlCodeSousFamille.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCodeSousFamille.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlCodeSousFamille.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCodeSousFamille.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCodeSousFamille.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- snSousFamille ----
              snSousFamille.setName("snSousFamille");
              pnlCodeSousFamille.add(snSousFamille, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlCodeSousFamille, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlMagasin ========
            {
              pnlMagasin.setName("pnlMagasin");
              pnlMagasin.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMagasin.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMagasin.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlMagasin.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlMagasin.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbMagasin1 ----
              lbMagasin1.setText("Magasin 1");
              lbMagasin1.setName("lbMagasin1");
              pnlMagasin.add(lbMagasin1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snMagasin1 ----
              snMagasin1.setFont(new Font("sansserif", Font.PLAIN, 14));
              snMagasin1.setName("snMagasin1");
              snMagasin1.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snMagasin1ValueChanged(e);
                }
              });
              pnlMagasin.add(snMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMagasin2 ----
              lbMagasin2.setText("Magasin 2");
              lbMagasin2.setName("lbMagasin2");
              pnlMagasin.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snMagasin2 ----
              snMagasin2.setFont(new Font("sansserif", Font.PLAIN, 14));
              snMagasin2.setName("snMagasin2");
              snMagasin2.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snMagasin2ValueChanged(e);
                }
              });
              pnlMagasin.add(snMagasin2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMagasin3 ----
              lbMagasin3.setText("Magasin 3");
              lbMagasin3.setName("lbMagasin3");
              pnlMagasin.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snMagasin3 ----
              snMagasin3.setFont(new Font("sansserif", Font.PLAIN, 14));
              snMagasin3.setName("snMagasin3");
              snMagasin3.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snMagasin3ValueChanged(e);
                }
              });
              pnlMagasin.add(snMagasin3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMagasin4 ----
              lbMagasin4.setText("Magasin 4");
              lbMagasin4.setName("lbMagasin4");
              pnlMagasin.add(lbMagasin4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snMagasin4 ----
              snMagasin4.setFont(new Font("sansserif", Font.PLAIN, 14));
              snMagasin4.setName("snMagasin4");
              snMagasin4.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snMagasin4ValueChanged(e);
                }
              });
              pnlMagasin.add(snMagasin4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMagasin5 ----
              lbMagasin5.setText("Magasin 5");
              lbMagasin5.setName("lbMagasin5");
              pnlMagasin.add(lbMagasin5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snMagasin5 ----
              snMagasin5.setFont(new Font("sansserif", Font.PLAIN, 14));
              snMagasin5.setName("snMagasin5");
              snMagasin5.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snMagasin5ValueChanged(e);
                }
              });
              pnlMagasin.add(snMagasin5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMagasin6 ----
              lbMagasin6.setText("Magasin 6");
              lbMagasin6.setName("lbMagasin6");
              pnlMagasin.add(lbMagasin6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snMagasin6 ----
              snMagasin6.setFont(new Font("sansserif", Font.PLAIN, 14));
              snMagasin6.setName("snMagasin6");
              pnlMagasin.add(snMagasin6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlMagasin, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTriAdresse ========
          {
            pnlTriAdresse.setForeground(Color.black);
            pnlTriAdresse.setTitre("Adresse de stockage");
            pnlTriAdresse.setName("pnlTriAdresse");
            pnlTriAdresse.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTriAdresse.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlTriAdresse.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlTriAdresse.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTriAdresse.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbGenerationBordereau1 ----
            lbGenerationBordereau1.setText("@LSL1@");
            lbGenerationBordereau1.setHorizontalAlignment(SwingConstants.RIGHT);
            lbGenerationBordereau1.setName("lbGenerationBordereau1");
            pnlTriAdresse.add(lbGenerationBordereau1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WADR1 ----
            WADR1.setMinimumSize(new Dimension(50, 30));
            WADR1.setPreferredSize(new Dimension(50, 30));
            WADR1.setFont(new Font("sansserif", Font.PLAIN, 14));
            WADR1.setMaximumSize(new Dimension(50, 30));
            WADR1.setName("WADR1");
            pnlTriAdresse.add(WADR1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbGenerationBordereau2 ----
            lbGenerationBordereau2.setText("@LSL2@");
            lbGenerationBordereau2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbGenerationBordereau2.setName("lbGenerationBordereau2");
            pnlTriAdresse.add(lbGenerationBordereau2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WADR2 ----
            WADR2.setPreferredSize(new Dimension(50, 30));
            WADR2.setMinimumSize(new Dimension(50, 30));
            WADR2.setFont(new Font("sansserif", Font.PLAIN, 14));
            WADR2.setMaximumSize(new Dimension(50, 30));
            WADR2.setName("WADR2");
            pnlTriAdresse.add(WADR2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbGenerationBordereau3 ----
            lbGenerationBordereau3.setText("@LSL3@");
            lbGenerationBordereau3.setHorizontalAlignment(SwingConstants.RIGHT);
            lbGenerationBordereau3.setName("lbGenerationBordereau3");
            pnlTriAdresse.add(lbGenerationBordereau3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WADR3 ----
            WADR3.setMinimumSize(new Dimension(50, 30));
            WADR3.setPreferredSize(new Dimension(50, 30));
            WADR3.setFont(new Font("sansserif", Font.PLAIN, 14));
            WADR3.setMaximumSize(new Dimension(50, 30));
            WADR3.setName("WADR3");
            pnlTriAdresse.add(WADR3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlGauche.add(pnlTriAdresse, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEnCours_ ----
            tfEnCours_.setText("@WENCX@");
            tfEnCours_.setEditable(false);
            tfEnCours_.setPreferredSize(new Dimension(260, 30));
            tfEnCours_.setMinimumSize(new Dimension(260, 30));
            tfEnCours_.setEnabled(false);
            tfEnCours_.setName("tfEnCours_");
            pnlEtablissement.add(tfEnCours_, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Option d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- ARTSTH ----
            ARTSTH.setText("Articles avec stock th\u00e9orique positif uniquement");
            ARTSTH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARTSTH.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARTSTH.setMinimumSize(new Dimension(323, 30));
            ARTSTH.setPreferredSize(new Dimension(323, 30));
            ARTSTH.setName("ARTSTH");
            pnlOptionEdition.add(ARTSTH, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSautLigne ----
            lbSautLigne.setText("Saut de ligne");
            lbSautLigne.setName("lbSautLigne");
            pnlOptionEdition.add(lbSautLigne, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPON2 ----
            REPON2.setModel(new DefaultComboBoxModel(new String[] { "Pas d'interligne", "Interligne simple", "Interligne tirets" }));
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON2.setMinimumSize(new Dimension(150, 30));
            REPON2.setPreferredSize(new Dimension(150, 30));
            REPON2.setName("REPON2");
            pnlOptionEdition.add(REPON2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbOptionGeneration ----
            lbOptionGeneration.setText("Option de g\u00e9n\u00e9ration");
            lbOptionGeneration.setMinimumSize(new Dimension(135, 30));
            lbOptionGeneration.setPreferredSize(new Dimension(135, 30));
            lbOptionGeneration.setMaximumSize(new Dimension(135, 30));
            lbOptionGeneration.setName("lbOptionGeneration");
            pnlOptionEdition.add(lbOptionGeneration, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- TRIADS ----
            TRIADS.setModel(new DefaultComboBoxModel(new String[] { "Bordereaux tri\u00e9s par articles",
                "Bordereaux tri\u00e9s par familles", "Bordereaux tri\u00e9s par adresses" }));
            TRIADS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRIADS.setFont(new Font("sansserif", Font.PLAIN, 14));
            TRIADS.setPreferredSize(new Dimension(250, 30));
            TRIADS.setMinimumSize(new Dimension(250, 30));
            TRIADS.setMaximumSize(new Dimension(250, 30));
            TRIADS.setName("TRIADS");
            TRIADS.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                TRIADSItemStateChanged(e);
              }
            });
            pnlOptionEdition.add(TRIADS, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- REPON1 ----
            REPON1.setText("Stock th\u00e9orique");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setPreferredSize(new Dimension(121, 30));
            REPON1.setMinimumSize(new Dimension(121, 30));
            REPON1.setName("REPON1");
            pnlOptionEdition.add(REPON1, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EDTCND ----
            EDTCND.setText("Conditionnement");
            EDTCND.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTCND.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTCND.setPreferredSize(new Dimension(135, 30));
            EDTCND.setMinimumSize(new Dimension(135, 30));
            EDTCND.setMaximumSize(new Dimension(135, 30));
            EDTCND.setName("EDTCND");
            pnlOptionEdition.add(EDTCND, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EDTMIN ----
            EDTMIN.setText("Stock minimum");
            EDTMIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTMIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTMIN.setMinimumSize(new Dimension(87, 30));
            EDTMIN.setPreferredSize(new Dimension(87, 30));
            EDTMIN.setName("EDTMIN");
            pnlOptionEdition.add(EDTMIN, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WGENF ----
            WGENF.setText("Un bordereau par famille");
            WGENF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WGENF.setFont(new Font("sansserif", Font.PLAIN, 14));
            WGENF.setPreferredSize(new Dimension(180, 30));
            WGENF.setMinimumSize(new Dimension(180, 30));
            WGENF.setName("WGENF");
            pnlOptionEdition.add(WGENF, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTriBordereaux ========
          {
            pnlTriBordereaux.setTitre("Tri du bordereaux par adresse");
            pnlTriBordereaux.setName("pnlTriBordereaux");
            pnlTriBordereaux.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTriBordereaux.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTriBordereaux.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlTriBordereaux.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTriBordereaux.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- WGEN1 ----
            WGEN1.setFont(new Font("sansserif", Font.PLAIN, 14));
            WGEN1.setText("Tri\u00e9 par @LSL1@");
            WGEN1.setPreferredSize(new Dimension(253, 30));
            WGEN1.setMinimumSize(new Dimension(253, 30));
            WGEN1.setMaximumSize(new Dimension(253, 30));
            WGEN1.setName("WGEN1");
            pnlTriBordereaux.add(WGEN1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WGEN2 ----
            WGEN2.setFont(new Font("sansserif", Font.PLAIN, 14));
            WGEN2.setText("Tri\u00e9 par @LSL12@ + @LSL2@");
            WGEN2.setPreferredSize(new Dimension(253, 30));
            WGEN2.setMinimumSize(new Dimension(253, 30));
            WGEN2.setMaximumSize(new Dimension(253, 30));
            WGEN2.setName("WGEN2");
            pnlTriBordereaux.add(WGEN2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WGEN3 ----
            WGEN3.setFont(new Font("sansserif", Font.PLAIN, 14));
            WGEN3.setText("Tri\u00e9 par @LSL13@ + @LSL21@ + @LSL3@");
            WGEN3.setPreferredSize(new Dimension(253, 30));
            WGEN3.setMinimumSize(new Dimension(253, 30));
            WGEN3.setMaximumSize(new Dimension(253, 30));
            WGEN3.setName("WGEN3");
            pnlTriBordereaux.add(WGEN3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlTriBordereaux, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(FAM_RB);
    buttonGroup1.add(SFAM_RB);
    
    // ---- buttonGroup2 ----
    ButtonGroup buttonGroup2 = new ButtonGroup();
    buttonGroup2.add(WGEN1);
    buttonGroup2.add(WGEN2);
    buttonGroup2.add(WGEN3);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNPanel pnlBordereau;
  private SNLabelChamp lbBordereau;
  private XRiCalendrier DATINV;
  private XRiCheckBox GENQTE;
  private JRadioButton FAM_RB;
  private SNPanel pnlSelectionFamille;
  private SNPanel pnlBordereauFamille;
  private SNLabelChamp lbDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFin;
  private SNFamille snFamilleFin;
  private JRadioButton SFAM_RB;
  private SNPanel pnlCodeSousFamille;
  private SNSousFamille snSousFamille;
  private SNPanel pnlMagasin;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNPanelTitre pnlTriAdresse;
  private SNLabelChamp lbGenerationBordereau1;
  private XRiTextField WADR1;
  private SNLabelChamp lbGenerationBordereau2;
  private XRiTextField WADR2;
  private SNLabelChamp lbGenerationBordereau3;
  private XRiTextField WADR3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfEnCours_;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox ARTSTH;
  private SNLabelChamp lbSautLigne;
  private XRiComboBox REPON2;
  private SNLabelChamp lbOptionGeneration;
  private XRiComboBox TRIADS;
  private XRiCheckBox REPON1;
  private XRiCheckBox EDTCND;
  private XRiCheckBox EDTMIN;
  private XRiCheckBox WGENF;
  private SNPanelTitre pnlTriBordereaux;
  private JRadioButton WGEN1;
  private JRadioButton WGEN2;
  private JRadioButton WGEN3;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
