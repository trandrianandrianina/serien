
package ri.serien.libecranrpg.sgvx.SGVX35FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM3237] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Inventaires -> Génération inventaire à zéro / Art.
 * Indicateur:00000001
 * Titre:Génération de bordereaux d'inventaire à zéro par articles non inventoriés
 * 
 * [GVM3238] Gestion des ventes -> Stocks -> Bordereaux stocks et inventaires -> Inventaires -> Génération inventaire à zéro / Fam.
 * Indicateur:10000001 (91)
 * Titre:Génération de bordereaux d'inventaire à zéro par famille
 */
public class SGVX35FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  
  public SGVX35FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    
    OEDT.setValeursSelection("OUI", "NON");
    WGNU.setValeursSelection("OUI", "NON");
    OHOM.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    Boolean isFamille = lexique.isTrue("91");
    
    // Titre
    if (isFamille) {
      bpPresentation.setText("Génération de bordereaux d'inventaire à zéro par famille");
    }
    else {
      bpPresentation.setText("Génération de bordereaux d'inventaire à zéro par articles non inventoriés");
    }
    
    // Visibilité sous famille
    lbSousFamilleDebut.setVisible(isFamille);
    lbSousFamilleFin.setVisible(isFamille);
    snSousFamilleDebut.setVisible(isFamille);
    snSousFamilleFin.setVisible(isFamille);
    
    // Visibilité famille
    lbFamilleDebut.setVisible(isFamille);
    lbFamilleFin.setVisible(isFamille);
    snFamilleDebut.setVisible(isFamille);
    snFamilleFin.setVisible(isFamille);
    
    // Visibilité article
    snArticleDebut.setVisible(!isFamille);
    snArticleFin.setVisible(!isFamille);
    lbArticleDebut.setVisible(!isFamille);
    lbArticleFin.setVisible(!isFamille);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Initialisation des composants
    chargerListeComposantsMagasin();
    chargerComposantFamille();
    chargerComposantSousFamille();
    chargerComposantArticle();
  }
  
  @Override
  public void getData() {
    super.getData();
    Boolean isFamille = lexique.isTrue("91");
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Gestion de la selection complete
    // Article
    if (!isFamille) {
      if (snArticleDebut.getSelection() == null && snArticleFin.getSelection() == null) {
        lexique.HostFieldPutData("WTOUA", 0, "**");
      }
      else {
        snArticleDebut.renseignerChampRPG(lexique, "EDEBA");
        snArticleFin.renseignerChampRPG(lexique, "EFINA");
        lexique.HostFieldPutData("WTOUA", 0, "");
      }
    }
    // Magasin
    if (snMagasin1.getIdSelection() == null && snMagasin2.getIdSelection() == null && snMagasin3.getIdSelection() == null
        && snMagasin4.getIdSelection() == null && snMagasin5.getIdSelection() == null && snMagasin6.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOUM", 0, "**");
    }
    else {
      snMagasin1.renseignerChampRPG(lexique, "MA01");
      snMagasin2.renseignerChampRPG(lexique, "MA02");
      snMagasin3.renseignerChampRPG(lexique, "MA03");
      snMagasin4.renseignerChampRPG(lexique, "MA04");
      snMagasin5.renseignerChampRPG(lexique, "MA05");
      snMagasin6.renseignerChampRPG(lexique, "MA06");
      lexique.HostFieldPutData("WTOUM", 0, "");
    }
    
    // Famille
    if (isFamille) {
      if (snFamilleDebut.getIdSelection() == null && snFamilleFin.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOU", 0, "**");
      }
      else {
        snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
        snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
        lexique.HostFieldPutData("WTOU", 0, "");
      }
      
      // Sous-famille
      if (snSousFamilleDebut.getIdSelection() == null && snSousFamilleFin.getIdSelection() == null) {
        lexique.HostFieldPutData("WTSFA", 0, "**");
      }
      else {
        snSousFamilleDebut.renseignerChampRPG(lexique, "SFADEB");
        snSousFamilleFin.renseignerChampRPG(lexique, "SFAFIN");
        lexique.HostFieldPutData("WTSFA", 0, "");
      }
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le composant snMagasin1 ne contient pas "Aucun"
   * snMagasin1-6 : "Tous" interdit, "Aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setAucunAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On vérifie que le nombre de magasins n'est pas inférieur au nombre de composant magasin présent.
    // Dans le cas où le nombre de magasins est inférieur au nombre de composants présents.
    // on affiche un nombre de composants égal au nombre de choix possibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    for (int i = 0; i < listeComposant.size(); i++) {
      listeComposant.get(i).setVisible(false);
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On rend le composant snMagasin2 visibles si le composant snMagasin1 contient autre chose que "Tous".
      if (i == 0) {
        listeComposant.get(i).setVisible(!(snMagasin1.getIdSelection() == null));
      }
      // On rend les composants suivants visibles si le composant précédent contient autre chose que "Aucun" (null)
      else {
        listeComposant.get(i).setVisible(!(listeComposant.get(i - 1).getIdSelection() == null));
      }
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé.
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
    
  }
  
  /**
   * Initialise les composant article
   */
  private void chargerComposantArticle() {
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "EDEBA");
    
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "EFINA");
  }
  
  /**
   * initialise le composant famille
   */
  private void chargerComposantFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(true);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(true);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  /**
   * initialise le composant Sous-famille
   */
  private void chargerComposantSousFamille() {
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.setTousAutorise(true);
    snSousFamilleDebut.charger(true);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "SFADEB");
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.setTousAutorise(true);
    snSousFamilleFin.charger(true);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "SFAFIN");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeComposantsMagasin();
      chargerComposantFamille();
      chargerComposantSousFamille();
      chargerComposantArticle();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // On observe le changement de valeur du composant snMagasin1.
  // On affiche ou non le composants snMagasin2 si snMagasin1 différent de "Aucun" (null).
  // On affiche le composant s'il est censé pouvoir être affiché.
  private void snMagasin1ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 2 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 2) {
      snMagasin2.setVisible(!(snMagasin1.getIdSelection() == null));
      lbMagasin2.setVisible(snMagasin2.isVisible());
    }
  }
  
  // On effectue la même observation pour chaque composant snMagasin.
  // On affiche le composant suivant selon les même conditions.
  private void snMagasin2ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 3 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 3) {
      snMagasin3.setVisible(!(snMagasin2.getIdSelection() == null));
      lbMagasin3.setVisible(snMagasin3.isVisible());
    }
  }
  
  private void snMagasin3ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 4 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 4) {
      snMagasin4.setVisible(!(snMagasin3.getIdSelection() == null));
      lbMagasin4.setVisible(snMagasin4.isVisible());
    }
  }
  
  private void snMagasin4ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 5 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 5) {
      snMagasin5.setVisible(!(snMagasin4.getIdSelection() == null));
      lbMagasin5.setVisible(snMagasin5.isVisible());
    }
  }
  
  private void snMagasin5ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 6 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 6) {
      snMagasin6.setVisible(!(snMagasin5.getIdSelection() == null));
      lbMagasin6.setVisible(snMagasin6.isVisible());
      if (snMagasin5.getIdSelection() == null) {
        snMagasin6.setSelection(null);
      }
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbDateInventaire = new SNLabelChamp();
    DATIX = new XRiCalendrier();
    lbArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticleFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    lbFamilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFamilleFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbSousFamilleDebut = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamilleFin = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfEnCours = new SNTexte();
    pnlOptions = new SNPanelTitre();
    OHOM = new XRiCheckBox();
    OEDT = new XRiCheckBox();
    WGNU = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1225, 700));
    setPreferredSize(new Dimension(1225, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateInventaire ----
            lbDateInventaire.setText("Date d'inventaire");
            lbDateInventaire.setName("lbDateInventaire");
            pnlCritereDeSelection.add(lbDateInventaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DATIX ----
            DATIX.setPreferredSize(new Dimension(110, 30));
            DATIX.setMinimumSize(new Dimension(110, 30));
            DATIX.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATIX.setName("DATIX");
            pnlCritereDeSelection.add(DATIX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleDebut ----
            lbArticleDebut.setText("Article de d\u00e9but");
            lbArticleDebut.setName("lbArticleDebut");
            pnlCritereDeSelection.add(lbArticleDebut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleDebut ----
            snArticleDebut.setPreferredSize(new Dimension(300, 30));
            snArticleDebut.setMaximumSize(new Dimension(300, 30));
            snArticleDebut.setMinimumSize(new Dimension(300, 30));
            snArticleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snArticleDebut.setName("snArticleDebut");
            pnlCritereDeSelection.add(snArticleDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleFin ----
            lbArticleFin.setText("Article de fin");
            lbArticleFin.setPreferredSize(new Dimension(50, 30));
            lbArticleFin.setMinimumSize(new Dimension(50, 30));
            lbArticleFin.setMaximumSize(new Dimension(50, 30));
            lbArticleFin.setName("lbArticleFin");
            pnlCritereDeSelection.add(lbArticleFin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleFin ----
            snArticleFin.setMaximumSize(new Dimension(300, 30));
            snArticleFin.setMinimumSize(new Dimension(300, 30));
            snArticleFin.setPreferredSize(new Dimension(300, 30));
            snArticleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snArticleFin.setName("snArticleFin");
            pnlCritereDeSelection.add(snArticleFin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleDebut ----
            lbFamilleDebut.setText("Famille de d\u00e9but");
            lbFamilleDebut.setName("lbFamilleDebut");
            pnlCritereDeSelection.add(lbFamilleDebut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereDeSelection.add(snFamilleDebut, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleFin ----
            lbFamilleFin.setText("Famille de fin");
            lbFamilleFin.setPreferredSize(new Dimension(50, 30));
            lbFamilleFin.setMinimumSize(new Dimension(50, 30));
            lbFamilleFin.setMaximumSize(new Dimension(50, 30));
            lbFamilleFin.setName("lbFamilleFin");
            pnlCritereDeSelection.add(lbFamilleFin, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleFin ----
            snFamilleFin.setName("snFamilleFin");
            pnlCritereDeSelection.add(snFamilleFin, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleDebut ----
            lbSousFamilleDebut.setText("Sous-famille de d\u00e9but");
            lbSousFamilleDebut.setName("lbSousFamilleDebut");
            pnlCritereDeSelection.add(lbSousFamilleDebut, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleDebut ----
            snSousFamilleDebut.setName("snSousFamilleDebut");
            pnlCritereDeSelection.add(snSousFamilleDebut, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleFin ----
            lbSousFamilleFin.setText("Sous-famille de fin");
            lbSousFamilleFin.setPreferredSize(new Dimension(50, 30));
            lbSousFamilleFin.setMinimumSize(new Dimension(50, 30));
            lbSousFamilleFin.setMaximumSize(new Dimension(50, 30));
            lbSousFamilleFin.setName("lbSousFamilleFin");
            pnlCritereDeSelection.add(lbSousFamilleFin, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleFin ----
            snSousFamilleFin.setName("snSousFamilleFin");
            pnlCritereDeSelection.add(snSousFamilleFin, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin1 ----
            lbMagasin1.setText("Magasin 1");
            lbMagasin1.setName("lbMagasin1");
            pnlCritereDeSelection.add(lbMagasin1, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setName("snMagasin1");
            snMagasin1.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin1ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin1, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin 2");
            lbMagasin2.setName("lbMagasin2");
            pnlCritereDeSelection.add(lbMagasin2, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin2 ----
            snMagasin2.setName("snMagasin2");
            snMagasin2.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin2ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin2, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Magasin 3");
            lbMagasin3.setName("lbMagasin3");
            pnlCritereDeSelection.add(lbMagasin3, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin3 ----
            snMagasin3.setName("snMagasin3");
            snMagasin3.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin3ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin3, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin4 ----
            lbMagasin4.setText("Magasin 4");
            lbMagasin4.setName("lbMagasin4");
            pnlCritereDeSelection.add(lbMagasin4, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin4 ----
            snMagasin4.setName("snMagasin4");
            snMagasin4.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin4ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin4, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin5 ----
            lbMagasin5.setText("Magasin 5");
            lbMagasin5.setName("lbMagasin5");
            pnlCritereDeSelection.add(lbMagasin5, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin5 ----
            snMagasin5.setName("snMagasin5");
            snMagasin5.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin5ValueChanged(e);
              }
            });
            pnlCritereDeSelection.add(snMagasin5, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin6 ----
            lbMagasin6.setText("Magasin 6");
            lbMagasin6.setName("lbMagasin6");
            pnlCritereDeSelection.add(lbMagasin6, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin6 ----
            snMagasin6.setName("snMagasin6");
            pnlCritereDeSelection.add(snMagasin6, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEditable(false);
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setEnabled(false);
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptions ========
          {
            pnlOptions.setTitre("Options");
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OHOM ----
            OHOM.setText("Homologation des bordereaux g\u00e9n\u00e9r\u00e9s");
            OHOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OHOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            OHOM.setMinimumSize(new Dimension(275, 30));
            OHOM.setPreferredSize(new Dimension(275, 30));
            OHOM.setMaximumSize(new Dimension(275, 30));
            OHOM.setName("OHOM");
            pnlOptions.add(OHOM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OEDT ----
            OEDT.setText("Edition des bordereaux g\u00e9n\u00e9r\u00e9s");
            OEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OEDT.setFont(new Font("sansserif", Font.PLAIN, 14));
            OEDT.setPreferredSize(new Dimension(250, 30));
            OEDT.setMinimumSize(new Dimension(250, 30));
            OEDT.setMaximumSize(new Dimension(250, 30));
            OEDT.setName("OEDT");
            pnlOptions.add(OEDT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WGNU ----
            WGNU.setText("G\u00e9n\u00e9ration articles avec stock nul");
            WGNU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WGNU.setFont(new Font("sansserif", Font.PLAIN, 14));
            WGNU.setMinimumSize(new Dimension(250, 30));
            WGNU.setPreferredSize(new Dimension(250, 30));
            WGNU.setMaximumSize(new Dimension(250, 30));
            WGNU.setName("WGNU");
            pnlOptions.add(WGNU, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbDateInventaire;
  private XRiCalendrier DATIX;
  private SNLabelChamp lbArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticleFin;
  private SNArticle snArticleFin;
  private SNLabelChamp lbFamilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFamilleFin;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSousFamilleDebut;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSousFamilleFin;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfEnCours;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox OHOM;
  private XRiCheckBox OEDT;
  private XRiCheckBox WGNU;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
