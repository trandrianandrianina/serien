
package ri.serien.libecranrpg.sgvx.SGVXJ02F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVXJ02F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] TARNAT_Value = { "TTC", "HT" };
  
  public SGVXJ02F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TARNAT.setValeurs(TARNAT_Value, null);
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    WENCX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    


    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    
    WENCX.setEnabled(false);
    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Codes Articles
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "ARTDEB");
    
    snArticlefin.setSession(getSession());
    snArticlefin.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    snArticlefin.charger(false);
    snArticlefin.setSelectionParChampRPG(lexique, "ARTFIN");
    
    // Magasins
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "CODMAG");
    
    // Famille
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
    
    // Sous-Famille
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.setTousAutorise(true);
    snSousFamilleDebut.charger(false);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "SFADEB");
    
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.setTousAutorise(true);
    snSousFamilleFin.charger(false);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "SFAFIN");
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    

    
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snArticleDebut.renseignerChampRPG(lexique, "ARTDEB");
    snArticlefin.renseignerChampRPG(lexique, "ARTFIN");
    snMagasin.renseignerChampRPG(lexique, "CODMAG");
    snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
    snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    snSousFamilleDebut.renseignerChampRPG(lexique, "SFADEB");
    snSousFamilleFin.renseignerChampRPG(lexique, "SFAFIN");
    
    if (REPON1.isSelected()) {
      lexique.HostFieldPutData("REPON1", 0, "OUI");
    }
    else {
      lexique.HostFieldPutData("REPON1", 0, "NON");
    }
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "CODMAG");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbWETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCODMAG = new SNLabelChamp();
    lbPeriodeEnCours = new SNLabelChamp();
    WENCX = new RiZoneSortie();
    snMagasin = new SNMagasin();
    pnlSelectionArticle = new SNPanelTitre();
    lbFAMDEB = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFAMFIN = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbSFADEB = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSFAFIN = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    lbARTDEB = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbARTFIN = new SNLabelChamp();
    snArticlefin = new SNArticle();
    pnlOption = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    lbTARNAT = new SNLabelChamp();
    TARNAT = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setOpaque(true);
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbWETB ----
          lbWETB.setText("Etablissement");
          lbWETB.setMaximumSize(new Dimension(200, 30));
          lbWETB.setMinimumSize(new Dimension(200, 30));
          lbWETB.setPreferredSize(new Dimension(200, 30));
          lbWETB.setInheritsPopupMenu(false);
          lbWETB.setName("lbWETB");
          pnlEtablissement.add(lbWETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCODMAG ----
          lbCODMAG.setText("Magasin");
          lbCODMAG.setMaximumSize(new Dimension(200, 30));
          lbCODMAG.setMinimumSize(new Dimension(200, 30));
          lbCODMAG.setPreferredSize(new Dimension(200, 30));
          lbCODMAG.setInheritsPopupMenu(false);
          lbCODMAG.setName("lbCODMAG");
          pnlEtablissement.add(lbCODMAG, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setMaximumSize(new Dimension(200, 30));
          lbPeriodeEnCours.setMinimumSize(new Dimension(200, 30));
          lbPeriodeEnCours.setPreferredSize(new Dimension(200, 30));
          lbPeriodeEnCours.setInheritsPopupMenu(false);
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WENCX ----
          WENCX.setText("@WENCX@");
          WENCX.setPreferredSize(new Dimension(220, 28));
          WENCX.setMinimumSize(new Dimension(220, 28));
          WENCX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WENCX.setMaximumSize(new Dimension(220, 28));
          WENCX.setName("WENCX");
          pnlEtablissement.add(WENCX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setName("snMagasin");
          pnlEtablissement.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlSelectionArticle ========
        {
          pnlSelectionArticle.setOpaque(false);
          pnlSelectionArticle.setTitre("S\u00e9lection des articles");
          pnlSelectionArticle.setName("pnlSelectionArticle");
          pnlSelectionArticle.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlSelectionArticle.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlSelectionArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlSelectionArticle.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlSelectionArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbFAMDEB ----
          lbFAMDEB.setText("Famille de d\u00e9but  ");
          lbFAMDEB.setMaximumSize(new Dimension(200, 30));
          lbFAMDEB.setMinimumSize(new Dimension(200, 30));
          lbFAMDEB.setPreferredSize(new Dimension(200, 30));
          lbFAMDEB.setInheritsPopupMenu(false);
          lbFAMDEB.setName("lbFAMDEB");
          pnlSelectionArticle.add(lbFAMDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snFamilleDebut ----
          snFamilleDebut.setName("snFamilleDebut");
          pnlSelectionArticle.add(snFamilleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbFAMFIN ----
          lbFAMFIN.setText("Code famille de fin  ");
          lbFAMFIN.setMaximumSize(new Dimension(200, 30));
          lbFAMFIN.setName("lbFAMFIN");
          pnlSelectionArticle.add(lbFAMFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snFamilleFin ----
          snFamilleFin.setName("snFamilleFin");
          pnlSelectionArticle.add(snFamilleFin, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbSFADEB ----
          lbSFADEB.setText("Sous-famille de d\u00e9but");
          lbSFADEB.setMaximumSize(new Dimension(200, 30));
          lbSFADEB.setMinimumSize(new Dimension(200, 30));
          lbSFADEB.setPreferredSize(new Dimension(200, 30));
          lbSFADEB.setInheritsPopupMenu(false);
          lbSFADEB.setName("lbSFADEB");
          pnlSelectionArticle.add(lbSFADEB, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snSousFamilleDebut ----
          snSousFamilleDebut.setName("snSousFamilleDebut");
          pnlSelectionArticle.add(snSousFamilleDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbSFAFIN ----
          lbSFAFIN.setText("Sous-famille de fin");
          lbSFAFIN.setMaximumSize(new Dimension(200, 30));
          lbSFAFIN.setName("lbSFAFIN");
          pnlSelectionArticle.add(lbSFAFIN, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snSousFamilleFin ----
          snSousFamilleFin.setName("snSousFamilleFin");
          pnlSelectionArticle.add(snSousFamilleFin, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbARTDEB ----
          lbARTDEB.setText("Article d\u00e9but");
          lbARTDEB.setMaximumSize(new Dimension(200, 30));
          lbARTDEB.setMinimumSize(new Dimension(200, 30));
          lbARTDEB.setPreferredSize(new Dimension(200, 30));
          lbARTDEB.setInheritsPopupMenu(false);
          lbARTDEB.setName("lbARTDEB");
          pnlSelectionArticle.add(lbARTDEB, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleDebut ----
          snArticleDebut.setName("snArticleDebut");
          pnlSelectionArticle.add(snArticleDebut, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbARTFIN ----
          lbARTFIN.setText("Article fin");
          lbARTFIN.setMaximumSize(new Dimension(200, 30));
          lbARTFIN.setMinimumSize(new Dimension(200, 30));
          lbARTFIN.setPreferredSize(new Dimension(200, 30));
          lbARTFIN.setInheritsPopupMenu(false);
          lbARTFIN.setName("lbARTFIN");
          pnlSelectionArticle.add(lbARTFIN, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snArticlefin ----
          snArticlefin.setName("snArticlefin");
          pnlSelectionArticle.add(snArticlefin, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlSelectionArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOption ========
        {
          pnlOption.setOpaque(false);
          pnlOption.setTitre("Options");
          pnlOption.setName("pnlOption");
          pnlOption.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOption.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlOption.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlOption.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOption.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- REPON1 ----
          REPON1.setText("S\u00e9lection des articles d\u00e9sactiv\u00e9s");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
          REPON1.setPreferredSize(new Dimension(250, 30));
          REPON1.setMaximumSize(new Dimension(250, 30));
          REPON1.setMinimumSize(new Dimension(250, 30));
          REPON1.setName("REPON1");
          pnlOption.add(REPON1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTARNAT ----
          lbTARNAT.setText("Tarif article \u00e0 exporter");
          lbTARNAT.setInheritsPopupMenu(false);
          lbTARNAT.setMaximumSize(new Dimension(200, 30));
          lbTARNAT.setMinimumSize(new Dimension(200, 30));
          lbTARNAT.setPreferredSize(new Dimension(200, 30));
          lbTARNAT.setName("lbTARNAT");
          pnlOption.add(lbTARNAT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- TARNAT ----
          TARNAT.setModel(new DefaultComboBoxModel(new String[] { "Tarif TTC", "Tarif HT" }));
          TARNAT.setComponentPopupMenu(null);
          TARNAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TARNAT.setFont(new Font("sansserif", Font.PLAIN, 14));
          TARNAT.setMinimumSize(new Dimension(100, 30));
          TARNAT.setPreferredSize(new Dimension(100, 30));
          TARNAT.setMaximumSize(new Dimension(100, 30));
          TARNAT.setName("TARNAT");
          pnlOption.add(TARNAT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlOption, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    
    // ======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");
      
      // ======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 200));
        menus_haut.setPreferredSize(new Dimension(160, 200));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());
      }
      scroll_droite.setViewportView(menus_haut);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbWETB;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCODMAG;
  private SNLabelChamp lbPeriodeEnCours;
  private RiZoneSortie WENCX;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlSelectionArticle;
  private SNLabelChamp lbFAMDEB;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFAMFIN;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSFADEB;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSFAFIN;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbARTDEB;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbARTFIN;
  private SNArticle snArticlefin;
  private SNPanelTitre pnlOption;
  private XRiCheckBox REPON1;
  private SNLabelChamp lbTARNAT;
  private XRiComboBox TARNAT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
