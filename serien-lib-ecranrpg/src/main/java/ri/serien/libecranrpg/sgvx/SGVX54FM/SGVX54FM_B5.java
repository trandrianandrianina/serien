
package ri.serien.libecranrpg.sgvx.SGVX54FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM3411] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Général -> Par famille
 * Indicateur:10010001 (91 et 94)
 * Titre: Chiffrage général des stocks par famille
 * 
 * [GVM3412] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Général -> Par article
 * Indicateur:10000001 (91)
 * Titre: Chiffrage général des stocks par article
 * 
 * [GVM3413] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Général -> Par mot de classement 2
 * Indicateurs:10100001 (91 et 93)
 * Titre: Chiffrage général des stocks par le 2ème mot de classement
 * 
 * [GVM3414] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Général -> Par sous-famille
 * Indicateur:11000001 (91 et 92)
 * Titre: Chiffrage général des stocks par sous-famille
 * 
 * [GVM3415] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Général -> Par fournisseur
 * Indicateur:10001001 (91 et 95)
 * Titre:Chiffrage général des stocks par fournisseur
 * 
 * [GVM3421] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Par magasin -> Par famille
 * Indicateur:00010001 (94)
 * Titre: Chiffrage général des stocks par magasin et par famille
 * 
 * [GVM3422] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Par magasin -> Par article
 * Indicateur: 00000001
 * Titre: Chiffrage des stocks par magasin et par article
 * 
 * [GVM3423] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Par magasin -> Par mot de classement 2
 * Indicateurs:00100001 (93)
 * Titre: Chiffrage général des stocks par magasin et par le 2ème mot de classement
 * 
 * [GVM3424] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Par magasin -> Par sous-famille
 * Indicateur:01000001 (92)
 * Titre: Chiffrage général des stocks par magasin et par sous-famille
 * 
 * [GVM3425] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Par magasin -> Par fournisseur
 * Indicateurs:00001001 (95)
 * Titre:Chiffrage général des stocks par magasin et par fournisseur
 * 
 * [GVM3461] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (général) -> Par famille
 * Indicateur:110010001(91,92 et 94)
 * Titre:Chiffrage général de lots de stocks par famille
 * 
 * [GVM3462] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (général) -> Par article
 * Indicateur:11000001 (91 et 92)
 * Titre:Chiffrage général des lots de stocks par article
 * 
 * [GVM3463] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (général) -> Par mot de classement 2
 * Indicateurs:110100001(91,92 et 94)
 * Titre:Chiffrage général des lots de stocks par le 2ème mot de classement
 *
 * [GVM3464] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (général) -> Par sous-famille
 * Indicateur:111000001(91,92 et 93)
 * Titre:Chiffrage général de lots de stocks par sous-famille
 * 
 * [GVM3465] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (général) -> Par fournisseur
 * Indicateur:110001001(91,92 et 95)
 * Titre: Chiffrage général des lots de stock par fournisseur
 * 
 * [GVM3471] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (par magasin) -> Par famille
 * Indicateur:100010001 (91 et 94)
 * Titre:Chiffrage des lots de stocks par magasin et par famille
 * 
 * [GVM3472] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (par magasin) -> Par article
 * Indicateur: Indicateur:10000001 (90)
 * Titre: Chiffrage général des lots de stocks par magasin et par articles
 * 
 * [GVM3473] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (par magasin) -> Par mot de classement 2
 * Indicateur:100100001(90 et 93)
 * Titre:Chiffrage général des lots de stocks par le magasin et par le 2ème mot de classement
 * 
 * [GVM3474] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (par magasin) -> Par sous-famille
 * Indicateur:101000001 (90 et 92)
 * Titre:Chiffrage des lots de stocks par magasin et par sous-famille
 * 
 * [GVM3475] Gestion des ventes -> Stocks -> Chiffrage des stocks -> Chiffrage stocks lots (par magasin) -> Par fournisseur
 * Indicateur:100001001 (90 et 95)
 * Titre:Chiffrage des lots de stocks par magasin et par fournisseur
 * 
 * [GAM3411] Gestion des achats -> Stocks -> Chiffrage stocks -> Général -> Par famille
 * Indicateur:10010001 (91 et 94)
 * Titre: Chiffrage général des stocks et par famille
 * 
 * [GAM3412] Gestion des achats -> Stocks -> Chiffrage stocks -> Général -> Par article
 * Indicateur:10000001 (91)
 * Titre: Chiffrage général des stocks par article
 * 
 * [GAM3413] Gestion des achats -> Stocks -> Chiffrage stocks -> Général -> Par mot de classement 2
 * Indicateurs:10100001 (91 et 93)
 * Titre: Chiffrage général des stocks par le 2ème mot de classement
 * 
 * [GAM3414] Gestion des achats -> Stocks -> Chiffrage stocks -> Général -> Par sous-famille
 * Indicateur:11000001 (91 et 92)
 * Titre: Chiffrage général des stocks par sous-famille
 * 
 * [GAM3415] Gestion des achats -> Stocks -> Chiffrage stocks -> Général -> Par fournisseur
 * Indicateur:10001001 (91 et 95)
 * Titre:Chiffrage général des stocks par fournisseur
 * 
 * [GAM3421] Gestion des achats -> Stocks -> Chiffrage stocks -> Par magasin -> Par famille
 * Indicateur:00010001 (94)
 * Titre: Chiffrage général des stocks par magasin et par famille
 * 
 * [GAM3422] Gestion des achats -> Stocks -> Chiffrage stocks -> Par magasin -> Par article
 * Indicateur: 00000001
 * Titre: Chiffrage des stocks par magasin et par article
 * 
 * [GAM3423] Gestion des achats -> Stocks -> Chiffrage stocks -> Par magasin -> Par mot de classement 2
 * Indicateurs:00100001 (93)
 * Titre: Chiffrage général des stocks par magasin et par le 2ème mot de classement
 * 
 * [GAM3424] Gestion des achats -> Stocks -> Chiffrage stocks -> Par magasin -> Par sous-famille
 * Indicateur:01000001 (92)
 * Titre: Chiffrage général des stocks par magasin et par sous-famille
 * 
 * [GAM3425] Gestion des achats -> Stocks -> Chiffrage stocks -> Par magasin -> Par fournisseur
 * Indicateurs:00001001 (95)
 * Titre:Chiffrage général des stocks par magasin et par fournisseur
 * 
 * [GAM3451] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (général) -> Par famille
 * Indicateur:110010001 (90,91 et 94)
 * Titre:Chiffrage des lots de stocks par famille
 * 
 * [GAM3452] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (général) -> Par article
 * Indicateur:110000001 (90 et 91)
 * Titre:Chiffrage des loits de stocks par article
 * 
 * [GAM3453] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (général) -> Par mot de classement 2
 * Indicateur:10100001 (90,91 et 93)
 * Titre:Chiffrage des lots de stocks par le 2ème mot de classement
 * 
 * [GAM3454] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (général) -> Par sous-famille
 * Indicateur:111000001 (90,91 et 92)
 * Titre:Chiffrage des lots de stocks par sous-famille
 * 
 * [GAM3455] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (général) -> Par fournisseur
 * Indicateur:110001001 (90,91 et 95)
 * Titre:Chiffrage des lots de stocks par fournisseur
 * 
 * [GAM3461] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (par magasin) -> Par famille
 * Indicateur:100010001 (90 et 94)
 * Titre:Chiffrage des lots de stocks par magasin et par famille
 * 
 * [GAM3462] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (par magasin) -> Par article
 * Indicateur:100000001(90)
 * Titre:Chiffrage des lots de stocks par magasin et par article
 * 
 * [GAM3463] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (par magasin) -> Par mot de classement 2
 * Indicateur:10100001(90 et 93)
 * Titre:Chiffrage des lots de stocks par magasin et par le 2ème mot de classement
 * 
 * [GAM3464] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (par magasin) -> Par sous-famille
 * Indicateur:101000001(90 et 92)
 * Titre:Chiffrage des lots de stock par magasin et par sous-famille
 * 
 * [GAM3465] Gestion des achats -> Stocks -> Chiffrage stocks -> Chiffrage stocks lots (par magasin) -> Par fournisseur
 * Indicateur:100001001(90 et 95)
 * Titre:Chiffrage des lots de stocks par magasin et par fournisseur
 * 
 * [GPM5412] Gestion de la production -> Stocks -> Chiffrages de stocks -> Général -> Par Article
 * Indicateur:10001001 (91 et 95)
 * Titre: Chiffrage général des stocks par article
 * 
 * [GPM5413] Gestion de la production -> Stocks -> Chiffrages de stocks -> Général -> Par Mot de Classement 2
 * Indicateur:10101001 (91,93 et 95)
 * Titre: Chiffrage général des stocks par le 2ème mot de classement
 * 
 * [GPM5414] Gestion de la production -> Stocks -> Chiffrages de stocks -> Général -> Par Sous-Famille
 * Indicateur:11001001 (91,92 et 95)
 * Titre: Chiffrage général des stocks par la sous-famille
 * 
 * [GPM5421] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Famille
 * Indicateur:00011001 (94 et 95)
 * Titre:Chiffrage général des stocks par magasin et par famille
 * 
 * [GPM5422] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Article
 * Indicateur:00001001 (95)
 * Titre:Chiffrage général des stocks par magasin et par article
 * 
 * [GPM5423] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Mot de Classement 2
 * Indicateur:00101001 (93 et 95)
 * Titre:Chiffrage général des stocks par magasin et par le 2ème mot de classement
 * 
 * [GPM5424] Gestion de la production -> Stocks -> Chiffrages de stocks -> Par Magasin -> Par Sous-Famille
 * Indicateur:01001001 (92 et 95)
 * Titre:Chiffrage général des stocks par magasin et par la sous-famille
 */
public class SGVX54FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_EXPORTER = "Exporter";
  private Message LOCTP = null;
  
  public SGVX54FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    
    CPRI5.setValeurs("5", "RB");
    CPRI4.setValeurs("4", "RB");
    CPRI3.setValeurs("3", "RB");
    CPRI2.setValeurs("2", "RB");
    CPRI1.setValeurs("1", "RB");
    
    EDTSIM.setValeursSelection("OUI", "NON");
    EDTDES.setValeursSelection("OUI", "NON");
    EDTPDS.setValeursSelection("OUI", "NON");
    EDTRES.setValeursSelection("OUI", "NON");
    RAZNEG.setValeursSelection("OUI", "NON");
    NWABC.setValeursSelection("1", " ");
    REPON1.setValeursSelection("OUI", "NON");
    GENVSK.setValeursSelection("F", " ");
    
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snSousFamilleDebut.lierComposantFin(snSousFamilleFin);
    
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'E', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Indicateur
    Boolean isFournisseurSeul = lexique.isTrue("87"); // is87 numauto fournisseur => pas de plage
    
    Boolean isMagasinVisible = lexique.isTrue("(N91)");
    Boolean isPlageCleArticle = lexique.isTrue("93");
    Boolean isPlageFamille = lexique.isTrue("94");
    Boolean isPlageSousFamille = lexique.isTrue("92");
    Boolean isPlageFournisseur = lexique.isTrue("95");
    Boolean isPlageArticle = lexique.isTrue("(N92) AND (N93) AND (N94) AND (N95)");
    
    // General
    Boolean isGeneralSousFamille = lexique.isTrue("(N90) AND 91 AND 92 AND (N93) AND (N94) AND (N95)");
    Boolean isGeneralFamille = lexique.isTrue("(N90) AND 91 AND (N92) AND (N93) AND 94 AND (N95)");
    Boolean isGeneralFournisseur = lexique.isTrue("(N90) AND 91 AND (N92) AND (N93) AND (N94) AND 95");
    Boolean isGeneralMotClassement = lexique.isTrue("(N90) AND 91 AND (N92) AND 93 AND (N94) AND (N95)");
    Boolean isGeneralArticle = lexique.isTrue("(N90) AND 91 AND (N92) AND (N93) AND (N94) AND (N95)");
    
    // General Magasin
    Boolean isGeneralMagasinMotClassement = lexique.isTrue("(N90) AND (N91) AND (N92) AND 93 AND (N94) AND (N95)");
    Boolean isGeneralMagasinSousFamille = lexique.isTrue("N90 AND (N91) AND 92 AND (N93) AND (N94) AND (N95)");
    Boolean isGeneralMagasinFamille = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N93) AND 94 AND (N95)");
    Boolean isGeneralMagasinFournisseur = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N93) AND (N94) AND 95 AND 59");
    Boolean isGeneralMagasinArticle = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N93) AND (N94) AND 95");
    
    // General lot
    Boolean isGeneralLotArticle = lexique.isTrue("(N90) AND 91 AND 92 AND (N93) AND (N94) AND (N95) AND 19");
    Boolean isGeneralLotFournisseur = lexique.isTrue("(N90) AND 91 AND 92 AND (N93) AND (N94) AND 95");
    Boolean isGeneralLotSousFamille = lexique.isTrue("(N90) AND 91 AND 92 AND (N93) AND 94 AND (N95)");
    
    // Lot
    Boolean isLotArticle = lexique.isTrue("90 AND 91 AND (N92) AND (N93) AND (N94) AND (N95)");
    Boolean isLotMagasinMotsClassement = lexique.isTrue("90 AND (N91) AND (N92) AND 93 AND (N94) AND (N95)");
    Boolean isLotMotClassement = lexique.isTrue("90 AND 91 AND (N92) AND 93 AND (N94) AND (N95)");
    Boolean isLotSousFamille = lexique.isTrue("90 AND 91 AND 92 AND (N93) AND (N94) AND (N95)");
    Boolean isLotFamille = lexique.isTrue("90 AND 91 AND (N92) AND (N93) AND 94 AND (N95)");
    Boolean isLotFournisseur = lexique.isTrue("90 AND 91 AND (N92) AND (N93) AND (N94) AND 95");
    
    // Lot Magasin
    Boolean isLotMagasinArticle = lexique.isTrue("90 AND (N92) AND (N93) AND (N94) AND (N95)");
    Boolean isLotMagasinSousFamille = lexique.isTrue("90 AND (N91) AND 92 AND (N93) AND (N94) AND (N95)");
    Boolean isLotMagasinFamille = lexique.isTrue("90 AND (N91) AND (N92) AND (N93) AND 94 AND (N95)");
    Boolean isLotMagasinFournisseur = lexique.isTrue("90 AND (N91) AND (N92) AND (N93) AND (N94) AND 95");
    
    // Spécifique
    Boolean isStockMagasinArticle = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N93) AND (N94) AND (N95)");
    Boolean isCalculStock = lexique.isTrue("(N90) AND (N91) AND (N92) AND (N93) AND (N94) AND 95");
    
    // Titre
    // General
    if (isGeneralFournisseur) {
      bpPresentation.setText("Chiffrage général des stocks par fournisseur");
    }
    if (isGeneralFamille || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5411")) {
      bpPresentation.setText("Chiffrage général des stocks par famille");
    }
    if (isGeneralSousFamille || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5414")) {
      bpPresentation.setText("Chiffrage général des stocks par sous-famille");
    }
    if (isGeneralMotClassement || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5413")) {
      bpPresentation.setText("Chiffrage général des stocks par le 2ème mot de classement");
    }
    if (isGeneralArticle || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5412")) {
      bpPresentation.setText("Chiffrage général des stocks par article");
    }
    
    // General Magasin
    if (isGeneralMagasinArticle || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5422")) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par article");
    }
    if (isGeneralMagasinSousFamille || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5424")) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par sous-famille");
    }
    if (isGeneralMagasinMotClassement || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5423")) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par le 2ème mot de classement");
    }
    if (isGeneralMagasinFamille || lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5421")) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par famille");
    }
    if (isGeneralMagasinFournisseur && (lexique.HostFieldGetData("LOCMNU").trim().contains("GVM3425")
        || lexique.HostFieldGetData("LOCMNU").trim().contains("GAM3425"))) {
      bpPresentation.setText("Chiffrage général des stocks par magasin et par fournisseur");
    }
    
    // General Lot
    if (isGeneralLotFournisseur && !(lexique.HostFieldGetData("LOCMNU").trim().contains("GPM5414"))) {
      bpPresentation.setText("Chiffrage général des lots de stock par fournisseur");
    }
    if (isGeneralLotSousFamille) {
      bpPresentation.setText("Chiffrage général de lots de stocks par sous-famille");
    }
    if (isGeneralLotArticle) {
      bpPresentation.setText("Chiffrage général des lots de stocks par article");
    }
    
    // Lot
    if (isLotFournisseur) {
      bpPresentation.setText("Chiffrage des lots de stocks par fournisseur");
    }
    if (isLotSousFamille) {
      bpPresentation.setText("Chiffrage des lots de stocks par sous-famille");
    }
    if (isLotFamille) {
      bpPresentation.setText("Chiffrage des lots de stocks par famille");
    }
    if (isLotMotClassement) {
      bpPresentation.setText("Chiffrage général des lots de stocks par le 2ème mot de classement");
    }
    if (isLotArticle) {
      bpPresentation.setText("Chiffrage des lots de stocks par article");
    }
    
    // Lot Magasin
    if (isLotMagasinSousFamille) {
      bpPresentation.setText("Chiffrage des lots de stock par magasin et par sous-famille");
    }
    if (isLotMagasinFamille) {
      bpPresentation.setText("Chiffrage des lots de stocks par magasin et par famille");
    }
    if (isLotMagasinFournisseur) {
      bpPresentation.setText("Chiffrage des lots de stocks par magasin et par fournisseur");
    }
    if (isLotMagasinArticle) {
      bpPresentation.setText("Chiffrage des lots de stocks par magasin et par article");
    }
    if (isLotMagasinMotsClassement) {
      bpPresentation.setText("Chiffrage des lots de stocks par magasin et par le 2ème mot de classement");
    }
    
    // Spécifique
    if (isCalculStock && lexique.HostFieldGetData("LOCMNU").trim().contains("GAM3951")) {
      bpPresentation.setText("Calcul des stocks sur les achats et par les consommation mensuel (par magasin)");
    }
    if (isStockMagasinArticle) {
      bpPresentation.setText("Chiffrage des stocks par magasin et par article");
    }
    
    // Permet de récupéré les bonnes valeurs
    if (isPlageCleArticle) {
      lbCleArticleDebut.setVisible(true);
      lbCleArticleFin.setVisible(true);
      DEBIAK.setVisible(true);
      FINIAK.setVisible(true);
      lbFamilleDebut.setVisible(false);
      lbFamilleFin.setVisible(false);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      
      lbSousFamilleDebut.setVisible(false);
      lbSousFamilleFin.setVisible(false);
      snSousFamilleDebut.setVisible(false);
      snSousFamilleFin.setVisible(false);
      
      lbFournisseurDebut.setVisible(false);
      lbFournisseurFin.setVisible(false);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      
      lbArticleDebut.setVisible(false);
      lbArticleFin.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else if (isPlageFamille) {
      lbCleArticleDebut.setVisible(false);
      lbCleArticleFin.setVisible(false);
      DEBIAK.setVisible(false);
      FINIAK.setVisible(false);
      
      lbFamilleDebut.setVisible(true);
      lbFamilleFin.setVisible(true);
      snFamilleDebut.setVisible(true);
      snFamilleFin.setVisible(true);
      
      lbSousFamilleDebut.setVisible(false);
      lbSousFamilleFin.setVisible(false);
      snSousFamilleDebut.setVisible(false);
      snSousFamilleFin.setVisible(false);
      
      lbFournisseurDebut.setVisible(false);
      lbFournisseurFin.setVisible(false);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      
      lbArticleDebut.setVisible(false);
      lbArticleFin.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else if (isPlageSousFamille) {
      lbCleArticleDebut.setVisible(false);
      lbCleArticleFin.setVisible(false);
      DEBIAK.setVisible(false);
      FINIAK.setVisible(false);
      
      lbFamilleDebut.setVisible(false);
      lbFamilleFin.setVisible(false);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      
      lbSousFamilleDebut.setVisible(true);
      lbSousFamilleFin.setVisible(true);
      snSousFamilleDebut.setVisible(true);
      snSousFamilleFin.setVisible(true);
      
      lbFournisseurDebut.setVisible(false);
      lbFournisseurFin.setVisible(false);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      
      lbArticleDebut.setVisible(false);
      lbArticleFin.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
    }
    else if (isPlageFournisseur) {
      lbCleArticleDebut.setVisible(false);
      lbCleArticleFin.setVisible(false);
      DEBIAK.setVisible(false);
      FINIAK.setVisible(false);
      
      lbFamilleDebut.setVisible(false);
      lbFamilleFin.setVisible(false);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      
      lbSousFamilleDebut.setVisible(true);
      lbSousFamilleFin.setVisible(true);
      snSousFamilleDebut.setVisible(true);
      snSousFamilleFin.setVisible(true);
      
      lbFournisseurDebut.setVisible(true);
      lbFournisseurFin.setVisible(!isFournisseurSeul);
      snFournisseurDebut.setVisible(true);
      snFournisseurFin.setVisible(!isFournisseurSeul);
      
      lbArticleDebut.setVisible(false);
      lbArticleFin.setVisible(false);
      snArticleDebut.setVisible(false);
      snArticleFin.setVisible(false);
      if (isFournisseurSeul) {
        lbFournisseurDebut.setText("Fournisseur");
      }
      else {
        lbFournisseurDebut.setText("Fournisseur début");
      }
    }
    else if (isPlageArticle) {
      lbCleArticleDebut.setVisible(false);
      lbCleArticleFin.setVisible(false);
      DEBIAK.setVisible(false);
      FINIAK.setVisible(false);
      
      lbFamilleDebut.setVisible(false);
      lbFamilleFin.setVisible(false);
      snFamilleDebut.setVisible(false);
      snFamilleFin.setVisible(false);
      
      lbSousFamilleDebut.setVisible(false);
      lbSousFamilleFin.setVisible(false);
      snSousFamilleDebut.setVisible(false);
      snSousFamilleFin.setVisible(false);
      
      lbFournisseurDebut.setVisible(false);
      lbFournisseurFin.setVisible(false);
      snFournisseurDebut.setVisible(false);
      snFournisseurFin.setVisible(false);
      
      lbArticleDebut.setVisible(true);
      lbArticleFin.setVisible(true);
      snArticleDebut.setVisible(true);
      snArticleFin.setVisible(true);
    }
    
    // Libellé zone personnalisée
    lbZonePersonnalise1.setText(lexique.HostFieldGetData("LZPL1").trim());
    lbZonePersonnalise2.setText(lexique.HostFieldGetData("LZPL2").trim());
    lbZonePersonnalise3.setText(lexique.HostFieldGetData("LZPL3").trim());
    lbZonePersonnalise4.setText(lexique.HostFieldGetData("LZPL4").trim());
    lbZonePersonnalise5.setText(lexique.HostFieldGetData("LZPL5").trim());
    
    // Gérer les erreurs automatique
    gererLesErreurs("19");
    
    // Visibilité des composants
    if (lexique.HostFieldGetData("LZP1").trim().isEmpty()) {
      lbZonePersonnalise1.setVisible(false);
      ZP1.setVisible(false);
    }
    if (lexique.HostFieldGetData("LZP2").trim().isEmpty()) {
      lbZonePersonnalise2.setVisible(false);
      ZP2.setVisible(false);
    }
    if (lexique.HostFieldGetData("LZP3").trim().isEmpty()) {
      lbZonePersonnalise3.setVisible(false);
      ZP3.setVisible(false);
    }
    if (lexique.HostFieldGetData("LZP4").trim().isEmpty()) {
      lbZonePersonnalise4.setVisible(false);
      ZP4.setVisible(false);
    }
    if (lexique.HostFieldGetData("LZP5").trim().isEmpty()) {
      lbZonePersonnalise5.setVisible(false);
      ZP5.setVisible(false);
    }
    snMagasin1.setVisible(isMagasinVisible);
    lbMagasin1.setVisible(isMagasinVisible);
    snMagasin2.setVisible(isMagasinVisible);
    lbMagasin2.setVisible(isMagasinVisible);
    snMagasin3.setVisible(isMagasinVisible);
    lbMagasin3.setVisible(isMagasinVisible);
    snMagasin4.setVisible(isMagasinVisible);
    lbMagasin4.setVisible(isMagasinVisible);
    snMagasin5.setVisible(isMagasinVisible);
    lbMagasin5.setVisible(isMagasinVisible);
    snMagasin6.setVisible(isMagasinVisible);
    lbMagasin6.setVisible(isMagasinVisible);
    
    GENVSK.setVisible(lexique.isTrue("70"));
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Charger l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charger les composants
    chargerListeComposantsMagasin();
    chargerFamille();
    chargerSousFamille();
    chargerFournisseur();
    chargerArticle();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    // Gestion de la sélection complete
    if (snMagasin1.isVisible() || snMagasin2.isVisible() || snMagasin3.isVisible() || snMagasin4.isVisible() || snMagasin5.isVisible()
        || snMagasin6.isVisible()) {
      if (snMagasin1.getIdSelection() == null && snMagasin2.getIdSelection() == null && snMagasin3.getIdSelection() == null
          && snMagasin4.getIdSelection() == null && snMagasin5.getIdSelection() == null && snMagasin6.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUM", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUM", 0, "  ");
        snMagasin1.renseignerChampRPG(lexique, "MA01");
        snMagasin2.renseignerChampRPG(lexique, "MA02");
        snMagasin3.renseignerChampRPG(lexique, "MA03");
        snMagasin4.renseignerChampRPG(lexique, "MA04");
        snMagasin5.renseignerChampRPG(lexique, "MA05");
        snMagasin6.renseignerChampRPG(lexique, "MA06");
      }
    }
    // 2eme clé article
    if (DEBIAK.isVisible() && FINIAK.isVisible()) {
      if (DEBIAK.getText().isEmpty() && FINIAK.getText().isEmpty()) {
        lexique.HostFieldPutData("WTOU", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU", 0, "");
      }
    }
    // Famille
    if (snFamilleDebut.isVisible() && snFamilleFin.isVisible()) {
      if (snFamilleDebut.getIdSelection() == null & snFamilleFin.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOU", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU", 0, "");
        snFamilleDebut.renseignerChampRPG(lexique, "EDEB");
        snFamilleFin.renseignerChampRPG(lexique, "EFIN");
      }
    }
    // Sous-famille
    if (snSousFamilleDebut.isVisible() && snSousFamilleFin.isVisible()) {
      if (snSousFamilleDebut.getIdSelection() == null & snSousFamilleFin.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOU", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU", 0, "");
        snSousFamilleDebut.renseignerChampRPG(lexique, "DEBIAE");
        snSousFamilleFin.renseignerChampRPG(lexique, "FINIAE");
      }
    }
    // Fournisseur
    if (snFournisseurDebut.isVisible() && snFournisseurFin.isVisible()) {
      if (snFournisseurDebut.getSelection() == null & snFournisseurFin.getSelection() == null) {
        lexique.HostFieldPutData("WTOU", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU", 0, "");
        snFournisseurDebut.renseignerChampRPG(lexique, "FRSDEB");
        snFournisseurFin.renseignerChampRPG(lexique, "FRSFIN");
      }
    }
    // Article
    if (snArticleDebut.isVisible() && snArticleFin.isVisible()) {
      if (snArticleDebut.getSelection() == null & snArticleFin.getSelection() == null) {
        lexique.HostFieldPutData("WTOU", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU", 0, "");
        snArticleDebut.renseignerChampRPG(lexique, "EDEBA");
        snArticleFin.renseignerChampRPG(lexique, "EFINA");
      }
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * snMagasin1 : "Tous" autorisé, "Aucun" interdit
   * snMagasin2-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    if (lexique.isTrue("91")) {
      return;
    }
    snMagasin2.setVisible(false);
    snMagasin3.setVisible(false);
    snMagasin4.setVisible(false);
    snMagasin5.setVisible(false);
    snMagasin6.setVisible(false);
    
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On vérifie que le nombre de magasins n'est pas supérieur au nombre de composant magasin présent.
    // On charge un nombre de composants égal au nombre de choix possibles ou au nombre maximal de composants disponibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On affiche les composants snMagasin2-6 si ils ne sont pas en "Aucun" (null)
      listeComposant.get(i).setVisible(!(listeComposant.get(i).getIdSelection() == null));
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
  }
  
  /*
   * Charge les familes
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "EDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "EFIN");
  }
  
  /*
   * Charge les sous-familles
   */
  private void chargerSousFamille() {
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.setTousAutorise(true);
    snSousFamilleDebut.charger(false);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "DEBIAE");
    
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.setTousAutorise(true);
    snSousFamilleFin.charger(false);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "FINIAE");
  }
  
  /*
   * Charge les fournisseur
   */
  private void chargerFournisseur() {
    snFournisseurDebut.setSession(getSession());
    snFournisseurDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurDebut.charger(false);
    snFournisseurDebut.setSelectionParChampRPG(lexique, "FRSDEB");
    
    snFournisseurFin.setSession(getSession());
    snFournisseurFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurFin.charger(false);
    snFournisseurFin.setSelectionParChampRPG(lexique, "FRSFIN");
  }
  
  /*
   * Charge les article
   */
  private void chargerArticle() {
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "EDEBA");
    
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "EFINA");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    lbCleArticleDebut = new SNLabelChamp();
    DEBIAK = new XRiTextField();
    lbCleArticleFin = new SNLabelChamp();
    FINIAK = new XRiTextField();
    lbFamilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFamilleFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbSousFamilleDebut = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamilleFin = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    lbFournisseurDebut = new SNLabelChamp();
    snFournisseurDebut = new SNFournisseur();
    lbFournisseurFin = new SNLabelChamp();
    snFournisseurFin = new SNFournisseur();
    lbArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticleFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    pnlZonePersonnalise = new SNPanel();
    lbZonePersonnalise1 = new SNLabelChamp();
    ZP1 = new XRiTextField();
    lbZonePersonnalise4 = new SNLabelChamp();
    ZP4 = new XRiTextField();
    lbZonePersonnalise2 = new SNLabelChamp();
    ZP2 = new XRiTextField();
    lbZonePersonnalise5 = new SNLabelChamp();
    ZP5 = new XRiTextField();
    lbZonePersonnalise3 = new SNLabelChamp();
    ZP3 = new XRiTextField();
    lbCodeABC = new SNLabelChamp();
    pnlABCD = new SNPanel();
    WABC = new XRiTextField();
    NWABC = new XRiCheckBox();
    lbChiffrageDeStock = new SNLabelChamp();
    DATCHF = new XRiCalendrier();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    pnlOptions = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    EDTRES = new XRiCheckBox();
    EDTSIM = new XRiCheckBox();
    EDTPDS = new XRiCheckBox();
    EDTDES = new XRiCheckBox();
    RAZNEG = new XRiCheckBox();
    GENVSK = new XRiCheckBox();
    pnlPrix = new SNPanelTitre();
    CPRI1 = new XRiRadioButton();
    CPRI2 = new XRiRadioButton();
    CPRI3 = new XRiRadioButton();
    CPRI4 = new XRiRadioButton();
    CPRI5 = new XRiRadioButton();
    snBarreBouton = new SNBarreBouton();
    RB_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights =
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin1 ----
            lbMagasin1.setText("Magasin 1");
            lbMagasin1.setMinimumSize(new Dimension(185, 30));
            lbMagasin1.setPreferredSize(new Dimension(185, 30));
            lbMagasin1.setMaximumSize(new Dimension(185, 30));
            lbMagasin1.setName("lbMagasin1");
            pnlCritereDeSelection.add(lbMagasin1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin1.setEnabled(false);
            snMagasin1.setName("snMagasin1");
            pnlCritereDeSelection.add(snMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin 2");
            lbMagasin2.setMinimumSize(new Dimension(185, 30));
            lbMagasin2.setPreferredSize(new Dimension(185, 30));
            lbMagasin2.setMaximumSize(new Dimension(185, 30));
            lbMagasin2.setName("lbMagasin2");
            pnlCritereDeSelection.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin2 ----
            snMagasin2.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin2.setEnabled(false);
            snMagasin2.setName("snMagasin2");
            pnlCritereDeSelection.add(snMagasin2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Magasin 3");
            lbMagasin3.setMinimumSize(new Dimension(185, 30));
            lbMagasin3.setPreferredSize(new Dimension(185, 30));
            lbMagasin3.setMaximumSize(new Dimension(185, 30));
            lbMagasin3.setName("lbMagasin3");
            pnlCritereDeSelection.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin3 ----
            snMagasin3.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin3.setEnabled(false);
            snMagasin3.setName("snMagasin3");
            pnlCritereDeSelection.add(snMagasin3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin4 ----
            lbMagasin4.setText("Magasin 4");
            lbMagasin4.setMinimumSize(new Dimension(185, 30));
            lbMagasin4.setPreferredSize(new Dimension(185, 30));
            lbMagasin4.setMaximumSize(new Dimension(185, 30));
            lbMagasin4.setName("lbMagasin4");
            pnlCritereDeSelection.add(lbMagasin4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin4 ----
            snMagasin4.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin4.setEnabled(false);
            snMagasin4.setName("snMagasin4");
            pnlCritereDeSelection.add(snMagasin4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin5 ----
            lbMagasin5.setText("Magasin 5");
            lbMagasin5.setMinimumSize(new Dimension(185, 30));
            lbMagasin5.setPreferredSize(new Dimension(185, 30));
            lbMagasin5.setMaximumSize(new Dimension(185, 30));
            lbMagasin5.setName("lbMagasin5");
            pnlCritereDeSelection.add(lbMagasin5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin5 ----
            snMagasin5.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin5.setEnabled(false);
            snMagasin5.setName("snMagasin5");
            pnlCritereDeSelection.add(snMagasin5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin6 ----
            lbMagasin6.setText("Magasin 6");
            lbMagasin6.setMinimumSize(new Dimension(185, 30));
            lbMagasin6.setPreferredSize(new Dimension(185, 30));
            lbMagasin6.setMaximumSize(new Dimension(185, 30));
            lbMagasin6.setName("lbMagasin6");
            pnlCritereDeSelection.add(lbMagasin6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin6 ----
            snMagasin6.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin6.setEnabled(false);
            snMagasin6.setName("snMagasin6");
            pnlCritereDeSelection.add(snMagasin6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCleArticleDebut ----
            lbCleArticleDebut.setText("2eme cl\u00e9 article de d\u00e9but");
            lbCleArticleDebut.setMinimumSize(new Dimension(185, 30));
            lbCleArticleDebut.setPreferredSize(new Dimension(185, 30));
            lbCleArticleDebut.setMaximumSize(new Dimension(185, 30));
            lbCleArticleDebut.setName("lbCleArticleDebut");
            pnlCritereDeSelection.add(lbCleArticleDebut, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DEBIAK ----
            DEBIAK.setEnabled(false);
            DEBIAK.setPreferredSize(new Dimension(250, 30));
            DEBIAK.setMinimumSize(new Dimension(250, 30));
            DEBIAK.setName("DEBIAK");
            pnlCritereDeSelection.add(DEBIAK, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCleArticleFin ----
            lbCleArticleFin.setText("2eme cl\u00e9 article de fin");
            lbCleArticleFin.setMinimumSize(new Dimension(185, 30));
            lbCleArticleFin.setPreferredSize(new Dimension(185, 30));
            lbCleArticleFin.setMaximumSize(new Dimension(185, 30));
            lbCleArticleFin.setName("lbCleArticleFin");
            pnlCritereDeSelection.add(lbCleArticleFin, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- FINIAK ----
            FINIAK.setEnabled(false);
            FINIAK.setPreferredSize(new Dimension(250, 30));
            FINIAK.setMinimumSize(new Dimension(250, 30));
            FINIAK.setName("FINIAK");
            pnlCritereDeSelection.add(FINIAK, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleDebut ----
            lbFamilleDebut.setText("Famille de d\u00e9but");
            lbFamilleDebut.setMinimumSize(new Dimension(185, 30));
            lbFamilleDebut.setPreferredSize(new Dimension(185, 30));
            lbFamilleDebut.setMaximumSize(new Dimension(185, 30));
            lbFamilleDebut.setName("lbFamilleDebut");
            pnlCritereDeSelection.add(lbFamilleDebut, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFamilleDebut.setEnabled(false);
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereDeSelection.add(snFamilleDebut, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleFin ----
            lbFamilleFin.setText("Famille de fin");
            lbFamilleFin.setMinimumSize(new Dimension(185, 30));
            lbFamilleFin.setPreferredSize(new Dimension(185, 30));
            lbFamilleFin.setMaximumSize(new Dimension(185, 30));
            lbFamilleFin.setName("lbFamilleFin");
            pnlCritereDeSelection.add(lbFamilleFin, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleFin ----
            snFamilleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFamilleFin.setEnabled(false);
            snFamilleFin.setName("snFamilleFin");
            pnlCritereDeSelection.add(snFamilleFin, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleDebut ----
            lbSousFamilleDebut.setText("Sous-famille de d\u00e9but");
            lbSousFamilleDebut.setMinimumSize(new Dimension(185, 30));
            lbSousFamilleDebut.setPreferredSize(new Dimension(185, 30));
            lbSousFamilleDebut.setMaximumSize(new Dimension(185, 30));
            lbSousFamilleDebut.setName("lbSousFamilleDebut");
            pnlCritereDeSelection.add(lbSousFamilleDebut, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleDebut ----
            snSousFamilleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snSousFamilleDebut.setEnabled(false);
            snSousFamilleDebut.setName("snSousFamilleDebut");
            pnlCritereDeSelection.add(snSousFamilleDebut, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleFin ----
            lbSousFamilleFin.setText("Sous-famille de fin");
            lbSousFamilleFin.setMinimumSize(new Dimension(185, 30));
            lbSousFamilleFin.setPreferredSize(new Dimension(185, 30));
            lbSousFamilleFin.setMaximumSize(new Dimension(185, 30));
            lbSousFamilleFin.setName("lbSousFamilleFin");
            pnlCritereDeSelection.add(lbSousFamilleFin, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleFin ----
            snSousFamilleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snSousFamilleFin.setEnabled(false);
            snSousFamilleFin.setName("snSousFamilleFin");
            pnlCritereDeSelection.add(snSousFamilleFin, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurDebut ----
            lbFournisseurDebut.setText("Voir texte dans le code");
            lbFournisseurDebut.setMinimumSize(new Dimension(185, 30));
            lbFournisseurDebut.setPreferredSize(new Dimension(185, 30));
            lbFournisseurDebut.setMaximumSize(new Dimension(185, 30));
            lbFournisseurDebut.setName("lbFournisseurDebut");
            pnlCritereDeSelection.add(lbFournisseurDebut, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseurDebut ----
            snFournisseurDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFournisseurDebut.setEnabled(false);
            snFournisseurDebut.setName("snFournisseurDebut");
            pnlCritereDeSelection.add(snFournisseurDebut, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurFin ----
            lbFournisseurFin.setText("Fournisseur de fin");
            lbFournisseurFin.setMinimumSize(new Dimension(185, 30));
            lbFournisseurFin.setPreferredSize(new Dimension(185, 30));
            lbFournisseurFin.setMaximumSize(new Dimension(185, 30));
            lbFournisseurFin.setName("lbFournisseurFin");
            pnlCritereDeSelection.add(lbFournisseurFin, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseurFin ----
            snFournisseurFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFournisseurFin.setEnabled(false);
            snFournisseurFin.setName("snFournisseurFin");
            pnlCritereDeSelection.add(snFournisseurFin, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleDebut ----
            lbArticleDebut.setText("Article de d\u00e9but");
            lbArticleDebut.setMinimumSize(new Dimension(185, 30));
            lbArticleDebut.setPreferredSize(new Dimension(185, 30));
            lbArticleDebut.setMaximumSize(new Dimension(185, 30));
            lbArticleDebut.setName("lbArticleDebut");
            pnlCritereDeSelection.add(lbArticleDebut, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleDebut ----
            snArticleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snArticleDebut.setEnabled(false);
            snArticleDebut.setName("snArticleDebut");
            pnlCritereDeSelection.add(snArticleDebut, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleFin ----
            lbArticleFin.setText("Article de fin");
            lbArticleFin.setMinimumSize(new Dimension(185, 30));
            lbArticleFin.setPreferredSize(new Dimension(185, 30));
            lbArticleFin.setMaximumSize(new Dimension(185, 30));
            lbArticleFin.setName("lbArticleFin");
            pnlCritereDeSelection.add(lbArticleFin, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleFin ----
            snArticleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snArticleFin.setEnabled(false);
            snArticleFin.setName("snArticleFin");
            pnlCritereDeSelection.add(snArticleFin, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlZonePersonnalise ========
            {
              pnlZonePersonnalise.setName("pnlZonePersonnalise");
              pnlZonePersonnalise.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbZonePersonnalise1 ----
              lbZonePersonnalise1.setText("LZPL1");
              lbZonePersonnalise1.setMinimumSize(new Dimension(185, 30));
              lbZonePersonnalise1.setMaximumSize(new Dimension(185, 30));
              lbZonePersonnalise1.setPreferredSize(new Dimension(185, 30));
              lbZonePersonnalise1.setName("lbZonePersonnalise1");
              pnlZonePersonnalise.add(lbZonePersonnalise1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ZP1 ----
              ZP1.setFont(new Font("sansserif", Font.PLAIN, 14));
              ZP1.setPreferredSize(new Dimension(36, 30));
              ZP1.setMinimumSize(new Dimension(36, 30));
              ZP1.setName("ZP1");
              pnlZonePersonnalise.add(ZP1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbZonePersonnalise4 ----
              lbZonePersonnalise4.setText("LZPL4");
              lbZonePersonnalise4.setMaximumSize(new Dimension(185, 30));
              lbZonePersonnalise4.setMinimumSize(new Dimension(185, 30));
              lbZonePersonnalise4.setPreferredSize(new Dimension(185, 30));
              lbZonePersonnalise4.setName("lbZonePersonnalise4");
              pnlZonePersonnalise.add(lbZonePersonnalise4, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ZP4 ----
              ZP4.setFont(new Font("sansserif", Font.PLAIN, 14));
              ZP4.setPreferredSize(new Dimension(36, 30));
              ZP4.setMinimumSize(new Dimension(36, 30));
              ZP4.setName("ZP4");
              pnlZonePersonnalise.add(ZP4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbZonePersonnalise2 ----
              lbZonePersonnalise2.setText("LZPL2");
              lbZonePersonnalise2.setPreferredSize(new Dimension(185, 30));
              lbZonePersonnalise2.setMinimumSize(new Dimension(185, 30));
              lbZonePersonnalise2.setMaximumSize(new Dimension(185, 30));
              lbZonePersonnalise2.setName("lbZonePersonnalise2");
              pnlZonePersonnalise.add(lbZonePersonnalise2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ZP2 ----
              ZP2.setFont(new Font("sansserif", Font.PLAIN, 14));
              ZP2.setPreferredSize(new Dimension(36, 30));
              ZP2.setMinimumSize(new Dimension(36, 30));
              ZP2.setName("ZP2");
              pnlZonePersonnalise.add(ZP2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbZonePersonnalise5 ----
              lbZonePersonnalise5.setText("LZPL5");
              lbZonePersonnalise5.setPreferredSize(new Dimension(185, 30));
              lbZonePersonnalise5.setMinimumSize(new Dimension(185, 30));
              lbZonePersonnalise5.setMaximumSize(new Dimension(185, 30));
              lbZonePersonnalise5.setName("lbZonePersonnalise5");
              pnlZonePersonnalise.add(lbZonePersonnalise5, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ZP5 ----
              ZP5.setFont(new Font("sansserif", Font.PLAIN, 14));
              ZP5.setPreferredSize(new Dimension(36, 30));
              ZP5.setMinimumSize(new Dimension(36, 30));
              ZP5.setName("ZP5");
              pnlZonePersonnalise.add(ZP5, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbZonePersonnalise3 ----
              lbZonePersonnalise3.setText("LZPL3");
              lbZonePersonnalise3.setMaximumSize(new Dimension(185, 30));
              lbZonePersonnalise3.setMinimumSize(new Dimension(185, 30));
              lbZonePersonnalise3.setPreferredSize(new Dimension(185, 30));
              lbZonePersonnalise3.setName("lbZonePersonnalise3");
              pnlZonePersonnalise.add(lbZonePersonnalise3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ZP3 ----
              ZP3.setFont(new Font("sansserif", Font.PLAIN, 14));
              ZP3.setPreferredSize(new Dimension(36, 30));
              ZP3.setMinimumSize(new Dimension(36, 30));
              ZP3.setName("ZP3");
              pnlZonePersonnalise.add(ZP3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            }
            pnlCritereDeSelection.add(pnlZonePersonnalise, new GridBagConstraints(0, 16, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeABC ----
            lbCodeABC.setText("Code A,B,C,D");
            lbCodeABC.setMinimumSize(new Dimension(185, 30));
            lbCodeABC.setPreferredSize(new Dimension(185, 30));
            lbCodeABC.setMaximumSize(new Dimension(185, 30));
            lbCodeABC.setName("lbCodeABC");
            pnlCritereDeSelection.add(lbCodeABC, new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlABCD ========
            {
              pnlABCD.setName("pnlABCD");
              pnlABCD.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlABCD.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlABCD.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlABCD.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlABCD.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WABC ----
              WABC.setFont(new Font("sansserif", Font.PLAIN, 14));
              WABC.setMinimumSize(new Dimension(24, 30));
              WABC.setPreferredSize(new Dimension(24, 30));
              WABC.setName("WABC");
              pnlABCD.add(WABC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- NWABC ----
              NWABC.setText("Exclusion");
              NWABC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              NWABC.setFont(new Font("sansserif", Font.PLAIN, 14));
              NWABC.setMinimumSize(new Dimension(85, 30));
              NWABC.setPreferredSize(new Dimension(85, 30));
              NWABC.setMaximumSize(new Dimension(85, 30));
              NWABC.setName("NWABC");
              pnlABCD.add(NWABC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlABCD, new GridBagConstraints(1, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbChiffrageDeStock ----
            lbChiffrageDeStock.setText("Chiffrage des stocks");
            lbChiffrageDeStock.setMinimumSize(new Dimension(185, 30));
            lbChiffrageDeStock.setPreferredSize(new Dimension(185, 30));
            lbChiffrageDeStock.setMaximumSize(new Dimension(185, 30));
            lbChiffrageDeStock.setName("lbChiffrageDeStock");
            pnlCritereDeSelection.add(lbChiffrageDeStock, new GridBagConstraints(0, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- DATCHF ----
            DATCHF.setPreferredSize(new Dimension(110, 30));
            DATCHF.setMinimumSize(new Dimension(110, 30));
            DATCHF.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATCHF.setName("DATCHF");
            pnlCritereDeSelection.add(DATCHF, new GridBagConstraints(1, 18, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptions ========
          {
            pnlOptions.setTitre("Options d'\u00e9dition");
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- REPON1 ----
            REPON1.setText("Lignes d\u00e9tail articles");
            REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
            REPON1.setMinimumSize(new Dimension(151, 30));
            REPON1.setPreferredSize(new Dimension(151, 30));
            REPON1.setName("REPON1");
            pnlOptions.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EDTRES ----
            EDTRES.setText("Quantit\u00e9 r\u00e9serv\u00e9e");
            EDTRES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTRES.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTRES.setPreferredSize(new Dimension(136, 30));
            EDTRES.setMinimumSize(new Dimension(136, 30));
            EDTRES.setName("EDTRES");
            pnlOptions.add(EDTRES, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EDTSIM ----
            EDTSIM.setText("Edition simplifi\u00e9e");
            EDTSIM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTSIM.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTSIM.setPreferredSize(new Dimension(128, 30));
            EDTSIM.setMinimumSize(new Dimension(128, 30));
            EDTSIM.setName("EDTSIM");
            pnlOptions.add(EDTSIM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EDTPDS ----
            EDTPDS.setText("Totalisation du poids");
            EDTPDS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTPDS.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTPDS.setMinimumSize(new Dimension(153, 30));
            EDTPDS.setPreferredSize(new Dimension(153, 30));
            EDTPDS.setName("EDTPDS");
            pnlOptions.add(EDTPDS, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EDTDES ----
            EDTDES.setText("Edition articles d\u00e9sactiv\u00e9s");
            EDTDES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTDES.setFont(new Font("sansserif", Font.PLAIN, 14));
            EDTDES.setPreferredSize(new Dimension(185, 30));
            EDTDES.setMinimumSize(new Dimension(185, 30));
            EDTDES.setName("EDTDES");
            pnlOptions.add(EDTDES, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RAZNEG ----
            RAZNEG.setText("Remise \u00e0 z\u00e9ro des n\u00e9gatifs");
            RAZNEG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RAZNEG.setFont(new Font("sansserif", Font.PLAIN, 14));
            RAZNEG.setMinimumSize(new Dimension(195, 30));
            RAZNEG.setPreferredSize(new Dimension(195, 30));
            RAZNEG.setName("RAZNEG");
            pnlOptions.add(RAZNEG, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- GENVSK ----
            GENVSK.setText("Fichier stock valoris\u00e9");
            GENVSK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GENVSK.setFont(new Font("sansserif", Font.PLAIN, 14));
            GENVSK.setMinimumSize(new Dimension(155, 30));
            GENVSK.setPreferredSize(new Dimension(155, 30));
            GENVSK.setName("GENVSK");
            pnlOptions.add(GENVSK, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlPrix ========
          {
            pnlPrix.setOpaque(false);
            pnlPrix.setTitre("Type de prix");
            pnlPrix.setName("pnlPrix");
            pnlPrix.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrix.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrix.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlPrix.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPrix.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- CPRI1 ----
            CPRI1.setText("Prix de revient");
            CPRI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI1.setFont(new Font("sansserif", Font.PLAIN, 14));
            CPRI1.setPreferredSize(new Dimension(113, 30));
            CPRI1.setMinimumSize(new Dimension(113, 30));
            CPRI1.setName("CPRI1");
            pnlPrix.add(CPRI1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CPRI2 ----
            CPRI2.setText("Prix derni\u00e8re entr\u00e9e");
            CPRI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI2.setFont(new Font("sansserif", Font.PLAIN, 14));
            CPRI2.setPreferredSize(new Dimension(150, 30));
            CPRI2.setMinimumSize(new Dimension(150, 30));
            CPRI2.setName("CPRI2");
            pnlPrix.add(CPRI2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CPRI3 ----
            CPRI3.setText("Prix d'inventaire");
            CPRI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI3.setFont(new Font("sansserif", Font.PLAIN, 14));
            CPRI3.setMinimumSize(new Dimension(123, 30));
            CPRI3.setPreferredSize(new Dimension(123, 30));
            CPRI3.setName("CPRI3");
            pnlPrix.add(CPRI3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CPRI4 ----
            CPRI4.setText("P.U.M.P");
            CPRI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI4.setFont(new Font("sansserif", Font.PLAIN, 14));
            CPRI4.setPreferredSize(new Dimension(74, 30));
            CPRI4.setMinimumSize(new Dimension(74, 30));
            CPRI4.setName("CPRI4");
            pnlPrix.add(CPRI4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CPRI5 ----
            CPRI5.setText("Prix F.I.F.O");
            CPRI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CPRI5.setFont(new Font("sansserif", Font.PLAIN, 14));
            CPRI5.setMinimumSize(new Dimension(95, 30));
            CPRI5.setPreferredSize(new Dimension(95, 30));
            CPRI5.setName("CPRI5");
            pnlPrix.add(CPRI5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlPrix, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- RB_GRP ----
    RB_GRP.add(CPRI1);
    RB_GRP.add(CPRI2);
    RB_GRP.add(CPRI3);
    RB_GRP.add(CPRI4);
    RB_GRP.add(CPRI5);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNLabelChamp lbCleArticleDebut;
  private XRiTextField DEBIAK;
  private SNLabelChamp lbCleArticleFin;
  private XRiTextField FINIAK;
  private SNLabelChamp lbFamilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFamilleFin;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSousFamilleDebut;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSousFamilleFin;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbFournisseurDebut;
  private SNFournisseur snFournisseurDebut;
  private SNLabelChamp lbFournisseurFin;
  private SNFournisseur snFournisseurFin;
  private SNLabelChamp lbArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticleFin;
  private SNArticle snArticleFin;
  private SNPanel pnlZonePersonnalise;
  private SNLabelChamp lbZonePersonnalise1;
  private XRiTextField ZP1;
  private SNLabelChamp lbZonePersonnalise4;
  private XRiTextField ZP4;
  private SNLabelChamp lbZonePersonnalise2;
  private XRiTextField ZP2;
  private SNLabelChamp lbZonePersonnalise5;
  private XRiTextField ZP5;
  private SNLabelChamp lbZonePersonnalise3;
  private XRiTextField ZP3;
  private SNLabelChamp lbCodeABC;
  private SNPanel pnlABCD;
  private XRiTextField WABC;
  private XRiCheckBox NWABC;
  private SNLabelChamp lbChiffrageDeStock;
  private XRiCalendrier DATCHF;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox REPON1;
  private XRiCheckBox EDTRES;
  private XRiCheckBox EDTSIM;
  private XRiCheckBox EDTPDS;
  private XRiCheckBox EDTDES;
  private XRiCheckBox RAZNEG;
  private XRiCheckBox GENVSK;
  private SNPanelTitre pnlPrix;
  private XRiRadioButton CPRI1;
  private XRiRadioButton CPRI2;
  private XRiRadioButton CPRI3;
  private XRiRadioButton CPRI4;
  private XRiRadioButton CPRI5;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup RB_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
