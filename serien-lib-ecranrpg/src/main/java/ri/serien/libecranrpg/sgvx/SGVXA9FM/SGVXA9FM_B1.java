
package ri.serien.libecranrpg.sgvx.SGVXA9FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVXA9FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public SGVXA9FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    REPON2.setValeursSelection("O", "N");
    REPON1.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    lbWlib1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    // Disponibilité des composants
    REPON2.setEnabled(REPON1.isSelected());
    if (!REPON1.isSelected()) {
      REPON2.setSelected(false);
    }
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charge l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Charge les composants
    chargerListeComposantsMagasin();
    /**
     * Charge les article
     */
    
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "ARTDEB");
    
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "ARTFIN");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    
    
    lbCodeEquipe.setVisible(lexique.isPresent("EQUIP"));
    lbAWLIB1.setVisible(lexique.isPresent("AD1DEB"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Gestion selection complete
    if (snMagasin1.getIdSelection() == null && snMagasin2.getIdSelection() == null && snMagasin3.getIdSelection() == null
        && snMagasin4.getIdSelection() == null && snMagasin5.getIdSelection() == null && snMagasin6.getIdSelection() == null) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
      snMagasin1.renseignerChampRPG(lexique, "MA01");
      snMagasin2.renseignerChampRPG(lexique, "MA02");
      snMagasin3.renseignerChampRPG(lexique, "MA03");
      snMagasin4.renseignerChampRPG(lexique, "MA04");
      snMagasin5.renseignerChampRPG(lexique, "MA05");
      snMagasin6.renseignerChampRPG(lexique, "MA06");
    }
    
    snArticleDebut.renseignerChampRPG(lexique, "ARTDEB");
    snArticleFin.renseignerChampRPG(lexique, "ARTFIN");
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.WatchHelp(pmBTD.getInvoker().getName());
    
    lexique.HostScreenSendKey(this, "F4");
    
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeComposantsMagasin();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * snMagasin1 : "Tous" autorisé, "Aucun" interdit
   * snMagasin2-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA02", "MA03", "MA04", "MA05", "MA06");
    
    // On vérifie que le nombre de magasins n'est pas supérieur au nombre de composant magasin présent.
    // On charge un nombre de composants égal au nombre de choix possibles ou au nombre maximal de composants disponibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    for (int i = 0; i < listeComposant.size(); i++) {
      listeComposant.get(i).setVisible(false);
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On rend le composant snMagasin2 visibles si le composant snMagasin1 contient autre chose que "Tous".
      if (i == 0) {
        listeComposant.get(i).setVisible(!(snMagasin1.getIdSelection() == null));
      }
      // On rend les composants suivants visibles si le composant précédent contient autre chose que "Aucun" (null)
      else {
        listeComposant.get(i).setVisible(!(listeComposant.get(i - 1).getIdSelection() == null));
      }
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé.
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
    
  }
  
  private void REPON1ActionPerformed(ActionEvent e) {
    REPON2.setEnabled(REPON1.isSelected());
    if (!REPON1.isSelected()) {
      REPON2.setSelected(false);
    }
  }
  
  // On observe le changement de valeur du composant snMagasin1.
  // On affiche ou non le composants snMagasin2 si snMagasin1 différent de "Aucun" (null).
  // On affiche le composant s'il est censé pouvoir être affiché.
  private void snMagasin1ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 2 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 2) {
      snMagasin2.setVisible(!(snMagasin1.getIdSelection() == null));
      lbMagasin2.setVisible(snMagasin2.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin1.getIdSelection() == null) {
      snMagasin2.setSelection(null);
    }
  }
  
  // On effectue la même observation pour chaque composant snMagasin.
  // On affiche le composant suivant selon les même conditions.
  private void snMagasin2ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 3 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 3) {
      snMagasin3.setVisible(!(snMagasin2.getIdSelection() == null));
      lbMagasin3.setVisible(snMagasin3.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin2.getIdSelection() == null) {
      snMagasin3.setSelection(null);
    }
  }
  
  private void snMagasin3ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 4 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 4) {
      snMagasin4.setVisible(!(snMagasin3.getIdSelection() == null));
      lbMagasin4.setVisible(snMagasin4.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin3.getIdSelection() == null) {
      snMagasin4.setSelection(null);
    }
  }
  
  private void snMagasin4ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 5 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 5) {
      snMagasin5.setVisible(!(snMagasin4.getIdSelection() == null));
      lbMagasin5.setVisible(snMagasin5.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin4.getIdSelection() == null) {
      snMagasin5.setSelection(null);
    }
  }
  
  private void snMagasin5ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 6 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 6) {
      snMagasin6.setVisible(!(snMagasin5.getIdSelection() == null));
      lbMagasin6.setVisible(snMagasin6.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin5.getIdSelection() == null) {
      snMagasin6.setSelection(null);
    }
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCriteDeSelection = new SNPanelTitre();
    lbDateInventaire = new SNLabelChamp();
    DATINV = new XRiCalendrier();
    lbCodeEquipe = new SNLabelChamp();
    EQUIP = new XRiTextField();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    lbArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticlefin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    lbWlib1 = new SNLabelChamp();
    pnlAdresseNiveau1 = new SNPanel();
    AD1DEB = new XRiTextField();
    lbAWLIB1 = new SNLabelChamp();
    AD1FIN = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfDateEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    REPON1 = new XRiCheckBox();
    REPON2 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== pnlCriteDeSelection ========
        {
          pnlCriteDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCriteDeSelection.setName("pnlCriteDeSelection");
          pnlCriteDeSelection.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCriteDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCriteDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCriteDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCriteDeSelection.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbDateInventaire ----
          lbDateInventaire.setText("Date d'inventaire");
          lbDateInventaire.setName("lbDateInventaire");
          pnlCriteDeSelection.add(lbDateInventaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- DATINV ----
          DATINV.setPreferredSize(new Dimension(120, 30));
          DATINV.setMinimumSize(new Dimension(120, 30));
          DATINV.setFont(new Font("sansserif", Font.PLAIN, 14));
          DATINV.setName("DATINV");
          pnlCriteDeSelection.add(DATINV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCodeEquipe ----
          lbCodeEquipe.setText("Code \u00e9quipe");
          lbCodeEquipe.setName("lbCodeEquipe");
          pnlCriteDeSelection.add(lbCodeEquipe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- EQUIP ----
          EQUIP.setComponentPopupMenu(pmBTD);
          EQUIP.setPreferredSize(new Dimension(44, 30));
          EQUIP.setMinimumSize(new Dimension(44, 30));
          EQUIP.setName("EQUIP");
          pnlCriteDeSelection.add(EQUIP, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin1 ----
          lbMagasin1.setText("Magasin 1");
          lbMagasin1.setName("lbMagasin1");
          pnlCriteDeSelection.add(lbMagasin1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin1 ----
          snMagasin1.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin1.setName("snMagasin1");
          snMagasin1.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snMagasin1ValueChanged(e);
            }
          });
          pnlCriteDeSelection.add(snMagasin1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin2 ----
          lbMagasin2.setText("Magasin 2");
          lbMagasin2.setName("lbMagasin2");
          pnlCriteDeSelection.add(lbMagasin2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin2 ----
          snMagasin2.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin2.setName("snMagasin2");
          snMagasin2.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snMagasin2ValueChanged(e);
            }
          });
          pnlCriteDeSelection.add(snMagasin2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin3 ----
          lbMagasin3.setText("Magasin 3");
          lbMagasin3.setName("lbMagasin3");
          pnlCriteDeSelection.add(lbMagasin3, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin3 ----
          snMagasin3.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin3.setName("snMagasin3");
          snMagasin3.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snMagasin3ValueChanged(e);
            }
          });
          pnlCriteDeSelection.add(snMagasin3, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin4 ----
          lbMagasin4.setText("Magasin 4");
          lbMagasin4.setName("lbMagasin4");
          pnlCriteDeSelection.add(lbMagasin4, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin4 ----
          snMagasin4.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin4.setName("snMagasin4");
          snMagasin4.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snMagasin4ValueChanged(e);
            }
          });
          pnlCriteDeSelection.add(snMagasin4, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin5 ----
          lbMagasin5.setText("Magasin 5");
          lbMagasin5.setName("lbMagasin5");
          pnlCriteDeSelection.add(lbMagasin5, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin5 ----
          snMagasin5.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin5.setName("snMagasin5");
          snMagasin5.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snMagasin5ValueChanged(e);
            }
          });
          pnlCriteDeSelection.add(snMagasin5, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin6 ----
          lbMagasin6.setText("Magasin 6");
          lbMagasin6.setName("lbMagasin6");
          pnlCriteDeSelection.add(lbMagasin6, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin6 ----
          snMagasin6.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin6.setName("snMagasin6");
          pnlCriteDeSelection.add(snMagasin6, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticleDebut ----
          lbArticleDebut.setText("Article d\u00e9but");
          lbArticleDebut.setName("lbArticleDebut");
          pnlCriteDeSelection.add(lbArticleDebut, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleDebut ----
          snArticleDebut.setName("snArticleDebut");
          pnlCriteDeSelection.add(snArticleDebut, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticlefin ----
          lbArticlefin.setText("Article fin");
          lbArticlefin.setName("lbArticlefin");
          pnlCriteDeSelection.add(lbArticlefin, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleFin ----
          snArticleFin.setName("snArticleFin");
          pnlCriteDeSelection.add(snArticleFin, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbWlib1 ----
          lbWlib1.setText("@LSL1@");
          lbWlib1.setName("lbWlib1");
          pnlCriteDeSelection.add(lbWlib1, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlAdresseNiveau1 ========
          {
            pnlAdresseNiveau1.setName("pnlAdresseNiveau1");
            pnlAdresseNiveau1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlAdresseNiveau1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlAdresseNiveau1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlAdresseNiveau1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlAdresseNiveau1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- AD1DEB ----
            AD1DEB.setComponentPopupMenu(null);
            AD1DEB.setPreferredSize(new Dimension(44, 30));
            AD1DEB.setMinimumSize(new Dimension(44, 30));
            AD1DEB.setMaximumSize(new Dimension(44, 30));
            AD1DEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            AD1DEB.setName("AD1DEB");
            pnlAdresseNiveau1.add(AD1DEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbAWLIB1 ----
            lbAWLIB1.setText("/");
            lbAWLIB1.setHorizontalAlignment(SwingConstants.CENTER);
            lbAWLIB1.setHorizontalTextPosition(SwingConstants.CENTER);
            lbAWLIB1.setPreferredSize(new Dimension(20, 30));
            lbAWLIB1.setMinimumSize(new Dimension(20, 30));
            lbAWLIB1.setMaximumSize(new Dimension(20, 30));
            lbAWLIB1.setName("lbAWLIB1");
            pnlAdresseNiveau1.add(lbAWLIB1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- AD1FIN ----
            AD1FIN.setComponentPopupMenu(null);
            AD1FIN.setPreferredSize(new Dimension(44, 30));
            AD1FIN.setMinimumSize(new Dimension(44, 30));
            AD1FIN.setMaximumSize(new Dimension(44, 30));
            AD1FIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            AD1FIN.setName("AD1FIN");
            pnlAdresseNiveau1.add(AD1FIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCriteDeSelection.add(pnlAdresseNiveau1, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCriteDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissementSelectionne ========
        {
          pnlEtablissementSelectionne.setTitre("Etablissement");
          pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
          pnlEtablissementSelectionne.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissementSelectionne.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfDateEnCours ----
          tfDateEnCours.setText("@WENCX@");
          tfDateEnCours.setPreferredSize(new Dimension(260, 30));
          tfDateEnCours.setMinimumSize(new Dimension(260, 30));
          tfDateEnCours.setEditable(false);
          tfDateEnCours.setEnabled(false);
          tfDateEnCours.setName("tfDateEnCours");
          pnlEtablissementSelectionne.add(tfDateEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOptionEdition ========
        {
          pnlOptionEdition.setTitre("Options d'\u00e9dition");
          pnlOptionEdition.setName("pnlOptionEdition");
          pnlOptionEdition.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- REPON1 ----
          REPON1.setText("Edition des bordereaux g\u00e9n\u00e9r\u00e9s");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setFont(new Font("sansserif", Font.PLAIN, 14));
          REPON1.setPreferredSize(new Dimension(225, 30));
          REPON1.setMinimumSize(new Dimension(225, 30));
          REPON1.setName("REPON1");
          REPON1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              REPON1ActionPerformed(e);
            }
          });
          pnlOptionEdition.add(REPON1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- REPON2 ----
          REPON2.setText("avec d\u00e9tail des lignes articles");
          REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON2.setFont(new Font("sansserif", Font.PLAIN, 14));
          REPON2.setMinimumSize(new Dimension(207, 30));
          REPON2.setPreferredSize(new Dimension(207, 30));
          REPON2.setName("REPON2");
          pnlOptionEdition.add(REPON2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCriteDeSelection;
  private SNLabelChamp lbDateInventaire;
  private XRiCalendrier DATINV;
  private SNLabelChamp lbCodeEquipe;
  private XRiTextField EQUIP;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNLabelChamp lbArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticlefin;
  private SNArticle snArticleFin;
  private SNLabelChamp lbWlib1;
  private SNPanel pnlAdresseNiveau1;
  private XRiTextField AD1DEB;
  private SNLabelChamp lbAWLIB1;
  private XRiTextField AD1FIN;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfDateEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox REPON1;
  private XRiCheckBox REPON2;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
