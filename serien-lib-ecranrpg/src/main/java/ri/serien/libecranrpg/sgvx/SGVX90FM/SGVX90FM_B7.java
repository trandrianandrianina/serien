
package ri.serien.libecranrpg.sgvx.SGVX90FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVX90FM_B7 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVX90FM_B7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Titre de la fenêtre
    setTitle("Heure de départ de traitement");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlHeureTraitement = new SNPanelTitre();
    lbHeureTraitement = new JLabel();
    UHHMN = new XRiTextField();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(460, 170));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setMinimumSize(new Dimension(208, 350));
      pnlContenu.setPreferredSize(new Dimension(208, 1000));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlHeureTraitement ========
      {
        pnlHeureTraitement.setTitre("S\u00e9lection de l'heure de traitement");
        pnlHeureTraitement.setMinimumSize(new Dimension(188, 175));
        pnlHeureTraitement.setPreferredSize(new Dimension(188, 175));
        pnlHeureTraitement.setName("pnlHeureTraitement");
        pnlHeureTraitement.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlHeureTraitement.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlHeureTraitement.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlHeureTraitement.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlHeureTraitement.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbHeureTraitement ----
        lbHeureTraitement.setText("Heure de d\u00e9part traitement");
        lbHeureTraitement.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbHeureTraitement.setName("lbHeureTraitement");
        pnlHeureTraitement.add(lbHeureTraitement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- UHHMN ----
        UHHMN.setPreferredSize(new Dimension(60, 28));
        UHHMN.setMinimumSize(new Dimension(60, 28));
        UHHMN.setFont(new Font("sansserif", Font.PLAIN, 14));
        UHHMN.setName("UHHMN");
        pnlHeureTraitement.add(UHHMN, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlHeureTraitement);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlHeureTraitement;
  private JLabel lbHeureTraitement;
  private XRiTextField UHHMN;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
