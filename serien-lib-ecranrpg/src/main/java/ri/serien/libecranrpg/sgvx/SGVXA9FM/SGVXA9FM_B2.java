
package ri.serien.libecranrpg.sgvx.SGVXA9FM;
// Nom Fichier: pop_SGVXA9FM_FMTB2_FMTF1_454.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SGVXA9FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVXA9FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    OPT3.setValeurs("3", "RB");
    OPT2.setValeurs("2", "RB");
    OPT1.setValeurs("1", "RB");
    
    // Bouton par défaut
    setDefaultButton(OBJ_9);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // OPT3.setVisible( lexique.isPresent("RB"));
    // OPT3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // OPT2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // OPT1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    OBJ_9.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Option de reprise"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (OPT3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (OPT2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (OPT1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_4 = new JPanel();
    OBJ_5 = new JLabel();
    OPT1 = new XRiRadioButton();
    OPT2 = new XRiRadioButton();
    OPT3 = new XRiRadioButton();
    P_PnlOpts = new JPanel();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();

    //======== this ========
    setName("this");

    //======== OBJ_4 ========
    {
      OBJ_4.setBorder(new TitledBorder(new EtchedBorder(), "", TitledBorder.LEADING, TitledBorder.TOP));
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Au moins un bordereau d\u00e9j\u00e0 trait\u00e9");
      OBJ_5.setName("OBJ_5");

      //---- OPT1 ----
      OPT1.setText("Saut");
      OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OPT1.setName("OPT1");

      //---- OPT2 ----
      OPT2.setText("For\u00e7age");
      OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OPT2.setName("OPT2");

      //---- OPT3 ----
      OPT3.setText("Purge");
      OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OPT3.setName("OPT3");

      GroupLayout OBJ_4Layout = new GroupLayout(OBJ_4);
      OBJ_4.setLayout(OBJ_4Layout);
      OBJ_4Layout.setHorizontalGroup(
        OBJ_4Layout.createParallelGroup()
          .addGroup(OBJ_4Layout.createSequentialGroup()
            .addGap(25, 25, 25)
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE))
          .addGroup(OBJ_4Layout.createSequentialGroup()
            .addGap(112, 112, 112)
            .addComponent(OPT1, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
          .addGroup(OBJ_4Layout.createSequentialGroup()
            .addGap(112, 112, 112)
            .addComponent(OPT2, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
          .addGroup(OBJ_4Layout.createSequentialGroup()
            .addGap(112, 112, 112)
            .addComponent(OPT3, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
      );
      OBJ_4Layout.setVerticalGroup(
        OBJ_4Layout.createParallelGroup()
          .addGroup(OBJ_4Layout.createSequentialGroup()
            .addGap(13, 13, 13)
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addComponent(OPT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(9, 9, 9)
            .addComponent(OPT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(8, 8, 8)
            .addComponent(OPT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
      );
    }

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      //---- OBJ_9 ----
      OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_9);
      OBJ_9.setBounds(5, 5, 56, 40);

      //---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_10);
      OBJ_10.setBounds(60, 5, 56, 40);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(11, 11, 11)
          .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(175, 175, 175)
          .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(12, 12, 12)
          .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
          .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
    );

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(OPT1);
    RB_GRP.add(OPT2);
    RB_GRP.add(OPT3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel OBJ_4;
  private JLabel OBJ_5;
  private XRiRadioButton OPT1;
  private XRiRadioButton OPT2;
  private XRiRadioButton OPT3;
  private JPanel P_PnlOpts;
  private JButton OBJ_9;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
