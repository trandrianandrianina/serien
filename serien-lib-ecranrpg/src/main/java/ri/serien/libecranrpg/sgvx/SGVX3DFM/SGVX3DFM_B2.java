
package ri.serien.libecranrpg.sgvx.SGVX3DFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SGVX3DFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public SGVX3DFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    WECART.setValeursSelection("1", " ");
    OPT2.setValeursSelection("1", " ");
    OPT1.setValeursSelection("1", " ");
    WTVTE.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTVTEActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!WTVTE.isSelected());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!WTOU.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_54 = new JXTitledSeparator();
    OBJ_40 = new JXTitledSeparator();
    OBJ_72 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_43 = new JLabel();
    OBJ_46 = new JLabel();
    NUMDEB = new XRiTextField();
    NUMFIN = new XRiTextField();
    SUFDEB = new XRiTextField();
    SUFFIN = new XRiTextField();
    WTOU = new XRiCheckBox();
    OPT1 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OBJ_73 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    OBJ_75 = new JLabel();
    OBJ_74 = new JLabel();
    WMAG = new XRiTextField();
    WTVTE = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_57 = new JLabel();
    OBJ_60 = new JLabel();
    VTEDEB = new XRiTextField();
    VTEFIN = new XRiTextField();
    VTEDES = new XRiTextField();
    VTEFIS = new XRiTextField();
    WECART = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(680, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel2.add(sep_etablissement);
            sep_etablissement.setBounds(5, 5, 625, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel2.add(z_dgnom_);
            z_dgnom_.setBounds(175, 30, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel2.add(z_wencx_);
            z_wencx_.setBounds(175, 60, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel2.add(z_etablissement_);
            z_etablissement_.setBounds(20, 50, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel2.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(65, 45), bouton_etablissement.getPreferredSize()));

            //---- OBJ_54 ----
            OBJ_54.setTitle("Commande de vente");
            OBJ_54.setName("OBJ_54");
            panel2.add(OBJ_54);
            OBJ_54.setBounds(5, 315, 625, OBJ_54.getPreferredSize().height);

            //---- OBJ_40 ----
            OBJ_40.setTitle("Commande d'achat");
            OBJ_40.setName("OBJ_40");
            panel2.add(OBJ_40);
            OBJ_40.setBounds(5, 100, 625, OBJ_40.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setTitle("");
            OBJ_72.setName("OBJ_72");
            panel2.add(OBJ_72);
            OBJ_72.setBounds(5, 405, 625, OBJ_72.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_43 ----
              OBJ_43.setText("Num\u00e9ro de d\u00e9but");
              OBJ_43.setName("OBJ_43");
              P_SEL0.add(OBJ_43);
              OBJ_43.setBounds(10, 13, 110, 20);

              //---- OBJ_46 ----
              OBJ_46.setText("Num\u00e9ro de fin");
              OBJ_46.setName("OBJ_46");
              P_SEL0.add(OBJ_46);
              OBJ_46.setBounds(220, 13, 110, 20);

              //---- NUMDEB ----
              NUMDEB.setComponentPopupMenu(BTD);
              NUMDEB.setName("NUMDEB");
              P_SEL0.add(NUMDEB);
              NUMDEB.setBounds(124, 9, 58, NUMDEB.getPreferredSize().height);

              //---- NUMFIN ----
              NUMFIN.setComponentPopupMenu(BTD);
              NUMFIN.setName("NUMFIN");
              P_SEL0.add(NUMFIN);
              NUMFIN.setBounds(330, 9, 58, NUMFIN.getPreferredSize().height);

              //---- SUFDEB ----
              SUFDEB.setComponentPopupMenu(BTD);
              SUFDEB.setName("SUFDEB");
              P_SEL0.add(SUFDEB);
              SUFDEB.setBounds(185, 9, 20, SUFDEB.getPreferredSize().height);

              //---- SUFFIN ----
              SUFFIN.setComponentPopupMenu(BTD);
              SUFFIN.setName("SUFFIN");
              P_SEL0.add(SUFFIN);
              SUFFIN.setBounds(390, 9, 20, SUFFIN.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL0.setMinimumSize(preferredSize);
                P_SEL0.setPreferredSize(preferredSize);
              }
            }
            panel2.add(P_SEL0);
            P_SEL0.setBounds(180, 120, 420, 45);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel2.add(WTOU);
            WTOU.setBounds(20, 135, 141, WTOU.getPreferredSize().height);

            //---- OPT1 ----
            OPT1.setText("Commandes homologu\u00e9es");
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel2.add(OPT1);
            OPT1.setBounds(20, 210, 188, 20);

            //---- OPT2 ----
            OPT2.setText("Commandes r\u00e9ceptionn\u00e9es");
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel2.add(OPT2);
            OPT2.setBounds(20, 240, 191, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("P\u00e9riode du");
            OBJ_73.setName("OBJ_73");
            panel2.add(OBJ_73);
            OBJ_73.setBounds(20, 180, 150, 20);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel2.add(DATDEB);
            DATDEB.setBounds(180, 176, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setName("DATFIN");
            panel2.add(DATFIN);
            DATFIN.setBounds(340, 176, 105, DATFIN.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("au");
            OBJ_75.setName("OBJ_75");
            panel2.add(OBJ_75);
            OBJ_75.setBounds(305, 180, 18, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("Magasin");
            OBJ_74.setName("OBJ_74");
            panel2.add(OBJ_74);
            OBJ_74.setBounds(20, 270, 150, 20);

            //---- WMAG ----
            WMAG.setName("WMAG");
            panel2.add(WMAG);
            WMAG.setBounds(180, 266, 34, WMAG.getPreferredSize().height);

            //---- WTVTE ----
            WTVTE.setText("S\u00e9lection compl\u00e8te");
            WTVTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTVTE.setName("WTVTE");
            WTVTE.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTVTEActionPerformed(e);
              }
            });
            panel2.add(WTVTE);
            WTVTE.setBounds(20, 350, 141, WTVTE.getPreferredSize().height);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_57 ----
              OBJ_57.setText("Num\u00e9ro de d\u00e9but");
              OBJ_57.setName("OBJ_57");
              P_SEL1.add(OBJ_57);
              OBJ_57.setBounds(15, 11, 110, 20);

              //---- OBJ_60 ----
              OBJ_60.setText("Num\u00e9ro de fin");
              OBJ_60.setName("OBJ_60");
              P_SEL1.add(OBJ_60);
              OBJ_60.setBounds(235, 11, 94, 20);

              //---- VTEDEB ----
              VTEDEB.setComponentPopupMenu(BTD);
              VTEDEB.setName("VTEDEB");
              P_SEL1.add(VTEDEB);
              VTEDEB.setBounds(124, 7, 58, VTEDEB.getPreferredSize().height);

              //---- VTEFIN ----
              VTEFIN.setComponentPopupMenu(BTD);
              VTEFIN.setName("VTEFIN");
              P_SEL1.add(VTEFIN);
              VTEFIN.setBounds(330, 7, 58, VTEFIN.getPreferredSize().height);

              //---- VTEDES ----
              VTEDES.setComponentPopupMenu(BTD);
              VTEDES.setName("VTEDES");
              P_SEL1.add(VTEDES);
              VTEDES.setBounds(185, 7, 20, VTEDES.getPreferredSize().height);

              //---- VTEFIS ----
              VTEFIS.setComponentPopupMenu(BTD);
              VTEFIS.setName("VTEFIS");
              P_SEL1.add(VTEFIS);
              VTEFIS.setBounds(390, 7, 20, VTEFIS.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL1.setMinimumSize(preferredSize);
                P_SEL1.setPreferredSize(preferredSize);
              }
            }
            panel2.add(P_SEL1);
            P_SEL1.setBounds(180, 335, 420, 42);

            //---- WECART ----
            WECART.setText("Ecarts d'exp\u00e9dition/r\u00e9ception seuls");
            WECART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WECART.setName("WECART");
            panel2.add(WECART);
            WECART.setBounds(20, 420, 295, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 678, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_54;
  private JXTitledSeparator OBJ_40;
  private JXTitledSeparator OBJ_72;
  private JPanel P_SEL0;
  private JLabel OBJ_43;
  private JLabel OBJ_46;
  private XRiTextField NUMDEB;
  private XRiTextField NUMFIN;
  private XRiTextField SUFDEB;
  private XRiTextField SUFFIN;
  private XRiCheckBox WTOU;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT2;
  private JLabel OBJ_73;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JLabel OBJ_75;
  private JLabel OBJ_74;
  private XRiTextField WMAG;
  private XRiCheckBox WTVTE;
  private JPanel P_SEL1;
  private JLabel OBJ_57;
  private JLabel OBJ_60;
  private XRiTextField VTEDEB;
  private XRiTextField VTEFIN;
  private XRiTextField VTEDES;
  private XRiTextField VTEFIS;
  private XRiCheckBox WECART;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
