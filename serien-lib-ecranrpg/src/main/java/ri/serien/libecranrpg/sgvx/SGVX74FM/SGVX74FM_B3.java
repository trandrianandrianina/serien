
package ri.serien.libecranrpg.sgvx.SGVX74FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGVX74FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX74FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WSTK.setValeursSelection("X", " ");
    WLB4.setValeursSelection("X", " ");
    WLB3.setValeursSelection("X", " ");
    WLB2.setValeursSelection("X", " ");
    WLB1.setValeursSelection("X", " ");
    WPRV.setValeursSelection("X", " ");
    WPRA.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // WLB4.setVisible( lexique.isPresent("WLB4"));
    // WLB4.setEnabled( lexique.isPresent("WLB4"));
    // WLB4.setSelected(lexique.HostFieldGetData("WLB4").equalsIgnoreCase("X"));
    // WLB3.setVisible( lexique.isPresent("WLB3"));
    // WLB3.setEnabled( lexique.isPresent("WLB3"));
    // WLB3.setSelected(lexique.HostFieldGetData("WLB3").equalsIgnoreCase("X"));
    // WLB2.setVisible( lexique.isPresent("WLB2"));
    // WLB2.setEnabled( lexique.isPresent("WLB2"));
    // WLB2.setSelected(lexique.HostFieldGetData("WLB2").equalsIgnoreCase("X"));
    // WLB1.setVisible( lexique.isPresent("WLB1"));
    // WLB1.setEnabled( lexique.isPresent("WLB1"));
    // WLB1.setSelected(lexique.HostFieldGetData("WLB1").equalsIgnoreCase("X"));
    WMIN.setVisible(lexique.isPresent("WMIN"));
    // WPRV.setVisible( lexique.isPresent("WPRV"));
    // WPRV.setSelected(lexique.HostFieldGetData("WPRV").equalsIgnoreCase("X"));
    // WSTK.setVisible( lexique.isPresent("WSTK"));
    // WSTK.setSelected(lexique.HostFieldGetData("WSTK").equalsIgnoreCase("X"));
    // WPRA.setVisible( lexique.isPresent("WPRA"));
    // WPRA.setSelected(lexique.HostFieldGetData("WPRA").equalsIgnoreCase("X"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WLB4.isSelected())
    // lexique.HostFieldPutData("WLB4", 0, "X");
    // else
    // lexique.HostFieldPutData("WLB4", 0, " ");
    // if (WLB3.isSelected())
    // lexique.HostFieldPutData("WLB3", 0, "X");
    // else
    // lexique.HostFieldPutData("WLB3", 0, " ");
    // if (WLB2.isSelected())
    // lexique.HostFieldPutData("WLB2", 0, "X");
    // else
    // lexique.HostFieldPutData("WLB2", 0, " ");
    // if (WLB1.isSelected())
    // lexique.HostFieldPutData("WLB1", 0, "X");
    // else
    // lexique.HostFieldPutData("WLB1", 0, " ");
    // if (WPRV.isSelected())
    // lexique.HostFieldPutData("WPRV", 0, "X");
    // else
    // lexique.HostFieldPutData("WPRV", 0, " ");
    // if (WSTK.isSelected())
    // lexique.HostFieldPutData("WSTK", 0, "X");
    // else
    // lexique.HostFieldPutData("WSTK", 0, " ");
    // if (WPRA.isSelected())
    // lexique.HostFieldPutData("WPRA", 0, "X");
    // else
    // lexique.HostFieldPutData("WPRA", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_27 = new JLabel();
    WDATX = new XRiCalendrier();
    OBJ_28 = new JLabel();
    WDEV = new XRiTextField();
    panel2 = new JPanel();
    WPRA = new XRiCheckBox();
    WPRV = new XRiCheckBox();
    OBJ_29 = new JLabel();
    WLB1 = new XRiCheckBox();
    WLB2 = new XRiCheckBox();
    WLB3 = new XRiCheckBox();
    WLB4 = new XRiCheckBox();
    panel3 = new JPanel();
    WSTK = new XRiCheckBox();
    WMIN = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setText("Date tarif");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(10, 19, 75, 20);

          //---- WDATX ----
          WDATX.setName("WDATX");
          panel1.add(WDATX);
          WDATX.setBounds(85, 15, 105, WDATX.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("Devise tarif");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(10, 49, 80, 20);

          //---- WDEV ----
          WDEV.setComponentPopupMenu(BTD);
          WDEV.setName("WDEV");
          panel1.add(WDEV);
          WDEV.setBounds(85, 45, 40, WDEV.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WPRA ----
          WPRA.setText("Prix catalogue CNA");
          WPRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPRA.setName("WPRA");
          panel2.add(WPRA);
          WPRA.setBounds(20, 50, 155, 20);

          //---- WPRV ----
          WPRV.setText("Prix de revient");
          WPRV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPRV.setName("WPRV");
          panel2.add(WPRV);
          WPRV.setBounds(185, 50, 130, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Libell\u00e9s");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(20, 20, 110, 20);

          //---- WLB1 ----
          WLB1.setText("1");
          WLB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WLB1.setName("WLB1");
          panel2.add(WLB1);
          WLB1.setBounds(135, 20, 45, 20);

          //---- WLB2 ----
          WLB2.setText("2");
          WLB2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WLB2.setName("WLB2");
          panel2.add(WLB2);
          WLB2.setBounds(180, 20, 45, 20);

          //---- WLB3 ----
          WLB3.setText("3");
          WLB3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WLB3.setName("WLB3");
          panel2.add(WLB3);
          WLB3.setBounds(225, 20, 45, 20);

          //---- WLB4 ----
          WLB4.setText("4");
          WLB4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WLB4.setName("WLB4");
          panel2.add(WLB4);
          WLB4.setBounds(270, 20, 45, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(""));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- WSTK ----
          WSTK.setText("Stock minimum");
          WSTK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSTK.setName("WSTK");
          panel3.add(WSTK);
          WSTK.setBounds(15, 20, 135, 20);

          //---- WMIN ----
          WMIN.setComponentPopupMenu(BTD);
          WMIN.setName("WMIN");
          panel3.add(WMIN);
          WMIN.setBounds(15, 45, 90, WMIN.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_27;
  private XRiCalendrier WDATX;
  private JLabel OBJ_28;
  private XRiTextField WDEV;
  private JPanel panel2;
  private XRiCheckBox WPRA;
  private XRiCheckBox WPRV;
  private JLabel OBJ_29;
  private XRiCheckBox WLB1;
  private XRiCheckBox WLB2;
  private XRiCheckBox WLB3;
  private XRiCheckBox WLB4;
  private JPanel panel3;
  private XRiCheckBox WSTK;
  private XRiTextField WMIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
