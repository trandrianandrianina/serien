
package ri.serien.libecranrpg.sgvx.SGVX50FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class SGVX50FM_B7 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGVX50FM_B7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    // Ajout
    initDiverses();
    UCH2.setValeursSelection("X", " ");
    UCH1.setValeursSelection("X", " ");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererLesErreurs("19");
    setTitle("Chiffrage écarts sur inventaire");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    UCH1 = new XRiCheckBox();
    UCH2 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(400, 128));
    setPreferredSize(new Dimension(400, 128));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //---- UCH1 ----
      UCH1.setText("Au P.U.M.P");
      UCH1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      UCH1.setFont(new Font("sansserif", Font.PLAIN, 14));
      UCH1.setPreferredSize(new Dimension(95, 30));
      UCH1.setMinimumSize(new Dimension(95, 30));
      UCH1.setName("UCH1");
      pnlContenu.add(UCH1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- UCH2 ----
      UCH2.setText("Au prix de revient");
      UCH2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      UCH2.setFont(new Font("sansserif", Font.PLAIN, 14));
      UCH2.setMinimumSize(new Dimension(133, 30));
      UCH2.setPreferredSize(new Dimension(133, 30));
      UCH2.setName("UCH2");
      pnlContenu.add(UCH2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private XRiCheckBox UCH1;
  private XRiCheckBox UCH2;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
