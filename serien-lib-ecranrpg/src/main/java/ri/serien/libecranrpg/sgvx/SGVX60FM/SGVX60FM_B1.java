
package ri.serien.libecranrpg.sgvx.SGVX60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGVX60FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top= null;
  private String[] _MOI1_Title = { "Mois", "Réelles", "Corrections", };
  private String[][] _MOI1_Data =
      { { "MOI1", "S1X01", "S1C01", }, { "MOI2", "S1X02", "S1C02", }, { "MOI3", "S1X03", "S1C03", }, { "MOI4", "S1X04", "S1C04", },
          { "MOI5", "S1X05", "S1C05", }, { "MOI6", "S1X06", "S1C06", }, { "MOI7", "S1X07", "S1C07", }, { "MOI8", "S1X08", "S1C08", },
          { "MOI9", "S1X09", "S1C09", }, { "MOI10", "S1X10", "S1C10", }, { "MOI11", "S1X11", "S1C11", }, { "MOI12", "S1X12", "S1C12", },
          { "MOI13", "S1X13", "S1C13", }, { "MOI14", "S1X14", "S1C14", }, { "MOI15", "S1X15", "S1C15", }, { "MOI16", "S1X16", "S1C16", },
          { "MOI17", "S1X17", "S1C17", }, { "MOI18", "S1X18", "S1C18", }, { "MOI19", "S1X19", "S1C19", }, { "MOI20", "S1X20", "S1C20", },
          { "MOI21", "S1X21", "S1C21", }, { "MOI22", "S1X22", "S1C22", }, { "MOI23", "S1X23", "S1C23", }, { "MOI24", "S1X24", "S1C24", }, };
  private int[] _MOI1_Width = { 43, 84, 79, };
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  public SGVX60FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    MOI1.setAspectTable(null, _MOI1_Title, _MOI1_Data, _MOI1_Width, true, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top, _LIST_Justification);
    
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Historique des consommations et corrections"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_OBJ_20 = new JScrollPane();
    MOI1 = new XRiTable();
    TOTG = new XRiTextField();
    MOYG = new XRiTextField();
    TOTC = new XRiTextField();
    MOYC = new XRiTextField();
    OBJ_24 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(695, 570));
    setPreferredSize(new Dimension(695, 570));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Historique des consommations"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //======== SCROLLPANE_OBJ_20 ========
          {
            SCROLLPANE_OBJ_20.setComponentPopupMenu(BTD);
            SCROLLPANE_OBJ_20.setName("SCROLLPANE_OBJ_20");

            //---- MOI1 ----
            MOI1.setName("MOI1");
            SCROLLPANE_OBJ_20.setViewportView(MOI1);
          }

          //---- TOTG ----
          TOTG.setComponentPopupMenu(BTD);
          TOTG.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTG.setName("TOTG");

          //---- MOYG ----
          MOYG.setComponentPopupMenu(BTD);
          MOYG.setHorizontalAlignment(SwingConstants.RIGHT);
          MOYG.setName("MOYG");

          //---- TOTC ----
          TOTC.setComponentPopupMenu(BTD);
          TOTC.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTC.setName("TOTC");

          //---- MOYC ----
          MOYC.setComponentPopupMenu(BTD);
          MOYC.setHorizontalAlignment(SwingConstants.RIGHT);
          MOYC.setName("MOYC");

          //---- OBJ_24 ----
          OBJ_24.setText("Total");
          OBJ_24.setFont(OBJ_24.getFont().deriveFont(OBJ_24.getFont().getStyle() | Font.BOLD));
          OBJ_24.setName("OBJ_24");

          //---- OBJ_25 ----
          OBJ_25.setText("Moyenne par jour");
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD));
          OBJ_25.setName("OBJ_25");

          //---- OBJ_26 ----
          OBJ_26.setText("R\u00e9el");
          OBJ_26.setName("OBJ_26");

          //---- OBJ_27 ----
          OBJ_27.setText("Corrig\u00e9");
          OBJ_27.setName("OBJ_27");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(SCROLLPANE_OBJ_20, GroupLayout.PREFERRED_SIZE, 462, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(TOTG, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
                .addGap(85, 85, 85)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
                  .addComponent(TOTC, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(MOYG, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                .addGap(85, 85, 85)
                .addComponent(MOYC, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(SCROLLPANE_OBJ_20, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(OBJ_24))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(TOTG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_26)
                  .addComponent(OBJ_27)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(TOTC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_25))
                  .addComponent(MOYG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MOYC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_OBJ_20;
  private XRiTable MOI1;
  private XRiTextField TOTG;
  private XRiTextField MOYG;
  private XRiTextField TOTC;
  private XRiTextField MOYC;
  private JLabel OBJ_24;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
