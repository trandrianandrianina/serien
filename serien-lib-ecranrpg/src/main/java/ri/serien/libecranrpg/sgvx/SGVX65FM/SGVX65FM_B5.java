
package ri.serien.libecranrpg.sgvx.SGVX65FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVX65FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVX65FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPON2.setValeursSelection("OUI", "NON");
    UCH2.setValeursSelection("X", " ");
    UCH1.setValeursSelection("X", " ");
    WTOUM.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
    REPON1.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_21.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBELLE1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_36.setVisible(lexique.isPresent("WTOUM"));
    // WTOUM.setSelected(lexique.HostFieldGetData("WTOUM").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!(WTOUM.isSelected() || lexique.isTrue("91")));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    P_SEL1.setVisible(!WTOU.isSelected() && lexique.isTrue("94"));
    P_SEL0.setVisible(!WTOU.isSelected() && !lexique.isTrue("94") && !lexique.isTrue("93") && !lexique.isTrue("95"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    UCH1.setVisible(!UCH2.isSelected());
    // UCH1.setSelected(lexique.HostFieldGetData("UCH1").equalsIgnoreCase("X"));
    UCH2.setVisible(!UCH1.isSelected());
    // UCH2.setSelected(lexique.HostFieldGetData("UCH2").equalsIgnoreCase("X"));
    // REPON2.setSelected(lexique.HostFieldGetData("REPON2").equalsIgnoreCase("OUI"));
    
    if (lexique.isTrue("94")) {
      OBJ_21.setTitle("Plage de famille");
    }
    else {
      OBJ_21.setTitle("Plage Articles");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (UCH1.isSelected())
    // lexique.HostFieldPutData("UCH1", 0, "X");
    // else
    // lexique.HostFieldPutData("UCH1", 0, " ");
    // if (WTOUM.isSelected())
    // lexique.HostFieldPutData("WTOUM", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUM", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (UCH2.isSelected())
    // lexique.HostFieldPutData("UCH2", 0, "X");
    // else
    // lexique.HostFieldPutData("UCH2", 0, " ");
    // if (REPON2.isSelected())
    // lexique.HostFieldPutData("REPON2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON2", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void UCH1ActionPerformed(ActionEvent e) {
    UCH2.setVisible(!UCH1.isSelected());
  }
  
  private void UCH2ActionPerformed(ActionEvent e) {
    UCH1.setVisible(!UCH2.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_20 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_36 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    EDEBA = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_29 = new JLabel();
    EFINA = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_23 = new JLabel();
    OBJ_28 = new JLabel();
    EFIN = new XRiTextField();
    EDEB = new XRiTextField();
    P_SEL2 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    REPON1 = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    WTOUM = new XRiCheckBox();
    panel1 = new JPanel();
    DTDEB1 = new XRiCalendrier();
    DTDEB2 = new XRiCalendrier();
    OBJ_64 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_67 = new JLabel();
    DTFIN2 = new XRiCalendrier();
    DTFIN1 = new XRiCalendrier();
    panel2 = new JPanel();
    OBJ_56 = new JXTitledSeparator();
    UCH1 = new XRiCheckBox();
    UCH2 = new XRiCheckBox();
    OBJ_19 = new JXTitledSeparator();
    REPON2 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_20 ----
          OBJ_20.setTitle("P\u00e9riodes \u00e0 s\u00e9lectionner");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_21 ----
          OBJ_21.setTitle("@LIBELLE1@");
          OBJ_21.setName("OBJ_21");

          //---- OBJ_36 ----
          OBJ_36.setTitle("Codes magasins");
          OBJ_36.setName("OBJ_36");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- EDEBA ----
            EDEBA.setComponentPopupMenu(BTD);
            EDEBA.setName("EDEBA");
            P_SEL0.add(EDEBA);
            EDEBA.setBounds(40, 7, 210, EDEBA.getPreferredSize().height);

            //---- OBJ_25 ----
            OBJ_25.setText("\u00e0");
            OBJ_25.setName("OBJ_25");
            P_SEL0.add(OBJ_25);
            OBJ_25.setBounds(260, 12, 12, 18);

            //---- OBJ_29 ----
            OBJ_29.setText("De");
            OBJ_29.setName("OBJ_29");
            P_SEL0.add(OBJ_29);
            OBJ_29.setBounds(15, 12, 21, 18);

            //---- EFINA ----
            EFINA.setComponentPopupMenu(BTD);
            EFINA.setName("EFINA");
            P_SEL0.add(EFINA);
            EFINA.setBounds(275, 7, 210, EFINA.getPreferredSize().height);
          }

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_23 ----
            OBJ_23.setText("Famille de d\u00e9but");
            OBJ_23.setName("OBJ_23");
            P_SEL1.add(OBJ_23);
            OBJ_23.setBounds(15, 14, 110, 18);

            //---- OBJ_28 ----
            OBJ_28.setText("Famille de fin");
            OBJ_28.setName("OBJ_28");
            P_SEL1.add(OBJ_28);
            OBJ_28.setBounds(300, 14, 82, 18);

            //---- EFIN ----
            EFIN.setComponentPopupMenu(BTD);
            EFIN.setName("EFIN");
            P_SEL1.add(EFIN);
            EFIN.setBounds(400, 9, 44, EFIN.getPreferredSize().height);

            //---- EDEB ----
            EDEB.setComponentPopupMenu(BTD);
            EDEB.setName("EDEB");
            P_SEL1.add(EDEB);
            EDEB.setBounds(152, 9, 44, EDEB.getPreferredSize().height);
          }

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- MA01 ----
            MA01.setComponentPopupMenu(BTD);
            MA01.setName("MA01");
            P_SEL2.add(MA01);
            MA01.setBounds(7, 9, 34, MA01.getPreferredSize().height);

            //---- MA02 ----
            MA02.setComponentPopupMenu(BTD);
            MA02.setName("MA02");
            P_SEL2.add(MA02);
            MA02.setBounds(47, 9, 34, MA02.getPreferredSize().height);

            //---- MA03 ----
            MA03.setComponentPopupMenu(BTD);
            MA03.setName("MA03");
            P_SEL2.add(MA03);
            MA03.setBounds(87, 9, 34, MA03.getPreferredSize().height);

            //---- MA04 ----
            MA04.setComponentPopupMenu(BTD);
            MA04.setName("MA04");
            P_SEL2.add(MA04);
            MA04.setBounds(127, 9, 34, MA04.getPreferredSize().height);

            //---- MA05 ----
            MA05.setComponentPopupMenu(BTD);
            MA05.setName("MA05");
            P_SEL2.add(MA05);
            MA05.setBounds(167, 9, 34, MA05.getPreferredSize().height);

            //---- MA06 ----
            MA06.setComponentPopupMenu(BTD);
            MA06.setName("MA06");
            P_SEL2.add(MA06);
            MA06.setBounds(207, 9, 34, MA06.getPreferredSize().height);

            //---- MA07 ----
            MA07.setComponentPopupMenu(BTD);
            MA07.setName("MA07");
            P_SEL2.add(MA07);
            MA07.setBounds(247, 9, 34, MA07.getPreferredSize().height);

            //---- MA08 ----
            MA08.setComponentPopupMenu(BTD);
            MA08.setName("MA08");
            P_SEL2.add(MA08);
            MA08.setBounds(287, 9, 34, MA08.getPreferredSize().height);

            //---- MA09 ----
            MA09.setComponentPopupMenu(BTD);
            MA09.setName("MA09");
            P_SEL2.add(MA09);
            MA09.setBounds(327, 9, 34, MA09.getPreferredSize().height);

            //---- MA10 ----
            MA10.setComponentPopupMenu(BTD);
            MA10.setName("MA10");
            P_SEL2.add(MA10);
            MA10.setBounds(367, 9, 34, MA10.getPreferredSize().height);

            //---- MA11 ----
            MA11.setComponentPopupMenu(BTD);
            MA11.setName("MA11");
            P_SEL2.add(MA11);
            MA11.setBounds(407, 9, 34, MA11.getPreferredSize().height);

            //---- MA12 ----
            MA12.setComponentPopupMenu(BTD);
            MA12.setName("MA12");
            P_SEL2.add(MA12);
            MA12.setBounds(447, 9, 34, MA12.getPreferredSize().height);
          }

          //---- REPON1 ----
          REPON1.setText("D\u00e9tail lignes articles");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- WTOUM ----
          WTOUM.setText("S\u00e9lection compl\u00e8te");
          WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUM.setName("WTOUM");
          WTOUM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUMActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- DTDEB1 ----
            DTDEB1.setName("DTDEB1");
            panel1.add(DTDEB1);
            DTDEB1.setBounds(165, 6, 105, DTDEB1.getPreferredSize().height);

            //---- DTDEB2 ----
            DTDEB2.setName("DTDEB2");
            panel1.add(DTDEB2);
            DTDEB2.setBounds(165, 41, 105, 28);

            //---- OBJ_64 ----
            OBJ_64.setText("Deuxi\u00e8me p\u00e9riode du");
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(10, 45, 132, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("Premi\u00e8re p\u00e9riode du");
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(10, 10, 126, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("au");
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(465, 10, 18, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("au");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(465, 45, 18, 20);

            //---- DTFIN2 ----
            DTFIN2.setName("DTFIN2");
            panel1.add(DTFIN2);
            DTFIN2.setBounds(555, 41, 105, 28);

            //---- DTFIN1 ----
            DTFIN1.setName("DTFIN1");
            panel1.add(DTFIN1);
            DTFIN1.setBounds(555, 6, 105, DTFIN1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_56 ----
            OBJ_56.setTitle("Chiffrage");
            OBJ_56.setName("OBJ_56");
            panel2.add(OBJ_56);
            OBJ_56.setBounds(15, 5, 309, OBJ_56.getPreferredSize().height);

            //---- UCH1 ----
            UCH1.setText("au P.U.M.P");
            UCH1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UCH1.setName("UCH1");
            UCH1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                UCH1ActionPerformed(e);
              }
            });
            panel2.add(UCH1);
            UCH1.setBounds(30, 30, 94, 24);

            //---- UCH2 ----
            UCH2.setText("Au prix de revient");
            UCH2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            UCH2.setName("UCH2");
            UCH2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                UCH2ActionPerformed(e);
              }
            });
            panel2.add(UCH2);
            UCH2.setBounds(185, 30, 126, 24);

            //---- OBJ_19 ----
            OBJ_19.setTitle("");
            OBJ_19.setName("OBJ_19");
            panel2.add(OBJ_19);
            OBJ_19.setBounds(460, 5, 235, 15);

            //---- REPON2 ----
            REPON2.setText("Edition des articles d\u00e9sactiv\u00e9s");
            REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPON2.setName("REPON2");
            panel2.add(REPON2);
            REPON2.setBounds(485, 30, 195, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                  .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 710, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_20;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_36;
  private JPanel P_SEL0;
  private XRiTextField EDEBA;
  private JLabel OBJ_25;
  private JLabel OBJ_29;
  private XRiTextField EFINA;
  private JPanel P_SEL1;
  private JLabel OBJ_23;
  private JLabel OBJ_28;
  private XRiTextField EFIN;
  private XRiTextField EDEB;
  private JPanel P_SEL2;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private XRiCheckBox REPON1;
  private XRiCheckBox WTOU;
  private XRiCheckBox WTOUM;
  private JPanel panel1;
  private XRiCalendrier DTDEB1;
  private XRiCalendrier DTDEB2;
  private JLabel OBJ_64;
  private JLabel OBJ_63;
  private JLabel OBJ_62;
  private JLabel OBJ_67;
  private XRiCalendrier DTFIN2;
  private XRiCalendrier DTFIN1;
  private JPanel panel2;
  private JXTitledSeparator OBJ_56;
  private XRiCheckBox UCH1;
  private XRiCheckBox UCH2;
  private JXTitledSeparator OBJ_19;
  private XRiCheckBox REPON2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
