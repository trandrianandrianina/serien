
package ri.serien.libecranrpg.stbm.STBM08FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class STBM08FM_MTT2 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  
  public STBM08FM_MTT2(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 65);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 78);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    OBJ_13 = new JXTitledSeparator();
    OBJ_10 = new JButton();
    button1 = new JButton();
    button2 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 230));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- OBJ_13 ----
      OBJ_13.setTitle("R\u00e9sultat");
      OBJ_13.setName("OBJ_13");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      // ---- button1 ----
      button1.setText("Par mois");
      button1.setName("button1");
      button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button1ActionPerformed(e);
        }
      });
      
      // ---- button2 ----
      button2.setText("Par dates");
      button2.setName("button2");
      button2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button2ActionPerformed(e);
        }
      });
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
              .addGroup(P_CentreLayout.createParallelGroup()
                  .addGroup(P_CentreLayout.createSequentialGroup().addGap(15, 15, 15).addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE,
                      292, GroupLayout.PREFERRED_SIZE))
                  .addGroup(P_CentreLayout.createSequentialGroup().addGap(260, 260, 260).addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE,
                      54, GroupLayout.PREFERRED_SIZE))
                  .addGroup(P_CentreLayout.createSequentialGroup().addGap(35, 35, 35)
                      .addGroup(P_CentreLayout.createParallelGroup()
                          .addComponent(button1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                          .addComponent(button2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))))
              .addContainerGap(13, Short.MAX_VALUE)));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(5, 5, 5)
              .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31).addComponent(button1)
              .addGap(7, 7, 7).addComponent(button2).addGap(26, 26, 26)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_13;
  private JButton OBJ_10;
  private JButton button1;
  private JButton button2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
