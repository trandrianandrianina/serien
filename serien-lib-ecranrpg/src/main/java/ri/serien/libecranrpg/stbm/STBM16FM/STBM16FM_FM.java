
package ri.serien.libecranrpg.stbm.STBM16FM;
// Nom Fichier: pop_STBM16FM_FMTFM_81.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STBM16FM_FM extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "Soc", "B", "Famille", "Montant", };
  private String[][] _T01_Data = { { "S01", "E01", "LE01", "F01", }, { "S02", "E02", "LE02", "F02", }, { "S03", "E03", "LE03", "F03", },
      { "S04", "E04", "LE04", "F04", }, { "S05", "E05", "LE05", "F05", }, { "S06", "E06", "LE06", "F06", },
      { "S07", "E07", "LE07", "F07", }, { "S08", "E08", "LE08", "F08", }, { "S09", "E09", "LE09", "F09", },
      { "S10", "E10", "LE10", "F10", }, { "S11", "E11", "LE11", "F11", }, { "S12", "E12", "LE12", "F12", },
      { "S13", "E13", "LE13", "F13", }, { "S14", "E14", "LE14", "F14", }, { "S15", "E15", "LE15", "F15", }, };
  private int[] _T01_Width = { 25, 25, 219, 120, };
  // private String[][] _LIST_Title_Data_Brut=null;
  private int[] _LIST_Justification =
      { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT };
  
  public STBM16FM_FM(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, _LIST_Justification, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(retour);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, _LIST_Title_Data_Brut, _T01_Top, _LIST_Justification);
    
    
    
    
    // TODO Icones
    OBJ_26.setIcon(lexique.chargerImage("images/stat01.gif", true));
    bt_UP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    bt_DOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    retour.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Tableau de bord des achats"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void VALActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void bt_UPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void bt_DOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "ENTER", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_26 = new JButton();
    retour = new JButton();
    MTTOT = new XRiTextField();
    OBJ_25 = new JLabel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    bt_UP = new JButton();
    bt_DOWN = new JButton();
    separator1 = compFactory.createSeparator("@LIBPOS@");

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //======== this ========
    setName("this");

    //---- OBJ_26 ----
    OBJ_26.setText("");
    OBJ_26.setToolTipText("Graphe");
    OBJ_26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_26.setName("OBJ_26");
    OBJ_26.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_26ActionPerformed(e);
      }
    });

    //---- retour ----
    retour.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    retour.setName("retour");
    retour.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_28ActionPerformed(e);
      }
    });

    //---- MTTOT ----
    MTTOT.setComponentPopupMenu(BTD);
    MTTOT.setName("MTTOT");

    //---- OBJ_25 ----
    OBJ_25.setText("Total");
    OBJ_25.setName("OBJ_25");

    //======== SCROLLPANE_LIST ========
    {
      SCROLLPANE_LIST.setComponentPopupMenu(BTD);
      SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

      //---- T01 ----
      T01.setName("T01");
      T01.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          T01MouseClicked(e);
        }
      });
      SCROLLPANE_LIST.setViewportView(T01);
    }

    //---- bt_UP ----
    bt_UP.setText("");
    bt_UP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_UP.setName("bt_UP");
    bt_UP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_UPActionPerformed(e);
      }
    });

    //---- bt_DOWN ----
    bt_DOWN.setText("");
    bt_DOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_DOWN.setName("bt_DOWN");
    bt_DOWN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_DOWNActionPerformed(e);
      }
    });

    //---- separator1 ----
    separator1.setName("separator1");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(5, 5, 5)
          .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 595, GroupLayout.PREFERRED_SIZE)
          .addGap(10, 10, 10)
          .addGroup(layout.createParallelGroup()
            .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
            .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        .addGroup(layout.createSequentialGroup()
          .addGap(415, 415, 415)
          .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
          .addGap(11, 11, 11)
          .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
          .addGap(518, 518, 518)
          .addComponent(retour, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(11, 11, 11)
          .addGroup(layout.createParallelGroup()
            .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
              .addGap(25, 25, 25)
              .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
          .addGap(5, 5, 5)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(6, 6, 6)
              .addComponent(OBJ_25))
            .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(7, 7, 7)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(retour, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JButton OBJ_26;
  private JButton retour;
  private XRiTextField MTTOT;
  private JLabel OBJ_25;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JButton bt_UP;
  private JButton bt_DOWN;
  private JComponent separator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
