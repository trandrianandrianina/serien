
package ri.serien.libecranrpg.stbm.STBM08FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class STBM08FM_B301 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_LINE);
  private String[] mois =
      { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };
  private int month = 0;
  
  public STBM08FM_B301(JPanel panel, Lexical lex, iData iD, int m) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    month = m;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    
    String chaine = null;
    String[] libelle = new String[5];
    String[] donnee = new String[5];
    int ind = 0;
    // Chargement des libellés
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("TIT" + (i + 1)).trim();
    }
    
    // Chargement des données
    
    donnee[1] = lexique.HostFieldGetNumericData("WT" + (month < 2 ? "0" + (1) : (+1)));
    donnee[2] = lexique.HostFieldGetNumericData("WT" + month + 12);
    donnee[3] = lexique.HostFieldGetNumericData("WT" + month + 24);
    donnee[4] = lexique.HostFieldGetNumericData("WT" + month + 36);
    donnee[5] = lexique.HostFieldGetNumericData("WT" + month + 48);
    
    // Préparation des données
    Object[][] data = new Object[libelle.length][2];
    for (int i = 0; i < libelle.length; i++) {
      data[i][0] = libelle[i];
      data[i][1] = Double.parseDouble(donnee[i]);
      
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe(lexique.HostFieldGetData("LIBPOS"), false);
    
    // ajout du graphe dans le lablel
    l_graphe.setIcon(graphe.getPicture(800, 480));
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(3, 50);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(3, 65);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    OBJ_13 = new JXTitledSeparator();
    OBJ_10 = new JButton();
    l_graphe = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 220));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);
      
      // ---- OBJ_13 ----
      OBJ_13.setTitle("Montant factur\u00e9 non comptabilis\u00e9");
      OBJ_13.setName("OBJ_13");
      P_Centre.add(OBJ_13);
      OBJ_13.setBounds(15, 5, 955, 20);
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_10);
      OBJ_10.setBounds(921, 560, 54, 40);
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setName("l_graphe");
      P_Centre.add(l_graphe);
      l_graphe.setBounds(40, 40, 889, 490);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_13;
  private JButton OBJ_10;
  private JLabel l_graphe;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
