
package ri.serien.libecranrpg.stbm.STBM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class STBM08FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  private String[] _LB01_Title = { "Libellé", "Date", "Début", "Fin", "Temps", };
  private String[][] _LB01_Data = { { "LB01", "D01", "H01", "H201", "P01", }, { "LB02", "D02", "H02", "H202", "P02", },
      { "LB03", "D03", "H03", "H203", "P03", }, { "LB04", "D04", "H04", "H204", "P04", }, { "LB05", "D05", "H05", "H205", "P05", },
      { "LB06", "D06", "H06", "H206", "P06", }, { "LB07", "D07", "H07", "H207", "P07", }, { "LB08", "D08", "H08", "H208", "P08", },
      { "LB09", "D09", "H09", "H209", "P09", }, { "LB10", "D10", "H10", "H210", "P10", }, { "LB11", "D11", "H11", "H211", "P11", },
      { "LB12", "D12", "H12", "H212", "P12", }, { "LB13", "D13", "H13", "H213", "P13", }, { "LB14", "D14", "H14", "H214", "P14", },
      { "LB15", "D15", "H15", "H215", "P15", }, { "LB16", "D16", "H16", "H216", "P16", }, { "LB17", "D17", "H17", "H217", "P17", }, };
  private int[] _LB01_Width = { 279, 73, 63, 64, 78, };
  // private String[] _LIST_Top=null;
  private int[] _LIST_Justification =
      { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  public STBM08FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    C17.setValeursSelection("1", " ");
    C16.setValeursSelection("1", " ");
    C15.setValeursSelection("1", " ");
    C14.setValeursSelection("1", " ");
    C13.setValeursSelection("1", " ");
    C12.setValeursSelection("1", " ");
    C11.setValeursSelection("1", " ");
    C10.setValeursSelection("1", " ");
    C09.setValeursSelection("1", " ");
    C08.setValeursSelection("1", " ");
    C07.setValeursSelection("1", " ");
    C06.setValeursSelection("1", " ");
    C05.setValeursSelection("1", " ");
    C04.setValeursSelection("1", " ");
    C03.setValeursSelection("1", " ");
    C02.setValeursSelection("1", " ");
    C01.setValeursSelection("1", " ");
    T01.setValeursSelection("1", " ");
    T17.setValeursSelection("1", " ");
    T16.setValeursSelection("1", " ");
    T15.setValeursSelection("1", " ");
    T14.setValeursSelection("1", " ");
    T13.setValeursSelection("1", " ");
    T12.setValeursSelection("1", " ");
    T11.setValeursSelection("1", " ");
    T10.setValeursSelection("1", " ");
    T09.setValeursSelection("1", " ");
    T08.setValeursSelection("1", " ");
    T07.setValeursSelection("1", " ");
    T06.setValeursSelection("1", " ");
    T05.setValeursSelection("1", " ");
    T04.setValeursSelection("1", " ");
    T03.setValeursSelection("1", " ");
    T02.setValeursSelection("1", " ");
    LB01.setAspectTable(null, _LB01_Title, _LB01_Data, _LB01_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    T01.setSelected(!T01.isSelected());
    T02.setSelected(!T02.isSelected());
    T03.setSelected(!T03.isSelected());
    T04.setSelected(!T04.isSelected());
    T05.setSelected(!T05.isSelected());
    T06.setSelected(!T06.isSelected());
    T07.setSelected(!T07.isSelected());
    T08.setSelected(!T08.isSelected());
    T09.setSelected(!T09.isSelected());
    T10.setSelected(!T10.isSelected());
    T11.setSelected(!T11.isSelected());
    T12.setSelected(!T12.isSelected());
    T13.setSelected(!T13.isSelected());
    T14.setSelected(!T14.isSelected());
    T15.setSelected(!T15.isSelected());
    T16.setSelected(!T16.isSelected());
    T17.setSelected(!T17.isSelected());
  }
  
  private void riSousMenu_bt_export2ActionPerformed(ActionEvent e) {
    C01.setSelected(!C01.isSelected());
    C02.setSelected(!C02.isSelected());
    C03.setSelected(!C03.isSelected());
    C04.setSelected(!C04.isSelected());
    C05.setSelected(!C05.isSelected());
    C06.setSelected(!C06.isSelected());
    C07.setSelected(!C07.isSelected());
    C08.setSelected(!C08.isSelected());
    C09.setSelected(!C09.isSelected());
    C10.setSelected(!C10.isSelected());
    C11.setSelected(!C11.isSelected());
    C12.setSelected(!C12.isSelected());
    C13.setSelected(!C13.isSelected());
    C14.setSelected(!C14.isSelected());
    C15.setSelected(!C15.isSelected());
    C16.setSelected(!C16.isSelected());
    C17.setSelected(!C17.isSelected());
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt_export2 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    SCROLLPANE_OBJ_28 = new JScrollPane();
    LB01 = new XRiTable();
    OBJ_92 = new RiZoneSortie();
    OBJ_93 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_91 = new RiZoneSortie();
    PER01 = new XRiTextField();
    PER02 = new XRiTextField();
    OBJ_80 = new JXTitledSeparator();
    panel1 = new JPanel();
    T02 = new XRiCheckBox();
    T03 = new XRiCheckBox();
    T04 = new XRiCheckBox();
    T05 = new XRiCheckBox();
    T06 = new XRiCheckBox();
    T07 = new XRiCheckBox();
    T08 = new XRiCheckBox();
    T09 = new XRiCheckBox();
    T10 = new XRiCheckBox();
    T11 = new XRiCheckBox();
    T12 = new XRiCheckBox();
    T13 = new XRiCheckBox();
    T14 = new XRiCheckBox();
    T15 = new XRiCheckBox();
    T16 = new XRiCheckBox();
    T17 = new XRiCheckBox();
    T01 = new XRiCheckBox();
    panel2 = new JPanel();
    C01 = new XRiCheckBox();
    C02 = new XRiCheckBox();
    C03 = new XRiCheckBox();
    C04 = new XRiCheckBox();
    C05 = new XRiCheckBox();
    C06 = new XRiCheckBox();
    C07 = new XRiCheckBox();
    C08 = new XRiCheckBox();
    C09 = new XRiCheckBox();
    C10 = new XRiCheckBox();
    C11 = new XRiCheckBox();
    C12 = new XRiCheckBox();
    C13 = new XRiCheckBox();
    C14 = new XRiCheckBox();
    C15 = new XRiCheckBox();
    C16 = new XRiCheckBox();
    C17 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.PAGE_START);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Tous sur periode 1");
              riSousMenu_bt_export.setToolTipText("S\u00e9lection tous les postes de la p\u00e9riode 1");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt_export2 ----
              riSousMenu_bt_export2.setText("Tous sur periode 2");
              riSousMenu_bt_export2.setToolTipText("S\u00e9lection tous les postes de la p\u00e9riode 2");
              riSousMenu_bt_export2.setName("riSousMenu_bt_export2");
              riSousMenu_bt_export2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_export2ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt_export2);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //======== SCROLLPANE_OBJ_28 ========
          {
            SCROLLPANE_OBJ_28.setName("SCROLLPANE_OBJ_28");

            //---- LB01 ----
            LB01.setName("LB01");
            SCROLLPANE_OBJ_28.setViewportView(LB01);
          }

          //---- OBJ_92 ----
          OBJ_92.setText("@MESCLO@");
          OBJ_92.setName("OBJ_92");

          //---- OBJ_93 ----
          OBJ_93.setText("Choix p\u00e9riode 2");
          OBJ_93.setName("OBJ_93");

          //---- OBJ_94 ----
          OBJ_94.setText("Choix p\u00e9riode 1");
          OBJ_94.setName("OBJ_94");

          //---- OBJ_88 ----
          OBJ_88.setText("En cours");
          OBJ_88.setName("OBJ_88");

          //---- OBJ_91 ----
          OBJ_91.setText("@DGPERX@");
          OBJ_91.setName("OBJ_91");

          //---- PER01 ----
          PER01.setComponentPopupMenu(BTD);
          PER01.setName("PER01");

          //---- PER02 ----
          PER02.setComponentPopupMenu(BTD);
          PER02.setName("PER02");

          //---- OBJ_80 ----
          OBJ_80.setTitle("Etat de mise \u00e0 jour du tableau de bord");
          OBJ_80.setName("OBJ_80");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- T02 ----
            T02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T02.setName("T02");
            panel1.add(T02);
            T02.setBounds(0, 16, 25, 14);

            //---- T03 ----
            T03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T03.setName("T03");
            panel1.add(T03);
            T03.setBounds(0, 32, 25, 14);

            //---- T04 ----
            T04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T04.setName("T04");
            panel1.add(T04);
            T04.setBounds(0, 48, 25, 14);

            //---- T05 ----
            T05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T05.setName("T05");
            panel1.add(T05);
            T05.setBounds(0, 64, 25, 14);

            //---- T06 ----
            T06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T06.setName("T06");
            panel1.add(T06);
            T06.setBounds(0, 80, 25, 14);

            //---- T07 ----
            T07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T07.setName("T07");
            panel1.add(T07);
            T07.setBounds(0, 96, 25, 14);

            //---- T08 ----
            T08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T08.setName("T08");
            panel1.add(T08);
            T08.setBounds(0, 112, 25, 14);

            //---- T09 ----
            T09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T09.setName("T09");
            panel1.add(T09);
            T09.setBounds(0, 128, 25, 14);

            //---- T10 ----
            T10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T10.setName("T10");
            panel1.add(T10);
            T10.setBounds(0, 144, 25, 14);

            //---- T11 ----
            T11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T11.setName("T11");
            panel1.add(T11);
            T11.setBounds(0, 160, 25, 14);

            //---- T12 ----
            T12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T12.setName("T12");
            panel1.add(T12);
            T12.setBounds(0, 176, 25, 14);

            //---- T13 ----
            T13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T13.setName("T13");
            panel1.add(T13);
            T13.setBounds(0, 192, 25, 14);

            //---- T14 ----
            T14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T14.setName("T14");
            panel1.add(T14);
            T14.setBounds(0, 208, 25, 14);

            //---- T15 ----
            T15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T15.setName("T15");
            panel1.add(T15);
            T15.setBounds(0, 224, 25, 14);

            //---- T16 ----
            T16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T16.setName("T16");
            panel1.add(T16);
            T16.setBounds(0, 240, 25, 14);

            //---- T17 ----
            T17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T17.setName("T17");
            panel1.add(T17);
            T17.setBounds(0, 256, 25, 14);

            //---- T01 ----
            T01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T01.setName("T01");
            panel1.add(T01);
            T01.setBounds(0, 0, 25, 14);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- C01 ----
            C01.setText("");
            C01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C01.setName("C01");
            panel2.add(C01);
            C01.setBounds(0, 0, 21, 14);

            //---- C02 ----
            C02.setText("");
            C02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C02.setName("C02");
            panel2.add(C02);
            C02.setBounds(0, 16, 21, 14);

            //---- C03 ----
            C03.setText("");
            C03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C03.setName("C03");
            panel2.add(C03);
            C03.setBounds(0, 32, 21, 14);

            //---- C04 ----
            C04.setText("");
            C04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C04.setName("C04");
            panel2.add(C04);
            C04.setBounds(0, 48, 21, 14);

            //---- C05 ----
            C05.setText("");
            C05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C05.setName("C05");
            panel2.add(C05);
            C05.setBounds(0, 64, 21, 14);

            //---- C06 ----
            C06.setText("");
            C06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C06.setName("C06");
            panel2.add(C06);
            C06.setBounds(0, 80, 21, 14);

            //---- C07 ----
            C07.setText("");
            C07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C07.setName("C07");
            panel2.add(C07);
            C07.setBounds(0, 96, 21, 14);

            //---- C08 ----
            C08.setText("");
            C08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C08.setName("C08");
            panel2.add(C08);
            C08.setBounds(0, 112, 21, 14);

            //---- C09 ----
            C09.setText("");
            C09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C09.setName("C09");
            panel2.add(C09);
            C09.setBounds(0, 128, 21, 14);

            //---- C10 ----
            C10.setText("");
            C10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C10.setName("C10");
            panel2.add(C10);
            C10.setBounds(0, 144, 21, 14);

            //---- C11 ----
            C11.setText("");
            C11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C11.setName("C11");
            panel2.add(C11);
            C11.setBounds(0, 160, 21, 14);

            //---- C12 ----
            C12.setText("");
            C12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C12.setName("C12");
            panel2.add(C12);
            C12.setBounds(0, 176, 21, 14);

            //---- C13 ----
            C13.setText("");
            C13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C13.setName("C13");
            panel2.add(C13);
            C13.setBounds(0, 192, 21, 14);

            //---- C14 ----
            C14.setText("");
            C14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C14.setName("C14");
            panel2.add(C14);
            C14.setBounds(0, 208, 21, 14);

            //---- C15 ----
            C15.setText("");
            C15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C15.setName("C15");
            panel2.add(C15);
            C15.setBounds(0, 224, 21, 14);

            //---- C16 ----
            C16.setText("");
            C16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C16.setName("C16");
            panel2.add(C16);
            C16.setBounds(0, 240, 21, 14);

            //---- C17 ----
            C17.setText("");
            C17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            C17.setName("C17");
            panel2.add(C17);
            C17.setBounds(0, 256, 21, 14);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addComponent(OBJ_94, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(PER01, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_93, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(PER02, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(SCROLLPANE_OBJ_28, GroupLayout.PREFERRED_SIZE, 675, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(27, 27, 27)
                .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_88))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_94))
                  .addComponent(PER01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_93))
                  .addComponent(PER02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(SCROLLPANE_OBJ_28, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt_export2;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JScrollPane SCROLLPANE_OBJ_28;
  private XRiTable LB01;
  private RiZoneSortie OBJ_92;
  private JLabel OBJ_93;
  private JLabel OBJ_94;
  private JLabel OBJ_88;
  private RiZoneSortie OBJ_91;
  private XRiTextField PER01;
  private XRiTextField PER02;
  private JXTitledSeparator OBJ_80;
  private JPanel panel1;
  private XRiCheckBox T02;
  private XRiCheckBox T03;
  private XRiCheckBox T04;
  private XRiCheckBox T05;
  private XRiCheckBox T06;
  private XRiCheckBox T07;
  private XRiCheckBox T08;
  private XRiCheckBox T09;
  private XRiCheckBox T10;
  private XRiCheckBox T11;
  private XRiCheckBox T12;
  private XRiCheckBox T13;
  private XRiCheckBox T14;
  private XRiCheckBox T15;
  private XRiCheckBox T16;
  private XRiCheckBox T17;
  private XRiCheckBox T01;
  private JPanel panel2;
  private XRiCheckBox C01;
  private XRiCheckBox C02;
  private XRiCheckBox C03;
  private XRiCheckBox C04;
  private XRiCheckBox C05;
  private XRiCheckBox C06;
  private XRiCheckBox C07;
  private XRiCheckBox C08;
  private XRiCheckBox C09;
  private XRiCheckBox C10;
  private XRiCheckBox C11;
  private XRiCheckBox C12;
  private XRiCheckBox C13;
  private XRiCheckBox C14;
  private XRiCheckBox C15;
  private XRiCheckBox C16;
  private XRiCheckBox C17;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
