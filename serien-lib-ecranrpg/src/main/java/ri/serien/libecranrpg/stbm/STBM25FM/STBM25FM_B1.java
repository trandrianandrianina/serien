
package ri.serien.libecranrpg.stbm.STBM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STBM25FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STBM25FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    WTOUC.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
    WTOUCC.setValeursSelection("**", "  ");
    WTOUSF.setValeursSelection("**", "  ");
    WTOURS.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    panel1.setVisible(!WTOUC.isSelected());
    panel2.setVisible(!WTOUM.isSelected());
    panel3.setVisible(!WTOUCC.isSelected());
    panel4.setVisible(!WTOUSF.isSelected());
    panel5.setVisible(!WTOURS.isSelected());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ - @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUCActionPerformed(ActionEvent e) {
    panel1.setVisible(!WTOUC.isSelected());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    panel2.setVisible(!WTOUM.isSelected());
  }
  
  private void WTOUCCActionPerformed(ActionEvent e) {
    panel3.setVisible(!WTOUCC.isSelected());
  }
  
  private void WTOUSFActionPerformed(ActionEvent e) {
    panel4.setVisible(!WTOUSF.isSelected());
  }
  
  private void WTOURSActionPerformed(ActionEvent e) {
    panel5.setVisible(!WTOURS.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    OBJ_24 = new JXTitledSeparator();
    WTOUC = new XRiCheckBox();
    WTOUCC = new XRiCheckBox();
    WTOUSF = new XRiCheckBox();
    WTOUM = new XRiCheckBox();
    WTOURS = new XRiCheckBox();
    OBJ_75 = new JLabel();
    PERFIC = new XRiCalendrier();
    OBJ_21 = new JXTitledSeparator();
    panel1 = new JPanel();
    OBJ_76 = new JLabel();
    COLDEB = new XRiTextField();
    COLFIN = new XRiTextField();
    OBJ_77 = new JLabel();
    panel2 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA011 = new XRiTextField();
    MA12 = new XRiTextField();
    panel3 = new JPanel();
    CC01 = new XRiTextField();
    CC02 = new XRiTextField();
    CC03 = new XRiTextField();
    CC04 = new XRiTextField();
    CC05 = new XRiTextField();
    CC06 = new XRiTextField();
    CC07 = new XRiTextField();
    CC08 = new XRiTextField();
    CC09 = new XRiTextField();
    CC10 = new XRiTextField();
    CC11 = new XRiTextField();
    CC12 = new XRiTextField();
    panel4 = new JPanel();
    SF01 = new XRiTextField();
    SF02 = new XRiTextField();
    SF03 = new XRiTextField();
    SF04 = new XRiTextField();
    SF05 = new XRiTextField();
    panel5 = new JPanel();
    RS01 = new XRiTextField();
    RS02 = new XRiTextField();
    RS03 = new XRiTextField();
    RS04 = new XRiTextField();
    RS05 = new XRiTextField();
    OBJ_25 = new JXTitledSeparator();
    OBJ_23 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    OBJ_88 = new JLabel();
    OBJ_91 = new RiZoneSortie();
    OBJ_92 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setForeground(Color.black);
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_19 ----
          OBJ_19.setTitle("Code magasin \u00e0 traiter");
          OBJ_19.setName("OBJ_19");

          //---- OBJ_24 ----
          OBJ_24.setTitle("Plage de comptes \u00e0 traiter");
          OBJ_24.setName("OBJ_24");

          //---- WTOUC ----
          WTOUC.setText("Selection compl\u00e8te");
          WTOUC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUC.setName("WTOUC");
          WTOUC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUCActionPerformed(e);
            }
          });

          //---- WTOUCC ----
          WTOUCC.setText("Selection compl\u00e8te");
          WTOUCC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUCC.setName("WTOUCC");
          WTOUCC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUCCActionPerformed(e);
            }
          });

          //---- WTOUSF ----
          WTOUSF.setText("Selection compl\u00e8te");
          WTOUSF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUSF.setName("WTOUSF");
          WTOUSF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUSFActionPerformed(e);
            }
          });

          //---- WTOUM ----
          WTOUM.setText("Selection compl\u00e8te");
          WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUM.setName("WTOUM");
          WTOUM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUMActionPerformed(e);
            }
          });

          //---- WTOURS ----
          WTOURS.setText("Selection compl\u00e8te");
          WTOURS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOURS.setName("WTOURS");
          WTOURS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOURSActionPerformed(e);
            }
          });

          //---- OBJ_75 ----
          OBJ_75.setText("Date de demande");
          OBJ_75.setName("OBJ_75");

          //---- PERFIC ----
          PERFIC.setComponentPopupMenu(BTD);
          PERFIC.setName("PERFIC");

          //---- OBJ_21 ----
          OBJ_21.setTitle("");
          OBJ_21.setName("OBJ_21");

          //======== panel1 ========
          {
            panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_76 ----
            OBJ_76.setText("Compte collectif client de d\u00e9but");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(10, 14, 205, 20);

            //---- COLDEB ----
            COLDEB.setComponentPopupMenu(BTD);
            COLDEB.setName("COLDEB");
            panel1.add(COLDEB);
            COLDEB.setBounds(220, 10, 60, COLDEB.getPreferredSize().height);

            //---- COLFIN ----
            COLFIN.setComponentPopupMenu(BTD);
            COLFIN.setName("COLFIN");
            panel1.add(COLFIN);
            COLFIN.setBounds(220, 45, 60, COLFIN.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("Compte collectif client de fin");
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(10, 49, 205, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- MA01 ----
            MA01.setComponentPopupMenu(BTD);
            MA01.setName("MA01");
            panel2.add(MA01);
            MA01.setBounds(10, 10, 34, MA01.getPreferredSize().height);

            //---- MA02 ----
            MA02.setComponentPopupMenu(BTD);
            MA02.setName("MA02");
            panel2.add(MA02);
            MA02.setBounds(43, 10, 34, MA02.getPreferredSize().height);

            //---- MA03 ----
            MA03.setComponentPopupMenu(BTD);
            MA03.setName("MA03");
            panel2.add(MA03);
            MA03.setBounds(76, 10, 34, MA03.getPreferredSize().height);

            //---- MA04 ----
            MA04.setComponentPopupMenu(BTD);
            MA04.setName("MA04");
            panel2.add(MA04);
            MA04.setBounds(109, 10, 34, MA04.getPreferredSize().height);

            //---- MA05 ----
            MA05.setComponentPopupMenu(BTD);
            MA05.setName("MA05");
            panel2.add(MA05);
            MA05.setBounds(142, 10, 34, MA05.getPreferredSize().height);

            //---- MA06 ----
            MA06.setComponentPopupMenu(BTD);
            MA06.setName("MA06");
            panel2.add(MA06);
            MA06.setBounds(175, 10, 34, MA06.getPreferredSize().height);

            //---- MA07 ----
            MA07.setComponentPopupMenu(BTD);
            MA07.setName("MA07");
            panel2.add(MA07);
            MA07.setBounds(208, 10, 34, MA07.getPreferredSize().height);

            //---- MA08 ----
            MA08.setComponentPopupMenu(BTD);
            MA08.setName("MA08");
            panel2.add(MA08);
            MA08.setBounds(241, 10, 34, MA08.getPreferredSize().height);

            //---- MA09 ----
            MA09.setComponentPopupMenu(BTD);
            MA09.setName("MA09");
            panel2.add(MA09);
            MA09.setBounds(274, 10, 34, MA09.getPreferredSize().height);

            //---- MA10 ----
            MA10.setComponentPopupMenu(BTD);
            MA10.setName("MA10");
            panel2.add(MA10);
            MA10.setBounds(307, 10, 34, MA10.getPreferredSize().height);

            //---- MA011 ----
            MA011.setComponentPopupMenu(BTD);
            MA011.setName("MA011");
            panel2.add(MA011);
            MA011.setBounds(340, 10, 34, MA011.getPreferredSize().height);

            //---- MA12 ----
            MA12.setComponentPopupMenu(BTD);
            MA12.setName("MA12");
            panel2.add(MA12);
            MA12.setBounds(373, 10, 34, MA12.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- CC01 ----
            CC01.setComponentPopupMenu(BTD);
            CC01.setName("CC01");
            panel3.add(CC01);
            CC01.setBounds(10, 10, 44, CC01.getPreferredSize().height);

            //---- CC02 ----
            CC02.setComponentPopupMenu(BTD);
            CC02.setName("CC02");
            panel3.add(CC02);
            CC02.setBounds(51, 10, 44, CC02.getPreferredSize().height);

            //---- CC03 ----
            CC03.setComponentPopupMenu(BTD);
            CC03.setName("CC03");
            panel3.add(CC03);
            CC03.setBounds(92, 10, 44, CC03.getPreferredSize().height);

            //---- CC04 ----
            CC04.setComponentPopupMenu(BTD);
            CC04.setName("CC04");
            panel3.add(CC04);
            CC04.setBounds(133, 10, 44, CC04.getPreferredSize().height);

            //---- CC05 ----
            CC05.setComponentPopupMenu(BTD);
            CC05.setName("CC05");
            panel3.add(CC05);
            CC05.setBounds(174, 10, 44, CC05.getPreferredSize().height);

            //---- CC06 ----
            CC06.setComponentPopupMenu(BTD);
            CC06.setName("CC06");
            panel3.add(CC06);
            CC06.setBounds(215, 10, 44, CC06.getPreferredSize().height);

            //---- CC07 ----
            CC07.setComponentPopupMenu(BTD);
            CC07.setName("CC07");
            panel3.add(CC07);
            CC07.setBounds(256, 10, 44, CC07.getPreferredSize().height);

            //---- CC08 ----
            CC08.setComponentPopupMenu(BTD);
            CC08.setName("CC08");
            panel3.add(CC08);
            CC08.setBounds(338, 10, 44, CC08.getPreferredSize().height);

            //---- CC09 ----
            CC09.setComponentPopupMenu(BTD);
            CC09.setName("CC09");
            panel3.add(CC09);
            CC09.setBounds(297, 10, 44, CC09.getPreferredSize().height);

            //---- CC10 ----
            CC10.setComponentPopupMenu(BTD);
            CC10.setName("CC10");
            panel3.add(CC10);
            CC10.setBounds(379, 10, 44, CC10.getPreferredSize().height);

            //---- CC11 ----
            CC11.setComponentPopupMenu(BTD);
            CC11.setName("CC11");
            panel3.add(CC11);
            CC11.setBounds(420, 10, 44, CC11.getPreferredSize().height);

            //---- CC12 ----
            CC12.setComponentPopupMenu(BTD);
            CC12.setName("CC12");
            panel3.add(CC12);
            CC12.setBounds(461, 10, 44, CC12.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- SF01 ----
            SF01.setComponentPopupMenu(BTD);
            SF01.setName("SF01");
            panel4.add(SF01);
            SF01.setBounds(10, 10, 64, SF01.getPreferredSize().height);

            //---- SF02 ----
            SF02.setComponentPopupMenu(BTD);
            SF02.setName("SF02");
            panel4.add(SF02);
            SF02.setBounds(92, 10, 64, SF02.getPreferredSize().height);

            //---- SF03 ----
            SF03.setComponentPopupMenu(BTD);
            SF03.setName("SF03");
            panel4.add(SF03);
            SF03.setBounds(174, 10, 64, SF03.getPreferredSize().height);

            //---- SF04 ----
            SF04.setComponentPopupMenu(BTD);
            SF04.setName("SF04");
            panel4.add(SF04);
            SF04.setBounds(256, 10, 64, SF04.getPreferredSize().height);

            //---- SF05 ----
            SF05.setComponentPopupMenu(BTD);
            SF05.setName("SF05");
            panel4.add(SF05);
            SF05.setBounds(338, 10, 64, SF05.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- RS01 ----
            RS01.setComponentPopupMenu(BTD);
            RS01.setName("RS01");
            panel5.add(RS01);
            RS01.setBounds(10, 10, 24, RS01.getPreferredSize().height);

            //---- RS02 ----
            RS02.setComponentPopupMenu(BTD);
            RS02.setName("RS02");
            panel5.add(RS02);
            RS02.setBounds(65, 10, 24, RS02.getPreferredSize().height);

            //---- RS03 ----
            RS03.setComponentPopupMenu(BTD);
            RS03.setName("RS03");
            panel5.add(RS03);
            RS03.setBounds(125, 10, 24, RS03.getPreferredSize().height);

            //---- RS04 ----
            RS04.setComponentPopupMenu(BTD);
            RS04.setName("RS04");
            panel5.add(RS04);
            RS04.setBounds(180, 10, 24, RS04.getPreferredSize().height);

            //---- RS05 ----
            RS05.setComponentPopupMenu(BTD);
            RS05.setName("RS05");
            panel5.add(RS05);
            RS05.setBounds(240, 10, 24, RS05.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_25 ----
          OBJ_25.setTitle("Code restriction \u00e0 traiter");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_23 ----
          OBJ_23.setTitle("Code assurance \u00e0 traiter");
          OBJ_23.setName("OBJ_23");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Code cat\u00e9gorie \u00e0 traiter");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_88 ----
          OBJ_88.setText("En cours");
          OBJ_88.setName("OBJ_88");

          //---- OBJ_91 ----
          OBJ_91.setText("@DGPERX@");
          OBJ_91.setName("OBJ_91");

          //---- OBJ_92 ----
          OBJ_92.setText("@MESCLO@");
          OBJ_92.setName("OBJ_92");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(25, 25, 25)
                    .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(9, 9, 9)
                    .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(PERFIC, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUC, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 435, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUCC, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 515, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUSF, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOURS, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_88))
                      .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(16, 16, 16)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PERFIC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(WTOUC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOUM, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOUCC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOUSF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(WTOURS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_24;
  private XRiCheckBox WTOUC;
  private XRiCheckBox WTOUCC;
  private XRiCheckBox WTOUSF;
  private XRiCheckBox WTOUM;
  private XRiCheckBox WTOURS;
  private JLabel OBJ_75;
  private XRiCalendrier PERFIC;
  private JXTitledSeparator OBJ_21;
  private JPanel panel1;
  private JLabel OBJ_76;
  private XRiTextField COLDEB;
  private XRiTextField COLFIN;
  private JLabel OBJ_77;
  private JPanel panel2;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA011;
  private XRiTextField MA12;
  private JPanel panel3;
  private XRiTextField CC01;
  private XRiTextField CC02;
  private XRiTextField CC03;
  private XRiTextField CC04;
  private XRiTextField CC05;
  private XRiTextField CC06;
  private XRiTextField CC07;
  private XRiTextField CC08;
  private XRiTextField CC09;
  private XRiTextField CC10;
  private XRiTextField CC11;
  private XRiTextField CC12;
  private JPanel panel4;
  private XRiTextField SF01;
  private XRiTextField SF02;
  private XRiTextField SF03;
  private XRiTextField SF04;
  private XRiTextField SF05;
  private JPanel panel5;
  private XRiTextField RS01;
  private XRiTextField RS02;
  private XRiTextField RS03;
  private XRiTextField RS04;
  private XRiTextField RS05;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_23;
  private JXTitledSeparator OBJ_22;
  private JLabel OBJ_88;
  private RiZoneSortie OBJ_91;
  private RiZoneSortie OBJ_92;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
