
package ri.serien.libecranrpg.stbm.STBM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class STBM08FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  private STBM08FM_MT01 mt01 = null;
  private STBM08FM_MT02 mt02 = null;
  private STBM08FM_MT03 mt03 = null;
  private STBM08FM_MT04 mt04 = null;
  private STBM08FM_MT06 mt06 = null;
  private STBM08FM_MT10 mt10 = null;
  private STBM08FM_MT12 mt12 = null;
  private STBM08FM_MT16 mt16 = null;
  private STBM08FM_MTT2 mtt2 = null;
  
  public STBM08FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    label34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPERIO@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB02@")).trim());
    bt_mt02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB03@")).trim());
    bt_mt03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT03@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB04@")).trim());
    bt_mt04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT04@")).trim());
    bt_mt01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT01@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB01@")).trim());
    bt_mtt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT1@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB17@")).trim());
    bt_mt17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT17@")).trim());
    bt_mt05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT05@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB11@")).trim());
    bt_mt11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT11@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB08@")).trim());
    bt_mt08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT08@")).trim());
    label18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB09@")).trim());
    bt_mt09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT09@")).trim());
    label19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB07@")).trim());
    bt_mt07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT07@")).trim());
    bt_mtt3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT3@")).trim());
    label20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB14@")).trim());
    bt_mt14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT14@")).trim());
    bt_mtt4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT4@")).trim());
    bt_mtt5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT5@")).trim());
    label23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB13@")).trim());
    bt_mt13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT13@")).trim());
    bt_mtt6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT6@")).trim());
    label27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB15@")).trim());
    bt_mt15w.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT15W@")).trim());
    bt_mt10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT045@")).trim());
    bt_mt102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT102@")).trim());
    bt_mt103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT103@")).trim());
    bt_mt06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT06@")).trim());
    bt_mtt2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT2@")).trim());
    bt_mt16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB12@")).trim());
    bt_mt12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT12@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    
    

    
    p_bpresentation.setCodeEtablissement(WSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WSOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/stats.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bt_mt02ActionPerformed(ActionEvent e) {
    if (mt02 == null) {
      mt02 = new STBM08FM_MT02(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt03ActionPerformed(ActionEvent e) {
    if (mt03 == null) {
      mt03 = new STBM08FM_MT03(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt04ActionPerformed(ActionEvent e) {
    if (mt04 == null) {
      mt04 = new STBM08FM_MT04(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt01ActionPerformed(ActionEvent e) {
    if (mt01 == null) {
      mt01 = new STBM08FM_MT01(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt05ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(8, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt10ActionPerformed(ActionEvent e) {
    if (mt10 == null) {
      mt10 = new STBM08FM_MT10(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt06ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(9, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mtt2ActionPerformed(ActionEvent e) {
    if (mtt2 == null) {
      mtt2 = new STBM08FM_MTT2(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt16ActionPerformed(ActionEvent e) {
    if (mt16 == null) {
      mt16 = new STBM08FM_MT16(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt12ActionPerformed(ActionEvent e) {
    if (mt12 == null) {
      mt12 = new STBM08FM_MT12(this, lexique, interpreteurD);
    }
  }
  
  private void bt_mt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt08ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(14, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt09ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(15, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt07ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(19, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt15wActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    label32 = new JLabel();
    WSOC = new XRiTextField();
    label33 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    label34 = new JLabel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    label4 = new JLabel();
    bt_mt02 = new JButton();
    label5 = new JLabel();
    bt_mt03 = new JButton();
    label6 = new JLabel();
    bt_mt04 = new JButton();
    bt_mt01 = new JButton();
    label2 = new JLabel();
    panel7 = new JPanel();
    label3 = new JLabel();
    bt_mtt1 = new JButton();
    label7 = new JLabel();
    bt_mt17 = new JButton();
    label8 = new JLabel();
    bt_mt05 = new JButton();
    panel4 = new JPanel();
    label16 = new JLabel();
    bt_mt11 = new JButton();
    label17 = new JLabel();
    bt_mt08 = new JButton();
    label18 = new JLabel();
    bt_mt09 = new JButton();
    label19 = new JLabel();
    bt_mt07 = new JButton();
    panel8 = new JPanel();
    label31 = new JLabel();
    bt_mtt3 = new JButton();
    panel5 = new JPanel();
    label20 = new JLabel();
    bt_mt14 = new JButton();
    label21 = new JLabel();
    bt_mtt4 = new JButton();
    label22 = new JLabel();
    bt_mtt5 = new JButton();
    label23 = new JLabel();
    bt_mt13 = new JButton();
    panel6 = new JPanel();
    label24 = new JLabel();
    bt_mtt6 = new JButton();
    label27 = new JLabel();
    bt_mt15w = new JButton();
    panel3 = new JPanel();
    label10 = new JLabel();
    bt_mt10 = new JButton();
    label11 = new JLabel();
    bt_mt102 = new JButton();
    label12 = new JLabel();
    bt_mt103 = new JButton();
    bt_mt06 = new JButton();
    label9 = new JLabel();
    panel9 = new JPanel();
    label13 = new JLabel();
    bt_mtt2 = new JButton();
    label14 = new JLabel();
    bt_mt16 = new JButton();
    label15 = new JLabel();
    bt_mt12 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord g\u00e9n\u00e9ral");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- label32 ----
          label32.setText("soci\u00e9t\u00e9");
          label32.setName("label32");
          p_tete_gauche.add(label32);
          label32.setBounds(5, 5, 57, label32.getPreferredSize().height);

          //---- WSOC ----
          WSOC.setName("WSOC");
          p_tete_gauche.add(WSOC);
          WSOC.setBounds(80, -1, 40, WSOC.getPreferredSize().height);

          //---- label33 ----
          label33.setText("@DGNOM@");
          label33.setOpaque(false);
          label33.setName("label33");
          p_tete_gauche.add(label33);
          label33.setBounds(125, 1, 310, label33.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label34 ----
          label34.setText("@WPERIO@");
          label34.setFont(label34.getFont().deriveFont(label34.getFont().getStyle() | Font.BOLD, label34.getFont().getSize() + 2f));
          label34.setName("label34");
          p_tete_droite.add(label34);

          //---- label1 ----
          label1.setText("P\u00e9riode :");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 2f));
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Statistiques");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Tableau de bord acheteur");
            riSousMenu_bt6.setToolTipText("Tableau de bord acheteur");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Tabl. de bord commercial");
            riSousMenu_bt7.setToolTipText("Tableau de bord commercial");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Pr\u00e9visions de tr\u00e9sorerie");
            riSousMenu_bt8.setToolTipText("Pr\u00e9visions de tr\u00e9sorerie");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Statistiques sur le factur\u00e9");
            riSousMenu_bt9.setToolTipText("Statistiques sur le factur\u00e9");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Stat. sur le command\u00e9");
            riSousMenu_bt10.setToolTipText("Statistiques sur le command\u00e9");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);

          //======== riSousMenu11 ========
          {
            riSousMenu11.setName("riSousMenu11");

            //---- riSousMenu_bt11 ----
            riSousMenu_bt11.setText("Statistiques d'achats");
            riSousMenu_bt11.setToolTipText("Statistiques d'achats");
            riSousMenu_bt11.setName("riSousMenu_bt11");
            riSousMenu_bt11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt11ActionPerformed(e);
              }
            });
            riSousMenu11.add(riSousMenu_bt11);
          }
          menus_haut.add(riSousMenu11);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Changement de p\u00e9riode");
            riSousMenu_bt14.setToolTipText("Changement de p\u00e9riode d'analyse");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(970, 590));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Chiffre d'affaire"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- label4 ----
              label4.setText("@LB02@");
              label4.setName("label4");
              panel2.add(label4);
              label4.setBounds(15, 55, 275, 20);

              //---- bt_mt02 ----
              bt_mt02.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt02.setText("@MT02@");
              bt_mt02.setName("bt_mt02");
              bt_mt02.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt02ActionPerformed(e);
                }
              });
              panel2.add(bt_mt02);
              bt_mt02.setBounds(325, 53, 116, 25);

              //---- label5 ----
              label5.setText("@LB03@");
              label5.setName("label5");
              panel2.add(label5);
              label5.setBounds(15, 80, 275, 20);

              //---- bt_mt03 ----
              bt_mt03.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt03.setText("@MT03@");
              bt_mt03.setName("bt_mt03");
              bt_mt03.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt03ActionPerformed(e);
                }
              });
              panel2.add(bt_mt03);
              bt_mt03.setBounds(325, 78, 116, 25);

              //---- label6 ----
              label6.setText("@LB04@");
              label6.setName("label6");
              panel2.add(label6);
              label6.setBounds(15, 105, 275, 20);

              //---- bt_mt04 ----
              bt_mt04.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt04.setText("@MT04@");
              bt_mt04.setName("bt_mt04");
              bt_mt04.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt04ActionPerformed(e);
                }
              });
              panel2.add(bt_mt04);
              bt_mt04.setBounds(325, 103, 116, 25);

              //---- bt_mt01 ----
              bt_mt01.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt01.setText("@MT01@");
              bt_mt01.setName("bt_mt01");
              bt_mt01.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt01ActionPerformed(e);
                }
              });
              panel2.add(bt_mt01);
              bt_mt01.setBounds(325, 28, 116, 25);

              //---- label2 ----
              label2.setText("@LB01@");
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(15, 30, 275, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(12, 13, 458, 152);

            //======== panel7 ========
            {
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);

              //---- label3 ----
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setText("TOTAL POSTES CHIFFRE D'AFFAIRES");
              label3.setName("label3");
              panel7.add(label3);
              label3.setBounds(10, 15, 275, 20);

              //---- bt_mtt1 ----
              bt_mtt1.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mtt1.setFocusable(false);
              bt_mtt1.setFont(bt_mtt1.getFont().deriveFont(bt_mtt1.getFont().getStyle() | Font.BOLD));
              bt_mtt1.setText("@MTT1@");
              bt_mtt1.setBackground(Color.lightGray);
              bt_mtt1.setName("bt_mtt1");
              panel7.add(bt_mtt1);
              bt_mtt1.setBounds(330, 10, 116, 25);

              //---- label7 ----
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
              label7.setText("@LB17@");
              label7.setName("label7");
              panel7.add(label7);
              label7.setBounds(10, 45, 275, 20);

              //---- bt_mt17 ----
              bt_mt17.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt17.setFont(bt_mt17.getFont().deriveFont(bt_mt17.getFont().getStyle() | Font.BOLD));
              bt_mt17.setText("@MT17@");
              bt_mt17.setName("bt_mt17");
              bt_mt17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt17ActionPerformed(e);
                }
              });
              panel7.add(bt_mt17);
              bt_mt17.setBounds(330, 40, 116, 25);

              //---- label8 ----
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
              label8.setText("BANQUES");
              label8.setName("label8");
              panel7.add(label8);
              label8.setBounds(10, 75, 275, 20);

              //---- bt_mt05 ----
              bt_mt05.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt05.setFont(bt_mt05.getFont().deriveFont(bt_mt05.getFont().getStyle() | Font.BOLD));
              bt_mt05.setText("@MT05@");
              bt_mt05.setName("bt_mt05");
              bt_mt05.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt05ActionPerformed(e);
                }
              });
              panel7.add(bt_mt05);
              bt_mt05.setBounds(330, 70, 116, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel7);
            panel7.setBounds(12, 169, 458, 117);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("dettes"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- label16 ----
              label16.setText("@LB11@");
              label16.setName("label16");
              panel4.add(label16);
              label16.setBounds(15, 55, 275, 20);

              //---- bt_mt11 ----
              bt_mt11.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt11.setText("@MT11@");
              bt_mt11.setName("bt_mt11");
              bt_mt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt11ActionPerformed(e);
                }
              });
              panel4.add(bt_mt11);
              bt_mt11.setBounds(328, 53, 116, 25);

              //---- label17 ----
              label17.setText("@LB08@");
              label17.setName("label17");
              panel4.add(label17);
              label17.setBounds(15, 80, 275, 20);

              //---- bt_mt08 ----
              bt_mt08.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt08.setText("@MT08@");
              bt_mt08.setName("bt_mt08");
              bt_mt08.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt08ActionPerformed(e);
                }
              });
              panel4.add(bt_mt08);
              bt_mt08.setBounds(328, 78, 116, 25);

              //---- label18 ----
              label18.setText("@LB09@");
              label18.setName("label18");
              panel4.add(label18);
              label18.setBounds(15, 105, 275, 20);

              //---- bt_mt09 ----
              bt_mt09.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt09.setText("@MT09@");
              bt_mt09.setName("bt_mt09");
              bt_mt09.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt09ActionPerformed(e);
                }
              });
              panel4.add(bt_mt09);
              bt_mt09.setBounds(328, 103, 116, 25);

              //---- label19 ----
              label19.setText("@LB07@");
              label19.setName("label19");
              panel4.add(label19);
              label19.setBounds(15, 30, 275, 20);

              //---- bt_mt07 ----
              bt_mt07.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt07.setText("@MT07@");
              bt_mt07.setName("bt_mt07");
              bt_mt07.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt07ActionPerformed(e);
                }
              });
              panel4.add(bt_mt07);
              bt_mt07.setBounds(328, 28, 116, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel4);
            panel4.setBounds(477, 13, 458, 152);

            //======== panel8 ========
            {
              panel8.setOpaque(false);
              panel8.setName("panel8");
              panel8.setLayout(null);

              //---- label31 ----
              label31.setFont(label31.getFont().deriveFont(label31.getFont().getStyle() | Font.BOLD));
              label31.setText("TOTAL DES POSTES DETTES");
              label31.setName("label31");
              panel8.add(label31);
              label31.setBounds(15, 9, 275, 20);

              //---- bt_mtt3 ----
              bt_mtt3.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mtt3.setFont(bt_mtt3.getFont().deriveFont(bt_mtt3.getFont().getStyle() | Font.BOLD));
              bt_mtt3.setText("@MTT3@");
              bt_mtt3.setBackground(Color.lightGray);
              bt_mtt3.setName("bt_mtt3");
              panel8.add(bt_mtt3);
              bt_mtt3.setBounds(330, 5, 116, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel8);
            panel8.setBounds(477, 175, 458, 44);

            //======== panel5 ========
            {
              panel5.setBorder(new TitledBorder("Co\u00fbt salarial"));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- label20 ----
              label20.setText("@LB14@");
              label20.setName("label20");
              panel5.add(label20);
              label20.setBounds(14, 65, 275, 20);

              //---- bt_mt14 ----
              bt_mt14.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt14.setText("@MT14@");
              bt_mt14.setName("bt_mt14");
              bt_mt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt14ActionPerformed(e);
                }
              });
              panel5.add(bt_mt14);
              bt_mt14.setBounds(328, 61, 116, 25);

              //---- label21 ----
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setText("TOTAL COUT SALARIAL");
              label21.setName("label21");
              panel5.add(label21);
              label21.setBounds(14, 95, 275, 20);

              //---- bt_mtt4 ----
              bt_mtt4.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mtt4.setFont(bt_mtt4.getFont().deriveFont(bt_mtt4.getFont().getStyle() | Font.BOLD));
              bt_mtt4.setText("@MTT4@");
              bt_mtt4.setBackground(Color.lightGray);
              bt_mtt4.setName("bt_mtt4");
              panel5.add(bt_mtt4);
              bt_mtt4.setBounds(328, 91, 116, 25);

              //---- label22 ----
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setText("COUT SALARIAL MENSUEL");
              label22.setName("label22");
              panel5.add(label22);
              label22.setBounds(14, 120, 275, 20);

              //---- bt_mtt5 ----
              bt_mtt5.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mtt5.setFont(bt_mtt5.getFont().deriveFont(bt_mtt5.getFont().getStyle() | Font.BOLD));
              bt_mtt5.setText("@MTT5@");
              bt_mtt5.setName("bt_mtt5");
              panel5.add(bt_mtt5);
              bt_mtt5.setBounds(328, 116, 116, 25);

              //---- label23 ----
              label23.setText("@LB13@");
              label23.setName("label23");
              panel5.add(label23);
              label23.setBounds(14, 40, 275, 20);

              //---- bt_mt13 ----
              bt_mt13.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt13.setText("@MT13@");
              bt_mt13.setName("bt_mt13");
              bt_mt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt13ActionPerformed(e);
                }
              });
              panel5.add(bt_mt13);
              bt_mt13.setBounds(328, 36, 116, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel5);
            panel5.setBounds(480, 290, 458, 155);

            //======== panel6 ========
            {
              panel6.setBorder(new TitledBorder("Effectifs"));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- label24 ----
              label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD));
              label24.setText("COUT SALARIAL MOYEN/SALARIE");
              label24.setName("label24");
              panel6.add(label24);
              label24.setBounds(14, 67, 275, 20);

              //---- bt_mtt6 ----
              bt_mtt6.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mtt6.setFont(bt_mtt6.getFont().deriveFont(bt_mtt6.getFont().getStyle() | Font.BOLD));
              bt_mtt6.setText("@MTT6@");
              bt_mtt6.setName("bt_mtt6");
              panel6.add(bt_mtt6);
              bt_mtt6.setBounds(330, 65, 116, 25);

              //---- label27 ----
              label27.setText("@LB15@");
              label27.setName("label27");
              panel6.add(label27);
              label27.setBounds(14, 32, 275, 20);

              //---- bt_mt15w ----
              bt_mt15w.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt15w.setText("@MT15W@");
              bt_mt15w.setName("bt_mt15w");
              bt_mt15w.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt15wActionPerformed(e);
                }
              });
              panel6.add(bt_mt15w);
              bt_mt15w.setBounds(330, 30, 116, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel6);
            panel6.setBounds(480, 450, 458, 112);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Cr\u00e9ances"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- label10 ----
              label10.setText("Du par les clients");
              label10.setName("label10");
              panel3.add(label10);
              label10.setBounds(14, 60, 275, 20);

              //---- bt_mt10 ----
              bt_mt10.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt10.setText("@MT045@");
              bt_mt10.setName("bt_mt10");
              bt_mt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt10ActionPerformed(e);
                }
              });
              panel3.add(bt_mt10);
              bt_mt10.setBounds(328, 56, 116, 25);

              //---- label11 ----
              label11.setText("Dont retards");
              label11.setName("label11");
              panel3.add(label11);
              label11.setBounds(14, 85, 275, 20);

              //---- bt_mt102 ----
              bt_mt102.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt102.setText("@MT102@");
              bt_mt102.setBackground(Color.lightGray);
              bt_mt102.setName("bt_mt102");
              panel3.add(bt_mt102);
              bt_mt102.setBounds(328, 81, 116, 25);

              //---- label12 ----
              label12.setText("Nombre de jours C.A.");
              label12.setName("label12");
              panel3.add(label12);
              label12.setBounds(14, 110, 275, 20);

              //---- bt_mt103 ----
              bt_mt103.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt103.setText("@MT103@");
              bt_mt103.setBackground(Color.lightGray);
              bt_mt103.setName("bt_mt103");
              panel3.add(bt_mt103);
              bt_mt103.setBounds(328, 106, 116, 25);

              //---- bt_mt06 ----
              bt_mt06.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt06.setText("@MT06@");
              bt_mt06.setName("bt_mt06");
              bt_mt06.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt06ActionPerformed(e);
                }
              });
              panel3.add(bt_mt06);
              bt_mt06.setBounds(328, 31, 116, 25);

              //---- label9 ----
              label9.setText("Portefeuille");
              label9.setName("label9");
              panel3.add(label9);
              label9.setBounds(14, 35, 275, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(12, 290, 458, 155);

            //======== panel9 ========
            {
              panel9.setOpaque(false);
              panel9.setName("panel9");
              panel9.setLayout(null);

              //---- label13 ----
              label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
              label13.setText("TOTAL DES POSTES DE CREANCES");
              label13.setName("label13");
              panel9.add(label13);
              label13.setBounds(15, 15, 275, 20);

              //---- bt_mtt2 ----
              bt_mtt2.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mtt2.setFont(bt_mtt2.getFont().deriveFont(bt_mtt2.getFont().getStyle() | Font.BOLD));
              bt_mtt2.setText("@MTT2@");
              bt_mtt2.setName("bt_mtt2");
              bt_mtt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mtt2ActionPerformed(e);
                }
              });
              panel9.add(bt_mtt2);
              bt_mtt2.setBounds(335, 11, 116, 25);

              //---- label14 ----
              label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
              label14.setText("RESULTAT");
              label14.setName("label14");
              panel9.add(label14);
              label14.setBounds(15, 40, 275, 20);

              //---- bt_mt16 ----
              bt_mt16.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt16.setFont(bt_mt16.getFont().deriveFont(bt_mt16.getFont().getStyle() | Font.BOLD));
              bt_mt16.setText("@MT02@");
              bt_mt16.setName("bt_mt16");
              bt_mt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt16ActionPerformed(e);
                }
              });
              panel9.add(bt_mt16);
              bt_mt16.setBounds(335, 40, 116, 25);

              //---- label15 ----
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setText("@LB12@");
              label15.setName("label15");
              panel9.add(label15);
              label15.setBounds(15, 70, 275, 20);

              //---- bt_mt12 ----
              bt_mt12.setHorizontalAlignment(SwingConstants.RIGHT);
              bt_mt12.setFont(bt_mt12.getFont().deriveFont(bt_mt12.getFont().getStyle() | Font.BOLD));
              bt_mt12.setText("@MT12@");
              bt_mt12.setName("bt_mt12");
              bt_mt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_mt12ActionPerformed(e);
                }
              });
              panel9.add(bt_mt12);
              bt_mt12.setBounds(335, 70, 116, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel9.getComponentCount(); i++) {
                  Rectangle bounds = panel9.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel9.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel9.setMinimumSize(preferredSize);
                panel9.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel9);
            panel9.setBounds(12, 460, 458, 105);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 947, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel label32;
  private XRiTextField WSOC;
  private RiZoneSortie label33;
  private JPanel p_tete_droite;
  private JLabel label34;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel2;
  private JLabel label4;
  private JButton bt_mt02;
  private JLabel label5;
  private JButton bt_mt03;
  private JLabel label6;
  private JButton bt_mt04;
  private JButton bt_mt01;
  private JLabel label2;
  private JPanel panel7;
  private JLabel label3;
  private JButton bt_mtt1;
  private JLabel label7;
  private JButton bt_mt17;
  private JLabel label8;
  private JButton bt_mt05;
  private JPanel panel4;
  private JLabel label16;
  private JButton bt_mt11;
  private JLabel label17;
  private JButton bt_mt08;
  private JLabel label18;
  private JButton bt_mt09;
  private JLabel label19;
  private JButton bt_mt07;
  private JPanel panel8;
  private JLabel label31;
  private JButton bt_mtt3;
  private JPanel panel5;
  private JLabel label20;
  private JButton bt_mt14;
  private JLabel label21;
  private JButton bt_mtt4;
  private JLabel label22;
  private JButton bt_mtt5;
  private JLabel label23;
  private JButton bt_mt13;
  private JPanel panel6;
  private JLabel label24;
  private JButton bt_mtt6;
  private JLabel label27;
  private JButton bt_mt15w;
  private JPanel panel3;
  private JLabel label10;
  private JButton bt_mt10;
  private JLabel label11;
  private JButton bt_mt102;
  private JLabel label12;
  private JButton bt_mt103;
  private JButton bt_mt06;
  private JLabel label9;
  private JPanel panel9;
  private JLabel label13;
  private JButton bt_mtt2;
  private JLabel label14;
  private JButton bt_mt16;
  private JLabel label15;
  private JButton bt_mt12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
