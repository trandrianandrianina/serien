
package ri.serien.libecranrpg.stbm.STBM16FM;
// Nom Fichier: pop_STBM16FM_FMTB2_76.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STBM16FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "Soc", "Echéance", "LIBDEV", "Nbr", };
  private String[][] _T01_Data = { { "S01", "D01", "F01", "Q01", }, { "S02", "D02", "F02", "Q02", }, { "S03", "D03", "F03", "Q03", },
      { "S04", "D04", "F04", "Q04", }, { "S05", "D05", "F05", "Q05", }, { "S06", "D06", "F06", "Q06", }, { "S07", "D07", "F07", "Q07", },
      { "S08", "D08", "F08", "Q08", }, { "S09", "D09", "F09", "Q09", }, { "S10", "D10", "F10", "Q10", }, { "S11", "D11", "F11", "Q11", },
      { "S12", "D12", "F12", "Q12", }, { "S13", "D13", "F13", "Q13", }, { "S14", "D14", "F14", "Q14", }, { "S15", "D15", "F15", "Q15", }, };
  private int[] _T01_Width = { 28, 78, 114, 27, };
  private String[][] _LIST_Title_Data_Brut = null;
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.LEFT };
  
  public STBM16FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, _LIST_Justification, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(retour);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, _LIST_Title_Data_Brut, _T01_Top, _LIST_Justification);
    
    
    
    
    // TODO Icones
    OBJ_32.setIcon(lexique.chargerImage("images/impgra.gif", true));
    OBJ_31.setIcon(lexique.chargerImage("images/stat01.gif", true));
    retour.setIcon(lexique.chargerImage("images/retour.png", true));
    bt_UP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    bt_DOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Tableau de bord des achats"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void VALActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bt_UPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void bt_DOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_31 = new JButton();
    OBJ_32 = new JButton();
    OBJ_33 = new JButton();
    retour = new JButton();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    separator1 = compFactory.createSeparator("@LIBPOS@");
    bt_UP = new JButton();
    bt_DOWN = new JButton();
    MTTOT = new XRiTextField();
    OBJ_29 = new JLabel();

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //======== this ========
    setName("this");

    //---- OBJ_31 ----
    OBJ_31.setText("");
    OBJ_31.setToolTipText("Graphe");
    OBJ_31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_31.setName("OBJ_31");
    OBJ_31.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_31ActionPerformed(e);
      }
    });

    //---- OBJ_32 ----
    OBJ_32.setText("");
    OBJ_32.setToolTipText("Impression");
    OBJ_32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_32.setName("OBJ_32");

    //---- OBJ_33 ----
    OBJ_33.setText("");
    OBJ_33.setToolTipText("Exportation vers excel");
    OBJ_33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_33.setName("OBJ_33");
    OBJ_33.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_33ActionPerformed(e);
      }
    });

    //---- retour ----
    retour.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    retour.setName("retour");
    retour.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_30ActionPerformed(e);
      }
    });

    //======== SCROLLPANE_LIST ========
    {
      SCROLLPANE_LIST.setComponentPopupMenu(BTD);
      SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

      //---- T01 ----
      T01.setName("T01");
      SCROLLPANE_LIST.setViewportView(T01);
    }

    //---- separator1 ----
    separator1.setName("separator1");

    //---- bt_UP ----
    bt_UP.setText("");
    bt_UP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_UP.setName("bt_UP");
    bt_UP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_UPActionPerformed(e);
      }
    });

    //---- bt_DOWN ----
    bt_DOWN.setText("");
    bt_DOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_DOWN.setName("bt_DOWN");
    bt_DOWN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_DOWNActionPerformed(e);
      }
    });

    //---- MTTOT ----
    MTTOT.setComponentPopupMenu(BTD);
    MTTOT.setName("MTTOT");

    //---- OBJ_29 ----
    OBJ_29.setText("Total");
    OBJ_29.setName("OBJ_29");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(5, 5, 5)
          .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 500, GroupLayout.PREFERRED_SIZE)
          .addGap(10, 10, 10)
          .addGroup(layout.createParallelGroup()
            .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
            .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        .addGroup(layout.createSequentialGroup()
          .addGap(330, 330, 330)
          .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
          .addGap(5, 5, 5)
          .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
          .addGap(1, 1, 1)
          .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
          .addGap(1, 1, 1)
          .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
          .addGap(314, 314, 314)
          .addComponent(retour, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addGroup(layout.createParallelGroup()
            .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
              .addGap(20, 20, 20)
              .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
          .addGap(5, 5, 5)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(6, 6, 6)
              .addComponent(OBJ_29))
            .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(2, 2, 2)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(retour, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JButton OBJ_31;
  private JButton OBJ_32;
  private JButton OBJ_33;
  private JButton retour;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JComponent separator1;
  private JButton bt_UP;
  private JButton bt_DOWN;
  private XRiTextField MTTOT;
  private JLabel OBJ_29;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
