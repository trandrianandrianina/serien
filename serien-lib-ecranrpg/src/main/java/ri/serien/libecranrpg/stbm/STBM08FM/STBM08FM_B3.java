
package ri.serien.libecranrpg.stbm.STBM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class STBM08FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  private String[] _WTM01_Title = { "Mois", "TIT1", "TIT2", "TIT3", "TIT4", "TIT5", };
  private String[][] _WTM01_Data =
      { { "WTM01", "WT01", "WT13", "WT25", "WT37", "WT49", }, { "WTM02", "WT02", "WT14", "WT26", "WT38", "WT50", },
          { "WTM03", "WT03", "WT15", "WT27", "WT39", "WT51", }, { "WTM04", "WT04", "WT16", "WT28", "WT40", "WT52", },
          { "WTM05", "WT05", "WT17", "WT29", "WT41", "WT53", }, { "WTM06", "WT06", "WT18", "WT30", "WT42", "WT54", },
          { "WTM07", "WT07", "WT19", "WT31", "WT43", "WT55", }, { "WTM08", "WT08", "WT20", "WT32", "WT44", "WT56", },
          { "WTM09", "WT09", "WT21", "WT33", "WT45", "WT57", }, { "WTM10", "WT10", "WT22", "WT34", "WT46", "WT58", },
          { "WTM11", "WT11", "WT23", "WT35", "WT47", "WT59", }, { "WTM12", "WT12", "WT24", "WT36", "WT48", "WT60", }, };
  private int[] _WTM01_Width = { 69, 100, 100, 99, 100, 100, };
  
  public STBM08FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    WTM01.setAspectTable(null, _WTM01_Title, _WTM01_Data, _WTM01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    riSousMenu_bt6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution de @TIT1@ à @TIT5@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Compte @G1NCG@ - @G1LIB@ (en @LIBDEV@)")).trim());
    OBJ_56.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution sur @TIT1@")).trim());
    OBJ_57.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution sur @TIT2@")).trim());
    OBJ_58.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution sur @TIT3@")).trim());
    OBJ_59.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution sur @TIT4@")).trim());
    OBJ_60.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution sur @TIT5@")).trim());
    OBJ_43.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M01@")).trim());
    OBJ_44.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M02@")).trim());
    OBJ_45.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M03@")).trim());
    OBJ_46.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M04@")).trim());
    OBJ_47.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M05@")).trim());
    OBJ_48.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M06@")).trim());
    OBJ_49.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M07@")).trim());
    OBJ_50.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M08@")).trim());
    OBJ_51.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M09@")).trim());
    OBJ_52.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M10@")).trim());
    OBJ_53.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M11@")).trim());
    OBJ_54.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution pour les mois de @M12@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_35.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Graphe : évolution de @TIT1@ à @TIT5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // TODO setData spécifiques...
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/stats.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_43ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_51ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_54ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTM01 = new XRiTable();
    OBJ_33 = new JLabel();
    OBJ_56 = new JButton();
    OBJ_57 = new JButton();
    OBJ_58 = new JButton();
    OBJ_59 = new JButton();
    OBJ_60 = new JButton();
    OBJ_43 = new JButton();
    OBJ_44 = new JButton();
    OBJ_45 = new JButton();
    OBJ_46 = new JButton();
    OBJ_47 = new JButton();
    OBJ_48 = new JButton();
    OBJ_49 = new JButton();
    OBJ_50 = new JButton();
    OBJ_51 = new JButton();
    OBJ_52 = new JButton();
    OBJ_53 = new JButton();
    OBJ_54 = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();
    WSOC = new XRiTextField();
    BT_ChgSoc = new JButton();
    OBJ_40 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_35 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord g\u00e9n\u00e9ral");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Statistiques");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Graphe");
              riSousMenu_bt6.setToolTipText("Graphe : \u00e9volution de @TIT1@ \u00e0 @TIT5@");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WTM01 ----
            WTM01.setName("WTM01");
            SCROLLPANE_LIST.setViewportView(WTM01);
          }

          //---- OBJ_33 ----
          OBJ_33.setText("Compte @G1NCG@ - @G1LIB@ (en @LIBDEV@)");
          OBJ_33.setName("OBJ_33");

          //---- OBJ_56 ----
          OBJ_56.setText("");
          OBJ_56.setToolTipText("Graphe : \u00e9volution sur @TIT1@");
          OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_56.setName("OBJ_56");
          OBJ_56.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_56ActionPerformed(e);
            }
          });

          //---- OBJ_57 ----
          OBJ_57.setText("");
          OBJ_57.setToolTipText("Graphe : \u00e9volution sur @TIT2@");
          OBJ_57.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_57.setName("OBJ_57");
          OBJ_57.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_57ActionPerformed(e);
            }
          });

          //---- OBJ_58 ----
          OBJ_58.setText("");
          OBJ_58.setToolTipText("Graphe : \u00e9volution sur @TIT3@");
          OBJ_58.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_58.setName("OBJ_58");
          OBJ_58.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_58ActionPerformed(e);
            }
          });

          //---- OBJ_59 ----
          OBJ_59.setText("");
          OBJ_59.setToolTipText("Graphe : \u00e9volution sur @TIT4@");
          OBJ_59.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_59.setName("OBJ_59");
          OBJ_59.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_59ActionPerformed(e);
            }
          });

          //---- OBJ_60 ----
          OBJ_60.setText("");
          OBJ_60.setToolTipText("Graphe : \u00e9volution sur @TIT5@");
          OBJ_60.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_60.setName("OBJ_60");
          OBJ_60.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_60ActionPerformed(e);
            }
          });

          //---- OBJ_43 ----
          OBJ_43.setText("");
          OBJ_43.setToolTipText("Graphe : \u00e9volution pour les mois de @M01@");
          OBJ_43.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_43.setName("OBJ_43");
          OBJ_43.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_43ActionPerformed(e);
            }
          });

          //---- OBJ_44 ----
          OBJ_44.setText("");
          OBJ_44.setToolTipText("Graphe : \u00e9volution pour les mois de @M02@");
          OBJ_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_44.setName("OBJ_44");
          OBJ_44.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_44ActionPerformed(e);
            }
          });

          //---- OBJ_45 ----
          OBJ_45.setText("");
          OBJ_45.setToolTipText("Graphe : \u00e9volution pour les mois de @M03@");
          OBJ_45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_45.setName("OBJ_45");
          OBJ_45.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_45ActionPerformed(e);
            }
          });

          //---- OBJ_46 ----
          OBJ_46.setText("");
          OBJ_46.setToolTipText("Graphe : \u00e9volution pour les mois de @M04@");
          OBJ_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_46.setName("OBJ_46");
          OBJ_46.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_46ActionPerformed(e);
            }
          });

          //---- OBJ_47 ----
          OBJ_47.setText("");
          OBJ_47.setToolTipText("Graphe : \u00e9volution pour les mois de @M05@");
          OBJ_47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_47.setName("OBJ_47");
          OBJ_47.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_47ActionPerformed(e);
            }
          });

          //---- OBJ_48 ----
          OBJ_48.setText("");
          OBJ_48.setToolTipText("Graphe : \u00e9volution pour les mois de @M06@");
          OBJ_48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_48.setName("OBJ_48");
          OBJ_48.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_48ActionPerformed(e);
            }
          });

          //---- OBJ_49 ----
          OBJ_49.setText("");
          OBJ_49.setToolTipText("Graphe : \u00e9volution pour les mois de @M07@");
          OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_49.setName("OBJ_49");
          OBJ_49.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_49ActionPerformed(e);
            }
          });

          //---- OBJ_50 ----
          OBJ_50.setText("");
          OBJ_50.setToolTipText("Graphe : \u00e9volution pour les mois de @M08@");
          OBJ_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_50.setName("OBJ_50");
          OBJ_50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_50ActionPerformed(e);
            }
          });

          //---- OBJ_51 ----
          OBJ_51.setText("");
          OBJ_51.setToolTipText("Graphe : \u00e9volution pour les mois de @M09@");
          OBJ_51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_51.setName("OBJ_51");
          OBJ_51.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_51ActionPerformed(e);
            }
          });

          //---- OBJ_52 ----
          OBJ_52.setText("");
          OBJ_52.setToolTipText("Graphe : \u00e9volution pour les mois de @M10@");
          OBJ_52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_52.setName("OBJ_52");
          OBJ_52.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_52ActionPerformed(e);
            }
          });

          //---- OBJ_53 ----
          OBJ_53.setText("");
          OBJ_53.setToolTipText("Graphe : \u00e9volution pour les mois de @M11@");
          OBJ_53.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_53.setName("OBJ_53");
          OBJ_53.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_53ActionPerformed(e);
            }
          });

          //---- OBJ_54 ----
          OBJ_54.setText("");
          OBJ_54.setToolTipText("Graphe : \u00e9volution pour les mois de @M12@");
          OBJ_54.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_54.setName("OBJ_54");
          OBJ_54.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_54ActionPerformed(e);
            }
          });

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Societ\u00e9");
          xTitledSeparator1.setName("xTitledSeparator1");

          //---- WSOC ----
          WSOC.setComponentPopupMenu(BTD);
          WSOC.setName("WSOC");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          //---- OBJ_40 ----
          OBJ_40.setText("@DGNOM@");
          OBJ_40.setForeground(Color.gray);
          OBJ_40.setFont(OBJ_40.getFont().deriveFont(OBJ_40.getFont().getStyle() | Font.BOLD));
          OBJ_40.setName("OBJ_40");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 855, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                    .addGap(43, 43, 43)
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 710, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(150, 150, 150)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                    .addGap(84, 84, 84)
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                    .addGap(84, 84, 84)
                    .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                    .addGap(84, 84, 84)
                    .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                    .addGap(84, 84, 84)
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(31, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(105, 105, 105)
                    .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(169, 169, 169)
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(153, 153, 153)
                    .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(41, 41, 41)
                    .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(89, 89, 89)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(201, 201, 201)
                    .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(57, 57, 57)
                    .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(137, 137, 137)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(121, 121, 121)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(73, 73, 73)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(185, 185, 185)
                    .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(197, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_35 ----
    OBJ_35.setText("");
    OBJ_35.setToolTipText("Graphe : \u00e9volution de @TIT1@ \u00e0 @TIT5@");
    OBJ_35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_35.setName("OBJ_35");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTM01;
  private JLabel OBJ_33;
  private JButton OBJ_56;
  private JButton OBJ_57;
  private JButton OBJ_58;
  private JButton OBJ_59;
  private JButton OBJ_60;
  private JButton OBJ_43;
  private JButton OBJ_44;
  private JButton OBJ_45;
  private JButton OBJ_46;
  private JButton OBJ_47;
  private JButton OBJ_48;
  private JButton OBJ_49;
  private JButton OBJ_50;
  private JButton OBJ_51;
  private JButton OBJ_52;
  private JButton OBJ_53;
  private JButton OBJ_54;
  private JXTitledSeparator xTitledSeparator1;
  private XRiTextField WSOC;
  private JButton BT_ChgSoc;
  private JLabel OBJ_40;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JButton OBJ_35;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
