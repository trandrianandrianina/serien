
package ri.serien.libecranrpg.stbm.STBM09FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

public class STBM09FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  private String[] _S01_Title = { "Soc", "Etb", "Mg", "Libellé", "LIBDEV", };
  private String[][] _S01_Data =
      { { "S01", "E01", "M01", "LE01", "F01", }, { "S02", "E02", "M02", "LE02", "F02", }, { "S03", "E03", "M03", "LE03", "F03", },
          { "S04", "E04", "M04", "LE04", "F04", }, { "S05", "E05", "M05", "LE05", "F05", }, { "S06", "E06", "M06", "LE06", "F06", },
          { "S07", "E07", "M07", "LE07", "F07", }, { "S08", "E08", "M08", "LE08", "F08", }, { "S09", "E09", "M09", "LE09", "F09", },
          { "S10", "E10", "M10", "LE10", "F10", }, { "S11", "E11", "M11", "LE11", "F11", }, { "S12", "E12", "M12", "LE12", "F12", },
          { "S13", "E13", "M13", "LE13", "F13", }, { "S14", "E14", "M14", "LE14", "F14", }, { "S15", "E15", "M15", "LE15", "F15", }, };
  private int[] _S01_Width = { 25, 25, 19, 217, 129, };
  // private String[] _LIST_Top=null;
  private int[] _LIST_Justification =
      { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT };
  
  public STBM09FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    S01.setAspectTable(null, _S01_Title, _S01_Data, _S01_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top, _LIST_Justification);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    
    

    
    
    riMenu_bt4.setIcon(lexique.chargerImage("images/stats.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void S01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _LIST_Top, "1", "ENTER", e);
    if (S01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _LIST_Top, "1", "Enter");
    S01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _LIST_Top, "2", "Enter");
    S01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_UPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void bt_DOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_26 = new JLabel();
    MTTOT = new XRiTextField();
    separator1 = compFactory.createSeparator("@LIBPOS@");
    SCROLLPANE_LIST1 = new JScrollPane();
    S01 = new XRiTable();
    bt_UP = new JButton();
    bt_DOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_11 = new JMenuItem();
    separator2 = new JSeparator();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(840, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu4 ========
          {
            riMenu4.setName("riMenu4");

            //---- riMenu_bt4 ----
            riMenu_bt4.setText("Statistiques");
            riMenu_bt4.setName("riMenu_bt4");
            riMenu4.add(riMenu_bt4);
          }
          menus_haut.add(riMenu4);

          //======== riSousMenu18 ========
          {
            riSousMenu18.setName("riSousMenu18");

            //---- riSousMenu_bt18 ----
            riSousMenu_bt18.setText("Comparaison");
            riSousMenu_bt18.setName("riSousMenu_bt18");
            riSousMenu_bt18.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt18ActionPerformed(e);
              }
            });
            riSousMenu18.add(riSousMenu_bt18);
          }
          menus_haut.add(riSousMenu18);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_26 ----
          OBJ_26.setText("Total");
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(415, 321, 45, 16);

          //---- MTTOT ----
          MTTOT.setComponentPopupMenu(BTD);
          MTTOT.setName("MTTOT");
          p_recup.add(MTTOT);
          MTTOT.setBounds(467, 315, 133, MTTOT.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          p_recup.add(separator1);
          separator1.setBounds(5, 15, 635, separator1.getPreferredSize().height);

          //======== SCROLLPANE_LIST1 ========
          {
            SCROLLPANE_LIST1.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

            //---- S01 ----
            S01.setComponentPopupMenu(BTD);
            S01.setName("S01");
            S01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                S01MouseClicked(e);
                S01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST1.setViewportView(S01);
          }
          p_recup.add(SCROLLPANE_LIST1);
          SCROLLPANE_LIST1.setBounds(15, 40, 585, 269);

          //---- bt_UP ----
          bt_UP.setText("");
          bt_UP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_UP.setName("bt_UP");
          bt_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_UPActionPerformed(e);
            }
          });
          p_recup.add(bt_UP);
          bt_UP.setBounds(615, 40, 25, 125);

          //---- bt_DOWN ----
          bt_DOWN.setText("");
          bt_DOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_DOWN.setName("bt_DOWN");
          bt_DOWN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_DOWNActionPerformed(e);
            }
          });
          p_recup.add(bt_DOWN);
          bt_DOWN.setBounds(615, 185, 25, 125);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(5, 5, 660, 355);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Tous les bons");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_11 ----
      OBJ_11.setText("Bons avec date de livraison ult\u00e9rieure au mois en cours");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- separator2 ----
      separator2.setName("separator2");
      BTD.add(separator2);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_26;
  private XRiTextField MTTOT;
  private JComponent separator1;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable S01;
  private JButton bt_UP;
  private JButton bt_DOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_11;
  private JSeparator separator2;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
