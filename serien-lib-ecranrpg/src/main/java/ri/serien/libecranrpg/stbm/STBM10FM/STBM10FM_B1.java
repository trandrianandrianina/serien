
package ri.serien.libecranrpg.stbm.STBM10FM;
// Nom Fichier: pop_STBM10FM_FMTB1_60.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STBM10FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", };
  private String[] _T01_Title = { "Soc", "Comptes", "E", "LIBDEV", };
  private String[][] _T01_Data = { { "S01", "L01", "E01", "B01", }, { "S02", "L02", "E02", "B02", }, { "S03", "L03", "E03", "B03", },
      { "S04", "L04", "E04", "B04", }, { "S05", "L05", "E05", "B05", }, { "S06", "L06", "E06", "B06", }, { "S07", "L07", "E07", "B07", },
      { "S08", "L08", "E08", "B08", }, { "S09", "L09", "E09", "B09", }, { "S10", "L10", "E10", "B10", }, { "S11", "L11", "E11", "B11", },
      { "S12", "L12", "E12", "B12", }, };
  private int[] _T01_Width = { 27, 142, 15, 118, };
  private String[][] _LIST_Title_Data_Brut = null;
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT };
  
  public STBM10FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, _LIST_Justification, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(OBJ_36);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xTitledSeparator1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, _LIST_Title_Data_Brut, _T01_Top, _LIST_Justification);
    
    
    
    
    // TODO Icones
    OBJ_34.setIcon(lexique.chargerImage("images/stat01.gif", true));
    OBJ_36.setIcon(lexique.chargerImage("images/retour.png", true));
    bt_UP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    bt_DOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Synthèse"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "Enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "ENTER", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_UPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void bt_DOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_34 = new JButton();
    OBJ_36 = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    MTTOT = new XRiTextField();
    OBJ_30 = new JLabel();
    bt_UP = new JButton();
    bt_DOWN = new JButton();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMaximumSize(new Dimension(460, 350));
    setName("this");

    //---- OBJ_34 ----
    OBJ_34.setText("");
    OBJ_34.setToolTipText("Graphe");
    OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_34.setName("OBJ_34");
    OBJ_34.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_34ActionPerformed(e);
      }
    });

    //---- OBJ_36 ----
    OBJ_36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_36.setName("OBJ_36");
    OBJ_36.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_36ActionPerformed(e);
      }
    });

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("@LIBPOS@");
    xTitledSeparator1.setName("xTitledSeparator1");

    //======== SCROLLPANE_LIST ========
    {
      SCROLLPANE_LIST.setComponentPopupMenu(BTD);
      SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

      //---- T01 ----
      T01.setComponentPopupMenu(BTD);
      T01.setName("T01");
      T01.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          T01MouseClicked(e);
        }
      });
      SCROLLPANE_LIST.setViewportView(T01);
    }

    //---- MTTOT ----
    MTTOT.setComponentPopupMenu(BTD);
    MTTOT.setName("MTTOT");

    //---- OBJ_30 ----
    OBJ_30.setText("Total");
    OBJ_30.setName("OBJ_30");

    //---- bt_UP ----
    bt_UP.setText("");
    bt_UP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_UP.setName("bt_UP");
    bt_UP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_UPActionPerformed(e);
      }
    });

    //---- bt_DOWN ----
    bt_DOWN.setText("");
    bt_DOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_DOWN.setName("bt_DOWN");
    bt_DOWN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_DOWNActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)
          .addGap(10, 10, 10)
          .addGroup(layout.createParallelGroup()
            .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
            .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        .addGroup(layout.createSequentialGroup()
          .addGap(335, 335, 335)
          .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
          .addGap(10, 10, 10)
          .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(18, 18, 18)
          .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
          .addGap(430, 430, 430)
          .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(11, 11, 11)
          .addGroup(layout.createParallelGroup()
            .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
              .addGap(20, 20, 20)
              .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
          .addGap(5, 5, 5)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(6, 6, 6)
              .addComponent(OBJ_30))
            .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(7, 7, 7)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton OBJ_34;
  private JButton OBJ_36;
  private JXTitledSeparator xTitledSeparator1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private XRiTextField MTTOT;
  private JLabel OBJ_30;
  private JButton bt_UP;
  private JButton bt_DOWN;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
