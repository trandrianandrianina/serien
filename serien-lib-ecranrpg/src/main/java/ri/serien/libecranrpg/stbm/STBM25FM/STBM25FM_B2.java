
package ri.serien.libecranrpg.stbm.STBM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STBM25FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STBM25FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    LD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02@")).trim());
    LB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT01@")).trim());
    LB6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02@")).trim());
    LB7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04@")).trim());
    LB8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT03@")).trim());
    LB9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05@")).trim());
    LB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT04@")).trim());
    LB11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06@")).trim());
    LB12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT05@")).trim());
    LB13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07@")).trim());
    LB15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08@")).trim());
    LB16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT07@")).trim());
    LB17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09@")).trim());
    LB18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT08@")).trim());
    LB19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11@")).trim());
    LB20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT09@")).trim());
    LB21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10@")).trim());
    LB22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT11@")).trim());
    LB23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT10@")).trim());
    LD3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01@")).trim());
    NBCL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL01@")).trim());
    NBCL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL02@")).trim());
    NBCL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL03@")).trim());
    NBCL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL04@")).trim());
    NBCL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL05@")).trim());
    NBCL06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL06@")).trim());
    NBCL07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL07@")).trim());
    NBCL08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL08@")).trim());
    NBCL09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL09@")).trim());
    NBCL10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL10@")).trim());
    NBCL11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL11@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void LB01ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LD02 = new JButton();
    LB4 = new RiZoneSortie();
    LB5 = new JButton();
    LB6 = new RiZoneSortie();
    LB7 = new JButton();
    LB8 = new RiZoneSortie();
    LB9 = new JButton();
    LB10 = new RiZoneSortie();
    LB11 = new JButton();
    LB12 = new RiZoneSortie();
    LB13 = new JButton();
    LB14 = new RiZoneSortie();
    LB15 = new JButton();
    LB16 = new RiZoneSortie();
    LB17 = new JButton();
    LB18 = new RiZoneSortie();
    LB19 = new JButton();
    LB20 = new RiZoneSortie();
    LB21 = new JButton();
    LB22 = new RiZoneSortie();
    LB23 = new RiZoneSortie();
    LD3 = new JButton();
    NBCL01 = new RiZoneSortie();
    NBCL02 = new RiZoneSortie();
    NBCL03 = new RiZoneSortie();
    NBCL04 = new RiZoneSortie();
    NBCL05 = new RiZoneSortie();
    NBCL06 = new RiZoneSortie();
    NBCL07 = new RiZoneSortie();
    NBCL08 = new RiZoneSortie();
    NBCL09 = new RiZoneSortie();
    NBCL10 = new RiZoneSortie();
    NBCL11 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- LD02 ----
            LD02.setText("@LD02@");
            LD02.setHorizontalAlignment(SwingConstants.LEFT);
            LD02.setBackground(new Color(239, 239, 222));
            LD02.setName("LD02");
            LD02.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LD02);
            LD02.setBounds(20, 65, 330, LD02.getPreferredSize().height);

            //---- LB4 ----
            LB4.setText("@MT01@");
            LB4.setHorizontalAlignment(SwingConstants.RIGHT);
            LB4.setName("LB4");
            panel1.add(LB4);
            LB4.setBounds(455, 30, 165, LB4.getPreferredSize().height);

            //---- LB5 ----
            LB5.setText("@LD03");
            LB5.setHorizontalAlignment(SwingConstants.LEFT);
            LB5.setBackground(new Color(239, 239, 222));
            LB5.setName("LB5");
            LB5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB5);
            LB5.setBounds(20, 100, 330, LB5.getPreferredSize().height);

            //---- LB6 ----
            LB6.setText("@MT02@");
            LB6.setHorizontalAlignment(SwingConstants.RIGHT);
            LB6.setName("LB6");
            panel1.add(LB6);
            LB6.setBounds(455, 65, 165, LB6.getPreferredSize().height);

            //---- LB7 ----
            LB7.setText("@LD04@");
            LB7.setHorizontalAlignment(SwingConstants.LEFT);
            LB7.setBackground(new Color(239, 239, 222));
            LB7.setName("LB7");
            LB7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB7);
            LB7.setBounds(20, 140, 330, LB7.getPreferredSize().height);

            //---- LB8 ----
            LB8.setText("@MT03@");
            LB8.setHorizontalAlignment(SwingConstants.RIGHT);
            LB8.setName("LB8");
            panel1.add(LB8);
            LB8.setBounds(455, 105, 165, LB8.getPreferredSize().height);

            //---- LB9 ----
            LB9.setText("@LD05@");
            LB9.setHorizontalAlignment(SwingConstants.LEFT);
            LB9.setBackground(new Color(239, 239, 222));
            LB9.setName("LB9");
            LB9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB9);
            LB9.setBounds(20, 175, 330, LB9.getPreferredSize().height);

            //---- LB10 ----
            LB10.setText("@MT04@");
            LB10.setHorizontalAlignment(SwingConstants.RIGHT);
            LB10.setName("LB10");
            panel1.add(LB10);
            LB10.setBounds(455, 140, 165, LB10.getPreferredSize().height);

            //---- LB11 ----
            LB11.setText("@LD06@");
            LB11.setHorizontalAlignment(SwingConstants.LEFT);
            LB11.setBackground(new Color(239, 239, 222));
            LB11.setName("LB11");
            LB11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB11);
            LB11.setBounds(20, 215, 330, LB11.getPreferredSize().height);

            //---- LB12 ----
            LB12.setText("@MT05@");
            LB12.setHorizontalAlignment(SwingConstants.RIGHT);
            LB12.setName("LB12");
            panel1.add(LB12);
            LB12.setBounds(455, 180, 165, LB12.getPreferredSize().height);

            //---- LB13 ----
            LB13.setText("@LD07@");
            LB13.setHorizontalAlignment(SwingConstants.LEFT);
            LB13.setBackground(new Color(239, 239, 222));
            LB13.setName("LB13");
            LB13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB13);
            LB13.setBounds(20, 250, 330, LB13.getPreferredSize().height);

            //---- LB14 ----
            LB14.setText("@MT06");
            LB14.setHorizontalAlignment(SwingConstants.RIGHT);
            LB14.setName("LB14");
            panel1.add(LB14);
            LB14.setBounds(455, 215, 165, LB14.getPreferredSize().height);

            //---- LB15 ----
            LB15.setText("@LD08@");
            LB15.setHorizontalAlignment(SwingConstants.LEFT);
            LB15.setBackground(new Color(239, 239, 222));
            LB15.setName("LB15");
            LB15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB15);
            LB15.setBounds(20, 285, 330, LB15.getPreferredSize().height);

            //---- LB16 ----
            LB16.setText("@MT07@");
            LB16.setHorizontalAlignment(SwingConstants.RIGHT);
            LB16.setName("LB16");
            panel1.add(LB16);
            LB16.setBounds(455, 250, 165, LB16.getPreferredSize().height);

            //---- LB17 ----
            LB17.setText("@LD09@");
            LB17.setHorizontalAlignment(SwingConstants.LEFT);
            LB17.setBackground(new Color(239, 239, 222));
            LB17.setName("LB17");
            LB17.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB17);
            LB17.setBounds(20, 325, 330, LB17.getPreferredSize().height);

            //---- LB18 ----
            LB18.setText("@MT08@");
            LB18.setHorizontalAlignment(SwingConstants.RIGHT);
            LB18.setName("LB18");
            panel1.add(LB18);
            LB18.setBounds(455, 290, 165, LB18.getPreferredSize().height);

            //---- LB19 ----
            LB19.setText("@LD11@");
            LB19.setHorizontalAlignment(SwingConstants.LEFT);
            LB19.setBackground(new Color(239, 239, 222));
            LB19.setName("LB19");
            LB19.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB19);
            LB19.setBounds(20, 400, 330, LB19.getPreferredSize().height);

            //---- LB20 ----
            LB20.setText("@MT09@");
            LB20.setHorizontalAlignment(SwingConstants.RIGHT);
            LB20.setName("LB20");
            panel1.add(LB20);
            LB20.setBounds(455, 325, 165, LB20.getPreferredSize().height);

            //---- LB21 ----
            LB21.setText("@LD10@");
            LB21.setHorizontalAlignment(SwingConstants.LEFT);
            LB21.setBackground(new Color(239, 239, 222));
            LB21.setName("LB21");
            LB21.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB21);
            LB21.setBounds(20, 360, 330, LB21.getPreferredSize().height);

            //---- LB22 ----
            LB22.setText("@MT11@");
            LB22.setHorizontalAlignment(SwingConstants.RIGHT);
            LB22.setName("LB22");
            panel1.add(LB22);
            LB22.setBounds(455, 400, 165, LB22.getPreferredSize().height);

            //---- LB23 ----
            LB23.setText("@MT10@");
            LB23.setHorizontalAlignment(SwingConstants.RIGHT);
            LB23.setName("LB23");
            panel1.add(LB23);
            LB23.setBounds(455, 365, 165, LB23.getPreferredSize().height);

            //---- LD3 ----
            LD3.setText("@LD01@");
            LD3.setHorizontalAlignment(SwingConstants.LEFT);
            LD3.setBackground(new Color(239, 239, 222));
            LD3.setName("LD3");
            LD3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LD3);
            LD3.setBounds(20, 30, 330, LD3.getPreferredSize().height);

            //---- NBCL01 ----
            NBCL01.setText("@NBCL01@");
            NBCL01.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL01.setName("NBCL01");
            panel1.add(NBCL01);
            NBCL01.setBounds(370, 30, 68, 24);

            //---- NBCL02 ----
            NBCL02.setText("@NBCL02@");
            NBCL02.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL02.setName("NBCL02");
            panel1.add(NBCL02);
            NBCL02.setBounds(370, 65, 68, NBCL02.getPreferredSize().height);

            //---- NBCL03 ----
            NBCL03.setText("@NBCL03@");
            NBCL03.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL03.setName("NBCL03");
            panel1.add(NBCL03);
            NBCL03.setBounds(370, 105, 68, NBCL03.getPreferredSize().height);

            //---- NBCL04 ----
            NBCL04.setText("@NBCL04@");
            NBCL04.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL04.setName("NBCL04");
            panel1.add(NBCL04);
            NBCL04.setBounds(370, 140, 68, NBCL04.getPreferredSize().height);

            //---- NBCL05 ----
            NBCL05.setText("@NBCL05@");
            NBCL05.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL05.setName("NBCL05");
            panel1.add(NBCL05);
            NBCL05.setBounds(370, 180, 68, NBCL05.getPreferredSize().height);

            //---- NBCL06 ----
            NBCL06.setText("@NBCL06@");
            NBCL06.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL06.setName("NBCL06");
            panel1.add(NBCL06);
            NBCL06.setBounds(370, 215, 68, NBCL06.getPreferredSize().height);

            //---- NBCL07 ----
            NBCL07.setText("@NBCL07@");
            NBCL07.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL07.setName("NBCL07");
            panel1.add(NBCL07);
            NBCL07.setBounds(370, 250, 68, NBCL07.getPreferredSize().height);

            //---- NBCL08 ----
            NBCL08.setText("@NBCL08@");
            NBCL08.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL08.setName("NBCL08");
            panel1.add(NBCL08);
            NBCL08.setBounds(370, 290, 68, NBCL08.getPreferredSize().height);

            //---- NBCL09 ----
            NBCL09.setText("@NBCL09@");
            NBCL09.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL09.setName("NBCL09");
            panel1.add(NBCL09);
            NBCL09.setBounds(370, 325, 68, NBCL09.getPreferredSize().height);

            //---- NBCL10 ----
            NBCL10.setText("@NBCL10@");
            NBCL10.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL10.setName("NBCL10");
            panel1.add(NBCL10);
            NBCL10.setBounds(370, 365, 68, NBCL10.getPreferredSize().height);

            //---- NBCL11 ----
            NBCL11.setText("@NBCL11@");
            NBCL11.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL11.setName("NBCL11");
            panel1.add(NBCL11);
            NBCL11.setBounds(370, 400, 68, NBCL11.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 880, 550);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton LD02;
  private RiZoneSortie LB4;
  private JButton LB5;
  private RiZoneSortie LB6;
  private JButton LB7;
  private RiZoneSortie LB8;
  private JButton LB9;
  private RiZoneSortie LB10;
  private JButton LB11;
  private RiZoneSortie LB12;
  private JButton LB13;
  private RiZoneSortie LB14;
  private JButton LB15;
  private RiZoneSortie LB16;
  private JButton LB17;
  private RiZoneSortie LB18;
  private JButton LB19;
  private RiZoneSortie LB20;
  private JButton LB21;
  private RiZoneSortie LB22;
  private RiZoneSortie LB23;
  private JButton LD3;
  private RiZoneSortie NBCL01;
  private RiZoneSortie NBCL02;
  private RiZoneSortie NBCL03;
  private RiZoneSortie NBCL04;
  private RiZoneSortie NBCL05;
  private RiZoneSortie NBCL06;
  private RiZoneSortie NBCL07;
  private RiZoneSortie NBCL08;
  private RiZoneSortie NBCL09;
  private RiZoneSortie NBCL10;
  private RiZoneSortie NBCL11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
