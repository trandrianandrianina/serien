
package ri.serien.libecranrpg.stbm.STBM15FM;
// Nom Fichier: i_STBM08FM_FMTB1_FMTF1_50.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STBM15FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LB01_Title = { "Libellé", "Date", "Début", "Fin", "Temps", };
  private String[][] _LB01_Data = { { "LB01", "D01", "H01", "H201", "P01", }, { "LB02", "D02", "H02", "H202", "P02", },
      { "LB03", "D03", "H03", "H203", "P03", }, { "LB04", "D04", "H04", "H204", "P04", }, { "LB05", "D05", "H05", "H205", "P05", },
      { "LB06", "D06", "H06", "H206", "P06", }, { "LB07", "D07", "H07", "H207", "P07", }, { "LB08", "D08", "H08", "H208", "P08", },
      { "LB09", "D09", "H09", "H209", "P09", }, { "LB10", "D10", "H10", "H210", "P10", }, { "LB11", "D11", "H11", "H211", "P11", }, };
  private int[] _LB01_Width = { 279, 73, 63, 64, 78, };
  
  public STBM15FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    C11.setValeursSelection("X", " ");
    C10.setValeursSelection("X", " ");
    C09.setValeursSelection("X", " ");
    C08.setValeursSelection("X", " ");
    C07.setValeursSelection("X", " ");
    C06.setValeursSelection("X", " ");
    C05.setValeursSelection("X", " ");
    C04.setValeursSelection("X", " ");
    C03.setValeursSelection("X", " ");
    C02.setValeursSelection("X", " ");
    C01.setValeursSelection("X", " ");
    T01.setValeursSelection("X", " ");
    T11.setValeursSelection("X", " ");
    T10.setValeursSelection("X", " ");
    T09.setValeursSelection("X", " ");
    T08.setValeursSelection("X", " ");
    T07.setValeursSelection("X", " ");
    T06.setValeursSelection("X", " ");
    T05.setValeursSelection("X", " ");
    T04.setValeursSelection("X", " ");
    T03.setValeursSelection("X", " ");
    T02.setValeursSelection("X", " ");
    LB01.setAspectTable(null, _LB01_Title, _LB01_Data, _LB01_Width, false, null, null, null, null);
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // C11.setSelected(lexique.HostFieldGetData("C11").equalsIgnoreCase("X"));
    // C10.setSelected(lexique.HostFieldGetData("C10").equalsIgnoreCase("X"));
    // C09.setSelected(lexique.HostFieldGetData("C09").equalsIgnoreCase("X"));
    // C08.setSelected(lexique.HostFieldGetData("C08").equalsIgnoreCase("X"));
    // C07.setSelected(lexique.HostFieldGetData("C07").equalsIgnoreCase("X"));
    // C06.setSelected(lexique.HostFieldGetData("C06").equalsIgnoreCase("X"));
    // C05.setSelected(lexique.HostFieldGetData("C05").equalsIgnoreCase("X"));
    // C04.setSelected(lexique.HostFieldGetData("C04").equalsIgnoreCase("X"));
    // C03.setSelected(lexique.HostFieldGetData("C03").equalsIgnoreCase("X"));
    // C02.setSelected(lexique.HostFieldGetData("C02").equalsIgnoreCase("X"));
    // C01.setSelected(lexique.HostFieldGetData("C01").equalsIgnoreCase("X"));
    WSOC.setVisible(lexique.isPresent("WSOC"));
    // T01.setSelected(lexique.HostFieldGetData("T01").equalsIgnoreCase("X"));
    // T02.setSelected(lexique.HostFieldGetData("T02").equalsIgnoreCase("X"));
    // T03.setSelected(lexique.HostFieldGetData("T03").equalsIgnoreCase("X"));
    // T04.setSelected(lexique.HostFieldGetData("T04").equalsIgnoreCase("X"));
    // T05.setSelected(lexique.HostFieldGetData("T05").equalsIgnoreCase("X"));
    // T06.setSelected(lexique.HostFieldGetData("T06").equalsIgnoreCase("X"));
    // T07.setSelected(lexique.HostFieldGetData("T07").equalsIgnoreCase("X"));
    // T08.setSelected(lexique.HostFieldGetData("T08").equalsIgnoreCase("X"));
    // T09.setSelected(lexique.HostFieldGetData("T09").equalsIgnoreCase("X"));
    // T10.setSelected(lexique.HostFieldGetData("T10").equalsIgnoreCase("X"));
    // T11.setSelected(lexique.HostFieldGetData("T11").equalsIgnoreCase("X"));
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    OBJ_68.setIcon(lexique.chargerImage("images/chart_accept.png", true));
    OBJ_67.setIcon(lexique.chargerImage("images/chart_accept.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WSOC.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (C11.isSelected())
    // lexique.HostFieldPutData("C11", 0, "X");
    // else
    // lexique.HostFieldPutData("C11", 0, " ");
    // if (C10.isSelected())
    // lexique.HostFieldPutData("C10", 0, "X");
    // else
    // lexique.HostFieldPutData("C10", 0, " ");
    // if (C09.isSelected())
    // lexique.HostFieldPutData("C09", 0, "X");
    // else
    // lexique.HostFieldPutData("C09", 0, " ");
    // if (C08.isSelected())
    // lexique.HostFieldPutData("C08", 0, "X");
    // else
    // lexique.HostFieldPutData("C08", 0, " ");
    // if (C07.isSelected())
    // lexique.HostFieldPutData("C07", 0, "X");
    // else
    // lexique.HostFieldPutData("C07", 0, " ");
    // if (C06.isSelected())
    // lexique.HostFieldPutData("C06", 0, "X");
    // else
    // lexique.HostFieldPutData("C06", 0, " ");
    // if (C05.isSelected())
    // lexique.HostFieldPutData("C05", 0, "X");
    // else
    // lexique.HostFieldPutData("C05", 0, " ");
    // if (C04.isSelected())
    // lexique.HostFieldPutData("C04", 0, "X");
    // else
    // lexique.HostFieldPutData("C04", 0, " ");
    // if (C03.isSelected())
    // lexique.HostFieldPutData("C03", 0, "X");
    // else
    // lexique.HostFieldPutData("C03", 0, " ");
    // if (C02.isSelected())
    // lexique.HostFieldPutData("C02", 0, "X");
    // else
    // lexique.HostFieldPutData("C02", 0, " ");
    // if (C01.isSelected())
    // lexique.HostFieldPutData("C01", 0, "X");
    // else
    // lexique.HostFieldPutData("C01", 0, " ");
    // if (T01.isSelected())
    // lexique.HostFieldPutData("T01", 0, "X");
    // else
    // lexique.HostFieldPutData("T01", 0, " ");
    // if (T02.isSelected())
    // lexique.HostFieldPutData("T02", 0, "X");
    // else
    // lexique.HostFieldPutData("T02", 0, " ");
    // if (T03.isSelected())
    // lexique.HostFieldPutData("T03", 0, "X");
    // else
    // lexique.HostFieldPutData("T03", 0, " ");
    // if (T04.isSelected())
    // lexique.HostFieldPutData("T04", 0, "X");
    // else
    // lexique.HostFieldPutData("T04", 0, " ");
    // if (T05.isSelected())
    // lexique.HostFieldPutData("T05", 0, "X");
    // else
    // lexique.HostFieldPutData("T05", 0, " ");
    // if (T06.isSelected())
    // lexique.HostFieldPutData("T06", 0, "X");
    // else
    // lexique.HostFieldPutData("T06", 0, " ");
    // if (T07.isSelected())
    // lexique.HostFieldPutData("T07", 0, "X");
    // else
    // lexique.HostFieldPutData("T07", 0, " ");
    // if (T08.isSelected())
    // lexique.HostFieldPutData("T08", 0, "X");
    // else
    // lexique.HostFieldPutData("T08", 0, " ");
    // if (T09.isSelected())
    // lexique.HostFieldPutData("T09", 0, "X");
    // else
    // lexique.HostFieldPutData("T09", 0, " ");
    // if (T10.isSelected())
    // lexique.HostFieldPutData("T10", 0, "X");
    // else
    // lexique.HostFieldPutData("T10", 0, " ");
    // if (T11.isSelected())
    // lexique.HostFieldPutData("T11", 0, "X");
    // else
    // lexique.HostFieldPutData("T11", 0, " ");
    
  }
  
  private void OBJ_67ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WRECAL", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WRECA2", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    LOCTP = new JLabel();
    label1 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    P_Centre = new JPanel();
    SCROLLPANE_OBJ_28 = new JScrollPane();
    LB01 = new XRiTable();
    T02 = new XRiCheckBox();
    T03 = new XRiCheckBox();
    T04 = new XRiCheckBox();
    T05 = new XRiCheckBox();
    T06 = new XRiCheckBox();
    T07 = new XRiCheckBox();
    T08 = new XRiCheckBox();
    T09 = new XRiCheckBox();
    T10 = new XRiCheckBox();
    T11 = new XRiCheckBox();
    T01 = new XRiCheckBox();
    OBJ_95 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_67 = new JButton();
    OBJ_68 = new JButton();
    OBJ_93 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_91 = new JLabel();
    PER01 = new XRiTextField();
    PER02 = new XRiTextField();
    C01 = new XRiCheckBox();
    C02 = new XRiCheckBox();
    C03 = new XRiCheckBox();
    C04 = new XRiCheckBox();
    C05 = new XRiCheckBox();
    C06 = new XRiCheckBox();
    C07 = new XRiCheckBox();
    C08 = new XRiCheckBox();
    C09 = new XRiCheckBox();
    C10 = new XRiCheckBox();
    C11 = new XRiCheckBox();
    OBJ_80 = new JXTitledSeparator();
    OBJ_29 = new JXTitledSeparator();
    WSOC = new XRiTextField();
    BT_ChgSoc = new JButton();
    OBJ_40 = new JLabel();
    WSOC2 = new XRiTextField();
    BT_ChgSoc2 = new JButton();
    OBJ_42 = new JLabel();
    CellConstraints cc = new CellConstraints();

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      OBJ_4.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Exploitation");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 35));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- LOCTP ----
        LOCTP.setText("@LOCTP@");
        LOCTP.setName("LOCTP");

        //---- label1 ----
        label1.setText("@V01F@");
        label1.setName("label1");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(label1)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 856, Short.MAX_VALUE)
              .addComponent(LOCTP)
              .addContainerGap())
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(LOCTP)
                .addComponent(label1))
              .addContainerGap(15, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(89)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== SCROLLPANE_OBJ_28 ========
      {
        SCROLLPANE_OBJ_28.setName("SCROLLPANE_OBJ_28");

        //---- LB01 ----
        LB01.setName("LB01");
        SCROLLPANE_OBJ_28.setViewportView(LB01);
      }
      P_Centre.add(SCROLLPANE_OBJ_28);
      SCROLLPANE_OBJ_28.setBounds(80, 174, 605, 206);

      //---- T02 ----
      T02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T02.setName("T02");
      P_Centre.add(T02);
      T02.setBounds(60, 215, 25, 14);

      //---- T03 ----
      T03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T03.setName("T03");
      P_Centre.add(T03);
      T03.setBounds(60, 231, 25, 14);

      //---- T04 ----
      T04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T04.setName("T04");
      P_Centre.add(T04);
      T04.setBounds(60, 247, 25, 14);

      //---- T05 ----
      T05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T05.setName("T05");
      P_Centre.add(T05);
      T05.setBounds(60, 263, 25, 14);

      //---- T06 ----
      T06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T06.setName("T06");
      P_Centre.add(T06);
      T06.setBounds(60, 279, 25, 14);

      //---- T07 ----
      T07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T07.setName("T07");
      P_Centre.add(T07);
      T07.setBounds(60, 295, 25, 14);

      //---- T08 ----
      T08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T08.setName("T08");
      P_Centre.add(T08);
      T08.setBounds(60, 311, 25, 14);

      //---- T09 ----
      T09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T09.setName("T09");
      P_Centre.add(T09);
      T09.setBounds(60, 327, 25, 14);

      //---- T10 ----
      T10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T10.setName("T10");
      P_Centre.add(T10);
      T10.setBounds(60, 343, 25, 14);

      //---- T11 ----
      T11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T11.setName("T11");
      P_Centre.add(T11);
      T11.setBounds(60, 359, 25, 14);

      //---- T01 ----
      T01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      T01.setName("T01");
      P_Centre.add(T01);
      T01.setBounds(60, 199, 25, 14);

      //---- OBJ_95 ----
      OBJ_95.setText("postes de la p\u00e9riode 1");
      OBJ_95.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_95.setName("OBJ_95");
      P_Centre.add(OBJ_95);
      OBJ_95.setBounds(865, 150, 129, 17);

      //---- OBJ_97 ----
      OBJ_97.setText("postes de la p\u00e9riode 2");
      OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_97.setName("OBJ_97");
      P_Centre.add(OBJ_97);
      OBJ_97.setBounds(865, 250, 129, 17);

      //---- OBJ_92 ----
      OBJ_92.setText("@MESCLO@");
      OBJ_92.setName("OBJ_92");
      P_Centre.add(OBJ_92);
      OBJ_92.setBounds(210, 135, 116, OBJ_92.getPreferredSize().height);

      //---- OBJ_86 ----
      OBJ_86.setText("S\u00e9lection tous les");
      OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_86.setName("OBJ_86");
      P_Centre.add(OBJ_86);
      OBJ_86.setBounds(865, 130, 129, 17);

      //---- OBJ_96 ----
      OBJ_96.setText("S\u00e9lection tous les");
      OBJ_96.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_96.setName("OBJ_96");
      P_Centre.add(OBJ_96);
      OBJ_96.setBounds(865, 230, 129, 17);

      //---- OBJ_67 ----
      OBJ_67.setText("");
      OBJ_67.setToolTipText("Recalcul de tous les postes de la p\u00e9riode 1");
      OBJ_67.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_67.setName("OBJ_67");
      OBJ_67.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_67ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_67);
      OBJ_67.setBounds(910, 175, 40, 40);

      //---- OBJ_68 ----
      OBJ_68.setText("");
      OBJ_68.setToolTipText("Recalcul de tous les postes de la p\u00e9riode 2");
      OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_68.setName("OBJ_68");
      OBJ_68.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_68ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_68);
      OBJ_68.setBounds(910, 270, 40, 40);

      //---- OBJ_93 ----
      OBJ_93.setText("Choix p\u00e9riode 2");
      OBJ_93.setName("OBJ_93");
      P_Centre.add(OBJ_93);
      OBJ_93.setBounds(561, 135, 96, OBJ_93.getPreferredSize().height);

      //---- OBJ_94 ----
      OBJ_94.setText("Choix p\u00e9riode 1");
      OBJ_94.setName("OBJ_94");
      P_Centre.add(OBJ_94);
      OBJ_94.setBounds(370, 135, 96, OBJ_94.getPreferredSize().height);

      //---- OBJ_88 ----
      OBJ_88.setText("En cours");
      OBJ_88.setName("OBJ_88");
      P_Centre.add(OBJ_88);
      OBJ_88.setBounds(85, 135, 55, OBJ_88.getPreferredSize().height);

      //---- OBJ_91 ----
      OBJ_91.setText("@DGPERX@");
      OBJ_91.setName("OBJ_91");
      P_Centre.add(OBJ_91);
      OBJ_91.setBounds(145, 135, 55, OBJ_91.getPreferredSize().height);

      //---- PER01 ----
      PER01.setComponentPopupMenu(BTD);
      PER01.setName("PER01");
      P_Centre.add(PER01);
      PER01.setBounds(465, 130, 45, PER01.getPreferredSize().height);

      //---- PER02 ----
      PER02.setComponentPopupMenu(BTD);
      PER02.setName("PER02");
      P_Centre.add(PER02);
      PER02.setBounds(661, 130, 45, PER02.getPreferredSize().height);

      //---- C01 ----
      C01.setText("");
      C01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C01.setName("C01");
      P_Centre.add(C01);
      C01.setBounds(685, 199, 21, 14);

      //---- C02 ----
      C02.setText("");
      C02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C02.setName("C02");
      P_Centre.add(C02);
      C02.setBounds(685, 215, 21, 14);

      //---- C03 ----
      C03.setText("");
      C03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C03.setName("C03");
      P_Centre.add(C03);
      C03.setBounds(685, 231, 21, 14);

      //---- C04 ----
      C04.setText("");
      C04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C04.setName("C04");
      P_Centre.add(C04);
      C04.setBounds(685, 247, 21, 14);

      //---- C05 ----
      C05.setText("");
      C05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C05.setName("C05");
      P_Centre.add(C05);
      C05.setBounds(685, 263, 21, 14);

      //---- C06 ----
      C06.setText("");
      C06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C06.setName("C06");
      P_Centre.add(C06);
      C06.setBounds(685, 279, 21, 14);

      //---- C07 ----
      C07.setText("");
      C07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C07.setName("C07");
      P_Centre.add(C07);
      C07.setBounds(685, 295, 21, 14);

      //---- C08 ----
      C08.setText("");
      C08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C08.setName("C08");
      P_Centre.add(C08);
      C08.setBounds(685, 311, 21, 14);

      //---- C09 ----
      C09.setText("");
      C09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C09.setName("C09");
      P_Centre.add(C09);
      C09.setBounds(685, 327, 21, 14);

      //---- C10 ----
      C10.setText("");
      C10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C10.setName("C10");
      P_Centre.add(C10);
      C10.setBounds(685, 343, 21, 14);

      //---- C11 ----
      C11.setText("");
      C11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      C11.setName("C11");
      P_Centre.add(C11);
      C11.setBounds(685, 359, 21, 14);

      //---- OBJ_80 ----
      OBJ_80.setTitle("Etat de mise \u00e0 jour du tableau de bord");
      OBJ_80.setName("OBJ_80");
      P_Centre.add(OBJ_80);
      OBJ_80.setBounds(25, 105, 950, OBJ_80.getPreferredSize().height);

      //---- OBJ_29 ----
      OBJ_29.setTitle("Soci\u00e9t\u00e9 s\u00e9lectionn\u00e9e");
      OBJ_29.setName("OBJ_29");
      P_Centre.add(OBJ_29);
      OBJ_29.setBounds(25, 25, 950, OBJ_29.getPreferredSize().height);

      //---- WSOC ----
      WSOC.setComponentPopupMenu(BTD);
      WSOC.setName("WSOC");
      P_Centre.add(WSOC);
      WSOC.setBounds(35, 60, 40, WSOC.getPreferredSize().height);

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });
      P_Centre.add(BT_ChgSoc);
      BT_ChgSoc.setBounds(80, 60, 32, 29);

      //---- OBJ_40 ----
      OBJ_40.setText("@DGNOM@");
      OBJ_40.setForeground(Color.gray);
      OBJ_40.setFont(OBJ_40.getFont().deriveFont(OBJ_40.getFont().getStyle() | Font.BOLD));
      OBJ_40.setName("OBJ_40");
      P_Centre.add(OBJ_40);
      OBJ_40.setBounds(155, 60, 400, 24);

      //---- WSOC2 ----
      WSOC2.setComponentPopupMenu(BTD);
      WSOC2.setName("WSOC2");
      P_Centre.add(WSOC2);
      WSOC2.setBounds(0, 0, 0, 0);

      //---- BT_ChgSoc2 ----
      BT_ChgSoc2.setText("");
      BT_ChgSoc2.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc2.setName("BT_ChgSoc2");
      BT_ChgSoc2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });
      P_Centre.add(BT_ChgSoc2);
      BT_ChgSoc2.setBounds(0, 0, 0, 0);

      //---- OBJ_42 ----
      OBJ_42.setText("@DGNOM@");
      OBJ_42.setForeground(Color.gray);
      OBJ_42.setFont(OBJ_42.getFont().deriveFont(OBJ_42.getFont().getStyle() | Font.BOLD));
      OBJ_42.setName("OBJ_42");
      P_Centre.add(OBJ_42);
      OBJ_42.setBounds(0, 0, 0, 0);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel LOCTP;
  private JLabel label1;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel P_Centre;
  private JScrollPane SCROLLPANE_OBJ_28;
  private XRiTable LB01;
  private XRiCheckBox T02;
  private XRiCheckBox T03;
  private XRiCheckBox T04;
  private XRiCheckBox T05;
  private XRiCheckBox T06;
  private XRiCheckBox T07;
  private XRiCheckBox T08;
  private XRiCheckBox T09;
  private XRiCheckBox T10;
  private XRiCheckBox T11;
  private XRiCheckBox T01;
  private JLabel OBJ_95;
  private JLabel OBJ_97;
  private JLabel OBJ_92;
  private JLabel OBJ_86;
  private JLabel OBJ_96;
  private JButton OBJ_67;
  private JButton OBJ_68;
  private JLabel OBJ_93;
  private JLabel OBJ_94;
  private JLabel OBJ_88;
  private JLabel OBJ_91;
  private XRiTextField PER01;
  private XRiTextField PER02;
  private XRiCheckBox C01;
  private XRiCheckBox C02;
  private XRiCheckBox C03;
  private XRiCheckBox C04;
  private XRiCheckBox C05;
  private XRiCheckBox C06;
  private XRiCheckBox C07;
  private XRiCheckBox C08;
  private XRiCheckBox C09;
  private XRiCheckBox C10;
  private XRiCheckBox C11;
  private JXTitledSeparator OBJ_80;
  private JXTitledSeparator OBJ_29;
  private XRiTextField WSOC;
  private JButton BT_ChgSoc;
  private JLabel OBJ_40;
  private XRiTextField WSOC2;
  private JButton BT_ChgSoc2;
  private JLabel OBJ_42;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
