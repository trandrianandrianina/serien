
package ri.serien.libecranrpg.stbm.STBM15FM;
// Nom Fichier: i_STBM08FM_FMTB2_FMTF1_51.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class STBM15FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STBM15FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim());
    label33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    label34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPERIO@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB05@")).trim());
    bt_mt05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT052@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB10@")).trim());
    bt_mt10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT10@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB08@")).trim());
    bt_mt08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT08@")).trim());
    label18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB09@")).trim());
    bt_mt09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT09@")).trim());
    label19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB07@")).trim());
    bt_mt07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT07@")).trim());
    bt_mtt3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT3@")).trim());
    label20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB01@")).trim());
    label21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB02@")).trim());
    bt_mt02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02@")).trim());
    bt_mt01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT01@")).trim());
    bt_mt033.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT033@")).trim());
    bt_mt03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT03@")).trim());
    bt_mt045.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT045@")).trim());
    bt_mt043.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT043@")).trim());
    bt_mt04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT04@")).trim());
    bt_mt034.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT034@")).trim());
    bt_mt032.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT032@")).trim());
    bt_mt046.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT046@")).trim());
    bt_mt044.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT044@")).trim());
    bt_mt042.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT042@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Cumul depuis le @WPERDB@")).trim());
    label31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB06@")).trim());
    bt_mt06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT06@")).trim());
    label35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB11@")).trim());
    bt_mt11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT11@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WSOC.setVisible(lexique.isPresent("WSOC"));
    if (lexique.HostFieldGetData("MT01").contains("-")) {
      bt_mt01.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT02").contains("-")) {
      bt_mt02.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT03").contains("-")) {
      bt_mt03.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT04").contains("-")) {
      bt_mt04.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT05").contains("-")) {
      bt_mt05.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT06").contains("-")) {
      bt_mt06.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT07").contains("-")) {
      bt_mt07.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT08").contains("-")) {
      bt_mt08.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT09").contains("-")) {
      bt_mt09.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT10").contains("-")) {
      bt_mt10.setForeground(Color.RED);
    }
    if (lexique.HostFieldGetData("MT11").contains("-")) {
      bt_mt11.setForeground(Color.RED);
      /*  if (lexique.HostFieldGetData("MT12").contains("-"))
        bt_mt12.setForeground(Color.RED);
        if (lexique.HostFieldGetData("MT13").contains("-"))
        bt_mt13.setForeground(Color.RED);
        if (lexique.HostFieldGetData("MT14").contains("-"))
        bt_mt14.setForeground(Color.RED);
        if (lexique.HostFieldGetData("MT15").contains("-"))
        bt_mt15w.setForeground(Color.RED);
        if (lexique.HostFieldGetData("MT16").contains("-"))
        bt_mt16.setForeground(Color.RED);
        if (lexique.HostFieldGetData("MT17").contains("-"))
        bt_mt17.setForeground(Color.RED);
        */
    }
    
    if (lexique.HostFieldGetData("MTT3").contains("-")) {
      bt_mtt3.setForeground(Color.RED);
    }
    
    bt_mt01.setVisible(!lexique.HostFieldGetData("MT01").isEmpty());
    bt_mt02.setVisible(!lexique.HostFieldGetData("MT02").isEmpty());
    bt_mt03.setVisible(!lexique.HostFieldGetData("MT03").isEmpty());
    bt_mt04.setVisible(!lexique.HostFieldGetData("MT04").isEmpty());
    // bt_mt07.setVisible( !lexique.HostFieldGetData("MT07").isEmpty());
    // bt_mt08.setVisible( !lexique.HostFieldGetData("MT08").isEmpty());
    // bt_mt09.setVisible( !lexique.HostFieldGetData("MT09").isEmpty());
    // bt_mt11.setVisible( !lexique.HostFieldGetData("MT11").isEmpty());
    
    if (lexique.isTrue("66")) {
      label26.setText("Période 1");
    }
    else {
      label26.setText("Période 2");
    }
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    OBJ_115.setIcon(lexique.chargerImage("images/chgdate.png", true));
    OBJ_107.setIcon(lexique.chargerImage("images/staachat.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WSOC.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("TABLEAU DE BORD DES ACHATS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_107ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void OBJ_115ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  // les boutons lancent des popups intermédiaires
  private void bt_mt01ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(10, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt02ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt05ActionPerformed(ActionEvent e) {
    STBM15FM_MT05 d_05 = new STBM15FM_MT05(this, lexique, interpreteurD);
  }
  
  private void bt_mt07ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt08ActionPerformed(ActionEvent e) {
    STBM15FM_MT08 d_15 = new STBM15FM_MT08(this, lexique, interpreteurD);
  }
  
  private void bt_mt09ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(8, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt033ActionPerformed(ActionEvent e) {
    STBM15FM_MT033 d_033 = new STBM15FM_MT033(this, lexique, interpreteurD);
  }
  
  private void bt_mt034ActionPerformed(ActionEvent e) {
    STBM15FM_MT033 d_033 = new STBM15FM_MT033(this, lexique, interpreteurD);
  }
  
  private void bt_mt03ActionPerformed(ActionEvent e) {
    STBM15FM_MT03 d_03 = new STBM15FM_MT03(this, lexique, interpreteurD);
  }
  
  private void bt_mt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(19, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt06ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_mt043ActionPerformed(ActionEvent e) {
    STBM15FM_MT043 d_043 = new STBM15FM_MT043(this, lexique, interpreteurD);
  }
  
  private void bt_mt044ActionPerformed(ActionEvent e) {
    STBM15FM_MT043 d_044 = new STBM15FM_MT043(this, lexique, interpreteurD);
  }
  
  private void bt_mt032ActionPerformed(ActionEvent e) {
    STBM15FM_MT03 d_032 = new STBM15FM_MT03(this, lexique, interpreteurD);
  }
  
  private void bt_mt04ActionPerformed(ActionEvent e) {
    STBM15FM_MT04 d_04 = new STBM15FM_MT04(this, lexique, interpreteurD);
  }
  
  private void bt_mt042ActionPerformed(ActionEvent e) {
    STBM15FM_MT04 d_042 = new STBM15FM_MT04(this, lexique, interpreteurD);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    WSOC = new XRiTextField();
    label32 = new JLabel();
    label33 = new JLabel();
    label26 = new JLabel();
    label34 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    panel6 = new JPanel();
    OBJ_115 = new JButton();
    OBJ_107 = new JButton();
    P_Centre = new JPanel();
    label8 = new JLabel();
    bt_mt05 = new JButton();
    panel3 = new JPanel();
    label16 = new JLabel();
    bt_mt10 = new JButton();
    label17 = new JLabel();
    bt_mt08 = new JButton();
    label18 = new JLabel();
    bt_mt09 = new JButton();
    label19 = new JLabel();
    bt_mt07 = new JButton();
    label9 = new JLabel();
    bt_mtt3 = new JButton();
    label20 = new JLabel();
    label21 = new JLabel();
    bt_mt02 = new JButton();
    bt_mt01 = new JButton();
    panel4 = new JPanel();
    label23 = new JLabel();
    bt_mt033 = new JButton();
    label22 = new JLabel();
    bt_mt03 = new JButton();
    label24 = new JLabel();
    label25 = new JLabel();
    bt_mt045 = new JButton();
    bt_mt043 = new JButton();
    label28 = new JLabel();
    bt_mt04 = new JButton();
    bt_mt034 = new JButton();
    bt_mt032 = new JButton();
    bt_mt046 = new JButton();
    bt_mt044 = new JButton();
    bt_mt042 = new JButton();
    label1 = new JLabel();
    label2 = new JLabel();
    label31 = new JLabel();
    bt_mt06 = new JButton();
    label35 = new JLabel();
    bt_mt11 = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Changement de p\u00e9riode");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Exploitation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);
    }

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("TABLEAU DE BORD GENERAL");
      xH_Titre.setDescription("@LIBPOS@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 35));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- WSOC ----
        WSOC.setName("WSOC");

        //---- label32 ----
        label32.setText("soci\u00e9t\u00e9");
        label32.setName("label32");

        //---- label33 ----
        label33.setText("@DGNOM@");
        label33.setName("label33");

        //---- label26 ----
        label26.setText("text");
        label26.setName("label26");

        //---- label34 ----
        label34.setText("@WPERIO@");
        label34.setName("label34");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(label32, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addComponent(label33, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 309, Short.MAX_VALUE)
              .addComponent(label26, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(label34, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
              .addGap(67, 67, 67))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(label32)
                .addComponent(label33)
                .addComponent(label26)
                .addComponent(label34))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(89)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== panel6 ========
    {
      panel6.setName("panel6");
      panel6.setLayout(null);

      //---- OBJ_115 ----
      OBJ_115.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_115.setToolTipText("Changement de p\u00e9riode");
      OBJ_115.setName("OBJ_115");
      OBJ_115.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_115ActionPerformed(e);
        }
      });
      panel6.add(OBJ_115);
      OBJ_115.setBounds(0, 10, 40, 40);

      //---- OBJ_107 ----
      OBJ_107.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_107.setToolTipText("Statistiques d'achats");
      OBJ_107.setName("OBJ_107");
      OBJ_107.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_107ActionPerformed(e);
        }
      });
      panel6.add(OBJ_107);
      OBJ_107.setBounds(0, 55, 40, 40);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel6.getComponentCount(); i++) {
          Rectangle bounds = panel6.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel6.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel6.setMinimumSize(preferredSize);
        panel6.setPreferredSize(preferredSize);
      }
    }
    add(panel6, BorderLayout.EAST);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- label8 ----
      label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
      label8.setText("@LB05@");
      label8.setName("label8");

      //---- bt_mt05 ----
      bt_mt05.setHorizontalAlignment(SwingConstants.RIGHT);
      bt_mt05.setFont(bt_mt05.getFont().deriveFont(bt_mt05.getFont().getStyle() | Font.BOLD));
      bt_mt05.setText("@MT052@");
      bt_mt05.setName("bt_mt05");
      bt_mt05.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_mt05ActionPerformed(e);
        }
      });

      //======== panel3 ========
      {
        panel3.setBorder(new TitledBorder("dettes"));
        panel3.setName("panel3");

        //---- label16 ----
        label16.setText("@LB10@");
        label16.setName("label16");

        //---- bt_mt10 ----
        bt_mt10.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt10.setText("@MT10@");
        bt_mt10.setName("bt_mt10");
        bt_mt10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt10ActionPerformed(e);
          }
        });

        //---- label17 ----
        label17.setText("@LB08@");
        label17.setName("label17");

        //---- bt_mt08 ----
        bt_mt08.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt08.setText("@MT08@");
        bt_mt08.setName("bt_mt08");
        bt_mt08.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt08ActionPerformed(e);
          }
        });

        //---- label18 ----
        label18.setText("@LB09@");
        label18.setName("label18");

        //---- bt_mt09 ----
        bt_mt09.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt09.setText("@MT09@");
        bt_mt09.setName("bt_mt09");
        bt_mt09.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt09ActionPerformed(e);
          }
        });

        //---- label19 ----
        label19.setText("@LB07@");
        label19.setName("label19");

        //---- bt_mt07 ----
        bt_mt07.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt07.setText("@MT07@");
        bt_mt07.setName("bt_mt07");
        bt_mt07.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt07ActionPerformed(e);
          }
        });

        //---- label9 ----
        label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
        label9.setText("TOTAL DETTES");
        label9.setName("label9");

        //---- bt_mtt3 ----
        bt_mtt3.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mtt3.setFont(bt_mtt3.getFont().deriveFont(bt_mtt3.getFont().getStyle() | Font.BOLD));
        bt_mtt3.setText("@MTT3@");
        bt_mtt3.setName("bt_mtt3");
        bt_mtt3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt05ActionPerformed(e);
          }
        });

        GroupLayout panel3Layout = new GroupLayout(panel3);
        panel3.setLayout(panel3Layout);
        panel3Layout.setHorizontalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGap(21, 21, 21)
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(label19, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
                  .addGap(40, 40, 40)
                  .addComponent(bt_mt07, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(label16, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
                  .addGap(40, 40, 40)
                  .addComponent(bt_mt10, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(label17, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
                  .addGap(40, 40, 40)
                  .addComponent(bt_mt08, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(label18, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
                  .addGap(40, 40, 40)
                  .addComponent(bt_mt09, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(label9, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
                  .addGap(40, 40, 40)
                  .addComponent(bt_mtt3, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))))
        );
        panel3Layout.setVerticalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label19))
                .addComponent(bt_mt07, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label16))
                .addComponent(bt_mt10, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label17))
                .addComponent(bt_mt08, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label18))
                .addComponent(bt_mt09, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGap(5, 5, 5)
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label9))
                .addComponent(bt_mtt3, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
      }

      //---- label20 ----
      label20.setText("@LB01@");
      label20.setName("label20");

      //---- label21 ----
      label21.setText("@LB02@");
      label21.setName("label21");

      //---- bt_mt02 ----
      bt_mt02.setHorizontalAlignment(SwingConstants.RIGHT);
      bt_mt02.setText("@MT02@");
      bt_mt02.setName("bt_mt02");
      bt_mt02.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_mt02ActionPerformed(e);
        }
      });

      //---- bt_mt01 ----
      bt_mt01.setHorizontalAlignment(SwingConstants.RIGHT);
      bt_mt01.setText("@MT01@");
      bt_mt01.setName("bt_mt01");
      bt_mt01.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_mt01ActionPerformed(e);
        }
      });

      //======== panel4 ========
      {
        panel4.setBorder(new TitledBorder(""));
        panel4.setName("panel4");

        //---- label23 ----
        label23.setText("Nombre de commandes");
        label23.setName("label23");

        //---- bt_mt033 ----
        bt_mt033.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt033.setText("@MT033@");
        bt_mt033.setName("bt_mt033");
        bt_mt033.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt033ActionPerformed(e);
          }
        });

        //---- label22 ----
        label22.setText("Montant des commandes");
        label22.setName("label22");

        //---- bt_mt03 ----
        bt_mt03.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt03.setText("@MT03@");
        bt_mt03.setName("bt_mt03");
        bt_mt03.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt03ActionPerformed(e);
          }
        });

        //---- label24 ----
        label24.setText("R\u00e9ceptions");
        label24.setName("label24");

        //---- label25 ----
        label25.setText("Poids");
        label25.setName("label25");

        //---- bt_mt045 ----
        bt_mt045.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt045.setText("@MT045@");
        bt_mt045.setName("bt_mt045");

        //---- bt_mt043 ----
        bt_mt043.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt043.setText("@MT043@");
        bt_mt043.setName("bt_mt043");
        bt_mt043.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt043ActionPerformed(e);
          }
        });

        //---- label28 ----
        label28.setText("Valeur des r\u00e9ceptions");
        label28.setName("label28");

        //---- bt_mt04 ----
        bt_mt04.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt04.setText("@MT04@");
        bt_mt04.setName("bt_mt04");
        bt_mt04.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt04ActionPerformed(e);
          }
        });

        //---- bt_mt034 ----
        bt_mt034.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt034.setText("@MT034@");
        bt_mt034.setName("bt_mt034");
        bt_mt034.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt034ActionPerformed(e);
          }
        });

        //---- bt_mt032 ----
        bt_mt032.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt032.setText("@MT032@");
        bt_mt032.setName("bt_mt032");
        bt_mt032.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt032ActionPerformed(e);
          }
        });

        //---- bt_mt046 ----
        bt_mt046.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt046.setText("@MT046@");
        bt_mt046.setName("bt_mt046");

        //---- bt_mt044 ----
        bt_mt044.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt044.setText("@MT044@");
        bt_mt044.setName("bt_mt044");
        bt_mt044.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt044ActionPerformed(e);
          }
        });

        //---- bt_mt042 ----
        bt_mt042.setHorizontalAlignment(SwingConstants.RIGHT);
        bt_mt042.setText("@MT042@");
        bt_mt042.setName("bt_mt042");
        bt_mt042.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_mt042ActionPerformed(e);
          }
        });

        //---- label1 ----
        label1.setText("Du mois");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
        label1.setName("label1");

        //---- label2 ----
        label2.setText("Cumul depuis le @WPERDB@");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
        label2.setName("label2");

        GroupLayout panel4Layout = new GroupLayout(panel4);
        panel4.setLayout(panel4Layout);
        panel4Layout.setHorizontalGroup(
          panel4Layout.createParallelGroup()
            .addGroup(panel4Layout.createSequentialGroup()
              .addGap(334, 334, 334)
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
              .addGap(29, 29, 29)
              .addComponent(label2, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel4Layout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(label23, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
              .addGap(40, 40, 40)
              .addComponent(bt_mt033, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
              .addGap(49, 49, 49)
              .addComponent(bt_mt034, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel4Layout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(label22, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
              .addGap(40, 40, 40)
              .addComponent(bt_mt03, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
              .addGap(49, 49, 49)
              .addComponent(bt_mt032, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel4Layout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(label25, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
              .addGap(40, 40, 40)
              .addComponent(bt_mt045, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
              .addGap(49, 49, 49)
              .addComponent(bt_mt046, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel4Layout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(label24, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
              .addGap(40, 40, 40)
              .addComponent(bt_mt043, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
              .addGap(49, 49, 49)
              .addComponent(bt_mt044, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel4Layout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(label28, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
              .addGap(40, 40, 40)
              .addComponent(bt_mt04, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
              .addGap(49, 49, 49)
              .addComponent(bt_mt042, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
        );
        panel4Layout.setVerticalGroup(
          panel4Layout.createParallelGroup()
            .addGroup(panel4Layout.createSequentialGroup()
              .addGap(9, 9, 9)
              .addGroup(panel4Layout.createParallelGroup()
                .addComponent(label1)
                .addComponent(label2))
              .addGap(5, 5, 5)
              .addGroup(panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label23))
                .addComponent(bt_mt033, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_mt034, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label22))
                .addComponent(bt_mt03, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_mt032, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label25))
                .addComponent(bt_mt045, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_mt046, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label24))
                .addComponent(bt_mt043, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_mt044, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label28))
                .addComponent(bt_mt04, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_mt042, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
      }

      //---- label31 ----
      label31.setText("@LB06@");
      label31.setName("label31");

      //---- bt_mt06 ----
      bt_mt06.setHorizontalAlignment(SwingConstants.RIGHT);
      bt_mt06.setText("@MT06@");
      bt_mt06.setName("bt_mt06");
      bt_mt06.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_mt06ActionPerformed(e);
        }
      });

      //---- label35 ----
      label35.setText("@LB11@");
      label35.setName("label35");

      //---- bt_mt11 ----
      bt_mt11.setHorizontalAlignment(SwingConstants.RIGHT);
      bt_mt11.setText("@MT11@");
      bt_mt11.setName("bt_mt11");
      bt_mt11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_mt11ActionPerformed(e);
        }
      });

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(25, 25, 25)
            .addComponent(label8, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
            .addGap(35, 35, 35)
            .addComponent(bt_mt05, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(135, 135, 135)
            .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(25, 25, 25)
            .addComponent(label20, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
            .addGap(35, 35, 35)
            .addComponent(bt_mt01, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
            .addGap(54, 54, 54)
            .addComponent(label21, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
            .addGap(20, 20, 20)
            .addComponent(bt_mt02, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(135, 135, 135)
            .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(25, 25, 25)
            .addComponent(label31, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
            .addGap(35, 35, 35)
            .addComponent(bt_mt06, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
            .addGap(54, 54, 54)
            .addComponent(label35, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
            .addGap(20, 20, 20)
            .addComponent(bt_mt11, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(label8))
              .addComponent(bt_mt05, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
            .addGap(9, 9, 9)
            .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
            .addGap(1, 1, 1)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(label20))
              .addComponent(bt_mt01, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(label21))
              .addComponent(bt_mt02, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
            .addGap(9, 9, 9)
            .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(label31))
              .addComponent(bt_mt06, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(label35))
              .addComponent(bt_mt11, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private XRiTextField WSOC;
  private JLabel label32;
  private JLabel label33;
  private JLabel label26;
  private JLabel label34;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel panel6;
  private JButton OBJ_115;
  private JButton OBJ_107;
  private JPanel P_Centre;
  private JLabel label8;
  private JButton bt_mt05;
  private JPanel panel3;
  private JLabel label16;
  private JButton bt_mt10;
  private JLabel label17;
  private JButton bt_mt08;
  private JLabel label18;
  private JButton bt_mt09;
  private JLabel label19;
  private JButton bt_mt07;
  private JLabel label9;
  private JButton bt_mtt3;
  private JLabel label20;
  private JLabel label21;
  private JButton bt_mt02;
  private JButton bt_mt01;
  private JPanel panel4;
  private JLabel label23;
  private JButton bt_mt033;
  private JLabel label22;
  private JButton bt_mt03;
  private JLabel label24;
  private JLabel label25;
  private JButton bt_mt045;
  private JButton bt_mt043;
  private JLabel label28;
  private JButton bt_mt04;
  private JButton bt_mt034;
  private JButton bt_mt032;
  private JButton bt_mt046;
  private JButton bt_mt044;
  private JButton bt_mt042;
  private JLabel label1;
  private JLabel label2;
  private JLabel label31;
  private JButton bt_mt06;
  private JLabel label35;
  private JButton bt_mt11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
