
package ri.serien.libecranrpg.stbm.STBM09FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STBM09FM_BB extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "Soc", "Fournisseur", "Libellé", "LIBDEV", };
  private String[][] _T01_Data = { { "S01", "J01", "LE01", "F01", }, { "S02", "J02", "LE02", "F02", }, { "S03", "J03", "LE03", "F03", },
      { "S04", "J04", "LE04", "F04", }, { "S05", "J05", "LE05", "F05", }, { "S06", "J06", "LE06", "F06", },
      { "S07", "J07", "LE07", "F07", }, { "S08", "J08", "LE08", "F08", }, { "S09", "J09", "LE09", "F09", },
      { "S10", "J10", "LE10", "F10", }, { "S11", "J11", "LE11", "F11", }, { "S12", "J12", "LE12", "F12", },
      { "S13", "J13", "LE13", "F13", }, { "S14", "J14", "LE14", "F14", }, { "S15", "J15", "LE15", "F15", }, };
  private int[] _T01_Width = { 22, 24, 219, 118, };
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT };
  
  public STBM09FM_BB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _T01_Top, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Synthèse"));
    
    

    
    
    riMenu_bt4.setIcon(lexique.chargerImage("images/stats.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "ENTER", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_UPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void bt_DOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "Enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    MTTOT = new XRiTextField();
    OBJ_24 = new JLabel();
    separator1 = compFactory.createSeparator("@LIBPOS@");
    bt_UP = new JButton();
    bt_DOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(930, 355));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu4 ========
          {
            riMenu4.setName("riMenu4");

            //---- riMenu_bt4 ----
            riMenu_bt4.setText("Statistiques");
            riMenu_bt4.setName("riMenu_bt4");
            riMenu4.add(riMenu_bt4);
          }
          menus_haut.add(riMenu4);

          //======== riSousMenu18 ========
          {
            riSousMenu18.setName("riSousMenu18");

            //---- riSousMenu_bt18 ----
            riSousMenu_bt18.setText("Comparaison");
            riSousMenu_bt18.setName("riSousMenu_bt18");
            riSousMenu_bt18.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt18ActionPerformed(e);
              }
            });
            riSousMenu18.add(riSousMenu_bt18);
          }
          menus_haut.add(riSousMenu18);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- T01 ----
            T01.setName("T01");
            T01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                T01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(T01);
          }

          //---- MTTOT ----
          MTTOT.setComponentPopupMenu(BTD);
          MTTOT.setName("MTTOT");

          //---- OBJ_24 ----
          OBJ_24.setText("Total");
          OBJ_24.setName("OBJ_24");

          //---- separator1 ----
          separator1.setName("separator1");

          //---- bt_UP ----
          bt_UP.setText("");
          bt_UP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_UP.setName("bt_UP");
          bt_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_UPActionPerformed(e);
            }
          });

          //---- bt_DOWN ----
          bt_DOWN.setText("");
          bt_DOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_DOWN.setName("bt_DOWN");
          bt_DOWN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_DOWNActionPerformed(e);
            }
          });

          GroupLayout p_recupLayout = new GroupLayout(p_recup);
          p_recup.setLayout(p_recupLayout);
          p_recupLayout.setHorizontalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(p_recupLayout.createParallelGroup()
                  .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(505, 505, 505)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
          );
          p_recupLayout.setVerticalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_recupLayout.createParallelGroup()
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_recupLayout.createSequentialGroup()
                    .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addGroup(p_recupLayout.createParallelGroup()
                  .addGroup(p_recupLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_24))
                  .addComponent(MTTOT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(57, 57, 57))
          );
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(5, 5, 750, 345);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private XRiTextField MTTOT;
  private JLabel OBJ_24;
  private JComponent separator1;
  private JButton bt_UP;
  private JButton bt_DOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
