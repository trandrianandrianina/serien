
package ri.serien.libecranrpg.stbm.STBM08FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class STBM08FM_MT04 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  
  public STBM08FM_MT04(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG04@")).trim());
  }
  
  public void setData() {
    // Valeur
    label4.setText(lexique.HostFieldGetData("MG04"));
    
    
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 50);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 65);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    OBJ_10 = new JButton();
    OBJ_13 = new JXTitledSeparator();
    label3 = new JLabel();
    label4 = new JLabel();
    button1 = new JButton();
    button2 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 230));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      // ---- OBJ_13 ----
      OBJ_13.setTitle("Montant en commande");
      OBJ_13.setName("OBJ_13");
      
      // ---- label3 ----
      label3.setText("Taux de marge : ");
      label3.setName("label3");
      
      // ---- label4 ----
      label4.setText("@MG04@");
      label4.setHorizontalAlignment(SwingConstants.RIGHT);
      label4.setForeground(new Color(102, 102, 255));
      label4.setName("label4");
      
      // ---- button1 ----
      button1.setText("Par mois");
      button1.setName("button1");
      button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button1ActionPerformed(e);
        }
      });
      
      // ---- button2 ----
      button2.setText("Par dates");
      button2.setName("button2");
      button2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button2ActionPerformed(e);
        }
      });
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout
          .setHorizontalGroup(
              P_CentreLayout.createParallelGroup()
                  .addGroup(P_CentreLayout.createSequentialGroup()
                      .addGroup(P_CentreLayout.createParallelGroup()
                          .addGroup(P_CentreLayout.createSequentialGroup().addGap(45, 45, 45).addGroup(P_CentreLayout
                              .createParallelGroup().addComponent(button1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                              .addComponent(button2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                              .addGroup(P_CentreLayout.createSequentialGroup()
                                  .addComponent(label3, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                                  .addComponent(label4, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))))
                          .addGroup(P_CentreLayout.createSequentialGroup().addContainerGap()
                              .addGroup(P_CentreLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                  .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, 292, GroupLayout.PREFERRED_SIZE))))
                      .addGap(33, 33, 33)));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(8, 8, 8)
              .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(14, 14, 14)
              .addGroup(P_CentreLayout.createParallelGroup().addComponent(label3).addComponent(label4))
              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(button1).addGap(7, 7, 7).addComponent(button2)
              .addGap(6, 6, 6).addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addContainerGap()));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JButton OBJ_10;
  private JXTitledSeparator OBJ_13;
  private JLabel label3;
  private JLabel label4;
  private JButton button1;
  private JButton button2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
