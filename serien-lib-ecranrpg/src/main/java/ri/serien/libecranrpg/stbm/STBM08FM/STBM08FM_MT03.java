
package ri.serien.libecranrpg.stbm.STBM08FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class STBM08FM_MT03 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  
  public STBM08FM_MT03(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG03@")).trim());
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // Valeur
    label2.setText(lexique.HostFieldGetData("MG03"));
    
    
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 50);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    getData();
    dispose();
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 50);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 65);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    OBJ_13 = new JXTitledSeparator();
    OBJ_10 = new JButton();
    label1 = new JLabel();
    label2 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 155));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- OBJ_13 ----
      OBJ_13.setTitle("Montant exp\u00e9di\u00e9 non factur\u00e9");
      OBJ_13.setName("OBJ_13");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      // ---- label1 ----
      label1.setText("Taux de marge : ");
      label1.setName("label1");
      
      // ---- label2 ----
      label2.setText("@MG03@");
      label2.setHorizontalAlignment(SwingConstants.RIGHT);
      label2.setForeground(new Color(102, 102, 255));
      label2.setName("label2");
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(15, 15, 15)
              .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, 292, GroupLayout.PREFERRED_SIZE).addContainerGap(20, Short.MAX_VALUE))
          .addGroup(GroupLayout.Alignment.TRAILING,
              P_CentreLayout.createSequentialGroup().addContainerGap(261, Short.MAX_VALUE)
                  .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE).addContainerGap())
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(38, 38, 38)
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
              .addComponent(label2, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE).addContainerGap(39, Short.MAX_VALUE)));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(5, 5, 5)
              .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
              .addGroup(P_CentreLayout.createParallelGroup().addComponent(label1).addComponent(label2)).addGap(26, 26, 26)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(35, 35, 35)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_13;
  private JButton OBJ_10;
  private JLabel label1;
  private JLabel label2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
