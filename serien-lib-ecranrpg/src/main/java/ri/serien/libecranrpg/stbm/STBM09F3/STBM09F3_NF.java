
package ri.serien.libecranrpg.stbm.STBM09F3;
// Nom Fichier: pop_STBM09F3_FMTNF_90.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class STBM09F3_NF extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STBM09F3_NF(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    SUFFI3.setEnabled(lexique.isPresent("SUFFI3"));
    SUFFI2.setEnabled(lexique.isPresent("SUFFI2"));
    SUFFI1.setEnabled(lexique.isPresent("SUFFI1"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Fichier sur disque"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void VALActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    SUFFI1 = new XRiTextField();
    SUFFI2 = new XRiTextField();
    SUFFI3 = new XRiTextField();
    OBJ_18 = new JLabel();
    OBJ_24 = new JButton();
    BT_ENTER = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_23 = new JLabel();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- SUFFI1 ----
    SUFFI1.setComponentPopupMenu(BTD);
    SUFFI1.setName("SUFFI1");
    add(SUFFI1);
    SUFFI1.setBounds(30, 65, 403, 20);

    //---- SUFFI2 ----
    SUFFI2.setComponentPopupMenu(BTD);
    SUFFI2.setName("SUFFI2");
    add(SUFFI2);
    SUFFI2.setBounds(30, 87, 403, 20);

    //---- SUFFI3 ----
    SUFFI3.setComponentPopupMenu(BTD);
    SUFFI3.setName("SUFFI3");
    add(SUFFI3);
    SUFFI3.setBounds(30, 109, 403, 20);

    //---- OBJ_18 ----
    OBJ_18.setText("Nom du fichier excel dans les documents li\u00e9s:");
    OBJ_18.setName("OBJ_18");
    add(OBJ_18);
    OBJ_18.setBounds(30, 45, 276, 20);

    //---- OBJ_24 ----
    OBJ_24.setText("Retour");
    OBJ_24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_24.setName("OBJ_24");
    OBJ_24.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_24ActionPerformed(e);
      }
    });
    add(OBJ_24);
    OBJ_24.setBounds(376, 146, 82, 24);

    //---- BT_ENTER ----
    BT_ENTER.setText("Ok");
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        VALActionPerformed(e);
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(291, 146, 82, 24);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Nom du fichier sur disque");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(20, 10, 425, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(466, 179));

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Exploitation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //---- OBJ_23 ----
    OBJ_23.setText("Nom du fichier sur disque");
    OBJ_23.setName("OBJ_23");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private XRiTextField SUFFI1;
  private XRiTextField SUFFI2;
  private XRiTextField SUFFI3;
  private JLabel OBJ_18;
  private JButton OBJ_24;
  private JButton BT_ENTER;
  private JXTitledSeparator xTitledSeparator1;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JLabel OBJ_23;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
