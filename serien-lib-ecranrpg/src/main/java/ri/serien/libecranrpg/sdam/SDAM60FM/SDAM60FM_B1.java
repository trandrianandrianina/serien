
package ri.serien.libecranrpg.sdam.SDAM60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SDAM60FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SDAM60FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPON2.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    WTOU2.setValeursSelection("**", "  ");
    WTOU3.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // TGRAT.setEnabled( lexique.isPresent("TGRAT"));
    // WMAG.setEnabled( lexique.isPresent("WMAG"));
    // SFAFIN.setEnabled( lexique.isPresent("SFAFIN"));
    // SFADEB.setEnabled( lexique.isPresent("SFADEB"));
    // FAMFIN.setEnabled( lexique.isPresent("FAMFIN"));
    // FAMDEB.setEnabled( lexique.isPresent("FAMDEB"));
    // DATFIN.setEnabled( lexique.isPresent("DATFIN"));
    // DATDEB.setEnabled( lexique.isPresent("DATDEB"));
    // WQTE.setEnabled( lexique.isPresent("WQTE"));
    // REPON2.setEnabled( lexique.isPresent("REPON2"));
    // REPON2.setSelected(lexique.HostFieldGetData("REPON2").equalsIgnoreCase("OUI"));
    // REPON1.setEnabled( lexique.isPresent("REPON1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    // WTOU2.setEnabled( lexique.isPresent("WTOU2"));
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    // WTOU3.setEnabled( lexique.isPresent("WTOU3"));
    // WTOU3.setSelected(lexique.HostFieldGetData("WTOU3").equalsIgnoreCase("**"));
    // WTOU1.setEnabled( lexique.isPresent("WTOU1"));
    // WTOU1.setSelected(lexique.HostFieldGetData("WTOU1").equalsIgnoreCase("**"));
    // ARTFIN.setEnabled( lexique.isPresent("ARTFIN"));
    // ARTDEB.setEnabled( lexique.isPresent("ARTDEB"));
    P_SEL1.setVisible(!WTOU3.isSelected());
    // SFAFIN.setEnabled( lexique.isPresent("SFAFIN"));
    P_SEL0.setVisible(!WTOU1.isSelected());
    // FAMFIN.setEnabled( lexique.isPresent("FAMFIN"));
    P_SEL2.setVisible(!WTOU2.isSelected());
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (REPON2.isSelected())
    // lexique.HostFieldPutData("REPON2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON2", 0, "NON");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // if (WTOU3.isSelected())
    // lexique.HostFieldPutData("WTOU3", 0, "**");
    // if (WTOU1.isSelected())
    // lexique.HostFieldPutData("WTOU1", 0, "**");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOU3ActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_67 = new JXTitledSeparator();
    OBJ_40 = new JXTitledSeparator();
    OBJ_51 = new JXTitledSeparator();
    OBJ_59 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_70 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_46 = new JXTitledSeparator();
    OBJ_75 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_55 = new JLabel();
    OBJ_57 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_62 = new JLabel();
    OBJ_64 = new JLabel();
    SFADEB = new XRiTextField();
    SFAFIN = new XRiTextField();
    OBJ_49 = new JLabel();
    WTOU1 = new XRiCheckBox();
    WTOU3 = new XRiCheckBox();
    WTOU2 = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    OBJ_76 = new JLabel();
    REPON2 = new XRiCheckBox();
    OBJ_81 = new JLabel();
    OBJ_83 = new JLabel();
    WQTE = new XRiTextField();
    OBJ_85 = new JLabel();
    WMAG = new XRiTextField();
    TGRAT = new XRiTextField();
    OBJ_78 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(910, 710));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 610));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(700, 610));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_67 ----
          OBJ_67.setTitle("Plages d'ouvrages");
          OBJ_67.setName("OBJ_67");

          //---- OBJ_40 ----
          OBJ_40.setTitle("Options");
          OBJ_40.setName("OBJ_40");

          //---- OBJ_51 ----
          OBJ_51.setTitle("Plages de Codes Groupe-Famille");
          OBJ_51.setName("OBJ_51");

          //---- OBJ_59 ----
          OBJ_59.setTitle("Plage de Sous-Famille");
          OBJ_59.setName("OBJ_59");

          //======== P_SEL2 ========
          {
            P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL2.setOpaque(false);
            P_SEL2.setName("P_SEL2");
            P_SEL2.setLayout(null);

            //---- ARTDEB ----
            ARTDEB.setComponentPopupMenu(BTD);
            ARTDEB.setName("ARTDEB");
            P_SEL2.add(ARTDEB);
            ARTDEB.setBounds(120, 10, 210, ARTDEB.getPreferredSize().height);

            //---- ARTFIN ----
            ARTFIN.setComponentPopupMenu(BTD);
            ARTFIN.setName("ARTFIN");
            P_SEL2.add(ARTFIN);
            ARTFIN.setBounds(120, 40, 210, ARTFIN.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("Ouvrage d\u00e9but");
            OBJ_70.setName("OBJ_70");
            P_SEL2.add(OBJ_70);
            OBJ_70.setBounds(15, 14, 89, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("Ouvrage fin");
            OBJ_74.setName("OBJ_74");
            P_SEL2.add(OBJ_74);
            OBJ_74.setBounds(15, 44, 69, 20);
          }

          //---- OBJ_46 ----
          OBJ_46.setTitle("");
          OBJ_46.setName("OBJ_46");

          //---- OBJ_75 ----
          OBJ_75.setTitle("");
          OBJ_75.setName("OBJ_75");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_55 ----
            OBJ_55.setText("Groupe d\u00e9but");
            OBJ_55.setName("OBJ_55");
            P_SEL0.add(OBJ_55);
            OBJ_55.setBounds(15, 19, 89, 20);

            //---- OBJ_57 ----
            OBJ_57.setText("Groupe fin");
            OBJ_57.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_57.setName("OBJ_57");
            P_SEL0.add(OBJ_57);
            OBJ_57.setBounds(185, 19, 69, 20);

            //---- FAMDEB ----
            FAMDEB.setComponentPopupMenu(BTD);
            FAMDEB.setName("FAMDEB");
            P_SEL0.add(FAMDEB);
            FAMDEB.setBounds(110, 15, 40, FAMDEB.getPreferredSize().height);

            //---- FAMFIN ----
            FAMFIN.setComponentPopupMenu(BTD);
            FAMFIN.setName("FAMFIN");
            P_SEL0.add(FAMFIN);
            FAMFIN.setBounds(270, 15, 40, FAMFIN.getPreferredSize().height);
          }

          //======== P_SEL1 ========
          {
            P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL1.setOpaque(false);
            P_SEL1.setName("P_SEL1");
            P_SEL1.setLayout(null);

            //---- OBJ_62 ----
            OBJ_62.setText("D\u00e9but");
            OBJ_62.setName("OBJ_62");
            P_SEL1.add(OBJ_62);
            OBJ_62.setBounds(15, 14, 89, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("Fin");
            OBJ_64.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_64.setName("OBJ_64");
            P_SEL1.add(OBJ_64);
            OBJ_64.setBounds(205, 14, 50, 20);

            //---- SFADEB ----
            SFADEB.setComponentPopupMenu(BTD);
            SFADEB.setName("SFADEB");
            P_SEL1.add(SFADEB);
            SFADEB.setBounds(110, 10, 60, SFADEB.getPreferredSize().height);

            //---- SFAFIN ----
            SFAFIN.setComponentPopupMenu(BTD);
            SFAFIN.setName("SFAFIN");
            P_SEL1.add(SFAFIN);
            SFAFIN.setBounds(270, 10, 60, SFAFIN.getPreferredSize().height);
          }

          //---- OBJ_49 ----
          OBJ_49.setText("Code(s) magasin(s) \u00e0 traiter");
          OBJ_49.setName("OBJ_49");

          //---- WTOU1 ----
          WTOU1.setText("S\u00e9lection compl\u00e8te");
          WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU1.setName("WTOU1");
          WTOU1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU1ActionPerformed(e);
            }
          });

          //---- WTOU3 ----
          WTOU3.setText("S\u00e9lection compl\u00e8te");
          WTOU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU3.setName("WTOU3");
          WTOU3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU3ActionPerformed(e);
            }
          });

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU2.setName("WTOU2");
          WTOU2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU2ActionPerformed(e);
            }
          });

          //---- REPON1 ----
          REPON1.setText("Offices payants");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- OBJ_76 ----
          OBJ_76.setText("P\u00e9riode de parution");
          OBJ_76.setName("OBJ_76");

          //---- REPON2 ----
          REPON2.setText("Offices gratuits");
          REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON2.setName("REPON2");

          //---- OBJ_81 ----
          OBJ_81.setText("Quantit\u00e9 \u00e0 servir");
          OBJ_81.setName("OBJ_81");

          //---- OBJ_83 ----
          OBJ_83.setText("( Type de gratuit");
          OBJ_83.setName("OBJ_83");

          //---- WQTE ----
          WQTE.setComponentPopupMenu(BTD);
          WQTE.setName("WQTE");

          //---- OBJ_85 ----
          OBJ_85.setText(")");
          OBJ_85.setName("OBJ_85");

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");

          //---- TGRAT ----
          TGRAT.setComponentPopupMenu(BTD);
          TGRAT.setName("TGRAT");

          //---- OBJ_78 ----
          OBJ_78.setText("/");
          OBJ_78.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_78.setName("OBJ_78");

          //---- DATDEB ----
          DATDEB.setName("DATDEB");

          //---- DATFIN ----
          DATFIN.setName("DATFIN");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(107, 107, 107)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(WTOU3, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
                .addGap(69, 69, 69)
                .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(TGRAT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(110, 110, 110)
                    .addComponent(WQTE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WTOU1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(WTOU3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(P_SEL2, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(TGRAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WQTE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_67;
  private JXTitledSeparator OBJ_40;
  private JXTitledSeparator OBJ_51;
  private JXTitledSeparator OBJ_59;
  private JPanel P_SEL2;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_70;
  private JLabel OBJ_74;
  private JXTitledSeparator OBJ_46;
  private JXTitledSeparator OBJ_75;
  private JPanel P_SEL0;
  private JLabel OBJ_55;
  private JLabel OBJ_57;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JPanel P_SEL1;
  private JLabel OBJ_62;
  private JLabel OBJ_64;
  private XRiTextField SFADEB;
  private XRiTextField SFAFIN;
  private JLabel OBJ_49;
  private XRiCheckBox WTOU1;
  private XRiCheckBox WTOU3;
  private XRiCheckBox WTOU2;
  private XRiCheckBox REPON1;
  private JLabel OBJ_76;
  private XRiCheckBox REPON2;
  private JLabel OBJ_81;
  private JLabel OBJ_83;
  private XRiTextField WQTE;
  private JLabel OBJ_85;
  private XRiTextField WMAG;
  private XRiTextField TGRAT;
  private JLabel OBJ_78;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
