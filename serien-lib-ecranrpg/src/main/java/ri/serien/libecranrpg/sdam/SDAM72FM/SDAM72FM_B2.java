
package ri.serien.libecranrpg.sdam.SDAM72FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SDAM72FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SDAM72FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTETB.setValeursSelection("**", "**");
    ETT005.setValeursSelection("1", " ");
    ETT004.setValeursSelection("1", " ");
    ETT003.setValeursSelection("1", " ");
    ETT002.setValeursSelection("1", " ");
    ETT001.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS001@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS002@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS003@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS004@")).trim());
    riZoneSortie5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS005@")).trim());
    riZoneSortie6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    riZoneSortie7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    riZoneSortie8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    riZoneSortie9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    riZoneSortie10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // WTETB.setSelected(lexique.HostFieldGetData("WTETB").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTETB.isSelected());
    
    // ETT001.setSelected(lexique.HostFieldGetData("ETT001").equals("1"));
    // ETT002.setSelected(lexique.HostFieldGetData("ETT002").equals("1"));
    // ETT003.setSelected(lexique.HostFieldGetData("ETT003").equals("1"));
    // ETT004.setSelected(lexique.HostFieldGetData("ETT004").equals("1"));
    // ETT005.setSelected(lexique.HostFieldGetData("ETT005").equals("1"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTETB.isSelected())
    // lexique.HostFieldPutData("WTETB", 0, "**");
    // else
    // lexique.HostFieldPutData("WTETB", 0, "**");
    
    // if (ETT001.isSelected())
    // lexique.HostFieldPutData("ETT001", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT001", 0, " ");
    // if (ETT002.isSelected())
    // lexique.HostFieldPutData("ETT002", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT002", 0, " ");
    // if (ETT003.isSelected())
    // lexique.HostFieldPutData("ETT003", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT003", 0, " ");
    // if (ETT004.isSelected())
    // lexique.HostFieldPutData("ETT004", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT004", 0, " ");
    // if (ETT005.isSelected())
    // lexique.HostFieldPutData("ETT005", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT005", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_SEL0 = new JPanel();
    ETT001 = new XRiCheckBox();
    ETT002 = new XRiCheckBox();
    ETT003 = new XRiCheckBox();
    ETT004 = new XRiCheckBox();
    ETT005 = new XRiCheckBox();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    riZoneSortie4 = new RiZoneSortie();
    riZoneSortie5 = new RiZoneSortie();
    riZoneSortie6 = new RiZoneSortie();
    riZoneSortie7 = new RiZoneSortie();
    riZoneSortie8 = new RiZoneSortie();
    riZoneSortie9 = new RiZoneSortie();
    riZoneSortie10 = new RiZoneSortie();
    ETM001 = new XRiTextField();
    ETM002 = new XRiTextField();
    WTETB = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(630, 220));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- ETT001 ----
            ETT001.setName("ETT001");
            P_SEL0.add(ETT001);
            ETT001.setBounds(15, 18, 25, ETT001.getPreferredSize().height);

            //---- ETT002 ----
            ETT002.setName("ETT002");
            P_SEL0.add(ETT002);
            ETT002.setBounds(15, 48, 25, 18);

            //---- ETT003 ----
            ETT003.setName("ETT003");
            P_SEL0.add(ETT003);
            ETT003.setBounds(15, 78, 25, 18);

            //---- ETT004 ----
            ETT004.setName("ETT004");
            P_SEL0.add(ETT004);
            ETT004.setBounds(15, 108, 25, 18);

            //---- ETT005 ----
            ETT005.setName("ETT005");
            P_SEL0.add(ETT005);
            ETT005.setBounds(15, 138, 25, 18);

            //---- riZoneSortie1 ----
            riZoneSortie1.setText("@ETS001@");
            riZoneSortie1.setName("riZoneSortie1");
            P_SEL0.add(riZoneSortie1);
            riZoneSortie1.setBounds(45, 15, 40, riZoneSortie1.getPreferredSize().height);

            //---- riZoneSortie2 ----
            riZoneSortie2.setText("@ETS002@");
            riZoneSortie2.setName("riZoneSortie2");
            P_SEL0.add(riZoneSortie2);
            riZoneSortie2.setBounds(45, 45, 40, 24);

            //---- riZoneSortie3 ----
            riZoneSortie3.setText("@ETS003@");
            riZoneSortie3.setName("riZoneSortie3");
            P_SEL0.add(riZoneSortie3);
            riZoneSortie3.setBounds(45, 75, 40, 24);

            //---- riZoneSortie4 ----
            riZoneSortie4.setText("@ETS004@");
            riZoneSortie4.setName("riZoneSortie4");
            P_SEL0.add(riZoneSortie4);
            riZoneSortie4.setBounds(45, 105, 40, 24);

            //---- riZoneSortie5 ----
            riZoneSortie5.setText("@ETS005@");
            riZoneSortie5.setName("riZoneSortie5");
            P_SEL0.add(riZoneSortie5);
            riZoneSortie5.setBounds(45, 135, 40, 24);

            //---- riZoneSortie6 ----
            riZoneSortie6.setText("@ETN001@");
            riZoneSortie6.setName("riZoneSortie6");
            P_SEL0.add(riZoneSortie6);
            riZoneSortie6.setBounds(90, 15, 260, riZoneSortie6.getPreferredSize().height);

            //---- riZoneSortie7 ----
            riZoneSortie7.setText("@ETN001@");
            riZoneSortie7.setName("riZoneSortie7");
            P_SEL0.add(riZoneSortie7);
            riZoneSortie7.setBounds(90, 45, 260, 24);

            //---- riZoneSortie8 ----
            riZoneSortie8.setText("@ETN001@");
            riZoneSortie8.setName("riZoneSortie8");
            P_SEL0.add(riZoneSortie8);
            riZoneSortie8.setBounds(90, 75, 260, 24);

            //---- riZoneSortie9 ----
            riZoneSortie9.setText("@ETN001@");
            riZoneSortie9.setName("riZoneSortie9");
            P_SEL0.add(riZoneSortie9);
            riZoneSortie9.setBounds(90, 105, 260, 24);

            //---- riZoneSortie10 ----
            riZoneSortie10.setText("@ETN001@");
            riZoneSortie10.setName("riZoneSortie10");
            P_SEL0.add(riZoneSortie10);
            riZoneSortie10.setBounds(90, 135, 260, 24);

            //---- ETM001 ----
            ETM001.setName("ETM001");
            P_SEL0.add(ETM001);
            ETM001.setBounds(355, 13, 60, ETM001.getPreferredSize().height);

            //---- ETM002 ----
            ETM002.setName("ETM002");
            P_SEL0.add(ETM002);
            ETM002.setBounds(355, 43, 60, 28);
          }

          //---- WTETB ----
          WTETB.setText("S\u00e9lection compl\u00e8te");
          WTETB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTETB.setName("WTETB");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(WTETB, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 435, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addComponent(WTETB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_SEL0;
  private XRiCheckBox ETT001;
  private XRiCheckBox ETT002;
  private XRiCheckBox ETT003;
  private XRiCheckBox ETT004;
  private XRiCheckBox ETT005;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private RiZoneSortie riZoneSortie3;
  private RiZoneSortie riZoneSortie4;
  private RiZoneSortie riZoneSortie5;
  private RiZoneSortie riZoneSortie6;
  private RiZoneSortie riZoneSortie7;
  private RiZoneSortie riZoneSortie8;
  private RiZoneSortie riZoneSortie9;
  private RiZoneSortie riZoneSortie10;
  private XRiTextField ETM001;
  private XRiTextField ETM002;
  private XRiCheckBox WTETB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
