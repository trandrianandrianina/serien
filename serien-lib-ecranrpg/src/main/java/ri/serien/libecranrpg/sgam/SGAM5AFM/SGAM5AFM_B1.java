
package ri.serien.libecranrpg.sgam.SGAM5AFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM5AFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM5AFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTTNPU.setValeursSelection("**", "  ");
    NPU02.setValeursSelection("1", " ");
    NPU01.setValeursSelection("1", " ");
    NPU06.setValeursSelection("1", " ");
    NPU05.setValeursSelection("1", " ");
    WTOU.setValeursSelection("**", "  ");
    NPU03.setValeursSelection("1", " ");
    NPU04.setValeursSelection("1", " ");
    REPON1.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMD@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    // WTTNPU.setEnabled( lexique.isPresent("WTTNPU"));
    // WTTNPU.setSelected(lexique.HostFieldGetData("WTTNPU").equalsIgnoreCase("**"));
    // NPU02.setEnabled( lexique.isPresent("NPU02"));
    // NPU02.setSelected(lexique.HostFieldGetData("NPU02").equalsIgnoreCase("1"));
    // NPU01.setEnabled( lexique.isPresent("NPU01"));
    // NPU01.setSelected(lexique.HostFieldGetData("NPU01").equalsIgnoreCase("1"));
    // NPU06.setEnabled( lexique.isPresent("NPU06"));
    // NPU06.setSelected(lexique.HostFieldGetData("NPU06").equalsIgnoreCase("1"));
    // NPU05.setEnabled( lexique.isPresent("NPU05"));
    // NPU05.setSelected(lexique.HostFieldGetData("NPU05").equalsIgnoreCase("1"));
    // WTOU.setVisible( lexique.isPresent("WTOU"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // NPU03.setEnabled( lexique.isPresent("NPU03"));
    // NPU03.setSelected(lexique.HostFieldGetData("NPU03").equalsIgnoreCase("1"));
    // NPU04.setEnabled( lexique.isPresent("NPU04"));
    // NPU04.setSelected(lexique.HostFieldGetData("NPU04").equalsIgnoreCase("1"));
    // REPON1.setEnabled( lexique.isPresent("REPON1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTTNPU.isSelected())
    // lexique.HostFieldPutData("WTTNPU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTTNPU", 0, " ");
    // if (NPU02.isSelected())
    // lexique.HostFieldPutData("NPU02", 0, "1");
    // else
    // lexique.HostFieldPutData("NPU02", 0, " ");
    // if (NPU01.isSelected())
    // lexique.HostFieldPutData("NPU01", 0, "1");
    // else
    // lexique.HostFieldPutData("NPU01", 0, " ");
    // if (NPU06.isSelected())
    // lexique.HostFieldPutData("NPU06", 0, "1");
    // else
    // lexique.HostFieldPutData("NPU06", 0, " ");
    // if (NPU05.isSelected())
    // lexique.HostFieldPutData("NPU05", 0, "1");
    // else
    // lexique.HostFieldPutData("NPU05", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (NPU03.isSelected())
    // lexique.HostFieldPutData("NPU03", 0, "1");
    // else
    // lexique.HostFieldPutData("NPU03", 0, " ");
    // if (NPU04.isSelected())
    // lexique.HostFieldPutData("NPU04", 0, "1");
    // else
    // lexique.HostFieldPutData("NPU04", 0, " ");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_41 = new JXTitledSeparator();
    OBJ_51 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_46 = new RiZoneSortie();
    OBJ_50 = new RiZoneSortie();
    OBJ_44 = new JLabel();
    OBJ_48 = new JLabel();
    DEBFRS = new XRiTextField();
    FINFRS = new XRiTextField();
    OBJ_52 = new JXTitledSeparator();
    REPON1 = new XRiCheckBox();
    NPU04 = new XRiCheckBox();
    NPU03 = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    NPU05 = new XRiCheckBox();
    NPU06 = new XRiCheckBox();
    NPU01 = new XRiCheckBox();
    NPU02 = new XRiCheckBox();
    WTTNPU = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(740, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_41 ----
          OBJ_41.setTitle("S\u00e9lection plage de codes fournisseurs");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_51 ----
          OBJ_51.setTitle("Articles \u00e0 ne pas \u00e9diter");
          OBJ_51.setName("OBJ_51");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_46 ----
            OBJ_46.setText("@FRNOMD@");
            OBJ_46.setName("OBJ_46");
            P_SEL0.add(OBJ_46);
            OBJ_46.setBounds(315, 10, 252, OBJ_46.getPreferredSize().height);

            //---- OBJ_50 ----
            OBJ_50.setText("@FRNOMF@");
            OBJ_50.setName("OBJ_50");
            P_SEL0.add(OBJ_50);
            OBJ_50.setBounds(315, 40, 252, OBJ_50.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("Code fournisseur de d\u00e9but");
            OBJ_44.setName("OBJ_44");
            P_SEL0.add(OBJ_44);
            OBJ_44.setBounds(20, 12, 200, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("Code fournisseur de fin");
            OBJ_48.setName("OBJ_48");
            P_SEL0.add(OBJ_48);
            OBJ_48.setBounds(20, 42, 200, 20);

            //---- DEBFRS ----
            DEBFRS.setComponentPopupMenu(BTD);
            DEBFRS.setName("DEBFRS");
            P_SEL0.add(DEBFRS);
            DEBFRS.setBounds(220, 8, 80, DEBFRS.getPreferredSize().height);

            //---- FINFRS ----
            FINFRS.setComponentPopupMenu(BTD);
            FINFRS.setName("FINFRS");
            P_SEL0.add(FINFRS);
            FINFRS.setBounds(220, 38, 80, FINFRS.getPreferredSize().height);
          }

          //---- OBJ_52 ----
          OBJ_52.setTitle("");
          OBJ_52.setName("OBJ_52");

          //---- REPON1 ----
          REPON1.setText("Date d'application prise en compte");
          REPON1.setComponentPopupMenu(BTD);
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- NPU04 ----
          NPU04.setText("Syst\u00e8me variable secondaire");
          NPU04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NPU04.setName("NPU04");

          //---- NPU03 ----
          NPU03.setText("Syst\u00e8me variable principal");
          NPU03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NPU03.setName("NPU03");

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setComponentPopupMenu(BTD);
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- NPU05 ----
          NPU05.setText("Pr\u00e9-fin de s\u00e9rie");
          NPU05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NPU05.setName("NPU05");

          //---- NPU06 ----
          NPU06.setText("Fin de s\u00e9rie");
          NPU06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NPU06.setName("NPU06");

          //---- NPU01 ----
          NPU01.setText("D\u00e9sactiv\u00e9");
          NPU01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NPU01.setName("NPU01");

          //---- NPU02 ----
          NPU02.setText("Epuis\u00e9");
          NPU02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NPU02.setName("NPU02");

          //---- WTTNPU ----
          WTTNPU.setText("Tous");
          WTTNPU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTTNPU.setName("WTTNPU");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(94, 94, 94)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 581, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 233, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(NPU01, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                .addGap(226, 226, 226)
                .addComponent(NPU04, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(NPU02, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                .addGap(245, 245, 245)
                .addComponent(NPU05, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(NPU03, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)
                .addGap(128, 128, 128)
                .addComponent(NPU06, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(128, 128, 128)
                .addComponent(WTTNPU, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(NPU01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NPU04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(NPU02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NPU05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(NPU03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NPU06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WTTNPU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_41;
  private JXTitledSeparator OBJ_51;
  private JPanel P_SEL0;
  private RiZoneSortie OBJ_46;
  private RiZoneSortie OBJ_50;
  private JLabel OBJ_44;
  private JLabel OBJ_48;
  private XRiTextField DEBFRS;
  private XRiTextField FINFRS;
  private JXTitledSeparator OBJ_52;
  private XRiCheckBox REPON1;
  private XRiCheckBox NPU04;
  private XRiCheckBox NPU03;
  private XRiCheckBox WTOU;
  private XRiCheckBox NPU05;
  private XRiCheckBox NPU06;
  private XRiCheckBox NPU01;
  private XRiCheckBox NPU02;
  private XRiCheckBox WTTNPU;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
