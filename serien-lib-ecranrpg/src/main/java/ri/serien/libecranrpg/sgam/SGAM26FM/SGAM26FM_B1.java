
package ri.serien.libecranrpg.sgam.SGAM26FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.sngroupe.SNGroupe;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM26FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  
  private String[] MAJPV_Value = { "OUI", "NON", };
  
  public SGAM26FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    CQS09.setValeursSelection("X", " ");
    CQS08.setValeursSelection("X", " ");
    CQS07.setValeursSelection("X", " ");
    CQS06.setValeursSelection("X", " ");
    CQS05.setValeursSelection("X", " ");
    CQS04.setValeursSelection("X", " ");
    CQS03.setValeursSelection("X", " ");
    CQS02.setValeursSelection("X", " ");
    CQS01.setValeursSelection("X", " ");
    CQS10.setValeursSelection("X", " ");
    CPRCAT.setValeursSelection("X", " ");
    MAJCQA.setValeursSelection("X", " ");
    MAJREM.setValeursSelection("X", " ");
    
    MAJPV.setValeurs(MAJPV_Value, null);
    
    // Lier les composants groupe, famille et sous-famille début
    snFamilleDebut.lierComposantGroupe(snGroupeDebut);
    snSousFamilleDebut.lierComposantGroupe(snGroupeDebut);
    
    // Lier les composants groupe, famille et sous-famille fin
    snFamilleFin.lierComposantGroupe(snGroupeFin);
    snSousFamilleFin.lierComposantGroupe(snGroupeFin);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    lbZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP1@")).trim());
    lbZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP2@")).trim());
    lbZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP3@")).trim());
    lbZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP4@")).trim());
    lbZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP5@")).trim());
    sNTextField1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Variable GAP
    boolean is92 = lexique.isTrue("92");
    boolean is91 = lexique.isTrue("91");
    boolean is93 = lexique.isTrue("93");
    boolean is94 = lexique.isTrue("94");
    boolean is80 = lexique.isTrue("80");
    
    // Visibilité
    
    pnlArticle.setVisible(is91 || is93 || is94);
    
    pnlSousFamille.setVisible(is92);
    pnlGroupe.setVisible(is92);
    pnlFamille.setVisible(is92);
    lbZP1.setVisible(!lexique.HostFieldGetData("LIBZP1").isEmpty());
    lbZP2.setVisible(!lexique.HostFieldGetData("LIBZP2").isEmpty());
    lbZP3.setVisible(!lexique.HostFieldGetData("LIBZP3").isEmpty());
    lbZP4.setVisible(!lexique.HostFieldGetData("LIBZP4").isEmpty());
    lbZP5.setVisible(!lexique.HostFieldGetData("LIBZP5").isEmpty());
    WZP1.setVisible(lbZP1.isVisible());
    WZP2.setVisible(lbZP2.isVisible());
    WZP3.setVisible(lbZP3.isVisible());
    WZP4.setVisible(lbZP4.isVisible());
    WZP5.setVisible(lbZP5.isVisible());
    pnlZonePersonnalise.setVisible(WZP1.isVisible() || WZP2.isVisible() || WZP3.isVisible() || WZP4.isVisible() || WZP5.isVisible());
    lbZonePersonnalise.setVisible(pnlZonePersonnalise.isVisible());
    
    lbMiseAJour.setVisible(!is80);
    lbClasseABC.setVisible(!is80);
    lbCoefMarge.setVisible(!is80);
    
    // Gestion des libelle
    lbZP1.setText(lexique.HostFieldGetData("LIBZP1"));
    lbZP2.setText(lexique.HostFieldGetData("LIBZP2"));
    lbZP3.setText(lexique.HostFieldGetData("LIBZP3"));
    lbZP4.setText(lexique.HostFieldGetData("LIBZP4"));
    lbZP5.setText(lexique.HostFieldGetData("LIBZP5"));
    
    rbApplicationCoefficient.setSelected(true);
    if (rbApplicationCoefficient.isSelected()) {
      pnlApplicationCoef.setVisible(true);
      pnlConditionReference.setVisible(false);
    }
    UCOEF.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CPRCAT.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS09.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS08.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS07.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS06.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS05.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS04.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS03.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS02.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS01.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    CQS10.setEnabled(lexique.HostFieldGetData("ARTREF").trim().equalsIgnoreCase(""));
    MAJREM.setEnabled(lexique.HostFieldGetData("UCOEF").trim().equalsIgnoreCase(""));
    MAJCQA.setEnabled(lexique.HostFieldGetData("UCOEF").trim().equalsIgnoreCase(""));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charge les composant
    chargerFournisseur();
    chargerSousFamille();
    chargerGroupe();
    chargerFamille();
    chargerArticle();
    chargerSelectionArticle();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snFournisseur.getIdSelection() == null) {
      lexique.HostFieldPutData("WTFRS", 0, "**");
    }
    else {
      snFournisseur.renseignerChampRPG(lexique, "CODFRS");
    }
    snFournisseur.renseignerChampRPG(lexique, "CODFRS");
    snArticle.renseignerChampRPG(lexique, "ARTREF");
    if (pnlSousFamille.isVisible()) {
      if (snSousFamilleDebut.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUT", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUT", 0, "  ");
        snSousFamilleDebut.renseignerChampRPG(lexique, "SFADEB");
        snSousFamilleFin.renseignerChampRPG(lexique, "SFAFIN");
      }
    }
    if (pnlGroupe.isVisible()) {
      if (snGroupeDebut.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUT", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUT", 0, "  ");
        snSousFamilleDebut.renseignerChampRPG(lexique, "GRPDEB");
        snSousFamilleFin.renseignerChampRPG(lexique, "GRPFIN");
      }
    }
    if (pnlFamille.isVisible()) {
      if (snFamilleDebut.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOUT", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUT", 0, "  ");
        snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
        snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
      }
    }
    if (pnlArticle.isVisible()) {
      if (snArticleDebut.getSelection() == null) {
        lexique.HostFieldPutData("WTOUT", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOUT", 0, " ");
        snArticleDebut.renseignerChampRPG(lexique, "ARTDEB");
        snArticleFin.renseignerChampRPG(lexique, "ARTFIN");
      }
    }
  }
  
  /**
   * Charger les fournisseur
   */
  private void chargerFournisseur() {
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "CODFRS");
  }
  
  /**
   * Charger les sous famille
   */
  private void chargerSelectionArticle() {
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "ARTDEB");
    
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "ARTFIN");
  }
  
  /**
   * Charger les article
   */
  private void chargerArticle() {
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "ARTREF");
  }
  
  /**
   * Charger les sous famille
   */
  private void chargerSousFamille() {
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.setTousAutorise(true);
    snSousFamilleDebut.charger(false);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "SFADEB");
    
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.setTousAutorise(true);
    snSousFamilleFin.charger(false);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "SFAFIN");
  }
  
  /**
   * Charger les groupe
   */
  private void chargerGroupe() {
    snGroupeDebut.setSession(getSession());
    snGroupeDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snGroupeDebut.setTousAutorise(true);
    snGroupeDebut.charger(false);
    snGroupeDebut.setSelectionParChampRPG(lexique, "GRPDEB");
    
    snGroupeFin.setSession(getSession());
    snGroupeFin.setIdEtablissement(snEtablissement.getIdSelection());
    snGroupeFin.setTousAutorise(true);
    snGroupeFin.charger(false);
    snGroupeFin.setSelectionParChampRPG(lexique, "GRPFIN");
  }
  
  /**
   * Charger les familles
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbApplicationCoefficientActionPerformed(ActionEvent e) {
    if (rbApplicationCoefficient.isSelected()) {
      pnlApplicationCoef.setVisible(true);
      pnlConditionReference.setVisible(false);
    }
    else {
      pnlApplicationCoef.setVisible(false);
      pnlConditionReference.setVisible(true);
    }
  }
  
  private void rbConditionReferenceActionPerformed(ActionEvent e) {
    if (rbConditionReference.isSelected()) {
      pnlApplicationCoef.setVisible(false);
      pnlConditionReference.setVisible(true);
    }
    else {
      pnlApplicationCoef.setVisible(true);
      pnlConditionReference.setVisible(false);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    sNLabelChamp1 = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    pnlGroupe = new SNPanel();
    lbGroupeDebut = new SNLabelChamp();
    snGroupeDebut = new SNGroupe();
    lbGroupeFin = new SNLabelChamp();
    snGroupeFin = new SNGroupe();
    pnlFamille = new SNPanel();
    sNLabelChamp5 = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    sNLabelChamp6 = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    pnlSousFamille = new SNPanel();
    lbSousFamilleDebut = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamilleFin = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    pnlArticle = new SNPanel();
    lbArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticleFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    lbZonePersonnalise = new SNLabelChamp();
    pnlZonePersonnalise = new SNPanel();
    lbZP1 = new SNLabelChamp();
    WZP1 = new XRiTextField();
    lbZP2 = new SNLabelChamp();
    WZP2 = new XRiTextField();
    lbZP3 = new SNLabelChamp();
    WZP3 = new XRiTextField();
    lbZP4 = new SNLabelChamp();
    WZP4 = new XRiTextField();
    lbZP5 = new SNLabelChamp();
    WZP5 = new XRiTextField();
    sNLabelChamp7 = new SNLabelChamp();
    URON = new XRiTextField();
    pnlMiseAJourPirx = new SNPanel();
    lbMiseAJour = new SNLabelChamp();
    MAJPV = new XRiComboBox();
    lbClasseABC = new SNLabelChamp();
    WABC = new XRiTextField();
    lbCoefMarge = new SNLabelChamp();
    WKPV = new XRiTextField();
    lbNouvelleDateApplciation = new SNLabelChamp();
    UDATAP = new XRiCalendrier();
    lbNouvelleDateValidite = new SNLabelChamp();
    DATVAL = new XRiCalendrier();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    sNLabelChamp3 = new SNLabelChamp();
    sNTextField1 = new SNTexte();
    pnlCoefficient = new SNPanelTitre();
    rbApplicationCoefficient = new JRadioButton();
    pnlApplicationCoef = new SNPanel();
    lbCoefAppliquer = new SNLabelChamp();
    UCOEF = new XRiTextField();
    CPRCAT = new XRiCheckBox();
    lbConditionQuantité = new SNLabelChamp();
    CQS01 = new XRiCheckBox();
    CQS02 = new XRiCheckBox();
    CQS03 = new XRiCheckBox();
    CQS04 = new XRiCheckBox();
    CQS05 = new XRiCheckBox();
    CQS06 = new XRiCheckBox();
    CQS07 = new XRiCheckBox();
    CQS08 = new XRiCheckBox();
    CQS09 = new XRiCheckBox();
    CQS10 = new XRiCheckBox();
    rbConditionReference = new JRadioButton();
    pnlConditionReference = new SNPanel();
    lbCodeArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbDateApplication = new SNLabelChamp();
    DAPREF = new XRiCalendrier();
    MAJCQA = new XRiCheckBox();
    MAJREM = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText(" Mise \u00e0 jour des conditions d'achat prix catalogue/conditions sur quantit\u00e9s");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelection ========
          {
            pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSelection.setName("pnlCritereSelection");
            pnlCritereSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelection.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- sNLabelChamp1 ----
            sNLabelChamp1.setText("Fournisseur");
            sNLabelChamp1.setName("sNLabelChamp1");
            pnlCritereSelection.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseur ----
            snFournisseur.setName("snFournisseur");
            pnlCritereSelection.add(snFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlGroupe ========
            {
              pnlGroupe.setName("pnlGroupe");
              pnlGroupe.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlGroupe.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlGroupe.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlGroupe.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlGroupe.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- lbGroupeDebut ----
              lbGroupeDebut.setText("Groupe de d\u00e9but");
              lbGroupeDebut.setPreferredSize(new Dimension(176, 30));
              lbGroupeDebut.setMinimumSize(new Dimension(176, 30));
              lbGroupeDebut.setMaximumSize(new Dimension(176, 30));
              lbGroupeDebut.setName("lbGroupeDebut");
              pnlGroupe.add(lbGroupeDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snGroupeDebut ----
              snGroupeDebut.setName("snGroupeDebut");
              pnlGroupe.add(snGroupeDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbGroupeFin ----
              lbGroupeFin.setText("Groupe de fin");
              lbGroupeFin.setPreferredSize(new Dimension(176, 30));
              lbGroupeFin.setMinimumSize(new Dimension(176, 30));
              lbGroupeFin.setMaximumSize(new Dimension(176, 30));
              lbGroupeFin.setName("lbGroupeFin");
              pnlGroupe.add(lbGroupeFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snGroupeFin ----
              snGroupeFin.setName("snGroupeFin");
              pnlGroupe.add(snGroupeFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlGroupe, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlFamille ========
            {
              pnlFamille.setName("pnlFamille");
              pnlFamille.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlFamille.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlFamille.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlFamille.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlFamille.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- sNLabelChamp5 ----
              sNLabelChamp5.setText("Famille de d\u00e9but");
              sNLabelChamp5.setPreferredSize(new Dimension(176, 30));
              sNLabelChamp5.setMinimumSize(new Dimension(176, 30));
              sNLabelChamp5.setMaximumSize(new Dimension(176, 30));
              sNLabelChamp5.setName("sNLabelChamp5");
              pnlFamille.add(sNLabelChamp5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snFamilleDebut ----
              snFamilleDebut.setName("snFamilleDebut");
              pnlFamille.add(snFamilleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp6 ----
              sNLabelChamp6.setText("Famille de fin");
              sNLabelChamp6.setPreferredSize(new Dimension(176, 30));
              sNLabelChamp6.setMinimumSize(new Dimension(176, 30));
              sNLabelChamp6.setMaximumSize(new Dimension(176, 30));
              sNLabelChamp6.setName("sNLabelChamp6");
              pnlFamille.add(sNLabelChamp6, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snFamilleFin ----
              snFamilleFin.setName("snFamilleFin");
              pnlFamille.add(snFamilleFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlFamille, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSousFamille ========
            {
              pnlSousFamille.setName("pnlSousFamille");
              pnlSousFamille.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSousFamille.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlSousFamille.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlSousFamille.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSousFamille.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- lbSousFamilleDebut ----
              lbSousFamilleDebut.setText("Sous famille de d\u00e9but");
              lbSousFamilleDebut.setPreferredSize(new Dimension(176, 30));
              lbSousFamilleDebut.setMinimumSize(new Dimension(176, 30));
              lbSousFamilleDebut.setMaximumSize(new Dimension(176, 30));
              lbSousFamilleDebut.setName("lbSousFamilleDebut");
              pnlSousFamille.add(lbSousFamilleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snSousFamilleDebut ----
              snSousFamilleDebut.setName("snSousFamilleDebut");
              pnlSousFamille.add(snSousFamilleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbSousFamilleFin ----
              lbSousFamilleFin.setText("Sous famille de fin");
              lbSousFamilleFin.setPreferredSize(new Dimension(176, 30));
              lbSousFamilleFin.setMinimumSize(new Dimension(176, 30));
              lbSousFamilleFin.setMaximumSize(new Dimension(176, 30));
              lbSousFamilleFin.setName("lbSousFamilleFin");
              pnlSousFamille.add(lbSousFamilleFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snSousFamilleFin ----
              snSousFamilleFin.setName("snSousFamilleFin");
              pnlSousFamille.add(snSousFamilleFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlSousFamille, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlArticle ========
            {
              pnlArticle.setName("pnlArticle");
              pnlArticle.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlArticle.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- lbArticleDebut ----
              lbArticleDebut.setText("Article d\u00e9but");
              lbArticleDebut.setPreferredSize(new Dimension(176, 30));
              lbArticleDebut.setMinimumSize(new Dimension(176, 30));
              lbArticleDebut.setMaximumSize(new Dimension(176, 30));
              lbArticleDebut.setName("lbArticleDebut");
              pnlArticle.add(lbArticleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snArticleDebut ----
              snArticleDebut.setName("snArticleDebut");
              pnlArticle.add(snArticleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbArticleFin ----
              lbArticleFin.setText("Article fin");
              lbArticleFin.setPreferredSize(new Dimension(176, 30));
              lbArticleFin.setMinimumSize(new Dimension(176, 30));
              lbArticleFin.setMaximumSize(new Dimension(176, 30));
              lbArticleFin.setName("lbArticleFin");
              pnlArticle.add(lbArticleFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snArticleFin ----
              snArticleFin.setName("snArticleFin");
              pnlArticle.add(snArticleFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlArticle, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbZonePersonnalise ----
            lbZonePersonnalise.setText("Zone personnalis\u00e9e");
            lbZonePersonnalise.setName("lbZonePersonnalise");
            pnlCritereSelection.add(lbZonePersonnalise, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlZonePersonnalise ========
            {
              pnlZonePersonnalise.setName("pnlZonePersonnalise");
              pnlZonePersonnalise.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).columnWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlZonePersonnalise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbZP1 ----
              lbZP1.setText("@LIBZP1@");
              lbZP1.setMinimumSize(new Dimension(25, 30));
              lbZP1.setMaximumSize(new Dimension(25, 30));
              lbZP1.setPreferredSize(new Dimension(25, 30));
              lbZP1.setName("lbZP1");
              pnlZonePersonnalise.add(lbZP1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WZP1 ----
              WZP1.setFont(new Font("sansserif", Font.PLAIN, 14));
              WZP1.setPreferredSize(new Dimension(30, 30));
              WZP1.setMinimumSize(new Dimension(30, 30));
              WZP1.setMaximumSize(new Dimension(30, 30));
              WZP1.setName("WZP1");
              pnlZonePersonnalise.add(WZP1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbZP2 ----
              lbZP2.setText("@LIBZP2@");
              lbZP2.setMaximumSize(new Dimension(25, 30));
              lbZP2.setMinimumSize(new Dimension(25, 30));
              lbZP2.setPreferredSize(new Dimension(25, 30));
              lbZP2.setName("lbZP2");
              pnlZonePersonnalise.add(lbZP2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WZP2 ----
              WZP2.setFont(new Font("sansserif", Font.PLAIN, 14));
              WZP2.setPreferredSize(new Dimension(30, 30));
              WZP2.setMinimumSize(new Dimension(30, 30));
              WZP2.setMaximumSize(new Dimension(30, 30));
              WZP2.setName("WZP2");
              pnlZonePersonnalise.add(WZP2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbZP3 ----
              lbZP3.setText("@LIBZP3@");
              lbZP3.setPreferredSize(new Dimension(25, 30));
              lbZP3.setMinimumSize(new Dimension(25, 30));
              lbZP3.setMaximumSize(new Dimension(25, 30));
              lbZP3.setName("lbZP3");
              pnlZonePersonnalise.add(lbZP3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WZP3 ----
              WZP3.setFont(new Font("sansserif", Font.PLAIN, 14));
              WZP3.setPreferredSize(new Dimension(30, 30));
              WZP3.setMinimumSize(new Dimension(30, 30));
              WZP3.setMaximumSize(new Dimension(30, 30));
              WZP3.setName("WZP3");
              pnlZonePersonnalise.add(WZP3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbZP4 ----
              lbZP4.setText("@LIBZP4@");
              lbZP4.setMaximumSize(new Dimension(25, 30));
              lbZP4.setMinimumSize(new Dimension(25, 30));
              lbZP4.setPreferredSize(new Dimension(25, 30));
              lbZP4.setName("lbZP4");
              pnlZonePersonnalise.add(lbZP4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WZP4 ----
              WZP4.setFont(new Font("sansserif", Font.PLAIN, 14));
              WZP4.setPreferredSize(new Dimension(30, 30));
              WZP4.setMinimumSize(new Dimension(30, 30));
              WZP4.setMaximumSize(new Dimension(30, 30));
              WZP4.setName("WZP4");
              pnlZonePersonnalise.add(WZP4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbZP5 ----
              lbZP5.setText("@LIBZP5@");
              lbZP5.setPreferredSize(new Dimension(25, 30));
              lbZP5.setMinimumSize(new Dimension(25, 30));
              lbZP5.setMaximumSize(new Dimension(25, 30));
              lbZP5.setName("lbZP5");
              pnlZonePersonnalise.add(lbZP5, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WZP5 ----
              WZP5.setFont(new Font("sansserif", Font.PLAIN, 14));
              WZP5.setPreferredSize(new Dimension(30, 30));
              WZP5.setMinimumSize(new Dimension(30, 30));
              WZP5.setMaximumSize(new Dimension(30, 30));
              WZP5.setName("WZP5");
              pnlZonePersonnalise.add(WZP5, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlZonePersonnalise, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp7 ----
            sNLabelChamp7.setText("Niveau d'arrondi");
            sNLabelChamp7.setName("sNLabelChamp7");
            pnlCritereSelection.add(sNLabelChamp7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- URON ----
            URON.setPreferredSize(new Dimension(25, 30));
            URON.setMinimumSize(new Dimension(25, 30));
            URON.setMaximumSize(new Dimension(25, 30));
            URON.setFont(new Font("sansserif", Font.PLAIN, 14));
            URON.setName("URON");
            pnlCritereSelection.add(URON, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlMiseAJourPirx ========
            {
              pnlMiseAJourPirx.setName("pnlMiseAJourPirx");
              pnlMiseAJourPirx.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMiseAJourPirx.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlMiseAJourPirx.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMiseAJourPirx.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMiseAJourPirx.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbMiseAJour ----
              lbMiseAJour.setText("Mise \u00e0 jour du prix de vente");
              lbMiseAJour.setMaximumSize(new Dimension(176, 30));
              lbMiseAJour.setMinimumSize(new Dimension(176, 30));
              lbMiseAJour.setPreferredSize(new Dimension(176, 30));
              lbMiseAJour.setName("lbMiseAJour");
              pnlMiseAJourPirx.add(lbMiseAJour, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAJPV ----
              MAJPV.setModel(new DefaultComboBoxModel(new String[] { "OUI", "NON" }));
              MAJPV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              MAJPV.setPreferredSize(new Dimension(80, 30));
              MAJPV.setMinimumSize(new Dimension(80, 30));
              MAJPV.setFont(new Font("sansserif", Font.PLAIN, 14));
              MAJPV.setMaximumSize(new Dimension(80, 30));
              MAJPV.setName("MAJPV");
              pnlMiseAJourPirx.add(MAJPV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbClasseABC ----
              lbClasseABC.setText("Classe ABC");
              lbClasseABC.setMinimumSize(new Dimension(75, 30));
              lbClasseABC.setMaximumSize(new Dimension(75, 30));
              lbClasseABC.setPreferredSize(new Dimension(75, 30));
              lbClasseABC.setName("lbClasseABC");
              pnlMiseAJourPirx.add(lbClasseABC, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WABC ----
              WABC.setPreferredSize(new Dimension(25, 30));
              WABC.setMinimumSize(new Dimension(25, 30));
              WABC.setMaximumSize(new Dimension(25, 30));
              WABC.setName("WABC");
              pnlMiseAJourPirx.add(WABC, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbCoefMarge ----
              lbCoefMarge.setText("Coef marge");
              lbCoefMarge.setMinimumSize(new Dimension(75, 30));
              lbCoefMarge.setPreferredSize(new Dimension(75, 30));
              lbCoefMarge.setMaximumSize(new Dimension(75, 30));
              lbCoefMarge.setName("lbCoefMarge");
              pnlMiseAJourPirx.add(lbCoefMarge, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WKPV ----
              WKPV.setPreferredSize(new Dimension(100, 30));
              WKPV.setMinimumSize(new Dimension(100, 30));
              WKPV.setMaximumSize(new Dimension(100, 30));
              WKPV.setName("WKPV");
              pnlMiseAJourPirx.add(WKPV, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelection.add(pnlMiseAJourPirx, new GridBagConstraints(0, 7, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNouvelleDateApplciation ----
            lbNouvelleDateApplciation.setText("Nouvelle date d'application");
            lbNouvelleDateApplciation.setPreferredSize(new Dimension(176, 30));
            lbNouvelleDateApplciation.setMinimumSize(new Dimension(176, 30));
            lbNouvelleDateApplciation.setMaximumSize(new Dimension(176, 30));
            lbNouvelleDateApplciation.setName("lbNouvelleDateApplciation");
            pnlCritereSelection.add(lbNouvelleDateApplciation, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- UDATAP ----
            UDATAP.setFont(new Font("sansserif", Font.PLAIN, 14));
            UDATAP.setPreferredSize(new Dimension(110, 30));
            UDATAP.setMinimumSize(new Dimension(110, 30));
            UDATAP.setMaximumSize(new Dimension(110, 30));
            UDATAP.setName("UDATAP");
            pnlCritereSelection.add(UDATAP, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNouvelleDateValidite ----
            lbNouvelleDateValidite.setText("Nouvelle date de validit\u00e9");
            lbNouvelleDateValidite.setPreferredSize(new Dimension(176, 30));
            lbNouvelleDateValidite.setMinimumSize(new Dimension(176, 30));
            lbNouvelleDateValidite.setMaximumSize(new Dimension(176, 30));
            lbNouvelleDateValidite.setName("lbNouvelleDateValidite");
            pnlCritereSelection.add(lbNouvelleDateValidite, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DATVAL ----
            DATVAL.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATVAL.setPreferredSize(new Dimension(110, 30));
            DATVAL.setMinimumSize(new Dimension(110, 30));
            DATVAL.setMaximumSize(new Dimension(110, 30));
            DATVAL.setName("DATVAL");
            pnlCritereSelection.add(DATVAL, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp3 ----
            sNLabelChamp3.setText("P\u00e9riode en cours");
            sNLabelChamp3.setName("sNLabelChamp3");
            pnlEtablissement.add(sNLabelChamp3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- sNTextField1 ----
            sNTextField1.setText("@WENCX@");
            sNTextField1.setEnabled(false);
            sNTextField1.setPreferredSize(new Dimension(260, 30));
            sNTextField1.setMinimumSize(new Dimension(260, 30));
            sNTextField1.setMaximumSize(new Dimension(260, 30));
            sNTextField1.setName("sNTextField1");
            pnlEtablissement.add(sNTextField1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlCoefficient ========
          {
            pnlCoefficient.setTitre("Coefficient");
            pnlCoefficient.setName("pnlCoefficient");
            pnlCoefficient.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCoefficient.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlCoefficient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCoefficient.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCoefficient.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- rbApplicationCoefficient ----
            rbApplicationCoefficient.setText("Application de coefficient");
            rbApplicationCoefficient.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbApplicationCoefficient.setPreferredSize(new Dimension(150, 30));
            rbApplicationCoefficient.setMinimumSize(new Dimension(150, 30));
            rbApplicationCoefficient.setMaximumSize(new Dimension(150, 30));
            rbApplicationCoefficient.setName("rbApplicationCoefficient");
            rbApplicationCoefficient.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbApplicationCoefficientActionPerformed(e);
              }
            });
            pnlCoefficient.add(rbApplicationCoefficient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlApplicationCoef ========
            {
              pnlApplicationCoef.setName("pnlApplicationCoef");
              pnlApplicationCoef.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlApplicationCoef.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlApplicationCoef.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlApplicationCoef.getLayout()).columnWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlApplicationCoef.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbCoefAppliquer ----
              lbCoefAppliquer.setText("Coefficient \u00e0 appliquer");
              lbCoefAppliquer.setPreferredSize(new Dimension(176, 30));
              lbCoefAppliquer.setMinimumSize(new Dimension(176, 30));
              lbCoefAppliquer.setMaximumSize(new Dimension(176, 30));
              lbCoefAppliquer.setName("lbCoefAppliquer");
              pnlApplicationCoef.add(lbCoefAppliquer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- UCOEF ----
              UCOEF.setPreferredSize(new Dimension(75, 30));
              UCOEF.setMinimumSize(new Dimension(75, 30));
              UCOEF.setMaximumSize(new Dimension(75, 30));
              UCOEF.setName("UCOEF");
              pnlApplicationCoef.add(UCOEF, new GridBagConstraints(1, 0, 5, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- CPRCAT ----
              CPRCAT.setText("Prix catalogue");
              CPRCAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CPRCAT.setFont(new Font("sansserif", Font.PLAIN, 14));
              CPRCAT.setMinimumSize(new Dimension(150, 30));
              CPRCAT.setMaximumSize(new Dimension(150, 30));
              CPRCAT.setPreferredSize(new Dimension(150, 30));
              CPRCAT.setName("CPRCAT");
              pnlApplicationCoef.add(CPRCAT, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbConditionQuantité ----
              lbConditionQuantité.setText("Conditions sur quantit\u00e9s");
              lbConditionQuantité.setMinimumSize(new Dimension(176, 30));
              lbConditionQuantité.setMaximumSize(new Dimension(176, 30));
              lbConditionQuantité.setPreferredSize(new Dimension(176, 30));
              lbConditionQuantité.setName("lbConditionQuantit\u00e9");
              pnlApplicationCoef.add(lbConditionQuantité, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS01 ----
              CQS01.setText("1");
              CQS01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS01.setName("CQS01");
              pnlApplicationCoef.add(CQS01, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS02 ----
              CQS02.setText("2");
              CQS02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS02.setName("CQS02");
              pnlApplicationCoef.add(CQS02, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS03 ----
              CQS03.setText("3");
              CQS03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS03.setName("CQS03");
              pnlApplicationCoef.add(CQS03, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS04 ----
              CQS04.setText("4");
              CQS04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS04.setName("CQS04");
              pnlApplicationCoef.add(CQS04, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS05 ----
              CQS05.setText("5");
              CQS05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS05.setName("CQS05");
              pnlApplicationCoef.add(CQS05, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS06 ----
              CQS06.setText("6");
              CQS06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS06.setName("CQS06");
              pnlApplicationCoef.add(CQS06, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS07 ----
              CQS07.setText("7");
              CQS07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS07.setName("CQS07");
              pnlApplicationCoef.add(CQS07, new GridBagConstraints(7, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS08 ----
              CQS08.setText("8");
              CQS08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS08.setName("CQS08");
              pnlApplicationCoef.add(CQS08, new GridBagConstraints(8, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS09 ----
              CQS09.setText("9");
              CQS09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS09.setName("CQS09");
              pnlApplicationCoef.add(CQS09, new GridBagConstraints(9, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CQS10 ----
              CQS10.setText("10");
              CQS10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CQS10.setName("CQS10");
              pnlApplicationCoef.add(CQS10, new GridBagConstraints(10, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCoefficient.add(pnlApplicationCoef, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbConditionReference ----
            rbConditionReference.setText("Condition de r\u00e9f\u00e9rence");
            rbConditionReference.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbConditionReference.setMinimumSize(new Dimension(167, 30));
            rbConditionReference.setPreferredSize(new Dimension(167, 30));
            rbConditionReference.setMaximumSize(new Dimension(167, 30));
            rbConditionReference.setName("rbConditionReference");
            rbConditionReference.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbConditionReferenceActionPerformed(e);
              }
            });
            pnlCoefficient.add(rbConditionReference, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlConditionReference ========
            {
              pnlConditionReference.setName("pnlConditionReference");
              pnlConditionReference.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlConditionReference.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlConditionReference.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlConditionReference.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlConditionReference.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbCodeArticle ----
              lbCodeArticle.setText("Code article");
              lbCodeArticle.setMaximumSize(new Dimension(176, 30));
              lbCodeArticle.setMinimumSize(new Dimension(176, 30));
              lbCodeArticle.setPreferredSize(new Dimension(176, 30));
              lbCodeArticle.setName("lbCodeArticle");
              pnlConditionReference.add(lbCodeArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snArticle ----
              snArticle.setName("snArticle");
              pnlConditionReference.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDateApplication ----
              lbDateApplication.setText("Date d'application");
              lbDateApplication.setMaximumSize(new Dimension(176, 30));
              lbDateApplication.setMinimumSize(new Dimension(176, 30));
              lbDateApplication.setPreferredSize(new Dimension(176, 30));
              lbDateApplication.setName("lbDateApplication");
              pnlConditionReference.add(lbDateApplication, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- DAPREF ----
              DAPREF.setFont(new Font("sansserif", Font.PLAIN, 14));
              DAPREF.setPreferredSize(new Dimension(110, 30));
              DAPREF.setMinimumSize(new Dimension(110, 30));
              DAPREF.setMaximumSize(new Dimension(110, 30));
              DAPREF.setName("DAPREF");
              pnlConditionReference.add(DAPREF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- MAJCQA ----
              MAJCQA.setText("Conditions quantitatives");
              MAJCQA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              MAJCQA.setMaximumSize(new Dimension(173, 30));
              MAJCQA.setMinimumSize(new Dimension(173, 30));
              MAJCQA.setFont(new Font("sansserif", Font.PLAIN, 14));
              MAJCQA.setPreferredSize(new Dimension(173, 30));
              MAJCQA.setName("MAJCQA");
              pnlConditionReference.add(MAJCQA, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAJREM ----
              MAJREM.setText("Remises sur prix catalogue");
              MAJREM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              MAJREM.setFont(new Font("sansserif", Font.PLAIN, 14));
              MAJREM.setPreferredSize(new Dimension(194, 30));
              MAJREM.setMinimumSize(new Dimension(194, 30));
              MAJREM.setMaximumSize(new Dimension(194, 30));
              MAJREM.setName("MAJREM");
              pnlConditionReference.add(MAJREM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCoefficient.add(pnlConditionReference, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlCoefficient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbApplicationCoefficient);
    buttonGroup1.add(rbConditionReference);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp sNLabelChamp1;
  private SNFournisseur snFournisseur;
  private SNPanel pnlGroupe;
  private SNLabelChamp lbGroupeDebut;
  private SNGroupe snGroupeDebut;
  private SNLabelChamp lbGroupeFin;
  private SNGroupe snGroupeFin;
  private SNPanel pnlFamille;
  private SNLabelChamp sNLabelChamp5;
  private SNFamille snFamilleDebut;
  private SNLabelChamp sNLabelChamp6;
  private SNFamille snFamilleFin;
  private SNPanel pnlSousFamille;
  private SNLabelChamp lbSousFamilleDebut;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSousFamilleFin;
  private SNSousFamille snSousFamilleFin;
  private SNPanel pnlArticle;
  private SNLabelChamp lbArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticleFin;
  private SNArticle snArticleFin;
  private SNLabelChamp lbZonePersonnalise;
  private SNPanel pnlZonePersonnalise;
  private SNLabelChamp lbZP1;
  private XRiTextField WZP1;
  private SNLabelChamp lbZP2;
  private XRiTextField WZP2;
  private SNLabelChamp lbZP3;
  private XRiTextField WZP3;
  private SNLabelChamp lbZP4;
  private XRiTextField WZP4;
  private SNLabelChamp lbZP5;
  private XRiTextField WZP5;
  private SNLabelChamp sNLabelChamp7;
  private XRiTextField URON;
  private SNPanel pnlMiseAJourPirx;
  private SNLabelChamp lbMiseAJour;
  private XRiComboBox MAJPV;
  private SNLabelChamp lbClasseABC;
  private XRiTextField WABC;
  private SNLabelChamp lbCoefMarge;
  private XRiTextField WKPV;
  private SNLabelChamp lbNouvelleDateApplciation;
  private XRiCalendrier UDATAP;
  private SNLabelChamp lbNouvelleDateValidite;
  private XRiCalendrier DATVAL;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp sNLabelChamp3;
  private SNTexte sNTextField1;
  private SNPanelTitre pnlCoefficient;
  private JRadioButton rbApplicationCoefficient;
  private SNPanel pnlApplicationCoef;
  private SNLabelChamp lbCoefAppliquer;
  private XRiTextField UCOEF;
  private XRiCheckBox CPRCAT;
  private SNLabelChamp lbConditionQuantité;
  private XRiCheckBox CQS01;
  private XRiCheckBox CQS02;
  private XRiCheckBox CQS03;
  private XRiCheckBox CQS04;
  private XRiCheckBox CQS05;
  private XRiCheckBox CQS06;
  private XRiCheckBox CQS07;
  private XRiCheckBox CQS08;
  private XRiCheckBox CQS09;
  private XRiCheckBox CQS10;
  private JRadioButton rbConditionReference;
  private SNPanel pnlConditionReference;
  private SNLabelChamp lbCodeArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbDateApplication;
  private XRiCalendrier DAPREF;
  private XRiCheckBox MAJCQA;
  private XRiCheckBox MAJREM;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
