
package ri.serien.libecranrpg.sgam.SGAM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM05FM_B7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM05FM_B7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_19.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBD@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBF@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBD@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBF@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMD@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    
    // WTOU.setEnabled(false);
    
    // *** Visibilité des panels
    panel2.setVisible(!WTOU.isSelected() && lexique.isTrue("96"));
    panel7.setVisible(!WTOU.isSelected() && lexique.isTrue("93"));
    panel6.setVisible(!WTOU.isSelected() && lexique.isTrue("94"));
    panel3.setVisible(!WTOU.isSelected() && lexique.isTrue("95"));
    panel4.setVisible(!WTOU.isSelected() && lexique.isTrue("91"));
    panel5.setVisible(!WTOU.isSelected() && lexique.isTrue("92"));
    
    // panel2.setEnabled(false);
    // panel6.setEnabled(false);
    // panel7.setEnabled(false);
    // panel3.setEnabled(false);
    // panel4.setEnabled(false);
    // panel5.setEnabled(false);
    
    // **** Titres
    if (lexique.isTrue("96")) {
      OBJ_19.setTitle("Plage de codes sous-famille");
    }
    if (lexique.isTrue("93")) {
      OBJ_19.setTitle("Plage de codes groupes - famille");
    }
    if (lexique.isTrue("91")) {
      OBJ_19.setTitle("Plage de mots de classement 1");
    }
    if (lexique.isTrue("92")) {
      OBJ_19.setTitle("Plage de codes articles");
    }
    if (lexique.isTrue("94")) {
      OBJ_19.setTitle("Plage de mots de classement 2");
    }
    if (lexique.isTrue("95")) {
      OBJ_19.setTitle("Plage de codes fournisseurs");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    WTOU = new XRiCheckBox();
    panel5 = new JPanel();
    ARTD2 = new XRiTextField();
    ARTF2 = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_26 = new RiZoneSortie();
    OBJ_27 = new RiZoneSortie();
    panel2 = new JPanel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    DEBIAE = new XRiTextField();
    FINIAE = new XRiTextField();
    panel7 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    DEBFAM = new XRiTextField();
    FINFAM = new XRiTextField();
    OBJ_25 = new RiZoneSortie();
    OBJ_38 = new RiZoneSortie();
    panel3 = new JPanel();
    DEBFRS = new XRiTextField();
    FINFRS = new XRiTextField();
    OBJ_32 = new RiZoneSortie();
    OBJ_34 = new RiZoneSortie();
    OBJ_35 = new JLabel();
    OBJ_37 = new JLabel();
    panel4 = new JPanel();
    DEBIAC = new XRiTextField();
    FINIAC = new XRiTextField();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    panel6 = new JPanel();
    OBJ_41 = new JLabel();
    DEBIAK = new XRiTextField();
    FINIAK = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_33 = new JLabel();
    DATCND = new XRiCalendrier();
    OBJ_36 = new JLabel();
    DATCNF = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setEnabled(false);
          bouton_etablissement.setName("bouton_etablissement");

          //---- OBJ_19 ----
          OBJ_19.setTitle("@PLAG@");
          OBJ_19.setName("OBJ_19");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Dates d'application des conditions d'achat");
          OBJ_22.setName("OBJ_22");

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setComponentPopupMenu(BTD);
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");

          //======== panel5 ========
          {
            panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- ARTD2 ----
            ARTD2.setComponentPopupMenu(BTD);
            ARTD2.setName("ARTD2");
            panel5.add(ARTD2);
            ARTD2.setBounds(125, 10, 210, ARTD2.getPreferredSize().height);

            //---- ARTF2 ----
            ARTF2.setComponentPopupMenu(BTD);
            ARTF2.setName("ARTF2");
            panel5.add(ARTF2);
            ARTF2.setBounds(125, 40, 210, ARTF2.getPreferredSize().height);

            //---- OBJ_28 ----
            OBJ_28.setText("Code de d\u00e9but");
            OBJ_28.setName("OBJ_28");
            panel5.add(OBJ_28);
            OBJ_28.setBounds(20, 14, 105, 20);

            //---- OBJ_29 ----
            OBJ_29.setText("Code de fin");
            OBJ_29.setName("OBJ_29");
            panel5.add(OBJ_29);
            OBJ_29.setBounds(20, 44, 105, 20);

            //---- OBJ_26 ----
            OBJ_26.setText("@A1LIBD@");
            OBJ_26.setName("OBJ_26");
            panel5.add(OBJ_26);
            OBJ_26.setBounds(340, 12, 234, OBJ_26.getPreferredSize().height);

            //---- OBJ_27 ----
            OBJ_27.setText("@A1LIBF@");
            OBJ_27.setName("OBJ_27");
            panel5.add(OBJ_27);
            OBJ_27.setBounds(340, 42, 234, OBJ_27.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_30 ----
            OBJ_30.setText("Sous-famille de d\u00e9but");
            OBJ_30.setName("OBJ_30");
            panel2.add(OBJ_30);
            OBJ_30.setBounds(20, 9, 165, 20);

            //---- OBJ_31 ----
            OBJ_31.setText("Sous-famille de fin");
            OBJ_31.setName("OBJ_31");
            panel2.add(OBJ_31);
            OBJ_31.setBounds(20, 39, 165, 20);

            //---- DEBIAE ----
            DEBIAE.setComponentPopupMenu(BTD);
            DEBIAE.setName("DEBIAE");
            panel2.add(DEBIAE);
            DEBIAE.setBounds(185, 5, 60, DEBIAE.getPreferredSize().height);

            //---- FINIAE ----
            FINIAE.setComponentPopupMenu(BTD);
            FINIAE.setName("FINIAE");
            panel2.add(FINIAE);
            FINIAE.setBounds(185, 35, 60, FINIAE.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel7 ========
          {
            panel7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- label1 ----
            label1.setText("Groupe - Famille de d\u00e9but");
            label1.setName("label1");
            panel7.add(label1);
            label1.setBounds(15, 9, 170, 20);

            //---- label2 ----
            label2.setText("Groupe - Famille de fin");
            label2.setName("label2");
            panel7.add(label2);
            label2.setBounds(15, 39, 170, 20);

            //---- DEBFAM ----
            DEBFAM.setComponentPopupMenu(BTD);
            DEBFAM.setName("DEBFAM");
            panel7.add(DEBFAM);
            DEBFAM.setBounds(185, 5, 40, DEBFAM.getPreferredSize().height);

            //---- FINFAM ----
            FINFAM.setComponentPopupMenu(BTD);
            FINFAM.setName("FINFAM");
            panel7.add(FINFAM);
            FINFAM.setBounds(185, 35, 40, FINFAM.getPreferredSize().height);

            //---- OBJ_25 ----
            OBJ_25.setText("@FALIBD@");
            OBJ_25.setName("OBJ_25");
            panel7.add(OBJ_25);
            OBJ_25.setBounds(235, 7, 234, OBJ_25.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("@FALIBF@");
            OBJ_38.setName("OBJ_38");
            panel7.add(OBJ_38);
            OBJ_38.setBounds(235, 37, 234, OBJ_38.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- DEBFRS ----
            DEBFRS.setComponentPopupMenu(BTD);
            DEBFRS.setName("DEBFRS");
            panel3.add(DEBFRS);
            DEBFRS.setBounds(190, 10, 80, DEBFRS.getPreferredSize().height);

            //---- FINFRS ----
            FINFRS.setComponentPopupMenu(BTD);
            FINFRS.setName("FINFRS");
            panel3.add(FINFRS);
            FINFRS.setBounds(190, 45, 80, FINFRS.getPreferredSize().height);

            //---- OBJ_32 ----
            OBJ_32.setText("@FRNOMD@");
            OBJ_32.setName("OBJ_32");
            panel3.add(OBJ_32);
            OBJ_32.setBounds(285, 12, 234, OBJ_32.getPreferredSize().height);

            //---- OBJ_34 ----
            OBJ_34.setText("@FRNOMF@");
            OBJ_34.setName("OBJ_34");
            panel3.add(OBJ_34);
            OBJ_34.setBounds(285, 47, 234, OBJ_34.getPreferredSize().height);

            //---- OBJ_35 ----
            OBJ_35.setText("Code de d\u00e9but");
            OBJ_35.setName("OBJ_35");
            panel3.add(OBJ_35);
            OBJ_35.setBounds(30, 14, 160, 20);

            //---- OBJ_37 ----
            OBJ_37.setText("Code de fin");
            OBJ_37.setName("OBJ_37");
            panel3.add(OBJ_37);
            OBJ_37.setBounds(30, 49, 160, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- DEBIAC ----
            DEBIAC.setComponentPopupMenu(BTD);
            DEBIAC.setName("DEBIAC");
            panel4.add(DEBIAC);
            DEBIAC.setBounds(155, 10, 210, DEBIAC.getPreferredSize().height);

            //---- FINIAC ----
            FINIAC.setComponentPopupMenu(BTD);
            FINIAC.setName("FINIAC");
            panel4.add(FINIAC);
            FINIAC.setBounds(155, 45, 210, FINIAC.getPreferredSize().height);

            //---- OBJ_39 ----
            OBJ_39.setText("Code de d\u00e9but");
            OBJ_39.setName("OBJ_39");
            panel4.add(OBJ_39);
            OBJ_39.setBounds(15, 14, 140, 20);

            //---- OBJ_40 ----
            OBJ_40.setText("Code de fin");
            OBJ_40.setName("OBJ_40");
            panel4.add(OBJ_40);
            OBJ_40.setBounds(15, 49, 140, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel6 ========
          {
            panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_41 ----
            OBJ_41.setText("Code de d\u00e9but");
            OBJ_41.setName("OBJ_41");
            panel6.add(OBJ_41);
            OBJ_41.setBounds(10, 14, 135, 20);

            //---- DEBIAK ----
            DEBIAK.setComponentPopupMenu(BTD);
            DEBIAK.setName("DEBIAK");
            panel6.add(DEBIAK);
            DEBIAK.setBounds(150, 10, 210, DEBIAK.getPreferredSize().height);

            //---- FINIAK ----
            FINIAK.setComponentPopupMenu(BTD);
            FINIAK.setName("FINIAK");
            panel6.add(FINIAK);
            FINIAK.setBounds(150, 40, 210, FINIAK.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Code de fin");
            OBJ_42.setName("OBJ_42");
            panel6.add(OBJ_42);
            OBJ_42.setBounds(10, 44, 135, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_33 ----
          OBJ_33.setText("Date d'application de d\u00e9but");
          OBJ_33.setName("OBJ_33");

          //---- DATCND ----
          DATCND.setName("DATCND");

          //---- OBJ_36 ----
          OBJ_36.setText("Date d'application de fin");
          OBJ_36.setName("OBJ_36");

          //---- DATCNF ----
          DATCNF.setName("DATCNF");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(94, 94, 94)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 480, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 585, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 535, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                .addComponent(DATCND, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                .addComponent(DATCNF, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATCND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATCNF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_22;
  private XRiCheckBox WTOU;
  private JPanel panel5;
  private XRiTextField ARTD2;
  private XRiTextField ARTF2;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private RiZoneSortie OBJ_26;
  private RiZoneSortie OBJ_27;
  private JPanel panel2;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private XRiTextField DEBIAE;
  private XRiTextField FINIAE;
  private JPanel panel7;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField DEBFAM;
  private XRiTextField FINFAM;
  private RiZoneSortie OBJ_25;
  private RiZoneSortie OBJ_38;
  private JPanel panel3;
  private XRiTextField DEBFRS;
  private XRiTextField FINFRS;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie OBJ_34;
  private JLabel OBJ_35;
  private JLabel OBJ_37;
  private JPanel panel4;
  private XRiTextField DEBIAC;
  private XRiTextField FINIAC;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JPanel panel6;
  private JLabel OBJ_41;
  private XRiTextField DEBIAK;
  private XRiTextField FINIAK;
  private JLabel OBJ_42;
  private JLabel OBJ_33;
  private XRiCalendrier DATCND;
  private JLabel OBJ_36;
  private XRiCalendrier DATCNF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
