
package ri.serien.libecranrpg.sgam.SGAM30FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM281] Gestion des achats -> Documents d'achats -> Editions factures fournisseurs -> Edt factures fournisseurs en cours
 * Indicateur : 00000001
 * Titre : Edition des factures fournisseurs en cours
 * 
 * [GAM282] Gestion des achats -> Documents d'achats -> Editions factures fournisseurs -> Edt factures avec imputations frais
 * Indicateur : 10000001
 * Titre : Edition des factures fournisseurs en cours avec amputation des frais
 */
public class SGAM30FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private String BOUTON_EXPORTER = "Exporter";
  
  public SGAM30FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    OPT1.setValeursSelection("X", " ");
    OPT2.setValeursSelection("X", " ");
    OPT3.setValeursSelection("X", " ");
    WDET.setValeursSelection("OUI", "NON");
    
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Visibilité des composant
    Boolean is91 = lexique.isTrue("91");
    if (is91) {
      lbDossier.setVisible(true);
      lbContainer.setVisible(true);
      lbPeriodeDeFacturation.setVisible(true);
      pnlPeriodeDeFacturation.setVisible(true);
      lbNumeroColonneTarif.setVisible(true);
      lbDevise.setVisible(false);
      snDevise.setVisible(false);
      bpPresentation.setText("Edition des factures fournisseurs en cours avec imputation des frais");
    }
    else {
      lbDossier.setVisible(false);
      lbContainer.setVisible(false);
      lbPeriodeDeFacturation.setVisible(false);
      pnlPeriodeDeFacturation.setVisible(false);
      lbNumeroColonneTarif.setVisible(false);
      lbDevise.setVisible(true);
      snDevise.setVisible(true);
      bpPresentation.setText("Edition des factures fournisseurs en cours");
    }
    pnlFactureEditer.setVisible(OPT1.isVisible() || OPT2.isVisible() || OPT3.isVisible());
    lbVeuillezSelectionner.setVisible(pnlFactureEditer.isVisible());
    pnlOptionEdition.setVisible(WDET.isVisible());
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise le composant etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialise le composant devise
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(snEtablissement.getIdSelection());
    snDevise.setTousAutorise(true);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "WDEV");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snDevise.renseignerChampRPG(lexique, "WDEV", true);
    if (NUMDEB.getText().isEmpty() || NUMFIN.getText().isEmpty()) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "  ");
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void OPT2ActionPerformed(ActionEvent e) {
    if (OPT2.isSelected() || OPT1.isSelected()) {
      OPT3.setEnabled(false);
      OPT3.setSelected(false);
    }
    else {
      OPT3.setEnabled(true);
    }
  }
  
  private void OPT1ActionPerformed(ActionEvent e) {
    if (OPT2.isSelected() || OPT1.isSelected()) {
      OPT3.setEnabled(false);
      OPT3.setSelected(false);
    }
    else {
      OPT3.setEnabled(true);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbNumeroDeFacture = new SNLabelChamp();
    pnlNumeroFacture = new SNPanel();
    NUMDEB = new XRiTextField();
    lbANumeroFacture = new SNLabelChamp();
    NUMFIN = new XRiTextField();
    lbPeriodeDeFacturation = new SNLabelChamp();
    pnlPeriodeDeFacturation = new SNPanel();
    DATDEB = new XRiCalendrier();
    lbAPeriodeFacturation = new SNLabelChamp();
    DATFIN = new XRiCalendrier();
    lbDossier = new SNLabelChamp();
    WDOS = new XRiTextField();
    lbContainer = new SNLabelChamp();
    WCONT = new XRiTextField();
    lbNumeroColonneTarif = new SNLabelChamp();
    WTAR = new XRiTextField();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    pnlFactureEditer = new SNPanelTitre();
    lbVeuillezSelectionner = new SNLabelTitre();
    OPT2 = new XRiCheckBox();
    OPT1 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    WDET = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 550));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition des factures fournisseurs en cours");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDeFacture ----
            lbNumeroDeFacture.setText("Num\u00e9ro de facture de");
            lbNumeroDeFacture.setName("lbNumeroDeFacture");
            pnlCritereDeSelection.add(lbNumeroDeFacture, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlNumeroFacture ========
            {
              pnlNumeroFacture.setName("pnlNumeroFacture");
              pnlNumeroFacture.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNumeroFacture.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlNumeroFacture.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- NUMDEB ----
              NUMDEB.setComponentPopupMenu(null);
              NUMDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMDEB.setPreferredSize(new Dimension(70, 30));
              NUMDEB.setMinimumSize(new Dimension(70, 30));
              NUMDEB.setMaximumSize(new Dimension(70, 30));
              NUMDEB.setName("NUMDEB");
              pnlNumeroFacture.add(NUMDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbANumeroFacture ----
              lbANumeroFacture.setText("\u00e0");
              lbANumeroFacture.setPreferredSize(new Dimension(8, 30));
              lbANumeroFacture.setMinimumSize(new Dimension(8, 30));
              lbANumeroFacture.setMaximumSize(new Dimension(8, 30));
              lbANumeroFacture.setName("lbANumeroFacture");
              pnlNumeroFacture.add(lbANumeroFacture, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- NUMFIN ----
              NUMFIN.setComponentPopupMenu(null);
              NUMFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMFIN.setMaximumSize(new Dimension(70, 30));
              NUMFIN.setMinimumSize(new Dimension(70, 30));
              NUMFIN.setPreferredSize(new Dimension(70, 30));
              NUMFIN.setName("NUMFIN");
              pnlNumeroFacture.add(NUMFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlNumeroFacture, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeDeFacturation ----
            lbPeriodeDeFacturation.setText("P\u00e9riode de facturation de");
            lbPeriodeDeFacturation.setPreferredSize(new Dimension(160, 30));
            lbPeriodeDeFacturation.setMinimumSize(new Dimension(160, 30));
            lbPeriodeDeFacturation.setMaximumSize(new Dimension(160, 30));
            lbPeriodeDeFacturation.setName("lbPeriodeDeFacturation");
            pnlCritereDeSelection.add(lbPeriodeDeFacturation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeDeFacturation ========
            {
              pnlPeriodeDeFacturation.setName("pnlPeriodeDeFacturation");
              pnlPeriodeDeFacturation.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeDeFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeDeFacturation.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeDeFacturation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeDeFacturation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- DATDEB ----
              DATDEB.setPreferredSize(new Dimension(110, 30));
              DATDEB.setMinimumSize(new Dimension(110, 30));
              DATDEB.setMaximumSize(new Dimension(110, 30));
              DATDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATDEB.setName("DATDEB");
              pnlPeriodeDeFacturation.add(DATDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAPeriodeFacturation ----
              lbAPeriodeFacturation.setText("\u00e0");
              lbAPeriodeFacturation.setPreferredSize(new Dimension(8, 30));
              lbAPeriodeFacturation.setMinimumSize(new Dimension(8, 30));
              lbAPeriodeFacturation.setMaximumSize(new Dimension(8, 30));
              lbAPeriodeFacturation.setName("lbAPeriodeFacturation");
              pnlPeriodeDeFacturation.add(lbAPeriodeFacturation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DATFIN ----
              DATFIN.setPreferredSize(new Dimension(110, 30));
              DATFIN.setMinimumSize(new Dimension(110, 30));
              DATFIN.setMaximumSize(new Dimension(110, 30));
              DATFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATFIN.setName("DATFIN");
              pnlPeriodeDeFacturation.add(DATFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlPeriodeDeFacturation, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDossier ----
            lbDossier.setText("Dossier");
            lbDossier.setName("lbDossier");
            pnlCritereDeSelection.add(lbDossier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WDOS ----
            WDOS.setComponentPopupMenu(null);
            WDOS.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDOS.setPreferredSize(new Dimension(80, 30));
            WDOS.setMinimumSize(new Dimension(80, 30));
            WDOS.setMaximumSize(new Dimension(80, 30));
            WDOS.setName("WDOS");
            pnlCritereDeSelection.add(WDOS, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbContainer ----
            lbContainer.setText("Container");
            lbContainer.setName("lbContainer");
            pnlCritereDeSelection.add(lbContainer, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WCONT ----
            WCONT.setComponentPopupMenu(null);
            WCONT.setPreferredSize(new Dimension(164, 30));
            WCONT.setMinimumSize(new Dimension(164, 30));
            WCONT.setMaximumSize(new Dimension(164, 30));
            WCONT.setFont(new Font("sansserif", Font.PLAIN, 14));
            WCONT.setName("WCONT");
            pnlCritereDeSelection.add(WCONT, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNumeroColonneTarif ----
            lbNumeroColonneTarif.setText("Num\u00e9ro de colonne tarif");
            lbNumeroColonneTarif.setPreferredSize(new Dimension(153, 30));
            lbNumeroColonneTarif.setMinimumSize(new Dimension(153, 30));
            lbNumeroColonneTarif.setMaximumSize(new Dimension(153, 30));
            lbNumeroColonneTarif.setName("lbNumeroColonneTarif");
            pnlCritereDeSelection.add(lbNumeroColonneTarif, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WTAR ----
            WTAR.setComponentPopupMenu(null);
            WTAR.setPreferredSize(new Dimension(30, 30));
            WTAR.setMinimumSize(new Dimension(30, 30));
            WTAR.setMaximumSize(new Dimension(30, 30));
            WTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTAR.setName("WTAR");
            pnlCritereDeSelection.add(WTAR, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDevise ----
            lbDevise.setText("Devise");
            lbDevise.setName("lbDevise");
            pnlCritereDeSelection.add(lbDevise, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snDevise ----
            snDevise.setFont(new Font("sansserif", Font.PLAIN, 14));
            snDevise.setName("snDevise");
            pnlCritereDeSelection.add(snDevise, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlFactureEditer ========
          {
            pnlFactureEditer.setTitre("Factures \u00e0 \u00e9diter");
            pnlFactureEditer.setName("pnlFactureEditer");
            pnlFactureEditer.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFactureEditer.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlFactureEditer.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFactureEditer.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlFactureEditer.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbVeuillezSelectionner ----
            lbVeuillezSelectionner.setText("Veuillez s\u00e9lectionner au moins un \u00e9l\u00e9ment ci-dessous :");
            lbVeuillezSelectionner.setMinimumSize(new Dimension(350, 30));
            lbVeuillezSelectionner.setPreferredSize(new Dimension(350, 30));
            lbVeuillezSelectionner.setMaximumSize(new Dimension(350, 30));
            lbVeuillezSelectionner.setName("lbVeuillezSelectionner");
            pnlFactureEditer.add(lbVeuillezSelectionner, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT2 ----
            OPT2.setText("Factures d'achat homologu\u00e9es (FAC)");
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT2.setPreferredSize(new Dimension(260, 30));
            OPT2.setMinimumSize(new Dimension(260, 30));
            OPT2.setMaximumSize(new Dimension(260, 30));
            OPT2.setName("OPT2");
            OPT2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OPT2ActionPerformed(e);
              }
            });
            pnlFactureEditer.add(OPT2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT1 ----
            OPT1.setText("Factures d'achat en attente (ATT)");
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT1.setPreferredSize(new Dimension(260, 30));
            OPT1.setMinimumSize(new Dimension(260, 30));
            OPT1.setMaximumSize(new Dimension(260, 30));
            OPT1.setName("OPT1");
            OPT1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OPT1ActionPerformed(e);
              }
            });
            pnlFactureEditer.add(OPT1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OPT3 ----
            OPT3.setText("Factures \"Flottant\"");
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPT3.setPreferredSize(new Dimension(260, 30));
            OPT3.setMinimumSize(new Dimension(260, 30));
            OPT3.setMaximumSize(new Dimension(260, 30));
            OPT3.setName("OPT3");
            pnlFactureEditer.add(OPT3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlFactureEditer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablisement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Option d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- WDET ----
            WDET.setText("Edition du d\u00e9tail");
            WDET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WDET.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDET.setPreferredSize(new Dimension(260, 30));
            WDET.setMinimumSize(new Dimension(260, 30));
            WDET.setMaximumSize(new Dimension(260, 30));
            WDET.setName("WDET");
            pnlOptionEdition.add(WDET, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbNumeroDeFacture;
  private SNPanel pnlNumeroFacture;
  private XRiTextField NUMDEB;
  private SNLabelChamp lbANumeroFacture;
  private XRiTextField NUMFIN;
  private SNLabelChamp lbPeriodeDeFacturation;
  private SNPanel pnlPeriodeDeFacturation;
  private XRiCalendrier DATDEB;
  private SNLabelChamp lbAPeriodeFacturation;
  private XRiCalendrier DATFIN;
  private SNLabelChamp lbDossier;
  private XRiTextField WDOS;
  private SNLabelChamp lbContainer;
  private XRiTextField WCONT;
  private SNLabelChamp lbNumeroColonneTarif;
  private XRiTextField WTAR;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNPanelTitre pnlFactureEditer;
  private SNLabelTitre lbVeuillezSelectionner;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox WDET;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
