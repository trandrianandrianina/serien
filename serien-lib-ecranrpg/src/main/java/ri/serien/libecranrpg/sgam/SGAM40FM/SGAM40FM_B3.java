
package ri.serien.libecranrpg.sgam.SGAM40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM40FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _CETB1_Title = { "Etb", "Identification", "Période à comptabiliser", "", };
  private String[][] _CETB1_Data = { { "CETB1", "CLIB1", "CPER1", "CFOR1", }, { "CETB2", "CLIB2", "CPER2", "CFOR2", },
      { "CETB3", "CLIB3", "CPER3", "CFOR3", }, { "CETB4", "CLIB4", "CPER4", "CFOR4", }, { "CETB5", "CLIB5", "CPER5", "CFOR5", },
      { "CETB6", "CLIB6", "CPER6", "CFOR6", }, { "CETB7", "CLIB7", "CPER7", "CFOR7", }, { "CETB8", "CLIB8", "CPER8", "CFOR8", },
      { "CETB9", "CLIB9", "CPER9", "CFOR9", }, { "CETB10", "CLIB10", "CPER10", "CFOR10", }, { "CETB11", "CLIB11", "CPER11", "CFOR11", },
      { "CETB12", "CLIB12", "CPER12", "CFOR12", }, { "CETB13", "CLIB13", "CPER13", "CFOR13", },
      { "CETB14", "CLIB14", "CPER14", "CFOR14", }, { "CETB15", "CLIB15", "CPER15", "CFOR15", }, };
  private int[] _CETB1_Width = { 25, 180, 166, 31, };
  
  public SGAM40FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    CETB1.setAspectTable(null, _CETB1_Title, _CETB1_Data, _CETB1_Width, false, null, null, null, null);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererLesErreurs("19");
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "F6");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlRecapitulatif = new SNPanelTitre();
    SCROLLPANE_LIST3 = new JScrollPane();
    CETB1 = new XRiTable();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setPreferredSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

      //======== pnlRecapitulatif ========
      {
        pnlRecapitulatif.setForeground(Color.black);
        pnlRecapitulatif.setTitre("R\u00e9capitulatif des demandes");
        pnlRecapitulatif.setPreferredSize(new Dimension(517, 325));
        pnlRecapitulatif.setMinimumSize(new Dimension(70, 325));
        pnlRecapitulatif.setName("pnlRecapitulatif");
        pnlRecapitulatif.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlRecapitulatif.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlRecapitulatif.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlRecapitulatif.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlRecapitulatif.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== SCROLLPANE_LIST3 ========
        {
          SCROLLPANE_LIST3.setName("SCROLLPANE_LIST3");

          //---- CETB1 ----
          CETB1.setName("CETB1");
          SCROLLPANE_LIST3.setViewportView(CETB1);
        }
        pnlRecapitulatif.add(SCROLLPANE_LIST3, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setMinimumSize(new Dimension(28, 132));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlRecapitulatif.add(BT_PGDOWN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setMinimumSize(new Dimension(28, 132));
        BT_PGUP.setPreferredSize(new Dimension(28, 150));
        BT_PGUP.setName("BT_PGUP");
        pnlRecapitulatif.add(BT_PGUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlRecapitulatif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlRecapitulatif;
  private JScrollPane SCROLLPANE_LIST3;
  private XRiTable CETB1;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
