
package ri.serien.libecranrpg.sgam.SGAM9CFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM9CFM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM9CFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    PAT016.setValeursSelection("1", " ");
    PAT015.setValeursSelection("1", " ");
    PAT014.setValeursSelection("1", " ");
    PAT013.setValeursSelection("1", " ");
    PAT012.setValeursSelection("1", " ");
    PAT011.setValeursSelection("1", " ");
    PAT010.setValeursSelection("1", " ");
    PAT009.setValeursSelection("1", " ");
    PAT008.setValeursSelection("1", " ");
    PAT007.setValeursSelection("1", " ");
    PAT006.setValeursSelection("1", " ");
    PAT005.setValeursSelection("1", " ");
    PAT004.setValeursSelection("1", " ");
    PAT003.setValeursSelection("1", " ");
    PAT002.setValeursSelection("1", " ");
    PAT001.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    PAS001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS001@")).trim());
    PAS002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS002@")).trim());
    PAS003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS003@")).trim());
    PAS004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS004@")).trim());
    PAS005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS005@")).trim());
    PAS006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS006@")).trim());
    PAS007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS007@")).trim());
    PAS008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS008@")).trim());
    PAS009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS009@")).trim());
    PAS010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS010@")).trim());
    PAS011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS011@")).trim());
    PAS012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS012@")).trim());
    PAS013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS013@")).trim());
    PAS014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS014@")).trim());
    PAS015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS015@")).trim());
    PAS016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS016@")).trim());
    PAN001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN001@")).trim());
    PAN003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN003@")).trim());
    PAN004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN004@")).trim());
    PAN005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN005@")).trim());
    PAN006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN006@")).trim());
    PAN007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN007@")).trim());
    PAN008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN008@")).trim());
    PAN009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN009@")).trim());
    PAN010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN010@")).trim());
    PAN011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN011@")).trim());
    PAN012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN012@")).trim());
    PAN013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN013@")).trim());
    PAN014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN014@")).trim());
    PAN015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN015@")).trim());
    PAN016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN016@")).trim());
    PAN002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN002@")).trim());
    PAE001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE001@")).trim());
    PAE002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE002@")).trim());
    PAE003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE003@")).trim());
    PAE004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE004@")).trim());
    PAE005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE005@")).trim());
    PAE006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE006@")).trim());
    PAE007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE007@")).trim());
    PAE008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE008@")).trim());
    PAE009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE009@")).trim());
    PAE010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE010@")).trim());
    PAE011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE011@")).trim());
    PAE012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE012@")).trim());
    PAE013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE013@")).trim());
    PAE014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE014@")).trim());
    PAE015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE015@")).trim());
    PAE016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAE016@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // OBJ_34.setEnabled( lexique.isPresent("WTETB"));
    // PAT001.setSelected(lexique.HostFieldGetData("PAT001").trim().equalsIgnoreCase("1"));
    // PAT002.setSelected(lexique.HostFieldGetData("PAT002").trim().equalsIgnoreCase("1"));
    // PAT003.setSelected(lexique.HostFieldGetData("PAT003").trim().equalsIgnoreCase("1"));
    // PAT004.setSelected(lexique.HostFieldGetData("PAT004").trim().equalsIgnoreCase("1"));
    // PAT005.setSelected(lexique.HostFieldGetData("PAT005").trim().equalsIgnoreCase("1"));
    // PAT006.setSelected(lexique.HostFieldGetData("PAT006").trim().equalsIgnoreCase("1"));
    // PAT007.setSelected(lexique.HostFieldGetData("PAT007").trim().equalsIgnoreCase("1"));
    // PAT008.setSelected(lexique.HostFieldGetData("PAT008").trim().equalsIgnoreCase("1"));
    // PAT009.setSelected(lexique.HostFieldGetData("PAT009").trim().equalsIgnoreCase("1"));
    // PAT010.setSelected(lexique.HostFieldGetData("PAT010").trim().equalsIgnoreCase("1"));
    // PAT011.setSelected(lexique.HostFieldGetData("PAT011").trim().equalsIgnoreCase("1"));
    // PAT012.setSelected(lexique.HostFieldGetData("PAT012").trim().equalsIgnoreCase("1"));
    // PAT013.setSelected(lexique.HostFieldGetData("PAT013").trim().equalsIgnoreCase("1"));
    // PAT014.setSelected(lexique.HostFieldGetData("PAT014").trim().equalsIgnoreCase("1"));
    // PAT015.setSelected(lexique.HostFieldGetData("PAT015").trim().equalsIgnoreCase("1"));
    // PAT016.setSelected(lexique.HostFieldGetData("PAT016").trim().equalsIgnoreCase("1"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if(PAT001.isSelected()) lexique.HostFieldPutData("PAT001", 0, "1");
    // if(PAT002.isSelected()) lexique.HostFieldPutData("PAT002", 0, "1");
    // if(PAT003.isSelected()) lexique.HostFieldPutData("PAT003", 0, "1");
    // if(PAT004.isSelected()) lexique.HostFieldPutData("PAT004", 0, "1");
    // if(PAT005.isSelected()) lexique.HostFieldPutData("PAT005", 0, "1");
    // if(PAT006.isSelected()) lexique.HostFieldPutData("PAT006", 0, "1");
    // if(PAT007.isSelected()) lexique.HostFieldPutData("PAT007", 0, "1");
    // if(PAT008.isSelected()) lexique.HostFieldPutData("PAT008", 0, "1");
    // if(PAT009.isSelected()) lexique.HostFieldPutData("PAT009", 0, "1");
    // if(PAT010.isSelected()) lexique.HostFieldPutData("PAT010", 0, "1");
    // if(PAT011.isSelected()) lexique.HostFieldPutData("PAT011", 0, "1");
    // if(PAT012.isSelected()) lexique.HostFieldPutData("PAT012", 0, "1");
    // if(PAT013.isSelected()) lexique.HostFieldPutData("PAT013", 0, "1");
    // if(PAT014.isSelected()) lexique.HostFieldPutData("PAT014", 0, "1");
    // if(PAT015.isSelected()) lexique.HostFieldPutData("PAT015", 0, "1");
    // if(PAT016.isSelected()) lexique.HostFieldPutData("PAT016", 0, "1");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTETB", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    PAS001 = new RiZoneSortie();
    PAS002 = new RiZoneSortie();
    PAS003 = new RiZoneSortie();
    PAS004 = new RiZoneSortie();
    PAS005 = new RiZoneSortie();
    PAS006 = new RiZoneSortie();
    PAS007 = new RiZoneSortie();
    PAS008 = new RiZoneSortie();
    PAS009 = new RiZoneSortie();
    PAS010 = new RiZoneSortie();
    PAS011 = new RiZoneSortie();
    PAS012 = new RiZoneSortie();
    PAS013 = new RiZoneSortie();
    PAS014 = new RiZoneSortie();
    PAS015 = new RiZoneSortie();
    PAS016 = new RiZoneSortie();
    PAT001 = new XRiCheckBox();
    PAT002 = new XRiCheckBox();
    PAT003 = new XRiCheckBox();
    PAT004 = new XRiCheckBox();
    PAT005 = new XRiCheckBox();
    PAT006 = new XRiCheckBox();
    PAT007 = new XRiCheckBox();
    PAT008 = new XRiCheckBox();
    PAT009 = new XRiCheckBox();
    PAT010 = new XRiCheckBox();
    PAT011 = new XRiCheckBox();
    PAT012 = new XRiCheckBox();
    PAT013 = new XRiCheckBox();
    PAT014 = new XRiCheckBox();
    PAT015 = new XRiCheckBox();
    PAT016 = new XRiCheckBox();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    PAN001 = new RiZoneSortie();
    PAN003 = new RiZoneSortie();
    PAN004 = new RiZoneSortie();
    PAN005 = new RiZoneSortie();
    PAN006 = new RiZoneSortie();
    PAN007 = new RiZoneSortie();
    PAN008 = new RiZoneSortie();
    PAN009 = new RiZoneSortie();
    PAN010 = new RiZoneSortie();
    PAN011 = new RiZoneSortie();
    PAN012 = new RiZoneSortie();
    PAN013 = new RiZoneSortie();
    PAN014 = new RiZoneSortie();
    PAN015 = new RiZoneSortie();
    PAN016 = new RiZoneSortie();
    PAN002 = new RiZoneSortie();
    PAM001 = new XRiTextField();
    PAM002 = new XRiTextField();
    PAM003 = new XRiTextField();
    PAM004 = new XRiTextField();
    PAM005 = new XRiTextField();
    PAM006 = new XRiTextField();
    PAM007 = new XRiTextField();
    PAM008 = new XRiTextField();
    PAM009 = new XRiTextField();
    PAM010 = new XRiTextField();
    PAM011 = new XRiTextField();
    PAM012 = new XRiTextField();
    PAM013 = new XRiTextField();
    PAM014 = new XRiTextField();
    PAM015 = new XRiTextField();
    PAM016 = new XRiTextField();
    PAE001 = new RiZoneSortie();
    PAE002 = new RiZoneSortie();
    PAE003 = new RiZoneSortie();
    PAE004 = new RiZoneSortie();
    PAE005 = new RiZoneSortie();
    PAE006 = new RiZoneSortie();
    PAE007 = new RiZoneSortie();
    PAE008 = new RiZoneSortie();
    PAE009 = new RiZoneSortie();
    PAE010 = new RiZoneSortie();
    PAE011 = new RiZoneSortie();
    PAE012 = new RiZoneSortie();
    PAE013 = new RiZoneSortie();
    PAE014 = new RiZoneSortie();
    PAE015 = new RiZoneSortie();
    PAE016 = new RiZoneSortie();
    OBJ_34 = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(730, 590));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(730, 590));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Choix de l'\u00e9tablissement"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- PAS001 ----
            PAS001.setText("@PAS001@");
            PAS001.setName("PAS001");
            panel1.add(PAS001);
            PAS001.setBounds(50, 60, 40, PAS001.getPreferredSize().height);

            //---- PAS002 ----
            PAS002.setText("@PAS002@");
            PAS002.setName("PAS002");
            panel1.add(PAS002);
            PAS002.setBounds(50, 88, 40, PAS002.getPreferredSize().height);

            //---- PAS003 ----
            PAS003.setText("@PAS003@");
            PAS003.setName("PAS003");
            panel1.add(PAS003);
            PAS003.setBounds(50, 116, 40, PAS003.getPreferredSize().height);

            //---- PAS004 ----
            PAS004.setText("@PAS004@");
            PAS004.setName("PAS004");
            panel1.add(PAS004);
            PAS004.setBounds(50, 144, 40, PAS004.getPreferredSize().height);

            //---- PAS005 ----
            PAS005.setText("@PAS005@");
            PAS005.setName("PAS005");
            panel1.add(PAS005);
            PAS005.setBounds(50, 172, 40, PAS005.getPreferredSize().height);

            //---- PAS006 ----
            PAS006.setText("@PAS006@");
            PAS006.setName("PAS006");
            panel1.add(PAS006);
            PAS006.setBounds(50, 200, 40, PAS006.getPreferredSize().height);

            //---- PAS007 ----
            PAS007.setText("@PAS007@");
            PAS007.setName("PAS007");
            panel1.add(PAS007);
            PAS007.setBounds(50, 228, 40, PAS007.getPreferredSize().height);

            //---- PAS008 ----
            PAS008.setText("@PAS008@");
            PAS008.setName("PAS008");
            panel1.add(PAS008);
            PAS008.setBounds(50, 256, 40, PAS008.getPreferredSize().height);

            //---- PAS009 ----
            PAS009.setText("@PAS009@");
            PAS009.setName("PAS009");
            panel1.add(PAS009);
            PAS009.setBounds(50, 284, 40, PAS009.getPreferredSize().height);

            //---- PAS010 ----
            PAS010.setText("@PAS010@");
            PAS010.setName("PAS010");
            panel1.add(PAS010);
            PAS010.setBounds(50, 312, 40, PAS010.getPreferredSize().height);

            //---- PAS011 ----
            PAS011.setText("@PAS011@");
            PAS011.setName("PAS011");
            panel1.add(PAS011);
            PAS011.setBounds(50, 340, 40, PAS011.getPreferredSize().height);

            //---- PAS012 ----
            PAS012.setText("@PAS012@");
            PAS012.setName("PAS012");
            panel1.add(PAS012);
            PAS012.setBounds(50, 368, 40, PAS012.getPreferredSize().height);

            //---- PAS013 ----
            PAS013.setText("@PAS013@");
            PAS013.setName("PAS013");
            panel1.add(PAS013);
            PAS013.setBounds(50, 396, 40, PAS013.getPreferredSize().height);

            //---- PAS014 ----
            PAS014.setText("@PAS014@");
            PAS014.setName("PAS014");
            panel1.add(PAS014);
            PAS014.setBounds(50, 424, 40, PAS014.getPreferredSize().height);

            //---- PAS015 ----
            PAS015.setText("@PAS015@");
            PAS015.setName("PAS015");
            panel1.add(PAS015);
            PAS015.setBounds(50, 452, 40, PAS015.getPreferredSize().height);

            //---- PAS016 ----
            PAS016.setText("@PAS016@");
            PAS016.setName("PAS016");
            panel1.add(PAS016);
            PAS016.setBounds(50, 480, 40, PAS016.getPreferredSize().height);

            //---- PAT001 ----
            PAT001.setName("PAT001");
            panel1.add(PAT001);
            PAT001.setBounds(new Rectangle(new Point(25, 63), PAT001.getPreferredSize()));

            //---- PAT002 ----
            PAT002.setName("PAT002");
            panel1.add(PAT002);
            PAT002.setBounds(new Rectangle(new Point(25, 91), PAT002.getPreferredSize()));

            //---- PAT003 ----
            PAT003.setName("PAT003");
            panel1.add(PAT003);
            PAT003.setBounds(new Rectangle(new Point(25, 119), PAT003.getPreferredSize()));

            //---- PAT004 ----
            PAT004.setName("PAT004");
            panel1.add(PAT004);
            PAT004.setBounds(new Rectangle(new Point(25, 147), PAT004.getPreferredSize()));

            //---- PAT005 ----
            PAT005.setName("PAT005");
            panel1.add(PAT005);
            PAT005.setBounds(new Rectangle(new Point(25, 175), PAT005.getPreferredSize()));

            //---- PAT006 ----
            PAT006.setName("PAT006");
            panel1.add(PAT006);
            PAT006.setBounds(new Rectangle(new Point(25, 203), PAT006.getPreferredSize()));

            //---- PAT007 ----
            PAT007.setName("PAT007");
            panel1.add(PAT007);
            PAT007.setBounds(new Rectangle(new Point(25, 231), PAT007.getPreferredSize()));

            //---- PAT008 ----
            PAT008.setName("PAT008");
            panel1.add(PAT008);
            PAT008.setBounds(new Rectangle(new Point(25, 259), PAT008.getPreferredSize()));

            //---- PAT009 ----
            PAT009.setName("PAT009");
            panel1.add(PAT009);
            PAT009.setBounds(new Rectangle(new Point(25, 287), PAT009.getPreferredSize()));

            //---- PAT010 ----
            PAT010.setName("PAT010");
            panel1.add(PAT010);
            PAT010.setBounds(new Rectangle(new Point(25, 315), PAT010.getPreferredSize()));

            //---- PAT011 ----
            PAT011.setName("PAT011");
            panel1.add(PAT011);
            PAT011.setBounds(new Rectangle(new Point(25, 343), PAT011.getPreferredSize()));

            //---- PAT012 ----
            PAT012.setName("PAT012");
            panel1.add(PAT012);
            PAT012.setBounds(new Rectangle(new Point(25, 371), PAT012.getPreferredSize()));

            //---- PAT013 ----
            PAT013.setName("PAT013");
            panel1.add(PAT013);
            PAT013.setBounds(new Rectangle(new Point(25, 399), PAT013.getPreferredSize()));

            //---- PAT014 ----
            PAT014.setName("PAT014");
            panel1.add(PAT014);
            PAT014.setBounds(new Rectangle(new Point(25, 427), PAT014.getPreferredSize()));

            //---- PAT015 ----
            PAT015.setName("PAT015");
            panel1.add(PAT015);
            PAT015.setBounds(new Rectangle(new Point(25, 455), PAT015.getPreferredSize()));

            //---- PAT016 ----
            PAT016.setName("PAT016");
            panel1.add(PAT016);
            PAT016.setBounds(new Rectangle(new Point(25, 485), PAT016.getPreferredSize()));

            //---- label1 ----
            label1.setText("Soci\u00e9t\u00e9");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(50, 35, 60, 25);

            //---- label2 ----
            label2.setText("Identification");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(115, 35, 140, 25);

            //---- label3 ----
            label3.setText("Purge");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(375, 35, 80, 25);

            //---- label4 ----
            label4.setText("En cours");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(465, 35, 125, 25);

            //---- PAN001 ----
            PAN001.setText("@PAN001@");
            PAN001.setName("PAN001");
            panel1.add(PAN001);
            PAN001.setBounds(95, 60, 265, PAN001.getPreferredSize().height);

            //---- PAN003 ----
            PAN003.setText("@PAN003@");
            PAN003.setName("PAN003");
            panel1.add(PAN003);
            PAN003.setBounds(95, 116, 265, PAN003.getPreferredSize().height);

            //---- PAN004 ----
            PAN004.setText("@PAN004@");
            PAN004.setName("PAN004");
            panel1.add(PAN004);
            PAN004.setBounds(95, 144, 265, PAN004.getPreferredSize().height);

            //---- PAN005 ----
            PAN005.setText("@PAN005@");
            PAN005.setName("PAN005");
            panel1.add(PAN005);
            PAN005.setBounds(95, 172, 265, PAN005.getPreferredSize().height);

            //---- PAN006 ----
            PAN006.setText("@PAN006@");
            PAN006.setName("PAN006");
            panel1.add(PAN006);
            PAN006.setBounds(95, 200, 265, PAN006.getPreferredSize().height);

            //---- PAN007 ----
            PAN007.setText("@PAN007@");
            PAN007.setName("PAN007");
            panel1.add(PAN007);
            PAN007.setBounds(95, 228, 265, PAN007.getPreferredSize().height);

            //---- PAN008 ----
            PAN008.setText("@PAN008@");
            PAN008.setName("PAN008");
            panel1.add(PAN008);
            PAN008.setBounds(95, 256, 265, PAN008.getPreferredSize().height);

            //---- PAN009 ----
            PAN009.setText("@PAN009@");
            PAN009.setName("PAN009");
            panel1.add(PAN009);
            PAN009.setBounds(95, 284, 265, PAN009.getPreferredSize().height);

            //---- PAN010 ----
            PAN010.setText("@PAN010@");
            PAN010.setName("PAN010");
            panel1.add(PAN010);
            PAN010.setBounds(95, 312, 265, PAN010.getPreferredSize().height);

            //---- PAN011 ----
            PAN011.setText("@PAN011@");
            PAN011.setName("PAN011");
            panel1.add(PAN011);
            PAN011.setBounds(95, 340, 265, PAN011.getPreferredSize().height);

            //---- PAN012 ----
            PAN012.setText("@PAN012@");
            PAN012.setName("PAN012");
            panel1.add(PAN012);
            PAN012.setBounds(95, 368, 265, PAN012.getPreferredSize().height);

            //---- PAN013 ----
            PAN013.setText("@PAN013@");
            PAN013.setName("PAN013");
            panel1.add(PAN013);
            PAN013.setBounds(95, 396, 265, PAN013.getPreferredSize().height);

            //---- PAN014 ----
            PAN014.setText("@PAN014@");
            PAN014.setName("PAN014");
            panel1.add(PAN014);
            PAN014.setBounds(95, 424, 265, PAN014.getPreferredSize().height);

            //---- PAN015 ----
            PAN015.setText("@PAN015@");
            PAN015.setName("PAN015");
            panel1.add(PAN015);
            PAN015.setBounds(95, 452, 265, PAN015.getPreferredSize().height);

            //---- PAN016 ----
            PAN016.setText("@PAN016@");
            PAN016.setName("PAN016");
            panel1.add(PAN016);
            PAN016.setBounds(95, 480, 265, PAN016.getPreferredSize().height);

            //---- PAN002 ----
            PAN002.setText("@PAN002@");
            PAN002.setName("PAN002");
            panel1.add(PAN002);
            PAN002.setBounds(95, 88, 265, PAN002.getPreferredSize().height);

            //---- PAM001 ----
            PAM001.setName("PAM001");
            panel1.add(PAM001);
            PAM001.setBounds(365, 58, 80, PAM001.getPreferredSize().height);

            //---- PAM002 ----
            PAM002.setName("PAM002");
            panel1.add(PAM002);
            PAM002.setBounds(365, 86, 80, PAM002.getPreferredSize().height);

            //---- PAM003 ----
            PAM003.setName("PAM003");
            panel1.add(PAM003);
            PAM003.setBounds(365, 114, 80, PAM003.getPreferredSize().height);

            //---- PAM004 ----
            PAM004.setName("PAM004");
            panel1.add(PAM004);
            PAM004.setBounds(365, 142, 80, PAM004.getPreferredSize().height);

            //---- PAM005 ----
            PAM005.setName("PAM005");
            panel1.add(PAM005);
            PAM005.setBounds(365, 170, 80, PAM005.getPreferredSize().height);

            //---- PAM006 ----
            PAM006.setName("PAM006");
            panel1.add(PAM006);
            PAM006.setBounds(365, 198, 80, PAM006.getPreferredSize().height);

            //---- PAM007 ----
            PAM007.setName("PAM007");
            panel1.add(PAM007);
            PAM007.setBounds(365, 226, 80, PAM007.getPreferredSize().height);

            //---- PAM008 ----
            PAM008.setName("PAM008");
            panel1.add(PAM008);
            PAM008.setBounds(365, 254, 80, PAM008.getPreferredSize().height);

            //---- PAM009 ----
            PAM009.setName("PAM009");
            panel1.add(PAM009);
            PAM009.setBounds(365, 282, 80, PAM009.getPreferredSize().height);

            //---- PAM010 ----
            PAM010.setName("PAM010");
            panel1.add(PAM010);
            PAM010.setBounds(365, 310, 80, PAM010.getPreferredSize().height);

            //---- PAM011 ----
            PAM011.setName("PAM011");
            panel1.add(PAM011);
            PAM011.setBounds(365, 338, 80, PAM011.getPreferredSize().height);

            //---- PAM012 ----
            PAM012.setName("PAM012");
            panel1.add(PAM012);
            PAM012.setBounds(365, 366, 80, PAM012.getPreferredSize().height);

            //---- PAM013 ----
            PAM013.setName("PAM013");
            panel1.add(PAM013);
            PAM013.setBounds(365, 394, 80, PAM013.getPreferredSize().height);

            //---- PAM014 ----
            PAM014.setName("PAM014");
            panel1.add(PAM014);
            PAM014.setBounds(365, 422, 80, PAM014.getPreferredSize().height);

            //---- PAM015 ----
            PAM015.setName("PAM015");
            panel1.add(PAM015);
            PAM015.setBounds(365, 450, 80, PAM015.getPreferredSize().height);

            //---- PAM016 ----
            PAM016.setName("PAM016");
            panel1.add(PAM016);
            PAM016.setBounds(365, 480, 80, PAM016.getPreferredSize().height);

            //---- PAE001 ----
            PAE001.setText("@PAE001@");
            PAE001.setName("PAE001");
            panel1.add(PAE001);
            PAE001.setBounds(450, 60, 180, PAE001.getPreferredSize().height);

            //---- PAE002 ----
            PAE002.setText("@PAE002@");
            PAE002.setName("PAE002");
            panel1.add(PAE002);
            PAE002.setBounds(450, 88, 180, PAE002.getPreferredSize().height);

            //---- PAE003 ----
            PAE003.setText("@PAE003@");
            PAE003.setName("PAE003");
            panel1.add(PAE003);
            PAE003.setBounds(450, 116, 180, PAE003.getPreferredSize().height);

            //---- PAE004 ----
            PAE004.setText("@PAE004@");
            PAE004.setName("PAE004");
            panel1.add(PAE004);
            PAE004.setBounds(450, 144, 180, PAE004.getPreferredSize().height);

            //---- PAE005 ----
            PAE005.setText("@PAE005@");
            PAE005.setName("PAE005");
            panel1.add(PAE005);
            PAE005.setBounds(450, 172, 180, PAE005.getPreferredSize().height);

            //---- PAE006 ----
            PAE006.setText("@PAE006@");
            PAE006.setName("PAE006");
            panel1.add(PAE006);
            PAE006.setBounds(450, 200, 180, PAE006.getPreferredSize().height);

            //---- PAE007 ----
            PAE007.setText("@PAE007@");
            PAE007.setName("PAE007");
            panel1.add(PAE007);
            PAE007.setBounds(450, 228, 180, PAE007.getPreferredSize().height);

            //---- PAE008 ----
            PAE008.setText("@PAE008@");
            PAE008.setName("PAE008");
            panel1.add(PAE008);
            PAE008.setBounds(450, 256, 180, PAE008.getPreferredSize().height);

            //---- PAE009 ----
            PAE009.setText("@PAE009@");
            PAE009.setName("PAE009");
            panel1.add(PAE009);
            PAE009.setBounds(450, 284, 180, PAE009.getPreferredSize().height);

            //---- PAE010 ----
            PAE010.setText("@PAE010@");
            PAE010.setName("PAE010");
            panel1.add(PAE010);
            PAE010.setBounds(450, 312, 180, PAE010.getPreferredSize().height);

            //---- PAE011 ----
            PAE011.setText("@PAE011@");
            PAE011.setName("PAE011");
            panel1.add(PAE011);
            PAE011.setBounds(450, 340, 180, PAE011.getPreferredSize().height);

            //---- PAE012 ----
            PAE012.setText("@PAE012@");
            PAE012.setName("PAE012");
            panel1.add(PAE012);
            PAE012.setBounds(450, 368, 180, PAE012.getPreferredSize().height);

            //---- PAE013 ----
            PAE013.setText("@PAE013@");
            PAE013.setName("PAE013");
            panel1.add(PAE013);
            PAE013.setBounds(450, 396, 180, PAE013.getPreferredSize().height);

            //---- PAE014 ----
            PAE014.setText("@PAE014@");
            PAE014.setName("PAE014");
            panel1.add(PAE014);
            PAE014.setBounds(450, 424, 180, PAE014.getPreferredSize().height);

            //---- PAE015 ----
            PAE015.setText("@PAE015@");
            PAE015.setName("PAE015");
            panel1.add(PAE015);
            PAE015.setBounds(450, 452, 180, PAE015.getPreferredSize().height);

            //---- PAE016 ----
            PAE016.setText("@PAE016@");
            PAE016.setName("PAE016");
            panel1.add(PAE016);
            PAE016.setBounds(450, 480, 180, PAE016.getPreferredSize().height);

            //---- OBJ_34 ----
            OBJ_34.setText("S\u00e9lection de tous les \u00e9tablissements");
            OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_34.setName("OBJ_34");
            OBJ_34.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_34ActionPerformed(e);
              }
            });
            panel1.add(OBJ_34);
            OBJ_34.setBounds(390, 510, 238, 24);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(640, 60, 25, 205);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(640, 300, 25, 205);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 687, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie PAS001;
  private RiZoneSortie PAS002;
  private RiZoneSortie PAS003;
  private RiZoneSortie PAS004;
  private RiZoneSortie PAS005;
  private RiZoneSortie PAS006;
  private RiZoneSortie PAS007;
  private RiZoneSortie PAS008;
  private RiZoneSortie PAS009;
  private RiZoneSortie PAS010;
  private RiZoneSortie PAS011;
  private RiZoneSortie PAS012;
  private RiZoneSortie PAS013;
  private RiZoneSortie PAS014;
  private RiZoneSortie PAS015;
  private RiZoneSortie PAS016;
  private XRiCheckBox PAT001;
  private XRiCheckBox PAT002;
  private XRiCheckBox PAT003;
  private XRiCheckBox PAT004;
  private XRiCheckBox PAT005;
  private XRiCheckBox PAT006;
  private XRiCheckBox PAT007;
  private XRiCheckBox PAT008;
  private XRiCheckBox PAT009;
  private XRiCheckBox PAT010;
  private XRiCheckBox PAT011;
  private XRiCheckBox PAT012;
  private XRiCheckBox PAT013;
  private XRiCheckBox PAT014;
  private XRiCheckBox PAT015;
  private XRiCheckBox PAT016;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private RiZoneSortie PAN001;
  private RiZoneSortie PAN003;
  private RiZoneSortie PAN004;
  private RiZoneSortie PAN005;
  private RiZoneSortie PAN006;
  private RiZoneSortie PAN007;
  private RiZoneSortie PAN008;
  private RiZoneSortie PAN009;
  private RiZoneSortie PAN010;
  private RiZoneSortie PAN011;
  private RiZoneSortie PAN012;
  private RiZoneSortie PAN013;
  private RiZoneSortie PAN014;
  private RiZoneSortie PAN015;
  private RiZoneSortie PAN016;
  private RiZoneSortie PAN002;
  private XRiTextField PAM001;
  private XRiTextField PAM002;
  private XRiTextField PAM003;
  private XRiTextField PAM004;
  private XRiTextField PAM005;
  private XRiTextField PAM006;
  private XRiTextField PAM007;
  private XRiTextField PAM008;
  private XRiTextField PAM009;
  private XRiTextField PAM010;
  private XRiTextField PAM011;
  private XRiTextField PAM012;
  private XRiTextField PAM013;
  private XRiTextField PAM014;
  private XRiTextField PAM015;
  private XRiTextField PAM016;
  private RiZoneSortie PAE001;
  private RiZoneSortie PAE002;
  private RiZoneSortie PAE003;
  private RiZoneSortie PAE004;
  private RiZoneSortie PAE005;
  private RiZoneSortie PAE006;
  private RiZoneSortie PAE007;
  private RiZoneSortie PAE008;
  private RiZoneSortie PAE009;
  private RiZoneSortie PAE010;
  private RiZoneSortie PAE011;
  private RiZoneSortie PAE012;
  private RiZoneSortie PAE013;
  private RiZoneSortie PAE014;
  private RiZoneSortie PAE015;
  private RiZoneSortie PAE016;
  private JButton OBJ_34;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
