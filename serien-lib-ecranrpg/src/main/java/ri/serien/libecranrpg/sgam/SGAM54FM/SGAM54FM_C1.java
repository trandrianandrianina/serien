
package ri.serien.libecranrpg.sgam.SGAM54FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGAM54FM_C1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM54FM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // HBN010.setEnabled( lexique.isPresent("HBN010"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix des réceptions"));
    
    

    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BN001 = new XRiTextField();
    BN002 = new XRiTextField();
    BN003 = new XRiTextField();
    BN004 = new XRiTextField();
    BN005 = new XRiTextField();
    BN006 = new XRiTextField();
    BN007 = new XRiTextField();
    BN008 = new XRiTextField();
    BN009 = new XRiTextField();
    BN010 = new XRiTextField();
    BN011 = new XRiTextField();
    BN012 = new XRiTextField();
    BN013 = new XRiTextField();
    BN014 = new XRiTextField();
    BN015 = new XRiTextField();
    BN016 = new XRiTextField();
    BN017 = new XRiTextField();
    BN018 = new XRiTextField();
    BN019 = new XRiTextField();
    BN020 = new XRiTextField();
    BN021 = new XRiTextField();
    BN022 = new XRiTextField();
    BN023 = new XRiTextField();
    BN024 = new XRiTextField();
    BN025 = new XRiTextField();
    BN026 = new XRiTextField();
    BN027 = new XRiTextField();
    BN028 = new XRiTextField();
    BN029 = new XRiTextField();
    BN030 = new XRiTextField();
    BN031 = new XRiTextField();
    BN032 = new XRiTextField();
    BN033 = new XRiTextField();
    BN034 = new XRiTextField();
    BN035 = new XRiTextField();
    BN036 = new XRiTextField();
    BN037 = new XRiTextField();
    BN038 = new XRiTextField();
    BN039 = new XRiTextField();
    BN040 = new XRiTextField();
    BN041 = new XRiTextField();
    BN042 = new XRiTextField();
    BN043 = new XRiTextField();
    BN044 = new XRiTextField();
    BN045 = new XRiTextField();
    BN046 = new XRiTextField();
    BN047 = new XRiTextField();
    BN048 = new XRiTextField();
    BN049 = new XRiTextField();
    BN050 = new XRiTextField();
    BN051 = new XRiTextField();
    BN052 = new XRiTextField();
    BN053 = new XRiTextField();
    BN054 = new XRiTextField();
    BN055 = new XRiTextField();
    BN056 = new XRiTextField();
    BN057 = new XRiTextField();
    BN058 = new XRiTextField();
    BN059 = new XRiTextField();
    BN060 = new XRiTextField();
    BN061 = new XRiTextField();
    BN062 = new XRiTextField();
    BN063 = new XRiTextField();
    BN064 = new XRiTextField();
    BN065 = new XRiTextField();
    BN066 = new XRiTextField();
    BN067 = new XRiTextField();
    BN068 = new XRiTextField();
    BN069 = new XRiTextField();
    BN070 = new XRiTextField();
    BN071 = new XRiTextField();
    BN072 = new XRiTextField();
    BN073 = new XRiTextField();
    BN074 = new XRiTextField();
    BN075 = new XRiTextField();
    BN076 = new XRiTextField();
    BN077 = new XRiTextField();
    BN078 = new XRiTextField();
    BN079 = new XRiTextField();
    BN080 = new XRiTextField();
    BN081 = new XRiTextField();
    BN082 = new XRiTextField();
    BN083 = new XRiTextField();
    BN084 = new XRiTextField();
    BN085 = new XRiTextField();
    BN086 = new XRiTextField();
    BN087 = new XRiTextField();
    BN088 = new XRiTextField();
    BN089 = new XRiTextField();
    BN090 = new XRiTextField();
    BN091 = new XRiTextField();
    BN092 = new XRiTextField();
    BN093 = new XRiTextField();
    BN094 = new XRiTextField();
    BN095 = new XRiTextField();
    BN096 = new XRiTextField();
    BN097 = new XRiTextField();
    BN098 = new XRiTextField();
    BN099 = new XRiTextField();
    BN100 = new XRiTextField();
    BN101 = new XRiTextField();
    BN102 = new XRiTextField();
    BN103 = new XRiTextField();
    BN104 = new XRiTextField();
    BN105 = new XRiTextField();
    BN106 = new XRiTextField();
    BN107 = new XRiTextField();
    BN108 = new XRiTextField();
    BN109 = new XRiTextField();
    BN110 = new XRiTextField();
    BN111 = new XRiTextField();
    BN112 = new XRiTextField();
    BN113 = new XRiTextField();
    BN114 = new XRiTextField();
    BN115 = new XRiTextField();
    BN116 = new XRiTextField();
    BN117 = new XRiTextField();
    BN118 = new XRiTextField();
    BN119 = new XRiTextField();
    BN120 = new XRiTextField();
    BN121 = new XRiTextField();
    BN122 = new XRiTextField();
    BN123 = new XRiTextField();
    BN124 = new XRiTextField();
    BN125 = new XRiTextField();
    BN126 = new XRiTextField();
    BS001 = new XRiTextField();
    BS002 = new XRiTextField();
    BS003 = new XRiTextField();
    BS004 = new XRiTextField();
    BS005 = new XRiTextField();
    BS006 = new XRiTextField();
    BS007 = new XRiTextField();
    BS008 = new XRiTextField();
    BS009 = new XRiTextField();
    BS010 = new XRiTextField();
    BS011 = new XRiTextField();
    BS012 = new XRiTextField();
    BS013 = new XRiTextField();
    BS014 = new XRiTextField();
    BS015 = new XRiTextField();
    BS016 = new XRiTextField();
    BS017 = new XRiTextField();
    BS018 = new XRiTextField();
    BS019 = new XRiTextField();
    BS020 = new XRiTextField();
    BS021 = new XRiTextField();
    BS022 = new XRiTextField();
    BS023 = new XRiTextField();
    BS024 = new XRiTextField();
    BS025 = new XRiTextField();
    BS026 = new XRiTextField();
    BS027 = new XRiTextField();
    BS028 = new XRiTextField();
    BS029 = new XRiTextField();
    BS030 = new XRiTextField();
    BS031 = new XRiTextField();
    BS032 = new XRiTextField();
    BS033 = new XRiTextField();
    BS034 = new XRiTextField();
    BS035 = new XRiTextField();
    BS036 = new XRiTextField();
    BS037 = new XRiTextField();
    BS038 = new XRiTextField();
    BS039 = new XRiTextField();
    BS040 = new XRiTextField();
    BS041 = new XRiTextField();
    BS042 = new XRiTextField();
    BS043 = new XRiTextField();
    BS044 = new XRiTextField();
    BS045 = new XRiTextField();
    BS046 = new XRiTextField();
    BS047 = new XRiTextField();
    BS048 = new XRiTextField();
    BS049 = new XRiTextField();
    BS050 = new XRiTextField();
    BS051 = new XRiTextField();
    BS052 = new XRiTextField();
    BS053 = new XRiTextField();
    BS054 = new XRiTextField();
    BS055 = new XRiTextField();
    BS056 = new XRiTextField();
    BS057 = new XRiTextField();
    BS058 = new XRiTextField();
    BS059 = new XRiTextField();
    BS060 = new XRiTextField();
    BS061 = new XRiTextField();
    BS062 = new XRiTextField();
    BS063 = new XRiTextField();
    BS064 = new XRiTextField();
    BS065 = new XRiTextField();
    BS066 = new XRiTextField();
    BS067 = new XRiTextField();
    BS068 = new XRiTextField();
    BS069 = new XRiTextField();
    BS070 = new XRiTextField();
    BS071 = new XRiTextField();
    BS072 = new XRiTextField();
    BS073 = new XRiTextField();
    BS074 = new XRiTextField();
    BS075 = new XRiTextField();
    BS076 = new XRiTextField();
    BS077 = new XRiTextField();
    BS078 = new XRiTextField();
    BS079 = new XRiTextField();
    BS080 = new XRiTextField();
    BS081 = new XRiTextField();
    BS082 = new XRiTextField();
    BS083 = new XRiTextField();
    BS084 = new XRiTextField();
    BS085 = new XRiTextField();
    BS086 = new XRiTextField();
    BS087 = new XRiTextField();
    BS088 = new XRiTextField();
    BS089 = new XRiTextField();
    BS090 = new XRiTextField();
    BS091 = new XRiTextField();
    BS092 = new XRiTextField();
    BS093 = new XRiTextField();
    BS094 = new XRiTextField();
    BS095 = new XRiTextField();
    BS096 = new XRiTextField();
    BS097 = new XRiTextField();
    BS098 = new XRiTextField();
    BS099 = new XRiTextField();
    BS100 = new XRiTextField();
    BS101 = new XRiTextField();
    BS102 = new XRiTextField();
    BS103 = new XRiTextField();
    BS104 = new XRiTextField();
    BS105 = new XRiTextField();
    BS106 = new XRiTextField();
    BS107 = new XRiTextField();
    BS108 = new XRiTextField();
    BS109 = new XRiTextField();
    BS110 = new XRiTextField();
    BS111 = new XRiTextField();
    BS112 = new XRiTextField();
    BS113 = new XRiTextField();
    BS114 = new XRiTextField();
    BS115 = new XRiTextField();
    BS116 = new XRiTextField();
    BS117 = new XRiTextField();
    BS118 = new XRiTextField();
    BS119 = new XRiTextField();
    BS120 = new XRiTextField();
    BS121 = new XRiTextField();
    BS122 = new XRiTextField();
    BS123 = new XRiTextField();
    BS124 = new XRiTextField();
    BS125 = new XRiTextField();
    BS126 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_8 = new JMenuItem();
    OBJ_7 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(905, 535));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Choix des r\u00e9ceptions"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(660, 35, 25, 180);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(660, 308, 25, 180);

          //---- BN001 ----
          BN001.setName("BN001");
          panel1.add(BN001);
          BN001.setBounds(15, 35, 55, BN001.getPreferredSize().height);

          //---- BN002 ----
          BN002.setName("BN002");
          panel1.add(BN002);
          BN002.setBounds(107, 35, 55, BN002.getPreferredSize().height);

          //---- BN003 ----
          BN003.setName("BN003");
          panel1.add(BN003);
          BN003.setBounds(199, 35, 55, BN003.getPreferredSize().height);

          //---- BN004 ----
          BN004.setName("BN004");
          panel1.add(BN004);
          BN004.setBounds(291, 35, 55, BN004.getPreferredSize().height);

          //---- BN005 ----
          BN005.setName("BN005");
          panel1.add(BN005);
          BN005.setBounds(383, 35, 55, BN005.getPreferredSize().height);

          //---- BN006 ----
          BN006.setName("BN006");
          panel1.add(BN006);
          BN006.setBounds(475, 35, 55, BN006.getPreferredSize().height);

          //---- BN007 ----
          BN007.setName("BN007");
          panel1.add(BN007);
          BN007.setBounds(567, 35, 55, BN007.getPreferredSize().height);

          //---- BN008 ----
          BN008.setName("BN008");
          panel1.add(BN008);
          BN008.setBounds(15, 60, 55, BN008.getPreferredSize().height);

          //---- BN009 ----
          BN009.setName("BN009");
          panel1.add(BN009);
          BN009.setBounds(107, 60, 55, BN009.getPreferredSize().height);

          //---- BN010 ----
          BN010.setName("BN010");
          panel1.add(BN010);
          BN010.setBounds(199, 60, 55, BN010.getPreferredSize().height);

          //---- BN011 ----
          BN011.setName("BN011");
          panel1.add(BN011);
          BN011.setBounds(291, 60, 55, BN011.getPreferredSize().height);

          //---- BN012 ----
          BN012.setName("BN012");
          panel1.add(BN012);
          BN012.setBounds(383, 60, 55, BN012.getPreferredSize().height);

          //---- BN013 ----
          BN013.setName("BN013");
          panel1.add(BN013);
          BN013.setBounds(475, 60, 55, BN013.getPreferredSize().height);

          //---- BN014 ----
          BN014.setName("BN014");
          panel1.add(BN014);
          BN014.setBounds(567, 60, 55, BN014.getPreferredSize().height);

          //---- BN015 ----
          BN015.setName("BN015");
          panel1.add(BN015);
          BN015.setBounds(15, 85, 55, BN015.getPreferredSize().height);

          //---- BN016 ----
          BN016.setName("BN016");
          panel1.add(BN016);
          BN016.setBounds(107, 85, 55, BN016.getPreferredSize().height);

          //---- BN017 ----
          BN017.setName("BN017");
          panel1.add(BN017);
          BN017.setBounds(199, 85, 55, BN017.getPreferredSize().height);

          //---- BN018 ----
          BN018.setName("BN018");
          panel1.add(BN018);
          BN018.setBounds(291, 85, 55, BN018.getPreferredSize().height);

          //---- BN019 ----
          BN019.setName("BN019");
          panel1.add(BN019);
          BN019.setBounds(383, 85, 55, BN019.getPreferredSize().height);

          //---- BN020 ----
          BN020.setName("BN020");
          panel1.add(BN020);
          BN020.setBounds(475, 85, 55, BN020.getPreferredSize().height);

          //---- BN021 ----
          BN021.setName("BN021");
          panel1.add(BN021);
          BN021.setBounds(567, 85, 55, BN021.getPreferredSize().height);

          //---- BN022 ----
          BN022.setName("BN022");
          panel1.add(BN022);
          BN022.setBounds(15, 110, 55, BN022.getPreferredSize().height);

          //---- BN023 ----
          BN023.setName("BN023");
          panel1.add(BN023);
          BN023.setBounds(107, 110, 55, BN023.getPreferredSize().height);

          //---- BN024 ----
          BN024.setName("BN024");
          panel1.add(BN024);
          BN024.setBounds(199, 110, 55, BN024.getPreferredSize().height);

          //---- BN025 ----
          BN025.setName("BN025");
          panel1.add(BN025);
          BN025.setBounds(291, 110, 55, BN025.getPreferredSize().height);

          //---- BN026 ----
          BN026.setName("BN026");
          panel1.add(BN026);
          BN026.setBounds(383, 110, 55, BN026.getPreferredSize().height);

          //---- BN027 ----
          BN027.setName("BN027");
          panel1.add(BN027);
          BN027.setBounds(475, 110, 55, BN027.getPreferredSize().height);

          //---- BN028 ----
          BN028.setName("BN028");
          panel1.add(BN028);
          BN028.setBounds(567, 110, 55, BN028.getPreferredSize().height);

          //---- BN029 ----
          BN029.setName("BN029");
          panel1.add(BN029);
          BN029.setBounds(15, 135, 55, BN029.getPreferredSize().height);

          //---- BN030 ----
          BN030.setName("BN030");
          panel1.add(BN030);
          BN030.setBounds(107, 135, 55, BN030.getPreferredSize().height);

          //---- BN031 ----
          BN031.setName("BN031");
          panel1.add(BN031);
          BN031.setBounds(199, 135, 55, BN031.getPreferredSize().height);

          //---- BN032 ----
          BN032.setName("BN032");
          panel1.add(BN032);
          BN032.setBounds(291, 135, 55, BN032.getPreferredSize().height);

          //---- BN033 ----
          BN033.setName("BN033");
          panel1.add(BN033);
          BN033.setBounds(383, 135, 55, BN033.getPreferredSize().height);

          //---- BN034 ----
          BN034.setName("BN034");
          panel1.add(BN034);
          BN034.setBounds(475, 135, 55, BN034.getPreferredSize().height);

          //---- BN035 ----
          BN035.setName("BN035");
          panel1.add(BN035);
          BN035.setBounds(567, 135, 55, BN035.getPreferredSize().height);

          //---- BN036 ----
          BN036.setName("BN036");
          panel1.add(BN036);
          BN036.setBounds(15, 160, 55, BN036.getPreferredSize().height);

          //---- BN037 ----
          BN037.setName("BN037");
          panel1.add(BN037);
          BN037.setBounds(107, 160, 55, BN037.getPreferredSize().height);

          //---- BN038 ----
          BN038.setName("BN038");
          panel1.add(BN038);
          BN038.setBounds(199, 160, 55, BN038.getPreferredSize().height);

          //---- BN039 ----
          BN039.setName("BN039");
          panel1.add(BN039);
          BN039.setBounds(291, 160, 55, BN039.getPreferredSize().height);

          //---- BN040 ----
          BN040.setName("BN040");
          panel1.add(BN040);
          BN040.setBounds(383, 160, 55, BN040.getPreferredSize().height);

          //---- BN041 ----
          BN041.setName("BN041");
          panel1.add(BN041);
          BN041.setBounds(475, 160, 55, BN041.getPreferredSize().height);

          //---- BN042 ----
          BN042.setName("BN042");
          panel1.add(BN042);
          BN042.setBounds(567, 160, 55, BN042.getPreferredSize().height);

          //---- BN043 ----
          BN043.setName("BN043");
          panel1.add(BN043);
          BN043.setBounds(15, 185, 55, BN043.getPreferredSize().height);

          //---- BN044 ----
          BN044.setName("BN044");
          panel1.add(BN044);
          BN044.setBounds(107, 185, 55, BN044.getPreferredSize().height);

          //---- BN045 ----
          BN045.setName("BN045");
          panel1.add(BN045);
          BN045.setBounds(199, 185, 55, BN045.getPreferredSize().height);

          //---- BN046 ----
          BN046.setName("BN046");
          panel1.add(BN046);
          BN046.setBounds(291, 185, 55, BN046.getPreferredSize().height);

          //---- BN047 ----
          BN047.setName("BN047");
          panel1.add(BN047);
          BN047.setBounds(383, 185, 55, BN047.getPreferredSize().height);

          //---- BN048 ----
          BN048.setName("BN048");
          panel1.add(BN048);
          BN048.setBounds(475, 185, 55, BN048.getPreferredSize().height);

          //---- BN049 ----
          BN049.setName("BN049");
          panel1.add(BN049);
          BN049.setBounds(567, 185, 55, BN049.getPreferredSize().height);

          //---- BN050 ----
          BN050.setName("BN050");
          panel1.add(BN050);
          BN050.setBounds(15, 210, 55, BN050.getPreferredSize().height);

          //---- BN051 ----
          BN051.setName("BN051");
          panel1.add(BN051);
          BN051.setBounds(107, 210, 55, BN051.getPreferredSize().height);

          //---- BN052 ----
          BN052.setName("BN052");
          panel1.add(BN052);
          BN052.setBounds(199, 210, 55, BN052.getPreferredSize().height);

          //---- BN053 ----
          BN053.setName("BN053");
          panel1.add(BN053);
          BN053.setBounds(291, 210, 55, BN053.getPreferredSize().height);

          //---- BN054 ----
          BN054.setName("BN054");
          panel1.add(BN054);
          BN054.setBounds(383, 210, 55, BN054.getPreferredSize().height);

          //---- BN055 ----
          BN055.setName("BN055");
          panel1.add(BN055);
          BN055.setBounds(475, 210, 55, BN055.getPreferredSize().height);

          //---- BN056 ----
          BN056.setName("BN056");
          panel1.add(BN056);
          BN056.setBounds(567, 210, 55, BN056.getPreferredSize().height);

          //---- BN057 ----
          BN057.setName("BN057");
          panel1.add(BN057);
          BN057.setBounds(15, 235, 55, BN057.getPreferredSize().height);

          //---- BN058 ----
          BN058.setName("BN058");
          panel1.add(BN058);
          BN058.setBounds(107, 235, 55, BN058.getPreferredSize().height);

          //---- BN059 ----
          BN059.setName("BN059");
          panel1.add(BN059);
          BN059.setBounds(199, 235, 55, BN059.getPreferredSize().height);

          //---- BN060 ----
          BN060.setName("BN060");
          panel1.add(BN060);
          BN060.setBounds(291, 235, 55, BN060.getPreferredSize().height);

          //---- BN061 ----
          BN061.setName("BN061");
          panel1.add(BN061);
          BN061.setBounds(383, 235, 55, BN061.getPreferredSize().height);

          //---- BN062 ----
          BN062.setName("BN062");
          panel1.add(BN062);
          BN062.setBounds(475, 235, 55, BN062.getPreferredSize().height);

          //---- BN063 ----
          BN063.setName("BN063");
          panel1.add(BN063);
          BN063.setBounds(567, 235, 55, BN063.getPreferredSize().height);

          //---- BN064 ----
          BN064.setName("BN064");
          panel1.add(BN064);
          BN064.setBounds(15, 260, 55, BN064.getPreferredSize().height);

          //---- BN065 ----
          BN065.setName("BN065");
          panel1.add(BN065);
          BN065.setBounds(107, 260, 55, BN065.getPreferredSize().height);

          //---- BN066 ----
          BN066.setName("BN066");
          panel1.add(BN066);
          BN066.setBounds(199, 260, 55, BN066.getPreferredSize().height);

          //---- BN067 ----
          BN067.setName("BN067");
          panel1.add(BN067);
          BN067.setBounds(291, 260, 55, BN067.getPreferredSize().height);

          //---- BN068 ----
          BN068.setName("BN068");
          panel1.add(BN068);
          BN068.setBounds(383, 260, 55, BN068.getPreferredSize().height);

          //---- BN069 ----
          BN069.setName("BN069");
          panel1.add(BN069);
          BN069.setBounds(475, 260, 55, BN069.getPreferredSize().height);

          //---- BN070 ----
          BN070.setName("BN070");
          panel1.add(BN070);
          BN070.setBounds(567, 260, 55, BN070.getPreferredSize().height);

          //---- BN071 ----
          BN071.setName("BN071");
          panel1.add(BN071);
          BN071.setBounds(15, 285, 55, BN071.getPreferredSize().height);

          //---- BN072 ----
          BN072.setName("BN072");
          panel1.add(BN072);
          BN072.setBounds(107, 285, 55, BN072.getPreferredSize().height);

          //---- BN073 ----
          BN073.setName("BN073");
          panel1.add(BN073);
          BN073.setBounds(199, 285, 55, BN073.getPreferredSize().height);

          //---- BN074 ----
          BN074.setName("BN074");
          panel1.add(BN074);
          BN074.setBounds(291, 285, 55, BN074.getPreferredSize().height);

          //---- BN075 ----
          BN075.setName("BN075");
          panel1.add(BN075);
          BN075.setBounds(383, 285, 55, BN075.getPreferredSize().height);

          //---- BN076 ----
          BN076.setName("BN076");
          panel1.add(BN076);
          BN076.setBounds(475, 285, 55, BN076.getPreferredSize().height);

          //---- BN077 ----
          BN077.setName("BN077");
          panel1.add(BN077);
          BN077.setBounds(567, 285, 55, BN077.getPreferredSize().height);

          //---- BN078 ----
          BN078.setName("BN078");
          panel1.add(BN078);
          BN078.setBounds(15, 310, 55, BN078.getPreferredSize().height);

          //---- BN079 ----
          BN079.setName("BN079");
          panel1.add(BN079);
          BN079.setBounds(107, 310, 55, BN079.getPreferredSize().height);

          //---- BN080 ----
          BN080.setName("BN080");
          panel1.add(BN080);
          BN080.setBounds(199, 310, 55, BN080.getPreferredSize().height);

          //---- BN081 ----
          BN081.setName("BN081");
          panel1.add(BN081);
          BN081.setBounds(291, 310, 55, BN081.getPreferredSize().height);

          //---- BN082 ----
          BN082.setName("BN082");
          panel1.add(BN082);
          BN082.setBounds(383, 310, 55, BN082.getPreferredSize().height);

          //---- BN083 ----
          BN083.setName("BN083");
          panel1.add(BN083);
          BN083.setBounds(475, 310, 55, BN083.getPreferredSize().height);

          //---- BN084 ----
          BN084.setName("BN084");
          panel1.add(BN084);
          BN084.setBounds(567, 310, 55, BN084.getPreferredSize().height);

          //---- BN085 ----
          BN085.setName("BN085");
          panel1.add(BN085);
          BN085.setBounds(15, 335, 55, BN085.getPreferredSize().height);

          //---- BN086 ----
          BN086.setName("BN086");
          panel1.add(BN086);
          BN086.setBounds(107, 335, 55, BN086.getPreferredSize().height);

          //---- BN087 ----
          BN087.setName("BN087");
          panel1.add(BN087);
          BN087.setBounds(199, 335, 55, BN087.getPreferredSize().height);

          //---- BN088 ----
          BN088.setName("BN088");
          panel1.add(BN088);
          BN088.setBounds(291, 335, 55, BN088.getPreferredSize().height);

          //---- BN089 ----
          BN089.setName("BN089");
          panel1.add(BN089);
          BN089.setBounds(383, 335, 55, BN089.getPreferredSize().height);

          //---- BN090 ----
          BN090.setName("BN090");
          panel1.add(BN090);
          BN090.setBounds(475, 335, 55, BN090.getPreferredSize().height);

          //---- BN091 ----
          BN091.setName("BN091");
          panel1.add(BN091);
          BN091.setBounds(567, 335, 55, BN091.getPreferredSize().height);

          //---- BN092 ----
          BN092.setName("BN092");
          panel1.add(BN092);
          BN092.setBounds(15, 360, 55, BN092.getPreferredSize().height);

          //---- BN093 ----
          BN093.setName("BN093");
          panel1.add(BN093);
          BN093.setBounds(107, 360, 55, BN093.getPreferredSize().height);

          //---- BN094 ----
          BN094.setName("BN094");
          panel1.add(BN094);
          BN094.setBounds(199, 360, 55, BN094.getPreferredSize().height);

          //---- BN095 ----
          BN095.setName("BN095");
          panel1.add(BN095);
          BN095.setBounds(291, 360, 55, BN095.getPreferredSize().height);

          //---- BN096 ----
          BN096.setName("BN096");
          panel1.add(BN096);
          BN096.setBounds(383, 360, 55, BN096.getPreferredSize().height);

          //---- BN097 ----
          BN097.setName("BN097");
          panel1.add(BN097);
          BN097.setBounds(475, 360, 55, BN097.getPreferredSize().height);

          //---- BN098 ----
          BN098.setName("BN098");
          panel1.add(BN098);
          BN098.setBounds(567, 360, 55, BN098.getPreferredSize().height);

          //---- BN099 ----
          BN099.setName("BN099");
          panel1.add(BN099);
          BN099.setBounds(15, 385, 55, BN099.getPreferredSize().height);

          //---- BN100 ----
          BN100.setName("BN100");
          panel1.add(BN100);
          BN100.setBounds(107, 385, 55, BN100.getPreferredSize().height);

          //---- BN101 ----
          BN101.setName("BN101");
          panel1.add(BN101);
          BN101.setBounds(199, 385, 55, BN101.getPreferredSize().height);

          //---- BN102 ----
          BN102.setName("BN102");
          panel1.add(BN102);
          BN102.setBounds(291, 385, 55, BN102.getPreferredSize().height);

          //---- BN103 ----
          BN103.setName("BN103");
          panel1.add(BN103);
          BN103.setBounds(383, 385, 55, BN103.getPreferredSize().height);

          //---- BN104 ----
          BN104.setName("BN104");
          panel1.add(BN104);
          BN104.setBounds(475, 385, 55, BN104.getPreferredSize().height);

          //---- BN105 ----
          BN105.setName("BN105");
          panel1.add(BN105);
          BN105.setBounds(567, 385, 55, BN105.getPreferredSize().height);

          //---- BN106 ----
          BN106.setName("BN106");
          panel1.add(BN106);
          BN106.setBounds(15, 410, 55, BN106.getPreferredSize().height);

          //---- BN107 ----
          BN107.setName("BN107");
          panel1.add(BN107);
          BN107.setBounds(107, 410, 55, BN107.getPreferredSize().height);

          //---- BN108 ----
          BN108.setName("BN108");
          panel1.add(BN108);
          BN108.setBounds(199, 410, 55, BN108.getPreferredSize().height);

          //---- BN109 ----
          BN109.setName("BN109");
          panel1.add(BN109);
          BN109.setBounds(291, 410, 55, BN109.getPreferredSize().height);

          //---- BN110 ----
          BN110.setName("BN110");
          panel1.add(BN110);
          BN110.setBounds(383, 410, 55, BN110.getPreferredSize().height);

          //---- BN111 ----
          BN111.setName("BN111");
          panel1.add(BN111);
          BN111.setBounds(475, 410, 55, BN111.getPreferredSize().height);

          //---- BN112 ----
          BN112.setName("BN112");
          panel1.add(BN112);
          BN112.setBounds(567, 410, 55, BN112.getPreferredSize().height);

          //---- BN113 ----
          BN113.setName("BN113");
          panel1.add(BN113);
          BN113.setBounds(15, 435, 55, BN113.getPreferredSize().height);

          //---- BN114 ----
          BN114.setName("BN114");
          panel1.add(BN114);
          BN114.setBounds(107, 435, 55, BN114.getPreferredSize().height);

          //---- BN115 ----
          BN115.setName("BN115");
          panel1.add(BN115);
          BN115.setBounds(199, 435, 55, BN115.getPreferredSize().height);

          //---- BN116 ----
          BN116.setName("BN116");
          panel1.add(BN116);
          BN116.setBounds(291, 435, 55, BN116.getPreferredSize().height);

          //---- BN117 ----
          BN117.setName("BN117");
          panel1.add(BN117);
          BN117.setBounds(383, 435, 55, BN117.getPreferredSize().height);

          //---- BN118 ----
          BN118.setName("BN118");
          panel1.add(BN118);
          BN118.setBounds(475, 435, 55, BN118.getPreferredSize().height);

          //---- BN119 ----
          BN119.setName("BN119");
          panel1.add(BN119);
          BN119.setBounds(567, 435, 55, BN119.getPreferredSize().height);

          //---- BN120 ----
          BN120.setName("BN120");
          panel1.add(BN120);
          BN120.setBounds(15, 460, 55, BN120.getPreferredSize().height);

          //---- BN121 ----
          BN121.setName("BN121");
          panel1.add(BN121);
          BN121.setBounds(107, 460, 55, BN121.getPreferredSize().height);

          //---- BN122 ----
          BN122.setName("BN122");
          panel1.add(BN122);
          BN122.setBounds(199, 460, 55, BN122.getPreferredSize().height);

          //---- BN123 ----
          BN123.setName("BN123");
          panel1.add(BN123);
          BN123.setBounds(291, 460, 55, BN123.getPreferredSize().height);

          //---- BN124 ----
          BN124.setName("BN124");
          panel1.add(BN124);
          BN124.setBounds(383, 460, 55, BN124.getPreferredSize().height);

          //---- BN125 ----
          BN125.setName("BN125");
          panel1.add(BN125);
          BN125.setBounds(475, 460, 55, BN125.getPreferredSize().height);

          //---- BN126 ----
          BN126.setName("BN126");
          panel1.add(BN126);
          BN126.setBounds(567, 460, 55, BN126.getPreferredSize().height);

          //---- BS001 ----
          BS001.setName("BS001");
          panel1.add(BS001);
          BS001.setBounds(70, 35, 24, BS001.getPreferredSize().height);

          //---- BS002 ----
          BS002.setName("BS002");
          panel1.add(BS002);
          BS002.setBounds(162, 35, 24, BS002.getPreferredSize().height);

          //---- BS003 ----
          BS003.setName("BS003");
          panel1.add(BS003);
          BS003.setBounds(254, 35, 24, BS003.getPreferredSize().height);

          //---- BS004 ----
          BS004.setName("BS004");
          panel1.add(BS004);
          BS004.setBounds(346, 35, 24, BS004.getPreferredSize().height);

          //---- BS005 ----
          BS005.setName("BS005");
          panel1.add(BS005);
          BS005.setBounds(438, 35, 24, BS005.getPreferredSize().height);

          //---- BS006 ----
          BS006.setName("BS006");
          panel1.add(BS006);
          BS006.setBounds(530, 35, 24, BS006.getPreferredSize().height);

          //---- BS007 ----
          BS007.setName("BS007");
          panel1.add(BS007);
          BS007.setBounds(622, 35, 24, BS007.getPreferredSize().height);

          //---- BS008 ----
          BS008.setName("BS008");
          panel1.add(BS008);
          BS008.setBounds(70, 60, 24, BS008.getPreferredSize().height);

          //---- BS009 ----
          BS009.setName("BS009");
          panel1.add(BS009);
          BS009.setBounds(162, 60, 24, BS009.getPreferredSize().height);

          //---- BS010 ----
          BS010.setName("BS010");
          panel1.add(BS010);
          BS010.setBounds(254, 60, 24, BS010.getPreferredSize().height);

          //---- BS011 ----
          BS011.setName("BS011");
          panel1.add(BS011);
          BS011.setBounds(346, 60, 24, BS011.getPreferredSize().height);

          //---- BS012 ----
          BS012.setName("BS012");
          panel1.add(BS012);
          BS012.setBounds(438, 60, 24, BS012.getPreferredSize().height);

          //---- BS013 ----
          BS013.setName("BS013");
          panel1.add(BS013);
          BS013.setBounds(530, 60, 24, BS013.getPreferredSize().height);

          //---- BS014 ----
          BS014.setName("BS014");
          panel1.add(BS014);
          BS014.setBounds(622, 60, 24, BS014.getPreferredSize().height);

          //---- BS015 ----
          BS015.setName("BS015");
          panel1.add(BS015);
          BS015.setBounds(70, 85, 24, BS015.getPreferredSize().height);

          //---- BS016 ----
          BS016.setName("BS016");
          panel1.add(BS016);
          BS016.setBounds(162, 85, 24, BS016.getPreferredSize().height);

          //---- BS017 ----
          BS017.setName("BS017");
          panel1.add(BS017);
          BS017.setBounds(254, 85, 24, BS017.getPreferredSize().height);

          //---- BS018 ----
          BS018.setName("BS018");
          panel1.add(BS018);
          BS018.setBounds(346, 85, 24, BS018.getPreferredSize().height);

          //---- BS019 ----
          BS019.setName("BS019");
          panel1.add(BS019);
          BS019.setBounds(438, 85, 24, BS019.getPreferredSize().height);

          //---- BS020 ----
          BS020.setName("BS020");
          panel1.add(BS020);
          BS020.setBounds(530, 85, 24, BS020.getPreferredSize().height);

          //---- BS021 ----
          BS021.setName("BS021");
          panel1.add(BS021);
          BS021.setBounds(622, 85, 24, BS021.getPreferredSize().height);

          //---- BS022 ----
          BS022.setName("BS022");
          panel1.add(BS022);
          BS022.setBounds(70, 110, 24, BS022.getPreferredSize().height);

          //---- BS023 ----
          BS023.setName("BS023");
          panel1.add(BS023);
          BS023.setBounds(162, 110, 24, BS023.getPreferredSize().height);

          //---- BS024 ----
          BS024.setName("BS024");
          panel1.add(BS024);
          BS024.setBounds(254, 110, 24, BS024.getPreferredSize().height);

          //---- BS025 ----
          BS025.setName("BS025");
          panel1.add(BS025);
          BS025.setBounds(346, 110, 24, BS025.getPreferredSize().height);

          //---- BS026 ----
          BS026.setName("BS026");
          panel1.add(BS026);
          BS026.setBounds(438, 110, 24, BS026.getPreferredSize().height);

          //---- BS027 ----
          BS027.setName("BS027");
          panel1.add(BS027);
          BS027.setBounds(530, 110, 24, BS027.getPreferredSize().height);

          //---- BS028 ----
          BS028.setName("BS028");
          panel1.add(BS028);
          BS028.setBounds(622, 110, 24, BS028.getPreferredSize().height);

          //---- BS029 ----
          BS029.setName("BS029");
          panel1.add(BS029);
          BS029.setBounds(70, 135, 24, BS029.getPreferredSize().height);

          //---- BS030 ----
          BS030.setName("BS030");
          panel1.add(BS030);
          BS030.setBounds(162, 135, 24, BS030.getPreferredSize().height);

          //---- BS031 ----
          BS031.setName("BS031");
          panel1.add(BS031);
          BS031.setBounds(254, 135, 24, BS031.getPreferredSize().height);

          //---- BS032 ----
          BS032.setName("BS032");
          panel1.add(BS032);
          BS032.setBounds(346, 135, 24, BS032.getPreferredSize().height);

          //---- BS033 ----
          BS033.setName("BS033");
          panel1.add(BS033);
          BS033.setBounds(438, 135, 24, BS033.getPreferredSize().height);

          //---- BS034 ----
          BS034.setName("BS034");
          panel1.add(BS034);
          BS034.setBounds(530, 135, 24, BS034.getPreferredSize().height);

          //---- BS035 ----
          BS035.setName("BS035");
          panel1.add(BS035);
          BS035.setBounds(622, 135, 24, BS035.getPreferredSize().height);

          //---- BS036 ----
          BS036.setName("BS036");
          panel1.add(BS036);
          BS036.setBounds(70, 160, 24, BS036.getPreferredSize().height);

          //---- BS037 ----
          BS037.setName("BS037");
          panel1.add(BS037);
          BS037.setBounds(162, 160, 24, BS037.getPreferredSize().height);

          //---- BS038 ----
          BS038.setName("BS038");
          panel1.add(BS038);
          BS038.setBounds(254, 160, 24, BS038.getPreferredSize().height);

          //---- BS039 ----
          BS039.setName("BS039");
          panel1.add(BS039);
          BS039.setBounds(346, 160, 24, BS039.getPreferredSize().height);

          //---- BS040 ----
          BS040.setName("BS040");
          panel1.add(BS040);
          BS040.setBounds(438, 160, 24, BS040.getPreferredSize().height);

          //---- BS041 ----
          BS041.setName("BS041");
          panel1.add(BS041);
          BS041.setBounds(530, 160, 24, BS041.getPreferredSize().height);

          //---- BS042 ----
          BS042.setName("BS042");
          panel1.add(BS042);
          BS042.setBounds(622, 160, 24, BS042.getPreferredSize().height);

          //---- BS043 ----
          BS043.setName("BS043");
          panel1.add(BS043);
          BS043.setBounds(70, 185, 24, BS043.getPreferredSize().height);

          //---- BS044 ----
          BS044.setName("BS044");
          panel1.add(BS044);
          BS044.setBounds(162, 185, 24, BS044.getPreferredSize().height);

          //---- BS045 ----
          BS045.setName("BS045");
          panel1.add(BS045);
          BS045.setBounds(254, 185, 24, BS045.getPreferredSize().height);

          //---- BS046 ----
          BS046.setName("BS046");
          panel1.add(BS046);
          BS046.setBounds(346, 185, 24, BS046.getPreferredSize().height);

          //---- BS047 ----
          BS047.setName("BS047");
          panel1.add(BS047);
          BS047.setBounds(438, 185, 24, BS047.getPreferredSize().height);

          //---- BS048 ----
          BS048.setName("BS048");
          panel1.add(BS048);
          BS048.setBounds(530, 185, 24, BS048.getPreferredSize().height);

          //---- BS049 ----
          BS049.setName("BS049");
          panel1.add(BS049);
          BS049.setBounds(622, 185, 24, BS049.getPreferredSize().height);

          //---- BS050 ----
          BS050.setName("BS050");
          panel1.add(BS050);
          BS050.setBounds(70, 210, 24, BS050.getPreferredSize().height);

          //---- BS051 ----
          BS051.setName("BS051");
          panel1.add(BS051);
          BS051.setBounds(162, 210, 24, BS051.getPreferredSize().height);

          //---- BS052 ----
          BS052.setName("BS052");
          panel1.add(BS052);
          BS052.setBounds(254, 210, 24, BS052.getPreferredSize().height);

          //---- BS053 ----
          BS053.setName("BS053");
          panel1.add(BS053);
          BS053.setBounds(346, 210, 24, BS053.getPreferredSize().height);

          //---- BS054 ----
          BS054.setName("BS054");
          panel1.add(BS054);
          BS054.setBounds(438, 210, 24, BS054.getPreferredSize().height);

          //---- BS055 ----
          BS055.setName("BS055");
          panel1.add(BS055);
          BS055.setBounds(530, 210, 24, BS055.getPreferredSize().height);

          //---- BS056 ----
          BS056.setName("BS056");
          panel1.add(BS056);
          BS056.setBounds(622, 210, 24, BS056.getPreferredSize().height);

          //---- BS057 ----
          BS057.setName("BS057");
          panel1.add(BS057);
          BS057.setBounds(70, 235, 24, BS057.getPreferredSize().height);

          //---- BS058 ----
          BS058.setName("BS058");
          panel1.add(BS058);
          BS058.setBounds(162, 235, 24, BS058.getPreferredSize().height);

          //---- BS059 ----
          BS059.setName("BS059");
          panel1.add(BS059);
          BS059.setBounds(254, 235, 24, BS059.getPreferredSize().height);

          //---- BS060 ----
          BS060.setName("BS060");
          panel1.add(BS060);
          BS060.setBounds(346, 235, 24, BS060.getPreferredSize().height);

          //---- BS061 ----
          BS061.setName("BS061");
          panel1.add(BS061);
          BS061.setBounds(438, 235, 24, BS061.getPreferredSize().height);

          //---- BS062 ----
          BS062.setName("BS062");
          panel1.add(BS062);
          BS062.setBounds(530, 235, 24, BS062.getPreferredSize().height);

          //---- BS063 ----
          BS063.setName("BS063");
          panel1.add(BS063);
          BS063.setBounds(622, 235, 24, BS063.getPreferredSize().height);

          //---- BS064 ----
          BS064.setName("BS064");
          panel1.add(BS064);
          BS064.setBounds(70, 260, 24, BS064.getPreferredSize().height);

          //---- BS065 ----
          BS065.setName("BS065");
          panel1.add(BS065);
          BS065.setBounds(162, 260, 24, BS065.getPreferredSize().height);

          //---- BS066 ----
          BS066.setName("BS066");
          panel1.add(BS066);
          BS066.setBounds(254, 260, 24, BS066.getPreferredSize().height);

          //---- BS067 ----
          BS067.setName("BS067");
          panel1.add(BS067);
          BS067.setBounds(346, 260, 24, BS067.getPreferredSize().height);

          //---- BS068 ----
          BS068.setName("BS068");
          panel1.add(BS068);
          BS068.setBounds(438, 260, 24, BS068.getPreferredSize().height);

          //---- BS069 ----
          BS069.setName("BS069");
          panel1.add(BS069);
          BS069.setBounds(530, 260, 24, BS069.getPreferredSize().height);

          //---- BS070 ----
          BS070.setName("BS070");
          panel1.add(BS070);
          BS070.setBounds(622, 260, 24, BS070.getPreferredSize().height);

          //---- BS071 ----
          BS071.setName("BS071");
          panel1.add(BS071);
          BS071.setBounds(70, 285, 24, BS071.getPreferredSize().height);

          //---- BS072 ----
          BS072.setName("BS072");
          panel1.add(BS072);
          BS072.setBounds(162, 285, 24, BS072.getPreferredSize().height);

          //---- BS073 ----
          BS073.setName("BS073");
          panel1.add(BS073);
          BS073.setBounds(254, 285, 24, BS073.getPreferredSize().height);

          //---- BS074 ----
          BS074.setName("BS074");
          panel1.add(BS074);
          BS074.setBounds(346, 285, 24, BS074.getPreferredSize().height);

          //---- BS075 ----
          BS075.setName("BS075");
          panel1.add(BS075);
          BS075.setBounds(438, 285, 24, BS075.getPreferredSize().height);

          //---- BS076 ----
          BS076.setName("BS076");
          panel1.add(BS076);
          BS076.setBounds(530, 285, 24, BS076.getPreferredSize().height);

          //---- BS077 ----
          BS077.setName("BS077");
          panel1.add(BS077);
          BS077.setBounds(622, 285, 24, BS077.getPreferredSize().height);

          //---- BS078 ----
          BS078.setName("BS078");
          panel1.add(BS078);
          BS078.setBounds(70, 310, 24, BS078.getPreferredSize().height);

          //---- BS079 ----
          BS079.setName("BS079");
          panel1.add(BS079);
          BS079.setBounds(162, 310, 24, BS079.getPreferredSize().height);

          //---- BS080 ----
          BS080.setName("BS080");
          panel1.add(BS080);
          BS080.setBounds(254, 310, 24, BS080.getPreferredSize().height);

          //---- BS081 ----
          BS081.setName("BS081");
          panel1.add(BS081);
          BS081.setBounds(346, 310, 24, BS081.getPreferredSize().height);

          //---- BS082 ----
          BS082.setName("BS082");
          panel1.add(BS082);
          BS082.setBounds(438, 310, 24, BS082.getPreferredSize().height);

          //---- BS083 ----
          BS083.setName("BS083");
          panel1.add(BS083);
          BS083.setBounds(530, 310, 24, BS083.getPreferredSize().height);

          //---- BS084 ----
          BS084.setName("BS084");
          panel1.add(BS084);
          BS084.setBounds(622, 310, 24, BS084.getPreferredSize().height);

          //---- BS085 ----
          BS085.setName("BS085");
          panel1.add(BS085);
          BS085.setBounds(70, 335, 24, BS085.getPreferredSize().height);

          //---- BS086 ----
          BS086.setName("BS086");
          panel1.add(BS086);
          BS086.setBounds(162, 335, 24, BS086.getPreferredSize().height);

          //---- BS087 ----
          BS087.setName("BS087");
          panel1.add(BS087);
          BS087.setBounds(254, 335, 24, BS087.getPreferredSize().height);

          //---- BS088 ----
          BS088.setName("BS088");
          panel1.add(BS088);
          BS088.setBounds(346, 335, 24, BS088.getPreferredSize().height);

          //---- BS089 ----
          BS089.setName("BS089");
          panel1.add(BS089);
          BS089.setBounds(438, 335, 24, BS089.getPreferredSize().height);

          //---- BS090 ----
          BS090.setName("BS090");
          panel1.add(BS090);
          BS090.setBounds(530, 335, 24, BS090.getPreferredSize().height);

          //---- BS091 ----
          BS091.setName("BS091");
          panel1.add(BS091);
          BS091.setBounds(622, 335, 24, BS091.getPreferredSize().height);

          //---- BS092 ----
          BS092.setName("BS092");
          panel1.add(BS092);
          BS092.setBounds(70, 360, 24, BS092.getPreferredSize().height);

          //---- BS093 ----
          BS093.setName("BS093");
          panel1.add(BS093);
          BS093.setBounds(162, 360, 24, BS093.getPreferredSize().height);

          //---- BS094 ----
          BS094.setName("BS094");
          panel1.add(BS094);
          BS094.setBounds(254, 360, 24, BS094.getPreferredSize().height);

          //---- BS095 ----
          BS095.setName("BS095");
          panel1.add(BS095);
          BS095.setBounds(346, 360, 24, BS095.getPreferredSize().height);

          //---- BS096 ----
          BS096.setName("BS096");
          panel1.add(BS096);
          BS096.setBounds(438, 360, 24, BS096.getPreferredSize().height);

          //---- BS097 ----
          BS097.setName("BS097");
          panel1.add(BS097);
          BS097.setBounds(530, 360, 24, BS097.getPreferredSize().height);

          //---- BS098 ----
          BS098.setName("BS098");
          panel1.add(BS098);
          BS098.setBounds(622, 360, 24, BS098.getPreferredSize().height);

          //---- BS099 ----
          BS099.setName("BS099");
          panel1.add(BS099);
          BS099.setBounds(70, 385, 24, BS099.getPreferredSize().height);

          //---- BS100 ----
          BS100.setName("BS100");
          panel1.add(BS100);
          BS100.setBounds(162, 385, 24, BS100.getPreferredSize().height);

          //---- BS101 ----
          BS101.setName("BS101");
          panel1.add(BS101);
          BS101.setBounds(254, 385, 24, BS101.getPreferredSize().height);

          //---- BS102 ----
          BS102.setName("BS102");
          panel1.add(BS102);
          BS102.setBounds(346, 385, 24, BS102.getPreferredSize().height);

          //---- BS103 ----
          BS103.setName("BS103");
          panel1.add(BS103);
          BS103.setBounds(438, 385, 24, BS103.getPreferredSize().height);

          //---- BS104 ----
          BS104.setName("BS104");
          panel1.add(BS104);
          BS104.setBounds(530, 385, 24, BS104.getPreferredSize().height);

          //---- BS105 ----
          BS105.setName("BS105");
          panel1.add(BS105);
          BS105.setBounds(622, 385, 24, BS105.getPreferredSize().height);

          //---- BS106 ----
          BS106.setName("BS106");
          panel1.add(BS106);
          BS106.setBounds(70, 410, 24, BS106.getPreferredSize().height);

          //---- BS107 ----
          BS107.setName("BS107");
          panel1.add(BS107);
          BS107.setBounds(162, 410, 24, BS107.getPreferredSize().height);

          //---- BS108 ----
          BS108.setName("BS108");
          panel1.add(BS108);
          BS108.setBounds(254, 410, 24, BS108.getPreferredSize().height);

          //---- BS109 ----
          BS109.setName("BS109");
          panel1.add(BS109);
          BS109.setBounds(346, 410, 24, BS109.getPreferredSize().height);

          //---- BS110 ----
          BS110.setName("BS110");
          panel1.add(BS110);
          BS110.setBounds(438, 410, 24, BS110.getPreferredSize().height);

          //---- BS111 ----
          BS111.setName("BS111");
          panel1.add(BS111);
          BS111.setBounds(530, 410, 24, BS111.getPreferredSize().height);

          //---- BS112 ----
          BS112.setName("BS112");
          panel1.add(BS112);
          BS112.setBounds(622, 410, 24, BS112.getPreferredSize().height);

          //---- BS113 ----
          BS113.setName("BS113");
          panel1.add(BS113);
          BS113.setBounds(70, 435, 24, BS113.getPreferredSize().height);

          //---- BS114 ----
          BS114.setName("BS114");
          panel1.add(BS114);
          BS114.setBounds(162, 435, 24, BS114.getPreferredSize().height);

          //---- BS115 ----
          BS115.setName("BS115");
          panel1.add(BS115);
          BS115.setBounds(254, 435, 24, BS115.getPreferredSize().height);

          //---- BS116 ----
          BS116.setName("BS116");
          panel1.add(BS116);
          BS116.setBounds(346, 435, 24, BS116.getPreferredSize().height);

          //---- BS117 ----
          BS117.setName("BS117");
          panel1.add(BS117);
          BS117.setBounds(438, 435, 24, BS117.getPreferredSize().height);

          //---- BS118 ----
          BS118.setName("BS118");
          panel1.add(BS118);
          BS118.setBounds(530, 435, 24, BS118.getPreferredSize().height);

          //---- BS119 ----
          BS119.setName("BS119");
          panel1.add(BS119);
          BS119.setBounds(622, 435, 24, BS119.getPreferredSize().height);

          //---- BS120 ----
          BS120.setName("BS120");
          panel1.add(BS120);
          BS120.setBounds(70, 460, 24, BS120.getPreferredSize().height);

          //---- BS121 ----
          BS121.setName("BS121");
          panel1.add(BS121);
          BS121.setBounds(162, 460, 24, BS121.getPreferredSize().height);

          //---- BS122 ----
          BS122.setName("BS122");
          panel1.add(BS122);
          BS122.setBounds(254, 460, 24, BS122.getPreferredSize().height);

          //---- BS123 ----
          BS123.setName("BS123");
          panel1.add(BS123);
          BS123.setBounds(346, 460, 24, BS123.getPreferredSize().height);

          //---- BS124 ----
          BS124.setName("BS124");
          panel1.add(BS124);
          BS124.setBounds(438, 460, 24, BS124.getPreferredSize().height);

          //---- BS125 ----
          BS125.setName("BS125");
          panel1.add(BS125);
          BS125.setBounds(530, 460, 24, BS125.getPreferredSize().height);

          //---- BS126 ----
          BS126.setName("BS126");
          panel1.add(BS126);
          BS126.setBounds(622, 460, 24, BS126.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 15, 705, 505);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_8 ----
      OBJ_8.setText("Choix possibles");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTD.add(OBJ_8);

      //---- OBJ_7 ----
      OBJ_7.setText("Aide en ligne");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      BTD.add(OBJ_7);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField BN001;
  private XRiTextField BN002;
  private XRiTextField BN003;
  private XRiTextField BN004;
  private XRiTextField BN005;
  private XRiTextField BN006;
  private XRiTextField BN007;
  private XRiTextField BN008;
  private XRiTextField BN009;
  private XRiTextField BN010;
  private XRiTextField BN011;
  private XRiTextField BN012;
  private XRiTextField BN013;
  private XRiTextField BN014;
  private XRiTextField BN015;
  private XRiTextField BN016;
  private XRiTextField BN017;
  private XRiTextField BN018;
  private XRiTextField BN019;
  private XRiTextField BN020;
  private XRiTextField BN021;
  private XRiTextField BN022;
  private XRiTextField BN023;
  private XRiTextField BN024;
  private XRiTextField BN025;
  private XRiTextField BN026;
  private XRiTextField BN027;
  private XRiTextField BN028;
  private XRiTextField BN029;
  private XRiTextField BN030;
  private XRiTextField BN031;
  private XRiTextField BN032;
  private XRiTextField BN033;
  private XRiTextField BN034;
  private XRiTextField BN035;
  private XRiTextField BN036;
  private XRiTextField BN037;
  private XRiTextField BN038;
  private XRiTextField BN039;
  private XRiTextField BN040;
  private XRiTextField BN041;
  private XRiTextField BN042;
  private XRiTextField BN043;
  private XRiTextField BN044;
  private XRiTextField BN045;
  private XRiTextField BN046;
  private XRiTextField BN047;
  private XRiTextField BN048;
  private XRiTextField BN049;
  private XRiTextField BN050;
  private XRiTextField BN051;
  private XRiTextField BN052;
  private XRiTextField BN053;
  private XRiTextField BN054;
  private XRiTextField BN055;
  private XRiTextField BN056;
  private XRiTextField BN057;
  private XRiTextField BN058;
  private XRiTextField BN059;
  private XRiTextField BN060;
  private XRiTextField BN061;
  private XRiTextField BN062;
  private XRiTextField BN063;
  private XRiTextField BN064;
  private XRiTextField BN065;
  private XRiTextField BN066;
  private XRiTextField BN067;
  private XRiTextField BN068;
  private XRiTextField BN069;
  private XRiTextField BN070;
  private XRiTextField BN071;
  private XRiTextField BN072;
  private XRiTextField BN073;
  private XRiTextField BN074;
  private XRiTextField BN075;
  private XRiTextField BN076;
  private XRiTextField BN077;
  private XRiTextField BN078;
  private XRiTextField BN079;
  private XRiTextField BN080;
  private XRiTextField BN081;
  private XRiTextField BN082;
  private XRiTextField BN083;
  private XRiTextField BN084;
  private XRiTextField BN085;
  private XRiTextField BN086;
  private XRiTextField BN087;
  private XRiTextField BN088;
  private XRiTextField BN089;
  private XRiTextField BN090;
  private XRiTextField BN091;
  private XRiTextField BN092;
  private XRiTextField BN093;
  private XRiTextField BN094;
  private XRiTextField BN095;
  private XRiTextField BN096;
  private XRiTextField BN097;
  private XRiTextField BN098;
  private XRiTextField BN099;
  private XRiTextField BN100;
  private XRiTextField BN101;
  private XRiTextField BN102;
  private XRiTextField BN103;
  private XRiTextField BN104;
  private XRiTextField BN105;
  private XRiTextField BN106;
  private XRiTextField BN107;
  private XRiTextField BN108;
  private XRiTextField BN109;
  private XRiTextField BN110;
  private XRiTextField BN111;
  private XRiTextField BN112;
  private XRiTextField BN113;
  private XRiTextField BN114;
  private XRiTextField BN115;
  private XRiTextField BN116;
  private XRiTextField BN117;
  private XRiTextField BN118;
  private XRiTextField BN119;
  private XRiTextField BN120;
  private XRiTextField BN121;
  private XRiTextField BN122;
  private XRiTextField BN123;
  private XRiTextField BN124;
  private XRiTextField BN125;
  private XRiTextField BN126;
  private XRiTextField BS001;
  private XRiTextField BS002;
  private XRiTextField BS003;
  private XRiTextField BS004;
  private XRiTextField BS005;
  private XRiTextField BS006;
  private XRiTextField BS007;
  private XRiTextField BS008;
  private XRiTextField BS009;
  private XRiTextField BS010;
  private XRiTextField BS011;
  private XRiTextField BS012;
  private XRiTextField BS013;
  private XRiTextField BS014;
  private XRiTextField BS015;
  private XRiTextField BS016;
  private XRiTextField BS017;
  private XRiTextField BS018;
  private XRiTextField BS019;
  private XRiTextField BS020;
  private XRiTextField BS021;
  private XRiTextField BS022;
  private XRiTextField BS023;
  private XRiTextField BS024;
  private XRiTextField BS025;
  private XRiTextField BS026;
  private XRiTextField BS027;
  private XRiTextField BS028;
  private XRiTextField BS029;
  private XRiTextField BS030;
  private XRiTextField BS031;
  private XRiTextField BS032;
  private XRiTextField BS033;
  private XRiTextField BS034;
  private XRiTextField BS035;
  private XRiTextField BS036;
  private XRiTextField BS037;
  private XRiTextField BS038;
  private XRiTextField BS039;
  private XRiTextField BS040;
  private XRiTextField BS041;
  private XRiTextField BS042;
  private XRiTextField BS043;
  private XRiTextField BS044;
  private XRiTextField BS045;
  private XRiTextField BS046;
  private XRiTextField BS047;
  private XRiTextField BS048;
  private XRiTextField BS049;
  private XRiTextField BS050;
  private XRiTextField BS051;
  private XRiTextField BS052;
  private XRiTextField BS053;
  private XRiTextField BS054;
  private XRiTextField BS055;
  private XRiTextField BS056;
  private XRiTextField BS057;
  private XRiTextField BS058;
  private XRiTextField BS059;
  private XRiTextField BS060;
  private XRiTextField BS061;
  private XRiTextField BS062;
  private XRiTextField BS063;
  private XRiTextField BS064;
  private XRiTextField BS065;
  private XRiTextField BS066;
  private XRiTextField BS067;
  private XRiTextField BS068;
  private XRiTextField BS069;
  private XRiTextField BS070;
  private XRiTextField BS071;
  private XRiTextField BS072;
  private XRiTextField BS073;
  private XRiTextField BS074;
  private XRiTextField BS075;
  private XRiTextField BS076;
  private XRiTextField BS077;
  private XRiTextField BS078;
  private XRiTextField BS079;
  private XRiTextField BS080;
  private XRiTextField BS081;
  private XRiTextField BS082;
  private XRiTextField BS083;
  private XRiTextField BS084;
  private XRiTextField BS085;
  private XRiTextField BS086;
  private XRiTextField BS087;
  private XRiTextField BS088;
  private XRiTextField BS089;
  private XRiTextField BS090;
  private XRiTextField BS091;
  private XRiTextField BS092;
  private XRiTextField BS093;
  private XRiTextField BS094;
  private XRiTextField BS095;
  private XRiTextField BS096;
  private XRiTextField BS097;
  private XRiTextField BS098;
  private XRiTextField BS099;
  private XRiTextField BS100;
  private XRiTextField BS101;
  private XRiTextField BS102;
  private XRiTextField BS103;
  private XRiTextField BS104;
  private XRiTextField BS105;
  private XRiTextField BS106;
  private XRiTextField BS107;
  private XRiTextField BS108;
  private XRiTextField BS109;
  private XRiTextField BS110;
  private XRiTextField BS111;
  private XRiTextField BS112;
  private XRiTextField BS113;
  private XRiTextField BS114;
  private XRiTextField BS115;
  private XRiTextField BS116;
  private XRiTextField BS117;
  private XRiTextField BS118;
  private XRiTextField BS119;
  private XRiTextField BS120;
  private XRiTextField BS121;
  private XRiTextField BS122;
  private XRiTextField BS123;
  private XRiTextField BS124;
  private XRiTextField BS125;
  private XRiTextField BS126;
  private JPopupMenu BTD;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
