
package ri.serien.libecranrpg.sgam.SGAM92FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM92FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGAM92FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ETT005.setValeursSelection("1", " ");
    ETT004.setValeursSelection("1", " ");
    ETT003.setValeursSelection("1", " ");
    ETT002.setValeursSelection("1", " ");
    ETT001.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SAUV@")).trim());
    ETS001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS001@")).trim());
    ETS002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS002@")).trim());
    ETS003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS003@")).trim());
    ETS004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS004@")).trim());
    ETS005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS005@")).trim());
    ETN001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    ETN002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN002@")).trim());
    ETN003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN003@")).trim());
    ETN004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN004@")).trim());
    ETN005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN005@")).trim());
    ETE001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE001@")).trim());
    ETE002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE002@")).trim());
    ETE003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE003@")).trim());
    ETE004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE004@")).trim());
    ETE005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE005@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    // **** Init des zones
    // ETT001.setSelected(lexique.HostFieldGetData("ETT001").equals("1"));
    // ETT002.setSelected(lexique.HostFieldGetData("ETT002").equals("1"));
    // ETT003.setSelected(lexique.HostFieldGetData("ETT003").equals("1"));
    // ETT004.setSelected(lexique.HostFieldGetData("ETT004").equals("1"));
    // ETT005.setSelected(lexique.HostFieldGetData("ETT005").equals("1"));
    
    ETN001.setText(lexique.HostFieldGetData("ETN001"));
    ETN002.setText(lexique.HostFieldGetData("ETN002"));
    ETN003.setText(lexique.HostFieldGetData("ETN003"));
    ETN004.setText(lexique.HostFieldGetData("ETN004"));
    ETN005.setText(lexique.HostFieldGetData("ETN005"));
    
    ETM001.setText(lexique.HostFieldGetData("ETM001"));
    ETM002.setText(lexique.HostFieldGetData("ETM002"));
    ETM003.setText(lexique.HostFieldGetData("ETM003"));
    ETM004.setText(lexique.HostFieldGetData("ETM004"));
    ETM005.setText(lexique.HostFieldGetData("ETM005"));
    
    ETE001.setText(lexique.HostFieldGetData("ETE001"));
    ETE002.setText(lexique.HostFieldGetData("ETE002"));
    ETE003.setText(lexique.HostFieldGetData("ETE003"));
    ETE004.setText(lexique.HostFieldGetData("ETE004"));
    ETE005.setText(lexique.HostFieldGetData("ETE005"));
    
    ETG001.setText(lexique.HostFieldGetData("ETG001"));
    ETG002.setText(lexique.HostFieldGetData("ETG002"));
    ETG003.setText(lexique.HostFieldGetData("ETG003"));
    ETG004.setText(lexique.HostFieldGetData("ETG004"));
    ETG005.setText(lexique.HostFieldGetData("ETG005"));
    
    // **** Visibilités
    ETM001.setVisible(!lexique.isTrue("61"));
    ETM002.setVisible(!lexique.isTrue("62"));
    ETM003.setVisible(!lexique.isTrue("63"));
    ETM004.setVisible(!lexique.isTrue("64"));
    ETM005.setVisible(!lexique.isTrue("65"));
    
    ETG001.setVisible(!lexique.isTrue("61"));
    ETG002.setVisible(!lexique.isTrue("62"));
    ETG003.setVisible(!lexique.isTrue("63"));
    ETG004.setVisible(!lexique.isTrue("64"));
    ETG005.setVisible(!lexique.isTrue("65"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTETBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTETB", 0, "**");
    
    lexique.HostScreenSendKey(this, "ENTER");
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void ETT001ActionPerformed(ActionEvent e) {
    // if(ETT001.isSelected())
    // lexique.HostFieldPutData("ETT001", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT001", 0, " ");
  }
  
  private void ETT002ActionPerformed(ActionEvent e) {
    // if(ETT002.isSelected())
    // lexique.HostFieldPutData("ETT002", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT002", 0, " ");
  }
  
  private void ETT003ActionPerformed(ActionEvent e) {
    // if(ETT003.isSelected())
    // lexique.HostFieldPutData("ETT003", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT003", 0, " ");
  }
  
  private void ETT004ActionPerformed(ActionEvent e) {
    // if(ETT004.isSelected())
    // lexique.HostFieldPutData("ETT004", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT004", 0, " ");
  }
  
  private void ETT005ActionPerformed(ActionEvent e) {
    // if(ETT005.isSelected())
    // lexique.HostFieldPutData("ETT005", 0, "1");
    // else
    // lexique.HostFieldPutData("ETT005", 0, " ");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_48 = new JLabel();
    WTETB = new SNBouton();
    xTitledSeparator1 = new JXTitledSeparator();
    ETT001 = new XRiCheckBox();
    ETT002 = new XRiCheckBox();
    ETT003 = new XRiCheckBox();
    ETT004 = new XRiCheckBox();
    ETT005 = new XRiCheckBox();
    ETS001 = new RiZoneSortie();
    ETS002 = new RiZoneSortie();
    ETS003 = new RiZoneSortie();
    ETS004 = new RiZoneSortie();
    ETS005 = new RiZoneSortie();
    ETN001 = new RiZoneSortie();
    ETN002 = new RiZoneSortie();
    ETN003 = new RiZoneSortie();
    ETN004 = new RiZoneSortie();
    ETN005 = new RiZoneSortie();
    ETM001 = new XRiTextField();
    ETM002 = new XRiTextField();
    ETM003 = new XRiTextField();
    ETM004 = new XRiTextField();
    ETM005 = new XRiTextField();
    ETE001 = new RiZoneSortie();
    ETE002 = new RiZoneSortie();
    ETE003 = new RiZoneSortie();
    ETE004 = new RiZoneSortie();
    ETE005 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    ETG001 = new XRiTextField();
    ETG002 = new XRiTextField();
    ETG003 = new XRiTextField();
    ETG004 = new XRiTextField();
    ETG005 = new XRiTextField();
    label5 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 370));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_48 ----
            OBJ_48.setText("@SAUV@");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(25, 20, 500, 20);

            //---- WTETB ----
            WTETB.setText("S\u00e9lection de tous les \u00e9tablissements");
            WTETB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTETB.setName("WTETB");
            WTETB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTETBActionPerformed(e);
              }
            });
            panel1.add(WTETB);
            WTETB.setBounds(new Rectangle(new Point(505, 285), WTETB.getPreferredSize()));

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("Soci\u00e9t\u00e9");
            xTitledSeparator1.setName("xTitledSeparator1");
            panel1.add(xTitledSeparator1);
            xTitledSeparator1.setBounds(15, 65, 640, xTitledSeparator1.getPreferredSize().height);

            //---- ETT001 ----
            ETT001.setName("ETT001");
            ETT001.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ETT001ActionPerformed(e);
              }
            });
            panel1.add(ETT001);
            ETT001.setBounds(new Rectangle(new Point(30, 128), ETT001.getPreferredSize()));

            //---- ETT002 ----
            ETT002.setName("ETT002");
            ETT002.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ETT002ActionPerformed(e);
              }
            });
            panel1.add(ETT002);
            ETT002.setBounds(new Rectangle(new Point(30, 158), ETT002.getPreferredSize()));

            //---- ETT003 ----
            ETT003.setName("ETT003");
            ETT003.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ETT003ActionPerformed(e);
              }
            });
            panel1.add(ETT003);
            ETT003.setBounds(new Rectangle(new Point(30, 190), ETT003.getPreferredSize()));

            //---- ETT004 ----
            ETT004.setName("ETT004");
            ETT004.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ETT004ActionPerformed(e);
              }
            });
            panel1.add(ETT004);
            ETT004.setBounds(new Rectangle(new Point(30, 218), ETT004.getPreferredSize()));

            //---- ETT005 ----
            ETT005.setName("ETT005");
            ETT005.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ETT005ActionPerformed(e);
              }
            });
            panel1.add(ETT005);
            ETT005.setBounds(new Rectangle(new Point(30, 248), ETT005.getPreferredSize()));

            //---- ETS001 ----
            ETS001.setText("@ETS001@");
            ETS001.setName("ETS001");
            panel1.add(ETS001);
            ETS001.setBounds(65, 125, 44, ETS001.getPreferredSize().height);

            //---- ETS002 ----
            ETS002.setText("@ETS002@");
            ETS002.setName("ETS002");
            panel1.add(ETS002);
            ETS002.setBounds(65, 155, 44, ETS002.getPreferredSize().height);

            //---- ETS003 ----
            ETS003.setText("@ETS003@");
            ETS003.setName("ETS003");
            panel1.add(ETS003);
            ETS003.setBounds(65, 185, 44, ETS003.getPreferredSize().height);

            //---- ETS004 ----
            ETS004.setText("@ETS004@");
            ETS004.setName("ETS004");
            panel1.add(ETS004);
            ETS004.setBounds(65, 215, 44, ETS004.getPreferredSize().height);

            //---- ETS005 ----
            ETS005.setText("@ETS005@");
            ETS005.setName("ETS005");
            panel1.add(ETS005);
            ETS005.setBounds(65, 245, 44, ETS005.getPreferredSize().height);

            //---- ETN001 ----
            ETN001.setText("@ETN001@");
            ETN001.setName("ETN001");
            panel1.add(ETN001);
            ETN001.setBounds(110, 125, 260, ETN001.getPreferredSize().height);

            //---- ETN002 ----
            ETN002.setText("@ETN002@");
            ETN002.setName("ETN002");
            panel1.add(ETN002);
            ETN002.setBounds(110, 155, 260, ETN002.getPreferredSize().height);

            //---- ETN003 ----
            ETN003.setText("@ETN003@");
            ETN003.setName("ETN003");
            panel1.add(ETN003);
            ETN003.setBounds(110, 185, 260, ETN003.getPreferredSize().height);

            //---- ETN004 ----
            ETN004.setText("@ETN004@");
            ETN004.setName("ETN004");
            panel1.add(ETN004);
            ETN004.setBounds(110, 215, 260, ETN004.getPreferredSize().height);

            //---- ETN005 ----
            ETN005.setText("@ETN005@");
            ETN005.setName("ETN005");
            panel1.add(ETN005);
            ETN005.setBounds(110, 245, 260, ETN005.getPreferredSize().height);

            //---- ETM001 ----
            ETM001.setName("ETM001");
            panel1.add(ETM001);
            ETM001.setBounds(370, 123, 64, ETM001.getPreferredSize().height);

            //---- ETM002 ----
            ETM002.setName("ETM002");
            panel1.add(ETM002);
            ETM002.setBounds(370, 153, 64, ETM002.getPreferredSize().height);

            //---- ETM003 ----
            ETM003.setName("ETM003");
            panel1.add(ETM003);
            ETM003.setBounds(370, 183, 64, ETM003.getPreferredSize().height);

            //---- ETM004 ----
            ETM004.setName("ETM004");
            panel1.add(ETM004);
            ETM004.setBounds(370, 213, 64, ETM004.getPreferredSize().height);

            //---- ETM005 ----
            ETM005.setName("ETM005");
            panel1.add(ETM005);
            ETM005.setBounds(370, 243, 64, ETM005.getPreferredSize().height);

            //---- ETE001 ----
            ETE001.setText("@ETE001@");
            ETE001.setName("ETE001");
            panel1.add(ETE001);
            ETE001.setBounds(435, 125, 165, ETE001.getPreferredSize().height);

            //---- ETE002 ----
            ETE002.setText("@ETE002@");
            ETE002.setName("ETE002");
            panel1.add(ETE002);
            ETE002.setBounds(435, 155, 164, ETE002.getPreferredSize().height);

            //---- ETE003 ----
            ETE003.setText("@ETE003@");
            ETE003.setName("ETE003");
            panel1.add(ETE003);
            ETE003.setBounds(435, 185, 164, ETE003.getPreferredSize().height);

            //---- ETE004 ----
            ETE004.setText("@ETE004@");
            ETE004.setName("ETE004");
            panel1.add(ETE004);
            ETE004.setBounds(435, 215, 164, ETE004.getPreferredSize().height);

            //---- ETE005 ----
            ETE005.setText("@ETE005@");
            ETE005.setName("ETE005");
            panel1.add(ETE005);
            ETE005.setBounds(435, 245, 164, ETE005.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Etb");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(75, 95, 30, 20);

            //---- label2 ----
            label2.setText("Identification");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(200, 95, 75, 20);

            //---- label3 ----
            label3.setText("Purge");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(385, 95, 35, 20);

            //---- label4 ----
            label4.setText("En cours");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(485, 95, 58, 20);

            //---- ETG001 ----
            ETG001.setName("ETG001");
            panel1.add(ETG001);
            ETG001.setBounds(610, 123, 34, ETG001.getPreferredSize().height);

            //---- ETG002 ----
            ETG002.setName("ETG002");
            panel1.add(ETG002);
            ETG002.setBounds(610, 153, 34, ETG002.getPreferredSize().height);

            //---- ETG003 ----
            ETG003.setName("ETG003");
            panel1.add(ETG003);
            ETG003.setBounds(610, 183, 34, ETG003.getPreferredSize().height);

            //---- ETG004 ----
            ETG004.setName("ETG004");
            panel1.add(ETG004);
            ETG004.setBounds(610, 213, 34, ETG004.getPreferredSize().height);

            //---- ETG005 ----
            ETG005.setName("ETG005");
            panel1.add(ETG005);
            ETG005.setBounds(610, 243, 34, ETG005.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Mg");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(615, 95, 23, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_48;
  private SNBouton WTETB;
  private JXTitledSeparator xTitledSeparator1;
  private XRiCheckBox ETT001;
  private XRiCheckBox ETT002;
  private XRiCheckBox ETT003;
  private XRiCheckBox ETT004;
  private XRiCheckBox ETT005;
  private RiZoneSortie ETS001;
  private RiZoneSortie ETS002;
  private RiZoneSortie ETS003;
  private RiZoneSortie ETS004;
  private RiZoneSortie ETS005;
  private RiZoneSortie ETN001;
  private RiZoneSortie ETN002;
  private RiZoneSortie ETN003;
  private RiZoneSortie ETN004;
  private RiZoneSortie ETN005;
  private XRiTextField ETM001;
  private XRiTextField ETM002;
  private XRiTextField ETM003;
  private XRiTextField ETM004;
  private XRiTextField ETM005;
  private RiZoneSortie ETE001;
  private RiZoneSortie ETE002;
  private RiZoneSortie ETE003;
  private RiZoneSortie ETE004;
  private RiZoneSortie ETE005;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField ETG001;
  private XRiTextField ETG002;
  private XRiTextField ETG003;
  private XRiTextField ETG004;
  private XRiTextField ETG005;
  private JLabel label5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
