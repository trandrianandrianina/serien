
package ri.serien.libecranrpg.sgam.SGAM40FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM511] Gestion des achats -> Opérations périodiques -> Comptabilisation des achats -> Comptabilisation régulière
 * Indicateur : 00100001 (93)
 * Titre : Comptabilisation régulière des achats
 * 
 * [GAM513] Gestion des achats -> Opérations périodiques -> Comptabilisation des achats -> Comptabilisation régulière simulée
 * Indicateur : 00000001
 * Titre : Comptabilisation régulière simulée des achats
 */
public class SGAM40FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message messageforçage = null;
  private Message LICOMP = null;
  
  public SGAM40FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    FORCAG.setValeursSelection("*FOR", "    ");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLICOMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LICOMP@")).trim());
    J101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J101@")).trim());
    J111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J111@")).trim());
    J121.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J121@")).trim());
    J122.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J122@")).trim());
    J112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J112@")).trim());
    J102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J102@")).trim());
    J103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J103@")).trim());
    J113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J113@")).trim());
    J123.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J123@")).trim());
    J124.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J124@")).trim());
    J114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J114@")).trim());
    J104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J104@")).trim());
    J105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J105@")).trim());
    J106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J106@")).trim());
    J107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J107@")).trim());
    J108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J108@")).trim());
    J109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J109@")).trim());
    J110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J110@")).trim());
    J115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J115@")).trim());
    J116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J116@")).trim());
    J117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J117@")).trim());
    J118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J118@")).trim());
    J119.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J119@")).trim());
    J120.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@120@")).trim());
    J125.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J125@")).trim());
    J126.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J126@")).trim());
    J127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J127@")).trim());
    J128.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J128@")).trim());
    J129.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J129@")).trim());
    J130.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J130@")).trim());
    J131.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@J131@")).trim());
    lbPerio.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO1@")).trim());
    EPER1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER1@")).trim());
    EPER2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER2@")).trim());
    EPER9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER9@")).trim());
    EPER10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER10@")).trim());
    EPER3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER3@")).trim());
    EPER11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER11@")).trim());
    EPER4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER4@")).trim());
    EPER12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER12@")).trim());
    EPER5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER5@")).trim());
    EPER13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER13@")).trim());
    EPER6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER6@")).trim());
    EPER14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER14@")).trim());
    EPER7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER7@")).trim());
    EPER15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER15@")).trim());
    EPER8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER8@")).trim());
    EPER16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPER16@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Indicateur
    Boolean is31 = lexique.isTrue("31");
    Boolean is93 = lexique.isTrue("93");
    
    // Titre
    if (is93) {
      bpPresentation.setText("Comptabilisation régulière des achats");
    }
    else {
      bpPresentation.setText("Comptabilisation régulière simulée des achats");
    }
    
    // Visibilité des composants
    if (is31) {
      snMagasin.setVisible(true);
      lbMagasinEnCours.setVisible(true);
    }
    else {
      snMagasin.setVisible(false);
      lbMagasinEnCours.setVisible(false);
    }
    
    EPER1.setVisible(!lexique.HostFieldGetData("EPER1").trim().isEmpty());
    EPER2.setVisible(!lexique.HostFieldGetData("EPER2").trim().isEmpty());
    EPER3.setVisible(!lexique.HostFieldGetData("EPER3").trim().isEmpty());
    EPER4.setVisible(!lexique.HostFieldGetData("EPER4").trim().isEmpty());
    EPER5.setVisible(!lexique.HostFieldGetData("EPER5").trim().isEmpty());
    EPER6.setVisible(!lexique.HostFieldGetData("EPER6").trim().isEmpty());
    EPER7.setVisible(!lexique.HostFieldGetData("EPER7").trim().isEmpty());
    EPER8.setVisible(!lexique.HostFieldGetData("EPER8").trim().isEmpty());
    EPER9.setVisible(!lexique.HostFieldGetData("EPER9").trim().isEmpty());
    EPER10.setVisible(!lexique.HostFieldGetData("EPER10").trim().isEmpty());
    EPER11.setVisible(!lexique.HostFieldGetData("EPER11").trim().isEmpty());
    EPER12.setVisible(!lexique.HostFieldGetData("EPER12").trim().isEmpty());
    EPER13.setVisible(!lexique.HostFieldGetData("EPER13").trim().isEmpty());
    EPER14.setVisible(!lexique.HostFieldGetData("EPER14").trim().isEmpty());
    EPER15.setVisible(!lexique.HostFieldGetData("EPER15").trim().isEmpty());
    EPER16.setVisible(!lexique.HostFieldGetData("EPER16").trim().isEmpty());
    pnlPeriodeEnregistrer.setVisible(EPER1.isVisible() || EPER2.isVisible() || EPER3.isVisible() || EPER4.isVisible() || EPER5.isVisible()
        || EPER6.isVisible() || EPER7.isVisible() || EPER8.isVisible() || EPER9.isVisible() || EPER10.isVisible() || EPER11.isVisible()
        || EPER12.isVisible() || EPER13.isVisible() || EPER14.isVisible() || EPER15.isVisible() || EPER16.isVisible());
    
    messageforçage = messageforçage.getMessageImportant("forçage");
    lbForçage1.setVisible(!lexique.HostFieldGetData("EFOR1").trim().isEmpty());
    lbForçage1.setMessage(messageforçage);
    lbForçage2.setVisible(!lexique.HostFieldGetData("EFOR2").trim().isEmpty());
    lbForçage2.setMessage(messageforçage);
    lbForçage3.setVisible(!lexique.HostFieldGetData("EFOR3").trim().isEmpty());
    lbForçage3.setMessage(messageforçage);
    lbForçage4.setVisible(!lexique.HostFieldGetData("EFOR4").trim().isEmpty());
    lbForçage4.setMessage(messageforçage);
    lbForçage5.setVisible(!lexique.HostFieldGetData("EFOR5").trim().isEmpty());
    lbForçage5.setMessage(messageforçage);
    lbForçage6.setVisible(!lexique.HostFieldGetData("EFOR6").trim().isEmpty());
    lbForçage6.setMessage(messageforçage);
    lbForçage7.setVisible(!lexique.HostFieldGetData("EFOR7").trim().isEmpty());
    lbForçage7.setMessage(messageforçage);
    lbForçage8.setVisible(!lexique.HostFieldGetData("EFOR8").trim().isEmpty());
    lbForçage8.setMessage(messageforçage);
    lbForçage9.setVisible(!lexique.HostFieldGetData("EFOR9").trim().isEmpty());
    lbForçage9.setMessage(messageforçage);
    lbForçage10.setVisible(!lexique.HostFieldGetData("EFOR10").trim().isEmpty());
    lbForçage10.setMessage(messageforçage);
    lbForçage11.setVisible(!lexique.HostFieldGetData("EFOR11").trim().isEmpty());
    lbForçage11.setMessage(messageforçage);
    lbForçage12.setVisible(!lexique.HostFieldGetData("EFOR12").trim().isEmpty());
    lbForçage12.setMessage(messageforçage);
    lbForçage13.setVisible(!lexique.HostFieldGetData("EFOR13").trim().isEmpty());
    lbForçage13.setMessage(messageforçage);
    lbForçage14.setVisible(!lexique.HostFieldGetData("EFOR14").trim().isEmpty());
    lbForçage14.setMessage(messageforçage);
    lbForçage15.setVisible(!lexique.HostFieldGetData("EFOR15").trim().isEmpty());
    lbForçage15.setMessage(messageforçage);
    lbForçage16.setVisible(!lexique.HostFieldGetData("EFOR16").trim().isEmpty());
    lbForçage16.setMessage(messageforçage);
    
    // Gestion de LICOMP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LICOMP = LICOMP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLICOMP.setMessage(LICOMP);
    
    // Titre
    pnlComptabilisation.setTitre(lexique.HostFieldGetData("LIPER1"));
    
    // Initialise l'etablissement
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    chargerMagasin();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        if (FORCAG.isSelected() || pnlPeriodeEnregistrer.isVisible() == false) {
          lexique.HostScreenSendKey(this, "ENTER");
        }
        else {
          lexique.HostScreenSendKey(this, "F6");
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Renseigne le magasin suivant l'etablissement
   */
  private void chargerMagasin() {
    // Renseigner le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
      chargerMagasin();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLICOMP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlComptabilisation = new SNPanelTitre();
    J101 = new SNTexte();
    J111 = new SNTexte();
    J121 = new SNTexte();
    J122 = new SNTexte();
    J112 = new SNTexte();
    J102 = new SNTexte();
    J103 = new SNTexte();
    J113 = new SNTexte();
    J123 = new SNTexte();
    J124 = new SNTexte();
    J114 = new SNTexte();
    J104 = new SNTexte();
    J105 = new SNTexte();
    J106 = new SNTexte();
    J107 = new SNTexte();
    J108 = new SNTexte();
    J109 = new SNTexte();
    J110 = new SNTexte();
    J115 = new SNTexte();
    J116 = new SNTexte();
    J117 = new SNTexte();
    J118 = new SNTexte();
    J119 = new SNTexte();
    J120 = new SNTexte();
    J125 = new SNTexte();
    J126 = new SNTexte();
    J127 = new SNTexte();
    J128 = new SNTexte();
    J129 = new SNTexte();
    J130 = new SNTexte();
    J131 = new SNTexte();
    pnlCritereSlection = new SNPanelTitre();
    lbMagasinEnCours = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbPeriodeAComptabiliser = new SNLabelChamp();
    UPER1 = new XRiTextField();
    lbAu = new SNLabelChamp();
    UPER2 = new XRiTextField();
    lbPerio = new SNLabelChamp();
    FORCAG = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    snEtablissement = new SNEtablissement();
    lbEtablissementEnCours = new SNLabelChamp();
    pnlPeriodeEnregistrer = new SNPanelTitre();
    EPER1 = new XRiTextField();
    EPER2 = new XRiTextField();
    EPER9 = new XRiTextField();
    lbForçage1 = new SNLabelUnite();
    lbForçage2 = new SNLabelUnite();
    lbForçage9 = new SNLabelUnite();
    EPER10 = new XRiTextField();
    lbForçage10 = new SNLabelUnite();
    EPER3 = new XRiTextField();
    lbForçage3 = new SNLabelUnite();
    EPER11 = new XRiTextField();
    lbForçage11 = new SNLabelUnite();
    EPER4 = new XRiTextField();
    lbForçage4 = new SNLabelUnite();
    EPER12 = new XRiTextField();
    lbForçage12 = new SNLabelUnite();
    EPER5 = new XRiTextField();
    lbForçage5 = new SNLabelUnite();
    EPER13 = new XRiTextField();
    lbForçage13 = new SNLabelUnite();
    EPER6 = new XRiTextField();
    lbForçage6 = new SNLabelUnite();
    EPER14 = new XRiTextField();
    lbForçage14 = new SNLabelUnite();
    EPER7 = new XRiTextField();
    lbForçage7 = new SNLabelUnite();
    EPER15 = new XRiTextField();
    lbForçage15 = new SNLabelUnite();
    EPER8 = new XRiTextField();
    lbForçage8 = new SNLabelUnite();
    EPER16 = new XRiTextField();
    lbForçage16 = new SNLabelUnite();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Comptabilisation r\u00e9guli\u00e8re des achats");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setMinimumSize(new Dimension(250, 30));
        pnlMessage.setPreferredSize(new Dimension(250, 30));
        pnlMessage.setMaximumSize(new Dimension(250, 30));
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLICOMP ----
        lbLICOMP.setText("@LICOMP@");
        lbLICOMP.setPreferredSize(new Dimension(82, 30));
        lbLICOMP.setMinimumSize(new Dimension(82, 30));
        lbLICOMP.setName("lbLICOMP");
        pnlMessage.add(lbLICOMP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlComptabilisation ========
          {
            pnlComptabilisation.setTitre("[Titre dans le code]");
            pnlComptabilisation.setName("pnlComptabilisation");
            pnlComptabilisation.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlComptabilisation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlComptabilisation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlComptabilisation.getLayout()).columnWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlComptabilisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- J101 ----
            J101.setText("@J101@");
            J101.setPreferredSize(new Dimension(36, 30));
            J101.setMinimumSize(new Dimension(36, 30));
            J101.setName("J101");
            pnlComptabilisation.add(J101, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J111 ----
            J111.setText("@J111@");
            J111.setPreferredSize(new Dimension(36, 30));
            J111.setMinimumSize(new Dimension(36, 30));
            J111.setName("J111");
            pnlComptabilisation.add(J111, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J121 ----
            J121.setText("@J121@");
            J121.setPreferredSize(new Dimension(36, 30));
            J121.setMinimumSize(new Dimension(36, 30));
            J121.setName("J121");
            pnlComptabilisation.add(J121, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J122 ----
            J122.setText("@J122@");
            J122.setPreferredSize(new Dimension(36, 30));
            J122.setMinimumSize(new Dimension(36, 30));
            J122.setName("J122");
            pnlComptabilisation.add(J122, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J112 ----
            J112.setText("@J112@");
            J112.setPreferredSize(new Dimension(36, 30));
            J112.setMinimumSize(new Dimension(36, 30));
            J112.setName("J112");
            pnlComptabilisation.add(J112, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J102 ----
            J102.setText("@J102@");
            J102.setPreferredSize(new Dimension(36, 30));
            J102.setMinimumSize(new Dimension(36, 30));
            J102.setName("J102");
            pnlComptabilisation.add(J102, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J103 ----
            J103.setText("@J103@");
            J103.setPreferredSize(new Dimension(36, 30));
            J103.setMinimumSize(new Dimension(36, 30));
            J103.setName("J103");
            pnlComptabilisation.add(J103, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J113 ----
            J113.setText("@J113@");
            J113.setPreferredSize(new Dimension(36, 30));
            J113.setMinimumSize(new Dimension(36, 30));
            J113.setName("J113");
            pnlComptabilisation.add(J113, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J123 ----
            J123.setText("@J123@");
            J123.setPreferredSize(new Dimension(36, 30));
            J123.setMinimumSize(new Dimension(36, 30));
            J123.setName("J123");
            pnlComptabilisation.add(J123, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J124 ----
            J124.setText("@J124@");
            J124.setPreferredSize(new Dimension(36, 30));
            J124.setMinimumSize(new Dimension(36, 30));
            J124.setName("J124");
            pnlComptabilisation.add(J124, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J114 ----
            J114.setText("@J114@");
            J114.setPreferredSize(new Dimension(36, 30));
            J114.setMinimumSize(new Dimension(36, 30));
            J114.setName("J114");
            pnlComptabilisation.add(J114, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J104 ----
            J104.setText("@J104@");
            J104.setPreferredSize(new Dimension(36, 30));
            J104.setMinimumSize(new Dimension(36, 30));
            J104.setName("J104");
            pnlComptabilisation.add(J104, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J105 ----
            J105.setText("@J105@");
            J105.setPreferredSize(new Dimension(36, 30));
            J105.setMinimumSize(new Dimension(36, 30));
            J105.setName("J105");
            pnlComptabilisation.add(J105, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J106 ----
            J106.setText("@J106@");
            J106.setPreferredSize(new Dimension(36, 30));
            J106.setMinimumSize(new Dimension(36, 30));
            J106.setName("J106");
            pnlComptabilisation.add(J106, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J107 ----
            J107.setText("@J107@");
            J107.setPreferredSize(new Dimension(36, 30));
            J107.setMinimumSize(new Dimension(36, 30));
            J107.setName("J107");
            pnlComptabilisation.add(J107, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J108 ----
            J108.setText("@J108@");
            J108.setPreferredSize(new Dimension(36, 30));
            J108.setMinimumSize(new Dimension(36, 30));
            J108.setName("J108");
            pnlComptabilisation.add(J108, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J109 ----
            J109.setText("@J109@");
            J109.setPreferredSize(new Dimension(36, 30));
            J109.setMinimumSize(new Dimension(36, 30));
            J109.setName("J109");
            pnlComptabilisation.add(J109, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J110 ----
            J110.setText("@J110@");
            J110.setPreferredSize(new Dimension(36, 30));
            J110.setMinimumSize(new Dimension(36, 30));
            J110.setName("J110");
            pnlComptabilisation.add(J110, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J115 ----
            J115.setText("@J115@");
            J115.setPreferredSize(new Dimension(36, 30));
            J115.setMinimumSize(new Dimension(36, 30));
            J115.setName("J115");
            pnlComptabilisation.add(J115, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J116 ----
            J116.setText("@J116@");
            J116.setPreferredSize(new Dimension(36, 30));
            J116.setMinimumSize(new Dimension(36, 30));
            J116.setName("J116");
            pnlComptabilisation.add(J116, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J117 ----
            J117.setText("@J117@");
            J117.setPreferredSize(new Dimension(36, 30));
            J117.setMinimumSize(new Dimension(36, 30));
            J117.setName("J117");
            pnlComptabilisation.add(J117, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J118 ----
            J118.setText("@J118@");
            J118.setPreferredSize(new Dimension(36, 30));
            J118.setMinimumSize(new Dimension(36, 30));
            J118.setName("J118");
            pnlComptabilisation.add(J118, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J119 ----
            J119.setText("@J119@");
            J119.setPreferredSize(new Dimension(36, 30));
            J119.setMinimumSize(new Dimension(36, 30));
            J119.setName("J119");
            pnlComptabilisation.add(J119, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J120 ----
            J120.setText("@120@");
            J120.setPreferredSize(new Dimension(36, 30));
            J120.setMinimumSize(new Dimension(36, 30));
            J120.setName("J120");
            pnlComptabilisation.add(J120, new GridBagConstraints(9, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- J125 ----
            J125.setText("@J125@");
            J125.setPreferredSize(new Dimension(36, 30));
            J125.setMinimumSize(new Dimension(36, 30));
            J125.setName("J125");
            pnlComptabilisation.add(J125, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J126 ----
            J126.setText("@J126@");
            J126.setPreferredSize(new Dimension(36, 30));
            J126.setMinimumSize(new Dimension(36, 30));
            J126.setName("J126");
            pnlComptabilisation.add(J126, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J127 ----
            J127.setText("@J127@");
            J127.setPreferredSize(new Dimension(36, 30));
            J127.setMinimumSize(new Dimension(36, 30));
            J127.setName("J127");
            pnlComptabilisation.add(J127, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J128 ----
            J128.setText("@J128@");
            J128.setPreferredSize(new Dimension(36, 30));
            J128.setMinimumSize(new Dimension(36, 30));
            J128.setName("J128");
            pnlComptabilisation.add(J128, new GridBagConstraints(7, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J129 ----
            J129.setText("@J129@");
            J129.setPreferredSize(new Dimension(36, 30));
            J129.setMinimumSize(new Dimension(36, 30));
            J129.setName("J129");
            pnlComptabilisation.add(J129, new GridBagConstraints(8, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J130 ----
            J130.setText("@J130@");
            J130.setPreferredSize(new Dimension(36, 30));
            J130.setMinimumSize(new Dimension(36, 30));
            J130.setName("J130");
            pnlComptabilisation.add(J130, new GridBagConstraints(9, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- J131 ----
            J131.setText("@J131@");
            J131.setPreferredSize(new Dimension(36, 30));
            J131.setMinimumSize(new Dimension(36, 30));
            J131.setName("J131");
            pnlComptabilisation.add(J131, new GridBagConstraints(10, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlComptabilisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlCritereSlection ========
          {
            pnlCritereSlection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereSlection.setName("pnlCritereSlection");
            pnlCritereSlection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSlection.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSlection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSlection.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSlection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasinEnCours ----
            lbMagasinEnCours.setText("Magasin en cours");
            lbMagasinEnCours.setName("lbMagasinEnCours");
            pnlCritereSlection.add(lbMagasinEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setName("snMagasin");
            pnlCritereSlection.add(snMagasin, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeAComptabiliser ----
            lbPeriodeAComptabiliser.setText("P\u00e9riode \u00e0 comptabiliser du");
            lbPeriodeAComptabiliser.setPreferredSize(new Dimension(169, 30));
            lbPeriodeAComptabiliser.setMinimumSize(new Dimension(169, 30));
            lbPeriodeAComptabiliser.setMaximumSize(new Dimension(169, 30));
            lbPeriodeAComptabiliser.setName("lbPeriodeAComptabiliser");
            pnlCritereSlection.add(lbPeriodeAComptabiliser, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- UPER1 ----
            UPER1.setToolTipText("Jour");
            UPER1.setMinimumSize(new Dimension(30, 30));
            UPER1.setPreferredSize(new Dimension(30, 30));
            UPER1.setFont(new Font("sansserif", Font.PLAIN, 14));
            UPER1.setName("UPER1");
            pnlCritereSlection.add(UPER1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbAu ----
            lbAu.setText("au");
            lbAu.setMinimumSize(new Dimension(16, 30));
            lbAu.setPreferredSize(new Dimension(16, 30));
            lbAu.setMaximumSize(new Dimension(16, 30));
            lbAu.setName("lbAu");
            pnlCritereSlection.add(lbAu, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- UPER2 ----
            UPER2.setToolTipText("Jour");
            UPER2.setPreferredSize(new Dimension(30, 30));
            UPER2.setMinimumSize(new Dimension(30, 30));
            UPER2.setFont(new Font("sansserif", Font.PLAIN, 14));
            UPER2.setName("UPER2");
            pnlCritereSlection.add(UPER2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPerio ----
            lbPerio.setText("@PERIO1@");
            lbPerio.setMinimumSize(new Dimension(125, 30));
            lbPerio.setPreferredSize(new Dimension(125, 30));
            lbPerio.setMaximumSize(new Dimension(125, 30));
            lbPerio.setName("lbPerio");
            pnlCritereSlection.add(lbPerio, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- FORCAG ----
            FORCAG.setText("Demande de for\u00e7age de la comptabilisation");
            FORCAG.setToolTipText("Demande for\u00e7age comptabilisation");
            FORCAG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            FORCAG.setFont(new Font("sansserif", Font.PLAIN, 14));
            FORCAG.setMinimumSize(new Dimension(87, 30));
            FORCAG.setPreferredSize(new Dimension(87, 30));
            FORCAG.setMaximumSize(new Dimension(87, 30));
            FORCAG.setName("FORCAG");
            pnlCritereSlection.add(FORCAG, new GridBagConstraints(0, 2, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSlection, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlPeriodeEnregistrer ========
          {
            pnlPeriodeEnregistrer.setTitre("P\u00e9riodes d\u00e9j\u00e0 enregistr\u00e9es");
            pnlPeriodeEnregistrer.setName("pnlPeriodeEnregistrer");
            pnlPeriodeEnregistrer.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPeriodeEnregistrer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlPeriodeEnregistrer.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlPeriodeEnregistrer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPeriodeEnregistrer.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- EPER1 ----
            EPER1.setText("@EPER1@");
            EPER1.setMinimumSize(new Dimension(140, 30));
            EPER1.setPreferredSize(new Dimension(140, 30));
            EPER1.setEnabled(false);
            EPER1.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER1.setName("EPER1");
            pnlPeriodeEnregistrer.add(EPER1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EPER2 ----
            EPER2.setText("@EPER2@");
            EPER2.setMinimumSize(new Dimension(140, 30));
            EPER2.setPreferredSize(new Dimension(140, 30));
            EPER2.setEnabled(false);
            EPER2.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER2.setName("EPER2");
            pnlPeriodeEnregistrer.add(EPER2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EPER9 ----
            EPER9.setText("@EPER9@");
            EPER9.setMinimumSize(new Dimension(140, 30));
            EPER9.setPreferredSize(new Dimension(140, 30));
            EPER9.setEnabled(false);
            EPER9.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER9.setName("EPER9");
            pnlPeriodeEnregistrer.add(EPER9, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage1 ----
            lbForçage1.setText("For\u00e7age");
            lbForçage1.setPreferredSize(new Dimension(60, 30));
            lbForçage1.setMinimumSize(new Dimension(60, 30));
            lbForçage1.setName("lbFor\u00e7age1");
            pnlPeriodeEnregistrer.add(lbForçage1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage2 ----
            lbForçage2.setText("For\u00e7age");
            lbForçage2.setPreferredSize(new Dimension(60, 30));
            lbForçage2.setMinimumSize(new Dimension(60, 30));
            lbForçage2.setName("lbFor\u00e7age2");
            pnlPeriodeEnregistrer.add(lbForçage2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage9 ----
            lbForçage9.setText("For\u00e7age");
            lbForçage9.setPreferredSize(new Dimension(60, 30));
            lbForçage9.setMinimumSize(new Dimension(60, 30));
            lbForçage9.setName("lbFor\u00e7age9");
            pnlPeriodeEnregistrer.add(lbForçage9, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPER10 ----
            EPER10.setText("@EPER10@");
            EPER10.setMinimumSize(new Dimension(140, 30));
            EPER10.setPreferredSize(new Dimension(140, 30));
            EPER10.setEnabled(false);
            EPER10.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER10.setName("EPER10");
            pnlPeriodeEnregistrer.add(EPER10, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage10 ----
            lbForçage10.setText("For\u00e7age");
            lbForçage10.setPreferredSize(new Dimension(60, 30));
            lbForçage10.setMinimumSize(new Dimension(60, 30));
            lbForçage10.setName("lbFor\u00e7age10");
            pnlPeriodeEnregistrer.add(lbForçage10, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPER3 ----
            EPER3.setText("@EPER3@");
            EPER3.setMinimumSize(new Dimension(140, 30));
            EPER3.setPreferredSize(new Dimension(140, 30));
            EPER3.setEnabled(false);
            EPER3.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER3.setName("EPER3");
            pnlPeriodeEnregistrer.add(EPER3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage3 ----
            lbForçage3.setText("For\u00e7age");
            lbForçage3.setPreferredSize(new Dimension(60, 30));
            lbForçage3.setMinimumSize(new Dimension(60, 30));
            lbForçage3.setName("lbFor\u00e7age3");
            pnlPeriodeEnregistrer.add(lbForçage3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EPER11 ----
            EPER11.setText("@EPER11@");
            EPER11.setMinimumSize(new Dimension(140, 30));
            EPER11.setPreferredSize(new Dimension(140, 30));
            EPER11.setEnabled(false);
            EPER11.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER11.setName("EPER11");
            pnlPeriodeEnregistrer.add(EPER11, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage11 ----
            lbForçage11.setText("For\u00e7age");
            lbForçage11.setPreferredSize(new Dimension(60, 30));
            lbForçage11.setMinimumSize(new Dimension(60, 30));
            lbForçage11.setName("lbFor\u00e7age11");
            pnlPeriodeEnregistrer.add(lbForçage11, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPER4 ----
            EPER4.setText("@EPER4@");
            EPER4.setMinimumSize(new Dimension(140, 30));
            EPER4.setPreferredSize(new Dimension(140, 30));
            EPER4.setEnabled(false);
            EPER4.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER4.setName("EPER4");
            pnlPeriodeEnregistrer.add(EPER4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage4 ----
            lbForçage4.setText("For\u00e7age");
            lbForçage4.setPreferredSize(new Dimension(60, 30));
            lbForçage4.setMinimumSize(new Dimension(60, 30));
            lbForçage4.setName("lbFor\u00e7age4");
            pnlPeriodeEnregistrer.add(lbForçage4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EPER12 ----
            EPER12.setText("@EPER12@");
            EPER12.setMinimumSize(new Dimension(140, 30));
            EPER12.setPreferredSize(new Dimension(140, 30));
            EPER12.setEnabled(false);
            EPER12.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER12.setName("EPER12");
            pnlPeriodeEnregistrer.add(EPER12, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage12 ----
            lbForçage12.setText("For\u00e7age");
            lbForçage12.setPreferredSize(new Dimension(60, 30));
            lbForçage12.setMinimumSize(new Dimension(60, 30));
            lbForçage12.setName("lbFor\u00e7age12");
            pnlPeriodeEnregistrer.add(lbForçage12, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPER5 ----
            EPER5.setText("@EPER5@");
            EPER5.setMinimumSize(new Dimension(140, 30));
            EPER5.setPreferredSize(new Dimension(140, 30));
            EPER5.setEnabled(false);
            EPER5.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER5.setName("EPER5");
            pnlPeriodeEnregistrer.add(EPER5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage5 ----
            lbForçage5.setText("For\u00e7age");
            lbForçage5.setPreferredSize(new Dimension(60, 30));
            lbForçage5.setMinimumSize(new Dimension(60, 30));
            lbForçage5.setName("lbFor\u00e7age5");
            pnlPeriodeEnregistrer.add(lbForçage5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EPER13 ----
            EPER13.setText("@EPER13@");
            EPER13.setMinimumSize(new Dimension(140, 30));
            EPER13.setPreferredSize(new Dimension(140, 30));
            EPER13.setEnabled(false);
            EPER13.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER13.setName("EPER13");
            pnlPeriodeEnregistrer.add(EPER13, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage13 ----
            lbForçage13.setText("For\u00e7age");
            lbForçage13.setPreferredSize(new Dimension(60, 30));
            lbForçage13.setMinimumSize(new Dimension(60, 30));
            lbForçage13.setName("lbFor\u00e7age13");
            pnlPeriodeEnregistrer.add(lbForçage13, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPER6 ----
            EPER6.setText("@EPER6@");
            EPER6.setMinimumSize(new Dimension(140, 30));
            EPER6.setPreferredSize(new Dimension(140, 30));
            EPER6.setEnabled(false);
            EPER6.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER6.setName("EPER6");
            pnlPeriodeEnregistrer.add(EPER6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage6 ----
            lbForçage6.setText("For\u00e7age");
            lbForçage6.setPreferredSize(new Dimension(60, 30));
            lbForçage6.setMinimumSize(new Dimension(60, 30));
            lbForçage6.setName("lbFor\u00e7age6");
            pnlPeriodeEnregistrer.add(lbForçage6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EPER14 ----
            EPER14.setText("@EPER14@");
            EPER14.setMinimumSize(new Dimension(140, 30));
            EPER14.setPreferredSize(new Dimension(140, 30));
            EPER14.setEnabled(false);
            EPER14.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER14.setName("EPER14");
            pnlPeriodeEnregistrer.add(EPER14, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage14 ----
            lbForçage14.setText("For\u00e7age");
            lbForçage14.setPreferredSize(new Dimension(60, 30));
            lbForçage14.setMinimumSize(new Dimension(60, 30));
            lbForçage14.setName("lbFor\u00e7age14");
            pnlPeriodeEnregistrer.add(lbForçage14, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPER7 ----
            EPER7.setText("@EPER7@");
            EPER7.setMinimumSize(new Dimension(140, 30));
            EPER7.setPreferredSize(new Dimension(140, 30));
            EPER7.setEnabled(false);
            EPER7.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER7.setName("EPER7");
            pnlPeriodeEnregistrer.add(EPER7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage7 ----
            lbForçage7.setText("For\u00e7age");
            lbForçage7.setPreferredSize(new Dimension(60, 30));
            lbForçage7.setMinimumSize(new Dimension(60, 30));
            lbForçage7.setName("lbFor\u00e7age7");
            pnlPeriodeEnregistrer.add(lbForçage7, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- EPER15 ----
            EPER15.setText("@EPER15@");
            EPER15.setMinimumSize(new Dimension(140, 30));
            EPER15.setPreferredSize(new Dimension(140, 30));
            EPER15.setEnabled(false);
            EPER15.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER15.setName("EPER15");
            pnlPeriodeEnregistrer.add(EPER15, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbForçage15 ----
            lbForçage15.setText("For\u00e7age");
            lbForçage15.setPreferredSize(new Dimension(60, 30));
            lbForçage15.setMinimumSize(new Dimension(60, 30));
            lbForçage15.setName("lbFor\u00e7age15");
            pnlPeriodeEnregistrer.add(lbForçage15, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- EPER8 ----
            EPER8.setText("@EPER8@");
            EPER8.setMinimumSize(new Dimension(140, 30));
            EPER8.setPreferredSize(new Dimension(140, 30));
            EPER8.setEnabled(false);
            EPER8.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER8.setName("EPER8");
            pnlPeriodeEnregistrer.add(EPER8, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbForçage8 ----
            lbForçage8.setText("For\u00e7age");
            lbForçage8.setPreferredSize(new Dimension(60, 30));
            lbForçage8.setMinimumSize(new Dimension(60, 30));
            lbForçage8.setName("lbFor\u00e7age8");
            pnlPeriodeEnregistrer.add(lbForçage8, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- EPER16 ----
            EPER16.setText("@EPER16@");
            EPER16.setMinimumSize(new Dimension(140, 30));
            EPER16.setPreferredSize(new Dimension(140, 30));
            EPER16.setEnabled(false);
            EPER16.setFont(new Font("sansserif", Font.PLAIN, 14));
            EPER16.setName("EPER16");
            pnlPeriodeEnregistrer.add(EPER16, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbForçage16 ----
            lbForçage16.setText("For\u00e7age");
            lbForçage16.setPreferredSize(new Dimension(60, 30));
            lbForçage16.setMinimumSize(new Dimension(60, 30));
            lbForçage16.setName("lbFor\u00e7age16");
            pnlPeriodeEnregistrer.add(lbForçage16, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlPeriodeEnregistrer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setEnabled(false);
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLICOMP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlComptabilisation;
  private SNTexte J101;
  private SNTexte J111;
  private SNTexte J121;
  private SNTexte J122;
  private SNTexte J112;
  private SNTexte J102;
  private SNTexte J103;
  private SNTexte J113;
  private SNTexte J123;
  private SNTexte J124;
  private SNTexte J114;
  private SNTexte J104;
  private SNTexte J105;
  private SNTexte J106;
  private SNTexte J107;
  private SNTexte J108;
  private SNTexte J109;
  private SNTexte J110;
  private SNTexte J115;
  private SNTexte J116;
  private SNTexte J117;
  private SNTexte J118;
  private SNTexte J119;
  private SNTexte J120;
  private SNTexte J125;
  private SNTexte J126;
  private SNTexte J127;
  private SNTexte J128;
  private SNTexte J129;
  private SNTexte J130;
  private SNTexte J131;
  private SNPanelTitre pnlCritereSlection;
  private SNLabelChamp lbMagasinEnCours;
  private SNMagasin snMagasin;
  private SNLabelChamp lbPeriodeAComptabiliser;
  private XRiTextField UPER1;
  private SNLabelChamp lbAu;
  private XRiTextField UPER2;
  private SNLabelChamp lbPerio;
  private XRiCheckBox FORCAG;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNPanelTitre pnlPeriodeEnregistrer;
  private XRiTextField EPER1;
  private XRiTextField EPER2;
  private XRiTextField EPER9;
  private SNLabelUnite lbForçage1;
  private SNLabelUnite lbForçage2;
  private SNLabelUnite lbForçage9;
  private XRiTextField EPER10;
  private SNLabelUnite lbForçage10;
  private XRiTextField EPER3;
  private SNLabelUnite lbForçage3;
  private XRiTextField EPER11;
  private SNLabelUnite lbForçage11;
  private XRiTextField EPER4;
  private SNLabelUnite lbForçage4;
  private XRiTextField EPER12;
  private SNLabelUnite lbForçage12;
  private XRiTextField EPER5;
  private SNLabelUnite lbForçage5;
  private XRiTextField EPER13;
  private SNLabelUnite lbForçage13;
  private XRiTextField EPER6;
  private SNLabelUnite lbForçage6;
  private XRiTextField EPER14;
  private SNLabelUnite lbForçage14;
  private XRiTextField EPER7;
  private SNLabelUnite lbForçage7;
  private XRiTextField EPER15;
  private SNLabelUnite lbForçage15;
  private XRiTextField EPER8;
  private SNLabelUnite lbForçage8;
  private XRiTextField EPER16;
  private SNLabelUnite lbForçage16;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
