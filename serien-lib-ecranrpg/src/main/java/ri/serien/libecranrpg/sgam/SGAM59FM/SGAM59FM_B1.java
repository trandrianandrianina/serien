
package ri.serien.libecranrpg.sgam.SGAM59FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM59FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGAM59FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU3.setValeursSelection("**", "  ");
    WTOU2.setValeursSelection("**", "  ");
    WTSFA.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
    WRPL.setValeursSelection("1", " ");
    WREA1.setValeursSelection("X", " ");
    WPQT.setValeursSelection("1", " ");
    WTRS.setValeursSelection("1", " ");
    WTAF.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    
    
    panel6.setVisible(lexique.isTrue("N94"));
    panel7.setVisible(lexique.isTrue("94"));
    
    WPAM.setVisible(!lexique.HostFieldGetData("TITPG2").trim().equalsIgnoreCase("Nomenclatures"));
    WPAM.setEnabled(lexique.isPresent("WPAM"));
    SFAFIN.setVisible(!lexique.HostFieldGetData("WTSFA").trim().equalsIgnoreCase("**"));
    SFADEB.setVisible(lexique.isPresent("SFADEB"));
    FRSDEB.setVisible(lexique.isPresent("FRSDEB"));
    WRPL.setVisible(!lexique.HostFieldGetData("TITPG2").trim().equalsIgnoreCase("Nomenclatures"));
    OBJ_97.setVisible(!lexique.HostFieldGetData("TITPG2").trim().equalsIgnoreCase("Nomenclatures"));
    WREA1.setVisible(!lexique.HostFieldGetData("TITPG2").trim().equalsIgnoreCase("Nomenclatures"));
    // ARTFIN.setVisible(!lexique.HostFieldGetData("WTOU3").trim().equalsIgnoreCase("**"));
    ARTDEB.setVisible(lexique.isPresent("ARTDEB"));
    WTRS.setVisible(!lexique.HostFieldGetData("TITPG2").trim().equalsIgnoreCase("Nomenclatures"));
    WTAF.setVisible(!lexique.HostFieldGetData("TITPG2").trim().equalsIgnoreCase("Nomenclatures"));
    P_SEL4.setVisible(!lexique.HostFieldGetData("WTOU3").trim().equalsIgnoreCase("**"));
    P_SEL3.setVisible(!lexique.HostFieldGetData("WTOU2").trim().equalsIgnoreCase("**"));
    P_SEL2.setVisible(!lexique.HostFieldGetData("WTSFA").trim().equalsIgnoreCase("**"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOU1").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("MA01").trim().equalsIgnoreCase("**"));
    
    MA01_tout.setSelected(lexique.HostFieldGetData("MA01").trim().equalsIgnoreCase("**"));
    
    OBJ_74.setVisible(lexique.isTrue("N94"));
    panel4.setVisible(lexique.isTrue("N94"));
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
    if (MA01_tout.isSelected()) {
      lexique.HostFieldPutData("MA01", 0, "**");
    }
    else {
      if (MA01.getText().equalsIgnoreCase("**")) {
        MA01.setText("");
        lexique.HostFieldPutData("MA01", 0, "");
      }
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WETB");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void MA01ActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTSFAActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
  }
  
  private void WTOU3ActionPerformed(ActionEvent e) {
    P_SEL4.setVisible(!P_SEL4.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_58 = new JXTitledSeparator();
    OBJ_74 = new JXTitledSeparator();
    OBJ_66 = new JXTitledSeparator();
    OBJ_42 = new JXTitledSeparator();
    OBJ_82 = new JXTitledSeparator();
    panel1 = new JPanel();
    MA01_tout = new JCheckBox();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    panel3 = new JPanel();
    WTOU1 = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_63 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    OBJ_64 = new JLabel();
    panel2 = new JPanel();
    WTSFA = new XRiCheckBox();
    P_SEL2 = new JPanel();
    OBJ_72 = new JLabel();
    SFADEB = new XRiTextField();
    SFAFIN = new XRiTextField();
    OBJ_73 = new JLabel();
    panel4 = new JPanel();
    WTOU2 = new XRiCheckBox();
    P_SEL3 = new JPanel();
    FRSDEB = new XRiTextField();
    FRSFIN = new XRiTextField();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    panel5 = new JPanel();
    P_SEL4 = new JPanel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_95 = new JLabel();
    OBJ_96 = new JLabel();
    WTOU3 = new XRiCheckBox();
    OBJ_37 = new JXTitledSeparator();
    panel6 = new JPanel();
    WTAF = new XRiCheckBox();
    WTRS = new XRiCheckBox();
    WPQT = new XRiCheckBox();
    WREA1 = new XRiCheckBox();
    OBJ_97 = new JLabel();
    OBJ_98 = new JLabel();
    WRPL = new XRiCheckBox();
    WPAM = new XRiTextField();
    WCGP = new XRiTextField();
    panel7 = new JPanel();
    OBJ_100 = new JLabel();
    WCOSAI = new XRiTextField();
    OBJ_101 = new JLabel();
    WDATIM = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_99 = new JXTitledSeparator();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(830, 630));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(830, 630));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_58 ----
          OBJ_58.setTitle("Codes groupe-famille");
          OBJ_58.setName("OBJ_58");

          //---- OBJ_74 ----
          OBJ_74.setTitle("Codes fournisseurs");
          OBJ_74.setName("OBJ_74");

          //---- OBJ_66 ----
          OBJ_66.setTitle("Codes sous-famille");
          OBJ_66.setName("OBJ_66");

          //---- OBJ_42 ----
          OBJ_42.setTitle("Codes magasins");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_82 ----
          OBJ_82.setTitle("Codes articles");
          OBJ_82.setName("OBJ_82");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- MA01_tout ----
            MA01_tout.setText("S\u00e9lection compl\u00e8te");
            MA01_tout.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MA01_tout.setName("MA01_tout");
            MA01_tout.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                MA01ActionPerformed(e);
              }
            });
            panel1.add(MA01_tout);
            MA01_tout.setBounds(5, 16, 150, MA01_tout.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(12, 6, 34, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(48, 6, 34, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(84, 6, 34, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(121, 6, 34, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(157, 6, 34, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(193, 6, 34, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(229, 6, 34, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(266, 6, 34, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(302, 6, 34, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(338, 6, 34, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(375, 6, 34, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(411, 6, 34, MA12.getPreferredSize().height);
            }
            panel1.add(P_SEL0);
            P_SEL0.setBounds(160, 5, 458, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WTOU1 ----
            WTOU1.setText("S\u00e9lection compl\u00e8te");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });
            panel3.add(WTOU1);
            WTOU1.setBounds(5, 14, 150, WTOU1.getPreferredSize().height);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_63 ----
              OBJ_63.setText("D\u00e9but");
              OBJ_63.setName("OBJ_63");
              P_SEL1.add(OBJ_63);
              OBJ_63.setBounds(12, 10, 48, 18);

              //---- FAMDEB ----
              FAMDEB.setComponentPopupMenu(BTD);
              FAMDEB.setName("FAMDEB");
              P_SEL1.add(FAMDEB);
              FAMDEB.setBounds(58, 5, 40, FAMDEB.getPreferredSize().height);

              //---- FAMFIN ----
              FAMFIN.setComponentPopupMenu(BTD);
              FAMFIN.setName("FAMFIN");
              P_SEL1.add(FAMFIN);
              FAMFIN.setBounds(276, 5, 40, FAMFIN.getPreferredSize().height);

              //---- OBJ_64 ----
              OBJ_64.setText("Fin");
              OBJ_64.setName("OBJ_64");
              P_SEL1.add(OBJ_64);
              OBJ_64.setBounds(247, 10, 28, 18);
            }
            panel3.add(P_SEL1);
            P_SEL1.setBounds(160, 5, 458, 37);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WTSFA ----
            WTSFA.setText("S\u00e9lection compl\u00e8te");
            WTSFA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTSFA.setName("WTSFA");
            WTSFA.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTSFAActionPerformed(e);
              }
            });
            panel2.add(WTSFA);
            WTSFA.setBounds(5, 14, 150, WTSFA.getPreferredSize().height);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- OBJ_72 ----
              OBJ_72.setText("D\u00e9but");
              OBJ_72.setName("OBJ_72");
              P_SEL2.add(OBJ_72);
              OBJ_72.setBounds(12, 10, 48, 18);

              //---- SFADEB ----
              SFADEB.setComponentPopupMenu(BTD);
              SFADEB.setName("SFADEB");
              P_SEL2.add(SFADEB);
              SFADEB.setBounds(58, 5, 80, SFADEB.getPreferredSize().height);

              //---- SFAFIN ----
              SFAFIN.setComponentPopupMenu(BTD);
              SFAFIN.setName("SFAFIN");
              P_SEL2.add(SFAFIN);
              SFAFIN.setBounds(276, 5, 80, SFAFIN.getPreferredSize().height);

              //---- OBJ_73 ----
              OBJ_73.setText("Fin");
              OBJ_73.setName("OBJ_73");
              P_SEL2.add(OBJ_73);
              OBJ_73.setBounds(247, 10, 33, 18);
            }
            panel2.add(P_SEL2);
            P_SEL2.setBounds(160, 5, 458, 37);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- WTOU2 ----
            WTOU2.setText("S\u00e9lection compl\u00e8te");
            WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU2.setName("WTOU2");
            WTOU2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU2ActionPerformed(e);
              }
            });
            panel4.add(WTOU2);
            WTOU2.setBounds(5, 19, 150, WTOU2.getPreferredSize().height);

            //======== P_SEL3 ========
            {
              P_SEL3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL3.setOpaque(false);
              P_SEL3.setName("P_SEL3");
              P_SEL3.setLayout(null);

              //---- FRSDEB ----
              FRSDEB.setComponentPopupMenu(BTD);
              FRSDEB.setName("FRSDEB");
              P_SEL3.add(FRSDEB);
              FRSDEB.setBounds(58, 5, 80, FRSDEB.getPreferredSize().height);

              //---- FRSFIN ----
              FRSFIN.setComponentPopupMenu(BTD);
              FRSFIN.setName("FRSFIN");
              P_SEL3.add(FRSFIN);
              FRSFIN.setBounds(276, 5, 80, FRSFIN.getPreferredSize().height);

              //---- OBJ_79 ----
              OBJ_79.setText("D\u00e9but");
              OBJ_79.setName("OBJ_79");
              P_SEL3.add(OBJ_79);
              OBJ_79.setBounds(12, 10, 48, 18);

              //---- OBJ_80 ----
              OBJ_80.setText("Fin");
              OBJ_80.setName("OBJ_80");
              P_SEL3.add(OBJ_80);
              OBJ_80.setBounds(247, 10, 33, 18);
            }
            panel4.add(P_SEL3);
            P_SEL3.setBounds(160, 10, 458, 37);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setPreferredSize(new Dimension(650, 42));
            panel5.setMinimumSize(new Dimension(650, 42));
            panel5.setName("panel5");
            panel5.setLayout(null);

            //======== P_SEL4 ========
            {
              P_SEL4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL4.setOpaque(false);
              P_SEL4.setName("P_SEL4");
              P_SEL4.setLayout(null);

              //---- ARTDEB ----
              ARTDEB.setComponentPopupMenu(BTD);
              ARTDEB.setName("ARTDEB");
              P_SEL4.add(ARTDEB);
              ARTDEB.setBounds(58, 5, 210, ARTDEB.getPreferredSize().height);

              //---- ARTFIN ----
              ARTFIN.setComponentPopupMenu(BTD);
              ARTFIN.setName("ARTFIN");
              P_SEL4.add(ARTFIN);
              ARTFIN.setBounds(330, 5, 210, ARTFIN.getPreferredSize().height);

              //---- OBJ_95 ----
              OBJ_95.setText("D\u00e9but");
              OBJ_95.setName("OBJ_95");
              P_SEL4.add(OBJ_95);
              OBJ_95.setBounds(12, 10, 48, 18);

              //---- OBJ_96 ----
              OBJ_96.setText("Fin");
              OBJ_96.setName("OBJ_96");
              P_SEL4.add(OBJ_96);
              OBJ_96.setBounds(295, 10, 35, 18);
            }
            panel5.add(P_SEL4);
            P_SEL4.setBounds(160, 5, 560, 37);

            //---- WTOU3 ----
            WTOU3.setText("S\u00e9lection compl\u00e8te");
            WTOU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU3.setName("WTOU3");
            WTOU3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU3ActionPerformed(e);
              }
            });
            panel5.add(WTOU3);
            WTOU3.setBounds(5, 14, 150, WTOU3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_37 ----
          OBJ_37.setTitle("");
          OBJ_37.setName("OBJ_37");

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- WTAF ----
            WTAF.setText("Tous les articles du  ou des fournisseur(s)");
            WTAF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTAF.setName("WTAF");
            panel6.add(WTAF);
            WTAF.setBounds(330, 17, 271, 15);

            //---- WTRS ----
            WTRS.setText("Proposition transfert si 'sur-stock'");
            WTRS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTRS.setName("WTRS");
            panel6.add(WTRS);
            WTRS.setBounds(25, 68, 266, 20);

            //---- WPQT ----
            WPQT.setText("Proposition quantit\u00e9s");
            WPQT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPQT.setName("WPQT");
            panel6.add(WPQT);
            WPQT.setBounds(25, 41, 150, 20);

            //---- WREA1 ----
            WREA1.setText("Saisie param\u00e8tres de calcul");
            WREA1.setToolTipText("Pour type de r\u00e9appro 1");
            WREA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WREA1.setName("WREA1");
            panel6.add(WREA1);
            WREA1.setBounds(330, 71, 192, 15);

            //---- OBJ_97 ----
            OBJ_97.setText("% d'augment. du stock mini");
            OBJ_97.setName("OBJ_97");
            panel6.add(OBJ_97);
            OBJ_97.setBounds(330, 42, 170, 18);

            //---- OBJ_98 ----
            OBJ_98.setText("Type produit ('GP')");
            OBJ_98.setName("OBJ_98");
            panel6.add(OBJ_98);
            OBJ_98.setBounds(25, 14, 176, 20);

            //---- WRPL ----
            WRPL.setText("Replanification si 'sur-stock'");
            WRPL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WRPL.setName("WRPL");
            panel6.add(WRPL);
            WRPL.setBounds(25, 95, 266, 15);

            //---- WPAM ----
            WPAM.setComponentPopupMenu(null);
            WPAM.setName("WPAM");
            panel6.add(WPAM);
            WPAM.setBounds(515, 37, 34, WPAM.getPreferredSize().height);

            //---- WCGP ----
            WCGP.setComponentPopupMenu(BTD);
            WCGP.setName("WCGP");
            panel6.add(WCGP);
            WCGP.setBounds(240, 10, 20, WCGP.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- OBJ_100 ----
            OBJ_100.setText("Coefficient de saisonnalit\u00e9 (%)");
            OBJ_100.setName("OBJ_100");
            panel7.add(OBJ_100);
            OBJ_100.setBounds(25, 25, 170, 18);

            //---- WCOSAI ----
            WCOSAI.setComponentPopupMenu(null);
            WCOSAI.setName("WCOSAI");
            panel7.add(WCOSAI);
            WCOSAI.setBounds(240, 20, 34, WCOSAI.getPreferredSize().height);

            //---- OBJ_101 ----
            OBJ_101.setText("Date de livraison impos\u00e9e");
            OBJ_101.setName("OBJ_101");
            panel7.add(OBJ_101);
            OBJ_101.setBounds(25, 60, 170, 18);

            //---- WDATIM ----
            WDATIM.setName("WDATIM");
            panel7.add(WDATIM);
            WDATIM.setBounds(240, 55, 105, WDATIM.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 762, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_99 ----
    OBJ_99.setTitle("");
    OBJ_99.setName("OBJ_99");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_58;
  private JXTitledSeparator OBJ_74;
  private JXTitledSeparator OBJ_66;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_82;
  private JPanel panel1;
  private JCheckBox MA01_tout;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JPanel panel3;
  private XRiCheckBox WTOU1;
  private JPanel P_SEL1;
  private JLabel OBJ_63;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JLabel OBJ_64;
  private JPanel panel2;
  private XRiCheckBox WTSFA;
  private JPanel P_SEL2;
  private JLabel OBJ_72;
  private XRiTextField SFADEB;
  private XRiTextField SFAFIN;
  private JLabel OBJ_73;
  private JPanel panel4;
  private XRiCheckBox WTOU2;
  private JPanel P_SEL3;
  private XRiTextField FRSDEB;
  private XRiTextField FRSFIN;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private JPanel panel5;
  private JPanel P_SEL4;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_95;
  private JLabel OBJ_96;
  private XRiCheckBox WTOU3;
  private JXTitledSeparator OBJ_37;
  private JPanel panel6;
  private XRiCheckBox WTAF;
  private XRiCheckBox WTRS;
  private XRiCheckBox WPQT;
  private XRiCheckBox WREA1;
  private JLabel OBJ_97;
  private JLabel OBJ_98;
  private XRiCheckBox WRPL;
  private XRiTextField WPAM;
  private XRiTextField WCGP;
  private JPanel panel7;
  private JLabel OBJ_100;
  private XRiTextField WCOSAI;
  private JLabel OBJ_101;
  private XRiCalendrier WDATIM;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JXTitledSeparator OBJ_99;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
