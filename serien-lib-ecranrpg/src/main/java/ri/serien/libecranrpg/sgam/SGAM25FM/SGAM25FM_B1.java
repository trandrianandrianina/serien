
package ri.serien.libecranrpg.sgam.SGAM25FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM25FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGAM25FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    WFOR.setValeursSelection("OUI", "NON");
    WRED.setValeursSelection("OUI", "NON");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfDateEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Ajout d'élements dans la combobox
    cbChoixTri.removeAllItems();
    cbChoixTri.addItem("Par magasin");
    cbChoixTri.addItem("Par fournisseur");
    cbChoixTri.addItem("Par magasin et fournisseur");
    cbChoixTri.addItem("Par fournisseur et magasin");
    
    // Selectionne une valeur à la combobox
    selectionValeurCombobox();
    
    // Visinilité
    WNEX.setVisible(lexique.isPresent("WNEX"));
    lbNombreExemplaire.setVisible(lexique.isPresent("WNEX"));
    pnlOptions.setVisible(lexique.isPresent("WRED"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation des composants
    initialisserComposantMagasin();
    initialiserComposantFournisseur();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snFournisseur.renseignerChampRPG(lexique, "WFRS");
    recupererValeurCombobox();
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Permet de charger la liste des magasins lors du changement d'etablissement
   */
  private void initialisserComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Permet de récuperer quelle est l'option séléctionner dans le combobox
   */
  private void recupererValeurCombobox() {
    
    switch (cbChoixTri.getSelectedIndex()) {
      case 0:
        lexique.HostFieldPutData("WTI1", 0, "1");
        lexique.HostFieldPutData("WTI2", 0, "");
        break;
      case 1:
        lexique.HostFieldPutData("WTI1", 0, "");
        lexique.HostFieldPutData("WTI2", 0, "1");
        break;
      case 2:
        lexique.HostFieldPutData("WTI1", 0, "1");
        lexique.HostFieldPutData("WTI2", 0, "2");
        break;
      case 3:
        lexique.HostFieldPutData("WTI1", 0, "2");
        lexique.HostFieldPutData("WTI2", 0, "1");
        break;
      default:
        lexique.HostFieldPutData("WTI1", 0, "");
        lexique.HostFieldPutData("WTI2", 0, "");
        break;
    }
  }
  
  /**
   * Permet d'initialiser la valeur de la combobox suivant l'état du buffer
   */
  private void selectionValeurCombobox() {
    String valeurWTI1 = lexique.HostFieldGetData("WTI1");
    String valeurWTI2 = lexique.HostFieldGetData("WTI2");
    
    if (valeurWTI1.equalsIgnoreCase("1") && valeurWTI2.equalsIgnoreCase("")
        || valeurWTI1.equalsIgnoreCase("") && valeurWTI2.equalsIgnoreCase("")) {
      cbChoixTri.setSelectedItem("Par magasin");
    }
    else {
      if (valeurWTI1.equalsIgnoreCase("") && valeurWTI2.equalsIgnoreCase("1")) {
        cbChoixTri.setSelectedItem("Par fournisseur");
      }
      else {
        if (valeurWTI1.equalsIgnoreCase("1") && valeurWTI2.equalsIgnoreCase("2")) {
          cbChoixTri.setSelectedItem("Par magasin et fournisseur");
        }
        else {
          if (valeurWTI1.equalsIgnoreCase("2") && valeurWTI2.equalsIgnoreCase("1")) {
            cbChoixTri.setSelectedItem("Par fournisseur et magasin");
          }
        }
      }
    }
  }
  
  /**
   * Initialise le composants fournisseur
   */
  private void initialiserComposantFournisseur() {
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(true);
    snFournisseur.setSelectionParChampRPG(lexique, "WFRS");
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    lexique.WatchHelp(pmBTD.getInvoker().getName());
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      initialisserComposantMagasin();
      initialiserComposantFournisseur();
      cbChoixTri.setSelectedItem(cbChoixTri.getSelectedIndex());
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbDocumentAchat = new SNLabelChamp();
    pnlPlageDocumentsAchat = new SNPanel();
    WNDEB = new XRiTextField();
    WNDEBS = new XRiTextField();
    lbA = new SNLabelChamp();
    WNFIN = new XRiTextField();
    WNFINS = new XRiTextField();
    lbParMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbParFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriode = new SNLabelChamp();
    tfDateEnCours = new SNTexte();
    pnlOptions = new SNPanelTitre();
    lbTriEdition = new SNLabelChamp();
    cbChoixTri = new SNComboBox();
    WRED = new XRiCheckBox();
    lbNombreExemplaire = new SNLabelChamp();
    WNEX = new XRiTextField();
    WFOR = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    miAideEnLigne = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(710, 630));
    setPreferredSize(new Dimension(710, 630));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      pnlNord.add(bpPresentation);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlCritereDeSelection ========
        {
          pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereDeSelection.setName("pnlCritereDeSelection");
          pnlCritereDeSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCritereDeSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbDocumentAchat ----
          lbDocumentAchat.setText("Plage de documents d'achat de");
          lbDocumentAchat.setPreferredSize(new Dimension(200, 30));
          lbDocumentAchat.setMinimumSize(new Dimension(175, 30));
          lbDocumentAchat.setName("lbDocumentAchat");
          pnlCritereDeSelection.add(lbDocumentAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlPlageDocumentsAchat ========
          {
            pnlPlageDocumentsAchat.setName("pnlPlageDocumentsAchat");
            pnlPlageDocumentsAchat.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlPlageDocumentsAchat.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlPlageDocumentsAchat.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlPlageDocumentsAchat.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlPlageDocumentsAchat.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- WNDEB ----
            WNDEB.setComponentPopupMenu(null);
            WNDEB.setMinimumSize(new Dimension(70, 30));
            WNDEB.setPreferredSize(new Dimension(70, 30));
            WNDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNDEB.setName("WNDEB");
            pnlPlageDocumentsAchat.add(WNDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- WNDEBS ----
            WNDEBS.setComponentPopupMenu(null);
            WNDEBS.setMinimumSize(new Dimension(24, 30));
            WNDEBS.setPreferredSize(new Dimension(24, 30));
            WNDEBS.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNDEBS.setName("WNDEBS");
            pnlPlageDocumentsAchat.add(WNDEBS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbA ----
            lbA.setText("\u00e0");
            lbA.setPreferredSize(new Dimension(25, 30));
            lbA.setMinimumSize(new Dimension(25, 30));
            lbA.setMaximumSize(new Dimension(115, 30));
            lbA.setName("lbA");
            pnlPlageDocumentsAchat.add(lbA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- WNFIN ----
            WNFIN.setComponentPopupMenu(null);
            WNFIN.setPreferredSize(new Dimension(70, 30));
            WNFIN.setMinimumSize(new Dimension(70, 30));
            WNFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNFIN.setName("WNFIN");
            pnlPlageDocumentsAchat.add(WNFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- WNFINS ----
            WNFINS.setComponentPopupMenu(null);
            WNFINS.setPreferredSize(new Dimension(24, 30));
            WNFINS.setMinimumSize(new Dimension(24, 30));
            WNFINS.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNFINS.setName("WNFINS");
            pnlPlageDocumentsAchat.add(WNFINS, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCritereDeSelection.add(pnlPlageDocumentsAchat, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbParMagasin ----
          lbParMagasin.setText("Magasin");
          lbParMagasin.setPreferredSize(new Dimension(200, 30));
          lbParMagasin.setMinimumSize(new Dimension(175, 30));
          lbParMagasin.setName("lbParMagasin");
          pnlCritereDeSelection.add(lbParMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snMagasin ----
          snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snMagasin.setName("snMagasin");
          pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbParFournisseur ----
          lbParFournisseur.setText("Fournisseur");
          lbParFournisseur.setPreferredSize(new Dimension(200, 30));
          lbParFournisseur.setMinimumSize(new Dimension(175, 30));
          lbParFournisseur.setName("lbParFournisseur");
          pnlCritereDeSelection.add(lbParFournisseur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snFournisseur ----
          snFournisseur.setPreferredSize(new Dimension(320, 30));
          snFournisseur.setMinimumSize(new Dimension(320, 30));
          snFournisseur.setMaximumSize(new Dimension(320, 30));
          snFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
          snFournisseur.setName("snFournisseur");
          pnlCritereDeSelection.add(snFournisseur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- tfDateEnCours ----
          tfDateEnCours.setText("@WENCX@");
          tfDateEnCours.setPreferredSize(new Dimension(260, 30));
          tfDateEnCours.setMinimumSize(new Dimension(260, 30));
          tfDateEnCours.setEditable(false);
          tfDateEnCours.setEnabled(false);
          tfDateEnCours.setName("tfDateEnCours");
          pnlEtablissement.add(tfDateEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOptions ========
        {
          pnlOptions.setTitre("Options d'\u00e9dition");
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbTriEdition ----
          lbTriEdition.setText("Tri de l'\u00e9dition");
          lbTriEdition.setPreferredSize(new Dimension(90, 30));
          lbTriEdition.setMinimumSize(new Dimension(90, 30));
          lbTriEdition.setMaximumSize(new Dimension(90, 30));
          lbTriEdition.setName("lbTriEdition");
          pnlOptions.add(lbTriEdition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- cbChoixTri ----
          cbChoixTri.setPreferredSize(new Dimension(210, 30));
          cbChoixTri.setMinimumSize(new Dimension(210, 30));
          cbChoixTri.setMaximumSize(new Dimension(210, 30));
          cbChoixTri.setName("cbChoixTri");
          pnlOptions.add(cbChoixTri, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- WRED ----
          WRED.setText("R\u00e9\u00e9dition");
          WRED.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WRED.setComponentPopupMenu(null);
          WRED.setFont(new Font("sansserif", Font.PLAIN, 14));
          WRED.setPreferredSize(new Dimension(90, 30));
          WRED.setMinimumSize(new Dimension(90, 30));
          WRED.setMaximumSize(new Dimension(90, 30));
          WRED.setName("WRED");
          pnlOptions.add(WRED, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNombreExemplaire ----
          lbNombreExemplaire.setText("Nombre d'exemplaires");
          lbNombreExemplaire.setPreferredSize(new Dimension(175, 19));
          lbNombreExemplaire.setMinimumSize(new Dimension(175, 19));
          lbNombreExemplaire.setName("lbNombreExemplaire");
          pnlOptions.add(lbNombreExemplaire, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WNEX ----
          WNEX.setComponentPopupMenu(null);
          WNEX.setPreferredSize(new Dimension(24, 30));
          WNEX.setMinimumSize(new Dimension(24, 30));
          WNEX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WNEX.setName("WNEX");
          pnlOptions.add(WNEX, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- WFOR ----
          WFOR.setText("For\u00e7age");
          WFOR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WFOR.setComponentPopupMenu(null);
          WFOR.setFont(new Font("sansserif", Font.PLAIN, 14));
          WFOR.setMaximumSize(new Dimension(90, 30));
          WFOR.setPreferredSize(new Dimension(90, 30));
          WFOR.setMinimumSize(new Dimension(90, 30));
          WFOR.setName("WFOR");
          pnlOptions.add(WFOR, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pmBTD ========
    {
      pmBTD.setName("pmBTD");

      //---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);

      //---- miAideEnLigne ----
      miAideEnLigne.setText("Aide en ligne");
      miAideEnLigne.setName("miAideEnLigne");
      miAideEnLigne.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigneActionPerformed(e);
        }
      });
      pmBTD.add(miAideEnLigne);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbDocumentAchat;
  private SNPanel pnlPlageDocumentsAchat;
  private XRiTextField WNDEB;
  private XRiTextField WNDEBS;
  private SNLabelChamp lbA;
  private XRiTextField WNFIN;
  private XRiTextField WNFINS;
  private SNLabelChamp lbParMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbParFournisseur;
  private SNFournisseur snFournisseur;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriode;
  private SNTexte tfDateEnCours;
  private SNPanelTitre pnlOptions;
  private SNLabelChamp lbTriEdition;
  private SNComboBox cbChoixTri;
  private XRiCheckBox WRED;
  private SNLabelChamp lbNombreExemplaire;
  private XRiTextField WNEX;
  private XRiCheckBox WFOR;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  private JMenuItem miAideEnLigne;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
