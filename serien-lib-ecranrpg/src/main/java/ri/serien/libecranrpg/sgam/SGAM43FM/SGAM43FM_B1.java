
package ri.serien.libecranrpg.sgam.SGAM43FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM43FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM43FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    MAJ03.setValeurs("3", "RB");
    MAJ02.setValeurs("2", "RB");
    MAJ01.setValeurs("1", "RB");
    WTOU.setValeursSelection("**", "  ");
    WTOUF.setValeursSelection("**", "  ");
    WBLOC1.setValeursSelection("X", " ");
    WBLOC2.setValeursSelection("X", " ");
    REPON1.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_42.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBELLE1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    WRA.setEnabled(lexique.isPresent("WRA"));
    // WTOUF.setVisible( lexique.isPresent("WTOUF"));
    // WTOUF.setSelected(lexique.HostFieldGetData("WTOUF").equalsIgnoreCase("**"));
    // WTOU.setVisible( lexique.isPresent("WTOU"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // WBLOC1.setSelected(lexique.HostFieldGetData("WBLOC1").equalsIgnoreCase("X"));
    // WBLOC2.setSelected(lexique.HostFieldGetData("WBLOC2").equalsIgnoreCase("X"));
    // MAJ03.setVisible( lexique.isPresent("MAJ03"));
    // MAJ03.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // MAJ02.setVisible( lexique.isPresent("MAJ02"));
    // MAJ02.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // MAJ01.setVisible( lexique.isPresent("MAJ01"));
    // MAJ01.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOUF").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    
    panel1.setVisible(lexique.isPresent("WTOUF"));
    panel2.setVisible(lexique.isPresent("WTOU"));
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOUF.isSelected())
    // lexique.HostFieldPutData("WTOUF", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUF", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (WBLOC1.isSelected())
    // lexique.HostFieldPutData("WBLOC1", 0, "X");
    // else
    // lexique.HostFieldPutData("WBLOC1", 0, " ");
    // if (WBLOC2.isSelected())
    // lexique.HostFieldPutData("WBLOC2", 0, "X");
    // else
    // lexique.HostFieldPutData("WBLOC2", 0, " ");
    // if (MAJ03.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (MAJ02.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (MAJ01.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUFActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_42 = new JXTitledSeparator();
    OBJ_36 = new JXTitledSeparator();
    OBJ_55 = new JXTitledSeparator();
    OBJ_37 = new JXTitledSeparator();
    OBJ_56 = new JXTitledSeparator();
    REPON1 = new XRiCheckBox();
    MAJ01 = new XRiRadioButton();
    MAJ02 = new XRiRadioButton();
    MAJ03 = new XRiRadioButton();
    WBLOC2 = new XRiCheckBox();
    WBLOC1 = new XRiCheckBox();
    panel1 = new JPanel();
    WTOUF = new XRiCheckBox();
    P_SEL1 = new JPanel();
    OBJ_44 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    label1 = new JLabel();
    panel2 = new JPanel();
    WTOU = new XRiCheckBox();
    P_SEL0 = new JPanel();
    OBJ_45 = new JLabel();
    OBJ_52 = new JLabel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    panel3 = new JPanel();
    OBJ_57 = new JLabel();
    WFRS = new XRiTextField();
    panel4 = new JPanel();
    OBJ_66 = new JLabel();
    WRA = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_42 ----
          OBJ_42.setTitle("@LIBELLE1@");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_36 ----
          OBJ_36.setTitle("Options");
          OBJ_36.setName("OBJ_36");

          //---- OBJ_55 ----
          OBJ_55.setTitle("Mise \u00e0 jour du prix de revient");
          OBJ_55.setName("OBJ_55");

          //---- OBJ_37 ----
          OBJ_37.setTitle("");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_56 ----
          OBJ_56.setTitle("");
          OBJ_56.setName("OBJ_56");

          //---- REPON1 ----
          REPON1.setText("Mise \u00e0 jour articles dont la protection est \u00e9gale \u00e0 1");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- MAJ01 ----
          MAJ01.setText("\u00e0 partir du dernier prix d'achat");
          MAJ01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MAJ01.setName("MAJ01");

          //---- MAJ02 ----
          MAJ02.setText("\u00e0 partir de la CNA principale");
          MAJ02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MAJ02.setName("MAJ02");

          //---- MAJ03 ----
          MAJ03.setText("\u00e0 partir du prix le plus r\u00e9cent");
          MAJ03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MAJ03.setName("MAJ03");

          //---- WBLOC2 ----
          WBLOC2.setText("D\u00e9blocage prix de revient");
          WBLOC2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WBLOC2.setName("WBLOC2");

          //---- WBLOC1 ----
          WBLOC1.setText("Blocage prix de revient");
          WBLOC1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WBLOC1.setName("WBLOC1");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WTOUF ----
            WTOUF.setText("S\u00e9lection compl\u00e8te");
            WTOUF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUF.setName("WTOUF");
            WTOUF.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUFActionPerformed(e);
              }
            });
            panel1.add(WTOUF);
            WTOUF.setBounds(5, 32, 141, 18);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");

              //---- OBJ_44 ----
              OBJ_44.setText("Code groupe-famille d\u00e9but");
              OBJ_44.setName("OBJ_44");

              //---- FAMDEB ----
              FAMDEB.setComponentPopupMenu(BTD);
              FAMDEB.setName("FAMDEB");

              //---- FAMFIN ----
              FAMFIN.setComponentPopupMenu(BTD);
              FAMFIN.setName("FAMFIN");

              //---- label1 ----
              label1.setText("Code groupe-famille fin");
              label1.setName("label1");

              GroupLayout P_SEL1Layout = new GroupLayout(P_SEL1);
              P_SEL1.setLayout(P_SEL1Layout);
              P_SEL1Layout.setHorizontalGroup(
                P_SEL1Layout.createParallelGroup()
                  .addGroup(P_SEL1Layout.createSequentialGroup()
                    .addGap(23, 23, 23)
                    .addGroup(P_SEL1Layout.createParallelGroup()
                      .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                    .addGap(40, 40, 40)
                    .addGroup(P_SEL1Layout.createParallelGroup()
                      .addComponent(FAMDEB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(FAMFIN, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
              );
              P_SEL1Layout.setVerticalGroup(
                P_SEL1Layout.createParallelGroup()
                  .addGroup(P_SEL1Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGroup(P_SEL1Layout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addGroup(P_SEL1Layout.createParallelGroup()
                      .addComponent(FAMDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(P_SEL1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(FAMFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
              );
            }
            panel1.add(P_SEL1);
            P_SEL1.setBounds(170, 5, 399, 72);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel2.add(WTOU);
            WTOU.setBounds(5, 35, 141, WTOU.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_45 ----
              OBJ_45.setText("Code article D\u00e9but");
              OBJ_45.setName("OBJ_45");
              P_SEL0.add(OBJ_45);
              OBJ_45.setBounds(20, 15, 114, 18);

              //---- OBJ_52 ----
              OBJ_52.setText("Code article Fin");
              OBJ_52.setName("OBJ_52");
              P_SEL0.add(OBJ_52);
              OBJ_52.setBounds(20, 40, 96, 18);

              //---- ARTDEB ----
              ARTDEB.setComponentPopupMenu(BTD);
              ARTDEB.setName("ARTDEB");
              P_SEL0.add(ARTDEB);
              ARTDEB.setBounds(175, 10, 210, ARTDEB.getPreferredSize().height);

              //---- ARTFIN ----
              ARTFIN.setComponentPopupMenu(BTD);
              ARTFIN.setName("ARTFIN");
              P_SEL0.add(ARTFIN);
              ARTFIN.setBounds(175, 35, 210, ARTFIN.getPreferredSize().height);
            }
            panel2.add(P_SEL0);
            P_SEL0.setBounds(170, 5, 399, 72);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_57 ----
            OBJ_57.setText("Code fournisseur");
            OBJ_57.setName("OBJ_57");
            panel3.add(OBJ_57);
            OBJ_57.setBounds(5, 10, 105, 18);

            //---- WFRS ----
            WFRS.setComponentPopupMenu(BTD);
            WFRS.setName("WFRS");
            panel3.add(WFRS);
            WFRS.setBounds(135, 5, 80, WFRS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_66 ----
            OBJ_66.setText("Regroupement achat");
            OBJ_66.setName("OBJ_66");
            panel4.add(OBJ_66);
            OBJ_66.setBounds(10, 10, 129, 18);

            //---- WRA ----
            WRA.setComponentPopupMenu(BTD);
            WRA.setName("WRA");
            panel4.add(WRA);
            WRA.setBounds(140, 5, 110, WRA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(92, 92, 92)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 585, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 585, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(MAJ01, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MAJ02, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(349, 349, 349)
                .addComponent(MAJ03, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 316, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WBLOC1, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(349, 349, 349)
                .addComponent(WBLOC2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(MAJ01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(MAJ02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(MAJ03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 3, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(WBLOC1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addComponent(WBLOC2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(MAJ01);
    RB_GRP.add(MAJ02);
    RB_GRP.add(MAJ03);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_42;
  private JXTitledSeparator OBJ_36;
  private JXTitledSeparator OBJ_55;
  private JXTitledSeparator OBJ_37;
  private JXTitledSeparator OBJ_56;
  private XRiCheckBox REPON1;
  private XRiRadioButton MAJ01;
  private XRiRadioButton MAJ02;
  private XRiRadioButton MAJ03;
  private XRiCheckBox WBLOC2;
  private XRiCheckBox WBLOC1;
  private JPanel panel1;
  private XRiCheckBox WTOUF;
  private JPanel P_SEL1;
  private JLabel OBJ_44;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JLabel label1;
  private JPanel panel2;
  private XRiCheckBox WTOU;
  private JPanel P_SEL0;
  private JLabel OBJ_45;
  private JLabel OBJ_52;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JPanel panel3;
  private JLabel OBJ_57;
  private XRiTextField WFRS;
  private JPanel panel4;
  private JLabel OBJ_66;
  private XRiTextField WRA;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
