
package ri.serien.libecranrpg.sgam.SGAM17FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM17FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private boolean isPlanning = false;
  private String[] cbCodeDate_Value = { "*DAT", "*ENC", "*PRE", };
  private String[] cbCodeDate_Text = { "Date du jour", "Mois en cours", "Mois précédant", };
  private Message LOCTP = null;
  private String BOUTON_EXPORTER = "Exporter";
  private String[] WTRI_Value = { "FRS", "FAM", };
  
  public SGAM17FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTRI.setValeurs(WTRI_Value, null);
    
    cbCodeDate.removeAllItems();
    for (String texte : cbCodeDate_Text) {
      cbCodeDate.addItem(texte);
    }
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Gestion de LOCTP
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    isPlanning = lexique.isTrue("96");
    
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty() || isPlanning);
    lbPeriode.setVisible(isPlanning);
    cbCodeDate.setVisible(isPlanning);
    lbPlanning.setVisible(isPlanning);
    pnlFournisseur.setVisible(!isPlanning);
    lbDateDebut.setVisible(!isPlanning);
    lbDateFin.setVisible(!isPlanning);
    
    if (isPlanning) {
      cbCodeDate.setSelectedItem(lexique.HostFieldGetData("PERIO1"));
    }
    
    // Charge le composant établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    if (isPlanning) {
      lexique.HostFieldPutData("PERIO1", 0, cbCodeDate_Value[cbCodeDate.getSelectedIndex()]);
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    p_sud = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    lbPlanning = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbDateDebut = new SNLabelChamp();
    pnlFournisseur = new SNPanel();
    PERIO1 = new XRiCalendrier();
    PERIO2 = new XRiCalendrier();
    lbDateFin = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    cbCodeDate = new SNComboBox();
    lbCategorieFournisseur = new SNLabelChamp();
    WTRI = new XRiComboBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      p_nord.add(bpPresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      p_sud.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());
        
        // ======== pnlMessage ========
        {
          pnlMessage.setName("pnlMessage");
          pnlMessage.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbLOCTP ----
          lbLOCTP.setText("@LOCTP@");
          lbLOCTP.setMinimumSize(new Dimension(120, 30));
          lbLOCTP.setPreferredSize(new Dimension(120, 30));
          lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbLOCTP.setName("lbLOCTP");
          pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPlanning ----
          lbPlanning.setText("Vous \u00eates en cours de param\u00e9trage en vue de la mise au planning");
          lbPlanning.setMinimumSize(new Dimension(120, 30));
          lbPlanning.setPreferredSize(new Dimension(120, 30));
          lbPlanning.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbPlanning.setBackground(new Color(214, 217, 223));
          lbPlanning.setForeground(Color.red);
          lbPlanning.setHorizontalAlignment(SwingConstants.CENTER);
          lbPlanning.setName("lbPlanning");
          pnlMessage.add(lbPlanning, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlMessage, BorderLayout.NORTH);
        
        // ======== pnlColonne ========
        {
          pnlColonne.setName("pnlColonne");
          pnlColonne.setLayout(new GridLayout());
          
          // ======== pnlGauche ========
          {
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlCritereDeSelection ========
            {
              pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
              pnlCritereDeSelection.setName("pnlCritereDeSelection");
              pnlCritereDeSelection.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbDateDebut ----
              lbDateDebut.setText("Date de d\u00e9but");
              lbDateDebut.setPreferredSize(new Dimension(200, 30));
              lbDateDebut.setMinimumSize(new Dimension(200, 30));
              lbDateDebut.setMaximumSize(new Dimension(200, 30));
              lbDateDebut.setName("lbDateDebut");
              pnlCritereDeSelection.add(lbDateDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlFournisseur ========
              {
                pnlFournisseur.setName("pnlFournisseur");
                pnlFournisseur.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlFournisseur.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlFournisseur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlFournisseur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- PERIO1 ----
                PERIO1.setTypeSaisie(6);
                PERIO1.setMaximumSize(new Dimension(100, 30));
                PERIO1.setMinimumSize(new Dimension(100, 30));
                PERIO1.setPreferredSize(new Dimension(100, 30));
                PERIO1.setFont(new Font("sansserif", Font.PLAIN, 14));
                PERIO1.setName("PERIO1");
                pnlFournisseur.add(PERIO1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- PERIO2 ----
                PERIO2.setTypeSaisie(6);
                PERIO2.setMaximumSize(new Dimension(100, 30));
                PERIO2.setMinimumSize(new Dimension(100, 30));
                PERIO2.setPreferredSize(new Dimension(100, 30));
                PERIO2.setFont(new Font("sansserif", Font.PLAIN, 14));
                PERIO2.setName("PERIO2");
                pnlFournisseur.add(PERIO2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlCritereDeSelection.add(pnlFournisseur, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDateFin ----
              lbDateFin.setText("Date de fin");
              lbDateFin.setPreferredSize(new Dimension(200, 30));
              lbDateFin.setMinimumSize(new Dimension(200, 30));
              lbDateFin.setMaximumSize(new Dimension(200, 30));
              lbDateFin.setName("lbDateFin");
              pnlCritereDeSelection.add(lbDateFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbPeriode ----
              lbPeriode.setText("P\u00e9riode");
              lbPeriode.setPreferredSize(new Dimension(200, 30));
              lbPeriode.setMinimumSize(new Dimension(200, 30));
              lbPeriode.setMaximumSize(new Dimension(200, 30));
              lbPeriode.setName("lbPeriode");
              pnlCritereDeSelection.add(lbPeriode, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- cbCodeDate ----
              cbCodeDate.setModel(new DefaultComboBoxModel(new String[] { "Date du jour", "D\u00e9but du mois en cours",
                  "Fin du mois en cours", "D\u00e9but du mois pr\u00e9c\u00e9dent", "Fin du mois pr\u00e9c\u00e9dent" }));
              cbCodeDate.setBackground(Color.white);
              cbCodeDate.setFont(new Font("sansserif", Font.PLAIN, 14));
              cbCodeDate.setName("cbCodeDate");
              pnlCritereDeSelection.add(cbCodeDate, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCategorieFournisseur ----
              lbCategorieFournisseur.setText("Tri par fournisseur ou famille");
              lbCategorieFournisseur.setPreferredSize(new Dimension(200, 30));
              lbCategorieFournisseur.setMinimumSize(new Dimension(200, 30));
              lbCategorieFournisseur.setMaximumSize(new Dimension(200, 30));
              lbCategorieFournisseur.setName("lbCategorieFournisseur");
              pnlCritereDeSelection.add(lbCategorieFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WTRI ----
              WTRI.setModel(new DefaultComboBoxModel(new String[] { "Tri par fournisseur", "Tri par famille" }));
              WTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTRI.setBackground(Color.white);
              WTRI.setFont(new Font("sansserif", Font.PLAIN, 14));
              WTRI.setName("WTRI");
              pnlCritereDeSelection.add(WTRI, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlColonne.add(pnlGauche);
          
          // ======== pnlDroite ========
          {
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlEtablissement ========
            {
              pnlEtablissement.setTitre("Etablissement");
              pnlEtablissement.setName("pnlEtablissement");
              pnlEtablissement.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- lbEtablissementEnCours ----
              lbEtablissementEnCours.setText("Etablissement en cours");
              lbEtablissementEnCours.setName("lbEtablissementEnCours");
              pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snEtablissement ----
              snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
              snEtablissement.setName("snEtablissement");
              snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snEtablissementValueChanged(e);
                }
              });
              pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbPeriodeEnCours ----
              lbPeriodeEnCours.setText("P\u00e9riode en cours");
              lbPeriodeEnCours.setName("lbPeriodeEnCours");
              pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfPeriodeEnCours ----
              tfPeriodeEnCours.setText("@WENCX@");
              tfPeriodeEnCours.setEnabled(false);
              tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
              tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
              tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
              tfPeriodeEnCours.setName("tfPeriodeEnCours");
              pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlColonne.add(pnlDroite);
        }
        pnlContenu.add(pnlColonne, BorderLayout.CENTER);
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre bpPresentation;
  private SNPanel p_sud;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNLabelTitre lbPlanning;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbDateDebut;
  private SNPanel pnlFournisseur;
  private XRiCalendrier PERIO1;
  private XRiCalendrier PERIO2;
  private SNLabelChamp lbDateFin;
  private SNLabelChamp lbPeriode;
  private SNComboBox cbCodeDate;
  private SNLabelChamp lbCategorieFournisseur;
  private XRiComboBox WTRI;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
