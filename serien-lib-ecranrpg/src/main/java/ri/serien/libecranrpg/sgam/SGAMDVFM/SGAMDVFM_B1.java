
package ri.serien.libecranrpg.sgam.SGAMDVFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SGAMDVFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAMDVFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CONF.setValeurs("OUI", CONF_GRP);
    CONF_NON.setValeurs("NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    IBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IBON@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // OBJ_27.setEnabled( lexique.isPresent("CONF"));
    // OBJ_27.setSelected(lexique.HostFieldGetData("CONF").equalsIgnoreCase("NON"));
    // OBJ_25.setEnabled( lexique.isPresent("CONF"));
    // OBJ_25.setSelected(lexique.HostFieldGetData("CONF").equalsIgnoreCase("OUI"));
    IBON.setVisible(lexique.isPresent("IBON"));
    
    // TODO Icones
    OBJ_24.setIcon(lexique.chargerImage("images/msgbox04.gif", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("GAM - DEVERROUILLAGE"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OBJ_27.isSelected())
    // lexique.HostFieldPutData("CONF", 0, "NON");
    // if (OBJ_25.isSelected())
    // lexique.HostFieldPutData("CONF", 0, "OUI");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_17 = new JLabel();
    IBON = new RiZoneSortie();
    OBJ_23 = new JPanel();
    OBJ_26 = new JLabel();
    OBJ_24 = new JLabel();
    CONF = new XRiRadioButton();
    CONF_NON = new XRiRadioButton();
    OBJ_21 = new JLabel();
    OBJ_19 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_22 = new JLabel();
    CONF_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(670, 300));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(670, 300));
        p_contenu.setName("p_contenu");

        //---- OBJ_17 ----
        OBJ_17.setText("D\u00e9verrouillage du bon de GAM");
        OBJ_17.setName("OBJ_17");

        //---- IBON ----
        IBON.setText("@IBON@");
        IBON.setName("IBON");

        //======== OBJ_23 ========
        {
          OBJ_23.setOpaque(false);
          OBJ_23.setName("OBJ_23");
          OBJ_23.setLayout(null);

          //---- OBJ_26 ----
          OBJ_26.setText("Confirmez votre demande ?");
          OBJ_26.setFont(OBJ_26.getFont().deriveFont(OBJ_26.getFont().getStyle() | Font.BOLD));
          OBJ_26.setName("OBJ_26");
          OBJ_23.add(OBJ_26);
          OBJ_26.setBounds(119, 12, 208, 26);

          //---- OBJ_24 ----
          OBJ_24.setIcon(new ImageIcon("images/msgbox04.gif"));
          OBJ_24.setName("OBJ_24");
          OBJ_23.add(OBJ_24);
          OBJ_24.setBounds(6, 2, 51, 42);

          //---- CONF ----
          CONF.setText("OUI");
          CONF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CONF.setFont(CONF.getFont().deriveFont(CONF.getFont().getStyle() | Font.BOLD));
          CONF.setName("CONF");
          OBJ_23.add(CONF);
          CONF.setBounds(372, 2, 66, 20);

          //---- CONF_NON ----
          CONF_NON.setText("NON");
          CONF_NON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CONF_NON.setFont(CONF_NON.getFont().deriveFont(CONF_NON.getFont().getStyle() | Font.BOLD));
          CONF_NON.setName("CONF_NON");
          OBJ_23.add(CONF_NON);
          CONF_NON.setBounds(372, 31, 66, 20);
        }

        //---- OBJ_21 ----
        OBJ_21.setText("<html>Ce bon est peut-\u00eatre en cours de traitement sur<br/><br/> un autre \u00e9cran (si tel est le cas, annulez l'op\u00e9ration)</html>");
        OBJ_21.setFont(OBJ_21.getFont().deriveFont(OBJ_21.getFont().getStyle() | Font.BOLD));
        OBJ_21.setName("OBJ_21");

        //---- OBJ_19 ----
        OBJ_19.setText("ATTENTION");
        OBJ_19.setFont(OBJ_19.getFont().deriveFont(OBJ_19.getFont().getStyle() | Font.BOLD));
        OBJ_19.setName("OBJ_19");

        //---- OBJ_20 ----
        OBJ_20.setText("Ceci est une op\u00e9ration grave :");
        OBJ_20.setFont(OBJ_20.getFont().deriveFont(OBJ_20.getFont().getStyle() | Font.BOLD));
        OBJ_20.setName("OBJ_20");

        //---- OBJ_22 ----
        OBJ_22.setText("Sinon, contactez votre assistance t\u00e9l\u00e9phonique...");
        OBJ_22.setFont(OBJ_22.getFont().deriveFont(OBJ_22.getFont().getStyle() | Font.BOLD));
        OBJ_22.setName("OBJ_22");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(190, 190, 190)
                      .addComponent(IBON, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))))
                .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 411, GroupLayout.PREFERRED_SIZE))
                .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 411, GroupLayout.PREFERRED_SIZE))
                .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 463, GroupLayout.PREFERRED_SIZE))
                .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 411, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_21, 0, 0, Short.MAX_VALUE))))
              .addContainerGap(22, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(32, 32, 32)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addComponent(IBON, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(19, 19, 19)
              .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addGap(4, 4, 4)
              .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addGap(15, 15, 15)
              .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- CONF_GRP ----
    CONF_GRP.add(CONF);
    CONF_GRP.add(CONF_NON);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JLabel OBJ_17;
  private RiZoneSortie IBON;
  private JPanel OBJ_23;
  private JLabel OBJ_26;
  private JLabel OBJ_24;
  private XRiRadioButton CONF;
  private XRiRadioButton CONF_NON;
  private JLabel OBJ_21;
  private JLabel OBJ_19;
  private JLabel OBJ_20;
  private JLabel OBJ_22;
  private ButtonGroup CONF_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
