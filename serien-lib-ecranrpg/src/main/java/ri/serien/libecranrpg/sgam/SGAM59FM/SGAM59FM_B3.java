
package ri.serien.libecranrpg.sgam.SGAM59FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGAM59FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM59FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CODPER.setValeurs("C", CODPER_GRP);
    CODPER_E.setValeurs("E");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // PERFIN.setEnabled( lexique.isPresent("PERFIN"));
    // PERDEB.setEnabled( lexique.isPresent("PERDEB"));
    // OBJ_13.setEnabled( lexique.isPresent("CODPER"));
    // OBJ_13.setSelected(lexique.HostFieldGetData("CODPER").equalsIgnoreCase("E"));
    // OBJ_14.setEnabled( lexique.isPresent("CODPER"));
    // OBJ_14.setSelected(lexique.HostFieldGetData("CODPER").equalsIgnoreCase("C"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Paramètres de calcul"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OBJ_13.isSelected())
    // lexique.HostFieldPutData("CODPER", 0, "E");
    // if (OBJ_14.isSelected())
    // lexique.HostFieldPutData("CODPER", 0, "C");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_6 = new JPanel();
    CODPER = new XRiRadioButton();
    CODPER_E = new XRiRadioButton();
    OBJ_19 = new JLabel();
    OBJ_8 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_23 = new JLabel();
    WPERCO = new XRiTextField();
    WPCDE = new XRiTextField();
    PERDEB = new XRiCalendrier();
    PERFIN = new XRiCalendrier();
    OBJ_7 = new JPanel();
    OBJ_20 = new JLabel();
    WCOEF = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    CODPER_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(705, 290));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== OBJ_6 ========
        {
          OBJ_6.setBorder(new TitledBorder("Calcul des consommations"));
          OBJ_6.setOpaque(false);
          OBJ_6.setName("OBJ_6");
          OBJ_6.setLayout(null);

          //---- CODPER ----
          CODPER.setText("Commandes");
          CODPER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CODPER.setName("CODPER");
          OBJ_6.add(CODPER);
          CODPER.setBounds(360, 129, 102, 20);

          //---- CODPER_E ----
          CODPER_E.setText("Exp\u00e9ditions");
          CODPER_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CODPER_E.setName("CODPER_E");
          OBJ_6.add(CODPER_E);
          CODPER_E.setBounds(360, 100, 102, 20);

          //---- OBJ_19 ----
          OBJ_19.setText("Commandes");
          OBJ_19.setName("OBJ_19");
          OBJ_6.add(OBJ_19);
          OBJ_19.setBounds(35, 85, 85, 18);

          //---- OBJ_8 ----
          OBJ_8.setText("Exp\u00e9ditions");
          OBJ_8.setName("OBJ_8");
          OBJ_6.add(OBJ_8);
          OBJ_8.setBounds(35, 40, 85, 18);

          //---- OBJ_21 ----
          OBJ_21.setText("ou");
          OBJ_21.setName("OBJ_21");
          OBJ_6.add(OBJ_21);
          OBJ_21.setBounds(35, 105, 25, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("ou");
          OBJ_18.setName("OBJ_18");
          OBJ_6.add(OBJ_18);
          OBJ_18.setBounds(35, 60, 20, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("du");
          OBJ_22.setName("OBJ_22");
          OBJ_6.add(OBJ_22);
          OBJ_22.setBounds(35, 129, 25, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("au");
          OBJ_23.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_23.setName("OBJ_23");
          OBJ_6.add(OBJ_23);
          OBJ_23.setBounds(195, 129, 25, 20);

          //---- WPERCO ----
          WPERCO.setToolTipText("Valeurs possibles: de 1 \u00e0 6");
          WPERCO.setComponentPopupMenu(BTD);
          WPERCO.setName("WPERCO");
          OBJ_6.add(WPERCO);
          WPERCO.setBounds(125, 35, 20, WPERCO.getPreferredSize().height);

          //---- WPCDE ----
          WPCDE.setToolTipText("Valeurs possibles: de 1 \u00e0 6");
          WPCDE.setComponentPopupMenu(BTD);
          WPCDE.setName("WPCDE");
          OBJ_6.add(WPCDE);
          WPCDE.setBounds(125, 80, 20, WPCDE.getPreferredSize().height);

          //---- PERDEB ----
          PERDEB.setName("PERDEB");
          OBJ_6.add(PERDEB);
          PERDEB.setBounds(75, 125, 105, PERDEB.getPreferredSize().height);

          //---- PERFIN ----
          PERFIN.setName("PERFIN");
          OBJ_6.add(PERFIN);
          PERFIN.setBounds(230, 125, 105, PERFIN.getPreferredSize().height);
        }

        //======== OBJ_7 ========
        {
          OBJ_7.setBorder(new TitledBorder(""));
          OBJ_7.setOpaque(false);
          OBJ_7.setName("OBJ_7");
          OBJ_7.setLayout(null);

          //---- OBJ_20 ----
          OBJ_20.setText("Coefficient");
          OBJ_20.setName("OBJ_20");
          OBJ_7.add(OBJ_20);
          OBJ_20.setBounds(35, 15, 73, 18);

          //---- WCOEF ----
          WCOEF.setComponentPopupMenu(BTD);
          WCOEF.setName("WCOEF");
          OBJ_7.add(WCOEF);
          WCOEF.setBounds(125, 10, 60, WCOEF.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //---- CODPER_GRP ----
    CODPER_GRP.add(CODPER);
    CODPER_GRP.add(CODPER_E);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel OBJ_6;
  private XRiRadioButton CODPER;
  private XRiRadioButton CODPER_E;
  private JLabel OBJ_19;
  private JLabel OBJ_8;
  private JLabel OBJ_21;
  private JLabel OBJ_18;
  private JLabel OBJ_22;
  private JLabel OBJ_23;
  private XRiTextField WPERCO;
  private XRiTextField WPCDE;
  private XRiCalendrier PERDEB;
  private XRiCalendrier PERFIN;
  private JPanel OBJ_7;
  private JLabel OBJ_20;
  private XRiTextField WCOEF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  private ButtonGroup CODPER_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
