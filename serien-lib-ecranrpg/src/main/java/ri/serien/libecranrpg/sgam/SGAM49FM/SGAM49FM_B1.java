
package ri.serien.libecranrpg.sgam.SGAM49FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM2781] Gestion des achats -> Documents d'achats -> Etat des bons -> Etats d'analyse des réceptions -> Analyse des réceptions (modèle
 * 1)
 * Indicateur : 00000001
 * Titre : Réception fournisseur
 */
public class SGAM49FM_B1 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  // Variables
  private boolean modePlanning = false;
  private Message LOCTP = null;
  
  public SGAM49FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    modePlanning = lexique.isTrue("97");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    rafraichirEtablissement();
    
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Mode planning
    lbDate.setVisible(!modePlanning);
    rbDate.setVisible(modePlanning);
    rbDateParametree.setVisible(modePlanning);
    pnlDateParametree.setVisible(modePlanning);
    // Message planning
    lbPlanning.setVisible(modePlanning);
    if (modePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
      // boites à choix date paramétrée
      cbDatePlanning.removeAllItems();
      cbDatePlanning.addItem("");
      cbDatePlanning.addItem("Date du jour");
      cbDatePlanning.addItem("Début du mois en cours");
      cbDatePlanning.addItem("Début du mois précédent");
      cbDatePlanning.addItem("Fin du mois précédent");
      cbDatePlanning2.removeAllItems();
      cbDatePlanning2.addItem("");
      cbDatePlanning2.addItem("Date du jour");
      cbDatePlanning2.addItem("Début du mois en cours");
      cbDatePlanning2.addItem("Début du mois précédent");
      cbDatePlanning2.addItem("Fin du mois précédent");
      if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).startsWith("*")) {
        rbDate.setSelected(false);
        rbDateParametree.setSelected(true);
      }
      else {
        rbDate.setSelected(true);
        rbDateParametree.setSelected(false);
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*DAT")) {
        cbDatePlanning.setSelectedItem("Date du jour");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*DME")) {
        cbDatePlanning.setSelectedItem("Début du mois en cours");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*DMP")) {
        cbDatePlanning.setSelectedItem("Début du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATDEB")).equals("*FMP")) {
        cbDatePlanning.setSelectedItem("Fin du mois précédent");
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*DAT")) {
        cbDatePlanning2.setSelectedItem("Date du jour");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*DME")) {
        cbDatePlanning2.setSelectedItem("Début du mois en cours");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*DMP")) {
        cbDatePlanning2.setSelectedItem("Début du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("DATFIN")).equals("*FMP")) {
        cbDatePlanning2.setSelectedItem("Fin du mois précédent");
      }
    }
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!Constantes.normerTexte(lexique.HostFieldGetData("LOCTP")).isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (NUMDEB.getText().isEmpty() || NUMFIN.getText().isEmpty()) {
      lexique.HostFieldPutData("WTOU", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU", 0, "");
    }
    if (modePlanning) {
      switch (cbDatePlanning.getSelectedIndex()) {
        case 1:
          lexique.HostFieldPutData("DATDEB", 0, "*DAT");
          break;
        case 2:
          lexique.HostFieldPutData("DATDEB", 0, "*DME");
          break;
        case 3:
          lexique.HostFieldPutData("DATDEB", 0, "*DMP");
          break;
        case 4:
          lexique.HostFieldPutData("DATDEB", 0, "*FMP");
          break;
        
        default:
          break;
      }
      switch (cbDatePlanning2.getSelectedIndex()) {
        case 1:
          lexique.HostFieldPutData("DATFIN", 0, "*DAT");
          break;
        case 2:
          lexique.HostFieldPutData("DATFIN", 0, "*DME");
          break;
        case 3:
          lexique.HostFieldPutData("DATFIN", 0, "*DMP");
          break;
        case 4:
          lexique.HostFieldPutData("DATFIN", 0, "*FMP");
          break;
        
        default:
          break;
      }
    }
  }
  
  /**
   * Initialise le composant établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  private void modifierParametrageDate() {
    if (rbDate.isSelected()) {
      DATDEB.setEnabled(true);
      DATFIN.setEnabled(true);
      DATDEB.setDate(null);
      DATFIN.setDate(null);
      cbDatePlanning.setSelectedIndex(0);
      cbDatePlanning.setEnabled(false);
      cbDatePlanning2.setSelectedIndex(0);
      cbDatePlanning2.setEnabled(false);
    }
    else if (rbDateParametree.isSelected()) {
      lexique.HostFieldPutData("DATDEB", 0, "");
      lexique.HostFieldPutData("DATFIN", 0, "");
      DATDEB.setDate(null);
      DATDEB.setEnabled(false);
      DATFIN.setDate(null);
      DATFIN.setEnabled(false);
      cbDatePlanning.setEnabled(true);
      cbDatePlanning2.setEnabled(true);
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbDateParametreeItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbDateItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void DATFINInputMethodTextChanged(InputMethodEvent e) {
    try {
      cbDatePlanning.setSelectedIndex(0);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void DATDEBInputMethodTextChanged(InputMethodEvent e) {
    try {
      cbDatePlanning2.setSelectedIndex(0);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbPlanning = new SNMessage();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbNumeroDeBon = new SNLabelChamp();
    pnlNumeroBon = new SNPanel();
    NUMDEB = new XRiTextField();
    lbANumeroBon = new SNLabelChamp();
    NUMFIN = new XRiTextField();
    pnlLibelleDate = new SNPanel();
    rbDate = new SNRadioButton();
    lbDate = new SNLabelChamp();
    pnlPeriodeAEditer = new SNPanel();
    DATDEB = new XRiCalendrier();
    lbAPeriode = new SNLabelChamp();
    DATFIN = new XRiCalendrier();
    rbDateParametree = new SNRadioButton();
    pnlDateParametree = new SNPanel();
    cbDatePlanning = new SNComboBox();
    lbAu2 = new SNLabelChamp();
    cbDatePlanning2 = new SNComboBox();
    lbNumeroColonne = new SNLabelChamp();
    WTAR = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition de l'analyse des r\u00e9ceptions (mod\u00e8le 1)");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridLayout());
        
        // ---- lbPlanning ----
        lbPlanning.setText("Label Planning");
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning);
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP);
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDeBon ----
            lbNumeroDeBon.setText("Num\u00e9ro de bon de");
            lbNumeroDeBon.setName("lbNumeroDeBon");
            pnlCritereDeSelection.add(lbNumeroDeBon, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlNumeroBon ========
            {
              pnlNumeroBon.setName("pnlNumeroBon");
              pnlNumeroBon.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNumeroBon.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlNumeroBon.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroBon.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlNumeroBon.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- NUMDEB ----
              NUMDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMDEB.setPreferredSize(new Dimension(70, 30));
              NUMDEB.setMinimumSize(new Dimension(70, 30));
              NUMDEB.setMaximumSize(new Dimension(70, 30));
              NUMDEB.setName("NUMDEB");
              pnlNumeroBon.add(NUMDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbANumeroBon ----
              lbANumeroBon.setText("\u00e0");
              lbANumeroBon.setPreferredSize(new Dimension(8, 30));
              lbANumeroBon.setMinimumSize(new Dimension(8, 30));
              lbANumeroBon.setMaximumSize(new Dimension(8, 30));
              lbANumeroBon.setName("lbANumeroBon");
              pnlNumeroBon.add(lbANumeroBon, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- NUMFIN ----
              NUMFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              NUMFIN.setMaximumSize(new Dimension(70, 30));
              NUMFIN.setMinimumSize(new Dimension(70, 30));
              NUMFIN.setPreferredSize(new Dimension(70, 30));
              NUMFIN.setName("NUMFIN");
              pnlNumeroBon.add(NUMFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlNumeroBon, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlLibelleDate ========
            {
              pnlLibelleDate.setName("pnlLibelleDate");
              pnlLibelleDate.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- rbDate ----
              rbDate.setText("P\u00e9riode \u00e0 \u00e9diter");
              rbDate.setPreferredSize(new Dimension(150, 30));
              rbDate.setMinimumSize(new Dimension(150, 30));
              rbDate.setMaximumSize(new Dimension(250, 30));
              rbDate.setName("rbDate");
              rbDate.addItemListener(e -> rbDateItemStateChanged(e));
              pnlLibelleDate.add(rbDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDate ----
              lbDate.setText("P\u00e9riode \u00e0 \u00e9diter");
              lbDate.setName("lbDate");
              pnlLibelleDate.add(lbDate, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlLibelleDate, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlPeriodeAEditer ========
            {
              pnlPeriodeAEditer.setName("pnlPeriodeAEditer");
              pnlPeriodeAEditer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPeriodeAEditer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- DATDEB ----
              DATDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATDEB.setPreferredSize(new Dimension(110, 30));
              DATDEB.setMinimumSize(new Dimension(110, 30));
              DATDEB.setMaximumSize(new Dimension(110, 30));
              DATDEB.setName("DATDEB");
              DATDEB.addInputMethodListener(new InputMethodListener() {
                @Override
                public void caretPositionChanged(InputMethodEvent e) {
                }
                
                @Override
                public void inputMethodTextChanged(InputMethodEvent e) {
                  DATDEBInputMethodTextChanged(e);
                }
              });
              pnlPeriodeAEditer.add(DATDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAPeriode ----
              lbAPeriode.setText("\u00e0");
              lbAPeriode.setPreferredSize(new Dimension(8, 30));
              lbAPeriode.setMinimumSize(new Dimension(8, 30));
              lbAPeriode.setMaximumSize(new Dimension(8, 30));
              lbAPeriode.setName("lbAPeriode");
              pnlPeriodeAEditer.add(lbAPeriode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DATFIN ----
              DATFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
              DATFIN.setPreferredSize(new Dimension(110, 30));
              DATFIN.setMinimumSize(new Dimension(110, 30));
              DATFIN.setMaximumSize(new Dimension(110, 30));
              DATFIN.setName("DATFIN");
              DATFIN.addInputMethodListener(new InputMethodListener() {
                @Override
                public void caretPositionChanged(InputMethodEvent e) {
                }
                
                @Override
                public void inputMethodTextChanged(InputMethodEvent e) {
                  DATFINInputMethodTextChanged(e);
                }
              });
              pnlPeriodeAEditer.add(DATFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlPeriodeAEditer, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbDateParametree ----
            rbDateParametree.setText("ou date param\u00e9r\u00e9e");
            rbDateParametree.setPreferredSize(new Dimension(200, 30));
            rbDateParametree.setMinimumSize(new Dimension(200, 30));
            rbDateParametree.setMaximumSize(new Dimension(250, 30));
            rbDateParametree.setName("rbDateParametree");
            rbDateParametree.addItemListener(e -> rbDateParametreeItemStateChanged(e));
            pnlCritereDeSelection.add(rbDateParametree, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateParametree ========
            {
              pnlDateParametree.setName("pnlDateParametree");
              pnlDateParametree.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- cbDatePlanning ----
              cbDatePlanning.setName("cbDatePlanning");
              pnlDateParametree.add(cbDatePlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu2 ----
              lbAu2.setText("\u00e0");
              lbAu2.setPreferredSize(new Dimension(30, 30));
              lbAu2.setMinimumSize(new Dimension(30, 30));
              lbAu2.setMaximumSize(new Dimension(30, 30));
              lbAu2.setName("lbAu2");
              pnlDateParametree.add(lbAu2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbDatePlanning2 ----
              cbDatePlanning2.setName("cbDatePlanning2");
              pnlDateParametree.add(cbDatePlanning2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlDateParametree, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNumeroColonne ----
            lbNumeroColonne.setText("Num\u00e9ro de colonne tarif");
            lbNumeroColonne.setPreferredSize(new Dimension(153, 30));
            lbNumeroColonne.setMinimumSize(new Dimension(153, 30));
            lbNumeroColonne.setMaximumSize(new Dimension(153, 30));
            lbNumeroColonne.setName("lbNumeroColonne");
            pnlCritereDeSelection.add(lbNumeroColonne, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WTAR ----
            WTAR.setPreferredSize(new Dimension(30, 30));
            WTAR.setMinimumSize(new Dimension(30, 30));
            WTAR.setMaximumSize(new Dimension(30, 30));
            WTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTAR.setName("WTAR");
            pnlCritereDeSelection.add(WTAR, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbDate);
    buttonGroup1.add(rbDateParametree);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNMessage lbPlanning;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbNumeroDeBon;
  private SNPanel pnlNumeroBon;
  private XRiTextField NUMDEB;
  private SNLabelChamp lbANumeroBon;
  private XRiTextField NUMFIN;
  private SNPanel pnlLibelleDate;
  private SNRadioButton rbDate;
  private SNLabelChamp lbDate;
  private SNPanel pnlPeriodeAEditer;
  private XRiCalendrier DATDEB;
  private SNLabelChamp lbAPeriode;
  private XRiCalendrier DATFIN;
  private SNRadioButton rbDateParametree;
  private SNPanel pnlDateParametree;
  private SNComboBox cbDatePlanning;
  private SNLabelChamp lbAu2;
  private SNComboBox cbDatePlanning2;
  private SNLabelChamp lbNumeroColonne;
  private XRiTextField WTAR;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
