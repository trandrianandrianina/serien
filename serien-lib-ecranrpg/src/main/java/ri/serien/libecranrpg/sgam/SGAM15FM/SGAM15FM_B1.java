
package ri.serien.libecranrpg.sgam.SGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM15FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM15FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TRIDEV.setValeursSelection("1", " ");
    UPER2.setValeursSelection("X", " ");
    UPER1.setValeursSelection("X", " ");
    SELBON.setValeursSelection("1", " ");
    TRIECH.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    UPER1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO1@")).trim());
    UPER2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIO2@")).trim());
    OBJ_25.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBELLE1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    // UPER2.setSelected(lexique.HostFieldGetData("UPER2").equalsIgnoreCase("X"));
    // UPER1.setSelected(lexique.HostFieldGetData("UPER1").equalsIgnoreCase("X"));
    
    // TRIDEV.setSelected(lexique.HostFieldGetData("TRIDEV").equalsIgnoreCase("1"));
    // SELBON.setSelected(lexique.HostFieldGetData("SELBON").equalsIgnoreCase("1"));
    // TRIECH.setSelected(lexique.HostFieldGetData("TRIECH").equalsIgnoreCase("1"));
    
    XPLAG1_ck.setVisible(lexique.isPresent("XPLAG1"));
    XPLAG1_ck.setSelected(lexique.HostFieldGetData("XPLAG1").trim().equalsIgnoreCase("**"));
    panel1.setVisible((!XPLAG1_ck.isSelected()) & XPLAG1_ck.isVisible());
    
    XDEV_ck.setVisible(lexique.isPresent("XDEV"));
    XDEV_ck.setSelected(lexique.HostFieldGetData("XDEV").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible((!XDEV_ck.isSelected()) & XDEV_ck.isVisible());
    
    OBJ_12.setVisible(lexique.HostFieldGetData("TITPG1").equalsIgnoreCase("ENCOURS SUR ACHATS"));
    
    if (!lexique.isTrue("91")) {
      OBJ_25.setTitle("Plages d'activités à traiter");
    }
    else {
      OBJ_25.setTitle("Plage de sections à traiter");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TRIDEV.isSelected())
    // lexique.HostFieldPutData("TRIDEV", 0, "1");
    // else
    // lexique.HostFieldPutData("TRIDEV", 0, " ");
    // if (UPER2.isSelected())
    // lexique.HostFieldPutData("UPER2", 0, "X");
    // else
    // lexique.HostFieldPutData("UPER2", 0, " ");
    // if (UPER1.isSelected())
    // lexique.HostFieldPutData("UPER1", 0, "X");
    // else
    // lexique.HostFieldPutData("UPER1", 0, " ");
    if (XDEV_ck.isSelected()) {
      lexique.HostFieldPutData("XDEV", 0, "**");
      // else
      // if (lexique.HostFieldGetData("XDEV").trim().equalsIgnoreCase("**"))
      // lexique.HostFieldPutData("XDEV", 0, " ");
    }
    
    if (XPLAG1_ck.isSelected()) {
      lexique.HostFieldPutData("XPLAG1", 0, "**");
      // else
      // if (lexique.HostFieldGetData("XPLAG1").trim().equalsIgnoreCase("**"))
      // lexique.HostFieldPutData("XPLAG1", 0, " ");
      // if (SELBON.isSelected())
      // lexique.HostFieldPutData("SELBON", 0, "1");
      // else
      // lexique.HostFieldPutData("SELBON", 0, " ");
      // if (TRIECH.isSelected())
      // lexique.HostFieldPutData("TRIECH", 0, "1");
      // else
      // lexique.HostFieldPutData("TRIECH", 0, " ");
    }
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void XPLAG1ActionPerformed(ActionEvent e) {
    panel1.setVisible(!panel1.isVisible());
    if (!XPLAG1_ck.isSelected()) {
      XPLAG1.setText("");
    }
  }
  
  private void XDEVActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!XDEV_ck.isSelected()) {
      XDEV.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_23 = new JXTitledSeparator();
    OBJ_24 = new JXTitledSeparator();
    TRIECH = new XRiCheckBox();
    SELBON = new XRiCheckBox();
    UPER1 = new XRiCheckBox();
    UPER2 = new XRiCheckBox();
    TRIDEV = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_25 = new JXTitledSeparator();
    XPLAG1_ck = new JCheckBox();
    panel1 = new JPanel();
    OBJ_31 = new JLabel();
    XPLAG1 = new XRiTextField();
    XPLAG2 = new XRiTextField();
    OBJ_32 = new JLabel();
    panel3 = new JPanel();
    OBJ_20 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_33 = new JLabel();
    XDEV = new XRiTextField();
    XDEV_ck = new JCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(645, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_23 ----
          OBJ_23.setTitle("Options");
          OBJ_23.setName("OBJ_23");

          //---- OBJ_24 ----
          OBJ_24.setTitle("Mois de commandes \u00e0 traiter");
          OBJ_24.setName("OBJ_24");

          //---- TRIECH ----
          TRIECH.setText("Tri par \u00e9ch\u00e9ance de r\u00e8glement");
          TRIECH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIECH.setName("TRIECH");

          //---- SELBON ----
          SELBON.setText("S\u00e9lection des bons \u00e0 payer");
          SELBON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SELBON.setName("SELBON");

          //---- UPER1 ----
          UPER1.setText("@PERIO1@");
          UPER1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UPER1.setName("UPER1");

          //---- UPER2 ----
          UPER2.setText("@PERIO2@");
          UPER2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UPER2.setName("UPER2");

          //---- TRIDEV ----
          TRIDEV.setText("Tri par code devise");
          TRIDEV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIDEV.setName("TRIDEV");

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_25 ----
            OBJ_25.setTitle("@LIBELLE1@");
            OBJ_25.setName("OBJ_25");
            panel2.add(OBJ_25);
            OBJ_25.setBounds(5, 5, 575, OBJ_25.getPreferredSize().height);

            //---- XPLAG1_ck ----
            XPLAG1_ck.setText("S\u00e9lection compl\u00e8te");
            XPLAG1_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            XPLAG1_ck.setName("XPLAG1_ck");
            XPLAG1_ck.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                XPLAG1ActionPerformed(e);
              }
            });
            panel2.add(XPLAG1_ck);
            XPLAG1_ck.setBounds(20, 50, 141, 24);

            //======== panel1 ========
            {
              panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_31 ----
              OBJ_31.setText("Code de d\u00e9but");
              OBJ_31.setName("OBJ_31");
              panel1.add(OBJ_31);
              OBJ_31.setBounds(15, 14, 85, 20);

              //---- XPLAG1 ----
              XPLAG1.setComponentPopupMenu(BTD);
              XPLAG1.setName("XPLAG1");
              panel1.add(XPLAG1);
              XPLAG1.setBounds(125, 10, 54, XPLAG1.getPreferredSize().height);

              //---- XPLAG2 ----
              XPLAG2.setComponentPopupMenu(BTD);
              XPLAG2.setName("XPLAG2");
              panel1.add(XPLAG2);
              XPLAG2.setBounds(125, 40, 54, XPLAG2.getPreferredSize().height);

              //---- OBJ_32 ----
              OBJ_32.setText("Code de fin");
              OBJ_32.setName("OBJ_32");
              panel1.add(OBJ_32);
              OBJ_32.setBounds(15, 44, 75, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel2.add(panel1);
            panel1.setBounds(175, 25, 260, 75);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_20 ----
            OBJ_20.setTitle("Code devise");
            OBJ_20.setName("OBJ_20");
            panel3.add(OBJ_20);
            OBJ_20.setBounds(5, 5, 575, OBJ_20.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_33 ----
              OBJ_33.setText("Code devise");
              OBJ_33.setName("OBJ_33");
              P_SEL0.add(OBJ_33);
              OBJ_33.setBounds(15, 14, 81, 20);

              //---- XDEV ----
              XDEV.setComponentPopupMenu(BTD);
              XDEV.setName("XDEV");
              P_SEL0.add(XDEV);
              XDEV.setBounds(125, 10, 44, XDEV.getPreferredSize().height);
            }
            panel3.add(P_SEL0);
            P_SEL0.setBounds(175, 25, 258, 50);

            //---- XDEV_ck ----
            XDEV_ck.setText("S\u00e9lection compl\u00e8te");
            XDEV_ck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            XDEV_ck.setName("XDEV_ck");
            XDEV_ck.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                XDEVActionPerformed(e);
              }
            });
            panel3.add(XDEV_ck);
            XDEV_ck.setBounds(20, 38, 141, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(82, 82, 82)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addGap(45, 45, 45)
                    .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(UPER1, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
                    .addGap(186, 186, 186)
                    .addComponent(TRIDEV, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(UPER2, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
                    .addGap(186, 186, 186)
                    .addComponent(TRIECH, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(404, 404, 404)
                    .addComponent(SELBON, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 585, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 596, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(UPER1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(TRIDEV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(UPER2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(TRIECH, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(SELBON, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_23;
  private JXTitledSeparator OBJ_24;
  private XRiCheckBox TRIECH;
  private XRiCheckBox SELBON;
  private XRiCheckBox UPER1;
  private XRiCheckBox UPER2;
  private XRiCheckBox TRIDEV;
  private JPanel panel2;
  private JXTitledSeparator OBJ_25;
  private JCheckBox XPLAG1_ck;
  private JPanel panel1;
  private JLabel OBJ_31;
  private XRiTextField XPLAG1;
  private XRiTextField XPLAG2;
  private JLabel OBJ_32;
  private JPanel panel3;
  private JXTitledSeparator OBJ_20;
  private JPanel P_SEL0;
  private JLabel OBJ_33;
  private XRiTextField XDEV;
  private JCheckBox XDEV_ck;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
