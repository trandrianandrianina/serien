
package ri.serien.libecranrpg.sgam.SGAMEZFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGAMEZFM_EZ extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", };
  private String[] _T01_Title = { "", };
  private String[][] _T01_Data = { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", }, };
  private int[] _T01_Width = { 283, };
  
  public SGAMEZFM_EZ(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Choix @LIBPAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _T01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WRECHI.setEnabled(lexique.isPresent("WRECHI"));
    panel2.setVisible(lexique.isPresent("WRECHI"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void LIST10MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "X", "ENTER", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OBJ_20 = new JLabel();
    panel1 = new JPanel();
    SCROLLPANE_LIST10 = new JScrollPane();
    T01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel2 = new JPanel();
    WRECHI = new XRiTextField();
    OBJ_24 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(570, 275));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- OBJ_20 ----
        OBJ_20.setText("Choix @LIBPAR@");
        OBJ_20.setName("OBJ_20");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== SCROLLPANE_LIST10 ========
          {
            SCROLLPANE_LIST10.setName("SCROLLPANE_LIST10");

            //---- T01 ----
            T01.setName("T01");
            T01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LIST10MouseClicked(e);
              }
            });
            SCROLLPANE_LIST10.setViewportView(T01);
          }
          panel1.add(SCROLLPANE_LIST10);
          SCROLLPANE_LIST10.setBounds(10, 10, 300, 140);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(315, 10, 25, 65);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(315, 85, 25, 65);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //---- WRECHI ----
          WRECHI.setName("WRECHI");

          //---- OBJ_24 ----
          OBJ_24.setText("Indicatif");
          OBJ_24.setName("OBJ_24");

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(WRECHI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_24))
              .addComponent(WRECHI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(30, 30, 30)
                  .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)))
              .addContainerGap(20, Short.MAX_VALUE))
        );
        p_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {panel1, panel2});
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(9, 9, 9))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JLabel OBJ_20;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST10;
  private XRiTable T01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel2;
  private XRiTextField WRECHI;
  private JLabel OBJ_24;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
