
package ri.serien.libecranrpg.sgam.SGAM40FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snjournal.SNJournal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM511] Gestion des achats -> Opérations périodiques -> Comptabilisation des achats -> Comptabilisation régulière
 * Indicateur : 00100001 (93)
 * Titre : Comptabilisation régulière des achats
 * 
 * [GAM513] Gestion des achats -> Opérations périodiques -> Comptabilisation des achats -> Comptabilisation régulière simulée
 * Indicateur : 00000001
 * Titre : Comptabilisation régulière simulée des achats
 */
public class SGAM40FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SGAM40FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    DEMGLO.setValeursSelection("X", " ");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Indicateur
    Boolean is93 = lexique.isTrue("93");
    
    // Titre
    if (is93) {
      bpPresentation.setText("Comptabilisation régulière des achats");
    }
    else {
      bpPresentation.setText("Comptabilisation régulière simulée des achats");
    }
    
    // Initialise l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charge les composants
    chargerJournaux();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    if (snJournal1.getIdSelection() == null && snJournal2.getIdSelection() == null && snJournal3.getIdSelection() == null
        && snJournal4.getIdSelection() == null && snJournal5.getIdSelection() == null && snJournal6.getIdSelection() == null
        && snJournal7.getIdSelection() == null && snJournal8.getIdSelection() == null && snJournal9.getIdSelection() == null
        && snJournal10.getIdSelection() == null) {
      lexique.HostFieldPutData("CAJD1", 0, "**");
    }
    else {
      snJournal1.renseignerChampRPG(lexique, "CAJD1");
      snJournal2.renseignerChampRPG(lexique, "CAJD2");
      snJournal3.renseignerChampRPG(lexique, "CAJD3");
      snJournal4.renseignerChampRPG(lexique, "CAJD4");
      snJournal5.renseignerChampRPG(lexique, "CAJD5");
      snJournal6.renseignerChampRPG(lexique, "CAJD6");
      snJournal7.renseignerChampRPG(lexique, "CAJD7");
      snJournal8.renseignerChampRPG(lexique, "CAJD8");
      snJournal9.renseignerChampRPG(lexique, "CAJD9");
      snJournal10.renseignerChampRPG(lexique, "CAJD10");
    }
  }
  
  /**
   * Charge les composant de sélection des journaux
   */
  private void chargerJournaux() {
    snJournal1.setSession(getSession());
    snJournal1.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal1.setTousAutorise(true);
    snJournal1.charger(false);
    snJournal1.setSelectionParChampRPG(lexique, "CAJD1");
    
    snJournal2.setSession(getSession());
    snJournal2.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal2.setTousAutorise(false);
    snJournal2.charger(false);
    snJournal2.setSelectionParChampRPG(lexique, "CAJD2");
    
    snJournal3.setSession(getSession());
    snJournal3.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal3.setTousAutorise(false);
    snJournal3.charger(false);
    snJournal3.setSelectionParChampRPG(lexique, "CAJD3");
    
    snJournal4.setSession(getSession());
    snJournal4.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal4.setTousAutorise(false);
    snJournal4.charger(false);
    snJournal4.setSelectionParChampRPG(lexique, "CAJD4");
    
    snJournal5.setSession(getSession());
    snJournal5.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal5.setTousAutorise(false);
    snJournal5.charger(false);
    snJournal5.setSelectionParChampRPG(lexique, "CAJD5");
    
    snJournal6.setSession(getSession());
    snJournal6.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal6.setTousAutorise(false);
    snJournal6.charger(false);
    snJournal6.setSelectionParChampRPG(lexique, "CAJD6");
    
    snJournal7.setSession(getSession());
    snJournal7.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal7.setTousAutorise(false);
    snJournal7.charger(false);
    snJournal7.setSelectionParChampRPG(lexique, "CAJD7");
    
    snJournal8.setSession(getSession());
    snJournal8.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal8.setTousAutorise(false);
    snJournal8.charger(false);
    snJournal8.setSelectionParChampRPG(lexique, "CAJD8");
    
    snJournal9.setSession(getSession());
    snJournal9.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal9.setTousAutorise(false);
    snJournal9.charger(false);
    snJournal9.setSelectionParChampRPG(lexique, "CAJD9");
    
    snJournal10.setSession(getSession());
    snJournal10.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal10.setTousAutorise(false);
    snJournal10.charger(false);
    snJournal10.setSelectionParChampRPG(lexique, "CAJD10");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btRechercheActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCritereSelection = new SNPanelTitre();
    lbComptabilisation = new SNLabelChamp();
    PERIOD = new XRiCalendrier();
    DEMGLO = new XRiCheckBox();
    lbJournal1 = new SNLabelChamp();
    snJournal1 = new SNJournal();
    lbJournal2 = new SNLabelChamp();
    snJournal2 = new SNJournal();
    lbJournal3 = new SNLabelChamp();
    snJournal3 = new SNJournal();
    lbJournal4 = new SNLabelChamp();
    snJournal4 = new SNJournal();
    lbJournal5 = new SNLabelChamp();
    snJournal5 = new SNJournal();
    lbJournal6 = new SNLabelChamp();
    snJournal6 = new SNJournal();
    lbJournal7 = new SNLabelChamp();
    snJournal7 = new SNJournal();
    lbJournal8 = new SNLabelChamp();
    snJournal8 = new SNJournal();
    lbJournal9 = new SNLabelChamp();
    snJournal9 = new SNJournal();
    lbJournal10 = new SNLabelChamp();
    snJournal10 = new SNJournal();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEnCours = new SNTexte();
    lbPeriode = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbEtablissementEnCours = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Comptabilisation r\u00e9guli\u00e8re des achats");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlCritereSelection ========
        {
          pnlCritereSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlCritereSelection.setName("pnlCritereSelection");
          pnlCritereSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlCritereSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCritereSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbComptabilisation ----
          lbComptabilisation.setText("P\u00e9riode de comptabilisation \u00e0 traiter");
          lbComptabilisation.setMinimumSize(new Dimension(230, 30));
          lbComptabilisation.setMaximumSize(new Dimension(230, 30));
          lbComptabilisation.setPreferredSize(new Dimension(230, 30));
          lbComptabilisation.setName("lbComptabilisation");
          pnlCritereSelection.add(lbComptabilisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- PERIOD ----
          PERIOD.setTypeSaisie(6);
          PERIOD.setPreferredSize(new Dimension(90, 30));
          PERIOD.setMinimumSize(new Dimension(90, 30));
          PERIOD.setFont(new Font("sansserif", Font.PLAIN, 14));
          PERIOD.setMaximumSize(new Dimension(90, 30));
          PERIOD.setName("PERIOD");
          pnlCritereSelection.add(PERIOD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- DEMGLO ----
          DEMGLO.setText("Comptabilisation globale");
          DEMGLO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DEMGLO.setFont(new Font("sansserif", Font.PLAIN, 14));
          DEMGLO.setMinimumSize(new Dimension(177, 30));
          DEMGLO.setPreferredSize(new Dimension(177, 30));
          DEMGLO.setName("DEMGLO");
          pnlCritereSelection.add(DEMGLO, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal1 ----
          lbJournal1.setText("Premier journal");
          lbJournal1.setName("lbJournal1");
          pnlCritereSelection.add(lbJournal1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal1 ----
          snJournal1.setName("snJournal1");
          pnlCritereSelection.add(snJournal1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal2 ----
          lbJournal2.setText("Deuxi\u00e8me journal");
          lbJournal2.setName("lbJournal2");
          pnlCritereSelection.add(lbJournal2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal2 ----
          snJournal2.setName("snJournal2");
          pnlCritereSelection.add(snJournal2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal3 ----
          lbJournal3.setText("Troisi\u00e8me journal");
          lbJournal3.setName("lbJournal3");
          pnlCritereSelection.add(lbJournal3, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal3 ----
          snJournal3.setName("snJournal3");
          pnlCritereSelection.add(snJournal3, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal4 ----
          lbJournal4.setText("Quatri\u00e8me journal");
          lbJournal4.setName("lbJournal4");
          pnlCritereSelection.add(lbJournal4, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal4 ----
          snJournal4.setName("snJournal4");
          pnlCritereSelection.add(snJournal4, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal5 ----
          lbJournal5.setText("Cinqui\u00e8me journal");
          lbJournal5.setName("lbJournal5");
          pnlCritereSelection.add(lbJournal5, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal5 ----
          snJournal5.setName("snJournal5");
          pnlCritereSelection.add(snJournal5, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal6 ----
          lbJournal6.setText("Sixi\u00e8me journal");
          lbJournal6.setName("lbJournal6");
          pnlCritereSelection.add(lbJournal6, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal6 ----
          snJournal6.setName("snJournal6");
          pnlCritereSelection.add(snJournal6, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal7 ----
          lbJournal7.setText("Septi\u00e8me journal");
          lbJournal7.setName("lbJournal7");
          pnlCritereSelection.add(lbJournal7, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal7 ----
          snJournal7.setName("snJournal7");
          pnlCritereSelection.add(snJournal7, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal8 ----
          lbJournal8.setText("Huiti\u00e8me journal");
          lbJournal8.setName("lbJournal8");
          pnlCritereSelection.add(lbJournal8, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal8 ----
          snJournal8.setName("snJournal8");
          pnlCritereSelection.add(snJournal8, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal9 ----
          lbJournal9.setText("Neuvi\u00e8me journal");
          lbJournal9.setName("lbJournal9");
          pnlCritereSelection.add(lbJournal9, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snJournal9 ----
          snJournal9.setName("snJournal9");
          pnlCritereSelection.add(snJournal9, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbJournal10 ----
          lbJournal10.setText("Dixi\u00e8me journal");
          lbJournal10.setName("lbJournal10");
          pnlCritereSelection.add(lbJournal10, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snJournal10 ----
          snJournal10.setName("snJournal10");
          pnlCritereSelection.add(snJournal10, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCritereSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEnCours ----
          lbEnCours.setText("@WENCX@");
          lbEnCours.setEnabled(false);
          lbEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbEnCours.setMinimumSize(new Dimension(260, 30));
          lbEnCours.setMaximumSize(new Dimension(260, 30));
          lbEnCours.setPreferredSize(new Dimension(260, 30));
          lbEnCours.setName("lbEnCours");
          pnlEtablissement.add(lbEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- lbPeriode ----
          lbPeriode.setText("P\u00e9riode en cours");
          lbPeriode.setName("lbPeriode");
          pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbEtablissementEnCours ----
          lbEtablissementEnCours.setText("Etablissement en cours");
          lbEtablissementEnCours.setName("lbEtablissementEnCours");
          pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelection;
  private SNLabelChamp lbComptabilisation;
  private XRiCalendrier PERIOD;
  private XRiCheckBox DEMGLO;
  private SNLabelChamp lbJournal1;
  private SNJournal snJournal1;
  private SNLabelChamp lbJournal2;
  private SNJournal snJournal2;
  private SNLabelChamp lbJournal3;
  private SNJournal snJournal3;
  private SNLabelChamp lbJournal4;
  private SNJournal snJournal4;
  private SNLabelChamp lbJournal5;
  private SNJournal snJournal5;
  private SNLabelChamp lbJournal6;
  private SNJournal snJournal6;
  private SNLabelChamp lbJournal7;
  private SNJournal snJournal7;
  private SNLabelChamp lbJournal8;
  private SNJournal snJournal8;
  private SNLabelChamp lbJournal9;
  private SNJournal snJournal9;
  private SNLabelChamp lbJournal10;
  private SNJournal snJournal10;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte lbEnCours;
  private SNLabelChamp lbPeriode;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
