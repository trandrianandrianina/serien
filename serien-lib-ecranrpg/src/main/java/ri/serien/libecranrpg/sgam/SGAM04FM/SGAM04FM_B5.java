
package ri.serien.libecranrpg.sgam.SGAM04FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.sncategoriefournisseur.SNCategorieFournisseur;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM04FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private String BOUTON_EXPORTER = "Exporter";
  
  public SGAM04FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    OPTEDT.setValeursSelection("1", " ");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    LOCTP = LOCTP.getMessageNormal(lexique.HostFieldGetData("LOCTP"));
    lbLOCTP.setMessage(LOCTP);
    
    // Initialise le composant fournisseur
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialise le composant fournisseur 1
    snFournisseur1.setSession(getSession());
    snFournisseur1.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur1.charger(false);
    snFournisseur1.setSelectionParChampRPG(lexique, "DEBFRS");
    
    // Initialise le composant fournisseur 2
    snFournisseur2.setSession(getSession());
    snFournisseur2.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur2.charger(false);
    snFournisseur2.setSelectionParChampRPG(lexique, "FINFRS");
    
    // Initialise le composant devise
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(snEtablissement.getIdSelection());
    snDevise.setTousAutorise(true);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "WDEV");
    
    // Initialise le composant devise
    snCategorieFournisseur.setSession(getSession());
    snCategorieFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieFournisseur.setTousAutorise(true);
    snCategorieFournisseur.charger(false);
    snCategorieFournisseur.setSelectionParChampRPG(lexique, "WCAT");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snFournisseur1.renseignerChampRPG(lexique, "DEBFRS");
    snFournisseur2.renseignerChampRPG(lexique, "FINFRS");
    snDevise.renseignerChampRPG(lexique, "WDEV");
    snCategorieFournisseur.renseignerChampRPG(lexique, "WCAT");
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER)) {
        lexique.HostScreenSendKey(this, "F10");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbFournisseurDebut = new SNLabelChamp();
    snFournisseur1 = new SNFournisseur();
    lbFournisseurFin = new SNLabelChamp();
    snFournisseur2 = new SNFournisseur();
    lbDevis = new SNLabelChamp();
    snDevise = new SNDevise();
    lbCategorieFournisseur = new SNLabelChamp();
    snCategorieFournisseur = new SNCategorieFournisseur();
    lbRegoupementFournisseur = new SNLabelChamp();
    pnlRegroupementFournisseur = new SNPanel();
    DEBFRR = new XRiTextField();
    lbARegroupement = new JLabel();
    FINFRR = new XRiTextField();
    lbMarque = new SNLabelChamp();
    pnlMarque = new SNPanel();
    DEBMRQ = new XRiTextField();
    lbAMarque = new JLabel();
    FINMRQ = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    pnlOptionEdition = new SNPanelTitre();
    OPTEDT = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Edition de la liste des fournisseurs par marque");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbFournisseurDebut ----
            lbFournisseurDebut.setText("Fournisseur de d\u00e9but");
            lbFournisseurDebut.setName("lbFournisseurDebut");
            pnlCritereDeSelection.add(lbFournisseurDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseur1 ----
            snFournisseur1.setName("snFournisseur1");
            pnlCritereDeSelection.add(snFournisseur1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurFin ----
            lbFournisseurFin.setText("Fournisseur de fin");
            lbFournisseurFin.setName("lbFournisseurFin");
            pnlCritereDeSelection.add(lbFournisseurFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseur2 ----
            snFournisseur2.setName("snFournisseur2");
            pnlCritereDeSelection.add(snFournisseur2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDevis ----
            lbDevis.setText("Devise");
            lbDevis.setName("lbDevis");
            pnlCritereDeSelection.add(lbDevis, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snDevise ----
            snDevise.setFont(new Font("sansserif", Font.PLAIN, 14));
            snDevise.setName("snDevise");
            pnlCritereDeSelection.add(snDevise, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCategorieFournisseur ----
            lbCategorieFournisseur.setText("Cat\u00e9gorie fournisseur");
            lbCategorieFournisseur.setName("lbCategorieFournisseur");
            pnlCritereDeSelection.add(lbCategorieFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCategorieFournisseur ----
            snCategorieFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
            snCategorieFournisseur.setName("snCategorieFournisseur");
            pnlCritereDeSelection.add(snCategorieFournisseur, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbRegoupementFournisseur ----
            lbRegoupementFournisseur.setText("Regroupement fournisseur de");
            lbRegoupementFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbRegoupementFournisseur.setMinimumSize(new Dimension(190, 30));
            lbRegoupementFournisseur.setMaximumSize(new Dimension(190, 30));
            lbRegoupementFournisseur.setPreferredSize(new Dimension(190, 30));
            lbRegoupementFournisseur.setName("lbRegoupementFournisseur");
            pnlCritereDeSelection.add(lbRegoupementFournisseur, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlRegroupementFournisseur ========
            {
              pnlRegroupementFournisseur.setName("pnlRegroupementFournisseur");
              pnlRegroupementFournisseur.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRegroupementFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlRegroupementFournisseur.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRegroupementFournisseur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRegroupementFournisseur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- DEBFRR ----
              DEBFRR.setPreferredSize(new Dimension(80, 30));
              DEBFRR.setMinimumSize(new Dimension(80, 30));
              DEBFRR.setMaximumSize(new Dimension(80, 30));
              DEBFRR.setFont(new Font("sansserif", Font.PLAIN, 14));
              DEBFRR.setName("DEBFRR");
              pnlRegroupementFournisseur.add(DEBFRR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbARegroupement ----
              lbARegroupement.setText("\u00e0");
              lbARegroupement.setHorizontalAlignment(SwingConstants.CENTER);
              lbARegroupement.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbARegroupement.setPreferredSize(new Dimension(10, 30));
              lbARegroupement.setMinimumSize(new Dimension(10, 30));
              lbARegroupement.setMaximumSize(new Dimension(10, 30));
              lbARegroupement.setName("lbARegroupement");
              pnlRegroupementFournisseur.add(lbARegroupement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- FINFRR ----
              FINFRR.setMaximumSize(new Dimension(80, 30));
              FINFRR.setMinimumSize(new Dimension(80, 30));
              FINFRR.setPreferredSize(new Dimension(80, 30));
              FINFRR.setFont(new Font("sansserif", Font.PLAIN, 14));
              FINFRR.setName("FINFRR");
              pnlRegroupementFournisseur.add(FINFRR, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlRegroupementFournisseur, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMarque ----
            lbMarque.setText("Marque de");
            lbMarque.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbMarque.setName("lbMarque");
            pnlCritereDeSelection.add(lbMarque, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlMarque ========
            {
              pnlMarque.setName("pnlMarque");
              pnlMarque.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMarque.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlMarque.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMarque.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMarque.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- DEBMRQ ----
              DEBMRQ.setPreferredSize(new Dimension(110, 30));
              DEBMRQ.setMinimumSize(new Dimension(110, 30));
              DEBMRQ.setMaximumSize(new Dimension(110, 30));
              DEBMRQ.setFont(new Font("sansserif", Font.PLAIN, 14));
              DEBMRQ.setName("DEBMRQ");
              pnlMarque.add(DEBMRQ, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAMarque ----
              lbAMarque.setText("\u00e0");
              lbAMarque.setHorizontalAlignment(SwingConstants.CENTER);
              lbAMarque.setPreferredSize(new Dimension(10, 30));
              lbAMarque.setMinimumSize(new Dimension(10, 30));
              lbAMarque.setMaximumSize(new Dimension(10, 30));
              lbAMarque.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbAMarque.setName("lbAMarque");
              pnlMarque.add(lbAMarque, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- FINMRQ ----
              FINMRQ.setPreferredSize(new Dimension(110, 30));
              FINMRQ.setMinimumSize(new Dimension(110, 30));
              FINMRQ.setMaximumSize(new Dimension(110, 30));
              FINMRQ.setFont(new Font("sansserif", Font.PLAIN, 14));
              FINMRQ.setName("FINMRQ");
              pnlMarque.add(FINMRQ, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlMarque, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("p\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEdition ========
          {
            pnlOptionEdition.setTitre("Option d'\u00e9dition");
            pnlOptionEdition.setName("pnlOptionEdition");
            pnlOptionEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- OPTEDT ----
            OPTEDT.setText("Edition simplifi\u00e9e");
            OPTEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPTEDT.setFont(new Font("sansserif", Font.PLAIN, 14));
            OPTEDT.setPreferredSize(new Dimension(128, 30));
            OPTEDT.setMinimumSize(new Dimension(128, 30));
            OPTEDT.setMaximumSize(new Dimension(128, 30));
            OPTEDT.setName("OPTEDT");
            pnlOptionEdition.add(OPTEDT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbFournisseurDebut;
  private SNFournisseur snFournisseur1;
  private SNLabelChamp lbFournisseurFin;
  private SNFournisseur snFournisseur2;
  private SNLabelChamp lbDevis;
  private SNDevise snDevise;
  private SNLabelChamp lbCategorieFournisseur;
  private SNCategorieFournisseur snCategorieFournisseur;
  private SNLabelChamp lbRegoupementFournisseur;
  private SNPanel pnlRegroupementFournisseur;
  private XRiTextField DEBFRR;
  private JLabel lbARegroupement;
  private XRiTextField FINFRR;
  private SNLabelChamp lbMarque;
  private SNPanel pnlMarque;
  private XRiTextField DEBMRQ;
  private JLabel lbAMarque;
  private XRiTextField FINMRQ;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private SNPanelTitre pnlOptionEdition;
  private XRiCheckBox OPTEDT;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
