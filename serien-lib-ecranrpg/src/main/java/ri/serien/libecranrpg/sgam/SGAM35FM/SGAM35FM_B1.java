
package ri.serien.libecranrpg.sgam.SGAM35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM35FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGAM35FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    LIGART.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    REPON2.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMD@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTOU.isSelected());
    OBJ_21.setVisible(lexique.isPresent("LIGART"));
    // LIGART.setSelected(lexique.HostFieldGetData("LIGART").equalsIgnoreCase("OUI"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    // REPON2.setSelected(lexique.HostFieldGetData("REPON2").equalsIgnoreCase("OUI"));
    
    if (lexique.isTrue("N91")) {
      WDEV_CHK.setSelected(lexique.HostFieldGetData("WDEV").trim().equalsIgnoreCase("**"));
      p_Devise.setVisible(!WDEV_CHK.isSelected());
      
      WACH_CHK.setSelected(lexique.HostFieldGetData("WACH").trim().equalsIgnoreCase("**"));
      p_Acheteur.setVisible(!WACH_CHK.isSelected());
    }
    else {
      p_DevAch.setVisible(false);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (LIGART.isSelected())
    // lexique.HostFieldPutData("LIGART", 0, "OUI");
    // else
    // lexique.HostFieldPutData("LIGART", 0, "NON");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    // if (REPON2.isSelected())
    // lexique.HostFieldPutData("REPON2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON2", 0, "NON");
    
    if (WDEV_CHK.isSelected()) {
      lexique.HostFieldPutData("WDEV", 0, "**");
    }
    if (WACH_CHK.isSelected()) {
      lexique.HostFieldPutData("WACH", 0, "**");
    }
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void chk_DeviseActionPerformed(ActionEvent e) {
    p_Devise.setVisible(!p_Devise.isVisible());
    if (p_Devise.isVisible()) {
      WDEV.setText("");
    }
  }
  
  private void chk_AcheteurActionPerformed(ActionEvent e) {
    p_Acheteur.setVisible(!p_Acheteur.isVisible());
    if (p_Acheteur.isVisible()) {
      WACH.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_28 = new RiZoneSortie();
    OBJ_29 = new RiZoneSortie();
    OBJ_22 = new JLabel();
    OBJ_27 = new JLabel();
    DEBFRS = new XRiTextField();
    FINFRS = new XRiTextField();
    REPON2 = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    LIGART = new XRiCheckBox();
    WTOU = new XRiCheckBox();
    p_DevAch = new JPanel();
    WDEV_CHK = new JCheckBox();
    p_Devise = new JPanel();
    OBJ_32 = new JLabel();
    WDEV = new XRiTextField();
    WACH_CHK = new JCheckBox();
    p_Acheteur = new JPanel();
    OBJ_33 = new JLabel();
    WACH = new XRiTextField();
    OBJ_30 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_19 ----
          OBJ_19.setTitle("Num\u00e9ros fournisseurs");
          OBJ_19.setName("OBJ_19");

          //---- OBJ_21 ----
          OBJ_21.setTitle("Options");
          OBJ_21.setName("OBJ_21");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_28 ----
            OBJ_28.setText("@FRNOMD@");
            OBJ_28.setName("OBJ_28");
            P_SEL0.add(OBJ_28);
            OBJ_28.setBounds(255, 11, 273, OBJ_28.getPreferredSize().height);

            //---- OBJ_29 ----
            OBJ_29.setText("@FRNOMF@");
            OBJ_29.setName("OBJ_29");
            P_SEL0.add(OBJ_29);
            OBJ_29.setBounds(255, 42, 273, OBJ_29.getPreferredSize().height);

            //---- OBJ_22 ----
            OBJ_22.setText("Num\u00e9ro de d\u00e9but");
            OBJ_22.setName("OBJ_22");
            P_SEL0.add(OBJ_22);
            OBJ_22.setBounds(10, 14, 122, 18);

            //---- OBJ_27 ----
            OBJ_27.setText("Num\u00e9ro de fin");
            OBJ_27.setName("OBJ_27");
            P_SEL0.add(OBJ_27);
            OBJ_27.setBounds(10, 45, 122, 18);

            //---- DEBFRS ----
            DEBFRS.setComponentPopupMenu(BTD);
            DEBFRS.setName("DEBFRS");
            P_SEL0.add(DEBFRS);
            DEBFRS.setBounds(155, 9, 80, DEBFRS.getPreferredSize().height);

            //---- FINFRS ----
            FINFRS.setComponentPopupMenu(BTD);
            FINFRS.setName("FINFRS");
            P_SEL0.add(FINFRS);
            FINFRS.setBounds(155, 40, 80, FINFRS.getPreferredSize().height);
          }

          //---- REPON2 ----
          REPON2.setText("Edition avec notion d'acheminement");
          REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON2.setName("REPON2");

          //---- REPON1 ----
          REPON1.setText("Saut de page par fournisseurs");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- LIGART ----
          LIGART.setText("D\u00e9tail des lignes articles");
          LIGART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIGART.setName("LIGART");

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //======== p_DevAch ========
          {
            p_DevAch.setOpaque(false);
            p_DevAch.setName("p_DevAch");

            //---- WDEV_CHK ----
            WDEV_CHK.setText("S\u00e9lection compl\u00e8te des devises");
            WDEV_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WDEV_CHK.setName("WDEV_CHK");
            WDEV_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                chk_DeviseActionPerformed(e);
              }
            });

            //======== p_Devise ========
            {
              p_Devise.setOpaque(false);
              p_Devise.setBorder(new BevelBorder(BevelBorder.LOWERED));
              p_Devise.setName("p_Devise");
              p_Devise.setLayout(null);

              //---- OBJ_32 ----
              OBJ_32.setText("Code devise");
              OBJ_32.setName("OBJ_32");
              p_Devise.add(OBJ_32);
              OBJ_32.setBounds(25, 10, 81, 18);

              //---- WDEV ----
              WDEV.setComponentPopupMenu(BTD);
              WDEV.setName("WDEV");
              p_Devise.add(WDEV);
              WDEV.setBounds(180, 5, 40, WDEV.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < p_Devise.getComponentCount(); i++) {
                  Rectangle bounds = p_Devise.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = p_Devise.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                p_Devise.setMinimumSize(preferredSize);
                p_Devise.setPreferredSize(preferredSize);
              }
            }

            //---- WACH_CHK ----
            WACH_CHK.setText("S\u00e9lection compl\u00e8te des acheteurs");
            WACH_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WACH_CHK.setName("WACH_CHK");
            WACH_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                chk_AcheteurActionPerformed(e);
              }
            });

            //======== p_Acheteur ========
            {
              p_Acheteur.setBorder(new BevelBorder(BevelBorder.LOWERED));
              p_Acheteur.setOpaque(false);
              p_Acheteur.setName("p_Acheteur");
              p_Acheteur.setLayout(null);

              //---- OBJ_33 ----
              OBJ_33.setText("Code acheteur");
              OBJ_33.setName("OBJ_33");
              p_Acheteur.add(OBJ_33);
              OBJ_33.setBounds(20, 10, 90, 18);

              //---- WACH ----
              WACH.setComponentPopupMenu(BTD);
              WACH.setName("WACH");
              p_Acheteur.add(WACH);
              WACH.setBounds(150, 5, 40, WACH.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < p_Acheteur.getComponentCount(); i++) {
                  Rectangle bounds = p_Acheteur.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = p_Acheteur.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                p_Acheteur.setMinimumSize(preferredSize);
                p_Acheteur.setPreferredSize(preferredSize);
              }
            }

            //---- OBJ_30 ----
            OBJ_30.setTitle("");
            OBJ_30.setName("OBJ_30");

            GroupLayout p_DevAchLayout = new GroupLayout(p_DevAch);
            p_DevAch.setLayout(p_DevAchLayout);
            p_DevAchLayout.setHorizontalGroup(
              p_DevAchLayout.createParallelGroup()
                .addGroup(p_DevAchLayout.createSequentialGroup()
                  .addGroup(p_DevAchLayout.createParallelGroup()
                    .addGroup(p_DevAchLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addGroup(p_DevAchLayout.createParallelGroup()
                        .addComponent(WDEV_CHK)
                        .addComponent(p_Devise, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                      .addGap(65, 65, 65)
                      .addGroup(p_DevAchLayout.createParallelGroup()
                        .addComponent(WACH_CHK)
                        .addComponent(p_Acheteur, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(p_DevAchLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE)))
                  .addGap(15, 15, 15))
            );
            p_DevAchLayout.setVerticalGroup(
              p_DevAchLayout.createParallelGroup()
                .addGroup(p_DevAchLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addGroup(p_DevAchLayout.createParallelGroup()
                    .addComponent(WDEV_CHK)
                    .addComponent(WACH_CHK))
                  .addGap(7, 7, 7)
                  .addGroup(p_DevAchLayout.createParallelGroup()
                    .addComponent(p_Devise, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(p_Acheteur, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(p_DevAch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(LIGART, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addComponent(p_DevAch, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(LIGART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JXTitledSeparator OBJ_21;
  private JPanel P_SEL0;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_29;
  private JLabel OBJ_22;
  private JLabel OBJ_27;
  private XRiTextField DEBFRS;
  private XRiTextField FINFRS;
  private XRiCheckBox REPON2;
  private XRiCheckBox REPON1;
  private XRiCheckBox LIGART;
  private XRiCheckBox WTOU;
  private JPanel p_DevAch;
  private JCheckBox WDEV_CHK;
  private JPanel p_Devise;
  private JLabel OBJ_32;
  private XRiTextField WDEV;
  private JCheckBox WACH_CHK;
  private JPanel p_Acheteur;
  private JLabel OBJ_33;
  private XRiTextField WACH;
  private JXTitledSeparator OBJ_30;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
