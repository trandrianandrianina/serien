
package ri.serien.libecranrpg.sgam.SGAM3B1F;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGAM3B1F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public SGAM3B1F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    OPTA.setValeursSelection("1", " ");
    OPTI.setValeursSelection("1", " ");
    OPTU.setValeursSelection("1", " ");
    OPTL.setValeursSelection("1", " ");
    OPTD.setValeursSelection("1", " ");
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Composant établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Composant magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setTousAutorise(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@"));
    
    p_bpresentation.setIdEtablissement(snEtablissement.getIdSelection());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG", true);
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snMagasin.setSelection(null);
      setData();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbDateReception = new SNLabelChamp();
    pnlDate = new SNPanel();
    PERDEB = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PERFIN = new XRiCalendrier();
    sNLabelChamp1 = new SNLabelChamp();
    lbCollectifFournisseur = new SNLabelChamp();
    EDOS = new XRiTextField();
    lbCategorieFournisseur = new SNLabelChamp();
    ECNT = new XRiTextField();
    pnlBons = new SNPanelTitre();
    OPTI = new XRiCheckBox();
    OPTU = new XRiCheckBox();
    OPTA = new XRiCheckBox();
    OPTL = new XRiCheckBox();
    OPTD = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());
        
        // ======== pnlMessage ========
        {
          pnlMessage.setName("pnlMessage");
          pnlMessage.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbLOCTP ----
          lbLOCTP.setText("@LOCTP@");
          lbLOCTP.setMinimumSize(new Dimension(120, 30));
          lbLOCTP.setPreferredSize(new Dimension(120, 30));
          lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbLOCTP.setName("lbLOCTP");
          pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlMessage, BorderLayout.NORTH);
        
        // ======== pnlColonne ========
        {
          pnlColonne.setName("pnlColonne");
          pnlColonne.setLayout(new GridLayout());
          
          // ======== pnlGauche ========
          {
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlCritereDeSelection ========
            {
              pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
              pnlCritereDeSelection.setName("pnlCritereDeSelection");
              pnlCritereDeSelection.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbMagasin ----
              lbMagasin.setText("Magasin");
              lbMagasin.setPreferredSize(new Dimension(200, 30));
              lbMagasin.setMinimumSize(new Dimension(200, 30));
              lbMagasin.setMaximumSize(new Dimension(200, 30));
              lbMagasin.setName("lbMagasin");
              pnlCritereDeSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snMagasin ----
              snMagasin.setName("snMagasin");
              pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDateReception ----
              lbDateReception.setText("Dates des r\u00e9ceptions du");
              lbDateReception.setPreferredSize(new Dimension(200, 30));
              lbDateReception.setMinimumSize(new Dimension(200, 30));
              lbDateReception.setMaximumSize(new Dimension(200, 30));
              lbDateReception.setName("lbDateReception");
              pnlCritereDeSelection.add(lbDateReception, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlDate ========
              {
                pnlDate.setName("pnlDate");
                pnlDate.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlDate.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlDate.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlDate.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlDate.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- PERDEB ----
                PERDEB.setPreferredSize(new Dimension(120, 30));
                PERDEB.setMinimumSize(new Dimension(120, 30));
                PERDEB.setMaximumSize(new Dimension(120, 30));
                PERDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
                PERDEB.setName("PERDEB");
                pnlDate.add(PERDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbAu ----
                lbAu.setText("au");
                lbAu.setMaximumSize(new Dimension(30, 30));
                lbAu.setMinimumSize(new Dimension(30, 30));
                lbAu.setPreferredSize(new Dimension(30, 30));
                lbAu.setName("lbAu");
                pnlDate.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- PERFIN ----
                PERFIN.setPreferredSize(new Dimension(120, 30));
                PERFIN.setMinimumSize(new Dimension(120, 30));
                PERFIN.setMaximumSize(new Dimension(120, 30));
                PERFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
                PERFIN.setName("PERFIN");
                pnlDate.add(PERFIN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlCritereDeSelection.add(pnlDate, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp1 ----
              sNLabelChamp1.setText("ou");
              sNLabelChamp1.setName("sNLabelChamp1");
              pnlCritereDeSelection.add(sNLabelChamp1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbCollectifFournisseur ----
              lbCollectifFournisseur.setText("Num\u00e9ro de dossier");
              lbCollectifFournisseur.setPreferredSize(new Dimension(200, 30));
              lbCollectifFournisseur.setMinimumSize(new Dimension(200, 30));
              lbCollectifFournisseur.setMaximumSize(new Dimension(200, 30));
              lbCollectifFournisseur.setName("lbCollectifFournisseur");
              pnlCritereDeSelection.add(lbCollectifFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- EDOS ----
              EDOS.setFont(new Font("sansserif", Font.PLAIN, 14));
              EDOS.setMinimumSize(new Dimension(100, 30));
              EDOS.setMaximumSize(new Dimension(100, 30));
              EDOS.setPreferredSize(new Dimension(100, 30));
              EDOS.setName("EDOS");
              pnlCritereDeSelection.add(EDOS, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCategorieFournisseur ----
              lbCategorieFournisseur.setText("Num\u00e9ro de container");
              lbCategorieFournisseur.setPreferredSize(new Dimension(160, 30));
              lbCategorieFournisseur.setMinimumSize(new Dimension(160, 30));
              lbCategorieFournisseur.setMaximumSize(new Dimension(160, 30));
              lbCategorieFournisseur.setName("lbCategorieFournisseur");
              pnlCritereDeSelection.add(lbCategorieFournisseur, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ECNT ----
              ECNT.setMinimumSize(new Dimension(150, 30));
              ECNT.setMaximumSize(new Dimension(150, 30));
              ECNT.setPreferredSize(new Dimension(150, 30));
              ECNT.setName("ECNT");
              pnlCritereDeSelection.add(ECNT, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlBons ========
            {
              pnlBons.setTitre("Commandes \u00e0 traiter");
              pnlBons.setName("pnlBons");
              pnlBons.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlBons.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlBons.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlBons.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlBons.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- OPTI ----
              OPTI.setText("Commandes internes");
              OPTI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTI.setFont(new Font("sansserif", Font.PLAIN, 14));
              OPTI.setMaximumSize(new Dimension(300, 30));
              OPTI.setMinimumSize(new Dimension(300, 30));
              OPTI.setPreferredSize(new Dimension(300, 30));
              OPTI.setName("OPTI");
              pnlBons.add(OPTI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OPTU ----
              OPTU.setText("Commandes urgentes");
              OPTU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTU.setFont(new Font("sansserif", Font.PLAIN, 14));
              OPTU.setMaximumSize(new Dimension(300, 30));
              OPTU.setMinimumSize(new Dimension(300, 30));
              OPTU.setPreferredSize(new Dimension(300, 30));
              OPTU.setName("OPTU");
              pnlBons.add(OPTU, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OPTA ----
              OPTA.setText("Autres commandes");
              OPTA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTA.setFont(new Font("sansserif", Font.PLAIN, 14));
              OPTA.setMaximumSize(new Dimension(300, 30));
              OPTA.setMinimumSize(new Dimension(300, 30));
              OPTA.setPreferredSize(new Dimension(300, 30));
              OPTA.setName("OPTA");
              pnlBons.add(OPTA, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OPTL ----
              OPTL.setText("Commandes li\u00e9es uniquement");
              OPTL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTL.setFont(new Font("sansserif", Font.PLAIN, 14));
              OPTL.setMaximumSize(new Dimension(300, 30));
              OPTL.setMinimumSize(new Dimension(300, 30));
              OPTL.setPreferredSize(new Dimension(300, 30));
              OPTL.setName("OPTL");
              pnlBons.add(OPTL, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OPTD ----
              OPTD.setText("D\u00e9coupage des commandes et \u00e9dition d'un \u00e9tat de pr\u00e9paration par commande livrable");
              OPTD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPTD.setFont(new Font("sansserif", Font.PLAIN, 14));
              OPTD.setMaximumSize(new Dimension(300, 30));
              OPTD.setMinimumSize(new Dimension(300, 30));
              OPTD.setPreferredSize(new Dimension(300, 30));
              OPTD.setName("OPTD");
              pnlBons.add(OPTD, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche.add(pnlBons, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlColonne.add(pnlGauche);
          
          // ======== pnlDroite ========
          {
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlEtablissement ========
            {
              pnlEtablissement.setTitre("Etablissement");
              pnlEtablissement.setName("pnlEtablissement");
              pnlEtablissement.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- lbEtablissementEnCours ----
              lbEtablissementEnCours.setText("Etablissement en cours");
              lbEtablissementEnCours.setName("lbEtablissementEnCours");
              pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snEtablissement ----
              snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
              snEtablissement.setName("snEtablissement");
              snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snEtablissementValueChanged(e);
                }
              });
              pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbPeriodeEnCours ----
              lbPeriodeEnCours.setText("P\u00e9riode en cours");
              lbPeriodeEnCours.setName("lbPeriodeEnCours");
              pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfPeriodeEnCours ----
              tfPeriodeEnCours.setText("@WENCX@");
              tfPeriodeEnCours.setEnabled(false);
              tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
              tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
              tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
              tfPeriodeEnCours.setName("tfPeriodeEnCours");
              pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlColonne.add(pnlDroite);
        }
        pnlContenu.add(pnlColonne, BorderLayout.CENTER);
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlFond, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbDateReception;
  private SNPanel pnlDate;
  private XRiCalendrier PERDEB;
  private SNLabelChamp lbAu;
  private XRiCalendrier PERFIN;
  private SNLabelChamp sNLabelChamp1;
  private SNLabelChamp lbCollectifFournisseur;
  private XRiTextField EDOS;
  private SNLabelChamp lbCategorieFournisseur;
  private XRiTextField ECNT;
  private SNPanelTitre pnlBons;
  private XRiCheckBox OPTI;
  private XRiCheckBox OPTU;
  private XRiCheckBox OPTA;
  private XRiCheckBox OPTL;
  private XRiCheckBox OPTD;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
