
package ri.serien.libecranrpg.vgem.VGEM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM06FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM06FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX7.setValeurs("7", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX1.setValeurs("1", "RB");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // RB5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // RB3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // RB4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // RB1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // RB7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // RB6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // RB2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Constitution de remise"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (RB5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (RB3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (RB4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (RB1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (RB7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (RB6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (RB2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX2 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    TIDX6 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG5 = new XRiTextField();
    ARG6 = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG22 = new XRiTextField();
    ARG11 = new XRiTextField();
    ARG7 = new XRiTextField();
    ARG31 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG23 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(620, 320));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- TIDX2 ----
          TIDX2.setText("Identification de remise");
          TIDX2.setToolTipText("Tri\u00e9 par");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(BTD);
          ARG4.setName("ARG4");

          //---- TIDX6 ----
          TIDX6.setText("Num\u00e9ro de compte");
          TIDX6.setToolTipText("Tri\u00e9 par");
          TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX6.setName("TIDX6");

          //---- TIDX7 ----
          TIDX7.setText("Date d'\u00e9ch\u00e9ance");
          TIDX7.setToolTipText("Tri\u00e9 par");
          TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX7.setName("TIDX7");

          //---- TIDX1 ----
          TIDX1.setText("Num\u00e9ro d'effet");
          TIDX1.setToolTipText("Tri\u00e9 par");
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setName("TIDX1");

          //---- TIDX4 ----
          TIDX4.setText("Nom du client");
          TIDX4.setToolTipText("Tri\u00e9 par");
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setName("TIDX4");

          //---- TIDX3 ----
          TIDX3.setText("Num\u00e9ro client");
          TIDX3.setToolTipText("Tri\u00e9 par");
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setName("TIDX3");

          //---- ARG5 ----
          ARG5.setComponentPopupMenu(BTD);
          ARG5.setName("ARG5");

          //---- ARG6 ----
          ARG6.setComponentPopupMenu(BTD);
          ARG6.setName("ARG6");

          //---- TIDX5 ----
          TIDX5.setText("Montant");
          TIDX5.setToolTipText("Tri\u00e9 par");
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setName("TIDX5");

          //---- ARG22 ----
          ARG22.setComponentPopupMenu(BTD);
          ARG22.setName("ARG22");

          //---- ARG11 ----
          ARG11.setComponentPopupMenu(BTD);
          ARG11.setName("ARG11");

          //---- ARG7 ----
          ARG7.setComponentPopupMenu(BTD);
          ARG7.setName("ARG7");

          //---- ARG31 ----
          ARG31.setComponentPopupMenu(BTD);
          ARG31.setName("ARG31");

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BTD);
          ARG2.setName("ARG2");

          //---- ARG23 ----
          ARG23.setComponentPopupMenu(BTD);
          ARG23.setName("ARG23");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG11, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(ARG22, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG31, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(ARG23, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG5, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX6, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX7, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG7, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(ARG22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(ARG23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX7, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 430, 300);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG5;
  private XRiTextField ARG6;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG22;
  private XRiTextField ARG11;
  private XRiTextField ARG7;
  private XRiTextField ARG31;
  private XRiTextField ARG2;
  private XRiTextField ARG23;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
