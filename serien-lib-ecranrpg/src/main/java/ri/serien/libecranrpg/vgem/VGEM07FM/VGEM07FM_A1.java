
package ri.serien.libecranrpg.vgem.VGEM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEM07FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM07FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX5.setValeurs("5", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX2.setValeurs("2", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    ARG23.setEnabled(lexique.isPresent("ARG23"));
    INDSUF.setEnabled(lexique.isPresent("INDSUF"));
    ARG241.setEnabled(lexique.isPresent("ARG241"));
    ARG2.setEnabled(lexique.isPresent("ARG2"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    ARG31.setEnabled(lexique.isPresent("ARG31"));
    INDNUM.setEnabled(lexique.isPresent("INDNUM"));
    ARG11.setEnabled(lexique.isPresent("ARG11"));
    ARG22.setEnabled(lexique.isPresent("ARG22"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    ARG6.setEnabled(lexique.isPresent("ARG6"));
    ARG5.setEnabled(lexique.isPresent("ARG5"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    ARG4.setEnabled(lexique.isPresent("ARG4"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    
    barre_dup.setVisible(lexique.isTrue("56"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40_OBJ_40 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_42_OBJ_42 = new JLabel();
    INDNUM = new XRiTextField();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    barre_dup = new JMenuBar();
    p_dup_gauche = new JPanel();
    OBJ_40_OBJ_41 = new JLabel();
    IN3ETB = new XRiTextField();
    OBJ_42_OBJ_43 = new JLabel();
    IN3NUM = new XRiTextField();
    IN3SUF = new XRiTextField();
    p_dup_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    TIDX2 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    TIDX1 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG5 = new XRiTextField();
    ARG6 = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG22 = new XRiTextField();
    ARG11 = new XRiTextField();
    ARG31 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG241 = new XRiTextField();
    ARG23 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des pr\u00e9l\u00e8vements");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Etablissement");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("Num\u00e9ro");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");

        //======== p_dup_gauche ========
        {
          p_dup_gauche.setOpaque(false);
          p_dup_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_dup_gauche.setPreferredSize(new Dimension(700, 32));
          p_dup_gauche.setMinimumSize(new Dimension(700, 32));
          p_dup_gauche.setName("p_dup_gauche");

          //---- OBJ_40_OBJ_41 ----
          OBJ_40_OBJ_41.setText("Par duplication de");
          OBJ_40_OBJ_41.setName("OBJ_40_OBJ_41");

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD);
          IN3ETB.setName("IN3ETB");

          //---- OBJ_42_OBJ_43 ----
          OBJ_42_OBJ_43.setText("Num\u00e9ro");
          OBJ_42_OBJ_43.setName("OBJ_42_OBJ_43");

          //---- IN3NUM ----
          IN3NUM.setComponentPopupMenu(BTD);
          IN3NUM.setName("IN3NUM");

          //---- IN3SUF ----
          IN3SUF.setComponentPopupMenu(BTD);
          IN3SUF.setName("IN3SUF");

          GroupLayout p_dup_gaucheLayout = new GroupLayout(p_dup_gauche);
          p_dup_gauche.setLayout(p_dup_gaucheLayout);
          p_dup_gaucheLayout.setHorizontalGroup(
            p_dup_gaucheLayout.createParallelGroup()
              .addGroup(p_dup_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40_OBJ_41, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_42_OBJ_43, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(IN3NUM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(IN3SUF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_dup_gaucheLayout.setVerticalGroup(
            p_dup_gaucheLayout.createParallelGroup()
              .addGroup(p_dup_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40_OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_dup_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42_OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(IN3NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(IN3SUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_dup.add(p_dup_gauche);

        //======== p_dup_droite ========
        {
          p_dup_droite.setOpaque(false);
          p_dup_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_dup_droite.setPreferredSize(new Dimension(150, 0));
          p_dup_droite.setMinimumSize(new Dimension(150, 0));
          p_dup_droite.setName("p_dup_droite");
          p_dup_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_dup.add(p_dup_droite);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(500, 300));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- TIDX2 ----
            TIDX2.setText("Identification de remise");
            TIDX2.setToolTipText("Tri\u00e9 par");
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");

            //---- TIDX6 ----
            TIDX6.setText("Num\u00e9ro de compte");
            TIDX6.setToolTipText("Tri\u00e9 par");
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");

            //---- ARG4 ----
            ARG4.setComponentPopupMenu(BTD);
            ARG4.setName("ARG4");

            //---- TIDX1 ----
            TIDX1.setText("Num\u00e9ro d'effet");
            TIDX1.setToolTipText("Tri\u00e9 par");
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");

            //---- TIDX4 ----
            TIDX4.setText("Nom du client");
            TIDX4.setToolTipText("Tri\u00e9 par");
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");

            //---- TIDX3 ----
            TIDX3.setText("Num\u00e9ro client");
            TIDX3.setToolTipText("Tri\u00e9 par");
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");

            //---- ARG5 ----
            ARG5.setComponentPopupMenu(BTD);
            ARG5.setName("ARG5");

            //---- ARG6 ----
            ARG6.setComponentPopupMenu(BTD);
            ARG6.setName("ARG6");

            //---- TIDX5 ----
            TIDX5.setText("Montant");
            TIDX5.setToolTipText("Tri\u00e9 par");
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");

            //---- ARG22 ----
            ARG22.setComponentPopupMenu(BTD);
            ARG22.setName("ARG22");

            //---- ARG11 ----
            ARG11.setComponentPopupMenu(BTD);
            ARG11.setName("ARG11");

            //---- ARG31 ----
            ARG31.setComponentPopupMenu(BTD);
            ARG31.setName("ARG31");

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(BTD);
            ARG2.setName("ARG2");

            //---- ARG241 ----
            ARG241.setComponentPopupMenu(BTD);
            ARG241.setName("ARG241");

            //---- ARG23 ----
            ARG23.setComponentPopupMenu(BTD);
            ARG23.setName("ARG23");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addContainerGap()
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ARG11, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(ARG22, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(ARG23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                      .addComponent(ARG241, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ARG31, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ARG5, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(TIDX6, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap())
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addContainerGap()
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ARG11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(ARG23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ARG241, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ARG31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ARG5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(TIDX6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(28, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField INDETB;
  private JLabel OBJ_42_OBJ_42;
  private XRiTextField INDNUM;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JMenuBar barre_dup;
  private JPanel p_dup_gauche;
  private JLabel OBJ_40_OBJ_41;
  private XRiTextField IN3ETB;
  private JLabel OBJ_42_OBJ_43;
  private XRiTextField IN3NUM;
  private XRiTextField IN3SUF;
  private JPanel p_dup_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG5;
  private XRiTextField ARG6;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG22;
  private XRiTextField ARG11;
  private XRiTextField ARG31;
  private XRiTextField ARG2;
  private XRiTextField ARG241;
  private XRiTextField ARG23;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
