
package ri.serien.libecranrpg.vgem.VGEM16FM;
// Nom Fichier: i_VGEM16FM_FMTA2_FMTF1_62.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGEM16FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 541, };
  
  public VGEM16FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CHOISIR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+1=@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+2=@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+3=@")).trim());
    OBJ_13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+4=@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+5=@")).trim());
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+6=@")).trim());
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+7=@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+8=@")).trim());
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@ @LIBPG@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WMTTX.setEnabled(lexique.isPresent("WMTTX"));
    OBJ_37.setEnabled(lexique.isPresent("WRECH1"));
    OBJ_36.setEnabled(lexique.isPresent("WRECH1"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ @LIBPG@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgem16"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WRECH1", 0, "D");
    
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WRECH1", 0, "F");
    
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_V01F = new JLabel();
    OBJ_25_OBJ_25 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_63_OBJ_63 = new JLabel();
    L16NCG = new XRiTextField();
    L16NCA = new XRiTextField();
    P_Centre = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LD01 = new XRiTable();
    OBJ_36 = new JButton();
    OBJ_37 = new JButton();
    WMTTX = new XRiTextField();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    E1TTCX = new XRiTextField();
    OBJ_28_OBJ_28 = new JLabel();
    INDNUM = new XRiTextField();
    L16NOM = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_63 = new JButton();
    OBJ_68 = new JButton();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("@FONCTION/+1=@");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_11 ----
      OBJ_11.setText("@FONCTION/+2=@");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("@FONCTION/+3=@");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("@FONCTION/+4=@");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("@FONCTION/+5=@");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("@FONCTION/+6=@");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("@FONCTION/+7=@");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_17 ----
      OBJ_17.setText("@FONCTION/+8=@");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
      BTD.addSeparator();

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITRE@ @LIBPG@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");
        P_Infos.setLayout(null);

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });
        P_Infos.add(bt_Fonctions);
        bt_Fonctions.setBounds(883, 5, 115, bt_Fonctions.getPreferredSize().height);

        //---- l_V01F ----
        l_V01F.setText("@V01F@");
        l_V01F.setFont(new Font("sansserif", Font.BOLD, 12));
        l_V01F.setName("l_V01F");
        P_Infos.add(l_V01F);
        l_V01F.setBounds(21, 9, 80, 20);

        //---- OBJ_25_OBJ_25 ----
        OBJ_25_OBJ_25.setText("Etablissement");
        OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
        P_Infos.add(OBJ_25_OBJ_25);
        OBJ_25_OBJ_25.setBounds(100, 10, 93, 16);

        //---- INDETB ----
        INDETB.setName("INDETB");
        P_Infos.add(INDETB);
        INDETB.setBounds(185, 5, 40, INDETB.getPreferredSize().height);

        //---- OBJ_63_OBJ_63 ----
        OBJ_63_OBJ_63.setText("N\u00b0 client");
        OBJ_63_OBJ_63.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
        P_Infos.add(OBJ_63_OBJ_63);
        OBJ_63_OBJ_63.setBounds(250, 10, 48, 16);

        //---- L16NCG ----
        L16NCG.setName("L16NCG");
        P_Infos.add(L16NCG);
        L16NCG.setBounds(315, 5, 60, L16NCG.getPreferredSize().height);

        //---- L16NCA ----
        L16NCA.setName("L16NCA");
        P_Infos.add(L16NCA);
        L16NCA.setBounds(375, 5, 60, L16NCA.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < P_Infos.getComponentCount(); i++) {
            Rectangle bounds = P_Infos.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = P_Infos.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          P_Infos.setMinimumSize(preferredSize);
          P_Infos.setPreferredSize(preferredSize);
        }
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== SCROLLPANE_LIST ========
      {
        SCROLLPANE_LIST.setComponentPopupMenu(BTD);
        SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

        //---- LD01 ----
        LD01.setName("LD01");
        SCROLLPANE_LIST.setViewportView(LD01);
      }
      P_Centre.add(SCROLLPANE_LIST);
      SCROLLPANE_LIST.setBounds(35, 80, 558, 265);

      //---- OBJ_36 ----
      OBJ_36.setText("D\u00e9but");
      OBJ_36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_36);
      OBJ_36.setBounds(415, 355, 82, 24);

      //---- OBJ_37 ----
      OBJ_37.setText("Fin");
      OBJ_37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_37);
      OBJ_37.setBounds(510, 355, 82, 24);

      //---- WMTTX ----
      WMTTX.setName("WMTTX");
      P_Centre.add(WMTTX);
      WMTTX.setBounds(120, 350, 108, WMTTX.getPreferredSize().height);

      //---- OBJ_65_OBJ_65 ----
      OBJ_65_OBJ_65.setText("Montant total");
      OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");
      P_Centre.add(OBJ_65_OBJ_65);
      OBJ_65_OBJ_65.setBounds(40, 355, 78, 20);

      //---- OBJ_64_OBJ_64 ----
      OBJ_64_OBJ_64.setText("Montant");
      OBJ_64_OBJ_64.setHorizontalAlignment(SwingConstants.RIGHT);
      OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
      P_Centre.add(OBJ_64_OBJ_64);
      OBJ_64_OBJ_64.setBounds(30, 20, 44, 16);

      //---- E1TTCX ----
      E1TTCX.setName("E1TTCX");
      P_Centre.add(E1TTCX);
      E1TTCX.setBounds(80, 15, 108, E1TTCX.getPreferredSize().height);

      //---- OBJ_28_OBJ_28 ----
      OBJ_28_OBJ_28.setText("Num\u00e9ro de pi\u00e8ce");
      OBJ_28_OBJ_28.setHorizontalAlignment(SwingConstants.RIGHT);
      OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
      P_Centre.add(OBJ_28_OBJ_28);
      OBJ_28_OBJ_28.setBounds(190, 20, 111, 18);

      //---- INDNUM ----
      INDNUM.setName("INDNUM");
      P_Centre.add(INDNUM);
      INDNUM.setBounds(315, 15, 68, INDNUM.getPreferredSize().height);

      //---- L16NOM ----
      L16NOM.setName("L16NOM");
      P_Centre.add(L16NOM);
      L16NOM.setBounds(385, 15, 204, L16NOM.getPreferredSize().height);

      //---- xTitledSeparator1 ----
      xTitledSeparator1.setTitle("Liste r\u00e8glement(s)");
      xTitledSeparator1.setName("xTitledSeparator1");
      P_Centre.add(xTitledSeparator1);
      xTitledSeparator1.setBounds(20, 55, 610, xTitledSeparator1.getPreferredSize().height);

      //---- OBJ_63 ----
      OBJ_63.setText("");
      OBJ_63.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_63.setName("OBJ_63");
      OBJ_63.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_62ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_63);
      OBJ_63.setBounds(600, 80, 25, 125);

      //---- OBJ_68 ----
      OBJ_68.setText("");
      OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_68.setName("OBJ_68");
      OBJ_68.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_68ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_68);
      OBJ_68.setBounds(600, 220, 25, 125);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_V01F;
  private JLabel OBJ_25_OBJ_25;
  private XRiTextField INDETB;
  private JLabel OBJ_63_OBJ_63;
  private XRiTextField L16NCG;
  private XRiTextField L16NCA;
  private JPanel P_Centre;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LD01;
  private JButton OBJ_36;
  private JButton OBJ_37;
  private XRiTextField WMTTX;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField E1TTCX;
  private JLabel OBJ_28_OBJ_28;
  private XRiTextField INDNUM;
  private XRiTextField L16NOM;
  private JXTitledSeparator xTitledSeparator1;
  private JButton OBJ_63;
  private JButton OBJ_68;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
