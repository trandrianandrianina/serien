
package ri.serien.libecranrpg.vgem.VGEM12FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM12FM_OT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM12FM_OT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("Caisse"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_22_OBJ_22 = new JLabel();
    TOT1 = new XRiTextField();
    TOT2 = new XRiTextField();
    TOT3 = new XRiTextField();
    TOT4 = new XRiTextField();
    TOT5 = new XRiTextField();
    TOT6 = new XRiTextField();
    TOT7 = new XRiTextField();
    OBJ_34_OBJ_34 = new JLabel();
    OBJ_18_OBJ_18 = new JLabel();
    OBJ_24_OBJ_24 = new JLabel();
    OBJ_26_OBJ_26 = new JLabel();
    DRGX = new XRiCalendrier();
    OBJ_32_OBJ_32 = new JLabel();
    WVDE = new XRiTextField();
    OBJ_20_OBJ_20 = new JLabel();
    OBJ_30_OBJ_30 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(490, 345));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Affichage caisse"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("Cartes bancaires");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          panel1.add(OBJ_28_OBJ_28);
          OBJ_28_OBJ_28.setBounds(15, 193, 145, 20);

          //---- OBJ_22_OBJ_22 ----
          OBJ_22_OBJ_22.setText("Fond de caisse");
          OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
          panel1.add(OBJ_22_OBJ_22);
          OBJ_22_OBJ_22.setBounds(15, 109, 145, 20);

          //---- TOT1 ----
          TOT1.setName("TOT1");
          panel1.add(TOT1);
          TOT1.setBounds(160, 105, 132, TOT1.getPreferredSize().height);

          //---- TOT2 ----
          TOT2.setName("TOT2");
          panel1.add(TOT2);
          TOT2.setBounds(160, 133, 132, TOT2.getPreferredSize().height);

          //---- TOT3 ----
          TOT3.setName("TOT3");
          panel1.add(TOT3);
          TOT3.setBounds(160, 161, 132, TOT3.getPreferredSize().height);

          //---- TOT4 ----
          TOT4.setName("TOT4");
          panel1.add(TOT4);
          TOT4.setBounds(160, 189, 132, TOT4.getPreferredSize().height);

          //---- TOT5 ----
          TOT5.setName("TOT5");
          panel1.add(TOT5);
          TOT5.setBounds(160, 217, 132, TOT5.getPreferredSize().height);

          //---- TOT6 ----
          TOT6.setName("TOT6");
          panel1.add(TOT6);
          TOT6.setBounds(160, 245, 132, TOT6.getPreferredSize().height);

          //---- TOT7 ----
          TOT7.setFont(TOT7.getFont().deriveFont(TOT7.getFont().getStyle() | Font.BOLD));
          TOT7.setName("TOT7");
          panel1.add(TOT7);
          TOT7.setBounds(160, 290, 132, TOT7.getPreferredSize().height);

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("Total Journ\u00e9e");
          OBJ_34_OBJ_34.setFont(OBJ_34_OBJ_34.getFont().deriveFont(OBJ_34_OBJ_34.getFont().getStyle() | Font.BOLD));
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
          panel1.add(OBJ_34_OBJ_34);
          OBJ_34_OBJ_34.setBounds(15, 294, 145, 20);

          //---- OBJ_18_OBJ_18 ----
          OBJ_18_OBJ_18.setText("Caisse au");
          OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");
          panel1.add(OBJ_18_OBJ_18);
          OBJ_18_OBJ_18.setBounds(15, 35, 145, 20);

          //---- OBJ_24_OBJ_24 ----
          OBJ_24_OBJ_24.setText("Ch\u00e8ques");
          OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");
          panel1.add(OBJ_24_OBJ_24);
          OBJ_24_OBJ_24.setBounds(15, 137, 145, 20);

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("Esp\u00e8ces");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
          panel1.add(OBJ_26_OBJ_26);
          OBJ_26_OBJ_26.setBounds(15, 165, 145, 20);

          //---- DRGX ----
          DRGX.setName("DRGX");
          panel1.add(DRGX);
          DRGX.setBounds(160, 31, 105, DRGX.getPreferredSize().height);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Retraits");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel1.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(15, 249, 145, 20);

          //---- WVDE ----
          WVDE.setName("WVDE");
          panel1.add(WVDE);
          WVDE.setBounds(160, 60, 40, WVDE.getPreferredSize().height);

          //---- OBJ_20_OBJ_20 ----
          OBJ_20_OBJ_20.setText("pour");
          OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");
          panel1.add(OBJ_20_OBJ_20);
          OBJ_20_OBJ_20.setBounds(15, 64, 145, 20);

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("T.I.P");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
          panel1.add(OBJ_30_OBJ_30);
          OBJ_30_OBJ_30.setBounds(15, 221, 145, 20);
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 310, 335);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_28_OBJ_28;
  private JLabel OBJ_22_OBJ_22;
  private XRiTextField TOT1;
  private XRiTextField TOT2;
  private XRiTextField TOT3;
  private XRiTextField TOT4;
  private XRiTextField TOT5;
  private XRiTextField TOT6;
  private XRiTextField TOT7;
  private JLabel OBJ_34_OBJ_34;
  private JLabel OBJ_18_OBJ_18;
  private JLabel OBJ_24_OBJ_24;
  private JLabel OBJ_26_OBJ_26;
  private XRiCalendrier DRGX;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField WVDE;
  private JLabel OBJ_20_OBJ_20;
  private JLabel OBJ_30_OBJ_30;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
