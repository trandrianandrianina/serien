
package ri.serien.libecranrpg.vgem.VGEM16FM;
// Nom Fichier: pop_VGEM16FM_FMTC1_FMTF1_63.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM16FM_C1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM16FM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_34);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_39_OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSF01@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WTCA.setEnabled(lexique.isPresent("WTCA"));
    WANVR.setEnabled(lexique.isPresent("WANVR"));
    WMOV.setEnabled(lexique.isPresent("WMOV"));
    CHVDE.setEnabled(lexique.isPresent("CHVDE"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    INDNUM.setEnabled(lexique.isPresent("INDNUM"));
    XMTT.setEnabled(lexique.isPresent("XMTT"));
    WCAN.setEnabled(lexique.isPresent("WCAN"));
    OBJ_39_OBJ_39.setVisible(lexique.isPresent("MSF01"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgem16"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_39_OBJ_39 = new JLabel();
    WCAN = new XRiTextField();
    OBJ_34 = new JButton();
    XMTT = new XRiTextField();
    OBJ_28_OBJ_28 = new JLabel();
    INDNUM = new XRiTextField();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    CHVDE = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_24_OBJ_24 = new JLabel();
    WMOV = new XRiTextField();
    WANVR = new XRiTextField();
    WTCA = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_39_OBJ_39 ----
    OBJ_39_OBJ_39.setText("@MSF01@");
    OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
    add(OBJ_39_OBJ_39);
    OBJ_39_OBJ_39.setBounds(15, 118, 442, 20);

    //---- WCAN ----
    WCAN.setComponentPopupMenu(BTD);
    WCAN.setName("WCAN");
    add(WCAN);
    WCAN.setBounds(70, 80, 164, WCAN.getPreferredSize().height);

    //---- OBJ_34 ----
    OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_34.setName("OBJ_34");
    OBJ_34.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_34ActionPerformed(e);
      }
    });
    add(OBJ_34);
    OBJ_34.setBounds(405, 135, 56, 40);

    //---- XMTT ----
    XMTT.setName("XMTT");
    add(XMTT);
    XMTT.setBounds(185, 20, 108, XMTT.getPreferredSize().height);

    //---- OBJ_28_OBJ_28 ----
    OBJ_28_OBJ_28.setText("Vendeur");
    OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
    add(OBJ_28_OBJ_28);
    OBJ_28_OBJ_28.setBounds(300, 25, 54, 20);

    //---- INDNUM ----
    INDNUM.setName("INDNUM");
    add(INDNUM);
    INDNUM.setBounds(115, 20, 68, INDNUM.getPreferredSize().height);

    //---- OBJ_36_OBJ_36 ----
    OBJ_36_OBJ_36.setText("Num\u00e9ro");
    OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
    add(OBJ_36_OBJ_36);
    OBJ_36_OBJ_36.setBounds(15, 84, 51, 20);

    //---- OBJ_38_OBJ_38 ----
    OBJ_38_OBJ_38.setText("Validit\u00e9");
    OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
    add(OBJ_38_OBJ_38);
    OBJ_38_OBJ_38.setBounds(327, 84, 48, 20);

    //---- OBJ_42_OBJ_42 ----
    OBJ_42_OBJ_42.setText("MM/AA");
    OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
    add(OBJ_42_OBJ_42);
    OBJ_42_OBJ_42.setBounds(380, 65, 48, 20);

    //---- INDETB ----
    INDETB.setName("INDETB");
    add(INDETB);
    INDETB.setBounds(75, 20, 40, INDETB.getPreferredSize().height);

    //---- CHVDE ----
    CHVDE.setComponentPopupMenu(BTD);
    CHVDE.setName("CHVDE");
    add(CHVDE);
    CHVDE.setBounds(355, 20, 40, CHVDE.getPreferredSize().height);

    //---- OBJ_37_OBJ_37 ----
    OBJ_37_OBJ_37.setText("Type");
    OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
    add(OBJ_37_OBJ_37);
    OBJ_37_OBJ_37.setBounds(250, 84, 36, 20);

    //---- OBJ_24_OBJ_24 ----
    OBJ_24_OBJ_24.setText("Cde");
    OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");
    add(OBJ_24_OBJ_24);
    OBJ_24_OBJ_24.setBounds(15, 25, 28, 20);

    //---- WMOV ----
    WMOV.setComponentPopupMenu(BTD);
    WMOV.setName("WMOV");
    add(WMOV);
    WMOV.setBounds(378, 80, 30, WMOV.getPreferredSize().height);

    //---- WANVR ----
    WANVR.setComponentPopupMenu(BTD);
    WANVR.setName("WANVR");
    add(WANVR);
    WANVR.setBounds(410, 80, 30, WANVR.getPreferredSize().height);

    //---- WTCA ----
    WTCA.setComponentPopupMenu(BTD);
    WTCA.setName("WTCA");
    add(WTCA);
    WTCA.setBounds(289, 80, 20, WTCA.getPreferredSize().height);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Carte");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 55, 445, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(472, 180));

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField WCAN;
  private JButton OBJ_34;
  private XRiTextField XMTT;
  private JLabel OBJ_28_OBJ_28;
  private XRiTextField INDNUM;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_38_OBJ_38;
  private JLabel OBJ_42_OBJ_42;
  private XRiTextField INDETB;
  private XRiTextField CHVDE;
  private JLabel OBJ_37_OBJ_37;
  private JLabel OBJ_24_OBJ_24;
  private XRiTextField WMOV;
  private XRiTextField WANVR;
  private XRiTextField WTCA;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
