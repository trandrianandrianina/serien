
package ri.serien.libecranrpg.vgem.VGEM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VGEM05FM_IM extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM05FM_IM(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("GESTION DES EFFETS"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_5_OBJ_5 = new JLabel();
    OBJ_7_OBJ_7 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(330, 130));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_5_OBJ_5 ----
          OBJ_5_OBJ_5.setText("Enregistrement d'un impay\u00e9");
          OBJ_5_OBJ_5.setFont(OBJ_5_OBJ_5.getFont().deriveFont(OBJ_5_OBJ_5.getFont().getStyle() | Font.BOLD, OBJ_5_OBJ_5.getFont().getSize() + 3f));
          OBJ_5_OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_5_OBJ_5.setName("OBJ_5_OBJ_5");
          p_recup.add(OBJ_5_OBJ_5);
          OBJ_5_OBJ_5.setBounds(0, 25, 320, 22);

          //---- OBJ_7_OBJ_7 ----
          OBJ_7_OBJ_7.setText("Merci de patienter quelques instants...");
          OBJ_7_OBJ_7.setFont(OBJ_7_OBJ_7.getFont().deriveFont(OBJ_7_OBJ_7.getFont().getStyle() | Font.BOLD, OBJ_7_OBJ_7.getFont().getSize() + 3f));
          OBJ_7_OBJ_7.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_7_OBJ_7.setName("OBJ_7_OBJ_7");
          p_recup.add(OBJ_7_OBJ_7);
          OBJ_7_OBJ_7.setBounds(0, 60, 320, 22);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(5, 5, 320, 120);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_5_OBJ_5;
  private JLabel OBJ_7_OBJ_7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
