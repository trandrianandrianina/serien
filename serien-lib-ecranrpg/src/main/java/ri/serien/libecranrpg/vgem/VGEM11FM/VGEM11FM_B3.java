
package ri.serien.libecranrpg.vgem.VGEM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM11FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGEM11FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_28_OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("GESTION"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new SNPanelContenu();
    sNLabelTitre1 = new SNLabelTitre();
    xTitledPanel1 = new SNPanelTitre();
    INDETB = new XRiTextField();
    CHVDE = new XRiTextField();
    CHMTT = new XRiTextField();
    CHDRGX = new XRiCalendrier();
    OBJ_27_OBJ_27 = new SNLabelChamp();
    OBJ_28_OBJ_28 = new SNLabelChamp();
    OBJ_42_OBJ_42 = new SNLabelChamp();
    OBJ_44_OBJ_44 = new SNLabelChamp();
    OBJ_29_OBJ_29 = new SNLabelChamp();
    OBJ_31_OBJ_31 = new SNLabelChamp();
    INDNUM = new XRiTextField();
    OBJ_34_OBJ_34 = new SNLabelChamp();
    OBJ_36_OBJ_36 = new SNLabelChamp();
    OBJ_38_OBJ_38 = new SNLabelChamp();
    CHTRG = new XRiTextField();
    OBJ_35_OBJ_35 = new SNLabelChamp();
    CHCLA = new XRiTextField();
    CHCL2 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(720, 340));
    setPreferredSize(new Dimension(720, 340));
    setMaximumSize(new Dimension(720, 340));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setOpaque(true);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- sNLabelTitre1 ----
        sNLabelTitre1.setText("D\u00e9tail");
        sNLabelTitre1.setName("sNLabelTitre1");
        p_contenu.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setPreferredSize(new Dimension(439, 280));
          xTitledPanel1.setName("xTitledPanel1");
          xTitledPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout)xTitledPanel1.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)xTitledPanel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)xTitledPanel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)xTitledPanel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setPreferredSize(new Dimension(40, 28));
          INDETB.setMinimumSize(new Dimension(40, 28));
          INDETB.setMaximumSize(new Dimension(40, 28));
          INDETB.setName("INDETB");
          xTitledPanel1.add(INDETB, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHVDE ----
          CHVDE.setComponentPopupMenu(BTD);
          CHVDE.setPreferredSize(new Dimension(40, 28));
          CHVDE.setMinimumSize(new Dimension(40, 28));
          CHVDE.setMaximumSize(new Dimension(40, 28));
          CHVDE.setName("CHVDE");
          xTitledPanel1.add(CHVDE, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHMTT ----
          CHMTT.setComponentPopupMenu(BTD);
          CHMTT.setMinimumSize(new Dimension(120, 28));
          CHMTT.setMaximumSize(new Dimension(120, 28));
          CHMTT.setPreferredSize(new Dimension(120, 28));
          CHMTT.setName("CHMTT");
          xTitledPanel1.add(CHMTT, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHDRGX ----
          CHDRGX.setComponentPopupMenu(BTD);
          CHDRGX.setPreferredSize(new Dimension(105, 28));
          CHDRGX.setMinimumSize(new Dimension(105, 28));
          CHDRGX.setMaximumSize(new Dimension(105, 28));
          CHDRGX.setName("CHDRGX");
          xTitledPanel1.add(CHDRGX, new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_27_OBJ_27 ----
          OBJ_27_OBJ_27.setText("Fond de caisse");
          OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");
          xTitledPanel1.add(OBJ_27_OBJ_27, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("@MTTC@");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          xTitledPanel1.add(OBJ_28_OBJ_28, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("Mot de classement 1");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          xTitledPanel1.add(OBJ_42_OBJ_42, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("Mot de classement 2");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          xTitledPanel1.add(OBJ_44_OBJ_44, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- OBJ_29_OBJ_29 ----
          OBJ_29_OBJ_29.setText("Etablissement");
          OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");
          xTitledPanel1.add(OBJ_29_OBJ_29, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Num\u00e9ro d'op\u00e9ration");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
          xTitledPanel1.add(OBJ_31_OBJ_31, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setMinimumSize(new Dimension(85, 28));
          INDNUM.setPreferredSize(new Dimension(85, 28));
          INDNUM.setMaximumSize(new Dimension(85, 28));
          INDNUM.setName("INDNUM");
          xTitledPanel1.add(INDNUM, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("Montant");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
          xTitledPanel1.add(OBJ_34_OBJ_34, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Vendeur");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
          xTitledPanel1.add(OBJ_36_OBJ_36, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Type r\u00e8glement");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
          xTitledPanel1.add(OBJ_38_OBJ_38, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHTRG ----
          CHTRG.setMinimumSize(new Dimension(24, 28));
          CHTRG.setPreferredSize(new Dimension(24, 28));
          CHTRG.setMaximumSize(new Dimension(24, 28));
          CHTRG.setName("CHTRG");
          xTitledPanel1.add(CHTRG, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("Date");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
          xTitledPanel1.add(OBJ_35_OBJ_35, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHCLA ----
          CHCLA.setComponentPopupMenu(BTD);
          CHCLA.setMinimumSize(new Dimension(160, 28));
          CHCLA.setMaximumSize(new Dimension(160, 28));
          CHCLA.setPreferredSize(new Dimension(160, 28));
          CHCLA.setName("CHCLA");
          xTitledPanel1.add(CHCLA, new GridBagConstraints(2, 5, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CHCL2 ----
          CHCL2.setComponentPopupMenu(BTD);
          CHCL2.setMinimumSize(new Dimension(160, 28));
          CHCL2.setMaximumSize(new Dimension(160, 28));
          CHCL2.setPreferredSize(new Dimension(160, 28));
          CHCL2.setName("CHCL2");
          xTitledPanel1.add(CHCL2, new GridBagConstraints(2, 6, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(xTitledPanel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu p_contenu;
  private SNLabelTitre sNLabelTitre1;
  private SNPanelTitre xTitledPanel1;
  private XRiTextField INDETB;
  private XRiTextField CHVDE;
  private XRiTextField CHMTT;
  private XRiCalendrier CHDRGX;
  private SNLabelChamp OBJ_27_OBJ_27;
  private SNLabelChamp OBJ_28_OBJ_28;
  private SNLabelChamp OBJ_42_OBJ_42;
  private SNLabelChamp OBJ_44_OBJ_44;
  private SNLabelChamp OBJ_29_OBJ_29;
  private SNLabelChamp OBJ_31_OBJ_31;
  private XRiTextField INDNUM;
  private SNLabelChamp OBJ_34_OBJ_34;
  private SNLabelChamp OBJ_36_OBJ_36;
  private SNLabelChamp OBJ_38_OBJ_38;
  private XRiTextField CHTRG;
  private SNLabelChamp OBJ_35_OBJ_35;
  private XRiTextField CHCLA;
  private XRiTextField CHCL2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
