
package ri.serien.libecranrpg.vgem.VGEM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEM08FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGEM08FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX5.setValeurs("5", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX8.setValeurs("8", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_81_OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AFFE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    barre_dup.setVisible(lexique.isTrue("56"));
    
    // Titre
    if (lexique.isTrue("60")) {
      setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Gestion des chèques en portefeuille"));
      p_bpresentation.setText("Gestion des chèques en portefeuille");
    }
    else {
      setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Gestion des chèques comptants"));
      p_bpresentation.setText("Gestion des chèques comptants");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTA.getInvoker().getName());
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTI.getInvoker().getName());
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTI.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_38_OBJ_38 = new JLabel();
    INDIDU = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_51_OBJ_51 = new JLabel();
    barre_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_36_OBJ_37 = new JLabel();
    IN3ETB = new XRiTextField();
    OBJ_38_OBJ_39 = new JLabel();
    IN3IDU = new XRiTextField();
    p_tete_droite2 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    sNLabelTitre1 = new SNLabelTitre();
    panel2 = new SNPanelTitre();
    TIDX8 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    ARG8 = new XRiTextField();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    TIDX6 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    ARG5 = new XRiTextField();
    ARG6 = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG22 = new XRiTextField();
    ARG11 = new XRiTextField();
    DECHX = new XRiCalendrier();
    DEMIX = new XRiCalendrier();
    ARG31 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG241 = new XRiTextField();
    TDECH = new XRiTextField();
    TDEMI = new XRiTextField();
    ARG23 = new XRiTextField();
    BTA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTI = new JPopupMenu();
    OBJ_8 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_26 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_81_OBJ_81 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des ch\u00e8ques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Etablissement");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTA);
          INDETB.setName("INDETB");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("N\u00b0pi\u00e8ce");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          //---- INDIDU ----
          INDIDU.setComponentPopupMenu(BTA);
          INDIDU.setName("INDIDU");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 541, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_tete_gaucheLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("Affectation");
          OBJ_51_OBJ_51.setFont(OBJ_51_OBJ_51.getFont().deriveFont(OBJ_51_OBJ_51.getFont().getSize() + 3f));
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          p_tete_droite.add(OBJ_51_OBJ_51);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- OBJ_36_OBJ_37 ----
          OBJ_36_OBJ_37.setText("Par duplication de");
          OBJ_36_OBJ_37.setName("OBJ_36_OBJ_37");

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTA);
          IN3ETB.setName("IN3ETB");

          //---- OBJ_38_OBJ_39 ----
          OBJ_38_OBJ_39.setText("N\u00b0pi\u00e8ce");
          OBJ_38_OBJ_39.setName("OBJ_38_OBJ_39");

          //---- IN3IDU ----
          IN3IDU.setComponentPopupMenu(BTA);
          IN3IDU.setName("IN3IDU");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addComponent(OBJ_36_OBJ_37, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_38_OBJ_39, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(IN3IDU, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(542, Short.MAX_VALUE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGroup(p_tete_gauche2Layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(OBJ_36_OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_38_OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(IN3IDU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(0, 2, Short.MAX_VALUE))
          );
        }
        barre_dup.add(p_tete_gauche2);

        //======== p_tete_droite2 ========
        {
          p_tete_droite2.setOpaque(false);
          p_tete_droite2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite2.setPreferredSize(new Dimension(150, 0));
          p_tete_droite2.setMinimumSize(new Dimension(150, 0));
          p_tete_droite2.setName("p_tete_droite2");
          p_tete_droite2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_dup.add(p_tete_droite2);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- sNLabelTitre1 ----
        sNLabelTitre1.setText("Recherche multi-crit\u00e8res");
        sNLabelTitre1.setName("sNLabelTitre1");
        pnlContenu.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(new GridBagLayout());
          ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- TIDX8 ----
          TIDX8.setText("Mot classement 2 ou magasin");
          TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8.setMinimumSize(new Dimension(250, 30));
          TIDX8.setMaximumSize(new Dimension(250, 30));
          TIDX8.setPreferredSize(new Dimension(250, 30));
          TIDX8.setName("TIDX8");
          panel2.add(TIDX8, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX2 ----
          TIDX2.setText("Identification remise");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setMinimumSize(new Dimension(250, 30));
          TIDX2.setMaximumSize(new Dimension(250, 30));
          TIDX2.setPreferredSize(new Dimension(250, 30));
          TIDX2.setName("TIDX2");
          panel2.add(TIDX2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(BTA);
          ARG4.setMinimumSize(new Dimension(160, 28));
          ARG4.setPreferredSize(new Dimension(160, 28));
          ARG4.setMaximumSize(new Dimension(160, 28));
          ARG4.setName("ARG4");
          panel2.add(ARG4, new GridBagConstraints(1, 3, 4, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ARG8 ----
          ARG8.setComponentPopupMenu(BTA);
          ARG8.setMinimumSize(new Dimension(160, 28));
          ARG8.setMaximumSize(new Dimension(160, 28));
          ARG8.setPreferredSize(new Dimension(160, 28));
          ARG8.setName("ARG8");
          panel2.add(ARG8, new GridBagConstraints(1, 6, 4, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_66_OBJ_66 ----
          OBJ_66_OBJ_66.setText("Date \u00e9ch\u00e9ance");
          OBJ_66_OBJ_66.setMinimumSize(new Dimension(250, 30));
          OBJ_66_OBJ_66.setMaximumSize(new Dimension(250, 30));
          OBJ_66_OBJ_66.setPreferredSize(new Dimension(250, 30));
          OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
          panel2.add(OBJ_66_OBJ_66, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("Date \u00e9mission");
          OBJ_67_OBJ_67.setMinimumSize(new Dimension(250, 30));
          OBJ_67_OBJ_67.setMaximumSize(new Dimension(250, 30));
          OBJ_67_OBJ_67.setPreferredSize(new Dimension(250, 30));
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
          panel2.add(OBJ_67_OBJ_67, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- TIDX6 ----
          TIDX6.setText("Num\u00e9ro compte");
          TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX6.setMinimumSize(new Dimension(250, 30));
          TIDX6.setMaximumSize(new Dimension(250, 30));
          TIDX6.setPreferredSize(new Dimension(250, 30));
          TIDX6.setName("TIDX6");
          panel2.add(TIDX6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX1 ----
          TIDX1.setText("Num\u00e9ro pi\u00e8ce");
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setMinimumSize(new Dimension(250, 30));
          TIDX1.setMaximumSize(new Dimension(250, 30));
          TIDX1.setPreferredSize(new Dimension(250, 30));
          TIDX1.setName("TIDX1");
          panel2.add(TIDX1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX3 ----
          TIDX3.setText("Num\u00e9ro client");
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setMinimumSize(new Dimension(250, 30));
          TIDX3.setMaximumSize(new Dimension(250, 30));
          TIDX3.setPreferredSize(new Dimension(250, 30));
          TIDX3.setName("TIDX3");
          panel2.add(TIDX3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX4 ----
          TIDX4.setText("Nom client");
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setMinimumSize(new Dimension(250, 30));
          TIDX4.setMaximumSize(new Dimension(250, 30));
          TIDX4.setPreferredSize(new Dimension(250, 30));
          TIDX4.setName("TIDX4");
          panel2.add(TIDX4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG5 ----
          ARG5.setComponentPopupMenu(BTA);
          ARG5.setPreferredSize(new Dimension(110, 28));
          ARG5.setMinimumSize(new Dimension(110, 28));
          ARG5.setMaximumSize(new Dimension(110, 28));
          ARG5.setName("ARG5");
          panel2.add(ARG5, new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG6 ----
          ARG6.setComponentPopupMenu(BTA);
          ARG6.setPreferredSize(new Dimension(110, 28));
          ARG6.setMinimumSize(new Dimension(110, 28));
          ARG6.setMaximumSize(new Dimension(110, 28));
          ARG6.setName("ARG6");
          panel2.add(ARG6, new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX5 ----
          TIDX5.setText("Montant");
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setMinimumSize(new Dimension(250, 30));
          TIDX5.setMaximumSize(new Dimension(250, 30));
          TIDX5.setPreferredSize(new Dimension(250, 30));
          TIDX5.setName("TIDX5");
          panel2.add(TIDX5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG22 ----
          ARG22.setComponentPopupMenu(BTA);
          ARG22.setMinimumSize(new Dimension(78, 28));
          ARG22.setPreferredSize(new Dimension(78, 28));
          ARG22.setMaximumSize(new Dimension(78, 28));
          ARG22.setName("ARG22");
          panel2.add(ARG22, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG11 ----
          ARG11.setComponentPopupMenu(BTA);
          ARG11.setPreferredSize(new Dimension(68, 28));
          ARG11.setMinimumSize(new Dimension(68, 28));
          ARG11.setMaximumSize(new Dimension(68, 28));
          ARG11.setName("ARG11");
          panel2.add(ARG11, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- DECHX ----
          DECHX.setComponentPopupMenu(BTA);
          DECHX.setPreferredSize(new Dimension(105, 28));
          DECHX.setMinimumSize(new Dimension(105, 28));
          DECHX.setMaximumSize(new Dimension(105, 28));
          DECHX.setName("DECHX");
          panel2.add(DECHX, new GridBagConstraints(2, 7, 3, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- DEMIX ----
          DEMIX.setComponentPopupMenu(BTA);
          DEMIX.setPreferredSize(new Dimension(105, 28));
          DEMIX.setMinimumSize(new Dimension(105, 28));
          DEMIX.setMaximumSize(new Dimension(105, 28));
          DEMIX.setName("DEMIX");
          panel2.add(DEMIX, new GridBagConstraints(2, 8, 3, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- ARG31 ----
          ARG31.setComponentPopupMenu(BTA);
          ARG31.setMinimumSize(new Dimension(60, 28));
          ARG31.setPreferredSize(new Dimension(60, 28));
          ARG31.setMaximumSize(new Dimension(60, 28));
          ARG31.setName("ARG31");
          panel2.add(ARG31, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BTA);
          ARG2.setMinimumSize(new Dimension(30, 28));
          ARG2.setPreferredSize(new Dimension(30, 28));
          ARG2.setMaximumSize(new Dimension(30, 28));
          ARG2.setName("ARG2");
          panel2.add(ARG2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG241 ----
          ARG241.setComponentPopupMenu(BTA);
          ARG241.setPreferredSize(new Dimension(40, 28));
          ARG241.setMinimumSize(new Dimension(40, 28));
          ARG241.setMaximumSize(new Dimension(40, 28));
          ARG241.setName("ARG241");
          panel2.add(ARG241, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- TDECH ----
          TDECH.setComponentPopupMenu(BTA);
          TDECH.setMinimumSize(new Dimension(27, 28));
          TDECH.setMaximumSize(new Dimension(27, 28));
          TDECH.setPreferredSize(new Dimension(27, 28));
          TDECH.setName("TDECH");
          panel2.add(TDECH, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TDEMI ----
          TDEMI.setComponentPopupMenu(BTA);
          TDEMI.setMinimumSize(new Dimension(27, 28));
          TDEMI.setMaximumSize(new Dimension(27, 28));
          TDEMI.setPreferredSize(new Dimension(27, 28));
          TDEMI.setName("TDEMI");
          panel2.add(TDEMI, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- ARG23 ----
          ARG23.setComponentPopupMenu(BTA);
          ARG23.setMinimumSize(new Dimension(24, 28));
          ARG23.setMaximumSize(new Dimension(24, 28));
          ARG23.setPreferredSize(new Dimension(24, 28));
          ARG23.setName("ARG23");
          panel2.add(ARG23, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        }
        pnlContenu.add(panel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTA ========
    {
      BTA.setName("BTA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTA.add(OBJ_5);
    }

    //======== BTI ========
    {
      BTI.setName("BTI");

      //---- OBJ_8 ----
      OBJ_8.setText("Choix possibles");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTI.add(OBJ_8);

      //---- OBJ_7 ----
      OBJ_7.setText("Aide en ligne");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      BTI.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_26 ----
      OBJ_26.setText("Choix possibles");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);
    }

    //---- OBJ_81_OBJ_81 ----
    OBJ_81_OBJ_81.setText("@AFFE@");
    OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX5);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField INDETB;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField INDIDU;
  private JPanel p_tete_droite;
  private JLabel OBJ_51_OBJ_51;
  private JMenuBar barre_dup;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_36_OBJ_37;
  private XRiTextField IN3ETB;
  private JLabel OBJ_38_OBJ_39;
  private XRiTextField IN3IDU;
  private JPanel p_tete_droite2;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre sNLabelTitre1;
  private SNPanelTitre panel2;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG4;
  private XRiTextField ARG8;
  private JLabel OBJ_66_OBJ_66;
  private JLabel OBJ_67_OBJ_67;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG5;
  private XRiTextField ARG6;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG22;
  private XRiTextField ARG11;
  private XRiCalendrier DECHX;
  private XRiCalendrier DEMIX;
  private XRiTextField ARG31;
  private XRiTextField ARG2;
  private XRiTextField ARG241;
  private XRiTextField TDECH;
  private XRiTextField TDEMI;
  private XRiTextField ARG23;
  private JPopupMenu BTA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTI;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_25;
  private JLabel OBJ_81_OBJ_81;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
