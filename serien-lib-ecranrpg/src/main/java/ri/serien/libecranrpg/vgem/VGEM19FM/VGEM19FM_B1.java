
package ri.serien.libecranrpg.vgem.VGEM19FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGEM19FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private int[] _LIST_Justification =
      { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.LEFT };
  private String[] _N01_Title = { "N° Effet", "", "Client", "Montant", "Sim.", };
  private String[][] _N01_Data =
      { { "N01", "NS01", "C01", "M01", "S01", }, { "N02", "NS02", "C02", "M02", "S02", }, { "N03", "NS03", "C03", "M03", "S03", },
          { "N04", "NS04", "C04", "M04", "S04", }, { "N05", "NS05", "C05", "M05", "S05", }, { "N06", "NS06", "C06", "M06", "S06", },
          { "N07", "NS07", "C07", "M07", "S07", }, { "N08", "NS08", "C08", "M08", "S08", }, { "N09", "NS09", "C09", "M09", "S09", },
          { "N10", "NS10", "C10", "M10", "S10", }, { "N11", "NS11", "C11", "M11", "S11", }, { "N12", "NS12", "C12", "M12", "S12", }, };
  private int[] _N01_Width = { 59, 17, 143, 108, 31, };
  
  public VGEM19FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    N01.setAspectTable(null, _N01_Title, _N01_Data, _N01_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TOTS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTS@")).trim());
    TOTM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTM@")).trim());
    DTECH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTECH@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), null, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    DTECH.setVisible(lexique.isPresent("DTECH"));
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("DTECH"));
    TOTM.setVisible(lexique.isPresent("TOTM"));
    TOTS.setVisible(lexique.isPresent("TOTS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    N01 = new XRiTable();
    TOTS = new RiZoneSortie();
    OBJ_24_OBJ_24 = new JLabel();
    TOTM = new RiZoneSortie();
    OBJ_32_OBJ_32 = new JLabel();
    DTECH = new RiZoneSortie();
    xTitledSeparator1 = new JXTitledSeparator();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(675, 385));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

            //---- N01 ----
            N01.setName("N01");
            SCROLLPANE_LIST2.setViewportView(N01);
          }
          p_recup.add(SCROLLPANE_LIST2);
          SCROLLPANE_LIST2.setBounds(30, 80, 399, 225);

          //---- TOTS ----
          TOTS.setText("@TOTS@");
          TOTS.setHorizontalTextPosition(SwingConstants.RIGHT);
          TOTS.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTS.setName("TOTS");
          p_recup.add(TOTS);
          TOTS.setBounds(162, 318, 132, TOTS.getPreferredSize().height);

          //---- OBJ_24_OBJ_24 ----
          OBJ_24_OBJ_24.setText("Total simulation");
          OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");
          p_recup.add(OBJ_24_OBJ_24);
          OBJ_24_OBJ_24.setBounds(60, 320, 100, 20);

          //---- TOTM ----
          TOTM.setText("@TOTM@");
          TOTM.setHorizontalTextPosition(SwingConstants.RIGHT);
          TOTM.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTM.setName("TOTM");
          p_recup.add(TOTM);
          TOTM.setBounds(297, 318, 132, TOTM.getPreferredSize().height);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Ech\u00e9ance du :");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          p_recup.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(245, 48, 95, 19);

          //---- DTECH ----
          DTECH.setText("@DTECH@");
          DTECH.setName("DTECH");
          p_recup.add(DTECH);
          DTECH.setBounds(359, 45, 70, DTECH.getPreferredSize().height);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Affichage d\u00e9tail portefeuille");
          xTitledSeparator1.setName("xTitledSeparator1");
          p_recup.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(15, 15, 460, xTitledSeparator1.getPreferredSize().height);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(435, 80, 25, 105);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(435, 200, 25, 105);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable N01;
  private RiZoneSortie TOTS;
  private JLabel OBJ_24_OBJ_24;
  private RiZoneSortie TOTM;
  private JLabel OBJ_32_OBJ_32;
  private RiZoneSortie DTECH;
  private JXTitledSeparator xTitledSeparator1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
