
package ri.serien.libecranrpg.vgem.VGEMSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEMSEFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGEMSEFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité GEM (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_53_OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_57_OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    SET016.setEnabled(lexique.isPresent("SET016"));
    SET015.setEnabled(lexique.isPresent("SET015"));
    SET014.setEnabled(lexique.isPresent("SET014"));
    SET013.setEnabled(lexique.isPresent("SET013"));
    SET012.setEnabled(lexique.isPresent("SET012"));
    SET011.setEnabled(lexique.isPresent("SET011"));
    SET008.setEnabled(lexique.isPresent("SET008"));
    SET007.setEnabled(lexique.isPresent("SET007"));
    SET006.setEnabled(lexique.isPresent("SET006"));
    SET005.setEnabled(lexique.isPresent("SET005"));
    SET004.setEnabled(lexique.isPresent("SET004"));
    SET003.setEnabled(lexique.isPresent("SET003"));
    SET002.setEnabled(lexique.isPresent("SET002"));
    SET001.setEnabled(lexique.isPresent("SET001"));
    V06F.setVisible(lexique.isPresent("V06F"));
    OBJ_57_OBJ_57.setVisible(lexique.isPresent("WPAGE"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    INDUSR.setVisible(lexique.isPresent("INDUSR"));
    OBJ_53_OBJ_53.setVisible(lexique.isPresent("DGNOM"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_29_OBJ_29 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_59_OBJ_59 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_53_OBJ_53 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    panel1 = new JPanel();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    OBJ_52 = new SNBoutonLeger();
    V06F = new XRiTextField();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_90_OBJ_90 = new JLabel();
    OBJ_117_OBJ_117 = new JLabel();
    OBJ_93_OBJ_93 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_96_OBJ_96 = new JLabel();
    OBJ_99_OBJ_99 = new JLabel();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_108_OBJ_108 = new JLabel();
    OBJ_102_OBJ_102 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    OBJ_105_OBJ_105 = new JLabel();
    OBJ_111_OBJ_111 = new JLabel();
    OBJ_84_OBJ_84 = new JLabel();
    OBJ_114_OBJ_114 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_110_OBJ_110 = new JLabel();
    OBJ_116_OBJ_116 = new JLabel();
    OBJ_119_OBJ_119 = new JLabel();
    SET001 = new XRiTextField();
    SET002 = new XRiTextField();
    SET003 = new XRiTextField();
    SET004 = new XRiTextField();
    SET005 = new XRiTextField();
    SET006 = new XRiTextField();
    SET007 = new XRiTextField();
    SET008 = new XRiTextField();
    SET011 = new XRiTextField();
    SET012 = new XRiTextField();
    SET013 = new XRiTextField();
    SET014 = new XRiTextField();
    SET015 = new XRiTextField();
    SET016 = new XRiTextField();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_89_OBJ_89 = new JLabel();
    OBJ_95_OBJ_95 = new JLabel();
    OBJ_101_OBJ_101 = new JLabel();
    OBJ_107_OBJ_107 = new JLabel();
    OBJ_113_OBJ_113 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_82_OBJ_82 = new JLabel();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_88_OBJ_88 = new JLabel();
    OBJ_91_OBJ_91 = new JLabel();
    OBJ_94_OBJ_94 = new JLabel();
    OBJ_97_OBJ_97 = new JLabel();
    OBJ_100_OBJ_100 = new JLabel();
    OBJ_103_OBJ_103 = new JLabel();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_109_OBJ_109 = new JLabel();
    OBJ_112_OBJ_112 = new JLabel();
    OBJ_115_OBJ_115 = new JLabel();
    OBJ_118_OBJ_118 = new JLabel();
    OBJ_113_OBJ_114 = new JLabel();
    OBJ_111_OBJ_112 = new JLabel();
    SET017 = new XRiTextField();
    OBJ_112_OBJ_113 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 GEM (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(750, 32));
          p_tete_gauche.setMinimumSize(new Dimension(750, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_29_OBJ_29 ----
          OBJ_29_OBJ_29.setText("utilisateur");
          OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setOpaque(false);
          INDUSR.setText("@INDUSR@");
          INDUSR.setName("INDUSR");

          //---- OBJ_59_OBJ_59 ----
          OBJ_59_OBJ_59.setText("Etablissement");
          OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("@DGNOM@");
          OBJ_53_OBJ_53.setOpaque(false);
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(70, 70, 70)
                    .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("Page");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
            panel1.add(OBJ_56_OBJ_56);
            OBJ_56_OBJ_56.setBounds(5, 1, 40, 21);

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("@WPAGE@");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
            panel1.add(OBJ_57_OBJ_57);
            OBJ_57_OBJ_57.setBounds(45, 0, 62, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_tete_droite.add(panel1);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autre tri");
              riSousMenu_bt6.setToolTipText("Tri par \u00e9tablissement ou code utilisateur");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(780, 420));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //---- OBJ_52 ----
        OBJ_52.setText("Aller \u00e0 la page");
        OBJ_52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_52.setName("OBJ_52");
        OBJ_52.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_52ActionPerformed(e);
          }
        });
        pnlContenu.add(OBJ_52);
        OBJ_52.setBounds(new Rectangle(new Point(555, 365), OBJ_52.getPreferredSize()));

        //---- V06F ----
        V06F.setComponentPopupMenu(BTD);
        V06F.setName("V06F");
        pnlContenu.add(V06F);
        V06F.setBounds(700, 365, 30, V06F.getPreferredSize().height);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle(" Objet de la s\u00e9curit\u00e9");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- OBJ_90_OBJ_90 ----
          OBJ_90_OBJ_90.setText("Remise ENCAISSEMENT/ESCOMPTE");
          OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
          xTitledPanel1ContentContainer.add(OBJ_90_OBJ_90);
          OBJ_90_OBJ_90.setBounds(65, 84, 220, 20);

          //---- OBJ_117_OBJ_117 ----
          OBJ_117_OBJ_117.setText("Purge des MOUVEMENTS G.E.M");
          OBJ_117_OBJ_117.setName("OBJ_117_OBJ_117");
          xTitledPanel1ContentContainer.add(OBJ_117_OBJ_117);
          OBJ_117_OBJ_117.setBounds(65, 234, 220, 20);

          //---- OBJ_93_OBJ_93 ----
          OBJ_93_OBJ_93.setText("G\u00e9n\u00e9rat\u00b0 Pr\u00e9l\u00e8vemt  Magn\u00e9tique");
          OBJ_93_OBJ_93.setName("OBJ_93_OBJ_93");
          xTitledPanel1ContentContainer.add(OBJ_93_OBJ_93);
          OBJ_93_OBJ_93.setBounds(405, 84, 190, 20);

          //---- OBJ_81_OBJ_81 ----
          OBJ_81_OBJ_81.setText("Gestion des PRELEVEMENTS");
          OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");
          xTitledPanel1ContentContainer.add(OBJ_81_OBJ_81);
          OBJ_81_OBJ_81.setBounds(405, 24, 190, 20);

          //---- OBJ_96_OBJ_96 ----
          OBJ_96_OBJ_96.setText("G\u00e9n\u00e9ration L.C.R Magn\u00e9tiques");
          OBJ_96_OBJ_96.setName("OBJ_96_OBJ_96");
          xTitledPanel1ContentContainer.add(OBJ_96_OBJ_96);
          OBJ_96_OBJ_96.setBounds(65, 114, 220, 20);

          //---- OBJ_99_OBJ_99 ----
          OBJ_99_OBJ_99.setText("Comptabilisation Pr\u00e9l\u00e8vements");
          OBJ_99_OBJ_99.setName("OBJ_99_OBJ_99");
          xTitledPanel1ContentContainer.add(OBJ_99_OBJ_99);
          OBJ_99_OBJ_99.setBounds(405, 114, 190, 20);

          //---- OBJ_62_OBJ_62 ----
          OBJ_62_OBJ_62.setText("Personnalisation si pas C.G.M");
          OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
          xTitledPanel1ContentContainer.add(OBJ_62_OBJ_62);
          OBJ_62_OBJ_62.setBounds(65, 24, 220, 20);

          //---- OBJ_108_OBJ_108 ----
          OBJ_108_OBJ_108.setText("Edition du PORTEFEUILLE");
          OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");
          xTitledPanel1ContentContainer.add(OBJ_108_OBJ_108);
          OBJ_108_OBJ_108.setBounds(65, 174, 220, 20);

          //---- OBJ_102_OBJ_102 ----
          OBJ_102_OBJ_102.setText("Comptabilisation des Effets");
          OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");
          xTitledPanel1ContentContainer.add(OBJ_102_OBJ_102);
          OBJ_102_OBJ_102.setBounds(65, 144, 220, 20);

          //---- OBJ_87_OBJ_87 ----
          OBJ_87_OBJ_87.setText("Demande de Pr\u00e9l\u00e8vement");
          OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
          xTitledPanel1ContentContainer.add(OBJ_87_OBJ_87);
          OBJ_87_OBJ_87.setBounds(405, 54, 190, 20);

          //---- OBJ_105_OBJ_105 ----
          OBJ_105_OBJ_105.setText("Edition des Pr\u00e9l\u00e8vements");
          OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");
          xTitledPanel1ContentContainer.add(OBJ_105_OBJ_105);
          OBJ_105_OBJ_105.setBounds(405, 144, 190, 20);

          //---- OBJ_111_OBJ_111 ----
          OBJ_111_OBJ_111.setText("Purge des Pr\u00e9l\u00e8vements");
          OBJ_111_OBJ_111.setName("OBJ_111_OBJ_111");
          xTitledPanel1ContentContainer.add(OBJ_111_OBJ_111);
          OBJ_111_OBJ_111.setBounds(405, 174, 190, 20);

          //---- OBJ_84_OBJ_84 ----
          OBJ_84_OBJ_84.setText("Gestion des EFFETS");
          OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");
          xTitledPanel1ContentContainer.add(OBJ_84_OBJ_84);
          OBJ_84_OBJ_84.setBounds(65, 54, 220, 20);

          //---- OBJ_114_OBJ_114 ----
          OBJ_114_OBJ_114.setText("Edition des Effets");
          OBJ_114_OBJ_114.setName("OBJ_114_OBJ_114");
          xTitledPanel1ContentContainer.add(OBJ_114_OBJ_114);
          OBJ_114_OBJ_114.setBounds(65, 204, 220, 20);

          //---- OBJ_80_OBJ_80 ----
          OBJ_80_OBJ_80.setText("01");
          OBJ_80_OBJ_80.setFont(OBJ_80_OBJ_80.getFont().deriveFont(OBJ_80_OBJ_80.getFont().getStyle() | Font.BOLD));
          OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
          xTitledPanel1ContentContainer.add(OBJ_80_OBJ_80);
          OBJ_80_OBJ_80.setBounds(35, 24, 21, 20);

          //---- OBJ_86_OBJ_86 ----
          OBJ_86_OBJ_86.setText("02");
          OBJ_86_OBJ_86.setFont(OBJ_86_OBJ_86.getFont().deriveFont(OBJ_86_OBJ_86.getFont().getStyle() | Font.BOLD));
          OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
          xTitledPanel1ContentContainer.add(OBJ_86_OBJ_86);
          OBJ_86_OBJ_86.setBounds(35, 54, 21, 20);

          //---- OBJ_92_OBJ_92 ----
          OBJ_92_OBJ_92.setText("03");
          OBJ_92_OBJ_92.setFont(OBJ_92_OBJ_92.getFont().deriveFont(OBJ_92_OBJ_92.getFont().getStyle() | Font.BOLD));
          OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");
          xTitledPanel1ContentContainer.add(OBJ_92_OBJ_92);
          OBJ_92_OBJ_92.setBounds(35, 84, 21, 20);

          //---- OBJ_98_OBJ_98 ----
          OBJ_98_OBJ_98.setText("04");
          OBJ_98_OBJ_98.setFont(OBJ_98_OBJ_98.getFont().deriveFont(OBJ_98_OBJ_98.getFont().getStyle() | Font.BOLD));
          OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
          xTitledPanel1ContentContainer.add(OBJ_98_OBJ_98);
          OBJ_98_OBJ_98.setBounds(35, 114, 21, 20);

          //---- OBJ_104_OBJ_104 ----
          OBJ_104_OBJ_104.setText("05");
          OBJ_104_OBJ_104.setFont(OBJ_104_OBJ_104.getFont().deriveFont(OBJ_104_OBJ_104.getFont().getStyle() | Font.BOLD));
          OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");
          xTitledPanel1ContentContainer.add(OBJ_104_OBJ_104);
          OBJ_104_OBJ_104.setBounds(35, 144, 21, 20);

          //---- OBJ_110_OBJ_110 ----
          OBJ_110_OBJ_110.setText("06");
          OBJ_110_OBJ_110.setFont(OBJ_110_OBJ_110.getFont().deriveFont(OBJ_110_OBJ_110.getFont().getStyle() | Font.BOLD));
          OBJ_110_OBJ_110.setName("OBJ_110_OBJ_110");
          xTitledPanel1ContentContainer.add(OBJ_110_OBJ_110);
          OBJ_110_OBJ_110.setBounds(35, 174, 21, 20);

          //---- OBJ_116_OBJ_116 ----
          OBJ_116_OBJ_116.setText("07");
          OBJ_116_OBJ_116.setFont(OBJ_116_OBJ_116.getFont().deriveFont(OBJ_116_OBJ_116.getFont().getStyle() | Font.BOLD));
          OBJ_116_OBJ_116.setName("OBJ_116_OBJ_116");
          xTitledPanel1ContentContainer.add(OBJ_116_OBJ_116);
          OBJ_116_OBJ_116.setBounds(35, 204, 21, 20);

          //---- OBJ_119_OBJ_119 ----
          OBJ_119_OBJ_119.setText("08");
          OBJ_119_OBJ_119.setFont(OBJ_119_OBJ_119.getFont().deriveFont(OBJ_119_OBJ_119.getFont().getStyle() | Font.BOLD));
          OBJ_119_OBJ_119.setName("OBJ_119_OBJ_119");
          xTitledPanel1ContentContainer.add(OBJ_119_OBJ_119);
          OBJ_119_OBJ_119.setBounds(35, 234, 21, 20);

          //---- SET001 ----
          SET001.setComponentPopupMenu(BTD);
          SET001.setName("SET001");
          xTitledPanel1ContentContainer.add(SET001);
          SET001.setBounds(290, 20, 20, SET001.getPreferredSize().height);

          //---- SET002 ----
          SET002.setComponentPopupMenu(BTD);
          SET002.setName("SET002");
          xTitledPanel1ContentContainer.add(SET002);
          SET002.setBounds(290, 50, 20, SET002.getPreferredSize().height);

          //---- SET003 ----
          SET003.setComponentPopupMenu(BTD);
          SET003.setName("SET003");
          xTitledPanel1ContentContainer.add(SET003);
          SET003.setBounds(290, 80, 20, SET003.getPreferredSize().height);

          //---- SET004 ----
          SET004.setComponentPopupMenu(BTD);
          SET004.setName("SET004");
          xTitledPanel1ContentContainer.add(SET004);
          SET004.setBounds(290, 110, 20, SET004.getPreferredSize().height);

          //---- SET005 ----
          SET005.setComponentPopupMenu(BTD);
          SET005.setName("SET005");
          xTitledPanel1ContentContainer.add(SET005);
          SET005.setBounds(290, 140, 20, SET005.getPreferredSize().height);

          //---- SET006 ----
          SET006.setComponentPopupMenu(BTD);
          SET006.setName("SET006");
          xTitledPanel1ContentContainer.add(SET006);
          SET006.setBounds(290, 170, 20, SET006.getPreferredSize().height);

          //---- SET007 ----
          SET007.setComponentPopupMenu(BTD);
          SET007.setName("SET007");
          xTitledPanel1ContentContainer.add(SET007);
          SET007.setBounds(290, 200, 20, SET007.getPreferredSize().height);

          //---- SET008 ----
          SET008.setComponentPopupMenu(BTD);
          SET008.setName("SET008");
          xTitledPanel1ContentContainer.add(SET008);
          SET008.setBounds(290, 230, 20, SET008.getPreferredSize().height);

          //---- SET011 ----
          SET011.setComponentPopupMenu(BTD);
          SET011.setName("SET011");
          xTitledPanel1ContentContainer.add(SET011);
          SET011.setBounds(600, 20, 20, SET011.getPreferredSize().height);

          //---- SET012 ----
          SET012.setComponentPopupMenu(BTD);
          SET012.setName("SET012");
          xTitledPanel1ContentContainer.add(SET012);
          SET012.setBounds(600, 50, 20, SET012.getPreferredSize().height);

          //---- SET013 ----
          SET013.setComponentPopupMenu(BTD);
          SET013.setName("SET013");
          xTitledPanel1ContentContainer.add(SET013);
          SET013.setBounds(600, 80, 20, SET013.getPreferredSize().height);

          //---- SET014 ----
          SET014.setComponentPopupMenu(BTD);
          SET014.setName("SET014");
          xTitledPanel1ContentContainer.add(SET014);
          SET014.setBounds(600, 110, 20, SET014.getPreferredSize().height);

          //---- SET015 ----
          SET015.setComponentPopupMenu(BTD);
          SET015.setName("SET015");
          xTitledPanel1ContentContainer.add(SET015);
          SET015.setBounds(600, 140, 20, SET015.getPreferredSize().height);

          //---- SET016 ----
          SET016.setComponentPopupMenu(BTD);
          SET016.setName("SET016");
          xTitledPanel1ContentContainer.add(SET016);
          SET016.setBounds(600, 170, 20, SET016.getPreferredSize().height);

          //---- OBJ_83_OBJ_83 ----
          OBJ_83_OBJ_83.setText("11");
          OBJ_83_OBJ_83.setFont(OBJ_83_OBJ_83.getFont().deriveFont(OBJ_83_OBJ_83.getFont().getStyle() | Font.BOLD));
          OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
          xTitledPanel1ContentContainer.add(OBJ_83_OBJ_83);
          OBJ_83_OBJ_83.setBounds(375, 24, 20, 20);

          //---- OBJ_89_OBJ_89 ----
          OBJ_89_OBJ_89.setText("12");
          OBJ_89_OBJ_89.setFont(OBJ_89_OBJ_89.getFont().deriveFont(OBJ_89_OBJ_89.getFont().getStyle() | Font.BOLD));
          OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
          xTitledPanel1ContentContainer.add(OBJ_89_OBJ_89);
          OBJ_89_OBJ_89.setBounds(375, 54, 20, 20);

          //---- OBJ_95_OBJ_95 ----
          OBJ_95_OBJ_95.setText("13");
          OBJ_95_OBJ_95.setFont(OBJ_95_OBJ_95.getFont().deriveFont(OBJ_95_OBJ_95.getFont().getStyle() | Font.BOLD));
          OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");
          xTitledPanel1ContentContainer.add(OBJ_95_OBJ_95);
          OBJ_95_OBJ_95.setBounds(375, 84, 20, 20);

          //---- OBJ_101_OBJ_101 ----
          OBJ_101_OBJ_101.setText("14");
          OBJ_101_OBJ_101.setFont(OBJ_101_OBJ_101.getFont().deriveFont(OBJ_101_OBJ_101.getFont().getStyle() | Font.BOLD));
          OBJ_101_OBJ_101.setName("OBJ_101_OBJ_101");
          xTitledPanel1ContentContainer.add(OBJ_101_OBJ_101);
          OBJ_101_OBJ_101.setBounds(375, 114, 20, 20);

          //---- OBJ_107_OBJ_107 ----
          OBJ_107_OBJ_107.setText("15");
          OBJ_107_OBJ_107.setFont(OBJ_107_OBJ_107.getFont().deriveFont(OBJ_107_OBJ_107.getFont().getStyle() | Font.BOLD));
          OBJ_107_OBJ_107.setName("OBJ_107_OBJ_107");
          xTitledPanel1ContentContainer.add(OBJ_107_OBJ_107);
          OBJ_107_OBJ_107.setBounds(375, 144, 20, 20);

          //---- OBJ_113_OBJ_113 ----
          OBJ_113_OBJ_113.setText("16");
          OBJ_113_OBJ_113.setFont(OBJ_113_OBJ_113.getFont().deriveFont(OBJ_113_OBJ_113.getFont().getStyle() | Font.BOLD));
          OBJ_113_OBJ_113.setName("OBJ_113_OBJ_113");
          xTitledPanel1ContentContainer.add(OBJ_113_OBJ_113);
          OBJ_113_OBJ_113.setBounds(375, 174, 20, 20);

          //---- OBJ_79_OBJ_79 ----
          OBJ_79_OBJ_79.setText("a");
          OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");
          xTitledPanel1ContentContainer.add(OBJ_79_OBJ_79);
          OBJ_79_OBJ_79.setBounds(325, 24, 20, 20);

          //---- OBJ_82_OBJ_82 ----
          OBJ_82_OBJ_82.setText("a");
          OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");
          xTitledPanel1ContentContainer.add(OBJ_82_OBJ_82);
          OBJ_82_OBJ_82.setBounds(635, 24, 20, 20);

          //---- OBJ_85_OBJ_85 ----
          OBJ_85_OBJ_85.setText("a");
          OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
          xTitledPanel1ContentContainer.add(OBJ_85_OBJ_85);
          OBJ_85_OBJ_85.setBounds(325, 54, 20, 20);

          //---- OBJ_88_OBJ_88 ----
          OBJ_88_OBJ_88.setText("b");
          OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
          xTitledPanel1ContentContainer.add(OBJ_88_OBJ_88);
          OBJ_88_OBJ_88.setBounds(635, 54, 20, 20);

          //---- OBJ_91_OBJ_91 ----
          OBJ_91_OBJ_91.setText("b");
          OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");
          xTitledPanel1ContentContainer.add(OBJ_91_OBJ_91);
          OBJ_91_OBJ_91.setBounds(325, 84, 20, 20);

          //---- OBJ_94_OBJ_94 ----
          OBJ_94_OBJ_94.setText("b");
          OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");
          xTitledPanel1ContentContainer.add(OBJ_94_OBJ_94);
          OBJ_94_OBJ_94.setBounds(635, 84, 20, 20);

          //---- OBJ_97_OBJ_97 ----
          OBJ_97_OBJ_97.setText("b");
          OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
          xTitledPanel1ContentContainer.add(OBJ_97_OBJ_97);
          OBJ_97_OBJ_97.setBounds(325, 114, 20, 20);

          //---- OBJ_100_OBJ_100 ----
          OBJ_100_OBJ_100.setText("b");
          OBJ_100_OBJ_100.setName("OBJ_100_OBJ_100");
          xTitledPanel1ContentContainer.add(OBJ_100_OBJ_100);
          OBJ_100_OBJ_100.setBounds(635, 114, 20, 20);

          //---- OBJ_103_OBJ_103 ----
          OBJ_103_OBJ_103.setText("b");
          OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");
          xTitledPanel1ContentContainer.add(OBJ_103_OBJ_103);
          OBJ_103_OBJ_103.setBounds(325, 144, 20, 20);

          //---- OBJ_106_OBJ_106 ----
          OBJ_106_OBJ_106.setText("b");
          OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");
          xTitledPanel1ContentContainer.add(OBJ_106_OBJ_106);
          OBJ_106_OBJ_106.setBounds(635, 144, 20, 20);

          //---- OBJ_109_OBJ_109 ----
          OBJ_109_OBJ_109.setText("b");
          OBJ_109_OBJ_109.setName("OBJ_109_OBJ_109");
          xTitledPanel1ContentContainer.add(OBJ_109_OBJ_109);
          OBJ_109_OBJ_109.setBounds(325, 174, 20, 20);

          //---- OBJ_112_OBJ_112 ----
          OBJ_112_OBJ_112.setText("b");
          OBJ_112_OBJ_112.setName("OBJ_112_OBJ_112");
          xTitledPanel1ContentContainer.add(OBJ_112_OBJ_112);
          OBJ_112_OBJ_112.setBounds(635, 174, 20, 20);

          //---- OBJ_115_OBJ_115 ----
          OBJ_115_OBJ_115.setText("b");
          OBJ_115_OBJ_115.setName("OBJ_115_OBJ_115");
          xTitledPanel1ContentContainer.add(OBJ_115_OBJ_115);
          OBJ_115_OBJ_115.setBounds(325, 204, 20, 20);

          //---- OBJ_118_OBJ_118 ----
          OBJ_118_OBJ_118.setText("b");
          OBJ_118_OBJ_118.setName("OBJ_118_OBJ_118");
          xTitledPanel1ContentContainer.add(OBJ_118_OBJ_118);
          OBJ_118_OBJ_118.setBounds(325, 234, 20, 20);

          //---- OBJ_113_OBJ_114 ----
          OBJ_113_OBJ_114.setText("17");
          OBJ_113_OBJ_114.setFont(OBJ_113_OBJ_114.getFont().deriveFont(OBJ_113_OBJ_114.getFont().getStyle() | Font.BOLD));
          OBJ_113_OBJ_114.setName("OBJ_113_OBJ_114");
          xTitledPanel1ContentContainer.add(OBJ_113_OBJ_114);
          OBJ_113_OBJ_114.setBounds(375, 204, 20, 20);

          //---- OBJ_111_OBJ_112 ----
          OBJ_111_OBJ_112.setText("Annulation des effets");
          OBJ_111_OBJ_112.setName("OBJ_111_OBJ_112");
          xTitledPanel1ContentContainer.add(OBJ_111_OBJ_112);
          OBJ_111_OBJ_112.setBounds(405, 204, 190, 20);

          //---- SET017 ----
          SET017.setComponentPopupMenu(BTD);
          SET017.setName("SET017");
          xTitledPanel1ContentContainer.add(SET017);
          SET017.setBounds(600, 200, 20, SET017.getPreferredSize().height);

          //---- OBJ_112_OBJ_113 ----
          OBJ_112_OBJ_113.setText("b");
          OBJ_112_OBJ_113.setName("OBJ_112_OBJ_113");
          xTitledPanel1ContentContainer.add(OBJ_112_OBJ_113);
          OBJ_112_OBJ_113.setBounds(635, 204, 20, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(40, 30, 695, 320);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_29_OBJ_29;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_59_OBJ_59;
  private RiZoneSortie INDETB;
  private RiZoneSortie OBJ_53_OBJ_53;
  private JPanel p_tete_droite;
  private JPanel panel1;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_57_OBJ_57;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelContenu pnlContenu;
  private SNBoutonLeger OBJ_52;
  private XRiTextField V06F;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_90_OBJ_90;
  private JLabel OBJ_117_OBJ_117;
  private JLabel OBJ_93_OBJ_93;
  private JLabel OBJ_81_OBJ_81;
  private JLabel OBJ_96_OBJ_96;
  private JLabel OBJ_99_OBJ_99;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_108_OBJ_108;
  private JLabel OBJ_102_OBJ_102;
  private JLabel OBJ_87_OBJ_87;
  private JLabel OBJ_105_OBJ_105;
  private JLabel OBJ_111_OBJ_111;
  private JLabel OBJ_84_OBJ_84;
  private JLabel OBJ_114_OBJ_114;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_86_OBJ_86;
  private JLabel OBJ_92_OBJ_92;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_110_OBJ_110;
  private JLabel OBJ_116_OBJ_116;
  private JLabel OBJ_119_OBJ_119;
  private XRiTextField SET001;
  private XRiTextField SET002;
  private XRiTextField SET003;
  private XRiTextField SET004;
  private XRiTextField SET005;
  private XRiTextField SET006;
  private XRiTextField SET007;
  private XRiTextField SET008;
  private XRiTextField SET011;
  private XRiTextField SET012;
  private XRiTextField SET013;
  private XRiTextField SET014;
  private XRiTextField SET015;
  private XRiTextField SET016;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_89_OBJ_89;
  private JLabel OBJ_95_OBJ_95;
  private JLabel OBJ_101_OBJ_101;
  private JLabel OBJ_107_OBJ_107;
  private JLabel OBJ_113_OBJ_113;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_82_OBJ_82;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_88_OBJ_88;
  private JLabel OBJ_91_OBJ_91;
  private JLabel OBJ_94_OBJ_94;
  private JLabel OBJ_97_OBJ_97;
  private JLabel OBJ_100_OBJ_100;
  private JLabel OBJ_103_OBJ_103;
  private JLabel OBJ_106_OBJ_106;
  private JLabel OBJ_109_OBJ_109;
  private JLabel OBJ_112_OBJ_112;
  private JLabel OBJ_115_OBJ_115;
  private JLabel OBJ_118_OBJ_118;
  private JLabel OBJ_113_OBJ_114;
  private JLabel OBJ_111_OBJ_112;
  private XRiTextField SET017;
  private JLabel OBJ_112_OBJ_113;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
