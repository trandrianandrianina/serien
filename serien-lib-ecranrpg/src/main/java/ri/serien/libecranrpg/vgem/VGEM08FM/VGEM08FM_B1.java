
package ri.serien.libecranrpg.vgem.VGEM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGEM08FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] CHACC_Value = { "0", "1", "2", };
  private String[] XCOD_Value = { "", "E", "D", };
  private boolean isGVMexiste = true;
  
  public VGEM08FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CHACC.setValeurs(CHACC_Value, null);
    XCOD.setValeurs(XCOD_Value, null);
    CHNC1X.setValeursSelection("OUI", "NON");
    CHNC2X.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_127_OBJ_127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // Indicateur 31 = le module GVM existe chez le cllient
    isGVMexiste = lexique.isTrue("31");
    
    lbEtablissementGVM.setVisible(lexique.isTrue("(51) AND (80)"));
    CHETB2.setVisible(lexique.isTrue("(51) AND (80)"));
    lbNumeroDocumentVente.setVisible(lexique.isPresent("XNUM"));
    XCOD.setVisible(lexique.isPresent("XNUM"));
    OBJ_51_OBJ_51.setVisible(lexique.isTrue("32"));
    if (lexique.isTrue("32")) {
      riSousMenu_bt8.setText("Désactiver affectation");
    }
    else {
      riSousMenu_bt8.setText("Activer affectation");
    }
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    if (isGVMexiste) {
      snMagasin.setVisible(true);
      snVendeur.setVisible(true);
      CHMAG.setVisible(false);
      CHVDE.setVisible(false);
      // Magasin
      snMagasin.setSession(getSession());
      snMagasin.setIdEtablissement(idEtablissement);
      snMagasin.charger(false);
      snMagasin.setSelectionParChampRPG(lexique, "CHMAG");
      snMagasin.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
      
      // Vendeur
      snVendeur.setSession(getSession());
      snVendeur.setIdEtablissement(idEtablissement);
      snVendeur.charger(false);
      snVendeur.setSelectionParChampRPG(lexique, "CHVDE");
      snVendeur.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    }
    else {
      snMagasin.setVisible(false);
      snVendeur.setVisible(false);
      CHMAG.setVisible(true);
      CHVDE.setVisible(true);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - GESTION DES CHEQUES"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (isGVMexiste) {
      snMagasin.renseignerChampRPG(lexique, "CHMAG");
      snVendeur.renseignerChampRPG(lexique, "CHVDE");
    }
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTA.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_133ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 17);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_38_OBJ_38 = new JLabel();
    INDIDU = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_51_OBJ_51 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    lbCheque = new SNLabelTitre();
    pnlCheque = new SNPanelTitre();
    lbMontant = new SNLabelChamp();
    CHMTT = new XRiTextField();
    lbEmission = new SNLabelChamp();
    CHDEMX = new XRiCalendrier();
    CHACC = new XRiComboBox();
    lbEmission2 = new SNLabelChamp();
    CHCL2 = new XRiTextField();
    lbNumeroDocumentVente = new SNLabelChamp();
    XNUM = new XRiTextField();
    XSUF = new XRiTextField();
    XCOD = new XRiComboBox();
    lbEmission3 = new SNLabelChamp();
    pnlVendeur = new SNPanel();
    CHVDE = new XRiTextField();
    snVendeur = new SNVendeur();
    lbEmission4 = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    CHMAG = new XRiTextField();
    snMagasin = new SNMagasin();
    lbMontant2 = new SNLabelChamp();
    CHDACX = new XRiCalendrier();
    CHNC1X = new XRiCheckBox();
    lbMontant4 = new SNLabelChamp();
    CHDREX = new XRiCalendrier();
    CHNC2X = new XRiCheckBox();
    pnlBanque = new JPanel();
    CHBRE = new XRiTextField();
    lbEmission5 = new SNLabelChamp();
    lbEmission6 = new SNLabelChamp();
    lbEmission7 = new SNLabelChamp();
    CHNRE = new XRiTextField();
    CHSRE = new XRiTextField();
    lbMontant3 = new SNLabelChamp();
    CHDECX = new XRiCalendrier();
    lbTireur = new SNLabelTitre();
    pnlTireur = new SNPanelTitre();
    CHCLA = new XRiTextField();
    CHNOM = new XRiTextField();
    CHAD1 = new XRiTextField();
    CHAD2 = new XRiTextField();
    CHETB2 = new XRiTextField();
    lbEtablissementGVM = new SNLabelChamp();
    CHAD3 = new XRiTextField();
    lbRaisonSociale = new SNLabelUnite();
    lbNumeroCompte = new SNLabelUnite();
    lbClassement = new SNLabelUnite();
    CHNCG = new XRiTextField();
    CHNCA = new XRiTextField();
    lbReference = new SNLabelChamp();
    CHRTI = new XRiTextField();
    lbDomiciliation = new SNLabelTitre();
    pnlDomiciliation = new SNPanelTitre();
    CHDOM = new XRiTextField();
    lbNomBanque = new SNLabelUnite();
    CHNCB = new XRiTextField();
    lbNumeroCompteDom = new SNLabelUnite();
    lbNumeroGuichetDom = new SNLabelUnite();
    CHCBQ = new XRiTextField();
    CHCGU = new XRiTextField();
    lbNumeroEtablissementDom = new SNLabelUnite();
    CHRIB = new XRiTextField();
    lbCleDom = new SNLabelUnite();
    OBJ_127_OBJ_127 = new RiZoneSortie();
    btDomiciliation = new SNBoutonDetail();
    BTA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des ch\u00e8ques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Etablissement");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTA);
          INDETB.setName("INDETB");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("N\u00b0pi\u00e8ce");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          //---- INDIDU ----
          INDIDU.setComponentPopupMenu(BTA);
          INDIDU.setName("INDIDU");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(538, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
              .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("Affectation");
          OBJ_51_OBJ_51.setFont(OBJ_51_OBJ_51.getFont().deriveFont(OBJ_51_OBJ_51.getFont().getSize() + 3f));
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          p_tete_droite.add(OBJ_51_OBJ_51);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Remise sur place");
              riSousMenu_bt6.setToolTipText("Remise sur place");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Remise hors place");
              riSousMenu_bt7.setToolTipText("Remise hors place");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Affectation");
              riSousMenu_bt8.setToolTipText("Remise hors place");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbCheque ----
        lbCheque.setText("Ch\u00e8que");
        lbCheque.setName("lbCheque");
        pnlContenu.add(lbCheque, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlCheque ========
        {
          pnlCheque.setName("pnlCheque");
          pnlCheque.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCheque.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlCheque.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlCheque.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlCheque.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbMontant ----
          lbMontant.setText("Montant");
          lbMontant.setMinimumSize(new Dimension(150, 19));
          lbMontant.setPreferredSize(new Dimension(150, 19));
          lbMontant.setName("lbMontant");
          pnlCheque.add(lbMontant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHMTT ----
          CHMTT.setComponentPopupMenu(BTD);
          CHMTT.setPreferredSize(new Dimension(120, 28));
          CHMTT.setMinimumSize(new Dimension(120, 28));
          CHMTT.setName("CHMTT");
          pnlCheque.add(CHMTT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbEmission ----
          lbEmission.setText("Emission");
          lbEmission.setMaximumSize(new Dimension(120, 30));
          lbEmission.setMinimumSize(new Dimension(120, 30));
          lbEmission.setPreferredSize(new Dimension(120, 30));
          lbEmission.setName("lbEmission");
          pnlCheque.add(lbEmission, new GridBagConstraints(2, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHDEMX ----
          CHDEMX.setToolTipText("Date");
          CHDEMX.setComponentPopupMenu(BTD);
          CHDEMX.setEditable(false);
          CHDEMX.setPreferredSize(new Dimension(105, 28));
          CHDEMX.setMinimumSize(new Dimension(105, 28));
          CHDEMX.setMaximumSize(new Dimension(105, 28));
          CHDEMX.setName("CHDEMX");
          pnlCheque.add(CHDEMX, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHACC ----
          CHACC.setModel(new DefaultComboBoxModel(new String[] {
            "Ch\u00e8que re\u00e7u",
            "Ch\u00e8que \u00e0 recevoir",
            "Ch\u00e8que comptant"
          }));
          CHACC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHACC.setName("CHACC");
          pnlCheque.add(CHACC, new GridBagConstraints(5, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbEmission2 ----
          lbEmission2.setText("Mot de classement 2");
          lbEmission2.setName("lbEmission2");
          pnlCheque.add(lbEmission2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHCL2 ----
          CHCL2.setComponentPopupMenu(BTA);
          CHCL2.setMinimumSize(new Dimension(150, 28));
          CHCL2.setPreferredSize(new Dimension(150, 28));
          CHCL2.setMaximumSize(new Dimension(150, 28));
          CHCL2.setName("CHCL2");
          pnlCheque.add(CHCL2, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNumeroDocumentVente ----
          lbNumeroDocumentVente.setText("Num\u00e9ro document de vente");
          lbNumeroDocumentVente.setMaximumSize(new Dimension(200, 30));
          lbNumeroDocumentVente.setMinimumSize(new Dimension(200, 30));
          lbNumeroDocumentVente.setPreferredSize(new Dimension(200, 30));
          lbNumeroDocumentVente.setName("lbNumeroDocumentVente");
          pnlCheque.add(lbNumeroDocumentVente, new GridBagConstraints(3, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- XNUM ----
          XNUM.setComponentPopupMenu(BTD);
          XNUM.setMinimumSize(new Dimension(60, 28));
          XNUM.setPreferredSize(new Dimension(60, 28));
          XNUM.setMaximumSize(new Dimension(60, 28));
          XNUM.setName("XNUM");
          pnlCheque.add(XNUM, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- XSUF ----
          XSUF.setComponentPopupMenu(BTD);
          XSUF.setMinimumSize(new Dimension(24, 28));
          XSUF.setPreferredSize(new Dimension(24, 28));
          XSUF.setMaximumSize(new Dimension(24, 28));
          XSUF.setName("XSUF");
          pnlCheque.add(XSUF, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- XCOD ----
          XCOD.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Document de vente",
            "Devis"
          }));
          XCOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          XCOD.setName("XCOD");
          pnlCheque.add(XCOD, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbEmission3 ----
          lbEmission3.setText("Vendeur");
          lbEmission3.setMaximumSize(new Dimension(120, 30));
          lbEmission3.setMinimumSize(new Dimension(120, 30));
          lbEmission3.setPreferredSize(new Dimension(120, 30));
          lbEmission3.setName("lbEmission3");
          pnlCheque.add(lbEmission3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlVendeur ========
          {
            pnlVendeur.setName("pnlVendeur");
            pnlVendeur.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlVendeur.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlVendeur.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlVendeur.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlVendeur.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- CHVDE ----
            CHVDE.setPreferredSize(new Dimension(50, 30));
            CHVDE.setMinimumSize(new Dimension(50, 30));
            CHVDE.setName("CHVDE");
            pnlVendeur.add(CHVDE, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snVendeur ----
            snVendeur.setName("snVendeur");
            pnlVendeur.add(snVendeur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCheque.add(pnlVendeur, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbEmission4 ----
          lbEmission4.setText("Magasin");
          lbEmission4.setMaximumSize(new Dimension(100, 30));
          lbEmission4.setMinimumSize(new Dimension(100, 30));
          lbEmission4.setPreferredSize(new Dimension(100, 30));
          lbEmission4.setName("lbEmission4");
          pnlCheque.add(lbEmission4, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- CHMAG ----
            CHMAG.setMinimumSize(new Dimension(30, 30));
            CHMAG.setPreferredSize(new Dimension(30, 30));
            CHMAG.setName("CHMAG");
            sNPanel1.add(CHMAG, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snMagasin ----
            snMagasin.setPreferredSize(new Dimension(300, 30));
            snMagasin.setMinimumSize(new Dimension(300, 30));
            snMagasin.setMaximumSize(new Dimension(300, 30));
            snMagasin.setName("snMagasin");
            sNPanel1.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCheque.add(sNPanel1, new GridBagConstraints(5, 2, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMontant2 ----
          lbMontant2.setText("Mise en portefeuille");
          lbMontant2.setMinimumSize(new Dimension(150, 19));
          lbMontant2.setPreferredSize(new Dimension(150, 19));
          lbMontant2.setName("lbMontant2");
          pnlCheque.add(lbMontant2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHDACX ----
          CHDACX.setToolTipText("Date");
          CHDACX.setComponentPopupMenu(BTA);
          CHDACX.setPreferredSize(new Dimension(105, 28));
          CHDACX.setMinimumSize(new Dimension(105, 28));
          CHDACX.setMaximumSize(new Dimension(105, 28));
          CHDACX.setName("CHDACX");
          pnlCheque.add(CHDACX, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHNC1X ----
          CHNC1X.setComponentPopupMenu(BTD);
          CHNC1X.setMinimumSize(new Dimension(40, 28));
          CHNC1X.setMaximumSize(new Dimension(40, 28));
          CHNC1X.setPreferredSize(new Dimension(40, 28));
          CHNC1X.setText("Comptabilis\u00e9");
          CHNC1X.setFont(new Font("sansserif", Font.PLAIN, 14));
          CHNC1X.setName("CHNC1X");
          pnlCheque.add(CHNC1X, new GridBagConstraints(2, 3, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbMontant4 ----
          lbMontant4.setText("Remise en banque");
          lbMontant4.setMinimumSize(new Dimension(150, 19));
          lbMontant4.setPreferredSize(new Dimension(150, 19));
          lbMontant4.setName("lbMontant4");
          pnlCheque.add(lbMontant4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHDREX ----
          CHDREX.setToolTipText("Date");
          CHDREX.setComponentPopupMenu(BTA);
          CHDREX.setPreferredSize(new Dimension(105, 28));
          CHDREX.setMinimumSize(new Dimension(105, 28));
          CHDREX.setMaximumSize(new Dimension(105, 28));
          CHDREX.setName("CHDREX");
          pnlCheque.add(CHDREX, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHNC2X ----
          CHNC2X.setComponentPopupMenu(BTD);
          CHNC2X.setMinimumSize(new Dimension(40, 28));
          CHNC2X.setMaximumSize(new Dimension(40, 28));
          CHNC2X.setPreferredSize(new Dimension(40, 28));
          CHNC2X.setText("Comptabilis\u00e9");
          CHNC2X.setFont(new Font("sansserif", Font.PLAIN, 14));
          CHNC2X.setName("CHNC2X");
          pnlCheque.add(CHNC2X, new GridBagConstraints(2, 4, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlBanque ========
          {
            pnlBanque.setOpaque(false);
            pnlBanque.setName("pnlBanque");
            pnlBanque.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlBanque.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlBanque.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlBanque.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlBanque.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- CHBRE ----
            CHBRE.setComponentPopupMenu(BTA);
            CHBRE.setMinimumSize(new Dimension(30, 28));
            CHBRE.setMaximumSize(new Dimension(30, 28));
            CHBRE.setPreferredSize(new Dimension(30, 28));
            CHBRE.setName("CHBRE");
            pnlBanque.add(CHBRE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbEmission5 ----
            lbEmission5.setText("Banque");
            lbEmission5.setMinimumSize(new Dimension(55, 30));
            lbEmission5.setMaximumSize(new Dimension(55, 30));
            lbEmission5.setPreferredSize(new Dimension(55, 30));
            lbEmission5.setName("lbEmission5");
            pnlBanque.add(lbEmission5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbEmission6 ----
            lbEmission6.setText("Type");
            lbEmission6.setMinimumSize(new Dimension(55, 30));
            lbEmission6.setMaximumSize(new Dimension(55, 30));
            lbEmission6.setPreferredSize(new Dimension(55, 30));
            lbEmission6.setName("lbEmission6");
            pnlBanque.add(lbEmission6, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbEmission7 ----
            lbEmission7.setText("Suffixe");
            lbEmission7.setMinimumSize(new Dimension(55, 30));
            lbEmission7.setMaximumSize(new Dimension(55, 30));
            lbEmission7.setPreferredSize(new Dimension(55, 30));
            lbEmission7.setName("lbEmission7");
            pnlBanque.add(lbEmission7, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHNRE ----
            CHNRE.setComponentPopupMenu(BTA);
            CHNRE.setPreferredSize(new Dimension(24, 28));
            CHNRE.setMinimumSize(new Dimension(24, 28));
            CHNRE.setMaximumSize(new Dimension(24, 28));
            CHNRE.setName("CHNRE");
            pnlBanque.add(CHNRE, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHSRE ----
            CHSRE.setComponentPopupMenu(BTA);
            CHSRE.setPreferredSize(new Dimension(30, 28));
            CHSRE.setMinimumSize(new Dimension(30, 28));
            CHSRE.setMaximumSize(new Dimension(30, 28));
            CHSRE.setName("CHSRE");
            pnlBanque.add(CHSRE, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCheque.add(pnlBanque, new GridBagConstraints(4, 4, 4, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMontant3 ----
          lbMontant3.setText("Date d'\u00e9ch\u00e9ance");
          lbMontant3.setMinimumSize(new Dimension(150, 19));
          lbMontant3.setPreferredSize(new Dimension(150, 19));
          lbMontant3.setName("lbMontant3");
          pnlCheque.add(lbMontant3, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- CHDECX ----
          CHDECX.setToolTipText("Date");
          CHDECX.setComponentPopupMenu(BTA);
          CHDECX.setPreferredSize(new Dimension(105, 28));
          CHDECX.setMinimumSize(new Dimension(105, 28));
          CHDECX.setMaximumSize(new Dimension(105, 28));
          CHDECX.setName("CHDECX");
          pnlCheque.add(CHDECX, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlCheque, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbTireur ----
        lbTireur.setText("Tireur");
        lbTireur.setName("lbTireur");
        pnlContenu.add(lbTireur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlTireur ========
        {
          pnlTireur.setPreferredSize(new Dimension(700, 180));
          pnlTireur.setMinimumSize(new Dimension(700, 180));
          pnlTireur.setName("pnlTireur");
          pnlTireur.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlTireur.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlTireur.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlTireur.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlTireur.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- CHCLA ----
          CHCLA.setComponentPopupMenu(BTD);
          CHCLA.setPreferredSize(new Dimension(160, 28));
          CHCLA.setMinimumSize(new Dimension(160, 28));
          CHCLA.setName("CHCLA");
          pnlTireur.add(CHCLA, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- CHNOM ----
          CHNOM.setComponentPopupMenu(BTD);
          CHNOM.setMinimumSize(new Dimension(330, 28));
          CHNOM.setPreferredSize(new Dimension(330, 28));
          CHNOM.setName("CHNOM");
          pnlTireur.add(CHNOM, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- CHAD1 ----
          CHAD1.setMinimumSize(new Dimension(330, 28));
          CHAD1.setPreferredSize(new Dimension(330, 28));
          CHAD1.setName("CHAD1");
          pnlTireur.add(CHAD1, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- CHAD2 ----
          CHAD2.setMinimumSize(new Dimension(330, 28));
          CHAD2.setPreferredSize(new Dimension(330, 28));
          CHAD2.setName("CHAD2");
          pnlTireur.add(CHAD2, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- CHETB2 ----
          CHETB2.setComponentPopupMenu(BTD);
          CHETB2.setMinimumSize(new Dimension(40, 28));
          CHETB2.setPreferredSize(new Dimension(40, 28));
          CHETB2.setName("CHETB2");
          pnlTireur.add(CHETB2, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbEtablissementGVM ----
          lbEtablissementGVM.setText("Etablissement/GVM");
          lbEtablissementGVM.setName("lbEtablissementGVM");
          pnlTireur.add(lbEtablissementGVM, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- CHAD3 ----
          CHAD3.setMinimumSize(new Dimension(330, 28));
          CHAD3.setPreferredSize(new Dimension(330, 28));
          CHAD3.setName("CHAD3");
          pnlTireur.add(CHAD3, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- lbRaisonSociale ----
          lbRaisonSociale.setText("Nom ou raison sociale");
          lbRaisonSociale.setName("lbRaisonSociale");
          pnlTireur.add(lbRaisonSociale, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- lbNumeroCompte ----
          lbNumeroCompte.setText("N\u00b0 compte");
          lbNumeroCompte.setPreferredSize(new Dimension(150, 30));
          lbNumeroCompte.setMinimumSize(new Dimension(150, 30));
          lbNumeroCompte.setName("lbNumeroCompte");
          pnlTireur.add(lbNumeroCompte, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- lbClassement ----
          lbClassement.setText("Classement");
          lbClassement.setName("lbClassement");
          pnlTireur.add(lbClassement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- CHNCG ----
          CHNCG.setComponentPopupMenu(BTD);
          CHNCG.setPreferredSize(new Dimension(60, 28));
          CHNCG.setMinimumSize(new Dimension(60, 28));
          CHNCG.setName("CHNCG");
          pnlTireur.add(CHNCG, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- CHNCA ----
          CHNCA.setComponentPopupMenu(BTD);
          CHNCA.setPreferredSize(new Dimension(60, 28));
          CHNCA.setMinimumSize(new Dimension(60, 28));
          CHNCA.setName("CHNCA");
          pnlTireur.add(CHNCA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- lbReference ----
          lbReference.setText("R\u00e9f\u00e9rence");
          lbReference.setName("lbReference");
          pnlTireur.add(lbReference, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- CHRTI ----
          CHRTI.setPreferredSize(new Dimension(70, 28));
          CHRTI.setMinimumSize(new Dimension(70, 28));
          CHRTI.setMaximumSize(new Dimension(70, 28));
          CHRTI.setName("CHRTI");
          pnlTireur.add(CHRTI, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 5), 0, 0));
        }
        pnlContenu.add(pnlTireur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDomiciliation ----
        lbDomiciliation.setText("Domiciliation");
        lbDomiciliation.setName("lbDomiciliation");
        pnlContenu.add(lbDomiciliation, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlDomiciliation ========
        {
          pnlDomiciliation.setPreferredSize(new Dimension(500, 100));
          pnlDomiciliation.setMinimumSize(new Dimension(500, 100));
          pnlDomiciliation.setMaximumSize(new Dimension(500, 100));
          pnlDomiciliation.setName("pnlDomiciliation");
          pnlDomiciliation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDomiciliation.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDomiciliation.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDomiciliation.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlDomiciliation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- CHDOM ----
          CHDOM.setComponentPopupMenu(BTD);
          CHDOM.setPreferredSize(new Dimension(250, 28));
          CHDOM.setMinimumSize(new Dimension(250, 28));
          CHDOM.setName("CHDOM");
          pnlDomiciliation.add(CHDOM, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbNomBanque ----
          lbNomBanque.setText("Nom de la banque");
          lbNomBanque.setName("lbNomBanque");
          pnlDomiciliation.add(lbNomBanque, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHNCB ----
          CHNCB.setComponentPopupMenu(BTD);
          CHNCB.setMinimumSize(new Dimension(110, 28));
          CHNCB.setPreferredSize(new Dimension(110, 28));
          CHNCB.setName("CHNCB");
          pnlDomiciliation.add(CHNCB, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbNumeroCompteDom ----
          lbNumeroCompteDom.setText("N\u00b0 Compte");
          lbNumeroCompteDom.setName("lbNumeroCompteDom");
          pnlDomiciliation.add(lbNumeroCompteDom, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNumeroGuichetDom ----
          lbNumeroGuichetDom.setText("N\u00b0Guichet");
          lbNumeroGuichetDom.setPreferredSize(new Dimension(70, 19));
          lbNumeroGuichetDom.setMinimumSize(new Dimension(70, 19));
          lbNumeroGuichetDom.setName("lbNumeroGuichetDom");
          pnlDomiciliation.add(lbNumeroGuichetDom, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHCBQ ----
          CHCBQ.setComponentPopupMenu(BTD);
          CHCBQ.setMinimumSize(new Dimension(50, 28));
          CHCBQ.setPreferredSize(new Dimension(50, 28));
          CHCBQ.setName("CHCBQ");
          pnlDomiciliation.add(CHCBQ, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- CHCGU ----
          CHCGU.setComponentPopupMenu(BTD);
          CHCGU.setPreferredSize(new Dimension(60, 28));
          CHCGU.setMinimumSize(new Dimension(60, 28));
          CHCGU.setName("CHCGU");
          pnlDomiciliation.add(CHCGU, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbNumeroEtablissementDom ----
          lbNumeroEtablissementDom.setText("N\u00b0 Etb");
          lbNumeroEtablissementDom.setName("lbNumeroEtablissementDom");
          pnlDomiciliation.add(lbNumeroEtablissementDom, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHRIB ----
          CHRIB.setComponentPopupMenu(BTD);
          CHRIB.setPreferredSize(new Dimension(30, 28));
          CHRIB.setMinimumSize(new Dimension(30, 28));
          CHRIB.setName("CHRIB");
          pnlDomiciliation.add(CHRIB, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbCleDom ----
          lbCleDom.setText("Cl\u00e9");
          lbCleDom.setName("lbCleDom");
          pnlDomiciliation.add(lbCleDom, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_127_OBJ_127 ----
          OBJ_127_OBJ_127.setText("@UCLEX@");
          OBJ_127_OBJ_127.setFont(OBJ_127_OBJ_127.getFont().deriveFont(OBJ_127_OBJ_127.getFont().getStyle() | Font.BOLD));
          OBJ_127_OBJ_127.setForeground(Color.red);
          OBJ_127_OBJ_127.setMinimumSize(new Dimension(30, 28));
          OBJ_127_OBJ_127.setPreferredSize(new Dimension(30, 28));
          OBJ_127_OBJ_127.setName("OBJ_127_OBJ_127");
          pnlDomiciliation.add(OBJ_127_OBJ_127, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- btDomiciliation ----
          btDomiciliation.setText("");
          btDomiciliation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btDomiciliation.setName("btDomiciliation");
          btDomiciliation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_133ActionPerformed(e);
            }
          });
          pnlDomiciliation.add(btDomiciliation, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlDomiciliation, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTA ========
    {
      BTA.setName("BTA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField INDETB;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField INDIDU;
  private JPanel p_tete_droite;
  private JLabel OBJ_51_OBJ_51;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbCheque;
  private SNPanelTitre pnlCheque;
  private SNLabelChamp lbMontant;
  private XRiTextField CHMTT;
  private SNLabelChamp lbEmission;
  private XRiCalendrier CHDEMX;
  private XRiComboBox CHACC;
  private SNLabelChamp lbEmission2;
  private XRiTextField CHCL2;
  private SNLabelChamp lbNumeroDocumentVente;
  private XRiTextField XNUM;
  private XRiTextField XSUF;
  private XRiComboBox XCOD;
  private SNLabelChamp lbEmission3;
  private SNPanel pnlVendeur;
  private XRiTextField CHVDE;
  private SNVendeur snVendeur;
  private SNLabelChamp lbEmission4;
  private SNPanel sNPanel1;
  private XRiTextField CHMAG;
  private SNMagasin snMagasin;
  private SNLabelChamp lbMontant2;
  private XRiCalendrier CHDACX;
  private XRiCheckBox CHNC1X;
  private SNLabelChamp lbMontant4;
  private XRiCalendrier CHDREX;
  private XRiCheckBox CHNC2X;
  private JPanel pnlBanque;
  private XRiTextField CHBRE;
  private SNLabelChamp lbEmission5;
  private SNLabelChamp lbEmission6;
  private SNLabelChamp lbEmission7;
  private XRiTextField CHNRE;
  private XRiTextField CHSRE;
  private SNLabelChamp lbMontant3;
  private XRiCalendrier CHDECX;
  private SNLabelTitre lbTireur;
  private SNPanelTitre pnlTireur;
  private XRiTextField CHCLA;
  private XRiTextField CHNOM;
  private XRiTextField CHAD1;
  private XRiTextField CHAD2;
  private XRiTextField CHETB2;
  private SNLabelChamp lbEtablissementGVM;
  private XRiTextField CHAD3;
  private SNLabelUnite lbRaisonSociale;
  private SNLabelUnite lbNumeroCompte;
  private SNLabelUnite lbClassement;
  private XRiTextField CHNCG;
  private XRiTextField CHNCA;
  private SNLabelChamp lbReference;
  private XRiTextField CHRTI;
  private SNLabelTitre lbDomiciliation;
  private SNPanelTitre pnlDomiciliation;
  private XRiTextField CHDOM;
  private SNLabelUnite lbNomBanque;
  private XRiTextField CHNCB;
  private SNLabelUnite lbNumeroCompteDom;
  private SNLabelUnite lbNumeroGuichetDom;
  private XRiTextField CHCBQ;
  private XRiTextField CHCGU;
  private SNLabelUnite lbNumeroEtablissementDom;
  private XRiTextField CHRIB;
  private SNLabelUnite lbCleDom;
  private RiZoneSortie OBJ_127_OBJ_127;
  private SNBoutonDetail btDomiciliation;
  private JPopupMenu BTA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
