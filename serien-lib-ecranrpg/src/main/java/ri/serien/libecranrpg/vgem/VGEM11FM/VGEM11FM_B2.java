
package ri.serien.libecranrpg.vgem.VGEM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM11FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private boolean gestionTIP;
  
  public VGEM11FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    CHTRG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHTRG@")).trim());
    OBJ_80_OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
    OBJ_33_OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MONT@")).trim());
    OBJ_34_OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MONT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    gestionTIP = lexique.isTrue("95");
    
    CB01.setLongueurSaisie(4);
    CB02.setLongueurSaisie(4);
    CB03.setLongueurSaisie(4);
    CB04.setLongueurSaisie(4);
    
    String codeBancaire = lexique.HostFieldGetData("CHCAN");
    if (codeBancaire != null && codeBancaire.length() == 16) {
      CB01.setText(codeBancaire.substring(0, 3));
      CB02.setText(codeBancaire.substring(4, 7));
      CB03.setText(codeBancaire.substring(8, 11));
      CB04.setText(codeBancaire.substring(12, 15));
    }
    
    
    CHTCA.setEnabled(lexique.isPresent("CHTCA"));
    CHTRG.setVisible(lexique.isPresent("CHTRG"));
    OBJ_26_OBJ_26.setVisible(INDETB.isVisible());
    OBJ_28_OBJ_28.setVisible(INDNUM.isVisible());
    
    OBJ_55_OBJ_55.setEnabled(lexique.isPresent("CHAUT"));
    OBJ_80_OBJ_80.setVisible(lexique.isPresent("UCLEX"));
    CHRIB.setVisible(lexique.isPresent("CHRIB"));
    OBJ_74_OBJ_74.setVisible(lexique.isPresent("CHRIB"));
    CHANVR.setEnabled(lexique.isPresent("CHANVR"));
    CHMOV.setEnabled(lexique.isPresent("CHMOV"));
    CHVDE.setVisible(lexique.isPresent("CHVDE"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    OBJ_71_OBJ_71.setVisible(lexique.isPresent("CHCBQ"));
    CHCGU.setVisible(lexique.isPresent("CHCGU"));
    CHCBQ.setVisible(lexique.isPresent("CHCBQ"));
    CHAUT.setEnabled(lexique.isPresent("CHAUT"));
    CHNCA.setVisible(lexique.isPresent("CHNCA"));
    // CHDRGX.setVisible( lexique.isPresent("CHDRGX"));
    INDNUM.setVisible(lexique.isPresent("INDNUM"));
    OBJ_72_OBJ_72.setVisible(lexique.isPresent("CHCGU"));
    CHMTT.setVisible(lexique.isPresent("CHMTT"));
    CHNCB.setVisible(lexique.isPresent("CHNCB"));
    OBJ_73_OBJ_73.setVisible(lexique.isPresent("CHNCB"));
    P11MTT.setVisible(lexique.isPresent("P11MTT"));
    OBJ_70_OBJ_70.setVisible(lexique.isPresent("CHDOM"));
    CHCL2.setVisible(lexique.isPresent("CHCL2"));
    CHCLA.setVisible(lexique.isPresent("CHCLA"));
    CHDOM.setVisible(lexique.isPresent("CHDOM"));
    CHNOM.setVisible(lexique.isPresent("CHNOM"));
    OBJ_33_OBJ_33.setVisible(!lexique.HostFieldGetData("DATE").trim().equalsIgnoreCase("Date"));
    CHRIE.setVisible(lexique.isPresent("CHRIE"));
    lbMontantDepasse.setVisible(lexique.isTrue("(20) AND (N59)"));
    
    CHDECX.setVisible(!gestionTIP);
    xTitledPanel4.setVisible(gestionTIP);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
    lexique.HostFieldPutData("CHCAN", 0, CB01.getText() + CB02.getText() + CB03.getText() + CB04.getText());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new SNPanelContenu();
    sNLabelTitre1 = new SNLabelTitre();
    xTitledPanel1 = new SNPanelTitre();
    INDETB = new RiZoneSortie();
    CHVDE = new XRiTextField();
    CHMTT = new XRiTextField();
    CHDRGX = new XRiCalendrier();
    CHDECX = new XRiCalendrier();
    OBJ_36_OBJ_36 = new SNLabelChamp();
    OBJ_28_OBJ_28 = new SNLabelChamp();
    OBJ_44_OBJ_44 = new SNLabelChamp();
    OBJ_42_OBJ_42 = new SNLabelChamp();
    OBJ_26_OBJ_26 = new SNLabelChamp();
    INDNUM = new XRiTextField();
    P11MTT = new XRiTextField();
    lbMontantDepasse = new SNLabelChamp();
    OBJ_37_OBJ_37 = new SNLabelChamp();
    label1 = new SNLabelChamp();
    CHTRG = new RiZoneSortie();
    CHCL2 = new XRiTextField();
    sNLabelTitre2 = new SNLabelTitre();
    xTitledPanel2 = new SNPanelTitre();
    CB01 = new XRiTextField();
    CB02 = new XRiTextField();
    CB03 = new XRiTextField();
    CB04 = new XRiTextField();
    CHTCA = new XRiTextField();
    OBJ_48_OBJ_48 = new SNLabelChamp();
    OBJ_53_OBJ_53 = new SNLabelChamp();
    OBJ_55_OBJ_55 = new SNLabelChamp();
    CHAUT = new XRiTextField();
    OBJ_57_OBJ_57 = new SNLabelChamp();
    CHMOV = new XRiTextField();
    CHANVR = new XRiTextField();
    sNLabelTitre3 = new SNLabelTitre();
    xTitledPanel3 = new SNPanelTitre();
    CHNCA = new XRiTextField();
    CHCLA = new XRiTextField();
    CHNOM = new XRiTextField();
    OBJ_88_OBJ_88 = new SNLabelUnite();
    OBJ_87_OBJ_87 = new SNLabelUnite();
    OBJ_63_OBJ_63 = new SNLabelUnite();
    sNLabelTitre4 = new SNLabelTitre();
    xTitledPanel4 = new SNPanelTitre();
    CHDOM = new XRiTextField();
    button2 = new SNBoutonDetail();
    CHCBQ = new XRiTextField();
    CHCGU = new XRiTextField();
    CHNCB = new XRiTextField();
    CHRIB = new XRiTextField();
    CHRIE = new XRiTextField();
    OBJ_70_OBJ_70 = new SNLabelUnite();
    OBJ_73_OBJ_73 = new SNLabelUnite();
    OBJ_72_OBJ_72 = new SNLabelUnite();
    OBJ_71_OBJ_71 = new SNLabelUnite();
    OBJ_74_OBJ_74 = new SNLabelUnite();
    OBJ_80_OBJ_80 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_34_OBJ_34 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(980, 590));
    setPreferredSize(new Dimension(980, 590));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");
            
            // ---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);
          
          // ======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");
            
            // ---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);
          
          // ======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");
            
            // ---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setOpaque(true);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout) p_contenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) p_contenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) p_contenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) p_contenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- sNLabelTitre1 ----
        sNLabelTitre1.setText("Commande");
        sNLabelTitre1.setName("sNLabelTitre1");
        p_contenu.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== xTitledPanel1 ========
        {
          xTitledPanel1.setName("xTitledPanel1");
          xTitledPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) xTitledPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) xTitledPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setMaximumSize(new Dimension(35, 28));
          INDETB.setMinimumSize(new Dimension(35, 28));
          INDETB.setPreferredSize(new Dimension(35, 28));
          INDETB.setName("INDETB");
          xTitledPanel1.add(INDETB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CHVDE ----
          CHVDE.setComponentPopupMenu(BTD);
          CHVDE.setPreferredSize(new Dimension(40, 28));
          CHVDE.setMinimumSize(new Dimension(40, 28));
          CHVDE.setMaximumSize(new Dimension(40, 28));
          CHVDE.setName("CHVDE");
          xTitledPanel1.add(CHVDE, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CHMTT ----
          CHMTT.setComponentPopupMenu(BTD);
          CHMTT.setName("CHMTT");
          xTitledPanel1.add(CHMTT, new GridBagConstraints(3, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CHDRGX ----
          CHDRGX.setComponentPopupMenu(BTD);
          CHDRGX.setMinimumSize(new Dimension(105, 28));
          CHDRGX.setMaximumSize(new Dimension(105, 28));
          CHDRGX.setPreferredSize(new Dimension(105, 28));
          CHDRGX.setName("CHDRGX");
          xTitledPanel1.add(CHDRGX, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CHDECX ----
          CHDECX.setComponentPopupMenu(BTD);
          CHDECX.setMinimumSize(new Dimension(105, 28));
          CHDECX.setMaximumSize(new Dimension(105, 28));
          CHDECX.setPreferredSize(new Dimension(105, 28));
          CHDECX.setName("CHDECX");
          xTitledPanel1.add(CHDECX, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Date");
          OBJ_36_OBJ_36.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_36_OBJ_36.setPreferredSize(new Dimension(100, 30));
          OBJ_36_OBJ_36.setMinimumSize(new Dimension(100, 30));
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
          xTitledPanel1.add(OBJ_36_OBJ_36, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("Num\u00e9ro d'op\u00e9ration");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          xTitledPanel1.add(OBJ_28_OBJ_28, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("Classement 2");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          xTitledPanel1.add(OBJ_44_OBJ_44, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("Type r\u00e8glement");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          xTitledPanel1.add(OBJ_42_OBJ_42, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("Etablissement");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
          xTitledPanel1.add(OBJ_26_OBJ_26, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setMinimumSize(new Dimension(85, 30));
          INDNUM.setMaximumSize(new Dimension(85, 30));
          INDNUM.setPreferredSize(new Dimension(85, 30));
          INDNUM.setName("INDNUM");
          xTitledPanel1.add(INDNUM, new GridBagConstraints(3, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- P11MTT ----
          P11MTT.setHorizontalAlignment(SwingConstants.RIGHT);
          P11MTT.setPreferredSize(new Dimension(90, 28));
          P11MTT.setMinimumSize(new Dimension(90, 28));
          P11MTT.setMaximumSize(new Dimension(90, 28));
          P11MTT.setName("P11MTT");
          xTitledPanel1.add(P11MTT, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMontantDepasse ----
          lbMontantDepasse.setText("montant d\u00e9pass\u00e9");
          lbMontantDepasse.setFont(lbMontantDepasse.getFont().deriveFont(lbMontantDepasse.getFont().getStyle() | Font.BOLD));
          lbMontantDepasse.setForeground(Color.red);
          lbMontantDepasse.setName("lbMontantDepasse");
          xTitledPanel1.add(lbMontantDepasse, new GridBagConstraints(6, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Vendeur");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
          xTitledPanel1.add(OBJ_37_OBJ_37, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- label1 ----
          label1.setText("Montant");
          label1.setName("label1");
          xTitledPanel1.add(label1, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CHTRG ----
          CHTRG.setText("@CHTRG@");
          CHTRG.setPreferredSize(new Dimension(30, 28));
          CHTRG.setMinimumSize(new Dimension(30, 28));
          CHTRG.setMaximumSize(new Dimension(30, 28));
          CHTRG.setName("CHTRG");
          xTitledPanel1.add(CHTRG, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CHCL2 ----
          CHCL2.setComponentPopupMenu(BTD);
          CHCL2.setMinimumSize(new Dimension(160, 28));
          CHCL2.setMaximumSize(new Dimension(160, 28));
          CHCL2.setPreferredSize(new Dimension(160, 28));
          CHCL2.setName("CHCL2");
          xTitledPanel1.add(CHCL2, new GridBagConstraints(3, 2, 5, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(xTitledPanel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelTitre2 ----
        sNLabelTitre2.setText("Carte");
        sNLabelTitre2.setName("sNLabelTitre2");
        p_contenu.add(sNLabelTitre2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== xTitledPanel2 ========
        {
          xTitledPanel2.setName("xTitledPanel2");
          xTitledPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout) xTitledPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) xTitledPanel2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) xTitledPanel2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- CB01 ----
          CB01.setComponentPopupMenu(BTD);
          CB01.setPreferredSize(new Dimension(40, 28));
          CB01.setMinimumSize(new Dimension(40, 28));
          CB01.setMaximumSize(new Dimension(40, 28));
          CB01.setName("CB01");
          xTitledPanel2.add(CB01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CB02 ----
          CB02.setComponentPopupMenu(BTD);
          CB02.setPreferredSize(new Dimension(40, 28));
          CB02.setMinimumSize(new Dimension(40, 28));
          CB02.setMaximumSize(new Dimension(40, 28));
          CB02.setName("CB02");
          xTitledPanel2.add(CB02, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CB03 ----
          CB03.setComponentPopupMenu(BTD);
          CB03.setPreferredSize(new Dimension(40, 28));
          CB03.setMinimumSize(new Dimension(40, 28));
          CB03.setMaximumSize(new Dimension(40, 28));
          CB03.setName("CB03");
          xTitledPanel2.add(CB03, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CB04 ----
          CB04.setComponentPopupMenu(BTD);
          CB04.setPreferredSize(new Dimension(40, 28));
          CB04.setMinimumSize(new Dimension(40, 28));
          CB04.setMaximumSize(new Dimension(40, 28));
          CB04.setName("CB04");
          xTitledPanel2.add(CB04, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CHTCA ----
          CHTCA.setComponentPopupMenu(BTD);
          CHTCA.setMinimumSize(new Dimension(24, 28));
          CHTCA.setMaximumSize(new Dimension(24, 28));
          CHTCA.setPreferredSize(new Dimension(24, 28));
          CHTCA.setName("CHTCA");
          xTitledPanel2.add(CHTCA, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Num\u00e9ro");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          xTitledPanel2.add(OBJ_48_OBJ_48, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("Type");
          OBJ_53_OBJ_53.setMinimumSize(new Dimension(150, 19));
          OBJ_53_OBJ_53.setPreferredSize(new Dimension(150, 19));
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
          xTitledPanel2.add(OBJ_53_OBJ_53, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- OBJ_55_OBJ_55 ----
          OBJ_55_OBJ_55.setText("Autorisation");
          OBJ_55_OBJ_55.setMinimumSize(new Dimension(150, 19));
          OBJ_55_OBJ_55.setPreferredSize(new Dimension(150, 19));
          OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");
          xTitledPanel2.add(OBJ_55_OBJ_55, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CHAUT ----
          CHAUT.setComponentPopupMenu(BTD);
          CHAUT.setPreferredSize(new Dimension(60, 28));
          CHAUT.setMinimumSize(new Dimension(60, 28));
          CHAUT.setMaximumSize(new Dimension(60, 28));
          CHAUT.setName("CHAUT");
          xTitledPanel2.add(CHAUT, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- OBJ_57_OBJ_57 ----
          OBJ_57_OBJ_57.setText("Validit\u00e9");
          OBJ_57_OBJ_57.setMinimumSize(new Dimension(150, 19));
          OBJ_57_OBJ_57.setPreferredSize(new Dimension(150, 19));
          OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
          xTitledPanel2.add(OBJ_57_OBJ_57, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CHMOV ----
          CHMOV.setComponentPopupMenu(BTD);
          CHMOV.setMinimumSize(new Dimension(34, 28));
          CHMOV.setMaximumSize(new Dimension(34, 28));
          CHMOV.setPreferredSize(new Dimension(34, 28));
          CHMOV.setName("CHMOV");
          xTitledPanel2.add(CHMOV, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CHANVR ----
          CHANVR.setComponentPopupMenu(BTD);
          CHANVR.setMinimumSize(new Dimension(34, 28));
          CHANVR.setMaximumSize(new Dimension(34, 28));
          CHANVR.setPreferredSize(new Dimension(34, 28));
          CHANVR.setName("CHANVR");
          xTitledPanel2.add(CHANVR, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(xTitledPanel2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelTitre3 ----
        sNLabelTitre3.setText("Client");
        sNLabelTitre3.setName("sNLabelTitre3");
        p_contenu.add(sNLabelTitre3, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== xTitledPanel3 ========
        {
          xTitledPanel3.setName("xTitledPanel3");
          xTitledPanel3.setLayout(null);
          
          // ---- CHNCA ----
          CHNCA.setComponentPopupMenu(BTD);
          CHNCA.setName("CHNCA");
          xTitledPanel3.add(CHNCA);
          CHNCA.setBounds(15, 38, 60, CHNCA.getPreferredSize().height);
          
          // ---- CHCLA ----
          CHCLA.setComponentPopupMenu(BTD);
          CHCLA.setName("CHCLA");
          xTitledPanel3.add(CHCLA);
          CHCLA.setBounds(85, 38, 160, CHCLA.getPreferredSize().height);
          
          // ---- CHNOM ----
          CHNOM.setComponentPopupMenu(BTD);
          CHNOM.setName("CHNOM");
          xTitledPanel3.add(CHNOM);
          CHNOM.setBounds(255, 38, 250, CHNOM.getPreferredSize().height);
          
          // ---- OBJ_88_OBJ_88 ----
          OBJ_88_OBJ_88.setText("Nom ou raison sociale");
          OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
          xTitledPanel3.add(OBJ_88_OBJ_88);
          OBJ_88_OBJ_88.setBounds(255, 18, 186, 20);
          
          // ---- OBJ_87_OBJ_87 ----
          OBJ_87_OBJ_87.setText("Classement");
          OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
          xTitledPanel3.add(OBJ_87_OBJ_87);
          OBJ_87_OBJ_87.setBounds(85, 18, 145, 20);
          
          // ---- OBJ_63_OBJ_63 ----
          OBJ_63_OBJ_63.setText("Num\u00e9ro");
          OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
          xTitledPanel3.add(OBJ_63_OBJ_63);
          OBJ_63_OBJ_63.setBounds(15, 18, 55, 20);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < xTitledPanel3.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel3.setMinimumSize(preferredSize);
            xTitledPanel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel3, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelTitre4 ----
        sNLabelTitre4.setText("Domiciliation");
        sNLabelTitre4.setName("sNLabelTitre4");
        p_contenu.add(sNLabelTitre4, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== xTitledPanel4 ========
        {
          xTitledPanel4.setName("xTitledPanel4");
          xTitledPanel4.setLayout(new GridBagLayout());
          ((GridBagLayout) xTitledPanel4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) xTitledPanel4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- CHDOM ----
          CHDOM.setComponentPopupMenu(BTD);
          CHDOM.setMinimumSize(new Dimension(250, 28));
          CHDOM.setPreferredSize(new Dimension(250, 28));
          CHDOM.setName("CHDOM");
          xTitledPanel4.add(CHDOM, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- button2 ----
          button2.setName("button2");
          xTitledPanel4.add(button2, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- CHCBQ ----
          CHCBQ.setComponentPopupMenu(BTD);
          CHCBQ.setPreferredSize(new Dimension(50, 28));
          CHCBQ.setMinimumSize(new Dimension(50, 28));
          CHCBQ.setName("CHCBQ");
          xTitledPanel4.add(CHCBQ, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- CHCGU ----
          CHCGU.setComponentPopupMenu(BTD);
          CHCGU.setMinimumSize(new Dimension(50, 28));
          CHCGU.setPreferredSize(new Dimension(50, 28));
          CHCGU.setName("CHCGU");
          xTitledPanel4.add(CHCGU, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- CHNCB ----
          CHNCB.setComponentPopupMenu(BTD);
          CHNCB.setPreferredSize(new Dimension(120, 28));
          CHNCB.setMinimumSize(new Dimension(120, 28));
          CHNCB.setName("CHNCB");
          xTitledPanel4.add(CHNCB, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- CHRIB ----
          CHRIB.setComponentPopupMenu(BTD);
          CHRIB.setMinimumSize(new Dimension(35, 28));
          CHRIB.setPreferredSize(new Dimension(35, 28));
          CHRIB.setName("CHRIB");
          xTitledPanel4.add(CHRIB, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- CHRIE ----
          CHRIE.setComponentPopupMenu(BTD);
          CHRIE.setName("CHRIE");
          xTitledPanel4.add(CHRIE, new GridBagConstraints(2, 2, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- OBJ_70_OBJ_70 ----
          OBJ_70_OBJ_70.setText("Nom de la banque");
          OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");
          xTitledPanel4.add(OBJ_70_OBJ_70, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- OBJ_73_OBJ_73 ----
          OBJ_73_OBJ_73.setText("N\u00b0 Compte");
          OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
          xTitledPanel4.add(OBJ_73_OBJ_73, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- OBJ_72_OBJ_72 ----
          OBJ_72_OBJ_72.setText("Guichet");
          OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
          xTitledPanel4.add(OBJ_72_OBJ_72, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- OBJ_71_OBJ_71 ----
          OBJ_71_OBJ_71.setText("Etb");
          OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
          xTitledPanel4.add(OBJ_71_OBJ_71, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- OBJ_74_OBJ_74 ----
          OBJ_74_OBJ_74.setText("Cl\u00e9");
          OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");
          xTitledPanel4.add(OBJ_74_OBJ_74, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- OBJ_80_OBJ_80 ----
          OBJ_80_OBJ_80.setText("@UCLEX@");
          OBJ_80_OBJ_80.setPreferredSize(new Dimension(35, 24));
          OBJ_80_OBJ_80.setMinimumSize(new Dimension(35, 20));
          OBJ_80_OBJ_80.setMaximumSize(new Dimension(35, 20));
          OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
          xTitledPanel4.add(OBJ_80_OBJ_80, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
        }
        p_contenu.add(xTitledPanel4, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    
    // ---- OBJ_33_OBJ_33 ----
    OBJ_33_OBJ_33.setText("@MONT@");
    OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
    
    // ---- OBJ_34_OBJ_34 ----
    OBJ_34_OBJ_34.setText("@MONT2@");
    OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu p_contenu;
  private SNLabelTitre sNLabelTitre1;
  private SNPanelTitre xTitledPanel1;
  private RiZoneSortie INDETB;
  private XRiTextField CHVDE;
  private XRiTextField CHMTT;
  private XRiCalendrier CHDRGX;
  private XRiCalendrier CHDECX;
  private SNLabelChamp OBJ_36_OBJ_36;
  private SNLabelChamp OBJ_28_OBJ_28;
  private SNLabelChamp OBJ_44_OBJ_44;
  private SNLabelChamp OBJ_42_OBJ_42;
  private SNLabelChamp OBJ_26_OBJ_26;
  private XRiTextField INDNUM;
  private XRiTextField P11MTT;
  private SNLabelChamp lbMontantDepasse;
  private SNLabelChamp OBJ_37_OBJ_37;
  private SNLabelChamp label1;
  private RiZoneSortie CHTRG;
  private XRiTextField CHCL2;
  private SNLabelTitre sNLabelTitre2;
  private SNPanelTitre xTitledPanel2;
  private XRiTextField CB01;
  private XRiTextField CB02;
  private XRiTextField CB03;
  private XRiTextField CB04;
  private XRiTextField CHTCA;
  private SNLabelChamp OBJ_48_OBJ_48;
  private SNLabelChamp OBJ_53_OBJ_53;
  private SNLabelChamp OBJ_55_OBJ_55;
  private XRiTextField CHAUT;
  private SNLabelChamp OBJ_57_OBJ_57;
  private XRiTextField CHMOV;
  private XRiTextField CHANVR;
  private SNLabelTitre sNLabelTitre3;
  private SNPanelTitre xTitledPanel3;
  private XRiTextField CHNCA;
  private XRiTextField CHCLA;
  private XRiTextField CHNOM;
  private SNLabelUnite OBJ_88_OBJ_88;
  private SNLabelUnite OBJ_87_OBJ_87;
  private SNLabelUnite OBJ_63_OBJ_63;
  private SNLabelTitre sNLabelTitre4;
  private SNPanelTitre xTitledPanel4;
  private XRiTextField CHDOM;
  private SNBoutonDetail button2;
  private XRiTextField CHCBQ;
  private XRiTextField CHCGU;
  private XRiTextField CHNCB;
  private XRiTextField CHRIB;
  private XRiTextField CHRIE;
  private SNLabelUnite OBJ_70_OBJ_70;
  private SNLabelUnite OBJ_73_OBJ_73;
  private SNLabelUnite OBJ_72_OBJ_72;
  private SNLabelUnite OBJ_71_OBJ_71;
  private SNLabelUnite OBJ_74_OBJ_74;
  private RiZoneSortie OBJ_80_OBJ_80;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_34_OBJ_34;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
