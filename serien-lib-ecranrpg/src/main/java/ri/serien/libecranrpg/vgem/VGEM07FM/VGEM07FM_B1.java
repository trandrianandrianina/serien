
package ri.serien.libecranrpg.vgem.VGEM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEM07FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  // private String[] EFACC_Value={"","0","1","2","4",};
  
  public VGEM07FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_116_OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
    WDO2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDO2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_116_OBJ_116.setVisible(lexique.isPresent("UCLEX"));
    EFRIB.setEnabled(lexique.isPresent("EFRIB"));
    EFSRE.setVisible(lexique.isPresent("EFSRE"));
    EFBRE.setVisible(lexique.isPresent("EFBRE"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    EFNC4X.setVisible(lexique.isPresent("EFNC4X"));
    EFNC3X.setVisible(lexique.isPresent("EFNC3X"));
    EFNC2X.setVisible(lexique.isPresent("EFNC2X"));
    EFCGU.setEnabled(lexique.isPresent("EFCGU"));
    EFCBQ.setEnabled(lexique.isPresent("EFCBQ"));
    EFNCG.setVisible(lexique.isTrue("N31"));
    INDNUM.setVisible(lexique.isPresent("INDNUM"));
    // EFDIMX.setVisible( lexique.isPresent("EFDIMX"));
    // EFDREX.setVisible( lexique.isPresent("EFDREX"));
    // EFDECX.setEnabled( lexique.isPresent("EFDECX"));
    // EFDEMX.setEnabled( lexique.isPresent("EFDEMX"));
    EFETB2.setVisible(lexique.isPresent("EFETB2"));
    EFMTT.setEnabled(lexique.isPresent("EFMTT"));
    EFNCB.setEnabled(lexique.isPresent("EFNCB"));
    EFRTI.setEnabled(lexique.isPresent("EFRTI"));
    EFCLA.setEnabled(lexique.isPresent("EFCLA"));
    EFDOM.setEnabled(lexique.isPresent("EFDOM"));
    EFRIE.setEnabled(lexique.isPresent("EFRIE"));
    EFAD3.setVisible(lexique.isPresent("EFAD3"));
    EFAD2.setVisible(lexique.isPresent("EFAD2"));
    EFAD1.setVisible(lexique.isPresent("EFAD1"));
    EFNOM.setEnabled(lexique.isPresent("EFNOM"));
    OBJ_111_OBJ_111.setVisible(lexique.isTrue("N31"));
    OBJ_111_OBJ_112.setVisible(lexique.isTrue("31"));
    OBJ_133.setEnabled(lexique.isTrue("N53"));
    OBJ_98_OBJ_98.setVisible(lexique.isTrue("(51) AND (80)"));
    EFETB2.setVisible(lexique.isTrue("(51) AND (80)"));
    
    l_impaye.setIcon(lexique.chargerImage("images/impaye3.png", true));
    p_impaye.setVisible(lexique.HostFieldGetData("EFIMP").trim().equalsIgnoreCase("I"));
    layeredPane1.setComponentZOrder(p_impaye, 1);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_133ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 17);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_117ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_111ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_120ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40_OBJ_40 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_42_OBJ_42 = new JLabel();
    INDNUM = new XRiTextField();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    layeredPane1 = new JLayeredPane();
    p_impaye = new JPanel();
    l_impaye = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    EFRIE = new XRiTextField();
    EFDOM = new XRiTextField();
    OBJ_105_OBJ_105 = new JLabel();
    OBJ_108_OBJ_108 = new JLabel();
    EFNCB = new XRiTextField();
    OBJ_107_OBJ_107 = new JLabel();
    EFCBQ = new XRiTextField();
    EFCGU = new XRiTextField();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_109_OBJ_109 = new JLabel();
    EFRIB = new XRiTextField();
    OBJ_116_OBJ_116 = new RiZoneSortie();
    OBJ_133 = new SNBoutonDetail();
    WDO2 = new RiZoneSortie();
    OBJ_105_OBJ_106 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    EFNOM = new XRiTextField();
    OBJ_88_OBJ_88 = new JLabel();
    EFCLA = new XRiTextField();
    OBJ_87_OBJ_87 = new JLabel();
    EFAD1 = new XRiTextField();
    OBJ_95_OBJ_95 = new JLabel();
    EFRTI = new XRiTextField();
    EFAD2 = new XRiTextField();
    OBJ_98_OBJ_98 = new JLabel();
    EFETB2 = new XRiTextField();
    EFAD3 = new XRiTextField();
    panel1 = new JPanel();
    EFNCG = new XRiTextField();
    EFNCA = new XRiTextField();
    OBJ_111_OBJ_111 = new JLabel();
    OBJ_111_OBJ_112 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_58_OBJ_58 = new JLabel();
    EFMTT = new XRiTextField();
    EFDEMX = new XRiCalendrier();
    EFDECX = new XRiCalendrier();
    OBJ_65_OBJ_65 = new JLabel();
    EFDREX = new XRiCalendrier();
    OBJ_67_OBJ_67 = new JLabel();
    EFBRE = new XRiTextField();
    OBJ_69_OBJ_69 = new JLabel();
    EFSRE = new XRiTextField();
    OBJ_71_OBJ_71 = new JLabel();
    EFNC2X = new XRiTextField();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_76_OBJ_76 = new JLabel();
    EFDIMX = new XRiCalendrier();
    OBJ_74_OBJ_74 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    EFNC4X = new XRiTextField();
    EFNC3X = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des pr\u00e9l\u00e8vements");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Etablissement");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("Num\u00e9ro");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Pr\u00e9lev\u00e8ment ordinaire");
              riSousMenu_bt6.setToolTipText("Pr\u00e9lev\u00e8ment ordinaire");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Pr\u00e9lev\u00e8ment acc\u00e9l\u00e9r\u00e9");
              riSousMenu_bt7.setToolTipText("Pr\u00e9lev\u00e8ment acc\u00e9l\u00e9r\u00e9");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== layeredPane1 ========
          {
            layeredPane1.setName("layeredPane1");

            //======== p_impaye ========
            {
              p_impaye.setOpaque(false);
              p_impaye.setPreferredSize(new Dimension(800, 510));
              p_impaye.setName("p_impaye");
              p_impaye.setLayout(null);

              //---- l_impaye ----
              l_impaye.setPreferredSize(new Dimension(800, 510));
              l_impaye.setName("l_impaye");
              p_impaye.add(l_impaye);
              l_impaye.setBounds(0, 0, 800, 510);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < p_impaye.getComponentCount(); i++) {
                  Rectangle bounds = p_impaye.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = p_impaye.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                p_impaye.setMinimumSize(preferredSize);
                p_impaye.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(p_impaye, JLayeredPane.DEFAULT_LAYER);
            p_impaye.setBounds(new Rectangle(new Point(10, 10), p_impaye.getPreferredSize()));
          }
          p_contenu.add(layeredPane1);
          layeredPane1.setBounds(0, 0, 820, 530);

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitle("Domiciliation");
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

            //---- EFRIE ----
            EFRIE.setComponentPopupMenu(BTD);
            EFRIE.setName("EFRIE");

            //---- EFDOM ----
            EFDOM.setComponentPopupMenu(BTD);
            EFDOM.setName("EFDOM");

            //---- OBJ_105_OBJ_105 ----
            OBJ_105_OBJ_105.setText("Nom de la banque");
            OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("N\u00b0 Compte");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");

            //---- EFNCB ----
            EFNCB.setComponentPopupMenu(BTD);
            EFNCB.setName("EFNCB");

            //---- OBJ_107_OBJ_107 ----
            OBJ_107_OBJ_107.setText("N\u00b0Guichet");
            OBJ_107_OBJ_107.setName("OBJ_107_OBJ_107");

            //---- EFCBQ ----
            EFCBQ.setComponentPopupMenu(BTD);
            EFCBQ.setName("EFCBQ");

            //---- EFCGU ----
            EFCGU.setComponentPopupMenu(BTD);
            EFCGU.setName("EFCGU");

            //---- OBJ_106_OBJ_106 ----
            OBJ_106_OBJ_106.setText("N\u00b0 Etb");
            OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");

            //---- OBJ_109_OBJ_109 ----
            OBJ_109_OBJ_109.setText("Cl\u00e9");
            OBJ_109_OBJ_109.setName("OBJ_109_OBJ_109");

            //---- EFRIB ----
            EFRIB.setComponentPopupMenu(BTD);
            EFRIB.setName("EFRIB");

            //---- OBJ_116_OBJ_116 ----
            OBJ_116_OBJ_116.setText("@UCLEX@");
            OBJ_116_OBJ_116.setName("OBJ_116_OBJ_116");

            //---- OBJ_133 ----
            OBJ_133.setText("");
            OBJ_133.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_133.setName("OBJ_133");
            OBJ_133.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_133ActionPerformed(e);
              }
            });

            //---- WDO2 ----
            WDO2.setText("@WDO2@");
            WDO2.setName("WDO2");

            //---- OBJ_105_OBJ_106 ----
            OBJ_105_OBJ_106.setText("SWIFT");
            OBJ_105_OBJ_106.setName("OBJ_105_OBJ_106");

            GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
            xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
            xTitledPanel3ContentContainerLayout.setHorizontalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_105_OBJ_105, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_105_OBJ_106, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
                  .addGap(16, 16, 16)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addComponent(EFDOM, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WDO2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_133, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(30, 30, 30)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_107_OBJ_107, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(OBJ_109_OBJ_109, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addComponent(EFCBQ, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(EFCGU, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(EFNCB, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(EFRIB, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_116_OBJ_116, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                    .addComponent(EFRIE, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)))
            );
            xTitledPanel3ContentContainerLayout.setVerticalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(OBJ_105_OBJ_105, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_105_OBJ_106, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(EFDOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(WDO2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_133, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_107_OBJ_107, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_109_OBJ_109, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                        .addComponent(EFCBQ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EFCGU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EFNCB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EFRIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                          .addGap(2, 2, 2)
                          .addComponent(OBJ_116_OBJ_116, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(2, 2, 2)
                      .addComponent(EFRIE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(4, Short.MAX_VALUE))
            );
          }
          p_contenu.add(xTitledPanel3);
          xTitledPanel3.setBounds(7, 407, 806, 116);

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("D\u00e9biteur");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- EFNOM ----
            EFNOM.setComponentPopupMenu(BTD);
            EFNOM.setName("EFNOM");
            xTitledPanel2ContentContainer.add(EFNOM);
            EFNOM.setBounds(450, 23, 250, EFNOM.getPreferredSize().height);

            //---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("Nom ou raison sociale");
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
            xTitledPanel2ContentContainer.add(OBJ_88_OBJ_88);
            OBJ_88_OBJ_88.setBounds(450, 3, 163, 20);

            //---- EFCLA ----
            EFCLA.setComponentPopupMenu(BTD);
            EFCLA.setName("EFCLA");
            xTitledPanel2ContentContainer.add(EFCLA);
            EFCLA.setBounds(255, 23, 160, EFCLA.getPreferredSize().height);

            //---- OBJ_87_OBJ_87 ----
            OBJ_87_OBJ_87.setText("Classement");
            OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
            xTitledPanel2ContentContainer.add(OBJ_87_OBJ_87);
            OBJ_87_OBJ_87.setBounds(255, 3, 93, 20);

            //---- EFAD1 ----
            EFAD1.setName("EFAD1");
            xTitledPanel2ContentContainer.add(EFAD1);
            EFAD1.setBounds(450, 53, 250, EFAD1.getPreferredSize().height);

            //---- OBJ_95_OBJ_95 ----
            OBJ_95_OBJ_95.setText("R\u00e9f\u00e9rence");
            OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");
            xTitledPanel2ContentContainer.add(OBJ_95_OBJ_95);
            OBJ_95_OBJ_95.setBounds(13, 87, 157, 20);

            //---- EFRTI ----
            EFRTI.setComponentPopupMenu(BTD);
            EFRTI.setName("EFRTI");
            xTitledPanel2ContentContainer.add(EFRTI);
            EFRTI.setBounds(255, 83, 110, EFRTI.getPreferredSize().height);

            //---- EFAD2 ----
            EFAD2.setName("EFAD2");
            xTitledPanel2ContentContainer.add(EFAD2);
            EFAD2.setBounds(450, 83, 250, EFAD2.getPreferredSize().height);

            //---- OBJ_98_OBJ_98 ----
            OBJ_98_OBJ_98.setText("Etablissement sur GVM");
            OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
            xTitledPanel2ContentContainer.add(OBJ_98_OBJ_98);
            OBJ_98_OBJ_98.setBounds(13, 117, 212, 20);

            //---- EFETB2 ----
            EFETB2.setComponentPopupMenu(BTD);
            EFETB2.setName("EFETB2");
            xTitledPanel2ContentContainer.add(EFETB2);
            EFETB2.setBounds(255, 113, 40, EFETB2.getPreferredSize().height);

            //---- EFAD3 ----
            EFAD3.setName("EFAD3");
            xTitledPanel2ContentContainer.add(EFAD3);
            EFAD3.setBounds(450, 113, 250, EFAD3.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");

              //---- EFNCG ----
              EFNCG.setComponentPopupMenu(BTD);
              EFNCG.setName("EFNCG");

              //---- EFNCA ----
              EFNCA.setComponentPopupMenu(BTD);
              EFNCA.setName("EFNCA");

              //---- OBJ_111_OBJ_111 ----
              OBJ_111_OBJ_111.setText("Num\u00e9ro compte client");
              OBJ_111_OBJ_111.setName("OBJ_111_OBJ_111");

              //---- OBJ_111_OBJ_112 ----
              OBJ_111_OBJ_112.setText("Num\u00e9ro");
              OBJ_111_OBJ_112.setName("OBJ_111_OBJ_112");

              GroupLayout panel1Layout = new GroupLayout(panel1);
              panel1.setLayout(panel1Layout);
              panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(OBJ_111_OBJ_111, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_111_OBJ_112, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(EFNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(EFNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))))
              );
              panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(OBJ_111_OBJ_111, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_111_OBJ_112, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(EFNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EFNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            xTitledPanel2ContentContainer.add(panel1);
            panel1.setBounds(5, 5, 200, 75);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel2);
          xTitledPanel2.setBounds(7, 205, 806, 190);

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Identification");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("Montant");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");

            //---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("Ech\u00e9ance");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("Emission");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");

            //---- EFMTT ----
            EFMTT.setComponentPopupMenu(BTD);
            EFMTT.setName("EFMTT");

            //---- EFDEMX ----
            EFDEMX.setToolTipText("Date");
            EFDEMX.setComponentPopupMenu(BTD);
            EFDEMX.setName("EFDEMX");

            //---- EFDECX ----
            EFDECX.setToolTipText("Date");
            EFDECX.setComponentPopupMenu(BTD);
            EFDECX.setName("EFDECX");

            //---- OBJ_65_OBJ_65 ----
            OBJ_65_OBJ_65.setText("S\u00e9lection");
            OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

            //---- EFDREX ----
            EFDREX.setToolTipText("Date");
            EFDREX.setComponentPopupMenu(BTD);
            EFDREX.setName("EFDREX");

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("Banque");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

            //---- EFBRE ----
            EFBRE.setComponentPopupMenu(BTD);
            EFBRE.setName("EFBRE");

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("Suffixe");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");

            //---- EFSRE ----
            EFSRE.setName("EFSRE");

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("Comptabilis\u00e9");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

            //---- EFNC2X ----
            EFNC2X.setComponentPopupMenu(BTD);
            EFNC2X.setName("EFNC2X");

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("R\u00e8glement \u00e0 \u00e9ch\u00e9ance");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");

            //---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("R\u00e8glement manuel");
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

            //---- EFDIMX ----
            EFDIMX.setToolTipText("Date");
            EFDIMX.setName("EFDIMX");

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("Comptabilis\u00e9");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

            //---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("Comptabilis\u00e9");
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

            //---- EFNC4X ----
            EFNC4X.setComponentPopupMenu(BTD);
            EFNC4X.setName("EFNC4X");

            //---- EFNC3X ----
            EFNC3X.setComponentPopupMenu(BTD);
            EFNC3X.setName("EFNC3X");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_57_OBJ_57, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                      .addGap(30, 30, 30)
                      .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addGap(57, 57, 57)
                      .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(EFMTT, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(EFDEMX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(EFDECX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                      .addGap(70, 70, 70)
                      .addComponent(EFDREX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                      .addGap(14, 14, 14)
                      .addComponent(EFBRE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                      .addGap(14, 14, 14)
                      .addComponent(EFSRE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(EFNC2X, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                      .addGap(330, 330, 330)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(EFNC3X, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(EFDIMX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(245, 245, 245)
                      .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(EFNC4X, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_57_OBJ_57, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(EFMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EFDEMX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EFDECX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(EFDREX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EFBRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EFSRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EFNC2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(EFNC3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(EFDIMX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EFNC4X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))))
            );
          }
          p_contenu.add(xTitledPanel1);
          xTitledPanel1.setBounds(7, 7, 806, 186);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField INDETB;
  private JLabel OBJ_42_OBJ_42;
  private XRiTextField INDNUM;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane layeredPane1;
  private JPanel p_impaye;
  private JLabel l_impaye;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField EFRIE;
  private XRiTextField EFDOM;
  private JLabel OBJ_105_OBJ_105;
  private JLabel OBJ_108_OBJ_108;
  private XRiTextField EFNCB;
  private JLabel OBJ_107_OBJ_107;
  private XRiTextField EFCBQ;
  private XRiTextField EFCGU;
  private JLabel OBJ_106_OBJ_106;
  private JLabel OBJ_109_OBJ_109;
  private XRiTextField EFRIB;
  private RiZoneSortie OBJ_116_OBJ_116;
  private SNBoutonDetail OBJ_133;
  private RiZoneSortie WDO2;
  private JLabel OBJ_105_OBJ_106;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField EFNOM;
  private JLabel OBJ_88_OBJ_88;
  private XRiTextField EFCLA;
  private JLabel OBJ_87_OBJ_87;
  private XRiTextField EFAD1;
  private JLabel OBJ_95_OBJ_95;
  private XRiTextField EFRTI;
  private XRiTextField EFAD2;
  private JLabel OBJ_98_OBJ_98;
  private XRiTextField EFETB2;
  private XRiTextField EFAD3;
  private JPanel panel1;
  private XRiTextField EFNCG;
  private XRiTextField EFNCA;
  private JLabel OBJ_111_OBJ_111;
  private JLabel OBJ_111_OBJ_112;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_58_OBJ_58;
  private XRiTextField EFMTT;
  private XRiCalendrier EFDEMX;
  private XRiCalendrier EFDECX;
  private JLabel OBJ_65_OBJ_65;
  private XRiCalendrier EFDREX;
  private JLabel OBJ_67_OBJ_67;
  private XRiTextField EFBRE;
  private JLabel OBJ_69_OBJ_69;
  private XRiTextField EFSRE;
  private JLabel OBJ_71_OBJ_71;
  private XRiTextField EFNC2X;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_76_OBJ_76;
  private XRiCalendrier EFDIMX;
  private JLabel OBJ_74_OBJ_74;
  private JLabel OBJ_78_OBJ_78;
  private XRiTextField EFNC4X;
  private XRiTextField EFNC3X;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
