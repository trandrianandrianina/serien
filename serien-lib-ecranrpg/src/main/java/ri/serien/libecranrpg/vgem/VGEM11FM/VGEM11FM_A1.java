
package ri.serien.libecranrpg.vgem.VGEM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEM11FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGEM11FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX8.setValeurs("8", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX6.setValeurs("6", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    lbAffectation.setVisible(!lexique.HostFieldGetData("AFFE").trim().equalsIgnoreCase(""));
    barre_dup.setVisible(lexique.isTrue("56"));
    
    if (lexique.isTrue("(N92) AND (N94) AND (N95)")) {
      p_bpresentation.setText("Gestion des espèces");
    }
    else if (lexique.isTrue("92")) {
      p_bpresentation.setText("Gestion des virements");
    }
    else if (lexique.isTrue("94")) {
      p_bpresentation.setText("Gestion des cartes");
    }
    else if (lexique.isTrue("95")) {
      p_bpresentation.setText("Gestion des T.I.P.");
    }
    
    ARG71.setEnabled(lexique.isTrue("94"));
    ARG72.setEnabled(lexique.isTrue("94"));
    ARG73.setEnabled(lexique.isTrue("94"));
    ARG74.setEnabled(lexique.isTrue("94"));
    ARG71.setEnabled(lexique.isTrue("95"));
    ARG72.setEnabled(lexique.isTrue("95"));
    ARG73.setEnabled(lexique.isTrue("95"));
    ARG74.setEnabled(lexique.isTrue("95"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    lbEtablissement = new JLabel();
    INDETB = new XRiTextField();
    lbNumeroOperation = new JLabel();
    INDIDU = new XRiTextField();
    lbDateOperation = new JLabel();
    DRGX = new XRiCalendrier();
    lbVendeur = new JLabel();
    WVDE = new XRiTextField();
    p_tete_droite = new JPanel();
    lbAffectation = new JLabel();
    barre_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    lbDuplication = new JLabel();
    IN3ETB = new XRiTextField();
    lbNumeroOperationDup = new JLabel();
    IN3IDU = new XRiTextField();
    p_tete_droite2 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    sNLabelTitre1 = new SNLabelTitre();
    panel2 = new JPanel();
    TIDX6 = new XRiRadioButton();
    ARG61 = new XRiTextField();
    TIDX2 = new XRiRadioButton();
    ARG21 = new XRiTextField();
    TIDX3 = new XRiRadioButton();
    ARG3 = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG5 = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    TIDX7 = new XRiRadioButton();
    ARG71 = new XRiTextField();
    ARG72 = new XRiTextField();
    ARG73 = new XRiTextField();
    ARG74 = new XRiTextField();
    TIDX8 = new XRiRadioButton();
    ARG8 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des esp\u00e8ces");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- lbNumeroOperation ----
          lbNumeroOperation.setText("Num\u00e9ro op\u00e9ration");
          lbNumeroOperation.setName("lbNumeroOperation");

          //---- INDIDU ----
          INDIDU.setComponentPopupMenu(BTD);
          INDIDU.setName("INDIDU");

          //---- lbDateOperation ----
          lbDateOperation.setText("Date op\u00e9ration");
          lbDateOperation.setName("lbDateOperation");

          //---- DRGX ----
          DRGX.setComponentPopupMenu(BTD);
          DRGX.setName("DRGX");

          //---- lbVendeur ----
          lbVendeur.setText("Vendeur");
          lbVendeur.setName("lbVendeur");

          //---- WVDE ----
          WVDE.setComponentPopupMenu(BTD);
          WVDE.setName("WVDE");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lbEtablissement, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(lbNumeroOperation, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(lbDateOperation, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(95, 95, 95)
                    .addComponent(DRGX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(lbVendeur, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WVDE, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DRGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WVDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(lbEtablissement, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(lbNumeroOperation, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(lbDateOperation, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(lbVendeur, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
              .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lbAffectation ----
          lbAffectation.setText("Affectation");
          lbAffectation.setFont(lbAffectation.getFont().deriveFont(lbAffectation.getFont().getSize() + 3f));
          lbAffectation.setName("lbAffectation");
          p_tete_droite.add(lbAffectation);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- lbDuplication ----
          lbDuplication.setText("Par duplication de");
          lbDuplication.setName("lbDuplication");

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD);
          IN3ETB.setName("IN3ETB");

          //---- lbNumeroOperationDup ----
          lbNumeroOperationDup.setText("Num\u00e9ro op\u00e9ration");
          lbNumeroOperationDup.setName("lbNumeroOperationDup");

          //---- IN3IDU ----
          IN3IDU.setComponentPopupMenu(BTD);
          IN3IDU.setName("IN3IDU");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lbDuplication, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(lbNumeroOperationDup, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IN3IDU, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGroup(p_tete_gauche2Layout.createParallelGroup()
                  .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_tete_gauche2Layout.createParallelGroup()
                      .addComponent(lbNumeroOperationDup, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(lbDuplication, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
                  .addComponent(IN3IDU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        barre_dup.add(p_tete_gauche2);

        //======== p_tete_droite2 ========
        {
          p_tete_droite2.setOpaque(false);
          p_tete_droite2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite2.setPreferredSize(new Dimension(150, 0));
          p_tete_droite2.setMinimumSize(new Dimension(150, 0));
          p_tete_droite2.setName("p_tete_droite2");
          p_tete_droite2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_dup.add(p_tete_droite2);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- sNLabelTitre1 ----
        sNLabelTitre1.setText("Recherche multi-crit\u00e8res");
        sNLabelTitre1.setName("sNLabelTitre1");
        pnlContenu.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(new GridBagLayout());
          ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- TIDX6 ----
          TIDX6.setText("Num\u00e9ro d'op\u00e9ration");
          TIDX6.setToolTipText("Tri\u00e9 par");
          TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX6.setMinimumSize(new Dimension(200, 30));
          TIDX6.setPreferredSize(new Dimension(200, 30));
          TIDX6.setName("TIDX6");
          panel2.add(TIDX6, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG61 ----
          ARG61.setComponentPopupMenu(BTD);
          ARG61.setPreferredSize(new Dimension(85, 28));
          ARG61.setMinimumSize(new Dimension(85, 28));
          ARG61.setName("ARG61");
          panel2.add(ARG61, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX2 ----
          TIDX2.setText("Num\u00e9ro client");
          TIDX2.setToolTipText("Tri\u00e9 par");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setMinimumSize(new Dimension(200, 30));
          TIDX2.setPreferredSize(new Dimension(200, 30));
          TIDX2.setName("TIDX2");
          panel2.add(TIDX2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG21 ----
          ARG21.setComponentPopupMenu(BTD);
          ARG21.setMinimumSize(new Dimension(60, 28));
          ARG21.setPreferredSize(new Dimension(60, 28));
          ARG21.setName("ARG21");
          panel2.add(ARG21, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX3 ----
          TIDX3.setText("Nom du client");
          TIDX3.setToolTipText("Tri\u00e9 par");
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setMinimumSize(new Dimension(200, 30));
          TIDX3.setPreferredSize(new Dimension(200, 30));
          TIDX3.setName("TIDX3");
          panel2.add(TIDX3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG3 ----
          ARG3.setComponentPopupMenu(BTD);
          ARG3.setPreferredSize(new Dimension(160, 28));
          ARG3.setMinimumSize(new Dimension(160, 28));
          ARG3.setName("ARG3");
          panel2.add(ARG3, new GridBagConstraints(1, 2, 4, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- TIDX5 ----
          TIDX5.setText("Montant");
          TIDX5.setToolTipText("Tri\u00e9 par");
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setMinimumSize(new Dimension(200, 30));
          TIDX5.setPreferredSize(new Dimension(200, 30));
          TIDX5.setName("TIDX5");
          panel2.add(TIDX5, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG5 ----
          ARG5.setComponentPopupMenu(BTD);
          ARG5.setMinimumSize(new Dimension(120, 28));
          ARG5.setPreferredSize(new Dimension(120, 28));
          ARG5.setName("ARG5");
          panel2.add(ARG5, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- TIDX4 ----
          TIDX4.setText("Mot de classement 2");
          TIDX4.setToolTipText("Tri\u00e9 par");
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setMinimumSize(new Dimension(200, 30));
          TIDX4.setPreferredSize(new Dimension(200, 30));
          TIDX4.setName("TIDX4");
          panel2.add(TIDX4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(BTD);
          ARG4.setPreferredSize(new Dimension(160, 28));
          ARG4.setMinimumSize(new Dimension(160, 28));
          ARG4.setName("ARG4");
          panel2.add(ARG4, new GridBagConstraints(1, 4, 4, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- TIDX7 ----
          TIDX7.setText("Identification de remise");
          TIDX7.setToolTipText("Tri\u00e9 par");
          TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX7.setMinimumSize(new Dimension(200, 30));
          TIDX7.setPreferredSize(new Dimension(200, 30));
          TIDX7.setName("TIDX7");
          panel2.add(TIDX7, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG71 ----
          ARG71.setComponentPopupMenu(BTD);
          ARG71.setMinimumSize(new Dimension(35, 28));
          ARG71.setPreferredSize(new Dimension(35, 28));
          ARG71.setName("ARG71");
          panel2.add(ARG71, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG72 ----
          ARG72.setComponentPopupMenu(BTD);
          ARG72.setPreferredSize(new Dimension(75, 28));
          ARG72.setMinimumSize(new Dimension(75, 28));
          ARG72.setName("ARG72");
          panel2.add(ARG72, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG73 ----
          ARG73.setComponentPopupMenu(BTD);
          ARG73.setMinimumSize(new Dimension(25, 28));
          ARG73.setPreferredSize(new Dimension(25, 28));
          ARG73.setName("ARG73");
          panel2.add(ARG73, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG74 ----
          ARG74.setComponentPopupMenu(BTD);
          ARG74.setPreferredSize(new Dimension(35, 28));
          ARG74.setName("ARG74");
          panel2.add(ARG74, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- TIDX8 ----
          TIDX8.setText("Num\u00e9ro de compte");
          TIDX8.setToolTipText("Tri\u00e9 par");
          TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8.setMinimumSize(new Dimension(200, 30));
          TIDX8.setPreferredSize(new Dimension(200, 30));
          TIDX8.setName("TIDX8");
          panel2.add(TIDX8, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- ARG8 ----
          ARG8.setComponentPopupMenu(BTD);
          ARG8.setMinimumSize(new Dimension(110, 28));
          ARG8.setPreferredSize(new Dimension(110, 28));
          ARG8.setName("ARG8");
          panel2.add(ARG8, new GridBagConstraints(1, 6, 2, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(panel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX8);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel lbEtablissement;
  private XRiTextField INDETB;
  private JLabel lbNumeroOperation;
  private XRiTextField INDIDU;
  private JLabel lbDateOperation;
  private XRiCalendrier DRGX;
  private JLabel lbVendeur;
  private XRiTextField WVDE;
  private JPanel p_tete_droite;
  private JLabel lbAffectation;
  private JMenuBar barre_dup;
  private JPanel p_tete_gauche2;
  private JLabel lbDuplication;
  private XRiTextField IN3ETB;
  private JLabel lbNumeroOperationDup;
  private XRiTextField IN3IDU;
  private JPanel p_tete_droite2;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre sNLabelTitre1;
  private JPanel panel2;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG61;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG21;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG3;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG5;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG71;
  private XRiTextField ARG72;
  private XRiTextField ARG73;
  private XRiTextField ARG74;
  private XRiRadioButton TIDX8;
  private XRiTextField ARG8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
