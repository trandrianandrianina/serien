
package ri.serien.libecranrpg.vgem.VGEM21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGEM21FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT };
  private String[] _N01_Title = { "N° Effet", "", "Client", "Montant", };
  private String[][] _N01_Data = { { "N01", "NS01", "C01", "M01", }, { "N02", "NS02", "C02", "M02", }, { "N03", "NS03", "C03", "M03", },
      { "N04", "NS04", "C04", "M04", }, { "N05", "NS05", "C05", "M05", }, { "N06", "NS06", "C06", "M06", },
      { "N07", "NS07", "C07", "M07", }, { "N08", "NS08", "C08", "M08", }, { "N09", "NS09", "C09", "M09", },
      { "N10", "NS10", "C10", "M10", }, { "N11", "NS11", "C11", "M11", }, { "N12", "NS12", "C12", "M12", }, };
  private int[] _N01_Width = { 65, 16, 143, 109, };
  
  public VGEM21FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    N01.setAspectTable(null, _N01_Title, _N01_Data, _N01_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TOTM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTM@")).trim());
    DTECH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTECH@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), null, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    DTECH.setVisible(lexique.isPresent("DTECH"));
    OBJ_23_OBJ_23.setVisible(lexique.isPresent("DTECH"));
    TOTM.setVisible(lexique.isPresent("TOTM"));
    OBJ_24.setIcon(lexique.chargerImage("images/retour.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    N01 = new XRiTable();
    TOTM = new RiZoneSortie();
    OBJ_23_OBJ_23 = new JLabel();
    DTECH = new RiZoneSortie();
    OBJ_21_OBJ_21 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_24 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(680, 360));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

            //---- N01 ----
            N01.setName("N01");
            SCROLLPANE_LIST2.setViewportView(N01);
          }
          p_recup.add(SCROLLPANE_LIST2);
          SCROLLPANE_LIST2.setBounds(15, 65, 435, 223);

          //---- TOTM ----
          TOTM.setText("@TOTM@");
          TOTM.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTM.setName("TOTM");
          p_recup.add(TOTM);
          TOTM.setBounds(318, 300, 132, TOTM.getPreferredSize().height);

          //---- OBJ_23_OBJ_23 ----
          OBJ_23_OBJ_23.setText("Ech\u00e9ance du :");
          OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");
          p_recup.add(OBJ_23_OBJ_23);
          OBJ_23_OBJ_23.setBounds(275, 37, 88, 20);

          //---- DTECH ----
          DTECH.setText("@DTECH@");
          DTECH.setName("DTECH");
          p_recup.add(DTECH);
          DTECH.setBounds(380, 35, 70, DTECH.getPreferredSize().height);

          //---- OBJ_21_OBJ_21 ----
          OBJ_21_OBJ_21.setText("Totaux");
          OBJ_21_OBJ_21.setName("OBJ_21_OBJ_21");
          p_recup.add(OBJ_21_OBJ_21);
          OBJ_21_OBJ_21.setBounds(255, 302, 50, 20);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("D\u00e9tail en attente d'acceptation");
          xTitledSeparator1.setName("xTitledSeparator1");
          p_recup.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(10, 15, 465, xTitledSeparator1.getPreferredSize().height);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(455, 65, 25, 103);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(455, 180, 25, 105);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 490, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- OBJ_24 ----
    OBJ_24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_24.setName("OBJ_24");
    OBJ_24.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_24ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable N01;
  private RiZoneSortie TOTM;
  private JLabel OBJ_23_OBJ_23;
  private RiZoneSortie DTECH;
  private JLabel OBJ_21_OBJ_21;
  private JXTitledSeparator xTitledSeparator1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton OBJ_24;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
