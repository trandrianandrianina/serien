
package ri.serien.libecranrpg.vgem.VGEMSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEMSEFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGEMSEFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité GEM (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_54_OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    SET035.setEnabled(lexique.isPresent("SET035"));
    SET034.setEnabled(lexique.isPresent("SET034"));
    SET033.setEnabled(lexique.isPresent("SET033"));
    SET032.setEnabled(lexique.isPresent("SET032"));
    SET031.setEnabled(lexique.isPresent("SET031"));
    SET026.setEnabled(lexique.isPresent("SET026"));
    SET025.setEnabled(lexique.isPresent("SET025"));
    SET024.setEnabled(lexique.isPresent("SET024"));
    SET023.setEnabled(lexique.isPresent("SET023"));
    SET022.setEnabled(lexique.isPresent("SET022"));
    SET021.setEnabled(lexique.isPresent("SET021"));
    V06F.setVisible(lexique.isPresent("V06F"));
    OBJ_54_OBJ_54.setVisible(lexique.isPresent("WPAGE"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    INDUSR.setVisible(lexique.isPresent("INDUSR"));
    OBJ_50_OBJ_50.setVisible(lexique.isPresent("DGNOM"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_26_OBJ_26 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_56_OBJ_56 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_50_OBJ_50 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_96_OBJ_96 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_102_OBJ_102 = new JLabel();
    OBJ_99_OBJ_99 = new JLabel();
    OBJ_90_OBJ_90 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    OBJ_93_OBJ_93 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_105_OBJ_105 = new JLabel();
    OBJ_84_OBJ_84 = new JLabel();
    SET021 = new XRiTextField();
    SET022 = new XRiTextField();
    SET023 = new XRiTextField();
    SET024 = new XRiTextField();
    SET025 = new XRiTextField();
    SET026 = new XRiTextField();
    SET031 = new XRiTextField();
    SET032 = new XRiTextField();
    SET033 = new XRiTextField();
    SET034 = new XRiTextField();
    SET035 = new XRiTextField();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_89_OBJ_89 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    OBJ_95_OBJ_95 = new JLabel();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_101_OBJ_101 = new JLabel();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_107_OBJ_107 = new JLabel();
    OBJ_76_OBJ_76 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_82_OBJ_82 = new JLabel();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_88_OBJ_88 = new JLabel();
    OBJ_91_OBJ_91 = new JLabel();
    OBJ_94_OBJ_94 = new JLabel();
    OBJ_97_OBJ_97 = new JLabel();
    OBJ_100_OBJ_100 = new JLabel();
    OBJ_103_OBJ_103 = new JLabel();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_49 = new SNBoutonLeger();
    V06F = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_29_OBJ_29 = new JLabel();
    OBJ_7 = new JMenuItem();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 GEM (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("Utilisateur");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setText("@INDUSR@");
          INDUSR.setOpaque(false);
          INDUSR.setName("INDUSR");

          //---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("Etablissement");
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("@DGNOM@");
          OBJ_50_OBJ_50.setOpaque(false);
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autres tris");
              riSousMenu_bt6.setToolTipText("Tri par \u00e9tablissement ou code utilisateur");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(720, 340));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle(" Objet de la s\u00e9curit\u00e9");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- OBJ_78_OBJ_78 ----
          OBJ_78_OBJ_78.setText("Gestion ESPECES, CARTES, T.I.P");
          OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
          xTitledPanel1ContentContainer.add(OBJ_78_OBJ_78);
          OBJ_78_OBJ_78.setBounds(375, 24, 205, 20);

          //---- OBJ_96_OBJ_96 ----
          OBJ_96_OBJ_96.setText("Comptab.Esp\u00e8ces, Cartes, T.I.P");
          OBJ_96_OBJ_96.setName("OBJ_96_OBJ_96");
          xTitledPanel1ContentContainer.add(OBJ_96_OBJ_96);
          OBJ_96_OBJ_96.setBounds(375, 114, 205, 20);

          //---- OBJ_81_OBJ_81 ----
          OBJ_81_OBJ_81.setText("Remise Sur Place / Hors Place");
          OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");
          xTitledPanel1ContentContainer.add(OBJ_81_OBJ_81);
          OBJ_81_OBJ_81.setBounds(50, 54, 195, 20);

          //---- OBJ_102_OBJ_102 ----
          OBJ_102_OBJ_102.setText("Purge   Esp\u00e8ces, Cartes, T.I.P");
          OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");
          xTitledPanel1ContentContainer.add(OBJ_102_OBJ_102);
          OBJ_102_OBJ_102.setBounds(375, 144, 205, 20);

          //---- OBJ_99_OBJ_99 ----
          OBJ_99_OBJ_99.setText("Comptabilisation des Ch\u00e8ques");
          OBJ_99_OBJ_99.setName("OBJ_99_OBJ_99");
          xTitledPanel1ContentContainer.add(OBJ_99_OBJ_99);
          OBJ_99_OBJ_99.setBounds(50, 144, 195, 20);

          //---- OBJ_90_OBJ_90 ----
          OBJ_90_OBJ_90.setText("Remise des Cartes Bancaires");
          OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
          xTitledPanel1ContentContainer.add(OBJ_90_OBJ_90);
          OBJ_90_OBJ_90.setBounds(375, 84, 205, 20);

          //---- OBJ_87_OBJ_87 ----
          OBJ_87_OBJ_87.setText("Edition Remises de Ch\u00e8ques");
          OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
          xTitledPanel1ContentContainer.add(OBJ_87_OBJ_87);
          OBJ_87_OBJ_87.setBounds(50, 84, 195, 20);

          //---- OBJ_93_OBJ_93 ----
          OBJ_93_OBJ_93.setText("Edition Portefeuille Ch\u00e8ques");
          OBJ_93_OBJ_93.setName("OBJ_93_OBJ_93");
          xTitledPanel1ContentContainer.add(OBJ_93_OBJ_93);
          OBJ_93_OBJ_93.setBounds(50, 114, 195, 20);

          //---- OBJ_59_OBJ_59 ----
          OBJ_59_OBJ_59.setText("Gestion des CHEQUES");
          OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");
          xTitledPanel1ContentContainer.add(OBJ_59_OBJ_59);
          OBJ_59_OBJ_59.setBounds(50, 24, 195, 20);

          //---- OBJ_105_OBJ_105 ----
          OBJ_105_OBJ_105.setText("Purge des Ch\u00e8ques");
          OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");
          xTitledPanel1ContentContainer.add(OBJ_105_OBJ_105);
          OBJ_105_OBJ_105.setBounds(50, 174, 195, 20);

          //---- OBJ_84_OBJ_84 ----
          OBJ_84_OBJ_84.setText("Etat de Caisse");
          OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");
          xTitledPanel1ContentContainer.add(OBJ_84_OBJ_84);
          OBJ_84_OBJ_84.setBounds(375, 54, 205, 20);

          //---- SET021 ----
          SET021.setComponentPopupMenu(BTD);
          SET021.setName("SET021");
          xTitledPanel1ContentContainer.add(SET021);
          SET021.setBounds(250, 20, 20, SET021.getPreferredSize().height);

          //---- SET022 ----
          SET022.setComponentPopupMenu(BTD);
          SET022.setName("SET022");
          xTitledPanel1ContentContainer.add(SET022);
          SET022.setBounds(250, 50, 20, SET022.getPreferredSize().height);

          //---- SET023 ----
          SET023.setComponentPopupMenu(BTD);
          SET023.setName("SET023");
          xTitledPanel1ContentContainer.add(SET023);
          SET023.setBounds(250, 80, 20, SET023.getPreferredSize().height);

          //---- SET024 ----
          SET024.setComponentPopupMenu(BTD);
          SET024.setName("SET024");
          xTitledPanel1ContentContainer.add(SET024);
          SET024.setBounds(250, 110, 20, SET024.getPreferredSize().height);

          //---- SET025 ----
          SET025.setComponentPopupMenu(BTD);
          SET025.setName("SET025");
          xTitledPanel1ContentContainer.add(SET025);
          SET025.setBounds(250, 140, 20, SET025.getPreferredSize().height);

          //---- SET026 ----
          SET026.setComponentPopupMenu(BTD);
          SET026.setName("SET026");
          xTitledPanel1ContentContainer.add(SET026);
          SET026.setBounds(250, 170, 20, SET026.getPreferredSize().height);

          //---- SET031 ----
          SET031.setComponentPopupMenu(BTD);
          SET031.setName("SET031");
          xTitledPanel1ContentContainer.add(SET031);
          SET031.setBounds(580, 20, 20, SET031.getPreferredSize().height);

          //---- SET032 ----
          SET032.setComponentPopupMenu(BTD);
          SET032.setName("SET032");
          xTitledPanel1ContentContainer.add(SET032);
          SET032.setBounds(580, 50, 20, SET032.getPreferredSize().height);

          //---- SET033 ----
          SET033.setComponentPopupMenu(BTD);
          SET033.setName("SET033");
          xTitledPanel1ContentContainer.add(SET033);
          SET033.setBounds(580, 80, 20, SET033.getPreferredSize().height);

          //---- SET034 ----
          SET034.setComponentPopupMenu(BTD);
          SET034.setName("SET034");
          xTitledPanel1ContentContainer.add(SET034);
          SET034.setBounds(580, 110, 20, SET034.getPreferredSize().height);

          //---- SET035 ----
          SET035.setComponentPopupMenu(BTD);
          SET035.setName("SET035");
          xTitledPanel1ContentContainer.add(SET035);
          SET035.setBounds(580, 140, 20, SET035.getPreferredSize().height);

          //---- OBJ_77_OBJ_77 ----
          OBJ_77_OBJ_77.setText("21");
          OBJ_77_OBJ_77.setFont(OBJ_77_OBJ_77.getFont().deriveFont(OBJ_77_OBJ_77.getFont().getStyle() | Font.BOLD));
          OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
          xTitledPanel1ContentContainer.add(OBJ_77_OBJ_77);
          OBJ_77_OBJ_77.setBounds(20, 24, 20, 20);

          //---- OBJ_80_OBJ_80 ----
          OBJ_80_OBJ_80.setText("31");
          OBJ_80_OBJ_80.setFont(OBJ_80_OBJ_80.getFont().deriveFont(OBJ_80_OBJ_80.getFont().getStyle() | Font.BOLD));
          OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
          xTitledPanel1ContentContainer.add(OBJ_80_OBJ_80);
          OBJ_80_OBJ_80.setBounds(345, 24, 20, 20);

          //---- OBJ_83_OBJ_83 ----
          OBJ_83_OBJ_83.setText("22");
          OBJ_83_OBJ_83.setFont(OBJ_83_OBJ_83.getFont().deriveFont(OBJ_83_OBJ_83.getFont().getStyle() | Font.BOLD));
          OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
          xTitledPanel1ContentContainer.add(OBJ_83_OBJ_83);
          OBJ_83_OBJ_83.setBounds(20, 54, 20, 20);

          //---- OBJ_86_OBJ_86 ----
          OBJ_86_OBJ_86.setText("32");
          OBJ_86_OBJ_86.setFont(OBJ_86_OBJ_86.getFont().deriveFont(OBJ_86_OBJ_86.getFont().getStyle() | Font.BOLD));
          OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
          xTitledPanel1ContentContainer.add(OBJ_86_OBJ_86);
          OBJ_86_OBJ_86.setBounds(345, 54, 20, 20);

          //---- OBJ_89_OBJ_89 ----
          OBJ_89_OBJ_89.setText("23");
          OBJ_89_OBJ_89.setFont(OBJ_89_OBJ_89.getFont().deriveFont(OBJ_89_OBJ_89.getFont().getStyle() | Font.BOLD));
          OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
          xTitledPanel1ContentContainer.add(OBJ_89_OBJ_89);
          OBJ_89_OBJ_89.setBounds(20, 84, 20, 20);

          //---- OBJ_92_OBJ_92 ----
          OBJ_92_OBJ_92.setText("33");
          OBJ_92_OBJ_92.setFont(OBJ_92_OBJ_92.getFont().deriveFont(OBJ_92_OBJ_92.getFont().getStyle() | Font.BOLD));
          OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");
          xTitledPanel1ContentContainer.add(OBJ_92_OBJ_92);
          OBJ_92_OBJ_92.setBounds(345, 84, 20, 20);

          //---- OBJ_95_OBJ_95 ----
          OBJ_95_OBJ_95.setText("24");
          OBJ_95_OBJ_95.setFont(OBJ_95_OBJ_95.getFont().deriveFont(OBJ_95_OBJ_95.getFont().getStyle() | Font.BOLD));
          OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");
          xTitledPanel1ContentContainer.add(OBJ_95_OBJ_95);
          OBJ_95_OBJ_95.setBounds(20, 114, 20, 20);

          //---- OBJ_98_OBJ_98 ----
          OBJ_98_OBJ_98.setText("34");
          OBJ_98_OBJ_98.setFont(OBJ_98_OBJ_98.getFont().deriveFont(OBJ_98_OBJ_98.getFont().getStyle() | Font.BOLD));
          OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
          xTitledPanel1ContentContainer.add(OBJ_98_OBJ_98);
          OBJ_98_OBJ_98.setBounds(345, 114, 20, 20);

          //---- OBJ_101_OBJ_101 ----
          OBJ_101_OBJ_101.setText("25");
          OBJ_101_OBJ_101.setFont(OBJ_101_OBJ_101.getFont().deriveFont(OBJ_101_OBJ_101.getFont().getStyle() | Font.BOLD));
          OBJ_101_OBJ_101.setName("OBJ_101_OBJ_101");
          xTitledPanel1ContentContainer.add(OBJ_101_OBJ_101);
          OBJ_101_OBJ_101.setBounds(20, 144, 20, 20);

          //---- OBJ_104_OBJ_104 ----
          OBJ_104_OBJ_104.setText("35");
          OBJ_104_OBJ_104.setFont(OBJ_104_OBJ_104.getFont().deriveFont(OBJ_104_OBJ_104.getFont().getStyle() | Font.BOLD));
          OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");
          xTitledPanel1ContentContainer.add(OBJ_104_OBJ_104);
          OBJ_104_OBJ_104.setBounds(345, 144, 20, 20);

          //---- OBJ_107_OBJ_107 ----
          OBJ_107_OBJ_107.setText("26");
          OBJ_107_OBJ_107.setFont(OBJ_107_OBJ_107.getFont().deriveFont(OBJ_107_OBJ_107.getFont().getStyle() | Font.BOLD));
          OBJ_107_OBJ_107.setName("OBJ_107_OBJ_107");
          xTitledPanel1ContentContainer.add(OBJ_107_OBJ_107);
          OBJ_107_OBJ_107.setBounds(20, 174, 20, 20);

          //---- OBJ_76_OBJ_76 ----
          OBJ_76_OBJ_76.setText("a");
          OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");
          xTitledPanel1ContentContainer.add(OBJ_76_OBJ_76);
          OBJ_76_OBJ_76.setBounds(280, 24, 20, 20);

          //---- OBJ_79_OBJ_79 ----
          OBJ_79_OBJ_79.setText("a");
          OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");
          xTitledPanel1ContentContainer.add(OBJ_79_OBJ_79);
          OBJ_79_OBJ_79.setBounds(610, 24, 20, 20);

          //---- OBJ_82_OBJ_82 ----
          OBJ_82_OBJ_82.setText("b");
          OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");
          xTitledPanel1ContentContainer.add(OBJ_82_OBJ_82);
          OBJ_82_OBJ_82.setBounds(280, 54, 20, 20);

          //---- OBJ_85_OBJ_85 ----
          OBJ_85_OBJ_85.setText("b");
          OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
          xTitledPanel1ContentContainer.add(OBJ_85_OBJ_85);
          OBJ_85_OBJ_85.setBounds(610, 54, 20, 20);

          //---- OBJ_88_OBJ_88 ----
          OBJ_88_OBJ_88.setText("b");
          OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
          xTitledPanel1ContentContainer.add(OBJ_88_OBJ_88);
          OBJ_88_OBJ_88.setBounds(280, 84, 20, 20);

          //---- OBJ_91_OBJ_91 ----
          OBJ_91_OBJ_91.setText("b");
          OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");
          xTitledPanel1ContentContainer.add(OBJ_91_OBJ_91);
          OBJ_91_OBJ_91.setBounds(610, 84, 20, 20);

          //---- OBJ_94_OBJ_94 ----
          OBJ_94_OBJ_94.setText("b");
          OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");
          xTitledPanel1ContentContainer.add(OBJ_94_OBJ_94);
          OBJ_94_OBJ_94.setBounds(280, 114, 20, 20);

          //---- OBJ_97_OBJ_97 ----
          OBJ_97_OBJ_97.setText("b");
          OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
          xTitledPanel1ContentContainer.add(OBJ_97_OBJ_97);
          OBJ_97_OBJ_97.setBounds(610, 114, 20, 20);

          //---- OBJ_100_OBJ_100 ----
          OBJ_100_OBJ_100.setText("b");
          OBJ_100_OBJ_100.setName("OBJ_100_OBJ_100");
          xTitledPanel1ContentContainer.add(OBJ_100_OBJ_100);
          OBJ_100_OBJ_100.setBounds(280, 144, 20, 20);

          //---- OBJ_103_OBJ_103 ----
          OBJ_103_OBJ_103.setText("b");
          OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");
          xTitledPanel1ContentContainer.add(OBJ_103_OBJ_103);
          OBJ_103_OBJ_103.setBounds(610, 144, 20, 20);

          //---- OBJ_106_OBJ_106 ----
          OBJ_106_OBJ_106.setText("b");
          OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");
          xTitledPanel1ContentContainer.add(OBJ_106_OBJ_106);
          OBJ_106_OBJ_106.setBounds(280, 174, 20, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(40, 30, 695, 320);

        //---- OBJ_49 ----
        OBJ_49.setText("Aller \u00e0 la page");
        OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_49.setName("OBJ_49");
        OBJ_49.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_49ActionPerformed(e);
          }
        });
        pnlContenu.add(OBJ_49);
        OBJ_49.setBounds(new Rectangle(new Point(555, 365), OBJ_49.getPreferredSize()));

        //---- V06F ----
        V06F.setComponentPopupMenu(BTD);
        V06F.setName("V06F");
        pnlContenu.add(V06F);
        V06F.setBounds(700, 365, 30, V06F.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- OBJ_29_OBJ_29 ----
    OBJ_29_OBJ_29.setText("Num\u00e9ro de compte");
    OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");

    //---- OBJ_7 ----
    OBJ_7.setText("Tri par \u00e9tablissement ou code utilisateur");
    OBJ_7.setName("OBJ_7");
    OBJ_7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_7ActionPerformed(e);
      }
    });

    //---- OBJ_53_OBJ_53 ----
    OBJ_53_OBJ_53.setText("Page");
    OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

    //---- OBJ_54_OBJ_54 ----
    OBJ_54_OBJ_54.setText("@WPAGE@");
    OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_26_OBJ_26;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_56_OBJ_56;
  private RiZoneSortie INDETB;
  private RiZoneSortie OBJ_50_OBJ_50;
  private JPanel p_tete_droite;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelContenu pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_96_OBJ_96;
  private JLabel OBJ_81_OBJ_81;
  private JLabel OBJ_102_OBJ_102;
  private JLabel OBJ_99_OBJ_99;
  private JLabel OBJ_90_OBJ_90;
  private JLabel OBJ_87_OBJ_87;
  private JLabel OBJ_93_OBJ_93;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_105_OBJ_105;
  private JLabel OBJ_84_OBJ_84;
  private XRiTextField SET021;
  private XRiTextField SET022;
  private XRiTextField SET023;
  private XRiTextField SET024;
  private XRiTextField SET025;
  private XRiTextField SET026;
  private XRiTextField SET031;
  private XRiTextField SET032;
  private XRiTextField SET033;
  private XRiTextField SET034;
  private XRiTextField SET035;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_86_OBJ_86;
  private JLabel OBJ_89_OBJ_89;
  private JLabel OBJ_92_OBJ_92;
  private JLabel OBJ_95_OBJ_95;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_101_OBJ_101;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_107_OBJ_107;
  private JLabel OBJ_76_OBJ_76;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_82_OBJ_82;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_88_OBJ_88;
  private JLabel OBJ_91_OBJ_91;
  private JLabel OBJ_94_OBJ_94;
  private JLabel OBJ_97_OBJ_97;
  private JLabel OBJ_100_OBJ_100;
  private JLabel OBJ_103_OBJ_103;
  private JLabel OBJ_106_OBJ_106;
  private SNBoutonLeger OBJ_49;
  private XRiTextField V06F;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JLabel OBJ_29_OBJ_29;
  private JMenuItem OBJ_7;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_54_OBJ_54;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
