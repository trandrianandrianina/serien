
package ri.serien.libecranrpg.vgem.VGEM16FM;
// Nom Fichier: pop_VGEM16FM_FMTC2_FMTF1_64.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM16FM_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM16FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_39);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_23_OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSF01@")).trim());
    OBJ_22_OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    DORIB.setEnabled(lexique.isPresent("DORIB"));
    OBJ_22_OBJ_22.setVisible(lexique.isPresent("UCLEX"));
    CHVDE.setEnabled(lexique.isPresent("CHVDE"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    DOGUI.setEnabled(lexique.isPresent("DOGUI"));
    DOBQE.setEnabled(lexique.isPresent("DOBQE"));
    INDNUM.setEnabled(lexique.isPresent("INDNUM"));
    DOCPT.setEnabled(lexique.isPresent("DOCPT"));
    WMTT.setEnabled(lexique.isPresent("WMTT"));
    DODO2.setEnabled(lexique.isPresent("DODO2"));
    DODO1.setEnabled(lexique.isPresent("DODO1"));
    DORIE.setEnabled(lexique.isPresent("DORIE"));
    OBJ_23_OBJ_23.setVisible(lexique.isPresent("MSF01"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Autres modes de règlement"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgem16"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_23_OBJ_23 = new JLabel();
    DORIE = new XRiTextField();
    DODO1 = new XRiTextField();
    DODO2 = new XRiTextField();
    OBJ_39 = new JButton();
    WMTT = new XRiTextField();
    DOCPT = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_30_OBJ_30 = new JLabel();
    OBJ_19_OBJ_19 = new JLabel();
    INDNUM = new XRiTextField();
    OBJ_21_OBJ_21 = new JLabel();
    DOBQE = new XRiTextField();
    DOGUI = new XRiTextField();
    OBJ_20_OBJ_20 = new JLabel();
    INDETB = new XRiTextField();
    CHVDE = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_26_OBJ_26 = new JLabel();
    OBJ_22_OBJ_22 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    DORIB = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_23_OBJ_23 ----
    OBJ_23_OBJ_23.setText("@MSF01@");
    OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");
    add(OBJ_23_OBJ_23);
    OBJ_23_OBJ_23.setBounds(16, 150, 474, 20);

    //---- DORIE ----
    DORIE.setComponentPopupMenu(BTD);
    DORIE.setName("DORIE");
    add(DORIE);
    DORIE.setBounds(243, 118, 252, DORIE.getPreferredSize().height);

    //---- DODO1 ----
    DODO1.setComponentPopupMenu(BTD);
    DODO1.setName("DODO1");
    add(DODO1);
    DODO1.setBounds(70, 90, 172, DODO1.getPreferredSize().height);

    //---- DODO2 ----
    DODO2.setComponentPopupMenu(BTD);
    DODO2.setName("DODO2");
    add(DODO2);
    DODO2.setBounds(70, 118, 172, DODO2.getPreferredSize().height);

    //---- OBJ_39 ----
    OBJ_39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_39.setName("OBJ_39");
    OBJ_39.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_39ActionPerformed(e);
      }
    });
    add(OBJ_39);
    OBJ_39.setBounds(460, 170, 56, 40);

    //---- WMTT ----
    WMTT.setName("WMTT");
    add(WMTT);
    WMTT.setBounds(185, 15, 108, WMTT.getPreferredSize().height);

    //---- DOCPT ----
    DOCPT.setComponentPopupMenu(BTD);
    DOCPT.setName("DOCPT");
    add(DOCPT);
    DOCPT.setBounds(348, 90, 100, DOCPT.getPreferredSize().height);

    //---- OBJ_41_OBJ_41 ----
    OBJ_41_OBJ_41.setText("N\u00b0de Compte");
    OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
    add(OBJ_41_OBJ_41);
    OBJ_41_OBJ_41.setBounds(350, 70, 82, 18);

    //---- OBJ_30_OBJ_30 ----
    OBJ_30_OBJ_30.setText("Vendeur");
    OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
    add(OBJ_30_OBJ_30);
    OBJ_30_OBJ_30.setBounds(295, 19, 54, 20);

    //---- OBJ_19_OBJ_19 ----
    OBJ_19_OBJ_19.setText("Banque");
    OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
    add(OBJ_19_OBJ_19);
    OBJ_19_OBJ_19.setBounds(15, 94, 51, 20);

    //---- INDNUM ----
    INDNUM.setName("INDNUM");
    add(INDNUM);
    INDNUM.setBounds(115, 15, 68, INDNUM.getPreferredSize().height);

    //---- OBJ_21_OBJ_21 ----
    OBJ_21_OBJ_21.setText("Guichet");
    OBJ_21_OBJ_21.setName("OBJ_21_OBJ_21");
    add(OBJ_21_OBJ_21);
    OBJ_21_OBJ_21.setBounds(16, 122, 48, 20);

    //---- DOBQE ----
    DOBQE.setComponentPopupMenu(BTD);
    DOBQE.setName("DOBQE");
    add(DOBQE);
    DOBQE.setBounds(243, 90, 52, DOBQE.getPreferredSize().height);

    //---- DOGUI ----
    DOGUI.setComponentPopupMenu(BTD);
    DOGUI.setName("DOGUI");
    add(DOGUI);
    DOGUI.setBounds(295, 90, 52, DOGUI.getPreferredSize().height);

    //---- OBJ_20_OBJ_20 ----
    OBJ_20_OBJ_20.setText("N\u00b0Bqe");
    OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");
    add(OBJ_20_OBJ_20);
    OBJ_20_OBJ_20.setBounds(245, 70, 42, 18);

    //---- INDETB ----
    INDETB.setName("INDETB");
    add(INDETB);
    INDETB.setBounds(70, 15, 40, INDETB.getPreferredSize().height);

    //---- CHVDE ----
    CHVDE.setComponentPopupMenu(BTD);
    CHVDE.setName("CHVDE");
    add(CHVDE);
    CHVDE.setBounds(350, 15, 40, CHVDE.getPreferredSize().height);

    //---- OBJ_40_OBJ_40 ----
    OBJ_40_OBJ_40.setText("N\u00b0Gui");
    OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
    add(OBJ_40_OBJ_40);
    OBJ_40_OBJ_40.setBounds(295, 70, 39, 18);

    //---- OBJ_26_OBJ_26 ----
    OBJ_26_OBJ_26.setText("Cde");
    OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
    add(OBJ_26_OBJ_26);
    OBJ_26_OBJ_26.setBounds(15, 19, 28, 20);

    //---- OBJ_22_OBJ_22 ----
    OBJ_22_OBJ_22.setText("@UCLEX@");
    OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
    add(OBJ_22_OBJ_22);
    OBJ_22_OBJ_22.setBounds(485, 94, 24, 20);

    //---- OBJ_42_OBJ_42 ----
    OBJ_42_OBJ_42.setText("Cl\u00e9");
    OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
    add(OBJ_42_OBJ_42);
    OBJ_42_OBJ_42.setBounds(450, 70, 24, 18);

    //---- DORIB ----
    DORIB.setComponentPopupMenu(BTD);
    DORIB.setName("DORIB");
    add(DORIB);
    DORIB.setBounds(450, 90, 30, DORIB.getPreferredSize().height);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Domiciliation");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 55, 510, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(534, 214));

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_23_OBJ_23;
  private XRiTextField DORIE;
  private XRiTextField DODO1;
  private XRiTextField DODO2;
  private JButton OBJ_39;
  private XRiTextField WMTT;
  private XRiTextField DOCPT;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_30_OBJ_30;
  private JLabel OBJ_19_OBJ_19;
  private XRiTextField INDNUM;
  private JLabel OBJ_21_OBJ_21;
  private XRiTextField DOBQE;
  private XRiTextField DOGUI;
  private JLabel OBJ_20_OBJ_20;
  private XRiTextField INDETB;
  private XRiTextField CHVDE;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_26_OBJ_26;
  private JLabel OBJ_22_OBJ_22;
  private JLabel OBJ_42_OBJ_42;
  private XRiTextField DORIB;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
