
package ri.serien.libecranrpg.vgem.VGEM09FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM09FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGEM09FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX8.setValeurs("8", "TIDX8");
    TIDX6.setValeurs("6", "TIDX6");
    TIDX5.setValeurs("5", "TIDX5");
    TIDX4.setValeurs("4", "TIDX4");
    TIDX3.setValeurs("3", "TIDX3");
    TIDX2.setValeurs("2", "TIDX2");
    TIDX1.setValeurs("1", "TIDX1");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // TIDX8.setSelected(lexique.HostFieldGetData("TIDX8").equalsIgnoreCase("7"));
    // TIDX6.setSelected(lexique.HostFieldGetData("TIDX6").equalsIgnoreCase("6"));
    // TIDX5.setSelected(lexique.HostFieldGetData("TIDX5").equalsIgnoreCase("5"));
    // TIDX4.setSelected(lexique.HostFieldGetData("TIDX4").equalsIgnoreCase("4"));
    // TIDX3.setSelected(lexique.HostFieldGetData("TIDX3").equalsIgnoreCase("3"));
    // TIDX2.setSelected(lexique.HostFieldGetData("TIDX2").equalsIgnoreCase("2"));
    // TIDX1.setSelected(lexique.HostFieldGetData("TIDX1").equalsIgnoreCase("1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("CONSTITUTION REMISE"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("TIDX8", 0, "7");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("TIDX6", 0, "6");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("TIDX5", 0, "5");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("TIDX4", 0, "4");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("TIDX3", 0, "3");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("TIDX2", 0, "2");
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("TIDX1", 0, "1");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    TIDX1 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    ARG4 = new XRiTextField();
    ARG8 = new XRiTextField();
    ARG6 = new XRiTextField();
    ARG5 = new XRiTextField();
    ARG22 = new XRiTextField();
    DECHX = new XRiTextField();
    DEMIX = new XRiTextField();
    ARG11 = new XRiTextField();
    ARG31 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG241 = new XRiTextField();
    TDECH = new XRiSpinner();
    TDEMI = new XRiSpinner();
    ARG23 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(625, 350));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- TIDX1 ----
          TIDX1.setText("Num\u00e9ro de pi\u00e8ce");
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setName("TIDX1");
          p_recup.add(TIDX1);
          TIDX1.setBounds(30, 55, 180, 20);

          //---- TIDX2 ----
          TIDX2.setText("Identification remise");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");
          p_recup.add(TIDX2);
          TIDX2.setBounds(30, 83, 180, 20);

          //---- TIDX3 ----
          TIDX3.setText("Num\u00e9ro de client");
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setName("TIDX3");
          p_recup.add(TIDX3);
          TIDX3.setBounds(30, 111, 180, 20);

          //---- TIDX4 ----
          TIDX4.setText("Nom du client");
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setName("TIDX4");
          p_recup.add(TIDX4);
          TIDX4.setBounds(30, 139, 180, 20);

          //---- TIDX5 ----
          TIDX5.setText("Montant");
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setName("TIDX5");
          p_recup.add(TIDX5);
          TIDX5.setBounds(30, 167, 180, 20);

          //---- TIDX6 ----
          TIDX6.setText("Num\u00e9ro de compte");
          TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX6.setName("TIDX6");
          p_recup.add(TIDX6);
          TIDX6.setBounds(30, 195, 180, 20);

          //---- TIDX8 ----
          TIDX8.setText("Mot de classement 2");
          TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8.setName("TIDX8");
          p_recup.add(TIDX8);
          TIDX8.setBounds(30, 223, 180, 20);

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Date \u00e9ch\u00e9ance");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
          p_recup.add(OBJ_49_OBJ_49);
          OBJ_49_OBJ_49.setBounds(35, 251, 180, 20);

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("Date \u00e9mission");
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
          p_recup.add(OBJ_50_OBJ_50);
          OBJ_50_OBJ_50.setBounds(35, 279, 180, 20);

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(BTD);
          ARG4.setName("ARG4");
          p_recup.add(ARG4);
          ARG4.setBounds(210, 135, 160, ARG4.getPreferredSize().height);

          //---- ARG8 ----
          ARG8.setComponentPopupMenu(BTD);
          ARG8.setName("ARG8");
          p_recup.add(ARG8);
          ARG8.setBounds(210, 219, 160, ARG8.getPreferredSize().height);

          //---- ARG6 ----
          ARG6.setComponentPopupMenu(BTD);
          ARG6.setName("ARG6");
          p_recup.add(ARG6);
          ARG6.setBounds(210, 191, 100, ARG6.getPreferredSize().height);

          //---- ARG5 ----
          ARG5.setComponentPopupMenu(BTD);
          ARG5.setName("ARG5");
          p_recup.add(ARG5);
          ARG5.setBounds(210, 163, 108, ARG5.getPreferredSize().height);

          //---- ARG22 ----
          ARG22.setComponentPopupMenu(BTD);
          ARG22.setName("ARG22");
          p_recup.add(ARG22);
          ARG22.setBounds(241, 79, 76, ARG22.getPreferredSize().height);

          //---- DECHX ----
          DECHX.setComponentPopupMenu(BTD);
          DECHX.setName("DECHX");
          p_recup.add(DECHX);
          DECHX.setBounds(250, 247, 76, DECHX.getPreferredSize().height);

          //---- DEMIX ----
          DEMIX.setComponentPopupMenu(BTD);
          DEMIX.setName("DEMIX");
          p_recup.add(DEMIX);
          DEMIX.setBounds(250, 275, 76, DEMIX.getPreferredSize().height);

          //---- ARG11 ----
          ARG11.setComponentPopupMenu(BTD);
          ARG11.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG11.setMinimumSize(new Dimension(85, 28));
          ARG11.setMaximumSize(new Dimension(85, 28));
          ARG11.setPreferredSize(new Dimension(85, 28));
          ARG11.setName("ARG11");
          p_recup.add(ARG11);
          ARG11.setBounds(210, 51, 85, ARG11.getPreferredSize().height);

          //---- ARG31 ----
          ARG31.setComponentPopupMenu(BTD);
          ARG31.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG31.setName("ARG31");
          p_recup.add(ARG31);
          ARG31.setBounds(210, 107, 60, ARG31.getPreferredSize().height);

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BTD);
          ARG2.setName("ARG2");
          p_recup.add(ARG2);
          ARG2.setBounds(210, 79, 30, ARG2.getPreferredSize().height);

          //---- ARG241 ----
          ARG241.setComponentPopupMenu(BTD);
          ARG241.setName("ARG241");
          p_recup.add(ARG241);
          ARG241.setBounds(339, 79, 30, ARG241.getPreferredSize().height);

          //---- TDECH ----
          TDECH.setComponentPopupMenu(BTD);
          TDECH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TDECH.setModel(new SpinnerListModel(new String[] {" ", "<", "=", ">"}));
          TDECH.setName("TDECH");
          p_recup.add(TDECH);
          TDECH.setBounds(210, 247, 40, 28);

          //---- TDEMI ----
          TDEMI.setComponentPopupMenu(BTD);
          TDEMI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TDEMI.setModel(new SpinnerListModel(new String[] {" ", "<", "=", ">"}));
          TDEMI.setName("TDEMI");
          p_recup.add(TDEMI);
          TDEMI.setBounds(210, 275, 40, 28);

          //---- ARG23 ----
          ARG23.setComponentPopupMenu(BTD);
          ARG23.setName("ARG23");
          p_recup.add(ARG23);
          ARG23.setBounds(318, 79, 20, ARG23.getPreferredSize().height);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 435, 330);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX8);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX8;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_50_OBJ_50;
  private XRiTextField ARG4;
  private XRiTextField ARG8;
  private XRiTextField ARG6;
  private XRiTextField ARG5;
  private XRiTextField ARG22;
  private XRiTextField DECHX;
  private XRiTextField DEMIX;
  private XRiTextField ARG11;
  private XRiTextField ARG31;
  private XRiTextField ARG2;
  private XRiTextField ARG241;
  private XRiSpinner TDECH;
  private XRiSpinner TDEMI;
  private XRiTextField ARG23;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
