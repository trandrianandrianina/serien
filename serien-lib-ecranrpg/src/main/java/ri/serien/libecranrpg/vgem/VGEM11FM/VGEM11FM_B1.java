
package ri.serien.libecranrpg.vgem.VGEM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGEM11FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] XCOD_Value = { "", "E", "D", };
  
  // private MaskFormatter masque=null;
  
  public VGEM11FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    XCOD.setValeurs(XCOD_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    lbReglement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ECHA@")).trim());
    lbRemise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@REMI@")).trim());
    OBJ_127_OBJ_127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    String codeBancaire = lexique.HostFieldGetData("CHCAN");
    if (codeBancaire != null && codeBancaire.length() == 16) {
      CB01.setText(codeBancaire.substring(0, 3));
      CB02.setText(codeBancaire.substring(4, 7));
      CB03.setText(codeBancaire.substring(8, 11));
      CB04.setText(codeBancaire.substring(12, 15));
      CB01.setEnabled(!isConsultation);
      CB02.setEnabled(!isConsultation);
      CB03.setEnabled(!isConsultation);
      CB04.setEnabled(!isConsultation);
    }
    
    if (lexique.isTrue("(N94) AND (N95)")) {
      p_bpresentation.setText("Gestion des espèces");
    }
    else if (lexique.isTrue("94")) {
      p_bpresentation.setText("Gestion des cartes");
    }
    else if (lexique.isTrue("95")) {
      p_bpresentation.setText("Gestion des T.I.P.");
    }
    
    lbReglement.setVisible(lexique.isPresent("CHDRGX"));
    lbVendeur.setVisible(lexique.isPresent("CHVDE"));
    lbMagasin.setVisible(lexique.isPresent("CHMAG"));
    lbRemise.setVisible(lexique.isPresent("CHDREX"));
    lbSur.setVisible(lexique.isPresent("CHBRE"));
    lbSuffixe.setVisible(lexique.isPresent("CHSRE"));
    lbCompa1.setVisible(lexique.isPresent("CHNC1X"));
    lbCompa2.setVisible(lexique.isPresent("CHNC2X"));
    lbType.setVisible(lexique.isPresent("CHTCA"));
    lbValidation.setVisible(lexique.isPresent("CHVAL"));
    lbNumeroAutorisation.setVisible(lexique.isPresent("CHAUT"));
    lbNumeroCarte.setVisible(CB01.isVisible());
    lbTypePaiement.setVisible(lexique.isPresent("CHPAI"));
    lbModeSaisie.setVisible(lexique.isPresent("CHSAI"));
    lbNumeroDocumentVente.setVisible(lexique.isPresent("XNUM"));
    XCOD.setVisible(lexique.isPresent("XNUM"));
    
    panel4.setVisible(lexique.isTrue("94"));
    
    lbEtablissementGVM.setVisible(lexique.isTrue("(51) AND (80)"));
    CHETB2.setVisible(lexique.isTrue("(51) AND (80)"));
    
    lbDomiciliation.setVisible(lexique.isTrue("95"));
    pnlDomiciliation.setVisible(lexique.isTrue("95"));
    
    btDomiciliation.setEnabled(lexique.isTrue("N53"));
    
    if (lexique.isTrue("(N92) AND (N94) AND (N95)")) {
      p_bpresentation.setText("Gestion des espèces");
      lbEspeces.setMessage(Message.getMessageNormal("Espèces"));
      lbReglement.setText("Réglement");
      
    }
    else if (lexique.isTrue("92")) {
      p_bpresentation.setText("Gestion des virements");
      lbEspeces.setMessage(Message.getMessageNormal("Virements"));
      lbReglement.setText("Réglement");
    }
    else if (lexique.isTrue("94")) {
      p_bpresentation.setText("Gestion des cartes");
      lbEspeces.setMessage(Message.getMessageNormal("Carte"));
      lbReglement.setText("Réglement");
      lbRemise.setText("Remise en banque le:");
    }
    else if (lexique.isTrue("95")) {
      p_bpresentation.setText("Gestion des T.I.P.");
      lbEspeces.setMessage(Message.getMessageNormal("T.I.P"));
      lbReglement.setText("Echéance");
      lbRemise.setText("Retour de banque le:");
    }
    
    // panel4.setVisible(lexique.isPresent("CHTCA"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    lexique.HostFieldPutData("CHCAN", 0, CB01.getText() + CB02.getText() + CB03.getText() + CB04.getText());
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("CHIMP", 0, "i");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_133ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 17);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40_OBJ_40 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    INDIDU = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    CHCL2 = new XRiTextField();
    CHIMP = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_51_OBJ_51 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    lbEspeces = new SNLabelTitre();
    pnlEspeces = new SNPanelTitre();
    panel2 = new JPanel();
    CHMTT = new XRiTextField();
    CHMTR = new XRiTextField();
    CHDRGX = new XRiCalendrier();
    CHVDE = new XRiTextField();
    lbMontant = new SNLabelChamp();
    lbReglement = new SNLabelChamp();
    lbVendeur = new SNLabelChamp();
    lbMagasin = new SNLabelChamp();
    CHMAG = new XRiTextField();
    lbCompa1 = new SNLabelChamp();
    CHNC1X = new XRiTextField();
    lbRemise = new SNLabelChamp();
    CHDREX = new XRiCalendrier();
    lbSur = new SNLabelChamp();
    CHBRE = new XRiTextField();
    lbSuffixe = new SNLabelChamp();
    CHSRE = new XRiTextField();
    lbCompa2 = new SNLabelChamp();
    CHNC2X = new XRiTextField();
    panel4 = new JPanel();
    CHTCA = new XRiTextField();
    lbNumeroCarte = new SNLabelChamp();
    lbType = new SNLabelChamp();
    lbValidation = new SNLabelChamp();
    panel5 = new JPanel();
    CHVAL = new XRiTextField();
    CHMOV = new XRiTextField();
    CHANVR = new XRiTextField();
    lbNumeroAutorisation = new SNLabelChamp();
    CHAUT = new XRiTextField();
    CB01 = new XRiTextField();
    CB02 = new XRiTextField();
    CB03 = new XRiTextField();
    CB04 = new XRiTextField();
    lbTypePaiement = new SNLabelChamp();
    CHPAI = new XRiTextField();
    lbModeSaisie = new SNLabelChamp();
    CHSAI = new XRiTextField();
    sNPanel1 = new SNPanel();
    lbNumeroDocumentVente = new SNLabelChamp();
    XNUM = new XRiTextField();
    XSUF = new XRiTextField();
    XCOD = new XRiComboBox();
    lbClient = new SNLabelTitre();
    pnlClient = new SNPanelTitre();
    CHCLA = new XRiTextField();
    CHNOM = new XRiTextField();
    CHAD1 = new XRiTextField();
    CHAD2 = new XRiTextField();
    CHETB2 = new XRiTextField();
    lbEtablissementGVM = new SNLabelChamp();
    CHAD3 = new XRiTextField();
    lbRaisonSociale = new SNLabelUnite();
    lbNumeroCompte = new SNLabelUnite();
    lbClassement = new SNLabelUnite();
    CHNCG = new XRiTextField();
    CHNCA = new XRiTextField();
    lbDomiciliation = new SNLabelTitre();
    pnlDomiciliation = new SNPanelTitre();
    CHRIE = new XRiTextField();
    CHDOM = new XRiTextField();
    lbNomBanque = new SNLabelUnite();
    CHNCB = new XRiTextField();
    lbNumeroCompteDom = new SNLabelUnite();
    lbNumeroGuichetDom = new SNLabelUnite();
    CHCBQ = new XRiTextField();
    CHCGU = new XRiTextField();
    lbNumeroEtablissementDom = new SNLabelUnite();
    CHRIB = new XRiTextField();
    lbCleDom = new SNLabelUnite();
    OBJ_127_OBJ_127 = new RiZoneSortie();
    btDomiciliation = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des esp\u00e8ces");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Etablissement");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Num\u00e9ro op\u00e9ration");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

          //---- INDIDU ----
          INDIDU.setComponentPopupMenu(BTD);
          INDIDU.setName("INDIDU");

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("Mot de classement 2");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");

          //---- CHCL2 ----
          CHCL2.setComponentPopupMenu(BTD);
          CHCL2.setName("CHCL2");

          //---- CHIMP ----
          CHIMP.setComponentPopupMenu(BTD);
          CHIMP.setName("CHIMP");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CHCL2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CHIMP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(137, 137, 137))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addComponent(CHCL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(CHIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
              .addComponent(INDIDU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("Affectation");
          OBJ_51_OBJ_51.setFont(OBJ_51_OBJ_51.getFont().deriveFont(OBJ_51_OBJ_51.getFont().getSize() + 3f));
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          p_tete_droite.add(OBJ_51_OBJ_51);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("G\u00e9n\u00e9ration d'un impay\u00e9");
              riSousMenu_bt6.setToolTipText("G\u00e9n\u00e9ration d'un impay\u00e9");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

        //---- lbEspeces ----
        lbEspeces.setText("Esp\u00e8ces");
        lbEspeces.setName("lbEspeces");
        pnlContenu.add(lbEspeces, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlEspeces ========
        {
          pnlEspeces.setMinimumSize(new Dimension(700, 200));
          pnlEspeces.setPreferredSize(new Dimension(700, 200));
          pnlEspeces.setName("pnlEspeces");
          pnlEspeces.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEspeces.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlEspeces.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlEspeces.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlEspeces.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(new GridBagLayout());
            ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- CHMTT ----
            CHMTT.setComponentPopupMenu(BTD);
            CHMTT.setPreferredSize(new Dimension(120, 28));
            CHMTT.setMinimumSize(new Dimension(120, 28));
            CHMTT.setName("CHMTT");
            panel2.add(CHMTT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CHMTR ----
            CHMTR.setComponentPopupMenu(BTD);
            CHMTR.setName("CHMTR");
            panel2.add(CHMTR, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CHDRGX ----
            CHDRGX.setToolTipText("Date");
            CHDRGX.setComponentPopupMenu(BTD);
            CHDRGX.setEditable(false);
            CHDRGX.setPreferredSize(new Dimension(105, 28));
            CHDRGX.setMinimumSize(new Dimension(105, 28));
            CHDRGX.setMaximumSize(new Dimension(105, 28));
            CHDRGX.setName("CHDRGX");
            panel2.add(CHDRGX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CHVDE ----
            CHVDE.setComponentPopupMenu(BTD);
            CHVDE.setMinimumSize(new Dimension(40, 28));
            CHVDE.setPreferredSize(new Dimension(40, 28));
            CHVDE.setMaximumSize(new Dimension(40, 28));
            CHVDE.setName("CHVDE");
            panel2.add(CHVDE, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbMontant ----
            lbMontant.setText("Montant");
            lbMontant.setMinimumSize(new Dimension(180, 30));
            lbMontant.setPreferredSize(new Dimension(180, 30));
            lbMontant.setMaximumSize(new Dimension(180, 30));
            lbMontant.setName("lbMontant");
            panel2.add(lbMontant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbReglement ----
            lbReglement.setText("@ECHA@");
            lbReglement.setName("lbReglement");
            panel2.add(lbReglement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbVendeur ----
            lbVendeur.setText("Vendeur");
            lbVendeur.setPreferredSize(new Dimension(85, 30));
            lbVendeur.setName("lbMagasin");
            lbVendeur.setMinimumSize(new Dimension(85, 30));
            lbVendeur.setMaximumSize(new Dimension(85, 30));
            panel2.add(lbVendeur, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbSuffixe");
            lbMagasin.setPreferredSize(new Dimension(85, 30));
            lbMagasin.setMinimumSize(new Dimension(85, 30));
            lbMagasin.setMaximumSize(new Dimension(85, 30));
            panel2.add(lbMagasin, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CHMAG ----
            CHMAG.setComponentPopupMenu(BTD);
            CHMAG.setMinimumSize(new Dimension(35, 28));
            CHMAG.setPreferredSize(new Dimension(35, 28));
            CHMAG.setMaximumSize(new Dimension(35, 28));
            CHMAG.setName("CHMAG");
            panel2.add(CHMAG, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbCompa1 ----
            lbCompa1.setText("Comptabilis\u00e9");
            lbCompa1.setPreferredSize(new Dimension(85, 30));
            lbCompa1.setMinimumSize(new Dimension(85, 30));
            lbCompa1.setMaximumSize(new Dimension(85, 30));
            lbCompa1.setName("lbCompa1");
            panel2.add(lbCompa1, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CHNC1X ----
            CHNC1X.setComponentPopupMenu(BTD);
            CHNC1X.setMinimumSize(new Dimension(40, 28));
            CHNC1X.setMaximumSize(new Dimension(40, 28));
            CHNC1X.setPreferredSize(new Dimension(40, 28));
            CHNC1X.setName("CHNC1X");
            panel2.add(CHNC1X, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbRemise ----
            lbRemise.setText("@REMI@");
            lbRemise.setPreferredSize(new Dimension(180, 30));
            lbRemise.setMinimumSize(new Dimension(180, 30));
            lbRemise.setMaximumSize(new Dimension(180, 30));
            lbRemise.setName("lbRemise");
            panel2.add(lbRemise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHDREX ----
            CHDREX.setToolTipText("Date");
            CHDREX.setComponentPopupMenu(BTD);
            CHDREX.setMinimumSize(new Dimension(105, 28));
            CHDREX.setMaximumSize(new Dimension(105, 28));
            CHDREX.setPreferredSize(new Dimension(105, 28));
            CHDREX.setName("CHDREX");
            panel2.add(CHDREX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbSur ----
            lbSur.setText("sur");
            lbSur.setMinimumSize(new Dimension(150, 19));
            lbSur.setPreferredSize(new Dimension(150, 19));
            lbSur.setName("lbSur");
            panel2.add(lbSur, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHBRE ----
            CHBRE.setComponentPopupMenu(BTD);
            CHBRE.setMinimumSize(new Dimension(30, 28));
            CHBRE.setMaximumSize(new Dimension(30, 28));
            CHBRE.setPreferredSize(new Dimension(30, 28));
            CHBRE.setName("CHBRE");
            panel2.add(CHBRE, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbSuffixe ----
            lbSuffixe.setText("Suffixe");
            lbSuffixe.setPreferredSize(new Dimension(85, 30));
            lbSuffixe.setMinimumSize(new Dimension(85, 30));
            lbSuffixe.setMaximumSize(new Dimension(85, 30));
            lbSuffixe.setName("lbSuffixe");
            panel2.add(lbSuffixe, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHSRE ----
            CHSRE.setComponentPopupMenu(BTD);
            CHSRE.setMinimumSize(new Dimension(30, 28));
            CHSRE.setPreferredSize(new Dimension(30, 28));
            CHSRE.setMaximumSize(new Dimension(30, 28));
            CHSRE.setName("CHSRE");
            panel2.add(CHSRE, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbCompa2 ----
            lbCompa2.setText("Comptabilis\u00e9");
            lbCompa2.setName("lbSuffixe");
            lbCompa2.setPreferredSize(new Dimension(85, 30));
            lbCompa2.setMinimumSize(new Dimension(85, 30));
            lbCompa2.setMaximumSize(new Dimension(85, 30));
            panel2.add(lbCompa2, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHNC2X ----
            CHNC2X.setComponentPopupMenu(BTD);
            CHNC2X.setMinimumSize(new Dimension(40, 28));
            CHNC2X.setPreferredSize(new Dimension(40, 28));
            CHNC2X.setMaximumSize(new Dimension(40, 28));
            CHNC2X.setName("CHNC2X");
            panel2.add(CHNC2X, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlEspeces.add(panel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(new GridBagLayout());
            ((GridBagLayout)panel4.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)panel4.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)panel4.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel4.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- CHTCA ----
            CHTCA.setComponentPopupMenu(BTD);
            CHTCA.setPreferredSize(new Dimension(24, 28));
            CHTCA.setMinimumSize(new Dimension(24, 28));
            CHTCA.setMaximumSize(new Dimension(24, 28));
            CHTCA.setName("CHTCA");
            panel4.add(CHTCA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbNumeroCarte ----
            lbNumeroCarte.setText("Num\u00e9ro carte");
            lbNumeroCarte.setMinimumSize(new Dimension(100, 30));
            lbNumeroCarte.setPreferredSize(new Dimension(100, 30));
            lbNumeroCarte.setName("lbNumeroCarte");
            panel4.add(lbNumeroCarte, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbType ----
            lbType.setText("Type");
            lbType.setMinimumSize(new Dimension(100, 30));
            lbType.setPreferredSize(new Dimension(100, 30));
            lbType.setName("lbType");
            panel4.add(lbType, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbValidation ----
            lbValidation.setText("Validation");
            lbValidation.setName("lbValidation");
            panel4.add(lbValidation, new GridBagConstraints(2, 0, 4, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(new GridBagLayout());
              ((GridBagLayout)panel5.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
              ((GridBagLayout)panel5.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)panel5.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)panel5.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- CHVAL ----
              CHVAL.setComponentPopupMenu(BTD);
              CHVAL.setMinimumSize(new Dimension(30, 28));
              CHVAL.setMaximumSize(new Dimension(30, 28));
              CHVAL.setPreferredSize(new Dimension(30, 28));
              CHVAL.setName("CHVAL");
              panel5.add(CHVAL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- CHMOV ----
              CHMOV.setComponentPopupMenu(BTD);
              CHMOV.setMinimumSize(new Dimension(30, 28));
              CHMOV.setMaximumSize(new Dimension(30, 28));
              CHMOV.setPreferredSize(new Dimension(30, 28));
              CHMOV.setName("CHMOV");
              panel5.add(CHMOV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- CHANVR ----
              CHANVR.setComponentPopupMenu(BTD);
              CHANVR.setMinimumSize(new Dimension(30, 28));
              CHANVR.setMaximumSize(new Dimension(30, 28));
              CHANVR.setPreferredSize(new Dimension(30, 28));
              CHANVR.setName("CHANVR");
              panel5.add(CHANVR, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            panel4.add(panel5, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbNumeroAutorisation ----
            lbNumeroAutorisation.setText("N\u00b0 autorisation");
            lbNumeroAutorisation.setName("lbNumeroAutorisation");
            panel4.add(lbNumeroAutorisation, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CHAUT ----
            CHAUT.setComponentPopupMenu(BTD);
            CHAUT.setPreferredSize(new Dimension(60, 28));
            CHAUT.setMinimumSize(new Dimension(60, 28));
            CHAUT.setMaximumSize(new Dimension(60, 28));
            CHAUT.setName("CHAUT");
            panel4.add(CHAUT, new GridBagConstraints(8, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CB01 ----
            CB01.setComponentPopupMenu(BTD);
            CB01.setMinimumSize(new Dimension(40, 28));
            CB01.setPreferredSize(new Dimension(40, 28));
            CB01.setMaximumSize(new Dimension(40, 28));
            CB01.setName("CB01");
            panel4.add(CB01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CB02 ----
            CB02.setComponentPopupMenu(BTD);
            CB02.setMinimumSize(new Dimension(40, 28));
            CB02.setPreferredSize(new Dimension(40, 28));
            CB02.setMaximumSize(new Dimension(40, 28));
            CB02.setName("CB02");
            panel4.add(CB02, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CB03 ----
            CB03.setComponentPopupMenu(BTD);
            CB03.setMinimumSize(new Dimension(40, 28));
            CB03.setPreferredSize(new Dimension(40, 28));
            CB03.setMaximumSize(new Dimension(40, 28));
            CB03.setName("CB03");
            panel4.add(CB03, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CB04 ----
            CB04.setComponentPopupMenu(BTD);
            CB04.setMinimumSize(new Dimension(40, 28));
            CB04.setPreferredSize(new Dimension(40, 28));
            CB04.setMaximumSize(new Dimension(40, 28));
            CB04.setName("CB04");
            panel4.add(CB04, new GridBagConstraints(5, 1, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbTypePaiement ----
            lbTypePaiement.setText("Type paiement");
            lbTypePaiement.setName("lbTypePaiement");
            panel4.add(lbTypePaiement, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHPAI ----
            CHPAI.setComponentPopupMenu(BTD);
            CHPAI.setMinimumSize(new Dimension(30, 28));
            CHPAI.setMaximumSize(new Dimension(30, 28));
            CHPAI.setPreferredSize(new Dimension(30, 28));
            CHPAI.setName("CHPAI");
            panel4.add(CHPAI, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbModeSaisie ----
            lbModeSaisie.setText("Mode de saisie");
            lbModeSaisie.setName("lbModeSaisie");
            panel4.add(lbModeSaisie, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CHSAI ----
            CHSAI.setComponentPopupMenu(BTD);
            CHSAI.setMinimumSize(new Dimension(30, 28));
            CHSAI.setMaximumSize(new Dimension(30, 28));
            CHSAI.setPreferredSize(new Dimension(30, 28));
            CHSAI.setName("CHSAI");
            panel4.add(CHSAI, new GridBagConstraints(9, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlEspeces.add(panel4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- lbNumeroDocumentVente ----
            lbNumeroDocumentVente.setText("Num\u00e9ro document de vente");
            lbNumeroDocumentVente.setPreferredSize(new Dimension(180, 30));
            lbNumeroDocumentVente.setMinimumSize(new Dimension(180, 30));
            lbNumeroDocumentVente.setMaximumSize(new Dimension(180, 30));
            lbNumeroDocumentVente.setName("lbNumeroDocumentVente");
            sNPanel1.add(lbNumeroDocumentVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- XNUM ----
            XNUM.setComponentPopupMenu(BTD);
            XNUM.setMinimumSize(new Dimension(60, 28));
            XNUM.setPreferredSize(new Dimension(60, 28));
            XNUM.setMaximumSize(new Dimension(60, 28));
            XNUM.setName("XNUM");
            sNPanel1.add(XNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- XSUF ----
            XSUF.setComponentPopupMenu(BTD);
            XSUF.setMinimumSize(new Dimension(24, 28));
            XSUF.setPreferredSize(new Dimension(24, 28));
            XSUF.setMaximumSize(new Dimension(24, 28));
            XSUF.setName("XSUF");
            sNPanel1.add(XSUF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- XCOD ----
            XCOD.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Document de vente",
              "Devis"
            }));
            XCOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            XCOD.setName("XCOD");
            sNPanel1.add(XCOD, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEspeces.add(sNPanel1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlEspeces, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- lbClient ----
        lbClient.setText("Client");
        lbClient.setName("lbClient");
        pnlContenu.add(lbClient, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlClient ========
        {
          pnlClient.setPreferredSize(new Dimension(700, 180));
          pnlClient.setMinimumSize(new Dimension(700, 180));
          pnlClient.setName("pnlClient");
          pnlClient.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlClient.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlClient.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlClient.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlClient.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- CHCLA ----
          CHCLA.setComponentPopupMenu(BTD);
          CHCLA.setPreferredSize(new Dimension(160, 28));
          CHCLA.setMinimumSize(new Dimension(160, 28));
          CHCLA.setName("CHCLA");
          pnlClient.add(CHCLA, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- CHNOM ----
          CHNOM.setComponentPopupMenu(BTD);
          CHNOM.setMinimumSize(new Dimension(330, 28));
          CHNOM.setPreferredSize(new Dimension(330, 28));
          CHNOM.setName("CHNOM");
          pnlClient.add(CHNOM, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- CHAD1 ----
          CHAD1.setMinimumSize(new Dimension(330, 28));
          CHAD1.setPreferredSize(new Dimension(330, 28));
          CHAD1.setName("CHAD1");
          pnlClient.add(CHAD1, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- CHAD2 ----
          CHAD2.setMinimumSize(new Dimension(330, 28));
          CHAD2.setPreferredSize(new Dimension(330, 28));
          CHAD2.setName("CHAD2");
          pnlClient.add(CHAD2, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- CHETB2 ----
          CHETB2.setComponentPopupMenu(BTD);
          CHETB2.setMinimumSize(new Dimension(40, 28));
          CHETB2.setPreferredSize(new Dimension(40, 28));
          CHETB2.setName("CHETB2");
          pnlClient.add(CHETB2, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbEtablissementGVM ----
          lbEtablissementGVM.setText("Etablissement/GVM");
          lbEtablissementGVM.setName("lbEtablissementGVM");
          pnlClient.add(lbEtablissementGVM, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- CHAD3 ----
          CHAD3.setMinimumSize(new Dimension(330, 28));
          CHAD3.setPreferredSize(new Dimension(330, 28));
          CHAD3.setName("CHAD3");
          pnlClient.add(CHAD3, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- lbRaisonSociale ----
          lbRaisonSociale.setText("Nom ou raison sociale");
          lbRaisonSociale.setName("lbRaisonSociale");
          pnlClient.add(lbRaisonSociale, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- lbNumeroCompte ----
          lbNumeroCompte.setText("N\u00b0 compte");
          lbNumeroCompte.setPreferredSize(new Dimension(150, 30));
          lbNumeroCompte.setMinimumSize(new Dimension(150, 30));
          lbNumeroCompte.setName("lbNumeroCompte");
          pnlClient.add(lbNumeroCompte, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- lbClassement ----
          lbClassement.setText("Classement");
          lbClassement.setName("lbClassement");
          pnlClient.add(lbClassement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- CHNCG ----
          CHNCG.setComponentPopupMenu(BTD);
          CHNCG.setPreferredSize(new Dimension(60, 28));
          CHNCG.setMinimumSize(new Dimension(60, 28));
          CHNCG.setName("CHNCG");
          pnlClient.add(CHNCG, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 5), 0, 0));

          //---- CHNCA ----
          CHNCA.setComponentPopupMenu(BTD);
          CHNCA.setPreferredSize(new Dimension(60, 28));
          CHNCA.setMinimumSize(new Dimension(60, 28));
          CHNCA.setName("CHNCA");
          pnlClient.add(CHNCA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 1, 5), 0, 0));
        }
        pnlContenu.add(pnlClient, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- lbDomiciliation ----
        lbDomiciliation.setText("Domiciliation");
        lbDomiciliation.setName("lbDomiciliation");
        pnlContenu.add(lbDomiciliation, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlDomiciliation ========
        {
          pnlDomiciliation.setPreferredSize(new Dimension(500, 150));
          pnlDomiciliation.setMinimumSize(new Dimension(500, 150));
          pnlDomiciliation.setName("pnlDomiciliation");
          pnlDomiciliation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDomiciliation.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDomiciliation.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlDomiciliation.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlDomiciliation.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- CHRIE ----
          CHRIE.setComponentPopupMenu(BTD);
          CHRIE.setPreferredSize(new Dimension(310, 28));
          CHRIE.setMinimumSize(new Dimension(310, 28));
          CHRIE.setName("CHRIE");
          pnlDomiciliation.add(CHRIE, new GridBagConstraints(2, 2, 5, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- CHDOM ----
          CHDOM.setComponentPopupMenu(BTD);
          CHDOM.setPreferredSize(new Dimension(250, 28));
          CHDOM.setMinimumSize(new Dimension(250, 28));
          CHDOM.setName("CHDOM");
          pnlDomiciliation.add(CHDOM, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNomBanque ----
          lbNomBanque.setText("Nom de la banque");
          lbNomBanque.setName("lbNomBanque");
          pnlDomiciliation.add(lbNomBanque, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHNCB ----
          CHNCB.setComponentPopupMenu(BTD);
          CHNCB.setMinimumSize(new Dimension(110, 28));
          CHNCB.setPreferredSize(new Dimension(110, 28));
          CHNCB.setName("CHNCB");
          pnlDomiciliation.add(CHNCB, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNumeroCompteDom ----
          lbNumeroCompteDom.setText("N\u00b0 Compte");
          lbNumeroCompteDom.setName("lbNumeroCompteDom");
          pnlDomiciliation.add(lbNumeroCompteDom, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNumeroGuichetDom ----
          lbNumeroGuichetDom.setText("N\u00b0Guichet");
          lbNumeroGuichetDom.setPreferredSize(new Dimension(70, 19));
          lbNumeroGuichetDom.setMinimumSize(new Dimension(70, 19));
          lbNumeroGuichetDom.setName("lbNumeroGuichetDom");
          pnlDomiciliation.add(lbNumeroGuichetDom, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHCBQ ----
          CHCBQ.setComponentPopupMenu(BTD);
          CHCBQ.setMinimumSize(new Dimension(50, 28));
          CHCBQ.setPreferredSize(new Dimension(50, 28));
          CHCBQ.setName("CHCBQ");
          pnlDomiciliation.add(CHCBQ, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHCGU ----
          CHCGU.setComponentPopupMenu(BTD);
          CHCGU.setPreferredSize(new Dimension(60, 28));
          CHCGU.setMinimumSize(new Dimension(60, 28));
          CHCGU.setName("CHCGU");
          pnlDomiciliation.add(CHCGU, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNumeroEtablissementDom ----
          lbNumeroEtablissementDom.setText("N\u00b0 Etb");
          lbNumeroEtablissementDom.setName("lbNumeroEtablissementDom");
          pnlDomiciliation.add(lbNumeroEtablissementDom, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- CHRIB ----
          CHRIB.setComponentPopupMenu(BTD);
          CHRIB.setPreferredSize(new Dimension(30, 28));
          CHRIB.setMinimumSize(new Dimension(30, 28));
          CHRIB.setName("CHRIB");
          pnlDomiciliation.add(CHRIB, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbCleDom ----
          lbCleDom.setText("Cl\u00e9");
          lbCleDom.setName("lbCleDom");
          pnlDomiciliation.add(lbCleDom, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_127_OBJ_127 ----
          OBJ_127_OBJ_127.setText("@UCLEX@");
          OBJ_127_OBJ_127.setFont(OBJ_127_OBJ_127.getFont().deriveFont(OBJ_127_OBJ_127.getFont().getStyle() | Font.BOLD));
          OBJ_127_OBJ_127.setForeground(Color.red);
          OBJ_127_OBJ_127.setMinimumSize(new Dimension(30, 28));
          OBJ_127_OBJ_127.setPreferredSize(new Dimension(30, 28));
          OBJ_127_OBJ_127.setName("OBJ_127_OBJ_127");
          pnlDomiciliation.add(OBJ_127_OBJ_127, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- btDomiciliation ----
          btDomiciliation.setText("");
          btDomiciliation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btDomiciliation.setName("btDomiciliation");
          btDomiciliation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_133ActionPerformed(e);
            }
          });
          pnlDomiciliation.add(btDomiciliation, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        }
        pnlContenu.add(pnlDomiciliation, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField INDETB;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField INDIDU;
  private JLabel OBJ_35_OBJ_35;
  private XRiTextField CHCL2;
  private XRiTextField CHIMP;
  private JPanel p_tete_droite;
  private JLabel OBJ_51_OBJ_51;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbEspeces;
  private SNPanelTitre pnlEspeces;
  private JPanel panel2;
  private XRiTextField CHMTT;
  private XRiTextField CHMTR;
  private XRiCalendrier CHDRGX;
  private XRiTextField CHVDE;
  private SNLabelChamp lbMontant;
  private SNLabelChamp lbReglement;
  private SNLabelChamp lbVendeur;
  private SNLabelChamp lbMagasin;
  private XRiTextField CHMAG;
  private SNLabelChamp lbCompa1;
  private XRiTextField CHNC1X;
  private SNLabelChamp lbRemise;
  private XRiCalendrier CHDREX;
  private SNLabelChamp lbSur;
  private XRiTextField CHBRE;
  private SNLabelChamp lbSuffixe;
  private XRiTextField CHSRE;
  private SNLabelChamp lbCompa2;
  private XRiTextField CHNC2X;
  private JPanel panel4;
  private XRiTextField CHTCA;
  private SNLabelChamp lbNumeroCarte;
  private SNLabelChamp lbType;
  private SNLabelChamp lbValidation;
  private JPanel panel5;
  private XRiTextField CHVAL;
  private XRiTextField CHMOV;
  private XRiTextField CHANVR;
  private SNLabelChamp lbNumeroAutorisation;
  private XRiTextField CHAUT;
  private XRiTextField CB01;
  private XRiTextField CB02;
  private XRiTextField CB03;
  private XRiTextField CB04;
  private SNLabelChamp lbTypePaiement;
  private XRiTextField CHPAI;
  private SNLabelChamp lbModeSaisie;
  private XRiTextField CHSAI;
  private SNPanel sNPanel1;
  private SNLabelChamp lbNumeroDocumentVente;
  private XRiTextField XNUM;
  private XRiTextField XSUF;
  private XRiComboBox XCOD;
  private SNLabelTitre lbClient;
  private SNPanelTitre pnlClient;
  private XRiTextField CHCLA;
  private XRiTextField CHNOM;
  private XRiTextField CHAD1;
  private XRiTextField CHAD2;
  private XRiTextField CHETB2;
  private SNLabelChamp lbEtablissementGVM;
  private XRiTextField CHAD3;
  private SNLabelUnite lbRaisonSociale;
  private SNLabelUnite lbNumeroCompte;
  private SNLabelUnite lbClassement;
  private XRiTextField CHNCG;
  private XRiTextField CHNCA;
  private SNLabelTitre lbDomiciliation;
  private SNPanelTitre pnlDomiciliation;
  private XRiTextField CHRIE;
  private XRiTextField CHDOM;
  private SNLabelUnite lbNomBanque;
  private XRiTextField CHNCB;
  private SNLabelUnite lbNumeroCompteDom;
  private SNLabelUnite lbNumeroGuichetDom;
  private XRiTextField CHCBQ;
  private XRiTextField CHCGU;
  private SNLabelUnite lbNumeroEtablissementDom;
  private XRiTextField CHRIB;
  private SNLabelUnite lbCleDom;
  private RiZoneSortie OBJ_127_OBJ_127;
  private SNBoutonDetail btDomiciliation;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
