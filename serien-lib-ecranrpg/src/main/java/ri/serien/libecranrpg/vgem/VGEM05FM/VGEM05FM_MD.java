
package ri.serien.libecranrpg.vgem.VGEM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VGEM05FM_MD extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM05FM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix fonctions"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "R", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "C", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "M", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "I", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "A", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "D", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_11 = new JButton();
    OBJ_12 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setBackground(new Color(90, 90, 90));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setBackground(new Color(90, 90, 90));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(null, "Fonctions", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, null, Color.white));
          panel1.setBackground(new Color(90, 90, 90));
          panel1.setForeground(Color.white);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_7 ----
          OBJ_7.setText("Retour");
          OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_7.setName("OBJ_7");
          OBJ_7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_7ActionPerformed(e);
            }
          });
          panel1.add(OBJ_7);
          OBJ_7.setBounds(34, 42, 130, 24);

          //---- OBJ_8 ----
          OBJ_8.setText("Cr\u00e9ation");
          OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_8.setName("OBJ_8");
          OBJ_8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_8ActionPerformed(e);
            }
          });
          panel1.add(OBJ_8);
          OBJ_8.setBounds(34, 70, 130, 24);

          //---- OBJ_9 ----
          OBJ_9.setText("Modification");
          OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_9.setName("OBJ_9");
          OBJ_9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_9ActionPerformed(e);
            }
          });
          panel1.add(OBJ_9);
          OBJ_9.setBounds(34, 98, 130, 24);

          //---- OBJ_10 ----
          OBJ_10.setText("Interrogation");
          OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_10.setName("OBJ_10");
          OBJ_10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_10ActionPerformed(e);
            }
          });
          panel1.add(OBJ_10);
          OBJ_10.setBounds(34, 126, 130, 24);

          //---- OBJ_11 ----
          OBJ_11.setText("Annulation");
          OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_11.setName("OBJ_11");
          OBJ_11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_11ActionPerformed(e);
            }
          });
          panel1.add(OBJ_11);
          OBJ_11.setBounds(34, 154, 130, 24);

          //---- OBJ_12 ----
          OBJ_12.setText("Duplication");
          OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_12.setName("OBJ_12");
          OBJ_12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_12ActionPerformed(e);
            }
          });
          panel1.add(OBJ_12);
          OBJ_12.setBounds(34, 182, 130, 24);
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 196, 236);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_11;
  private JButton OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
