
package ri.serien.libecranrpg.vgem.VGEM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGEM08FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CHIN1_Value = { "", "S", "H", };
  private String[] CHACC_Value = { "", "0", "1", };
  
  public VGEM08FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CHACC.setValeurs(CHACC_Value, null);
    CHIN1.setValeurs(CHIN1_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_54_OBJ_54.setVisible(lexique.isPresent("NUMR"));
    OBJ_62_OBJ_62.setVisible(lexique.isPresent("CHVDE"));
    OBJ_55_OBJ_55.setVisible(lexique.isPresent("NUMR"));
    OBJ_56_OBJ_56.setVisible(lexique.isPresent("NUMR"));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("GESTION DES CHEQUES"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("CHIN1", 0, CHIN1_Value[CHIN1.getSelectedIndex()]);
    // lexique.HostFieldPutData("CHACC", 0, CHACC_Value[CHACC.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CHIN1 = new XRiComboBox();
    CHCL2 = new XRiTextField();
    WMCI = new XRiTextField();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    CHMTT = new XRiTextField();
    OBJ_27_OBJ_27 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    CHDACX = new XRiCalendrier();
    CHDEMX = new XRiCalendrier();
    CHDECX = new XRiCalendrier();
    OBJ_62_OBJ_62 = new JLabel();
    INDNUM = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_48_OBJ_48 = new JLabel();
    CHVDE = new XRiTextField();
    INDSUF = new XRiTextField();
    CHACC = new XRiComboBox();
    OBJ_89_OBJ_91 = new JLabel();
    CHMAG = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    CHNOM = new XRiTextField();
    CHDOM = new XRiTextField();
    OBJ_56_OBJ_56 = new JLabel();
    CHCLA = new XRiTextField();
    CHRTI = new XRiTextField();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    OBJ_58_OBJ_58 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    CHNCA = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(710, 420));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Ch\u00e8que");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- CHIN1 ----
          CHIN1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Sur place",
            "Hors place"
          }));
          CHIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHIN1.setName("CHIN1");
          xTitledPanel1ContentContainer.add(CHIN1);
          CHIN1.setBounds(145, 46, 120, CHIN1.getPreferredSize().height);

          //---- CHCL2 ----
          CHCL2.setComponentPopupMenu(BTD);
          CHCL2.setName("CHCL2");
          xTitledPanel1ContentContainer.add(CHCL2);
          CHCL2.setBounds(105, 135, 138, CHCL2.getPreferredSize().height);

          //---- WMCI ----
          WMCI.setName("WMCI");
          xTitledPanel1ContentContainer.add(WMCI);
          WMCI.setBounds(220, 15, 97, WMCI.getPreferredSize().height);

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("Date \u00e9ch\u00e9ance");
          OBJ_51_OBJ_51.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          xTitledPanel1ContentContainer.add(OBJ_51_OBJ_51);
          OBJ_51_OBJ_51.setBounds(279, 139, 96, 20);

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("Classement 2");
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
          xTitledPanel1ContentContainer.add(OBJ_50_OBJ_50);
          OBJ_50_OBJ_50.setBounds(15, 139, 92, 20);

          //---- CHMTT ----
          CHMTT.setComponentPopupMenu(BTD);
          CHMTT.setName("CHMTT");
          xTitledPanel1ContentContainer.add(CHMTT);
          CHMTT.setBounds(380, 45, 81, CHMTT.getPreferredSize().height);

          //---- OBJ_27_OBJ_27 ----
          OBJ_27_OBJ_27.setText("Commande");
          OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");
          xTitledPanel1ContentContainer.add(OBJ_27_OBJ_27);
          OBJ_27_OBJ_27.setBounds(15, 19, 82, 20);

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("Montant");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          xTitledPanel1ContentContainer.add(OBJ_46_OBJ_46);
          OBJ_46_OBJ_46.setBounds(315, 49, 60, 20);

          //---- CHDACX ----
          CHDACX.setComponentPopupMenu(BTD);
          CHDACX.setName("CHDACX");
          xTitledPanel1ContentContainer.add(CHDACX);
          CHDACX.setBounds(380, 105, 105, CHDACX.getPreferredSize().height);

          //---- CHDEMX ----
          CHDEMX.setComponentPopupMenu(BTD);
          CHDEMX.setName("CHDEMX");
          xTitledPanel1ContentContainer.add(CHDEMX);
          CHDEMX.setBounds(380, 75, 105, CHDEMX.getPreferredSize().height);

          //---- CHDECX ----
          CHDECX.setComponentPopupMenu(BTD);
          CHDECX.setName("CHDECX");
          xTitledPanel1ContentContainer.add(CHDECX);
          CHDECX.setBounds(380, 135, 105, CHDECX.getPreferredSize().height);

          //---- OBJ_62_OBJ_62 ----
          OBJ_62_OBJ_62.setText("Vendeur");
          OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
          xTitledPanel1ContentContainer.add(OBJ_62_OBJ_62);
          OBJ_62_OBJ_62.setBounds(15, 79, 60, 20);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");
          xTitledPanel1ContentContainer.add(INDNUM);
          INDNUM.setBounds(145, 15, 68, INDNUM.getPreferredSize().height);

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("Emission");
          OBJ_47_OBJ_47.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
          xTitledPanel1ContentContainer.add(OBJ_47_OBJ_47);
          OBJ_47_OBJ_47.setBounds(305, 79, 70, 20);

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Ch\u00e8que");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
          xTitledPanel1ContentContainer.add(OBJ_49_OBJ_49);
          OBJ_49_OBJ_49.setBounds(15, 49, 62, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          xTitledPanel1ContentContainer.add(INDETB);
          INDETB.setBounds(105, 17, 34, INDETB.getPreferredSize().height);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Portefeuille");
          OBJ_48_OBJ_48.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          xTitledPanel1ContentContainer.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(299, 109, 76, 20);

          //---- CHVDE ----
          CHVDE.setComponentPopupMenu(BTD);
          CHVDE.setName("CHVDE");
          xTitledPanel1ContentContainer.add(CHVDE);
          CHVDE.setBounds(105, 75, 34, CHVDE.getPreferredSize().height);

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");
          xTitledPanel1ContentContainer.add(INDSUF);
          INDSUF.setBounds(105, 45, 26, INDSUF.getPreferredSize().height);

          //---- CHACC ----
          CHACC.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Ch\u00e8que re\u00e7u",
            "Ch\u00e8que \u00e0 recevoir"
          }));
          CHACC.setName("CHACC");
          xTitledPanel1ContentContainer.add(CHACC);
          CHACC.setBounds(145, 76, 120, CHACC.getPreferredSize().height);

          //---- OBJ_89_OBJ_91 ----
          OBJ_89_OBJ_91.setText("Magasin");
          OBJ_89_OBJ_91.setName("OBJ_89_OBJ_91");
          xTitledPanel1ContentContainer.add(OBJ_89_OBJ_91);
          OBJ_89_OBJ_91.setBounds(15, 105, 67, 28);

          //---- CHMAG ----
          CHMAG.setComponentPopupMenu(BTD);
          CHMAG.setName("CHMAG");
          xTitledPanel1ContentContainer.add(CHMAG);
          CHMAG.setBounds(105, 105, 30, CHMAG.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Tireur");
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //---- CHNOM ----
          CHNOM.setComponentPopupMenu(BTD);
          CHNOM.setName("CHNOM");
          xTitledPanel2ContentContainer.add(CHNOM);
          CHNOM.setBounds(261, 25, 216, CHNOM.getPreferredSize().height);

          //---- CHDOM ----
          CHDOM.setComponentPopupMenu(BTD);
          CHDOM.setName("CHDOM");
          xTitledPanel2ContentContainer.add(CHDOM);
          CHDOM.setBounds(260, 80, 216, CHDOM.getPreferredSize().height);

          //---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("Nom ou raison sociale");
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
          xTitledPanel2ContentContainer.add(OBJ_56_OBJ_56);
          OBJ_56_OBJ_56.setBounds(261, 5, 210, 19);

          //---- CHCLA ----
          CHCLA.setComponentPopupMenu(BTD);
          CHCLA.setName("CHCLA");
          xTitledPanel2ContentContainer.add(CHCLA);
          CHCLA.setBounds(110, 25, 138, CHCLA.getPreferredSize().height);

          //---- CHRTI ----
          CHRTI.setComponentPopupMenu(BTD);
          CHRTI.setName("CHRTI");
          xTitledPanel2ContentContainer.add(CHRTI);
          CHRTI.setBounds(110, 80, 138, CHRTI.getPreferredSize().height);

          //---- OBJ_57_OBJ_57 ----
          OBJ_57_OBJ_57.setText("Nom de la banque - Ville");
          OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
          xTitledPanel2ContentContainer.add(OBJ_57_OBJ_57);
          OBJ_57_OBJ_57.setBounds(260, 60, 212, 19);

          //---- OBJ_55_OBJ_55 ----
          OBJ_55_OBJ_55.setText("Classement");
          OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");
          xTitledPanel2ContentContainer.add(OBJ_55_OBJ_55);
          OBJ_55_OBJ_55.setBounds(110, 5, 94, 19);

          //---- OBJ_58_OBJ_58 ----
          OBJ_58_OBJ_58.setText("R\u00e9f\u00e9rence");
          OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
          xTitledPanel2ContentContainer.add(OBJ_58_OBJ_58);
          OBJ_58_OBJ_58.setBounds(35, 85, 75, 19);

          //---- OBJ_54_OBJ_54 ----
          OBJ_54_OBJ_54.setText("Num\u00e9ro");
          OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
          xTitledPanel2ContentContainer.add(OBJ_54_OBJ_54);
          OBJ_54_OBJ_54.setBounds(35, 5, 60, 19);

          //---- CHNCA ----
          CHNCA.setComponentPopupMenu(BTD);
          CHNCA.setName("CHNCA");
          xTitledPanel2ContentContainer.add(CHNCA);
          CHNCA.setBounds(35, 25, 60, CHNCA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 500, GroupLayout.PREFERRED_SIZE)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 500, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox CHIN1;
  private XRiTextField CHCL2;
  private XRiTextField WMCI;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_50_OBJ_50;
  private XRiTextField CHMTT;
  private JLabel OBJ_27_OBJ_27;
  private JLabel OBJ_46_OBJ_46;
  private XRiCalendrier CHDACX;
  private XRiCalendrier CHDEMX;
  private XRiCalendrier CHDECX;
  private JLabel OBJ_62_OBJ_62;
  private XRiTextField INDNUM;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_49_OBJ_49;
  private RiZoneSortie INDETB;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField CHVDE;
  private XRiTextField INDSUF;
  private XRiComboBox CHACC;
  private JLabel OBJ_89_OBJ_91;
  private XRiTextField CHMAG;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField CHNOM;
  private XRiTextField CHDOM;
  private JLabel OBJ_56_OBJ_56;
  private XRiTextField CHCLA;
  private XRiTextField CHRTI;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_55_OBJ_55;
  private JLabel OBJ_58_OBJ_58;
  private JLabel OBJ_54_OBJ_54;
  private XRiTextField CHNCA;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
