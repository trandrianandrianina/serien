
package ri.serien.libecranrpg.vgem.VGEM16FM;
// Nom Fichier: pop_VGEM16FM_FMTAF_FMTF1_60.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VGEM16FM_AF extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGEM16FM_AF(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_4_OBJ_4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@(TITLE)@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Veuillez patienter"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_4_OBJ_4 = new JLabel();
    OBJ_5 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_4_OBJ_4 ----
    OBJ_4_OBJ_4.setText("@(TITLE)@");
    OBJ_4_OBJ_4.setName("OBJ_4_OBJ_4");
    add(OBJ_4_OBJ_4);
    OBJ_4_OBJ_4.setBounds(90, 36, 237, 20);

    //---- OBJ_5 ----
    OBJ_5.setText("");
    OBJ_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(12, 24, 54, 44);

    setPreferredSize(new Dimension(346, 98));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_4_OBJ_4;
  private JButton OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
