
package ri.serien.libecranrpg.vgem.VGEM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEM05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] EFACC_Value = { "0", "1", "2", "4", };
  
  public VGEM05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EFACC.setValeurs(EFACC_Value, null);
    EFPGE.setValeursSelection("2", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    UCLEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // EFACC.setSelectedIndex(getIndice("EFACC", EFACC_Value));
    EFSRE.setVisible(lexique.isPresent("EFSRE"));
    EFBRE.setVisible(lexique.isPresent("EFBRE"));
    EFRIB.setEnabled(lexique.isPresent("EFRIB"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    EFNC4X.setVisible(lexique.isPresent("EFNC4X"));
    EFNC3X.setVisible(lexique.isPresent("EFNC3X"));
    EFNC2X.setVisible(lexique.isPresent("EFNC2X"));
    EFNC1X.setVisible(lexique.isPresent("EFNC1X"));
    EFETB2.setVisible(lexique.isPresent("EFETB2"));
    EFCGU.setEnabled(lexique.isPresent("EFCGU"));
    EFCBQ.setEnabled(lexique.isPresent("EFCBQ"));
    OBJ_111_OBJ_111.setVisible(lexique.isTrue("N31"));
    OBJ_111_OBJ_112.setVisible(lexique.isTrue("31"));
    EFNCA.setEnabled(lexique.isPresent("EFNCA"));
    EFNCG.setEnabled(lexique.isPresent("EFNCG"));
    EFNCG.setVisible(lexique.isTrue("N31"));
    EFDIMX.setVisible(lexique.isPresent("EFDIMX"));
    EFDREX.setVisible(lexique.isPresent("EFDREX"));
    EFDACX.setVisible(lexique.isPresent("EFDACX"));
    EFDECX.setEnabled(lexique.isPresent("EFDECX"));
    EFDEMX.setVisible(lexique.isPresent("EFDEMX"));
    EFRTI.setEnabled(lexique.isPresent("EFRTI"));
    OBJ_52.setVisible(lexique.isTrue("32"));
    EFMTT.setEnabled(lexique.isPresent("EFMTT"));
    EFNCB.setEnabled(lexique.isPresent("EFNCB"));
    // EFPGE.setVisible( lexique.isPresent("EFPGE"));
    // EFPGE.setSelected(lexique.HostFieldGetData("EFPGE").equalsIgnoreCase("2"));
    EFCLA.setEnabled(lexique.isPresent("EFCLA"));
    OBJ_118_OBJ_118.setVisible(lexique.isPresent("EFETB2"));
    EFDOM.setEnabled(lexique.isPresent("EFDOM"));
    EFRIE.setEnabled(lexique.isPresent("EFRIE"));
    EFAD3.setVisible(lexique.isPresent("EFAD3"));
    EFAD2.setVisible(lexique.isPresent("EFAD2"));
    EFAD1.setVisible(lexique.isPresent("EFAD1"));
    EFNOM.setEnabled(lexique.isPresent("EFNOM"));
    // EFACC.setEnabled( lexique.isPresent("EFACC"));
    OBJ_133.setEnabled(lexique.isTrue("N53"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (EFPGE.isSelected())
    // lexique.HostFieldPutData("EFPGE", 0, "2");
    // else
    // lexique.HostFieldPutData("EFPGE", 0, " ");
    
    // lexique.HostFieldPutData("EFACC", 0, EFACC_Value[EFACC.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("EFIMP", 0, "I");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_133ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 17);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_45_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46_OBJ_46 = new JLabel();
    INDNUM = new XRiTextField();
    INDSUF = new XRiTextField();
    EFIMP = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_52 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    EFMTT = new XRiTextField();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_84_OBJ_84 = new JLabel();
    EFDEMX = new XRiTextField();
    EFDECX = new XRiTextField();
    OBJ_85_OBJ_85 = new JLabel();
    EFPGE = new XRiCheckBox();
    EFACC = new XRiComboBox();
    OBJ_86_OBJ_86 = new JLabel();
    EFDACX = new XRiTextField();
    OBJ_87_OBJ_87 = new JLabel();
    EFNC1X = new XRiTextField();
    OBJ_89_OBJ_89 = new JLabel();
    EFDREX = new XRiTextField();
    OBJ_91_OBJ_91 = new JLabel();
    EFBRE = new XRiTextField();
    OBJ_93_OBJ_93 = new JLabel();
    EFNRE = new XRiTextField();
    OBJ_95_OBJ_95 = new JLabel();
    EFSRE = new XRiTextField();
    OBJ_97_OBJ_97 = new JLabel();
    EFNC2X = new XRiTextField();
    OBJ_99_OBJ_99 = new JLabel();
    OBJ_100_OBJ_100 = new JLabel();
    EFNC3X = new XRiTextField();
    OBJ_102_OBJ_102 = new JLabel();
    EFDIMX = new XRiTextField();
    OBJ_104_OBJ_104 = new JLabel();
    EFNC4X = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    EFNOM = new XRiTextField();
    EFAD1 = new XRiTextField();
    EFAD2 = new XRiTextField();
    EFAD3 = new XRiTextField();
    OBJ_113_OBJ_113 = new JLabel();
    OBJ_118_OBJ_118 = new JLabel();
    EFCLA = new XRiTextField();
    OBJ_112_OBJ_112 = new JLabel();
    OBJ_111_OBJ_111 = new JLabel();
    EFRTI = new XRiTextField();
    OBJ_116_OBJ_116 = new JLabel();
    EFNCG = new XRiTextField();
    EFNCA = new XRiTextField();
    EFETB2 = new XRiTextField();
    OBJ_111_OBJ_112 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    EFRIE = new XRiTextField();
    EFDOM = new XRiTextField();
    OBJ_122_OBJ_122 = new JLabel();
    EFNCB = new XRiTextField();
    OBJ_125_OBJ_125 = new JLabel();
    OBJ_133 = new SNBoutonDetail();
    OBJ_124_OBJ_124 = new JLabel();
    EFCBQ = new XRiTextField();
    EFCGU = new XRiTextField();
    OBJ_123_OBJ_123 = new JLabel();
    EFRIB = new XRiTextField();
    OBJ_126_OBJ_126 = new JLabel();
    UCLEX = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_24 = new JMenuItem();
    OBJ_23 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des effets");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("Etablissement");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("Num\u00e9ro effet");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");

          //---- EFIMP ----
          EFIMP.setComponentPopupMenu(BTD);
          EFIMP.setName("EFIMP");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(EFIMP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(313, 313, 313))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EFIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_52 ----
          OBJ_52.setText("Affectation");
          OBJ_52.setFont(OBJ_52.getFont().deriveFont(OBJ_52.getFont().getSize() + 3f));
          OBJ_52.setName("OBJ_52");
          p_tete_droite.add(OBJ_52);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Saisie d'un impay\u00e9");
              riSousMenu_bt6.setToolTipText("Saisie d'un impay\u00e9");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("D\u00e9tail du portefeuille");
              riSousMenu_bt7.setToolTipText("D\u00e9tail du portefeuille");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Remise \u00e0 l'escompte");
              riSousMenu_bt8.setToolTipText("Remise \u00e0 l'escompte");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Remise \u00e0 l'encaissement");
              riSousMenu_bt9.setToolTipText("Remise \u00e0 l'encaissement");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Affectation factures");
              riSousMenu_bt10.setToolTipText("Activation de l'affectation de factures");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Effet");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- EFMTT ----
            EFMTT.setComponentPopupMenu(BTD);
            EFMTT.setName("EFMTT");
            xTitledPanel1ContentContainer.add(EFMTT);
            EFMTT.setBounds(15, 20, 116, EFMTT.getPreferredSize().height);

            //---- OBJ_83_OBJ_83 ----
            OBJ_83_OBJ_83.setText("Montant");
            OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
            xTitledPanel1ContentContainer.add(OBJ_83_OBJ_83);
            OBJ_83_OBJ_83.setBounds(15, 5, 78, 18);

            //---- OBJ_84_OBJ_84 ----
            OBJ_84_OBJ_84.setText("Emission");
            OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");
            xTitledPanel1ContentContainer.add(OBJ_84_OBJ_84);
            OBJ_84_OBJ_84.setBounds(150, 5, 58, 18);

            //---- EFDEMX ----
            EFDEMX.setToolTipText("Date");
            EFDEMX.setComponentPopupMenu(BTD);
            EFDEMX.setName("EFDEMX");
            xTitledPanel1ContentContainer.add(EFDEMX);
            EFDEMX.setBounds(145, 20, 65, EFDEMX.getPreferredSize().height);

            //---- EFDECX ----
            EFDECX.setToolTipText("Date");
            EFDECX.setComponentPopupMenu(BTD);
            EFDECX.setName("EFDECX");
            xTitledPanel1ContentContainer.add(EFDECX);
            EFDECX.setBounds(225, 20, 65, EFDECX.getPreferredSize().height);

            //---- OBJ_85_OBJ_85 ----
            OBJ_85_OBJ_85.setText("Ech\u00e9ance");
            OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
            xTitledPanel1ContentContainer.add(OBJ_85_OBJ_85);
            OBJ_85_OBJ_85.setBounds(230, 5, 64, 18);

            //---- EFPGE ----
            EFPGE.setText("Effet prorog\u00e9");
            EFPGE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EFPGE.setName("EFPGE");
            xTitledPanel1ContentContainer.add(EFPGE);
            EFPGE.setBounds(310, 24, 102, 20);

            //---- EFACC ----
            EFACC.setModel(new DefaultComboBoxModel(new String[] {
              "LCR non soumise \u00e0 l'acceptation",
              "LCR soumise \u00e0 l'acceptation",
              "Billet \u00e0 ordre relev\u00e9",
              "Billet \u00e0 ordre relev\u00e9 (en portefeuille)"
            }));
            EFACC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EFACC.setName("EFACC");
            xTitledPanel1ContentContainer.add(EFACC);
            EFACC.setBounds(440, 21, 310, EFACC.getPreferredSize().height);

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("Acceptation");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
            xTitledPanel1ContentContainer.add(OBJ_86_OBJ_86);
            OBJ_86_OBJ_86.setBounds(15, 64, 79, 20);

            //---- EFDACX ----
            EFDACX.setToolTipText("Date");
            EFDACX.setComponentPopupMenu(BTD);
            EFDACX.setName("EFDACX");
            xTitledPanel1ContentContainer.add(EFDACX);
            EFDACX.setBounds(225, 60, 65, EFDACX.getPreferredSize().height);

            //---- OBJ_87_OBJ_87 ----
            OBJ_87_OBJ_87.setText("Comptabilis\u00e9");
            OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
            xTitledPanel1ContentContainer.add(OBJ_87_OBJ_87);
            OBJ_87_OBJ_87.setBounds(590, 64, 120, 20);

            //---- EFNC1X ----
            EFNC1X.setComponentPopupMenu(BTD);
            EFNC1X.setName("EFNC1X");
            xTitledPanel1ContentContainer.add(EFNC1X);
            EFNC1X.setBounds(710, 60, 40, EFNC1X.getPreferredSize().height);

            //---- OBJ_89_OBJ_89 ----
            OBJ_89_OBJ_89.setText("Remise");
            OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
            xTitledPanel1ContentContainer.add(OBJ_89_OBJ_89);
            OBJ_89_OBJ_89.setBounds(15, 99, 52, 20);

            //---- EFDREX ----
            EFDREX.setToolTipText("Date");
            EFDREX.setComponentPopupMenu(BTD);
            EFDREX.setEditable(false);
            EFDREX.setName("EFDREX");
            xTitledPanel1ContentContainer.add(EFDREX);
            EFDREX.setBounds(225, 95, 65, EFDREX.getPreferredSize().height);

            //---- OBJ_91_OBJ_91 ----
            OBJ_91_OBJ_91.setText("Banque");
            OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");
            xTitledPanel1ContentContainer.add(OBJ_91_OBJ_91);
            OBJ_91_OBJ_91.setBounds(310, 99, 51, 20);

            //---- EFBRE ----
            EFBRE.setComponentPopupMenu(BTD);
            EFBRE.setName("EFBRE");
            xTitledPanel1ContentContainer.add(EFBRE);
            EFBRE.setBounds(360, 95, 30, EFBRE.getPreferredSize().height);

            //---- OBJ_93_OBJ_93 ----
            OBJ_93_OBJ_93.setText("Type");
            OBJ_93_OBJ_93.setName("OBJ_93_OBJ_93");
            xTitledPanel1ContentContainer.add(OBJ_93_OBJ_93);
            OBJ_93_OBJ_93.setBounds(405, 99, 36, 20);

            //---- EFNRE ----
            EFNRE.setComponentPopupMenu(BTD);
            EFNRE.setName("EFNRE");
            xTitledPanel1ContentContainer.add(EFNRE);
            EFNRE.setBounds(440, 95, 20, EFNRE.getPreferredSize().height);

            //---- OBJ_95_OBJ_95 ----
            OBJ_95_OBJ_95.setText("Suffixe");
            OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");
            xTitledPanel1ContentContainer.add(OBJ_95_OBJ_95);
            OBJ_95_OBJ_95.setBounds(475, 99, 51, 20);

            //---- EFSRE ----
            EFSRE.setName("EFSRE");
            xTitledPanel1ContentContainer.add(EFSRE);
            EFSRE.setBounds(525, 95, 30, EFSRE.getPreferredSize().height);

            //---- OBJ_97_OBJ_97 ----
            OBJ_97_OBJ_97.setText("Comptabilis\u00e9");
            OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
            xTitledPanel1ContentContainer.add(OBJ_97_OBJ_97);
            OBJ_97_OBJ_97.setBounds(590, 99, 120, 20);

            //---- EFNC2X ----
            EFNC2X.setComponentPopupMenu(BTD);
            EFNC2X.setName("EFNC2X");
            xTitledPanel1ContentContainer.add(EFNC2X);
            EFNC2X.setBounds(710, 95, 40, EFNC2X.getPreferredSize().height);

            //---- OBJ_99_OBJ_99 ----
            OBJ_99_OBJ_99.setText("R\u00e8glement \u00e0 \u00e9ch\u00e9ance");
            OBJ_99_OBJ_99.setName("OBJ_99_OBJ_99");
            xTitledPanel1ContentContainer.add(OBJ_99_OBJ_99);
            OBJ_99_OBJ_99.setBounds(15, 134, 145, 20);

            //---- OBJ_100_OBJ_100 ----
            OBJ_100_OBJ_100.setText("Comptabilis\u00e9");
            OBJ_100_OBJ_100.setName("OBJ_100_OBJ_100");
            xTitledPanel1ContentContainer.add(OBJ_100_OBJ_100);
            OBJ_100_OBJ_100.setBounds(590, 134, 120, 20);

            //---- EFNC3X ----
            EFNC3X.setComponentPopupMenu(BTD);
            EFNC3X.setName("EFNC3X");
            xTitledPanel1ContentContainer.add(EFNC3X);
            EFNC3X.setBounds(710, 130, 40, EFNC3X.getPreferredSize().height);

            //---- OBJ_102_OBJ_102 ----
            OBJ_102_OBJ_102.setText("R\u00e8glement manuel");
            OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");
            xTitledPanel1ContentContainer.add(OBJ_102_OBJ_102);
            OBJ_102_OBJ_102.setBounds(15, 169, 120, 20);

            //---- EFDIMX ----
            EFDIMX.setToolTipText("Date");
            EFDIMX.setComponentPopupMenu(BTD);
            EFDIMX.setName("EFDIMX");
            xTitledPanel1ContentContainer.add(EFDIMX);
            EFDIMX.setBounds(225, 165, 65, EFDIMX.getPreferredSize().height);

            //---- OBJ_104_OBJ_104 ----
            OBJ_104_OBJ_104.setText("Comptabilis\u00e9");
            OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");
            xTitledPanel1ContentContainer.add(OBJ_104_OBJ_104);
            OBJ_104_OBJ_104.setBounds(590, 169, 120, 20);

            //---- EFNC4X ----
            EFNC4X.setComponentPopupMenu(BTD);
            EFNC4X.setName("EFNC4X");
            xTitledPanel1ContentContainer.add(EFNC4X);
            EFNC4X.setBounds(710, 165, 40, EFNC4X.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Tir\u00e9");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- EFNOM ----
            EFNOM.setComponentPopupMenu(BTD);
            EFNOM.setName("EFNOM");
            xTitledPanel2ContentContainer.add(EFNOM);
            EFNOM.setBounds(440, 20, 310, EFNOM.getPreferredSize().height);

            //---- EFAD1 ----
            EFAD1.setName("EFAD1");
            xTitledPanel2ContentContainer.add(EFAD1);
            EFAD1.setBounds(440, 50, 310, EFAD1.getPreferredSize().height);

            //---- EFAD2 ----
            EFAD2.setName("EFAD2");
            xTitledPanel2ContentContainer.add(EFAD2);
            EFAD2.setBounds(440, 80, 310, EFAD2.getPreferredSize().height);

            //---- EFAD3 ----
            EFAD3.setName("EFAD3");
            xTitledPanel2ContentContainer.add(EFAD3);
            EFAD3.setBounds(440, 110, 310, EFAD3.getPreferredSize().height);

            //---- OBJ_113_OBJ_113 ----
            OBJ_113_OBJ_113.setText("Nom ou raison sociale");
            OBJ_113_OBJ_113.setName("OBJ_113_OBJ_113");
            xTitledPanel2ContentContainer.add(OBJ_113_OBJ_113);
            OBJ_113_OBJ_113.setBounds(440, 0, 163, 20);

            //---- OBJ_118_OBJ_118 ----
            OBJ_118_OBJ_118.setText("Etablissement/GVM");
            OBJ_118_OBJ_118.setName("OBJ_118_OBJ_118");
            xTitledPanel2ContentContainer.add(OBJ_118_OBJ_118);
            OBJ_118_OBJ_118.setBounds(15, 96, 123, 20);

            //---- EFCLA ----
            EFCLA.setComponentPopupMenu(BTD);
            EFCLA.setName("EFCLA");
            xTitledPanel2ContentContainer.add(EFCLA);
            EFCLA.setBounds(140, 30, 132, EFCLA.getPreferredSize().height);

            //---- OBJ_112_OBJ_112 ----
            OBJ_112_OBJ_112.setText("Classement");
            OBJ_112_OBJ_112.setName("OBJ_112_OBJ_112");
            xTitledPanel2ContentContainer.add(OBJ_112_OBJ_112);
            OBJ_112_OBJ_112.setBounds(140, 10, 93, 20);

            //---- OBJ_111_OBJ_111 ----
            OBJ_111_OBJ_111.setText("Num\u00e9ro compte client");
            OBJ_111_OBJ_111.setName("OBJ_111_OBJ_111");
            xTitledPanel2ContentContainer.add(OBJ_111_OBJ_111);
            OBJ_111_OBJ_111.setBounds(15, 10, 125, 20);

            //---- EFRTI ----
            EFRTI.setComponentPopupMenu(BTD);
            EFRTI.setName("EFRTI");
            xTitledPanel2ContentContainer.add(EFRTI);
            EFRTI.setBounds(140, 61, 92, EFRTI.getPreferredSize().height);

            //---- OBJ_116_OBJ_116 ----
            OBJ_116_OBJ_116.setText("R\u00e9f\u00e9rence tir\u00e9");
            OBJ_116_OBJ_116.setName("OBJ_116_OBJ_116");
            xTitledPanel2ContentContainer.add(OBJ_116_OBJ_116);
            OBJ_116_OBJ_116.setBounds(15, 65, 105, 20);

            //---- EFNCG ----
            EFNCG.setComponentPopupMenu(BTD);
            EFNCG.setName("EFNCG");
            xTitledPanel2ContentContainer.add(EFNCG);
            EFNCG.setBounds(15, 30, 60, EFNCG.getPreferredSize().height);

            //---- EFNCA ----
            EFNCA.setComponentPopupMenu(BTD);
            EFNCA.setName("EFNCA");
            xTitledPanel2ContentContainer.add(EFNCA);
            EFNCA.setBounds(80, 30, 60, EFNCA.getPreferredSize().height);

            //---- EFETB2 ----
            EFETB2.setComponentPopupMenu(BTD);
            EFETB2.setName("EFETB2");
            xTitledPanel2ContentContainer.add(EFETB2);
            EFETB2.setBounds(140, 205, 40, EFETB2.getPreferredSize().height);

            //---- OBJ_111_OBJ_112 ----
            OBJ_111_OBJ_112.setText("Num\u00e9ro");
            OBJ_111_OBJ_112.setName("OBJ_111_OBJ_112");
            xTitledPanel2ContentContainer.add(OBJ_111_OBJ_112);
            OBJ_111_OBJ_112.setBounds(80, 10, 55, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitle("Domiciliation");
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- EFRIE ----
            EFRIE.setComponentPopupMenu(BTD);
            EFRIE.setName("EFRIE");
            xTitledPanel3ContentContainer.add(EFRIE);
            EFRIE.setBounds(440, 55, 310, EFRIE.getPreferredSize().height);

            //---- EFDOM ----
            EFDOM.setComponentPopupMenu(BTD);
            EFDOM.setName("EFDOM");
            xTitledPanel3ContentContainer.add(EFDOM);
            EFDOM.setBounds(15, 25, 250, EFDOM.getPreferredSize().height);

            //---- OBJ_122_OBJ_122 ----
            OBJ_122_OBJ_122.setText("Nom de la banque");
            OBJ_122_OBJ_122.setName("OBJ_122_OBJ_122");
            xTitledPanel3ContentContainer.add(OBJ_122_OBJ_122);
            OBJ_122_OBJ_122.setBounds(15, 5, 114, 20);

            //---- EFNCB ----
            EFNCB.setComponentPopupMenu(BTD);
            EFNCB.setName("EFNCB");
            xTitledPanel3ContentContainer.add(EFNCB);
            EFNCB.setBounds(570, 25, 100, EFNCB.getPreferredSize().height);

            //---- OBJ_125_OBJ_125 ----
            OBJ_125_OBJ_125.setText("N\u00b0 de compte");
            OBJ_125_OBJ_125.setName("OBJ_125_OBJ_125");
            xTitledPanel3ContentContainer.add(OBJ_125_OBJ_125);
            OBJ_125_OBJ_125.setBounds(570, 5, 97, 20);

            //---- OBJ_133 ----
            OBJ_133.setText("");
            OBJ_133.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_133.setName("OBJ_133");
            OBJ_133.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_133ActionPerformed(e);
              }
            });
            xTitledPanel3ContentContainer.add(OBJ_133);
            OBJ_133.setBounds(265, 20, 35, 40);

            //---- OBJ_124_OBJ_124 ----
            OBJ_124_OBJ_124.setText("N\u00b0Guichet");
            OBJ_124_OBJ_124.setName("OBJ_124_OBJ_124");
            xTitledPanel3ContentContainer.add(OBJ_124_OBJ_124);
            OBJ_124_OBJ_124.setBounds(505, 5, 63, 20);

            //---- EFCBQ ----
            EFCBQ.setComponentPopupMenu(BTD);
            EFCBQ.setName("EFCBQ");
            xTitledPanel3ContentContainer.add(EFCBQ);
            EFCBQ.setBounds(440, 25, 60, EFCBQ.getPreferredSize().height);

            //---- EFCGU ----
            EFCGU.setComponentPopupMenu(BTD);
            EFCGU.setName("EFCGU");
            xTitledPanel3ContentContainer.add(EFCGU);
            EFCGU.setBounds(505, 25, 60, EFCGU.getPreferredSize().height);

            //---- OBJ_123_OBJ_123 ----
            OBJ_123_OBJ_123.setText("N\u00b0 Etb");
            OBJ_123_OBJ_123.setName("OBJ_123_OBJ_123");
            xTitledPanel3ContentContainer.add(OBJ_123_OBJ_123);
            OBJ_123_OBJ_123.setBounds(440, 5, 40, 20);

            //---- EFRIB ----
            EFRIB.setComponentPopupMenu(BTD);
            EFRIB.setName("EFRIB");
            xTitledPanel3ContentContainer.add(EFRIB);
            EFRIB.setBounds(685, 25, 30, EFRIB.getPreferredSize().height);

            //---- OBJ_126_OBJ_126 ----
            OBJ_126_OBJ_126.setText("Cl\u00e9");
            OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");
            xTitledPanel3ContentContainer.add(OBJ_126_OBJ_126);
            OBJ_126_OBJ_126.setBounds(685, 5, 24, 20);

            //---- UCLEX ----
            UCLEX.setText("@UCLEX@");
            UCLEX.setName("UCLEX");
            xTitledPanel3ContentContainer.add(UCLEX);
            UCLEX.setBounds(726, 27, 24, UCLEX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(xTitledPanel3, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                  .addComponent(xTitledPanel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                  .addComponent(xTitledPanel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 231, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_24 ----
      OBJ_24.setText("Choix possibles");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_23 ----
      OBJ_23.setText("Aide en ligne");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_45_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_46_OBJ_46;
  private XRiTextField INDNUM;
  private XRiTextField INDSUF;
  private XRiTextField EFIMP;
  private JPanel p_tete_droite;
  private JLabel OBJ_52;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField EFMTT;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_84_OBJ_84;
  private XRiTextField EFDEMX;
  private XRiTextField EFDECX;
  private JLabel OBJ_85_OBJ_85;
  private XRiCheckBox EFPGE;
  private XRiComboBox EFACC;
  private JLabel OBJ_86_OBJ_86;
  private XRiTextField EFDACX;
  private JLabel OBJ_87_OBJ_87;
  private XRiTextField EFNC1X;
  private JLabel OBJ_89_OBJ_89;
  private XRiTextField EFDREX;
  private JLabel OBJ_91_OBJ_91;
  private XRiTextField EFBRE;
  private JLabel OBJ_93_OBJ_93;
  private XRiTextField EFNRE;
  private JLabel OBJ_95_OBJ_95;
  private XRiTextField EFSRE;
  private JLabel OBJ_97_OBJ_97;
  private XRiTextField EFNC2X;
  private JLabel OBJ_99_OBJ_99;
  private JLabel OBJ_100_OBJ_100;
  private XRiTextField EFNC3X;
  private JLabel OBJ_102_OBJ_102;
  private XRiTextField EFDIMX;
  private JLabel OBJ_104_OBJ_104;
  private XRiTextField EFNC4X;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField EFNOM;
  private XRiTextField EFAD1;
  private XRiTextField EFAD2;
  private XRiTextField EFAD3;
  private JLabel OBJ_113_OBJ_113;
  private JLabel OBJ_118_OBJ_118;
  private XRiTextField EFCLA;
  private JLabel OBJ_112_OBJ_112;
  private JLabel OBJ_111_OBJ_111;
  private XRiTextField EFRTI;
  private JLabel OBJ_116_OBJ_116;
  private XRiTextField EFNCG;
  private XRiTextField EFNCA;
  private XRiTextField EFETB2;
  private JLabel OBJ_111_OBJ_112;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField EFRIE;
  private XRiTextField EFDOM;
  private JLabel OBJ_122_OBJ_122;
  private XRiTextField EFNCB;
  private JLabel OBJ_125_OBJ_125;
  private SNBoutonDetail OBJ_133;
  private JLabel OBJ_124_OBJ_124;
  private XRiTextField EFCBQ;
  private XRiTextField EFCGU;
  private JLabel OBJ_123_OBJ_123;
  private XRiTextField EFRIB;
  private JLabel OBJ_126_OBJ_126;
  private RiZoneSortie UCLEX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_23;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
