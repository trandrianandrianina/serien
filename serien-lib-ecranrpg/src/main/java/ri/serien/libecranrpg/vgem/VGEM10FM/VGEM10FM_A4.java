
package ri.serien.libecranrpg.vgem.VGEM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGEM10FM_A4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTR01_Top = { "WTR01", "WTR02", "WTR03", "WTR04", "WTR05", "WTR06", "WTR07", "WTR08", "WTR09", "WTR10", "WTR11",
      "WTR12", "WTR13", "WTR14", "WTR15", };
  private String[] _WTR01_Title = { "Numéro     Echéance Création   Montant  N°Cli. Nom du Client  Acc Bq D.Envoi", };
  private String[][] _WTR01_Data = { { "LR01", }, { "LR02", }, { "LR03", }, { "LR04", }, { "LR05", }, { "LR06", }, { "LR07", }, { "LR08", },
      { "LR09", }, { "LR10", }, { "LR11", }, { "LR12", }, { "LR13", }, { "LR14", }, { "LR15", }, };
  private int[] _WTR01_Width = { 82, };
  
  public VGEM10FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTR01.setAspectTable(_WTR01_Top, _WTR01_Title, _WTR01_Data, _WTR01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIBREM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBREM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTR01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    INDBRE.setVisible(lexique.isPresent("INDBRE"));
    INDSRE.setEnabled(lexique.isPresent("INDSRE"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    R1NBE.setVisible(lexique.isPresent("R1NBE"));
    WDIMX.setVisible(lexique.isPresent("WDIMX"));
    INDDRX.setVisible(lexique.isPresent("INDDRX"));
    P88DFX.setEnabled(lexique.isPresent("P88DFX"));
    P88DEX.setEnabled(lexique.isPresent("P88DEX"));
    OBJ_49_OBJ_49.setVisible(lexique.isPresent("WDIMX"));
    P88MTT.setEnabled(lexique.isPresent("P88MTT"));
    R1MTT.setVisible(lexique.isPresent("R1MTT"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTA.getInvoker().getName());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTI.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTI.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTR01_Top, "+", "Enter");
    WTR01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTR01_Top, "-", "Enter");
    WTR01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTR01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTR01_Top, "+", "Enter", e);
    if (WTR01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_64_OBJ_64 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_65_OBJ_65 = new JLabel();
    INDBRE = new XRiTextField();
    OBJ_66_OBJ_66 = new JLabel();
    INDDRX = new XRiTextField();
    OBJ_67_OBJ_67 = new JLabel();
    INDSRE = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    LIBREM = new RiZoneSortie();
    OBJ_49_OBJ_49 = new JLabel();
    WDIMX = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    panel3 = new JPanel();
    R1MTT = new XRiTextField();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    P88MTT = new XRiTextField();
    P88DEX = new XRiTextField();
    P88DFX = new XRiTextField();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_82_OBJ_82 = new JLabel();
    R1NBE = new XRiTextField();
    OBJ_88_OBJ_88 = new JLabel();
    panel4 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTR01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTA = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    BTI = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_33 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Demande de pr\u00e9l\u00e8vement ordinaire");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(960, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("Etablissement");
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTA);
          INDETB.setName("INDETB");

          //---- OBJ_65_OBJ_65 ----
          OBJ_65_OBJ_65.setText("Banque");
          OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

          //---- INDBRE ----
          INDBRE.setComponentPopupMenu(BTI);
          INDBRE.setName("INDBRE");

          //---- OBJ_66_OBJ_66 ----
          OBJ_66_OBJ_66.setText("Date remise");
          OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

          //---- INDDRX ----
          INDDRX.setComponentPopupMenu(BTA);
          INDDRX.setName("INDDRX");

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("Suf");
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

          //---- INDSRE ----
          INDSRE.setComponentPopupMenu(BTA);
          INDSRE.setName("INDSRE");

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Type de remise");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

          //---- LIBREM ----
          LIBREM.setText("@LIBREM@");
          LIBREM.setOpaque(false);
          LIBREM.setName("LIBREM");

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Date r\u00e8glement");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

          //---- WDIMX ----
          WDIMX.setComponentPopupMenu(BTA);
          WDIMX.setName("WDIMX");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDBRE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDDRX, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDSRE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WDIMX, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(LIBREM, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_64_OBJ_64))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_65_OBJ_65))
              .addComponent(INDBRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_66_OBJ_66))
              .addComponent(INDDRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_67_OBJ_67))
              .addComponent(INDSRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_49_OBJ_49))
              .addComponent(WDIMX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_48_OBJ_48))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(LIBREM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Envoi automatique");
              riSousMenu_bt6.setToolTipText("Envoi automatique");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Pr\u00e9l\u00e8vement acc\u00e9l\u00e9r\u00e9");
              riSousMenu_bt7.setToolTipText("Pr\u00e9paration pr\u00e9l\u00e8vement acc\u00e9l\u00e9r\u00e9");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Pr\u00e9l\u00e8vement ordinaire");
              riSousMenu_bt8.setToolTipText("Pr\u00e9paration pr\u00e9l\u00e8vement ordinaire");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Liste des envois");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(""));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- R1MTT ----
              R1MTT.setName("R1MTT");
              panel3.add(R1MTT);
              R1MTT.setBounds(95, 15, 108, R1MTT.getPreferredSize().height);

              //---- OBJ_86_OBJ_86 ----
              OBJ_86_OBJ_86.setText("Ech\u00e9ance du");
              OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
              panel3.add(OBJ_86_OBJ_86);
              OBJ_86_OBJ_86.setBounds(495, 19, 90, 20);

              //---- OBJ_80_OBJ_80 ----
              OBJ_80_OBJ_80.setText("Total envoi");
              OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
              panel3.add(OBJ_80_OBJ_80);
              OBJ_80_OBJ_80.setBounds(15, 19, 78, 20);

              //---- P88MTT ----
              P88MTT.setComponentPopupMenu(BTA);
              P88MTT.setName("P88MTT");
              panel3.add(P88MTT);
              P88MTT.setBounds(390, 15, 92, P88MTT.getPreferredSize().height);

              //---- P88DEX ----
              P88DEX.setComponentPopupMenu(BTA);
              P88DEX.setName("P88DEX");
              panel3.add(P88DEX);
              P88DEX.setBounds(582, 15, 65, P88DEX.getPreferredSize().height);

              //---- P88DFX ----
              P88DFX.setComponentPopupMenu(BTA);
              P88DFX.setName("P88DFX");
              panel3.add(P88DFX);
              P88DFX.setBounds(675, 15, 65, P88DFX.getPreferredSize().height);

              //---- OBJ_81_OBJ_81 ----
              OBJ_81_OBJ_81.setText("Nombre");
              OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");
              panel3.add(OBJ_81_OBJ_81);
              OBJ_81_OBJ_81.setBounds(215, 19, 55, 20);

              //---- OBJ_82_OBJ_82 ----
              OBJ_82_OBJ_82.setText("Montant");
              OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");
              panel3.add(OBJ_82_OBJ_82);
              OBJ_82_OBJ_82.setBounds(335, 19, 56, 20);

              //---- R1NBE ----
              R1NBE.setName("R1NBE");
              panel3.add(R1NBE);
              R1NBE.setBounds(270, 15, 52, R1NBE.getPreferredSize().height);

              //---- OBJ_88_OBJ_88 ----
              OBJ_88_OBJ_88.setText("au");
              OBJ_88_OBJ_88.setHorizontalTextPosition(SwingConstants.CENTER);
              OBJ_88_OBJ_88.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
              panel3.add(OBJ_88_OBJ_88);
              OBJ_88_OBJ_88.setBounds(645, 19, 30, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel3);
            panel3.setBounds(15, 20, 770, 70);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //======== SCROLLPANE_LIST ========
              {
                SCROLLPANE_LIST.setComponentPopupMenu(BTD);
                SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

                //---- WTR01 ----
                WTR01.setComponentPopupMenu(BTD);
                WTR01.setName("WTR01");
                WTR01.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    WTR01MouseClicked(e);
                  }
                });
                SCROLLPANE_LIST.setViewportView(WTR01);
              }
              panel4.add(SCROLLPANE_LIST);
              SCROLLPANE_LIST.setBounds(40, 25, 650, 275);

              //---- BT_PGUP ----
              BT_PGUP.setText("");
              BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGUP.setName("BT_PGUP");
              panel4.add(BT_PGUP);
              BT_PGUP.setBounds(700, 25, 25, 125);

              //---- BT_PGDOWN ----
              BT_PGDOWN.setText("");
              BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGDOWN.setName("BT_PGDOWN");
              panel4.add(BT_PGDOWN);
              BT_PGDOWN.setBounds(700, 175, 25, 125);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel4);
            panel4.setBounds(15, 100, 770, 325);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 814, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTA ========
    {
      BTA.setName("BTA");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTA.add(OBJ_20);
    }

    //======== BTI ========
    {
      BTI.setName("BTI");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTI.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTI.add(OBJ_22);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_25 ----
      OBJ_25.setText("Envoi");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("Suppression envoi");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);
      BTD.addSeparator();

      //---- OBJ_34 ----
      OBJ_34.setText("Choix possibles");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_33 ----
      OBJ_33.setText("Aide en ligne");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      BTD.add(OBJ_33);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField INDETB;
  private JLabel OBJ_65_OBJ_65;
  private XRiTextField INDBRE;
  private JLabel OBJ_66_OBJ_66;
  private XRiTextField INDDRX;
  private JLabel OBJ_67_OBJ_67;
  private XRiTextField INDSRE;
  private JLabel OBJ_48_OBJ_48;
  private RiZoneSortie LIBREM;
  private JLabel OBJ_49_OBJ_49;
  private XRiTextField WDIMX;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JPanel panel3;
  private XRiTextField R1MTT;
  private JLabel OBJ_86_OBJ_86;
  private JLabel OBJ_80_OBJ_80;
  private XRiTextField P88MTT;
  private XRiTextField P88DEX;
  private XRiTextField P88DFX;
  private JLabel OBJ_81_OBJ_81;
  private JLabel OBJ_82_OBJ_82;
  private XRiTextField R1NBE;
  private JLabel OBJ_88_OBJ_88;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTR01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTA;
  private JMenuItem OBJ_20;
  private JPopupMenu BTI;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  private JPopupMenu BTD;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_33;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
