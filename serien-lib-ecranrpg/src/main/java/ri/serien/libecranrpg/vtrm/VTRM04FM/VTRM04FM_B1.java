
package ri.serien.libecranrpg.vtrm.VTRM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VTRM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VTRM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ZGLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZGLIB@")).trim());
    EXLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIB@")).trim());
    TRLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_titre = new JPanel();
    titre = new JLabel();
    fond_titre = new JLabel();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    label1 = new JLabel();
    INDETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    label2 = new JLabel();
    INDMEX = new XRiTextField();
    label3 = new JLabel();
    INDCTR = new XRiTextField();
    label4 = new JLabel();
    INDZON = new XRiTextField();
    ZGLIB = new RiZoneSortie();
    EXLIB = new RiZoneSortie();
    TRLIB = new RiZoneSortie();
    panel3 = new JPanel();
    label5 = new JLabel();
    TRMTF = new XRiTextField();
    label6 = new JLabel();
    TRART = new XRiTextField();
    label7 = new JLabel();
    TRBAS = new XRiTextField();
    panel1 = new JPanel();
    label9 = new JLabel();
    label10 = new JLabel();
    TTFF1 = new XRiTextField();
    TTFS1 = new XRiTextField();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    label24 = new JLabel();
    label25 = new JLabel();
    label26 = new JLabel();
    label27 = new JLabel();
    label28 = new JLabel();
    label29 = new JLabel();
    TTFF2 = new XRiTextField();
    TTFS2 = new XRiTextField();
    TTFF3 = new XRiTextField();
    TTFS3 = new XRiTextField();
    TTFF4 = new XRiTextField();
    TTFS4 = new XRiTextField();
    TTFF5 = new XRiTextField();
    TTFS5 = new XRiTextField();
    TTFF6 = new XRiTextField();
    TTFS6 = new XRiTextField();
    TTFF7 = new XRiTextField();
    TTFS7 = new XRiTextField();
    TTFF8 = new XRiTextField();
    TTFS8 = new XRiTextField();
    TTFF9 = new XRiTextField();
    TTFS9 = new XRiTextField();
    TTFF10 = new XRiTextField();
    TTFS10 = new XRiTextField();
    TTFF11 = new XRiTextField();
    TTFS11 = new XRiTextField();
    TTFF12 = new XRiTextField();
    TTFS12 = new XRiTextField();
    panel4 = new JPanel();
    label11 = new JLabel();
    label12 = new JLabel();
    TTFF13 = new XRiTextField();
    TTFS13 = new XRiTextField();
    label30 = new JLabel();
    label31 = new JLabel();
    label32 = new JLabel();
    label33 = new JLabel();
    label34 = new JLabel();
    label35 = new JLabel();
    label36 = new JLabel();
    label37 = new JLabel();
    label38 = new JLabel();
    label39 = new JLabel();
    label40 = new JLabel();
    label41 = new JLabel();
    TTFF14 = new XRiTextField();
    TTFS14 = new XRiTextField();
    TTFF15 = new XRiTextField();
    TTFS15 = new XRiTextField();
    TTFF16 = new XRiTextField();
    TTFS16 = new XRiTextField();
    TTFF17 = new XRiTextField();
    TTFS17 = new XRiTextField();
    TTFF18 = new XRiTextField();
    TTFS18 = new XRiTextField();
    TTFF19 = new XRiTextField();
    TTFS19 = new XRiTextField();
    TTFF20 = new XRiTextField();
    TTFS20 = new XRiTextField();
    TTFF21 = new XRiTextField();
    TTFS21 = new XRiTextField();
    TTFF22 = new XRiTextField();
    TTFS22 = new XRiTextField();
    TTFF23 = new XRiTextField();
    TTFS23 = new XRiTextField();
    TTFF24 = new XRiTextField();
    TTFS24 = new XRiTextField();
    panel5 = new JPanel();
    label13 = new JLabel();
    label14 = new JLabel();
    TTFF25 = new XRiTextField();
    TTFS25 = new XRiTextField();
    label42 = new JLabel();
    label43 = new JLabel();
    label44 = new JLabel();
    label45 = new JLabel();
    label46 = new JLabel();
    label47 = new JLabel();
    label48 = new JLabel();
    label49 = new JLabel();
    label50 = new JLabel();
    label51 = new JLabel();
    label52 = new JLabel();
    label53 = new JLabel();
    TTFF26 = new XRiTextField();
    TTFS26 = new XRiTextField();
    TTFF27 = new XRiTextField();
    TTFS27 = new XRiTextField();
    TTFF28 = new XRiTextField();
    TTFS28 = new XRiTextField();
    TTFF29 = new XRiTextField();
    TTFS29 = new XRiTextField();
    TTFF30 = new XRiTextField();
    TTFS30 = new XRiTextField();
    TTFF31 = new XRiTextField();
    TTFS31 = new XRiTextField();
    TTFF32 = new XRiTextField();
    TTFS32 = new XRiTextField();
    TTFF33 = new XRiTextField();
    TTFS33 = new XRiTextField();
    TTFF34 = new XRiTextField();
    TTFS34 = new XRiTextField();
    TTFF35 = new XRiTextField();
    TTFS35 = new XRiTextField();
    TTFF36 = new XRiTextField();
    TTFS36 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);

          //---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);

        //======== p_titre ========
        {
          p_titre.setBackground(new Color(238, 239, 241));
          p_titre.setPreferredSize(new Dimension(930, 0));
          p_titre.setMinimumSize(new Dimension(930, 0));
          p_titre.setName("p_titre");
          p_titre.setLayout(null);

          //---- titre ----
          titre.setBackground(new Color(198, 198, 200));
          titre.setText("Grille transporteur 2");
          titre.setHorizontalAlignment(SwingConstants.LEFT);
          titre.setFont(titre.getFont().deriveFont(titre.getFont().getStyle() | Font.BOLD, titre.getFont().getSize() + 6f));
          titre.setForeground(Color.darkGray);
          titre.setName("titre");
          p_titre.add(titre);
          titre.setBounds(10, 0, 600, 55);

          //---- fond_titre ----
          fond_titre.setFont(new Font("Arial", Font.BOLD, 18));
          fond_titre.setForeground(Color.white);
          fond_titre.setName("fond_titre");
          p_titre.add(fond_titre);
          fond_titre.setBounds(0, 0, 900, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_titre.getComponentCount(); i++) {
              Rectangle bounds = p_titre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_titre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_titre.setMinimumSize(preferredSize);
            p_titre.setPreferredSize(preferredSize);
          }
        }
        p_presentation.add(p_titre, BorderLayout.CENTER);
      }
      p_nord.add(p_presentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- label1 ----
          label1.setText("Etablissement");
          label1.setName("label1");
          p_tete_gauche.add(label1);
          label1.setBounds(5, 5, 95, 20);

          //---- INDETB ----
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Bloc notes");
              riSousMenu_bt6.setToolTipText("Bloc notes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
              new Font("sansserif", Font.BOLD, 12)));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label2 ----
            label2.setText("Mode d'exp\u00e9dition");
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(25, 14, 160, 20);

            //---- INDMEX ----
            INDMEX.setName("INDMEX");
            panel2.add(INDMEX);
            INDMEX.setBounds(190, 10, 34, INDMEX.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Code transporteur");
            label3.setName("label3");
            panel2.add(label3);
            label3.setBounds(25, 46, 160, 20);

            //---- INDCTR ----
            INDCTR.setName("INDCTR");
            panel2.add(INDCTR);
            INDCTR.setBounds(190, 42, 34, INDCTR.getPreferredSize().height);

            //---- label4 ----
            label4.setText("Zone g\u00e9ographique");
            label4.setName("label4");
            panel2.add(label4);
            label4.setBounds(25, 78, 160, 20);

            //---- INDZON ----
            INDZON.setName("INDZON");
            panel2.add(INDZON);
            INDZON.setBounds(190, 74, 60, INDZON.getPreferredSize().height);

            //---- ZGLIB ----
            ZGLIB.setText("@ZGLIB@");
            ZGLIB.setName("ZGLIB");
            panel2.add(ZGLIB);
            ZGLIB.setBounds(275, 76, 310, ZGLIB.getPreferredSize().height);

            //---- EXLIB ----
            EXLIB.setText("@EXLIB@");
            EXLIB.setName("EXLIB");
            panel2.add(EXLIB);
            EXLIB.setBounds(275, 12, 310, EXLIB.getPreferredSize().height);

            //---- TRLIB ----
            TRLIB.setText("@TRLIB@");
            TRLIB.setName("TRLIB");
            panel2.add(TRLIB);
            TRLIB.setBounds(275, 44, 310, TRLIB.getPreferredSize().height);
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label5 ----
            label5.setText("Montant fixe");
            label5.setName("label5");
            panel3.add(label5);
            label5.setBounds(30, 19, 75, 20);

            //---- TRMTF ----
            TRMTF.setName("TRMTF");
            panel3.add(TRMTF);
            TRMTF.setBounds(125, 15, 80, TRMTF.getPreferredSize().height);

            //---- label6 ----
            label6.setText("Code article associ\u00e9");
            label6.setName("label6");
            panel3.add(label6);
            label6.setBounds(275, 19, 140, 20);

            //---- TRART ----
            TRART.setName("TRART");
            panel3.add(TRART);
            TRART.setBounds(422, 15, 110, TRART.getPreferredSize().height);

            //---- label7 ----
            label7.setText("Base");
            label7.setName("label7");
            panel3.add(label7);
            label7.setBounds(635, 19, 75, 20);

            //---- TRBAS ----
            TRBAS.setName("TRBAS");
            panel3.add(TRBAS);
            TRBAS.setBounds(749, 15, 80, TRBAS.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new EtchedBorder());
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- label9 ----
              label9.setText("Forfait");
              label9.setHorizontalAlignment(SwingConstants.CENTER);
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
              label9.setName("label9");
              panel1.add(label9);
              label9.setBounds(45, 10, 80, 20);

              //---- label10 ----
              label10.setText("Forfait sup.");
              label10.setHorizontalAlignment(SwingConstants.CENTER);
              label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
              label10.setName("label10");
              panel1.add(label10);
              label10.setBounds(120, 10, 100, 20);

              //---- TTFF1 ----
              TTFF1.setName("TTFF1");
              panel1.add(TTFF1);
              TTFF1.setBounds(45, 35, 80, TTFF1.getPreferredSize().height);

              //---- TTFS1 ----
              TTFS1.setName("TTFS1");
              panel1.add(TTFS1);
              TTFS1.setBounds(130, 35, 80, TTFS1.getPreferredSize().height);

              //---- label18 ----
              label18.setText("1");
              label18.setHorizontalAlignment(SwingConstants.RIGHT);
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setName("label18");
              panel1.add(label18);
              label18.setBounds(10, 39, 20, 20);

              //---- label19 ----
              label19.setText("2");
              label19.setHorizontalAlignment(SwingConstants.RIGHT);
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setName("label19");
              panel1.add(label19);
              label19.setBounds(10, 64, 20, 20);

              //---- label20 ----
              label20.setText("3");
              label20.setHorizontalAlignment(SwingConstants.RIGHT);
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setName("label20");
              panel1.add(label20);
              label20.setBounds(10, 89, 20, 20);

              //---- label21 ----
              label21.setText("4");
              label21.setHorizontalAlignment(SwingConstants.RIGHT);
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setName("label21");
              panel1.add(label21);
              label21.setBounds(10, 114, 20, 20);

              //---- label22 ----
              label22.setText("5");
              label22.setHorizontalAlignment(SwingConstants.RIGHT);
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setName("label22");
              panel1.add(label22);
              label22.setBounds(10, 139, 20, 20);

              //---- label23 ----
              label23.setText("6");
              label23.setHorizontalAlignment(SwingConstants.RIGHT);
              label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD));
              label23.setName("label23");
              panel1.add(label23);
              label23.setBounds(10, 164, 20, 20);

              //---- label24 ----
              label24.setText("7");
              label24.setHorizontalAlignment(SwingConstants.RIGHT);
              label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD));
              label24.setName("label24");
              panel1.add(label24);
              label24.setBounds(10, 189, 20, 20);

              //---- label25 ----
              label25.setText("8");
              label25.setHorizontalAlignment(SwingConstants.RIGHT);
              label25.setFont(label25.getFont().deriveFont(label25.getFont().getStyle() | Font.BOLD));
              label25.setName("label25");
              panel1.add(label25);
              label25.setBounds(10, 214, 20, 20);

              //---- label26 ----
              label26.setText("9");
              label26.setHorizontalAlignment(SwingConstants.RIGHT);
              label26.setFont(label26.getFont().deriveFont(label26.getFont().getStyle() | Font.BOLD));
              label26.setName("label26");
              panel1.add(label26);
              label26.setBounds(10, 239, 20, 20);

              //---- label27 ----
              label27.setText("10");
              label27.setHorizontalAlignment(SwingConstants.RIGHT);
              label27.setFont(label27.getFont().deriveFont(label27.getFont().getStyle() | Font.BOLD));
              label27.setName("label27");
              panel1.add(label27);
              label27.setBounds(10, 264, 20, 20);

              //---- label28 ----
              label28.setText("11");
              label28.setHorizontalAlignment(SwingConstants.RIGHT);
              label28.setFont(label28.getFont().deriveFont(label28.getFont().getStyle() | Font.BOLD));
              label28.setName("label28");
              panel1.add(label28);
              label28.setBounds(10, 289, 20, 20);

              //---- label29 ----
              label29.setText("12");
              label29.setHorizontalAlignment(SwingConstants.RIGHT);
              label29.setFont(label29.getFont().deriveFont(label29.getFont().getStyle() | Font.BOLD));
              label29.setName("label29");
              panel1.add(label29);
              label29.setBounds(10, 314, 20, 20);

              //---- TTFF2 ----
              TTFF2.setName("TTFF2");
              panel1.add(TTFF2);
              TTFF2.setBounds(45, 60, 80, TTFF2.getPreferredSize().height);

              //---- TTFS2 ----
              TTFS2.setName("TTFS2");
              panel1.add(TTFS2);
              TTFS2.setBounds(130, 60, 80, TTFS2.getPreferredSize().height);

              //---- TTFF3 ----
              TTFF3.setName("TTFF3");
              panel1.add(TTFF3);
              TTFF3.setBounds(45, 85, 80, TTFF3.getPreferredSize().height);

              //---- TTFS3 ----
              TTFS3.setName("TTFS3");
              panel1.add(TTFS3);
              TTFS3.setBounds(130, 85, 80, TTFS3.getPreferredSize().height);

              //---- TTFF4 ----
              TTFF4.setName("TTFF4");
              panel1.add(TTFF4);
              TTFF4.setBounds(45, 110, 80, TTFF4.getPreferredSize().height);

              //---- TTFS4 ----
              TTFS4.setName("TTFS4");
              panel1.add(TTFS4);
              TTFS4.setBounds(130, 110, 80, TTFS4.getPreferredSize().height);

              //---- TTFF5 ----
              TTFF5.setName("TTFF5");
              panel1.add(TTFF5);
              TTFF5.setBounds(45, 135, 80, TTFF5.getPreferredSize().height);

              //---- TTFS5 ----
              TTFS5.setName("TTFS5");
              panel1.add(TTFS5);
              TTFS5.setBounds(130, 135, 80, TTFS5.getPreferredSize().height);

              //---- TTFF6 ----
              TTFF6.setName("TTFF6");
              panel1.add(TTFF6);
              TTFF6.setBounds(45, 160, 80, TTFF6.getPreferredSize().height);

              //---- TTFS6 ----
              TTFS6.setName("TTFS6");
              panel1.add(TTFS6);
              TTFS6.setBounds(130, 160, 80, TTFS6.getPreferredSize().height);

              //---- TTFF7 ----
              TTFF7.setName("TTFF7");
              panel1.add(TTFF7);
              TTFF7.setBounds(45, 185, 80, TTFF7.getPreferredSize().height);

              //---- TTFS7 ----
              TTFS7.setName("TTFS7");
              panel1.add(TTFS7);
              TTFS7.setBounds(130, 185, 80, TTFS7.getPreferredSize().height);

              //---- TTFF8 ----
              TTFF8.setName("TTFF8");
              panel1.add(TTFF8);
              TTFF8.setBounds(45, 210, 80, TTFF8.getPreferredSize().height);

              //---- TTFS8 ----
              TTFS8.setName("TTFS8");
              panel1.add(TTFS8);
              TTFS8.setBounds(130, 210, 80, TTFS8.getPreferredSize().height);

              //---- TTFF9 ----
              TTFF9.setName("TTFF9");
              panel1.add(TTFF9);
              TTFF9.setBounds(45, 235, 80, TTFF9.getPreferredSize().height);

              //---- TTFS9 ----
              TTFS9.setName("TTFS9");
              panel1.add(TTFS9);
              TTFS9.setBounds(130, 235, 80, TTFS9.getPreferredSize().height);

              //---- TTFF10 ----
              TTFF10.setName("TTFF10");
              panel1.add(TTFF10);
              TTFF10.setBounds(45, 260, 80, TTFF10.getPreferredSize().height);

              //---- TTFS10 ----
              TTFS10.setName("TTFS10");
              panel1.add(TTFS10);
              TTFS10.setBounds(130, 260, 80, TTFS10.getPreferredSize().height);

              //---- TTFF11 ----
              TTFF11.setName("TTFF11");
              panel1.add(TTFF11);
              TTFF11.setBounds(45, 285, 80, TTFF11.getPreferredSize().height);

              //---- TTFS11 ----
              TTFS11.setName("TTFS11");
              panel1.add(TTFS11);
              TTFS11.setBounds(130, 285, 80, TTFS11.getPreferredSize().height);

              //---- TTFF12 ----
              TTFF12.setName("TTFF12");
              panel1.add(TTFF12);
              TTFF12.setBounds(45, 310, 80, TTFF12.getPreferredSize().height);

              //---- TTFS12 ----
              TTFS12.setName("TTFS12");
              panel1.add(TTFS12);
              TTFS12.setBounds(130, 310, 80, TTFS12.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel1);
            panel1.setBounds(20, 55, 240, 350);

            //======== panel4 ========
            {
              panel4.setBorder(new EtchedBorder());
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- label11 ----
              label11.setText("Forfait");
              label11.setHorizontalAlignment(SwingConstants.CENTER);
              label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
              label11.setName("label11");
              panel4.add(label11);
              label11.setBounds(45, 10, 80, 20);

              //---- label12 ----
              label12.setText("Forfait sup.");
              label12.setHorizontalAlignment(SwingConstants.CENTER);
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
              label12.setName("label12");
              panel4.add(label12);
              label12.setBounds(120, 10, 100, 20);

              //---- TTFF13 ----
              TTFF13.setName("TTFF13");
              panel4.add(TTFF13);
              TTFF13.setBounds(45, 35, 80, TTFF13.getPreferredSize().height);

              //---- TTFS13 ----
              TTFS13.setName("TTFS13");
              panel4.add(TTFS13);
              TTFS13.setBounds(130, 35, 80, TTFS13.getPreferredSize().height);

              //---- label30 ----
              label30.setText("13");
              label30.setHorizontalAlignment(SwingConstants.RIGHT);
              label30.setFont(label30.getFont().deriveFont(label30.getFont().getStyle() | Font.BOLD));
              label30.setName("label30");
              panel4.add(label30);
              label30.setBounds(10, 39, 20, 20);

              //---- label31 ----
              label31.setText("14");
              label31.setHorizontalAlignment(SwingConstants.RIGHT);
              label31.setFont(label31.getFont().deriveFont(label31.getFont().getStyle() | Font.BOLD));
              label31.setName("label31");
              panel4.add(label31);
              label31.setBounds(10, 64, 20, 20);

              //---- label32 ----
              label32.setText("15");
              label32.setHorizontalAlignment(SwingConstants.RIGHT);
              label32.setFont(label32.getFont().deriveFont(label32.getFont().getStyle() | Font.BOLD));
              label32.setName("label32");
              panel4.add(label32);
              label32.setBounds(10, 89, 20, 20);

              //---- label33 ----
              label33.setText("16");
              label33.setHorizontalAlignment(SwingConstants.RIGHT);
              label33.setFont(label33.getFont().deriveFont(label33.getFont().getStyle() | Font.BOLD));
              label33.setName("label33");
              panel4.add(label33);
              label33.setBounds(10, 114, 20, 20);

              //---- label34 ----
              label34.setText("17");
              label34.setHorizontalAlignment(SwingConstants.RIGHT);
              label34.setFont(label34.getFont().deriveFont(label34.getFont().getStyle() | Font.BOLD));
              label34.setName("label34");
              panel4.add(label34);
              label34.setBounds(10, 139, 20, 20);

              //---- label35 ----
              label35.setText("18");
              label35.setHorizontalAlignment(SwingConstants.RIGHT);
              label35.setFont(label35.getFont().deriveFont(label35.getFont().getStyle() | Font.BOLD));
              label35.setName("label35");
              panel4.add(label35);
              label35.setBounds(10, 164, 20, 20);

              //---- label36 ----
              label36.setText("19");
              label36.setHorizontalAlignment(SwingConstants.RIGHT);
              label36.setFont(label36.getFont().deriveFont(label36.getFont().getStyle() | Font.BOLD));
              label36.setName("label36");
              panel4.add(label36);
              label36.setBounds(10, 189, 20, 20);

              //---- label37 ----
              label37.setText("20");
              label37.setHorizontalAlignment(SwingConstants.RIGHT);
              label37.setFont(label37.getFont().deriveFont(label37.getFont().getStyle() | Font.BOLD));
              label37.setName("label37");
              panel4.add(label37);
              label37.setBounds(10, 214, 20, 20);

              //---- label38 ----
              label38.setText("21");
              label38.setHorizontalAlignment(SwingConstants.RIGHT);
              label38.setFont(label38.getFont().deriveFont(label38.getFont().getStyle() | Font.BOLD));
              label38.setName("label38");
              panel4.add(label38);
              label38.setBounds(10, 239, 20, 20);

              //---- label39 ----
              label39.setText("22");
              label39.setHorizontalAlignment(SwingConstants.RIGHT);
              label39.setFont(label39.getFont().deriveFont(label39.getFont().getStyle() | Font.BOLD));
              label39.setName("label39");
              panel4.add(label39);
              label39.setBounds(10, 264, 20, 20);

              //---- label40 ----
              label40.setText("23");
              label40.setHorizontalAlignment(SwingConstants.RIGHT);
              label40.setFont(label40.getFont().deriveFont(label40.getFont().getStyle() | Font.BOLD));
              label40.setName("label40");
              panel4.add(label40);
              label40.setBounds(10, 289, 20, 20);

              //---- label41 ----
              label41.setText("24");
              label41.setHorizontalAlignment(SwingConstants.RIGHT);
              label41.setFont(label41.getFont().deriveFont(label41.getFont().getStyle() | Font.BOLD));
              label41.setName("label41");
              panel4.add(label41);
              label41.setBounds(10, 314, 20, 20);

              //---- TTFF14 ----
              TTFF14.setName("TTFF14");
              panel4.add(TTFF14);
              TTFF14.setBounds(45, 60, 80, TTFF14.getPreferredSize().height);

              //---- TTFS14 ----
              TTFS14.setName("TTFS14");
              panel4.add(TTFS14);
              TTFS14.setBounds(130, 60, 80, TTFS14.getPreferredSize().height);

              //---- TTFF15 ----
              TTFF15.setName("TTFF15");
              panel4.add(TTFF15);
              TTFF15.setBounds(45, 85, 80, TTFF15.getPreferredSize().height);

              //---- TTFS15 ----
              TTFS15.setName("TTFS15");
              panel4.add(TTFS15);
              TTFS15.setBounds(130, 85, 80, TTFS15.getPreferredSize().height);

              //---- TTFF16 ----
              TTFF16.setName("TTFF16");
              panel4.add(TTFF16);
              TTFF16.setBounds(45, 110, 80, TTFF16.getPreferredSize().height);

              //---- TTFS16 ----
              TTFS16.setName("TTFS16");
              panel4.add(TTFS16);
              TTFS16.setBounds(130, 110, 80, TTFS16.getPreferredSize().height);

              //---- TTFF17 ----
              TTFF17.setName("TTFF17");
              panel4.add(TTFF17);
              TTFF17.setBounds(45, 135, 80, TTFF17.getPreferredSize().height);

              //---- TTFS17 ----
              TTFS17.setName("TTFS17");
              panel4.add(TTFS17);
              TTFS17.setBounds(130, 135, 80, TTFS17.getPreferredSize().height);

              //---- TTFF18 ----
              TTFF18.setName("TTFF18");
              panel4.add(TTFF18);
              TTFF18.setBounds(45, 160, 80, TTFF18.getPreferredSize().height);

              //---- TTFS18 ----
              TTFS18.setName("TTFS18");
              panel4.add(TTFS18);
              TTFS18.setBounds(130, 160, 80, TTFS18.getPreferredSize().height);

              //---- TTFF19 ----
              TTFF19.setName("TTFF19");
              panel4.add(TTFF19);
              TTFF19.setBounds(45, 185, 80, TTFF19.getPreferredSize().height);

              //---- TTFS19 ----
              TTFS19.setName("TTFS19");
              panel4.add(TTFS19);
              TTFS19.setBounds(130, 185, 80, TTFS19.getPreferredSize().height);

              //---- TTFF20 ----
              TTFF20.setName("TTFF20");
              panel4.add(TTFF20);
              TTFF20.setBounds(45, 210, 80, TTFF20.getPreferredSize().height);

              //---- TTFS20 ----
              TTFS20.setName("TTFS20");
              panel4.add(TTFS20);
              TTFS20.setBounds(130, 210, 80, TTFS20.getPreferredSize().height);

              //---- TTFF21 ----
              TTFF21.setName("TTFF21");
              panel4.add(TTFF21);
              TTFF21.setBounds(45, 235, 80, TTFF21.getPreferredSize().height);

              //---- TTFS21 ----
              TTFS21.setName("TTFS21");
              panel4.add(TTFS21);
              TTFS21.setBounds(130, 235, 80, TTFS21.getPreferredSize().height);

              //---- TTFF22 ----
              TTFF22.setName("TTFF22");
              panel4.add(TTFF22);
              TTFF22.setBounds(45, 260, 80, TTFF22.getPreferredSize().height);

              //---- TTFS22 ----
              TTFS22.setName("TTFS22");
              panel4.add(TTFS22);
              TTFS22.setBounds(130, 260, 80, TTFS22.getPreferredSize().height);

              //---- TTFF23 ----
              TTFF23.setName("TTFF23");
              panel4.add(TTFF23);
              TTFF23.setBounds(45, 285, 80, TTFF23.getPreferredSize().height);

              //---- TTFS23 ----
              TTFS23.setName("TTFS23");
              panel4.add(TTFS23);
              TTFS23.setBounds(130, 285, 80, TTFS23.getPreferredSize().height);

              //---- TTFF24 ----
              TTFF24.setName("TTFF24");
              panel4.add(TTFF24);
              TTFF24.setBounds(45, 310, 80, TTFF24.getPreferredSize().height);

              //---- TTFS24 ----
              TTFS24.setName("TTFS24");
              panel4.add(TTFS24);
              TTFS24.setBounds(130, 310, 80, TTFS24.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel4);
            panel4.setBounds(312, 55, 240, 350);

            //======== panel5 ========
            {
              panel5.setBorder(new EtchedBorder());
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- label13 ----
              label13.setText("Forfait");
              label13.setHorizontalAlignment(SwingConstants.CENTER);
              label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
              label13.setName("label13");
              panel5.add(label13);
              label13.setBounds(45, 10, 80, 20);

              //---- label14 ----
              label14.setText("Forfait sup.");
              label14.setHorizontalAlignment(SwingConstants.CENTER);
              label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
              label14.setName("label14");
              panel5.add(label14);
              label14.setBounds(120, 10, 100, 20);

              //---- TTFF25 ----
              TTFF25.setName("TTFF25");
              panel5.add(TTFF25);
              TTFF25.setBounds(45, 35, 80, TTFF25.getPreferredSize().height);

              //---- TTFS25 ----
              TTFS25.setName("TTFS25");
              panel5.add(TTFS25);
              TTFS25.setBounds(130, 35, 80, TTFS25.getPreferredSize().height);

              //---- label42 ----
              label42.setText("25");
              label42.setHorizontalAlignment(SwingConstants.RIGHT);
              label42.setFont(label42.getFont().deriveFont(label42.getFont().getStyle() | Font.BOLD));
              label42.setName("label42");
              panel5.add(label42);
              label42.setBounds(10, 39, 20, 20);

              //---- label43 ----
              label43.setText("26");
              label43.setHorizontalAlignment(SwingConstants.RIGHT);
              label43.setFont(label43.getFont().deriveFont(label43.getFont().getStyle() | Font.BOLD));
              label43.setName("label43");
              panel5.add(label43);
              label43.setBounds(10, 64, 20, 20);

              //---- label44 ----
              label44.setText("27");
              label44.setHorizontalAlignment(SwingConstants.RIGHT);
              label44.setFont(label44.getFont().deriveFont(label44.getFont().getStyle() | Font.BOLD));
              label44.setName("label44");
              panel5.add(label44);
              label44.setBounds(10, 89, 20, 20);

              //---- label45 ----
              label45.setText("28");
              label45.setHorizontalAlignment(SwingConstants.RIGHT);
              label45.setFont(label45.getFont().deriveFont(label45.getFont().getStyle() | Font.BOLD));
              label45.setName("label45");
              panel5.add(label45);
              label45.setBounds(10, 114, 20, 20);

              //---- label46 ----
              label46.setText("29");
              label46.setHorizontalAlignment(SwingConstants.RIGHT);
              label46.setFont(label46.getFont().deriveFont(label46.getFont().getStyle() | Font.BOLD));
              label46.setName("label46");
              panel5.add(label46);
              label46.setBounds(10, 139, 20, 20);

              //---- label47 ----
              label47.setText("30");
              label47.setHorizontalAlignment(SwingConstants.RIGHT);
              label47.setFont(label47.getFont().deriveFont(label47.getFont().getStyle() | Font.BOLD));
              label47.setName("label47");
              panel5.add(label47);
              label47.setBounds(10, 164, 20, 20);

              //---- label48 ----
              label48.setText("31");
              label48.setHorizontalAlignment(SwingConstants.RIGHT);
              label48.setFont(label48.getFont().deriveFont(label48.getFont().getStyle() | Font.BOLD));
              label48.setName("label48");
              panel5.add(label48);
              label48.setBounds(10, 189, 20, 20);

              //---- label49 ----
              label49.setText("32");
              label49.setHorizontalAlignment(SwingConstants.RIGHT);
              label49.setFont(label49.getFont().deriveFont(label49.getFont().getStyle() | Font.BOLD));
              label49.setName("label49");
              panel5.add(label49);
              label49.setBounds(10, 214, 20, 20);

              //---- label50 ----
              label50.setText("33");
              label50.setHorizontalAlignment(SwingConstants.RIGHT);
              label50.setFont(label50.getFont().deriveFont(label50.getFont().getStyle() | Font.BOLD));
              label50.setName("label50");
              panel5.add(label50);
              label50.setBounds(10, 239, 20, 20);

              //---- label51 ----
              label51.setText("34");
              label51.setHorizontalAlignment(SwingConstants.RIGHT);
              label51.setFont(label51.getFont().deriveFont(label51.getFont().getStyle() | Font.BOLD));
              label51.setName("label51");
              panel5.add(label51);
              label51.setBounds(10, 264, 20, 20);

              //---- label52 ----
              label52.setText("35");
              label52.setHorizontalAlignment(SwingConstants.RIGHT);
              label52.setFont(label52.getFont().deriveFont(label52.getFont().getStyle() | Font.BOLD));
              label52.setName("label52");
              panel5.add(label52);
              label52.setBounds(10, 289, 20, 20);

              //---- label53 ----
              label53.setText("36");
              label53.setHorizontalAlignment(SwingConstants.RIGHT);
              label53.setFont(label53.getFont().deriveFont(label53.getFont().getStyle() | Font.BOLD));
              label53.setName("label53");
              panel5.add(label53);
              label53.setBounds(10, 314, 20, 20);

              //---- TTFF26 ----
              TTFF26.setName("TTFF26");
              panel5.add(TTFF26);
              TTFF26.setBounds(45, 60, 80, TTFF26.getPreferredSize().height);

              //---- TTFS26 ----
              TTFS26.setName("TTFS26");
              panel5.add(TTFS26);
              TTFS26.setBounds(130, 60, 80, TTFS26.getPreferredSize().height);

              //---- TTFF27 ----
              TTFF27.setName("TTFF27");
              panel5.add(TTFF27);
              TTFF27.setBounds(45, 85, 80, TTFF27.getPreferredSize().height);

              //---- TTFS27 ----
              TTFS27.setName("TTFS27");
              panel5.add(TTFS27);
              TTFS27.setBounds(130, 85, 80, TTFS27.getPreferredSize().height);

              //---- TTFF28 ----
              TTFF28.setName("TTFF28");
              panel5.add(TTFF28);
              TTFF28.setBounds(45, 110, 80, TTFF28.getPreferredSize().height);

              //---- TTFS28 ----
              TTFS28.setName("TTFS28");
              panel5.add(TTFS28);
              TTFS28.setBounds(130, 110, 80, TTFS28.getPreferredSize().height);

              //---- TTFF29 ----
              TTFF29.setName("TTFF29");
              panel5.add(TTFF29);
              TTFF29.setBounds(45, 135, 80, TTFF29.getPreferredSize().height);

              //---- TTFS29 ----
              TTFS29.setName("TTFS29");
              panel5.add(TTFS29);
              TTFS29.setBounds(130, 135, 80, TTFS29.getPreferredSize().height);

              //---- TTFF30 ----
              TTFF30.setName("TTFF30");
              panel5.add(TTFF30);
              TTFF30.setBounds(45, 160, 80, TTFF30.getPreferredSize().height);

              //---- TTFS30 ----
              TTFS30.setName("TTFS30");
              panel5.add(TTFS30);
              TTFS30.setBounds(130, 160, 80, TTFS30.getPreferredSize().height);

              //---- TTFF31 ----
              TTFF31.setName("TTFF31");
              panel5.add(TTFF31);
              TTFF31.setBounds(45, 185, 80, TTFF31.getPreferredSize().height);

              //---- TTFS31 ----
              TTFS31.setName("TTFS31");
              panel5.add(TTFS31);
              TTFS31.setBounds(130, 185, 80, TTFS31.getPreferredSize().height);

              //---- TTFF32 ----
              TTFF32.setName("TTFF32");
              panel5.add(TTFF32);
              TTFF32.setBounds(45, 210, 80, TTFF32.getPreferredSize().height);

              //---- TTFS32 ----
              TTFS32.setName("TTFS32");
              panel5.add(TTFS32);
              TTFS32.setBounds(130, 210, 80, TTFS32.getPreferredSize().height);

              //---- TTFF33 ----
              TTFF33.setName("TTFF33");
              panel5.add(TTFF33);
              TTFF33.setBounds(45, 235, 80, TTFF33.getPreferredSize().height);

              //---- TTFS33 ----
              TTFS33.setName("TTFS33");
              panel5.add(TTFS33);
              TTFS33.setBounds(130, 235, 80, TTFS33.getPreferredSize().height);

              //---- TTFF34 ----
              TTFF34.setName("TTFF34");
              panel5.add(TTFF34);
              TTFF34.setBounds(45, 260, 80, TTFF34.getPreferredSize().height);

              //---- TTFS34 ----
              TTFS34.setName("TTFS34");
              panel5.add(TTFS34);
              TTFS34.setBounds(130, 260, 80, TTFS34.getPreferredSize().height);

              //---- TTFF35 ----
              TTFF35.setName("TTFF35");
              panel5.add(TTFF35);
              TTFF35.setBounds(45, 285, 80, TTFF35.getPreferredSize().height);

              //---- TTFS35 ----
              TTFS35.setName("TTFS35");
              panel5.add(TTFS35);
              TTFS35.setBounds(130, 285, 80, TTFS35.getPreferredSize().height);

              //---- TTFF36 ----
              TTFF36.setName("TTFF36");
              panel5.add(TTFF36);
              TTFF36.setBounds(45, 310, 80, TTFF36.getPreferredSize().height);

              //---- TTFS36 ----
              TTFS36.setName("TTFS36");
              panel5.add(TTFS36);
              TTFS36.setBounds(130, 310, 80, TTFS36.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel5);
            panel5.setBounds(604, 55, 240, 350);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private JPanel p_titre;
  private JLabel titre;
  private JLabel fond_titre;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel label1;
  private XRiTextField INDETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel label2;
  private XRiTextField INDMEX;
  private JLabel label3;
  private XRiTextField INDCTR;
  private JLabel label4;
  private XRiTextField INDZON;
  private RiZoneSortie ZGLIB;
  private RiZoneSortie EXLIB;
  private RiZoneSortie TRLIB;
  private JPanel panel3;
  private JLabel label5;
  private XRiTextField TRMTF;
  private JLabel label6;
  private XRiTextField TRART;
  private JLabel label7;
  private XRiTextField TRBAS;
  private JPanel panel1;
  private JLabel label9;
  private JLabel label10;
  private XRiTextField TTFF1;
  private XRiTextField TTFS1;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JLabel label23;
  private JLabel label24;
  private JLabel label25;
  private JLabel label26;
  private JLabel label27;
  private JLabel label28;
  private JLabel label29;
  private XRiTextField TTFF2;
  private XRiTextField TTFS2;
  private XRiTextField TTFF3;
  private XRiTextField TTFS3;
  private XRiTextField TTFF4;
  private XRiTextField TTFS4;
  private XRiTextField TTFF5;
  private XRiTextField TTFS5;
  private XRiTextField TTFF6;
  private XRiTextField TTFS6;
  private XRiTextField TTFF7;
  private XRiTextField TTFS7;
  private XRiTextField TTFF8;
  private XRiTextField TTFS8;
  private XRiTextField TTFF9;
  private XRiTextField TTFS9;
  private XRiTextField TTFF10;
  private XRiTextField TTFS10;
  private XRiTextField TTFF11;
  private XRiTextField TTFS11;
  private XRiTextField TTFF12;
  private XRiTextField TTFS12;
  private JPanel panel4;
  private JLabel label11;
  private JLabel label12;
  private XRiTextField TTFF13;
  private XRiTextField TTFS13;
  private JLabel label30;
  private JLabel label31;
  private JLabel label32;
  private JLabel label33;
  private JLabel label34;
  private JLabel label35;
  private JLabel label36;
  private JLabel label37;
  private JLabel label38;
  private JLabel label39;
  private JLabel label40;
  private JLabel label41;
  private XRiTextField TTFF14;
  private XRiTextField TTFS14;
  private XRiTextField TTFF15;
  private XRiTextField TTFS15;
  private XRiTextField TTFF16;
  private XRiTextField TTFS16;
  private XRiTextField TTFF17;
  private XRiTextField TTFS17;
  private XRiTextField TTFF18;
  private XRiTextField TTFS18;
  private XRiTextField TTFF19;
  private XRiTextField TTFS19;
  private XRiTextField TTFF20;
  private XRiTextField TTFS20;
  private XRiTextField TTFF21;
  private XRiTextField TTFS21;
  private XRiTextField TTFF22;
  private XRiTextField TTFS22;
  private XRiTextField TTFF23;
  private XRiTextField TTFS23;
  private XRiTextField TTFF24;
  private XRiTextField TTFS24;
  private JPanel panel5;
  private JLabel label13;
  private JLabel label14;
  private XRiTextField TTFF25;
  private XRiTextField TTFS25;
  private JLabel label42;
  private JLabel label43;
  private JLabel label44;
  private JLabel label45;
  private JLabel label46;
  private JLabel label47;
  private JLabel label48;
  private JLabel label49;
  private JLabel label50;
  private JLabel label51;
  private JLabel label52;
  private JLabel label53;
  private XRiTextField TTFF26;
  private XRiTextField TTFS26;
  private XRiTextField TTFF27;
  private XRiTextField TTFS27;
  private XRiTextField TTFF28;
  private XRiTextField TTFS28;
  private XRiTextField TTFF29;
  private XRiTextField TTFS29;
  private XRiTextField TTFF30;
  private XRiTextField TTFS30;
  private XRiTextField TTFF31;
  private XRiTextField TTFS31;
  private XRiTextField TTFF32;
  private XRiTextField TTFS32;
  private XRiTextField TTFF33;
  private XRiTextField TTFS33;
  private XRiTextField TTFF34;
  private XRiTextField TTFS34;
  private XRiTextField TTFF35;
  private XRiTextField TTFS35;
  private XRiTextField TTFF36;
  private XRiTextField TTFS36;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
