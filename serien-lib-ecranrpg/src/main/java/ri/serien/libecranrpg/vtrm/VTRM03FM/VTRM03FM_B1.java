
package ri.serien.libecranrpg.vtrm.VTRM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VTRM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VTRM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ZGLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZGLIB@")).trim());
    EXLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIB@")).trim());
    TRLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_titre = new JPanel();
    titre = new JLabel();
    fond_titre = new JLabel();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    label1 = new JLabel();
    INDETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    label2 = new JLabel();
    INDMEX = new XRiTextField();
    label3 = new JLabel();
    INDCTR = new XRiTextField();
    label4 = new JLabel();
    INDZON = new XRiTextField();
    ZGLIB = new RiZoneSortie();
    EXLIB = new RiZoneSortie();
    TRLIB = new RiZoneSortie();
    panel3 = new JPanel();
    label5 = new JLabel();
    TRMTF = new XRiTextField();
    label6 = new JLabel();
    TRART = new XRiTextField();
    label7 = new JLabel();
    TRBAS = new XRiTextField();
    panel1 = new JPanel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    TRQT1 = new XRiTextField();
    TRQT2 = new XRiTextField();
    TRQT3 = new XRiTextField();
    TRQT4 = new XRiTextField();
    TRQT5 = new XRiTextField();
    TRQT6 = new XRiTextField();
    TRQT7 = new XRiTextField();
    TRQT8 = new XRiTextField();
    TRQT9 = new XRiTextField();
    TRQT10 = new XRiTextField();
    TRQT11 = new XRiTextField();
    TRQT12 = new XRiTextField();
    TRMF1 = new XRiTextField();
    TRMT1 = new XRiTextField();
    TRBS1 = new XRiTextField();
    TRAR1 = new XRiTextField();
    TRMF2 = new XRiTextField();
    TRMT2 = new XRiTextField();
    TRBS2 = new XRiTextField();
    TRAR2 = new XRiTextField();
    TRMF3 = new XRiTextField();
    TRMT3 = new XRiTextField();
    TRBS3 = new XRiTextField();
    TRAR3 = new XRiTextField();
    TRMF4 = new XRiTextField();
    TRMT4 = new XRiTextField();
    TRBS4 = new XRiTextField();
    TRAR4 = new XRiTextField();
    TRMF5 = new XRiTextField();
    TRMT5 = new XRiTextField();
    TRBS5 = new XRiTextField();
    TRAR5 = new XRiTextField();
    TRMF6 = new XRiTextField();
    TRMT6 = new XRiTextField();
    TRBS6 = new XRiTextField();
    TRAR6 = new XRiTextField();
    TRMF7 = new XRiTextField();
    TRMT7 = new XRiTextField();
    TRBS7 = new XRiTextField();
    TRAR7 = new XRiTextField();
    TRMF8 = new XRiTextField();
    TRMT8 = new XRiTextField();
    TRBS8 = new XRiTextField();
    TRAR8 = new XRiTextField();
    TRMF9 = new XRiTextField();
    TRMT9 = new XRiTextField();
    TRBS9 = new XRiTextField();
    TRAR9 = new XRiTextField();
    TRMF10 = new XRiTextField();
    TRMT10 = new XRiTextField();
    TRBS10 = new XRiTextField();
    TRAR10 = new XRiTextField();
    TRMF11 = new XRiTextField();
    TRMT11 = new XRiTextField();
    TRBS11 = new XRiTextField();
    TRAR11 = new XRiTextField();
    TRMF12 = new XRiTextField();
    TRMT12 = new XRiTextField();
    TRBS12 = new XRiTextField();
    TRAR12 = new XRiTextField();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    label24 = new JLabel();
    label25 = new JLabel();
    label26 = new JLabel();
    label27 = new JLabel();
    label28 = new JLabel();
    label29 = new JLabel();
    panel4 = new JPanel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    TRQT13 = new XRiTextField();
    TRQT14 = new XRiTextField();
    TRQT15 = new XRiTextField();
    TRQT16 = new XRiTextField();
    TRQT17 = new XRiTextField();
    TRQT18 = new XRiTextField();
    TRQT19 = new XRiTextField();
    TRQT20 = new XRiTextField();
    TRQT21 = new XRiTextField();
    TRQT22 = new XRiTextField();
    TRQT23 = new XRiTextField();
    TRQT24 = new XRiTextField();
    TRMF13 = new XRiTextField();
    TRMT13 = new XRiTextField();
    TRBS13 = new XRiTextField();
    TRAR13 = new XRiTextField();
    TRMF14 = new XRiTextField();
    TRMT14 = new XRiTextField();
    TRBS14 = new XRiTextField();
    TRAR14 = new XRiTextField();
    TRMF15 = new XRiTextField();
    TRMT15 = new XRiTextField();
    TRBS15 = new XRiTextField();
    TRAR15 = new XRiTextField();
    TRMF16 = new XRiTextField();
    TRMT16 = new XRiTextField();
    TRBS16 = new XRiTextField();
    TRAR16 = new XRiTextField();
    TRMF17 = new XRiTextField();
    TRMT17 = new XRiTextField();
    TRBS17 = new XRiTextField();
    TRAR17 = new XRiTextField();
    TRMF18 = new XRiTextField();
    TRMT18 = new XRiTextField();
    TRBS18 = new XRiTextField();
    TRAR18 = new XRiTextField();
    TRMF19 = new XRiTextField();
    TRMT19 = new XRiTextField();
    TRBS19 = new XRiTextField();
    TRAR19 = new XRiTextField();
    TRMF20 = new XRiTextField();
    TRMT20 = new XRiTextField();
    TRBS20 = new XRiTextField();
    TRAR20 = new XRiTextField();
    TRMF21 = new XRiTextField();
    TRMT21 = new XRiTextField();
    TRBS21 = new XRiTextField();
    TRAR21 = new XRiTextField();
    TRMF22 = new XRiTextField();
    TRMT22 = new XRiTextField();
    TRBS22 = new XRiTextField();
    TRAR22 = new XRiTextField();
    TRMF23 = new XRiTextField();
    TRMT23 = new XRiTextField();
    TRBS23 = new XRiTextField();
    TRAR23 = new XRiTextField();
    TRMF24 = new XRiTextField();
    TRMT24 = new XRiTextField();
    TRBS24 = new XRiTextField();
    TRAR24 = new XRiTextField();
    label30 = new JLabel();
    label31 = new JLabel();
    label32 = new JLabel();
    label33 = new JLabel();
    label34 = new JLabel();
    label35 = new JLabel();
    label36 = new JLabel();
    label37 = new JLabel();
    label38 = new JLabel();
    label39 = new JLabel();
    label40 = new JLabel();
    label41 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);

          //---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);

        //======== p_titre ========
        {
          p_titre.setBackground(new Color(238, 239, 241));
          p_titre.setPreferredSize(new Dimension(930, 0));
          p_titre.setMinimumSize(new Dimension(930, 0));
          p_titre.setName("p_titre");
          p_titre.setLayout(null);

          //---- titre ----
          titre.setBackground(new Color(198, 198, 200));
          titre.setText("Grille transporteur 1");
          titre.setHorizontalAlignment(SwingConstants.LEFT);
          titre.setFont(titre.getFont().deriveFont(titre.getFont().getStyle() | Font.BOLD, titre.getFont().getSize() + 6f));
          titre.setForeground(Color.darkGray);
          titre.setName("titre");
          p_titre.add(titre);
          titre.setBounds(10, 0, 600, 55);

          //---- fond_titre ----
          fond_titre.setFont(new Font("Arial", Font.BOLD, 18));
          fond_titre.setForeground(Color.white);
          fond_titre.setName("fond_titre");
          p_titre.add(fond_titre);
          fond_titre.setBounds(0, 0, 900, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_titre.getComponentCount(); i++) {
              Rectangle bounds = p_titre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_titre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_titre.setMinimumSize(preferredSize);
            p_titre.setPreferredSize(preferredSize);
          }
        }
        p_presentation.add(p_titre, BorderLayout.CENTER);
      }
      p_nord.add(p_presentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- label1 ----
          label1.setText("Etablissement");
          label1.setName("label1");
          p_tete_gauche.add(label1);
          label1.setBounds(5, 5, 95, 20);

          //---- INDETB ----
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Bloc notes");
              riSousMenu_bt6.setToolTipText("Bloc notes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label2 ----
            label2.setText("Mode d'exp\u00e9dition");
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(25, 14, 160, 20);

            //---- INDMEX ----
            INDMEX.setName("INDMEX");
            panel2.add(INDMEX);
            INDMEX.setBounds(190, 10, 34, INDMEX.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Code transporteur");
            label3.setName("label3");
            panel2.add(label3);
            label3.setBounds(25, 46, 160, 20);

            //---- INDCTR ----
            INDCTR.setName("INDCTR");
            panel2.add(INDCTR);
            INDCTR.setBounds(190, 42, 34, INDCTR.getPreferredSize().height);

            //---- label4 ----
            label4.setText("Zone g\u00e9ographique");
            label4.setName("label4");
            panel2.add(label4);
            label4.setBounds(25, 78, 160, 20);

            //---- INDZON ----
            INDZON.setName("INDZON");
            panel2.add(INDZON);
            INDZON.setBounds(190, 74, 60, INDZON.getPreferredSize().height);

            //---- ZGLIB ----
            ZGLIB.setText("@ZGLIB@");
            ZGLIB.setName("ZGLIB");
            panel2.add(ZGLIB);
            ZGLIB.setBounds(275, 76, 310, ZGLIB.getPreferredSize().height);

            //---- EXLIB ----
            EXLIB.setText("@EXLIB@");
            EXLIB.setName("EXLIB");
            panel2.add(EXLIB);
            EXLIB.setBounds(275, 12, 310, EXLIB.getPreferredSize().height);

            //---- TRLIB ----
            TRLIB.setText("@TRLIB@");
            TRLIB.setName("TRLIB");
            panel2.add(TRLIB);
            TRLIB.setBounds(275, 44, 310, TRLIB.getPreferredSize().height);
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label5 ----
            label5.setText("Montant fixe");
            label5.setName("label5");
            panel3.add(label5);
            label5.setBounds(30, 19, 75, 20);

            //---- TRMTF ----
            TRMTF.setName("TRMTF");
            panel3.add(TRMTF);
            TRMTF.setBounds(125, 15, 80, TRMTF.getPreferredSize().height);

            //---- label6 ----
            label6.setText("Code article associ\u00e9");
            label6.setName("label6");
            panel3.add(label6);
            label6.setBounds(275, 19, 140, 20);

            //---- TRART ----
            TRART.setName("TRART");
            panel3.add(TRART);
            TRART.setBounds(422, 15, 110, TRART.getPreferredSize().height);

            //---- label7 ----
            label7.setText("Base");
            label7.setName("label7");
            panel3.add(label7);
            label7.setBounds(635, 19, 75, 20);

            //---- TRBAS ----
            TRBAS.setName("TRBAS");
            panel3.add(TRBAS);
            TRBAS.setBounds(749, 15, 80, TRBAS.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new EtchedBorder());
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- label8 ----
              label8.setText("Poids max.");
              label8.setHorizontalAlignment(SwingConstants.CENTER);
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
              label8.setName("label8");
              panel1.add(label8);
              label8.setBounds(30, 10, 70, 20);

              //---- label9 ----
              label9.setText("Forfait");
              label9.setHorizontalAlignment(SwingConstants.CENTER);
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
              label9.setName("label9");
              panel1.add(label9);
              label9.setBounds(100, 10, 80, 20);

              //---- label10 ----
              label10.setText("Montant unitaire");
              label10.setHorizontalAlignment(SwingConstants.CENTER);
              label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
              label10.setName("label10");
              panel1.add(label10);
              label10.setBounds(175, 10, 100, 20);

              //---- label11 ----
              label11.setText("Unc.");
              label11.setHorizontalAlignment(SwingConstants.CENTER);
              label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
              label11.setName("label11");
              panel1.add(label11);
              label11.setBounds(270, 10, 50, 20);

              //---- label12 ----
              label12.setText("Arr.");
              label12.setHorizontalAlignment(SwingConstants.CENTER);
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
              label12.setName("label12");
              panel1.add(label12);
              label12.setBounds(325, 10, 50, 20);

              //---- TRQT1 ----
              TRQT1.setName("TRQT1");
              panel1.add(TRQT1);
              TRQT1.setBounds(35, 35, 60, TRQT1.getPreferredSize().height);

              //---- TRQT2 ----
              TRQT2.setName("TRQT2");
              panel1.add(TRQT2);
              TRQT2.setBounds(35, 60, 60, TRQT2.getPreferredSize().height);

              //---- TRQT3 ----
              TRQT3.setName("TRQT3");
              panel1.add(TRQT3);
              TRQT3.setBounds(35, 85, 60, TRQT3.getPreferredSize().height);

              //---- TRQT4 ----
              TRQT4.setName("TRQT4");
              panel1.add(TRQT4);
              TRQT4.setBounds(35, 110, 60, TRQT4.getPreferredSize().height);

              //---- TRQT5 ----
              TRQT5.setName("TRQT5");
              panel1.add(TRQT5);
              TRQT5.setBounds(35, 135, 60, TRQT5.getPreferredSize().height);

              //---- TRQT6 ----
              TRQT6.setName("TRQT6");
              panel1.add(TRQT6);
              TRQT6.setBounds(35, 160, 60, TRQT6.getPreferredSize().height);

              //---- TRQT7 ----
              TRQT7.setName("TRQT7");
              panel1.add(TRQT7);
              TRQT7.setBounds(35, 185, 60, TRQT7.getPreferredSize().height);

              //---- TRQT8 ----
              TRQT8.setName("TRQT8");
              panel1.add(TRQT8);
              TRQT8.setBounds(35, 210, 60, TRQT8.getPreferredSize().height);

              //---- TRQT9 ----
              TRQT9.setName("TRQT9");
              panel1.add(TRQT9);
              TRQT9.setBounds(35, 235, 60, TRQT9.getPreferredSize().height);

              //---- TRQT10 ----
              TRQT10.setName("TRQT10");
              panel1.add(TRQT10);
              TRQT10.setBounds(35, 260, 60, TRQT10.getPreferredSize().height);

              //---- TRQT11 ----
              TRQT11.setName("TRQT11");
              panel1.add(TRQT11);
              TRQT11.setBounds(35, 285, 60, TRQT11.getPreferredSize().height);

              //---- TRQT12 ----
              TRQT12.setName("TRQT12");
              panel1.add(TRQT12);
              TRQT12.setBounds(35, 310, 60, TRQT12.getPreferredSize().height);

              //---- TRMF1 ----
              TRMF1.setName("TRMF1");
              panel1.add(TRMF1);
              TRMF1.setBounds(100, 35, 80, TRMF1.getPreferredSize().height);

              //---- TRMT1 ----
              TRMT1.setName("TRMT1");
              panel1.add(TRMT1);
              TRMT1.setBounds(185, 35, 80, TRMT1.getPreferredSize().height);

              //---- TRBS1 ----
              TRBS1.setName("TRBS1");
              panel1.add(TRBS1);
              TRBS1.setBounds(270, 35, 50, TRBS1.getPreferredSize().height);

              //---- TRAR1 ----
              TRAR1.setName("TRAR1");
              panel1.add(TRAR1);
              TRAR1.setBounds(325, 35, 50, TRAR1.getPreferredSize().height);

              //---- TRMF2 ----
              TRMF2.setName("TRMF2");
              panel1.add(TRMF2);
              TRMF2.setBounds(100, 60, 80, TRMF2.getPreferredSize().height);

              //---- TRMT2 ----
              TRMT2.setName("TRMT2");
              panel1.add(TRMT2);
              TRMT2.setBounds(185, 60, 80, TRMT2.getPreferredSize().height);

              //---- TRBS2 ----
              TRBS2.setName("TRBS2");
              panel1.add(TRBS2);
              TRBS2.setBounds(270, 60, 50, TRBS2.getPreferredSize().height);

              //---- TRAR2 ----
              TRAR2.setName("TRAR2");
              panel1.add(TRAR2);
              TRAR2.setBounds(325, 60, 50, TRAR2.getPreferredSize().height);

              //---- TRMF3 ----
              TRMF3.setName("TRMF3");
              panel1.add(TRMF3);
              TRMF3.setBounds(100, 85, 80, TRMF3.getPreferredSize().height);

              //---- TRMT3 ----
              TRMT3.setName("TRMT3");
              panel1.add(TRMT3);
              TRMT3.setBounds(185, 85, 80, TRMT3.getPreferredSize().height);

              //---- TRBS3 ----
              TRBS3.setName("TRBS3");
              panel1.add(TRBS3);
              TRBS3.setBounds(270, 85, 50, TRBS3.getPreferredSize().height);

              //---- TRAR3 ----
              TRAR3.setName("TRAR3");
              panel1.add(TRAR3);
              TRAR3.setBounds(325, 85, 50, TRAR3.getPreferredSize().height);

              //---- TRMF4 ----
              TRMF4.setName("TRMF4");
              panel1.add(TRMF4);
              TRMF4.setBounds(100, 110, 80, 28);

              //---- TRMT4 ----
              TRMT4.setName("TRMT4");
              panel1.add(TRMT4);
              TRMT4.setBounds(185, 110, 80, 28);

              //---- TRBS4 ----
              TRBS4.setName("TRBS4");
              panel1.add(TRBS4);
              TRBS4.setBounds(270, 110, 50, 28);

              //---- TRAR4 ----
              TRAR4.setName("TRAR4");
              panel1.add(TRAR4);
              TRAR4.setBounds(325, 110, 50, 28);

              //---- TRMF5 ----
              TRMF5.setName("TRMF5");
              panel1.add(TRMF5);
              TRMF5.setBounds(100, 135, 80, TRMF5.getPreferredSize().height);

              //---- TRMT5 ----
              TRMT5.setName("TRMT5");
              panel1.add(TRMT5);
              TRMT5.setBounds(185, 135, 80, TRMT5.getPreferredSize().height);

              //---- TRBS5 ----
              TRBS5.setName("TRBS5");
              panel1.add(TRBS5);
              TRBS5.setBounds(270, 135, 50, TRBS5.getPreferredSize().height);

              //---- TRAR5 ----
              TRAR5.setName("TRAR5");
              panel1.add(TRAR5);
              TRAR5.setBounds(325, 135, 50, TRAR5.getPreferredSize().height);

              //---- TRMF6 ----
              TRMF6.setName("TRMF6");
              panel1.add(TRMF6);
              TRMF6.setBounds(100, 160, 80, TRMF6.getPreferredSize().height);

              //---- TRMT6 ----
              TRMT6.setName("TRMT6");
              panel1.add(TRMT6);
              TRMT6.setBounds(185, 160, 80, TRMT6.getPreferredSize().height);

              //---- TRBS6 ----
              TRBS6.setName("TRBS6");
              panel1.add(TRBS6);
              TRBS6.setBounds(270, 160, 50, TRBS6.getPreferredSize().height);

              //---- TRAR6 ----
              TRAR6.setName("TRAR6");
              panel1.add(TRAR6);
              TRAR6.setBounds(325, 160, 50, TRAR6.getPreferredSize().height);

              //---- TRMF7 ----
              TRMF7.setName("TRMF7");
              panel1.add(TRMF7);
              TRMF7.setBounds(100, 185, 80, TRMF7.getPreferredSize().height);

              //---- TRMT7 ----
              TRMT7.setName("TRMT7");
              panel1.add(TRMT7);
              TRMT7.setBounds(185, 185, 80, TRMT7.getPreferredSize().height);

              //---- TRBS7 ----
              TRBS7.setName("TRBS7");
              panel1.add(TRBS7);
              TRBS7.setBounds(270, 185, 50, TRBS7.getPreferredSize().height);

              //---- TRAR7 ----
              TRAR7.setName("TRAR7");
              panel1.add(TRAR7);
              TRAR7.setBounds(325, 185, 50, TRAR7.getPreferredSize().height);

              //---- TRMF8 ----
              TRMF8.setName("TRMF8");
              panel1.add(TRMF8);
              TRMF8.setBounds(100, 210, 80, TRMF8.getPreferredSize().height);

              //---- TRMT8 ----
              TRMT8.setName("TRMT8");
              panel1.add(TRMT8);
              TRMT8.setBounds(185, 210, 80, TRMT8.getPreferredSize().height);

              //---- TRBS8 ----
              TRBS8.setName("TRBS8");
              panel1.add(TRBS8);
              TRBS8.setBounds(270, 210, 50, TRBS8.getPreferredSize().height);

              //---- TRAR8 ----
              TRAR8.setName("TRAR8");
              panel1.add(TRAR8);
              TRAR8.setBounds(325, 210, 50, TRAR8.getPreferredSize().height);

              //---- TRMF9 ----
              TRMF9.setName("TRMF9");
              panel1.add(TRMF9);
              TRMF9.setBounds(100, 235, 80, TRMF9.getPreferredSize().height);

              //---- TRMT9 ----
              TRMT9.setName("TRMT9");
              panel1.add(TRMT9);
              TRMT9.setBounds(185, 235, 80, TRMT9.getPreferredSize().height);

              //---- TRBS9 ----
              TRBS9.setName("TRBS9");
              panel1.add(TRBS9);
              TRBS9.setBounds(270, 235, 50, TRBS9.getPreferredSize().height);

              //---- TRAR9 ----
              TRAR9.setName("TRAR9");
              panel1.add(TRAR9);
              TRAR9.setBounds(325, 235, 50, TRAR9.getPreferredSize().height);

              //---- TRMF10 ----
              TRMF10.setName("TRMF10");
              panel1.add(TRMF10);
              TRMF10.setBounds(100, 260, 80, TRMF10.getPreferredSize().height);

              //---- TRMT10 ----
              TRMT10.setName("TRMT10");
              panel1.add(TRMT10);
              TRMT10.setBounds(185, 260, 80, TRMT10.getPreferredSize().height);

              //---- TRBS10 ----
              TRBS10.setName("TRBS10");
              panel1.add(TRBS10);
              TRBS10.setBounds(270, 260, 50, TRBS10.getPreferredSize().height);

              //---- TRAR10 ----
              TRAR10.setName("TRAR10");
              panel1.add(TRAR10);
              TRAR10.setBounds(325, 260, 50, TRAR10.getPreferredSize().height);

              //---- TRMF11 ----
              TRMF11.setName("TRMF11");
              panel1.add(TRMF11);
              TRMF11.setBounds(100, 285, 80, TRMF11.getPreferredSize().height);

              //---- TRMT11 ----
              TRMT11.setName("TRMT11");
              panel1.add(TRMT11);
              TRMT11.setBounds(185, 285, 80, TRMT11.getPreferredSize().height);

              //---- TRBS11 ----
              TRBS11.setName("TRBS11");
              panel1.add(TRBS11);
              TRBS11.setBounds(270, 285, 50, TRBS11.getPreferredSize().height);

              //---- TRAR11 ----
              TRAR11.setName("TRAR11");
              panel1.add(TRAR11);
              TRAR11.setBounds(325, 285, 50, TRAR11.getPreferredSize().height);

              //---- TRMF12 ----
              TRMF12.setName("TRMF12");
              panel1.add(TRMF12);
              TRMF12.setBounds(100, 310, 80, TRMF12.getPreferredSize().height);

              //---- TRMT12 ----
              TRMT12.setName("TRMT12");
              panel1.add(TRMT12);
              TRMT12.setBounds(185, 310, 80, TRMT12.getPreferredSize().height);

              //---- TRBS12 ----
              TRBS12.setName("TRBS12");
              panel1.add(TRBS12);
              TRBS12.setBounds(270, 310, 50, TRBS12.getPreferredSize().height);

              //---- TRAR12 ----
              TRAR12.setName("TRAR12");
              panel1.add(TRAR12);
              TRAR12.setBounds(325, 310, 50, TRAR12.getPreferredSize().height);

              //---- label18 ----
              label18.setText("1");
              label18.setHorizontalAlignment(SwingConstants.RIGHT);
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setName("label18");
              panel1.add(label18);
              label18.setBounds(10, 39, 20, 20);

              //---- label19 ----
              label19.setText("2");
              label19.setHorizontalAlignment(SwingConstants.RIGHT);
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setName("label19");
              panel1.add(label19);
              label19.setBounds(10, 64, 20, 20);

              //---- label20 ----
              label20.setText("3");
              label20.setHorizontalAlignment(SwingConstants.RIGHT);
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setName("label20");
              panel1.add(label20);
              label20.setBounds(10, 89, 20, 20);

              //---- label21 ----
              label21.setText("4");
              label21.setHorizontalAlignment(SwingConstants.RIGHT);
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setName("label21");
              panel1.add(label21);
              label21.setBounds(10, 114, 20, 20);

              //---- label22 ----
              label22.setText("5");
              label22.setHorizontalAlignment(SwingConstants.RIGHT);
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setName("label22");
              panel1.add(label22);
              label22.setBounds(10, 139, 20, 20);

              //---- label23 ----
              label23.setText("6");
              label23.setHorizontalAlignment(SwingConstants.RIGHT);
              label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD));
              label23.setName("label23");
              panel1.add(label23);
              label23.setBounds(10, 164, 20, 20);

              //---- label24 ----
              label24.setText("7");
              label24.setHorizontalAlignment(SwingConstants.RIGHT);
              label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD));
              label24.setName("label24");
              panel1.add(label24);
              label24.setBounds(10, 189, 20, 20);

              //---- label25 ----
              label25.setText("8");
              label25.setHorizontalAlignment(SwingConstants.RIGHT);
              label25.setFont(label25.getFont().deriveFont(label25.getFont().getStyle() | Font.BOLD));
              label25.setName("label25");
              panel1.add(label25);
              label25.setBounds(10, 214, 20, 20);

              //---- label26 ----
              label26.setText("9");
              label26.setHorizontalAlignment(SwingConstants.RIGHT);
              label26.setFont(label26.getFont().deriveFont(label26.getFont().getStyle() | Font.BOLD));
              label26.setName("label26");
              panel1.add(label26);
              label26.setBounds(10, 239, 20, 20);

              //---- label27 ----
              label27.setText("10");
              label27.setHorizontalAlignment(SwingConstants.RIGHT);
              label27.setFont(label27.getFont().deriveFont(label27.getFont().getStyle() | Font.BOLD));
              label27.setName("label27");
              panel1.add(label27);
              label27.setBounds(10, 264, 20, 20);

              //---- label28 ----
              label28.setText("11");
              label28.setHorizontalAlignment(SwingConstants.RIGHT);
              label28.setFont(label28.getFont().deriveFont(label28.getFont().getStyle() | Font.BOLD));
              label28.setName("label28");
              panel1.add(label28);
              label28.setBounds(10, 289, 20, 20);

              //---- label29 ----
              label29.setText("12");
              label29.setHorizontalAlignment(SwingConstants.RIGHT);
              label29.setFont(label29.getFont().deriveFont(label29.getFont().getStyle() | Font.BOLD));
              label29.setName("label29");
              panel1.add(label29);
              label29.setBounds(10, 314, 20, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel1);
            panel1.setBounds(20, 55, 395, 350);

            //======== panel4 ========
            {
              panel4.setBorder(new EtchedBorder());
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- label13 ----
              label13.setText("Poids max.");
              label13.setHorizontalAlignment(SwingConstants.CENTER);
              label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
              label13.setName("label13");
              panel4.add(label13);
              label13.setBounds(30, 10, 70, 20);

              //---- label14 ----
              label14.setText("Forfait");
              label14.setHorizontalAlignment(SwingConstants.CENTER);
              label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
              label14.setName("label14");
              panel4.add(label14);
              label14.setBounds(100, 10, 80, 20);

              //---- label15 ----
              label15.setText("Montant unitaire");
              label15.setHorizontalAlignment(SwingConstants.CENTER);
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setName("label15");
              panel4.add(label15);
              label15.setBounds(175, 10, 100, 20);

              //---- label16 ----
              label16.setText("Unc.");
              label16.setHorizontalAlignment(SwingConstants.CENTER);
              label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
              label16.setName("label16");
              panel4.add(label16);
              label16.setBounds(270, 10, 50, 20);

              //---- label17 ----
              label17.setText("Arr.");
              label17.setHorizontalAlignment(SwingConstants.CENTER);
              label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
              label17.setName("label17");
              panel4.add(label17);
              label17.setBounds(325, 10, 50, 20);

              //---- TRQT13 ----
              TRQT13.setName("TRQT13");
              panel4.add(TRQT13);
              TRQT13.setBounds(35, 35, 60, TRQT13.getPreferredSize().height);

              //---- TRQT14 ----
              TRQT14.setName("TRQT14");
              panel4.add(TRQT14);
              TRQT14.setBounds(35, 60, 60, TRQT14.getPreferredSize().height);

              //---- TRQT15 ----
              TRQT15.setName("TRQT15");
              panel4.add(TRQT15);
              TRQT15.setBounds(35, 85, 60, TRQT15.getPreferredSize().height);

              //---- TRQT16 ----
              TRQT16.setName("TRQT16");
              panel4.add(TRQT16);
              TRQT16.setBounds(35, 110, 60, TRQT16.getPreferredSize().height);

              //---- TRQT17 ----
              TRQT17.setName("TRQT17");
              panel4.add(TRQT17);
              TRQT17.setBounds(35, 135, 60, TRQT17.getPreferredSize().height);

              //---- TRQT18 ----
              TRQT18.setName("TRQT18");
              panel4.add(TRQT18);
              TRQT18.setBounds(35, 160, 60, TRQT18.getPreferredSize().height);

              //---- TRQT19 ----
              TRQT19.setName("TRQT19");
              panel4.add(TRQT19);
              TRQT19.setBounds(35, 185, 60, TRQT19.getPreferredSize().height);

              //---- TRQT20 ----
              TRQT20.setName("TRQT20");
              panel4.add(TRQT20);
              TRQT20.setBounds(35, 210, 60, TRQT20.getPreferredSize().height);

              //---- TRQT21 ----
              TRQT21.setName("TRQT21");
              panel4.add(TRQT21);
              TRQT21.setBounds(35, 235, 60, TRQT21.getPreferredSize().height);

              //---- TRQT22 ----
              TRQT22.setName("TRQT22");
              panel4.add(TRQT22);
              TRQT22.setBounds(35, 260, 60, TRQT22.getPreferredSize().height);

              //---- TRQT23 ----
              TRQT23.setName("TRQT23");
              panel4.add(TRQT23);
              TRQT23.setBounds(35, 285, 60, TRQT23.getPreferredSize().height);

              //---- TRQT24 ----
              TRQT24.setName("TRQT24");
              panel4.add(TRQT24);
              TRQT24.setBounds(35, 310, 60, TRQT24.getPreferredSize().height);

              //---- TRMF13 ----
              TRMF13.setName("TRMF13");
              panel4.add(TRMF13);
              TRMF13.setBounds(100, 35, 80, TRMF13.getPreferredSize().height);

              //---- TRMT13 ----
              TRMT13.setName("TRMT13");
              panel4.add(TRMT13);
              TRMT13.setBounds(185, 35, 80, TRMT13.getPreferredSize().height);

              //---- TRBS13 ----
              TRBS13.setName("TRBS13");
              panel4.add(TRBS13);
              TRBS13.setBounds(270, 35, 50, TRBS13.getPreferredSize().height);

              //---- TRAR13 ----
              TRAR13.setName("TRAR13");
              panel4.add(TRAR13);
              TRAR13.setBounds(325, 35, 50, TRAR13.getPreferredSize().height);

              //---- TRMF14 ----
              TRMF14.setName("TRMF14");
              panel4.add(TRMF14);
              TRMF14.setBounds(100, 60, 80, TRMF14.getPreferredSize().height);

              //---- TRMT14 ----
              TRMT14.setName("TRMT14");
              panel4.add(TRMT14);
              TRMT14.setBounds(185, 60, 80, TRMT14.getPreferredSize().height);

              //---- TRBS14 ----
              TRBS14.setName("TRBS14");
              panel4.add(TRBS14);
              TRBS14.setBounds(270, 60, 50, TRBS14.getPreferredSize().height);

              //---- TRAR14 ----
              TRAR14.setName("TRAR14");
              panel4.add(TRAR14);
              TRAR14.setBounds(325, 60, 50, TRAR14.getPreferredSize().height);

              //---- TRMF15 ----
              TRMF15.setName("TRMF15");
              panel4.add(TRMF15);
              TRMF15.setBounds(100, 85, 80, TRMF15.getPreferredSize().height);

              //---- TRMT15 ----
              TRMT15.setName("TRMT15");
              panel4.add(TRMT15);
              TRMT15.setBounds(185, 85, 80, TRMT15.getPreferredSize().height);

              //---- TRBS15 ----
              TRBS15.setName("TRBS15");
              panel4.add(TRBS15);
              TRBS15.setBounds(270, 85, 50, TRBS15.getPreferredSize().height);

              //---- TRAR15 ----
              TRAR15.setName("TRAR15");
              panel4.add(TRAR15);
              TRAR15.setBounds(325, 85, 50, TRAR15.getPreferredSize().height);

              //---- TRMF16 ----
              TRMF16.setName("TRMF16");
              panel4.add(TRMF16);
              TRMF16.setBounds(100, 110, 80, 28);

              //---- TRMT16 ----
              TRMT16.setName("TRMT16");
              panel4.add(TRMT16);
              TRMT16.setBounds(185, 110, 80, 28);

              //---- TRBS16 ----
              TRBS16.setName("TRBS16");
              panel4.add(TRBS16);
              TRBS16.setBounds(270, 110, 50, 28);

              //---- TRAR16 ----
              TRAR16.setName("TRAR16");
              panel4.add(TRAR16);
              TRAR16.setBounds(325, 110, 50, 28);

              //---- TRMF17 ----
              TRMF17.setName("TRMF17");
              panel4.add(TRMF17);
              TRMF17.setBounds(100, 135, 80, TRMF17.getPreferredSize().height);

              //---- TRMT17 ----
              TRMT17.setName("TRMT17");
              panel4.add(TRMT17);
              TRMT17.setBounds(185, 135, 80, TRMT17.getPreferredSize().height);

              //---- TRBS17 ----
              TRBS17.setName("TRBS17");
              panel4.add(TRBS17);
              TRBS17.setBounds(270, 135, 50, TRBS17.getPreferredSize().height);

              //---- TRAR17 ----
              TRAR17.setName("TRAR17");
              panel4.add(TRAR17);
              TRAR17.setBounds(325, 135, 50, TRAR17.getPreferredSize().height);

              //---- TRMF18 ----
              TRMF18.setName("TRMF18");
              panel4.add(TRMF18);
              TRMF18.setBounds(100, 160, 80, TRMF18.getPreferredSize().height);

              //---- TRMT18 ----
              TRMT18.setName("TRMT18");
              panel4.add(TRMT18);
              TRMT18.setBounds(185, 160, 80, TRMT18.getPreferredSize().height);

              //---- TRBS18 ----
              TRBS18.setName("TRBS18");
              panel4.add(TRBS18);
              TRBS18.setBounds(270, 160, 50, TRBS18.getPreferredSize().height);

              //---- TRAR18 ----
              TRAR18.setName("TRAR18");
              panel4.add(TRAR18);
              TRAR18.setBounds(325, 160, 50, TRAR18.getPreferredSize().height);

              //---- TRMF19 ----
              TRMF19.setName("TRMF19");
              panel4.add(TRMF19);
              TRMF19.setBounds(100, 185, 80, TRMF19.getPreferredSize().height);

              //---- TRMT19 ----
              TRMT19.setName("TRMT19");
              panel4.add(TRMT19);
              TRMT19.setBounds(185, 185, 80, TRMT19.getPreferredSize().height);

              //---- TRBS19 ----
              TRBS19.setName("TRBS19");
              panel4.add(TRBS19);
              TRBS19.setBounds(270, 185, 50, TRBS19.getPreferredSize().height);

              //---- TRAR19 ----
              TRAR19.setName("TRAR19");
              panel4.add(TRAR19);
              TRAR19.setBounds(325, 185, 50, TRAR19.getPreferredSize().height);

              //---- TRMF20 ----
              TRMF20.setName("TRMF20");
              panel4.add(TRMF20);
              TRMF20.setBounds(100, 210, 80, TRMF20.getPreferredSize().height);

              //---- TRMT20 ----
              TRMT20.setName("TRMT20");
              panel4.add(TRMT20);
              TRMT20.setBounds(185, 210, 80, TRMT20.getPreferredSize().height);

              //---- TRBS20 ----
              TRBS20.setName("TRBS20");
              panel4.add(TRBS20);
              TRBS20.setBounds(270, 210, 50, TRBS20.getPreferredSize().height);

              //---- TRAR20 ----
              TRAR20.setName("TRAR20");
              panel4.add(TRAR20);
              TRAR20.setBounds(325, 210, 50, TRAR20.getPreferredSize().height);

              //---- TRMF21 ----
              TRMF21.setName("TRMF21");
              panel4.add(TRMF21);
              TRMF21.setBounds(100, 235, 80, TRMF21.getPreferredSize().height);

              //---- TRMT21 ----
              TRMT21.setName("TRMT21");
              panel4.add(TRMT21);
              TRMT21.setBounds(185, 235, 80, TRMT21.getPreferredSize().height);

              //---- TRBS21 ----
              TRBS21.setName("TRBS21");
              panel4.add(TRBS21);
              TRBS21.setBounds(270, 235, 50, TRBS21.getPreferredSize().height);

              //---- TRAR21 ----
              TRAR21.setName("TRAR21");
              panel4.add(TRAR21);
              TRAR21.setBounds(325, 235, 50, TRAR21.getPreferredSize().height);

              //---- TRMF22 ----
              TRMF22.setName("TRMF22");
              panel4.add(TRMF22);
              TRMF22.setBounds(100, 260, 80, TRMF22.getPreferredSize().height);

              //---- TRMT22 ----
              TRMT22.setName("TRMT22");
              panel4.add(TRMT22);
              TRMT22.setBounds(185, 260, 80, TRMT22.getPreferredSize().height);

              //---- TRBS22 ----
              TRBS22.setName("TRBS22");
              panel4.add(TRBS22);
              TRBS22.setBounds(270, 260, 50, TRBS22.getPreferredSize().height);

              //---- TRAR22 ----
              TRAR22.setName("TRAR22");
              panel4.add(TRAR22);
              TRAR22.setBounds(325, 260, 50, TRAR22.getPreferredSize().height);

              //---- TRMF23 ----
              TRMF23.setName("TRMF23");
              panel4.add(TRMF23);
              TRMF23.setBounds(100, 285, 80, TRMF23.getPreferredSize().height);

              //---- TRMT23 ----
              TRMT23.setName("TRMT23");
              panel4.add(TRMT23);
              TRMT23.setBounds(185, 285, 80, TRMT23.getPreferredSize().height);

              //---- TRBS23 ----
              TRBS23.setName("TRBS23");
              panel4.add(TRBS23);
              TRBS23.setBounds(270, 285, 50, TRBS23.getPreferredSize().height);

              //---- TRAR23 ----
              TRAR23.setName("TRAR23");
              panel4.add(TRAR23);
              TRAR23.setBounds(325, 285, 50, TRAR23.getPreferredSize().height);

              //---- TRMF24 ----
              TRMF24.setName("TRMF24");
              panel4.add(TRMF24);
              TRMF24.setBounds(100, 310, 80, TRMF24.getPreferredSize().height);

              //---- TRMT24 ----
              TRMT24.setName("TRMT24");
              panel4.add(TRMT24);
              TRMT24.setBounds(185, 310, 80, TRMT24.getPreferredSize().height);

              //---- TRBS24 ----
              TRBS24.setName("TRBS24");
              panel4.add(TRBS24);
              TRBS24.setBounds(270, 310, 50, TRBS24.getPreferredSize().height);

              //---- TRAR24 ----
              TRAR24.setName("TRAR24");
              panel4.add(TRAR24);
              TRAR24.setBounds(325, 310, 50, TRAR24.getPreferredSize().height);

              //---- label30 ----
              label30.setText("13");
              label30.setHorizontalAlignment(SwingConstants.RIGHT);
              label30.setFont(label30.getFont().deriveFont(label30.getFont().getStyle() | Font.BOLD));
              label30.setName("label30");
              panel4.add(label30);
              label30.setBounds(10, 39, 20, 20);

              //---- label31 ----
              label31.setText("14");
              label31.setHorizontalAlignment(SwingConstants.RIGHT);
              label31.setFont(label31.getFont().deriveFont(label31.getFont().getStyle() | Font.BOLD));
              label31.setName("label31");
              panel4.add(label31);
              label31.setBounds(10, 64, 20, 20);

              //---- label32 ----
              label32.setText("15");
              label32.setHorizontalAlignment(SwingConstants.RIGHT);
              label32.setFont(label32.getFont().deriveFont(label32.getFont().getStyle() | Font.BOLD));
              label32.setName("label32");
              panel4.add(label32);
              label32.setBounds(10, 89, 20, 20);

              //---- label33 ----
              label33.setText("16");
              label33.setHorizontalAlignment(SwingConstants.RIGHT);
              label33.setFont(label33.getFont().deriveFont(label33.getFont().getStyle() | Font.BOLD));
              label33.setName("label33");
              panel4.add(label33);
              label33.setBounds(10, 114, 20, 20);

              //---- label34 ----
              label34.setText("17");
              label34.setHorizontalAlignment(SwingConstants.RIGHT);
              label34.setFont(label34.getFont().deriveFont(label34.getFont().getStyle() | Font.BOLD));
              label34.setName("label34");
              panel4.add(label34);
              label34.setBounds(10, 139, 20, 20);

              //---- label35 ----
              label35.setText("18");
              label35.setHorizontalAlignment(SwingConstants.RIGHT);
              label35.setFont(label35.getFont().deriveFont(label35.getFont().getStyle() | Font.BOLD));
              label35.setName("label35");
              panel4.add(label35);
              label35.setBounds(10, 164, 20, 20);

              //---- label36 ----
              label36.setText("19");
              label36.setHorizontalAlignment(SwingConstants.RIGHT);
              label36.setFont(label36.getFont().deriveFont(label36.getFont().getStyle() | Font.BOLD));
              label36.setName("label36");
              panel4.add(label36);
              label36.setBounds(10, 189, 20, 20);

              //---- label37 ----
              label37.setText("20");
              label37.setHorizontalAlignment(SwingConstants.RIGHT);
              label37.setFont(label37.getFont().deriveFont(label37.getFont().getStyle() | Font.BOLD));
              label37.setName("label37");
              panel4.add(label37);
              label37.setBounds(10, 214, 20, 20);

              //---- label38 ----
              label38.setText("21");
              label38.setHorizontalAlignment(SwingConstants.RIGHT);
              label38.setFont(label38.getFont().deriveFont(label38.getFont().getStyle() | Font.BOLD));
              label38.setName("label38");
              panel4.add(label38);
              label38.setBounds(10, 239, 20, 20);

              //---- label39 ----
              label39.setText("22");
              label39.setHorizontalAlignment(SwingConstants.RIGHT);
              label39.setFont(label39.getFont().deriveFont(label39.getFont().getStyle() | Font.BOLD));
              label39.setName("label39");
              panel4.add(label39);
              label39.setBounds(10, 264, 20, 20);

              //---- label40 ----
              label40.setText("23");
              label40.setHorizontalAlignment(SwingConstants.RIGHT);
              label40.setFont(label40.getFont().deriveFont(label40.getFont().getStyle() | Font.BOLD));
              label40.setName("label40");
              panel4.add(label40);
              label40.setBounds(10, 289, 20, 20);

              //---- label41 ----
              label41.setText("24");
              label41.setHorizontalAlignment(SwingConstants.RIGHT);
              label41.setFont(label41.getFont().deriveFont(label41.getFont().getStyle() | Font.BOLD));
              label41.setName("label41");
              panel4.add(label41);
              label41.setBounds(10, 314, 20, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel4);
            panel4.setBounds(450, 55, 395, 350);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private JPanel p_titre;
  private JLabel titre;
  private JLabel fond_titre;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel label1;
  private XRiTextField INDETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel label2;
  private XRiTextField INDMEX;
  private JLabel label3;
  private XRiTextField INDCTR;
  private JLabel label4;
  private XRiTextField INDZON;
  private RiZoneSortie ZGLIB;
  private RiZoneSortie EXLIB;
  private RiZoneSortie TRLIB;
  private JPanel panel3;
  private JLabel label5;
  private XRiTextField TRMTF;
  private JLabel label6;
  private XRiTextField TRART;
  private JLabel label7;
  private XRiTextField TRBAS;
  private JPanel panel1;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private XRiTextField TRQT1;
  private XRiTextField TRQT2;
  private XRiTextField TRQT3;
  private XRiTextField TRQT4;
  private XRiTextField TRQT5;
  private XRiTextField TRQT6;
  private XRiTextField TRQT7;
  private XRiTextField TRQT8;
  private XRiTextField TRQT9;
  private XRiTextField TRQT10;
  private XRiTextField TRQT11;
  private XRiTextField TRQT12;
  private XRiTextField TRMF1;
  private XRiTextField TRMT1;
  private XRiTextField TRBS1;
  private XRiTextField TRAR1;
  private XRiTextField TRMF2;
  private XRiTextField TRMT2;
  private XRiTextField TRBS2;
  private XRiTextField TRAR2;
  private XRiTextField TRMF3;
  private XRiTextField TRMT3;
  private XRiTextField TRBS3;
  private XRiTextField TRAR3;
  private XRiTextField TRMF4;
  private XRiTextField TRMT4;
  private XRiTextField TRBS4;
  private XRiTextField TRAR4;
  private XRiTextField TRMF5;
  private XRiTextField TRMT5;
  private XRiTextField TRBS5;
  private XRiTextField TRAR5;
  private XRiTextField TRMF6;
  private XRiTextField TRMT6;
  private XRiTextField TRBS6;
  private XRiTextField TRAR6;
  private XRiTextField TRMF7;
  private XRiTextField TRMT7;
  private XRiTextField TRBS7;
  private XRiTextField TRAR7;
  private XRiTextField TRMF8;
  private XRiTextField TRMT8;
  private XRiTextField TRBS8;
  private XRiTextField TRAR8;
  private XRiTextField TRMF9;
  private XRiTextField TRMT9;
  private XRiTextField TRBS9;
  private XRiTextField TRAR9;
  private XRiTextField TRMF10;
  private XRiTextField TRMT10;
  private XRiTextField TRBS10;
  private XRiTextField TRAR10;
  private XRiTextField TRMF11;
  private XRiTextField TRMT11;
  private XRiTextField TRBS11;
  private XRiTextField TRAR11;
  private XRiTextField TRMF12;
  private XRiTextField TRMT12;
  private XRiTextField TRBS12;
  private XRiTextField TRAR12;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JLabel label23;
  private JLabel label24;
  private JLabel label25;
  private JLabel label26;
  private JLabel label27;
  private JLabel label28;
  private JLabel label29;
  private JPanel panel4;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private XRiTextField TRQT13;
  private XRiTextField TRQT14;
  private XRiTextField TRQT15;
  private XRiTextField TRQT16;
  private XRiTextField TRQT17;
  private XRiTextField TRQT18;
  private XRiTextField TRQT19;
  private XRiTextField TRQT20;
  private XRiTextField TRQT21;
  private XRiTextField TRQT22;
  private XRiTextField TRQT23;
  private XRiTextField TRQT24;
  private XRiTextField TRMF13;
  private XRiTextField TRMT13;
  private XRiTextField TRBS13;
  private XRiTextField TRAR13;
  private XRiTextField TRMF14;
  private XRiTextField TRMT14;
  private XRiTextField TRBS14;
  private XRiTextField TRAR14;
  private XRiTextField TRMF15;
  private XRiTextField TRMT15;
  private XRiTextField TRBS15;
  private XRiTextField TRAR15;
  private XRiTextField TRMF16;
  private XRiTextField TRMT16;
  private XRiTextField TRBS16;
  private XRiTextField TRAR16;
  private XRiTextField TRMF17;
  private XRiTextField TRMT17;
  private XRiTextField TRBS17;
  private XRiTextField TRAR17;
  private XRiTextField TRMF18;
  private XRiTextField TRMT18;
  private XRiTextField TRBS18;
  private XRiTextField TRAR18;
  private XRiTextField TRMF19;
  private XRiTextField TRMT19;
  private XRiTextField TRBS19;
  private XRiTextField TRAR19;
  private XRiTextField TRMF20;
  private XRiTextField TRMT20;
  private XRiTextField TRBS20;
  private XRiTextField TRAR20;
  private XRiTextField TRMF21;
  private XRiTextField TRMT21;
  private XRiTextField TRBS21;
  private XRiTextField TRAR21;
  private XRiTextField TRMF22;
  private XRiTextField TRMT22;
  private XRiTextField TRBS22;
  private XRiTextField TRAR22;
  private XRiTextField TRMF23;
  private XRiTextField TRMT23;
  private XRiTextField TRBS23;
  private XRiTextField TRAR23;
  private XRiTextField TRMF24;
  private XRiTextField TRMT24;
  private XRiTextField TRBS24;
  private XRiTextField TRAR24;
  private JLabel label30;
  private JLabel label31;
  private JLabel label32;
  private JLabel label33;
  private JLabel label34;
  private JLabel label35;
  private JLabel label36;
  private JLabel label37;
  private JLabel label38;
  private JLabel label39;
  private JLabel label40;
  private JLabel label41;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
