
package ri.serien.libecranrpg.sexm.SEXM60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXM60FM_B1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private boolean recLance = false;
  
  /**
   * Constructeur.
   */
  public SEXM60FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REC6.setValeursSelection("1", " ");
    REC5.setValeursSelection("1", " ");
    REC4.setValeursSelection("1", " ");
    REC3.setValeursSelection("1", " ");
    REC2.setValeursSelection("1", " ");
    REC1.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    REC1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRT01@")).trim());
    REC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRT02@")).trim());
    REC3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRT03@")).trim());
    REC4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRT04@")).trim());
    REC5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRT05@")).trim());
    REC6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRT06@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_51.setIcon(lexique.chargerImage("images/avert.gif", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // -- Méthodes évènementielles
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    recLance = true;
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void thisComponentRemoved(ContainerEvent e) {
    try {
      if (!recLance) {
        return;
      }
      // Mise à jour de la base de données à la fermeture du panel si le Rec a été lancé
      getSession().rafraichirBaseDeDonnees();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_25 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_17 = new JXTitledSeparator();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    REC1 = new XRiCheckBox();
    REC2 = new XRiCheckBox();
    REC3 = new XRiCheckBox();
    REC4 = new XRiCheckBox();
    REC5 = new XRiCheckBox();
    REC6 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    addContainerListener(new ContainerAdapter() {
      @Override
      public void componentRemoved(ContainerEvent e) {
        thisComponentRemoved(e);
      }
    });
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ---- OBJ_25 ----
          OBJ_25.setText("Le traitement des fichiers va \u00eatre d\u00e9marr\u00e9");
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD, OBJ_25.getFont().getSize() + 2f));
          OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_25.setName("OBJ_25");
          
          // ---- OBJ_51 ----
          OBJ_51.setIcon(null);
          OBJ_51.setName("OBJ_51");
          
          // ---- OBJ_26 ----
          OBJ_26.setText("ATTENTION");
          OBJ_26.setName("OBJ_26");
          
          // ---- OBJ_17 ----
          OBJ_17.setTitle("Groupe(s) de Fichier(s)");
          OBJ_17.setName("OBJ_17");
          
          // ---- OBJ_27 ----
          OBJ_27.setText("Cette op\u00e9ration est d\u00e9licate , et n\u00e9cessite");
          OBJ_27.setName("OBJ_27");
          
          // ---- OBJ_28 ----
          OBJ_28.setText("une sauvegarde pr\u00e9alable (menu EXP option 1)");
          OBJ_28.setName("OBJ_28");
          
          // ---- REC1 ----
          REC1.setText("@TRT01@");
          REC1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REC1.setName("REC1");
          
          // ---- REC2 ----
          REC2.setText("@TRT02@");
          REC2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REC2.setName("REC2");
          
          // ---- REC3 ----
          REC3.setText("@TRT03@");
          REC3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REC3.setName("REC3");
          
          // ---- REC4 ----
          REC4.setText("@TRT04@");
          REC4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REC4.setName("REC4");
          
          // ---- REC5 ----
          REC5.setText("@TRT05@");
          REC5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REC5.setName("REC5");
          
          // ---- REC6 ----
          REC6.setText("@TRT06@");
          REC6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REC6.setName("REC6");
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(59, 59, 59).addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 573,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 429,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 429,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(24, 24, 24).addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 635,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34)
                  .addComponent(REC1, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
                  .addComponent(REC2, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34)
                  .addComponent(REC3, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
                  .addComponent(REC4, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34)
                  .addComponent(REC5, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
                  .addComponent(REC6, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE)));
          p_contenuLayout
              .setVerticalGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(19, 19, 19)
                      .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE).addGap(28, 28, 28)
                      .addGroup(p_contenuLayout.createParallelGroup()
                          .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_26,
                              GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)))
                      .addGap(18, 18, 18).addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE).addGap(6, 6, 6)
                      .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE).addGap(21, 21, 21)
                      .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                      .addGroup(p_contenuLayout.createParallelGroup()
                          .addComponent(REC1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(REC2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(p_contenuLayout.createParallelGroup()
                          .addComponent(REC3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(REC4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(p_contenuLayout.createParallelGroup()
                          .addComponent(REC5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(REC6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_25;
  private JLabel OBJ_51;
  private JLabel OBJ_26;
  private JXTitledSeparator OBJ_17;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private XRiCheckBox REC1;
  private XRiCheckBox REC2;
  private XRiCheckBox REC3;
  private XRiCheckBox REC4;
  private XRiCheckBox REC5;
  private XRiCheckBox REC6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
