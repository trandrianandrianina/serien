
package ri.serien.libecranrpg.vexp.VEXP01AF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP01AF_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "Code Libellé", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 37, };
  
  public VEXP01AF_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(WTP01, WTP01.get_WTP01_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Gestion manuelle du focus
    if (WETB.getText().trim().equals("")) {
      setRequestComponent(WETB);
    }
    else if (WPAR.getText().trim().equals("")) {
      setRequestComponent(WPAR);
    }
    
    
    riBoutonDetailListe1.setVisible(lexique.HostFieldGetData("LD01").contains("?"));
    riBoutonDetailListe2.setVisible(lexique.HostFieldGetData("LD02").contains("?"));
    riBoutonDetailListe3.setVisible(lexique.HostFieldGetData("LD03").contains("?"));
    riBoutonDetailListe4.setVisible(lexique.HostFieldGetData("LD04").contains("?"));
    riBoutonDetailListe5.setVisible(lexique.HostFieldGetData("LD05").contains("?"));
    riBoutonDetailListe6.setVisible(lexique.HostFieldGetData("LD06").contains("?"));
    riBoutonDetailListe7.setVisible(lexique.HostFieldGetData("LD07").contains("?"));
    riBoutonDetailListe8.setVisible(lexique.HostFieldGetData("LD08").contains("?"));
    riBoutonDetailListe9.setVisible(lexique.HostFieldGetData("LD09").contains("?"));
    riBoutonDetailListe10.setVisible(lexique.HostFieldGetData("LD10").contains("?"));
    riBoutonDetailListe11.setVisible(lexique.HostFieldGetData("LD11").contains("?"));
    riBoutonDetailListe12.setVisible(lexique.HostFieldGetData("LD12").contains("?"));
    riBoutonDetailListe13.setVisible(lexique.HostFieldGetData("LD13").contains("?"));
    riBoutonDetailListe14.setVisible(lexique.HostFieldGetData("LD14").contains("?"));
    riBoutonDetailListe15.setVisible(lexique.HostFieldGetData("LD15").contains("?"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // lexique.HostScreenSendKey(this, "ENTER");
    // lexique.validSelection(LIST, _WTP01_Top, "1", "ENTER");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "E", "Enter");
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP05", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP06", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP07", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP08", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP09", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP10", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP11", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP12", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP13", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP14", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP15", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WPAR = new XRiTextField();
    OBJ_44 = new JLabel();
    WETB = new XRiTextField();
    OBJ_42 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    riBoutonDetailListe1 = new SNBoutonDetail();
    riBoutonDetailListe15 = new SNBoutonDetail();
    riBoutonDetailListe2 = new SNBoutonDetail();
    riBoutonDetailListe3 = new SNBoutonDetail();
    riBoutonDetailListe4 = new SNBoutonDetail();
    riBoutonDetailListe5 = new SNBoutonDetail();
    riBoutonDetailListe6 = new SNBoutonDetail();
    riBoutonDetailListe7 = new SNBoutonDetail();
    riBoutonDetailListe8 = new SNBoutonDetail();
    riBoutonDetailListe9 = new SNBoutonDetail();
    riBoutonDetailListe10 = new SNBoutonDetail();
    riBoutonDetailListe11 = new SNBoutonDetail();
    riBoutonDetailListe12 = new SNBoutonDetail();
    riBoutonDetailListe13 = new SNBoutonDetail();
    riBoutonDetailListe14 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_24 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de l'exploitation");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WPAR ----
          WPAR.setComponentPopupMenu(BTD);
          WPAR.setNextFocusableComponent(bouton_valider);
          WPAR.setName("WPAR");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(WPAR, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(WPAR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("S\u00e9lection de param\u00e8tre");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Resultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD2);
              SCROLLPANE_LIST2.setPreferredSize(new Dimension(452, 391));
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setMaximumSize(new Dimension(2147483647, 238));
              WTP01.setPreferredSize(new Dimension(36, 238));
              WTP01.setPreferredScrollableViewportSize(new Dimension(450, 238));
              WTP01.setRequestFocusEnabled(false);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(30, 45, 640, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(675, 45, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(675, 190, 25, 125);

            //---- riBoutonDetailListe1 ----
            riBoutonDetailListe1.setName("riBoutonDetailListe1");
            riBoutonDetailListe1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe1ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe1);
            riBoutonDetailListe1.setBounds(new Rectangle(new Point(10, 70), riBoutonDetailListe1.getPreferredSize()));

            //---- riBoutonDetailListe15 ----
            riBoutonDetailListe15.setName("riBoutonDetailListe15");
            riBoutonDetailListe15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe15ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe15);
            riBoutonDetailListe15.setBounds(new Rectangle(new Point(10, 294), riBoutonDetailListe15.getPreferredSize()));

            //---- riBoutonDetailListe2 ----
            riBoutonDetailListe2.setName("riBoutonDetailListe2");
            riBoutonDetailListe2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe2ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe2);
            riBoutonDetailListe2.setBounds(new Rectangle(new Point(10, 86), riBoutonDetailListe2.getPreferredSize()));

            //---- riBoutonDetailListe3 ----
            riBoutonDetailListe3.setName("riBoutonDetailListe3");
            riBoutonDetailListe3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe3ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe3);
            riBoutonDetailListe3.setBounds(new Rectangle(new Point(10, 102), riBoutonDetailListe3.getPreferredSize()));

            //---- riBoutonDetailListe4 ----
            riBoutonDetailListe4.setName("riBoutonDetailListe4");
            riBoutonDetailListe4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe4ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe4);
            riBoutonDetailListe4.setBounds(new Rectangle(new Point(10, 118), riBoutonDetailListe4.getPreferredSize()));

            //---- riBoutonDetailListe5 ----
            riBoutonDetailListe5.setName("riBoutonDetailListe5");
            riBoutonDetailListe5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe5ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe5);
            riBoutonDetailListe5.setBounds(new Rectangle(new Point(10, 134), riBoutonDetailListe5.getPreferredSize()));

            //---- riBoutonDetailListe6 ----
            riBoutonDetailListe6.setName("riBoutonDetailListe6");
            riBoutonDetailListe6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe6ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe6);
            riBoutonDetailListe6.setBounds(new Rectangle(new Point(10, 150), riBoutonDetailListe6.getPreferredSize()));

            //---- riBoutonDetailListe7 ----
            riBoutonDetailListe7.setName("riBoutonDetailListe7");
            riBoutonDetailListe7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe7ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe7);
            riBoutonDetailListe7.setBounds(new Rectangle(new Point(10, 166), riBoutonDetailListe7.getPreferredSize()));

            //---- riBoutonDetailListe8 ----
            riBoutonDetailListe8.setName("riBoutonDetailListe8");
            riBoutonDetailListe8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe8ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe8);
            riBoutonDetailListe8.setBounds(new Rectangle(new Point(10, 182), riBoutonDetailListe8.getPreferredSize()));

            //---- riBoutonDetailListe9 ----
            riBoutonDetailListe9.setName("riBoutonDetailListe9");
            riBoutonDetailListe9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe9ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe9);
            riBoutonDetailListe9.setBounds(new Rectangle(new Point(10, 198), riBoutonDetailListe9.getPreferredSize()));

            //---- riBoutonDetailListe10 ----
            riBoutonDetailListe10.setName("riBoutonDetailListe10");
            riBoutonDetailListe10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe10ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe10);
            riBoutonDetailListe10.setBounds(new Rectangle(new Point(10, 214), riBoutonDetailListe10.getPreferredSize()));

            //---- riBoutonDetailListe11 ----
            riBoutonDetailListe11.setName("riBoutonDetailListe11");
            riBoutonDetailListe11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe11ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe11);
            riBoutonDetailListe11.setBounds(new Rectangle(new Point(10, 230), riBoutonDetailListe11.getPreferredSize()));

            //---- riBoutonDetailListe12 ----
            riBoutonDetailListe12.setName("riBoutonDetailListe12");
            riBoutonDetailListe12.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe12ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe12);
            riBoutonDetailListe12.setBounds(new Rectangle(new Point(10, 246), riBoutonDetailListe12.getPreferredSize()));

            //---- riBoutonDetailListe13 ----
            riBoutonDetailListe13.setName("riBoutonDetailListe13");
            riBoutonDetailListe13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe13ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe13);
            riBoutonDetailListe13.setBounds(new Rectangle(new Point(10, 262), riBoutonDetailListe13.getPreferredSize()));

            //---- riBoutonDetailListe14 ----
            riBoutonDetailListe14.setName("riBoutonDetailListe14");
            riBoutonDetailListe14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe14ActionPerformed(e);
              }
            });
            panel1.add(riBoutonDetailListe14);
            riBoutonDetailListe14.setBounds(new Rectangle(new Point(10, 278), riBoutonDetailListe14.getPreferredSize()));

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 727, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(48, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_18 ----
      OBJ_18.setText("Edition");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_18);

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField WPAR;
  private JLabel OBJ_44;
  private XRiTextField WETB;
  private JLabel OBJ_42;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNBoutonDetail riBoutonDetailListe1;
  private SNBoutonDetail riBoutonDetailListe15;
  private SNBoutonDetail riBoutonDetailListe2;
  private SNBoutonDetail riBoutonDetailListe3;
  private SNBoutonDetail riBoutonDetailListe4;
  private SNBoutonDetail riBoutonDetailListe5;
  private SNBoutonDetail riBoutonDetailListe6;
  private SNBoutonDetail riBoutonDetailListe7;
  private SNBoutonDetail riBoutonDetailListe8;
  private SNBoutonDetail riBoutonDetailListe9;
  private SNBoutonDetail riBoutonDetailListe10;
  private SNBoutonDetail riBoutonDetailListe11;
  private SNBoutonDetail riBoutonDetailListe12;
  private SNBoutonDetail riBoutonDetailListe13;
  private SNBoutonDetail riBoutonDetailListe14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_24;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
