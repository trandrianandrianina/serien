
package ri.serien.libecranrpg.vexp.VEXP91FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP91FM_PL extends SNPanelEcranRPG implements ioFrame {
  
  private String[] JRM05_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM04_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM03_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM02_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM01_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] DATTRE_Value = { "", "*STR", "*END", };
  
  public VEXP91FM_PL(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    JRM05.setValeurs(JRM05_Value, null);
    JRM04.setValeurs(JRM04_Value, null);
    JRM03.setValeurs(JRM03_Value, null);
    JRM02.setValeurs(JRM02_Value, null);
    JRM01.setValeurs(JRM01_Value, null);
    DATTRE.setValeurs(DATTRE_Value, null);
    JT04.setValeursSelection("X", " ");
    JT01.setValeursSelection("X", " ");
    JT02.setValeursSelection("X", " ");
    JT06.setValeursSelection("X", " ");
    JT03.setValeursSelection("X", " ");
    JT05.setValeursSelection("X", " ");
    JT07.setValeursSelection("X", " ");
    FREQ.setValeurs("J", FREQ_GRP);
    FREQ_S.setValeurs("S");
    FREQ_M.setValeurs("M");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Travail @TRAV@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19", lexique.HostFieldGetData("WERR"));
    
    
    // OBJ_21.setVisible( lexique.isPresent("FREQ"));
    // OBJ_21.setSelected(lexique.HostFieldGetData("FREQ").equalsIgnoreCase("J"));
    // JT04.setVisible( lexique.isPresent("JT04"));
    // JT04.setSelected(lexique.HostFieldGetData("JT04").equalsIgnoreCase("X"));
    // JT01.setVisible( lexique.isPresent("JT01"));
    // JT01.setSelected(lexique.HostFieldGetData("JT01").equalsIgnoreCase("X"));
    // JT02.setVisible( lexique.isPresent("JT02"));
    // JT02.setSelected(lexique.HostFieldGetData("JT02").equalsIgnoreCase("X"));
    HEUTRT.setVisible(lexique.isPresent("HEUTRT"));
    // OBJ_23.setVisible( lexique.isPresent("FREQ"));
    // OBJ_23.setSelected(lexique.HostFieldGetData("FREQ").equalsIgnoreCase("M"));
    // JT06.setVisible( lexique.isPresent("JT06"));
    // JT06.setSelected(lexique.HostFieldGetData("JT06").equalsIgnoreCase("X"));
    // JT03.setVisible( lexique.isPresent("JT03"));
    // JT03.setSelected(lexique.HostFieldGetData("JT03").equalsIgnoreCase("X"));
    // JT05.setVisible( lexique.isPresent("JT05"));
    // JT05.setSelected(lexique.HostFieldGetData("JT05").equalsIgnoreCase("X"));
    // JT07.setVisible( lexique.isPresent("JT07"));
    // JT07.setSelected(lexique.HostFieldGetData("JT07").equalsIgnoreCase("X"));
    // OBJ_22.setVisible( lexique.isPresent("FREQ"));
    // OBJ_22.setSelected(lexique.HostFieldGetData("FREQ").equalsIgnoreCase("S"));
    // JRM05.setVisible( lexique.isPresent("JRM05"));
    // JRM04.setVisible( lexique.isPresent("JRM04"));
    // JRM03.setVisible( lexique.isPresent("JRM03"));
    // JRM02.setVisible( lexique.isPresent("JRM02"));
    // JRM01.setVisible( lexique.isPresent("JRM01"));
    // DATTRT.setVisible( lexique.isPresent("DATTRT"));
    // zone_date.getEditor().setText(lexique.HostFieldGetData("DATTRT"));
    // zone_date.setVisible(DATTRT.getSelectedIndex()==0);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Soumission d'un code au planning des travaux"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OBJ_21.isSelected())
    // lexique.HostFieldPutData("FREQ", 0, "J");
    // if (JT04.isSelected())
    // lexique.HostFieldPutData("JT04", 0, "X");
    // else
    // lexique.HostFieldPutData("JT04", 0, " ");
    // if (JT01.isSelected())
    // lexique.HostFieldPutData("JT01", 0, "X");
    // else
    // lexique.HostFieldPutData("JT01", 0, " ");
    // if (JT02.isSelected())
    // lexique.HostFieldPutData("JT02", 0, "X");
    // else
    // lexique.HostFieldPutData("JT02", 0, " ");
    // if (OBJ_23.isSelected())
    // lexique.HostFieldPutData("FREQ", 0, "M");
    // if (JT06.isSelected())
    // lexique.HostFieldPutData("JT06", 0, "X");
    // else
    // lexique.HostFieldPutData("JT06", 0, " ");
    // if (JT03.isSelected())
    // lexique.HostFieldPutData("JT03", 0, "X");
    // else
    // lexique.HostFieldPutData("JT03", 0, " ");
    // if (JT05.isSelected())
    // lexique.HostFieldPutData("JT05", 0, "X");
    // else
    // lexique.HostFieldPutData("JT05", 0, " ");
    // if (JT07.isSelected())
    // lexique.HostFieldPutData("JT07", 0, "X");
    // else
    // lexique.HostFieldPutData("JT07", 0, " ");
    // if (OBJ_22.isSelected())
    // lexique.HostFieldPutData("FREQ", 0, "S");
    
    // lexique.HostFieldPutData("JRM05", 0, JRM05_Value[JRM05.getSelectedIndex()]);
    // lexique.HostFieldPutData("JRM04", 0, JRM04_Value[JRM04.getSelectedIndex()]);
    // lexique.HostFieldPutData("JRM03", 0, JRM03_Value[JRM03.getSelectedIndex()]);
    // lexique.HostFieldPutData("JRM02", 0, JRM02_Value[JRM02.getSelectedIndex()]);
    // lexique.HostFieldPutData("JRM01", 0, JRM01_Value[JRM01.getSelectedIndex()]);
    
    if (DATTRE.getSelectedIndex() == 0) {
      lexique.HostFieldPutData("DATTRE", 0, DATTRT_CAL.getEditor().getText());
    }
    else {
      lexique.HostFieldPutData("DATTRE", 0, DATTRE_Value[DATTRE.getSelectedIndex()]);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void DATTREActionPerformed(ActionEvent e) {
    DATTRT_CAL.setVisible(DATTRE.getSelectedIndex() == 0);
  }
  
  private void DATTRTActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    FREQ_S = new XRiRadioButton();
    FREQ_M = new XRiRadioButton();
    OBJ_20 = new JLabel();
    FREQ = new XRiRadioButton();
    DATTRE = new XRiComboBox();
    OBJ_41 = new JLabel();
    DATTRT_CAL = new XRiCalendrier();
    OBJ_40 = new JLabel();
    JT07 = new XRiCheckBox();
    JT05 = new XRiCheckBox();
    JT03 = new XRiCheckBox();
    JT06 = new XRiCheckBox();
    JT02 = new XRiCheckBox();
    JT01 = new XRiCheckBox();
    JT04 = new XRiCheckBox();
    OBJ_42 = new JLabel();
    JRM01 = new XRiComboBox();
    JRM02 = new XRiComboBox();
    JRM03 = new XRiComboBox();
    JRM04 = new XRiComboBox();
    JRM05 = new XRiComboBox();
    OBJ_44 = new JLabel();
    HEUTRT = new XRiTextField();
    OBJ_43 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    FREQ_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(785, 330));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(610, 320));
        p_contenu.setName("p_contenu");
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Travail @TRAV@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- FREQ_S ----
          FREQ_S.setText("Hebdomadaire");
          FREQ_S.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FREQ_S.setName("FREQ_S");
          panel1.add(FREQ_S);
          FREQ_S.setBounds(240, 47, 117, 16);
          
          // ---- FREQ_M ----
          FREQ_M.setText("Mensuelle");
          FREQ_M.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FREQ_M.setName("FREQ_M");
          panel1.add(FREQ_M);
          FREQ_M.setBounds(360, 47, 117, 16);
          
          // ---- OBJ_20 ----
          OBJ_20.setText("Fr\u00e9quence");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(25, 45, 80, 20);
          
          // ---- FREQ ----
          FREQ.setText("Jour");
          FREQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FREQ.setName("FREQ");
          panel1.add(FREQ);
          FREQ.setBounds(120, 47, 117, 16);
          
          // ---- DATTRE ----
          DATTRE.setModel(new DefaultComboBoxModel(new String[] { "Saisir une date", "Premier jour du mois", "Dernier jour du mois" }));
          DATTRE.setToolTipText("Saisir sous la forme (JJMMAA)");
          DATTRE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DATTRE.setName("DATTRE");
          DATTRE.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              DATTRTActionPerformed(e);
            }
          });
          panel1.add(DATTRE);
          DATTRE.setBounds(120, 75, 126, DATTRE.getPreferredSize().height);
          
          // ---- OBJ_41 ----
          OBJ_41.setText("Date");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(25, 78, 80, 20);
          
          // ---- DATTRT_CAL ----
          DATTRT_CAL.setName("DATTRT_CAL");
          panel1.add(DATTRT_CAL);
          DATTRT_CAL.setBounds(280, 74, 105, DATTRT_CAL.getPreferredSize().height);
          
          // ---- OBJ_40 ----
          OBJ_40.setText("ou");
          OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_40.setFont(OBJ_40.getFont().deriveFont(OBJ_40.getFont().getStyle() | Font.BOLD));
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(245, 110, 37, 20);
          
          // ---- JT07 ----
          JT07.setText("Dimanche");
          JT07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT07.setName("JT07");
          panel1.add(JT07);
          JT07.setBounds(480, 135, 100, 20);
          
          // ---- JT05 ----
          JT05.setText("Vendredi");
          JT05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT05.setName("JT05");
          panel1.add(JT05);
          JT05.setBounds(360, 135, 105, 20);
          
          // ---- JT03 ----
          JT03.setText("Mercredi");
          JT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT03.setName("JT03");
          panel1.add(JT03);
          JT03.setBounds(230, 135, 95, 20);
          
          // ---- JT06 ----
          JT06.setText("Samedi");
          JT06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT06.setName("JT06");
          panel1.add(JT06);
          JT06.setBounds(360, 165, 105, 20);
          
          // ---- JT02 ----
          JT02.setText("Mardi");
          JT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT02.setName("JT02");
          panel1.add(JT02);
          JT02.setBounds(120, 165, 90, 20);
          
          // ---- JT01 ----
          JT01.setText("Lundi");
          JT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT01.setName("JT01");
          panel1.add(JT01);
          JT01.setBounds(120, 135, 90, 20);
          
          // ---- JT04 ----
          JT04.setText("Jeudi");
          JT04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT04.setName("JT04");
          panel1.add(JT04);
          JT04.setBounds(230, 165, 95, 20);
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Jour(s)");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(25, 135, 80, 20);
          
          // ---- JRM01 ----
          JRM01.setModel(new DefaultComboBoxModel(
              new String[] { "premier", "deuxi\u00e8me", "troisi\u00e8me", "quatri\u00e8me", "cinqui\u00e8me", "dernier" }));
          JRM01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM01.setName("JRM01");
          panel1.add(JRM01);
          JRM01.setBounds(145, 245, 79, JRM01.getPreferredSize().height);
          
          // ---- JRM02 ----
          JRM02.setModel(new DefaultComboBoxModel(
              new String[] { "premier", "deuxi\u00e8me", "troisi\u00e8me", "quatri\u00e8me", "cinqui\u00e8me", "dernier" }));
          JRM02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM02.setName("JRM02");
          panel1.add(JRM02);
          JRM02.setBounds(225, 245, 79, JRM02.getPreferredSize().height);
          
          // ---- JRM03 ----
          JRM03.setModel(new DefaultComboBoxModel(
              new String[] { "premier", "deuxi\u00e8me", "troisi\u00e8me", "quatri\u00e8me", "cinqui\u00e8me", "dernier" }));
          JRM03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM03.setName("JRM03");
          panel1.add(JRM03);
          JRM03.setBounds(305, 245, 79, JRM03.getPreferredSize().height);
          
          // ---- JRM04 ----
          JRM04.setModel(new DefaultComboBoxModel(
              new String[] { "premier", "deuxi\u00e8me", "troisi\u00e8me", "quatri\u00e8me", "cinqui\u00e8me", "dernier" }));
          JRM04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM04.setName("JRM04");
          panel1.add(JRM04);
          JRM04.setBounds(385, 245, 79, JRM04.getPreferredSize().height);
          
          // ---- JRM05 ----
          JRM05.setModel(new DefaultComboBoxModel(
              new String[] { "premier", "deuxi\u00e8me", "troisi\u00e8me", "quatri\u00e8me", "cinqui\u00e8me", "dernier" }));
          JRM05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM05.setName("JRM05");
          panel1.add(JRM05);
          JRM05.setBounds(465, 245, 79, JRM05.getPreferredSize().height);
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Jour relatif du mois");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(25, 248, 125, 20);
          
          // ---- HEUTRT ----
          HEUTRT.setToolTipText("Saisir sous la forme (HHMMSS)");
          HEUTRT.setComponentPopupMenu(BTD);
          HEUTRT.setName("HEUTRT");
          panel1.add(HEUTRT);
          HEUTRT.setBounds(120, 205, 70, HEUTRT.getPreferredSize().height);
          
          // ---- OBJ_43 ----
          OBJ_43.setText("Heure");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(25, 209, 80, 20);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 603, Short.MAX_VALUE).addContainerGap()));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    
    // ---- FREQ_GRP ----
    FREQ_GRP.add(FREQ_S);
    FREQ_GRP.add(FREQ_M);
    FREQ_GRP.add(FREQ);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton FREQ_S;
  private XRiRadioButton FREQ_M;
  private JLabel OBJ_20;
  private XRiRadioButton FREQ;
  private XRiComboBox DATTRE;
  private JLabel OBJ_41;
  private XRiCalendrier DATTRT_CAL;
  private JLabel OBJ_40;
  private XRiCheckBox JT07;
  private XRiCheckBox JT05;
  private XRiCheckBox JT03;
  private XRiCheckBox JT06;
  private XRiCheckBox JT02;
  private XRiCheckBox JT01;
  private XRiCheckBox JT04;
  private JLabel OBJ_42;
  private XRiComboBox JRM01;
  private XRiComboBox JRM02;
  private XRiComboBox JRM03;
  private XRiComboBox JRM04;
  private XRiComboBox JRM05;
  private JLabel OBJ_44;
  private XRiTextField HEUTRT;
  private JLabel OBJ_43;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private ButtonGroup FREQ_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
