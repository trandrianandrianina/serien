
package ri.serien.libecranrpg.vexp.VEXP13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP13FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP13FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    PSEM.setValeursSelection("1", " ");
    PPAM.setValeursSelection("1", " ");
    PGEM.setValeursSelection("1", " ");
    PCGM.setValeursSelection("1", " ");
    PGIM.setValeursSelection("1", " ");
    PGVM.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    BIBRST.setEnabled(lexique.isPresent("BIBRST"));
    BIBSAV.setEnabled(lexique.isPresent("BIBSAV"));
    // PSEM.setVisible( lexique.isPresent("PSEM"));
    // PSEM.setSelected(lexique.HostFieldGetData("PSEM").equalsIgnoreCase("1"));
    // PPAM.setVisible( lexique.isPresent("PPAM"));
    // PPAM.setSelected(lexique.HostFieldGetData("PPAM").equalsIgnoreCase("1"));
    // PGEM.setVisible( lexique.isPresent("PGEM"));
    // PGEM.setSelected(lexique.HostFieldGetData("PGEM").equalsIgnoreCase("1"));
    // PCGM.setVisible( lexique.isPresent("PCGM"));
    // PCGM.setSelected(lexique.HostFieldGetData("PCGM").equalsIgnoreCase("1"));
    // PGIM.setEnabled( lexique.isPresent("PGIM"));
    // PGIM.setSelected(lexique.HostFieldGetData("PGIM").equalsIgnoreCase("1"));
    // PGVM.setEnabled( lexique.isPresent("PGVM"));
    // PGVM.setSelected(lexique.HostFieldGetData("PGVM").equalsIgnoreCase("1"));
    UNITE.setEnabled(lexique.isPresent("UNITE"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (PSEM.isSelected())
    // lexique.HostFieldPutData("PSEM", 0, "1");
    // else
    // lexique.HostFieldPutData("PSEM", 0, " ");
    // if (PPAM.isSelected())
    // lexique.HostFieldPutData("PPAM", 0, "1");
    // else
    // lexique.HostFieldPutData("PPAM", 0, " ");
    // if (PGEM.isSelected())
    // lexique.HostFieldPutData("PGEM", 0, "1");
    // else
    // lexique.HostFieldPutData("PGEM", 0, " ");
    // if (PCGM.isSelected())
    // lexique.HostFieldPutData("PCGM", 0, "1");
    // else
    // lexique.HostFieldPutData("PCGM", 0, " ");
    // if (PGIM.isSelected())
    // lexique.HostFieldPutData("PGIM", 0, "1");
    // else
    // lexique.HostFieldPutData("PGIM", 0, " ");
    // if (PGVM.isSelected())
    // lexique.HostFieldPutData("PGVM", 0, "1");
    // else
    // lexique.HostFieldPutData("PGVM", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_51 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_49 = new JLabel();
    UNITE = new XRiTextField();
    PGVM = new XRiCheckBox();
    PGIM = new XRiCheckBox();
    PCGM = new XRiCheckBox();
    PGEM = new XRiCheckBox();
    PPAM = new XRiCheckBox();
    PSEM = new XRiCheckBox();
    BIBSAV = new XRiTextField();
    BIBRST = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(520, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Saisie d'informations sur la restauration"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_51 ----
            OBJ_51.setText("Code unit\u00e9 optique/bande");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(35, 215, 186, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("Biblioth\u00e8que de restauration");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(35, 180, 177, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("Biblioth\u00e8que de sauvegarde");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(35, 145, 175, 20);

            //---- UNITE ----
            UNITE.setComponentPopupMenu(BTD);
            UNITE.setName("UNITE");
            panel1.add(UNITE);
            UNITE.setBounds(240, 211, 110, UNITE.getPreferredSize().height);

            //---- PGVM ----
            PGVM.setText("PGVM");
            PGVM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PGVM.setName("PGVM");
            panel1.add(PGVM);
            PGVM.setBounds(35, 40, 70, 20);

            //---- PGIM ----
            PGIM.setText("PGIM");
            PGIM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PGIM.setName("PGIM");
            panel1.add(PGIM);
            PGIM.setBounds(240, 40, 70, 20);

            //---- PCGM ----
            PCGM.setText("PCGM");
            PCGM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PCGM.setName("PCGM");
            panel1.add(PCGM);
            PCGM.setBounds(35, 70, 70, 20);

            //---- PGEM ----
            PGEM.setText("PGEM");
            PGEM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PGEM.setName("PGEM");
            panel1.add(PGEM);
            PGEM.setBounds(240, 70, 70, 20);

            //---- PPAM ----
            PPAM.setText("PPAM");
            PPAM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PPAM.setName("PPAM");
            panel1.add(PPAM);
            PPAM.setBounds(35, 100, 70, 20);

            //---- PSEM ----
            PSEM.setText("PSEM");
            PSEM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PSEM.setName("PSEM");
            panel1.add(PSEM);
            PSEM.setBounds(240, 100, 70, 20);

            //---- BIBSAV ----
            BIBSAV.setComponentPopupMenu(BTD);
            BIBSAV.setName("BIBSAV");
            panel1.add(BIBSAV);
            BIBSAV.setBounds(240, 141, 60, BIBSAV.getPreferredSize().height);

            //---- BIBRST ----
            BIBRST.setComponentPopupMenu(BTD);
            BIBRST.setName("BIBRST");
            panel1.add(BIBRST);
            BIBRST.setBounds(240, 176, 60, BIBRST.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 410, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_51;
  private JLabel OBJ_50;
  private JLabel OBJ_49;
  private XRiTextField UNITE;
  private XRiCheckBox PGVM;
  private XRiCheckBox PGIM;
  private XRiCheckBox PCGM;
  private XRiCheckBox PGEM;
  private XRiCheckBox PPAM;
  private XRiCheckBox PSEM;
  private XRiTextField BIBSAV;
  private XRiTextField BIBRST;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
