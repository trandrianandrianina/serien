
package ri.serien.libecranrpg.vexp.VEXP08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP08FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP08FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV2@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV2B@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    UDEV2.setEnabled(lexique.isPresent("UDEV2"));
    UDEV1.setEnabled(lexique.isPresent("UDEV1"));
    DVBD1.setEnabled(lexique.isPresent("DVBD1"));
    DVMON.setEnabled(lexique.isPresent("DVMON"));
    DVBD2.setEnabled(lexique.isPresent("DVBD2"));
    DVTAUX.setEnabled(lexique.isPresent("DVTAUX"));
    ULIB2.setEnabled(lexique.isPresent("ULIB2"));
    ULIB1.setEnabled(lexique.isPresent("ULIB1"));
    DVLIB.setEnabled(lexique.isPresent("DVLIB"));
    DVPAY.setEnabled(lexique.isPresent("DVPAY"));
    DVFAC.setEnabled(lexique.isPresent("DVFAC"));
    label1.setVisible(lexique.isTrue("80"));
    DVDTHX.setVisible(lexique.isTrue("80"));
    DVDTHX.setEditable(false);
    
    if (lexique.isTrue("31")) {
      xTitledPanel4.setVisible(true);
      WBAS2.setVisible(lexique.isTrue("30"));
      riZoneSortie2.setVisible(lexique.isTrue("30"));
      label5.setVisible(lexique.isTrue("30)"));
      label3.setVisible(lexique.isTrue("33"));
      label6.setVisible(lexique.isTrue("33"));
      riZoneSortie3.setVisible(lexique.isTrue("33"));
    }
    else {
      xTitledPanel4.setVisible(false);
    }
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    // DUPLICATION

    
    barre_tete_dup.setVisible(lexique.isTrue("56"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_85_OBJ_85 = new JLabel();
    INDDEV = new XRiTextField();
    p_tete_droite = new JPanel();
    barre_tete_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    IN3DEV = new XRiTextField();
    OBJ_41_OBJ_42 = new JLabel();
    p_tete_droite2 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    DVPAY = new XRiTextField();
    DVLIB = new XRiTextField();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    DVMON = new XRiTextField();
    OBJ_54_OBJ_54 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    DVFAC = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    ULIB1 = new XRiTextField();
    ULIB2 = new XRiTextField();
    DVTAUX = new XRiTextField();
    DVDTHX = new XRiCalendrier();
    DVBD2 = new XRiTextField();
    OBJ_88_OBJ_88 = new JLabel();
    DVBD1 = new XRiTextField();
    UDEV1 = new XRiTextField();
    UDEV2 = new XRiTextField();
    OBJ_61_OBJ_61 = new JLabel();
    label1 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    WBAS2 = new XRiTextField();
    riZoneSortie2 = new RiZoneSortie();
    label4 = new JLabel();
    WCHG2X = new XRiTextField();
    label5 = new JLabel();
    label3 = new JLabel();
    label6 = new JLabel();
    riZoneSortie3 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(1000, 680));
    setPreferredSize(new Dimension(1000, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des devises");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_85_OBJ_85 ----
          OBJ_85_OBJ_85.setText("Devise");
          OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");

          //---- INDDEV ----
          INDDEV.setComponentPopupMenu(null);
          INDDEV.setName("INDDEV");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(OBJ_85_OBJ_85, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(INDDEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(552, 552, 552))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(OBJ_85_OBJ_85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_tete_dup ========
      {
        barre_tete_dup.setMinimumSize(new Dimension(111, 34));
        barre_tete_dup.setPreferredSize(new Dimension(111, 34));
        barre_tete_dup.setName("barre_tete_dup");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- IN3DEV ----
          IN3DEV.setComponentPopupMenu(null);
          IN3DEV.setName("IN3DEV");

          //---- OBJ_41_OBJ_42 ----
          OBJ_41_OBJ_42.setText("Duplication de");
          OBJ_41_OBJ_42.setName("OBJ_41_OBJ_42");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(OBJ_41_OBJ_42, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IN3DEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gauche2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(IN3DEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41_OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete_dup.add(p_tete_gauche2);

        //======== p_tete_droite2 ========
        {
          p_tete_droite2.setOpaque(false);
          p_tete_droite2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite2.setPreferredSize(new Dimension(150, 0));
          p_tete_droite2.setMinimumSize(new Dimension(150, 0));
          p_tete_droite2.setName("p_tete_droite2");
          p_tete_droite2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete_dup.add(p_tete_droite2);
      }
      p_nord.add(barre_tete_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Historisation");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Informations g\u00e9n\u00e9rales");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- DVPAY ----
            DVPAY.setComponentPopupMenu(null);
            DVPAY.setName("DVPAY");
            xTitledPanel1ContentContainer.add(DVPAY);
            DVPAY.setBounds(58, 54, 310, DVPAY.getPreferredSize().height);

            //---- DVLIB ----
            DVLIB.setComponentPopupMenu(null);
            DVLIB.setName("DVLIB");
            xTitledPanel1ContentContainer.add(DVLIB);
            DVLIB.setBounds(58, 19, 210, DVLIB.getPreferredSize().height);

            //---- OBJ_52_OBJ_52 ----
            OBJ_52_OBJ_52.setText("Code monnaie");
            OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
            xTitledPanel1ContentContainer.add(OBJ_52_OBJ_52);
            OBJ_52_OBJ_52.setBounds(283, 23, 97, 20);

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("Devise");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
            xTitledPanel1ContentContainer.add(OBJ_50_OBJ_50);
            OBJ_50_OBJ_50.setBounds(8, 23, 46, 20);

            //---- DVMON ----
            DVMON.setComponentPopupMenu(null);
            DVMON.setName("DVMON");
            xTitledPanel1ContentContainer.add(DVMON);
            DVMON.setBounds(383, 19, 44, DVMON.getPreferredSize().height);

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("Pays");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            xTitledPanel1ContentContainer.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(8, 58, 34, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Libell\u00e9 pour \u00e9dition de facture");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- DVFAC ----
            DVFAC.setComponentPopupMenu(null);
            DVFAC.setName("DVFAC");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addContainerGap(58, Short.MAX_VALUE)
                  .addComponent(DVFAC, GroupLayout.PREFERRED_SIZE, 710, GroupLayout.PREFERRED_SIZE)
                  .addGap(38, 38, 38))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addContainerGap()
                  .addComponent(DVFAC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(15, Short.MAX_VALUE))
            );
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Options de calcul");
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- ULIB1 ----
            ULIB1.setComponentPopupMenu(null);
            ULIB1.setName("ULIB1");
            xTitledPanel3ContentContainer.add(ULIB1);
            ULIB1.setBounds(149, 19, 172, ULIB1.getPreferredSize().height);

            //---- ULIB2 ----
            ULIB2.setComponentPopupMenu(null);
            ULIB2.setName("ULIB2");
            xTitledPanel3ContentContainer.add(ULIB2);
            ULIB2.setBounds(589, 19, 172, ULIB2.getPreferredSize().height);

            //---- DVTAUX ----
            DVTAUX.setComponentPopupMenu(null);
            DVTAUX.setName("DVTAUX");
            xTitledPanel3ContentContainer.add(DVTAUX);
            DVTAUX.setBounds(369, 19, 90, DVTAUX.getPreferredSize().height);

            //---- DVDTHX ----
            DVDTHX.setComponentPopupMenu(null);
            DVDTHX.setName("DVDTHX");
            xTitledPanel3ContentContainer.add(DVDTHX);
            DVDTHX.setBounds(213, 54, 105, DVDTHX.getPreferredSize().height);

            //---- DVBD2 ----
            DVBD2.setComponentPopupMenu(null);
            DVBD2.setName("DVBD2");
            xTitledPanel3ContentContainer.add(DVBD2);
            DVBD2.setBounds(489, 19, 44, DVBD2.getPreferredSize().height);

            //---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("\u00e9gale");
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
            xTitledPanel3ContentContainer.add(OBJ_88_OBJ_88);
            OBJ_88_OBJ_88.setBounds(329, 23, 39, 20);

            //---- DVBD1 ----
            DVBD1.setComponentPopupMenu(null);
            DVBD1.setName("DVBD1");
            xTitledPanel3ContentContainer.add(DVBD1);
            DVBD1.setBounds(54, 19, 44, DVBD1.getPreferredSize().height);

            //---- UDEV1 ----
            UDEV1.setComponentPopupMenu(null);
            UDEV1.setName("UDEV1");
            xTitledPanel3ContentContainer.add(UDEV1);
            UDEV1.setBounds(104, 19, 40, UDEV1.getPreferredSize().height);

            //---- UDEV2 ----
            UDEV2.setComponentPopupMenu(null);
            UDEV2.setName("UDEV2");
            xTitledPanel3ContentContainer.add(UDEV2);
            UDEV2.setBounds(539, 19, 40, UDEV2.getPreferredSize().height);

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("fois");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
            xTitledPanel3ContentContainer.add(OBJ_61_OBJ_61);
            OBJ_61_OBJ_61.setBounds(464, 23, 24, 20);

            //---- label1 ----
            label1.setText("Derni\u00e8re historisation le");
            label1.setName("label1");
            xTitledPanel3ContentContainer.add(label1);
            label1.setBounds(58, 60, 150, label1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Pour information");
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- WBAS2 ----
            WBAS2.setComponentPopupMenu(null);
            WBAS2.setName("WBAS2");
            xTitledPanel4ContentContainer.add(WBAS2);
            WBAS2.setBounds(58, 18, 54, WBAS2.getPreferredSize().height);

            //---- riZoneSortie2 ----
            riZoneSortie2.setText("@WDEV2@");
            riZoneSortie2.setName("riZoneSortie2");
            xTitledPanel4ContentContainer.add(riZoneSortie2);
            riZoneSortie2.setBounds(123, 20, 44, riZoneSortie2.getPreferredSize().height);

            //---- label4 ----
            label4.setText("=");
            label4.setName("label4");
            xTitledPanel4ContentContainer.add(label4);
            label4.setBounds(new Rectangle(new Point(173, 24), label4.getPreferredSize()));

            //---- WCHG2X ----
            WCHG2X.setName("WCHG2X");
            xTitledPanel4ContentContainer.add(WCHG2X);
            WCHG2X.setBounds(188, 18, 94, WCHG2X.getPreferredSize().height);

            //---- label5 ----
            label5.setText("EURO");
            label5.setName("label5");
            xTitledPanel4ContentContainer.add(label5);
            label5.setBounds(new Rectangle(new Point(288, 24), label5.getPreferredSize()));

            //---- label3 ----
            label3.setText("1");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            xTitledPanel4ContentContainer.add(label3);
            label3.setBounds(new Rectangle(new Point(103, 24), label3.getPreferredSize()));

            //---- label6 ----
            label6.setText("EURO");
            label6.setName("label6");
            xTitledPanel4ContentContainer.add(label6);
            label6.setBounds(new Rectangle(new Point(123, 24), label6.getPreferredSize()));

            //---- riZoneSortie3 ----
            riZoneSortie3.setText("@WDEV2B@");
            riZoneSortie3.setName("riZoneSortie3");
            xTitledPanel4ContentContainer.add(riZoneSortie3);
            riZoneSortie3.setBounds(288, 20, 44, riZoneSortie3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 810, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 810, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 810, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_85_OBJ_85;
  private XRiTextField INDDEV;
  private JPanel p_tete_droite;
  private JMenuBar barre_tete_dup;
  private JPanel p_tete_gauche2;
  private XRiTextField IN3DEV;
  private JLabel OBJ_41_OBJ_42;
  private JPanel p_tete_droite2;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField DVPAY;
  private XRiTextField DVLIB;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_50_OBJ_50;
  private XRiTextField DVMON;
  private JLabel OBJ_54_OBJ_54;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField DVFAC;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField ULIB1;
  private XRiTextField ULIB2;
  private XRiTextField DVTAUX;
  private XRiCalendrier DVDTHX;
  private XRiTextField DVBD2;
  private JLabel OBJ_88_OBJ_88;
  private XRiTextField DVBD1;
  private XRiTextField UDEV1;
  private XRiTextField UDEV2;
  private JLabel OBJ_61_OBJ_61;
  private JLabel label1;
  private JXTitledPanel xTitledPanel4;
  private XRiTextField WBAS2;
  private RiZoneSortie riZoneSortie2;
  private JLabel label4;
  private XRiTextField WCHG2X;
  private JLabel label5;
  private JLabel label3;
  private JLabel label6;
  private RiZoneSortie riZoneSortie3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
