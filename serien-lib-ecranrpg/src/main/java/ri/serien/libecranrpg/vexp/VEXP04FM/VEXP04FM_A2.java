//$$david$$ ££05/01/11££ -> tests et modifs + RiTable

package ri.serien.libecranrpg.vexp.VEXP04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiMailto;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VEXP04FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, };
  private int[] _WTP01_Width = { 543, };
  
  public VEXP04FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    lbNom.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Contacts"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      WTP01.setValeurTop("5");
      lexique.HostScreenSendKey(this, "Enter");
    }
    // Affichage du détail lors de la sélection d'une ligne avec la souris dans la liste des contacts
    else {
      WTP01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void WTP01KeyReleased(KeyEvent e) {
    int indexLigneSelectionnee = WTP01.getSelectedRow();
    if (e.getKeyCode() != KeyEvent.VK_UP && e.getKeyCode() != KeyEvent.VK_DOWN || indexLigneSelectionnee <= -1) {
      return;
    }
    // On force la sélection de la ligne
    WTP01.setRowSelectionInterval(indexLigneSelectionnee, indexLigneSelectionnee);
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01KeyPressed(KeyEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel2 = new JPanel();
    lbNomContact = new JLabel();
    lbNom = new RiZoneSortie();
    lbMail = new JLabel();
    EMAIL = new XRiMailto();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 375));
    setPreferredSize(new Dimension(1200, 375));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(660, 285));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout) p_contenu.getLayout()).columnWidths = new int[] { 1020, 0 };
        ((GridBagLayout) p_contenu.getLayout()).rowHeights = new int[] { 257, 0, 0 };
        ((GridBagLayout) p_contenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) p_contenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Liste des contacts"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");
            
            // ---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setPreferredSize(new Dimension(1290, 160));
            WTP01.setMinimumSize(new Dimension(1290, 160));
            WTP01.setMaximumSize(new Dimension(1290, 160));
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            WTP01.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                WTP01KeyPressed(e);
              }
            });
            SCROLLPANE_LIST2.setViewportView(WTP01);
          }
          panel1.add(SCROLLPANE_LIST2);
          SCROLLPANE_LIST2.setBounds(15, 25, 940, 190);
          
          // ---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(960, 25, 25, 85);
          
          // ---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(960, 130, 25, 85);
        }
        p_contenu.add(panel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(10, 10, 15, 10), 0, 0));
        
        // ======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setBorder(new TitledBorder("Contact s\u00e9lectionn\u00e9"));
          panel2.setName("panel2");
          panel2.setLayout(new GridBagLayout());
          ((GridBagLayout) panel2.getLayout()).columnWidths = new int[] { 98, 842, 0 };
          ((GridBagLayout) panel2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) panel2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) panel2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNomContact ----
          lbNomContact.setText("Nom du contact");
          lbNomContact.setMinimumSize(new Dimension(86, 30));
          lbNomContact.setMaximumSize(new Dimension(86, 30));
          lbNomContact.setPreferredSize(new Dimension(86, 30));
          lbNomContact.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNomContact.setName("lbNomContact");
          panel2.add(lbNomContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbNom ----
          lbNom.setText("@ENOM@");
          lbNom.setPreferredSize(new Dimension(350, 30));
          lbNom.setMinimumSize(new Dimension(350, 30));
          lbNom.setMaximumSize(new Dimension(350, 30));
          lbNom.setName("lbNom");
          panel2.add(lbNom, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMail ----
          lbMail.setText("Mail");
          lbMail.setMinimumSize(new Dimension(86, 30));
          lbMail.setMaximumSize(new Dimension(86, 30));
          lbMail.setPreferredSize(new Dimension(86, 30));
          lbMail.setHorizontalAlignment(SwingConstants.RIGHT);
          lbMail.setName("lbMail");
          panel2.add(lbMail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EMAIL ----
          EMAIL.setPreferredSize(new Dimension(800, 30));
          EMAIL.setMinimumSize(new Dimension(800, 30));
          EMAIL.setMaximumSize(new Dimension(800, 30));
          EMAIL.setName("EMAIL");
          panel2.add(EMAIL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(panel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 10, 10, 10), 0, 0));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Modifier");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Annuler");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Interroger");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel2;
  private JLabel lbNomContact;
  private RiZoneSortie lbNom;
  private JLabel lbMail;
  private XRiMailto EMAIL;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
