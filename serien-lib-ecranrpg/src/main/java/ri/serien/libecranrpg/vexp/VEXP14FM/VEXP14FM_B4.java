
package ri.serien.libecranrpg.vexp.VEXP14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP14FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP14FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    NBBIB.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    RFIC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFIC@")).trim());
    NBBIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sauvegarde des Fichiers @RFIC2@ de vos autres bibliothèques utilisateurs ?")).trim());
    MODUL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MODUL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    NVOL.setEnabled(lexique.isPresent("NVOL"));
    DENS.setEnabled(lexique.isPresent("DENS"));
    TGTRLS.setEnabled(lexique.isPresent("TGTRLS"));
    UNITE.setEnabled(lexique.isPresent("UNITE"));
    // NBBIB.setVisible( lexique.isPresent("NBBIB"));
    // NBBIB.setSelected(lexique.HostFieldGetData("NBBIB").equalsIgnoreCase("**"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (NBBIB.isSelected())
    // lexique.HostFieldPutData("NBBIB", 0, "**");
    // else
    // lexique.HostFieldPutData("NBBIB", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_64 = new JLabel();
    RFIC = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_58 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_53 = new JXTitledSeparator();
    OBJ_59 = new JXTitledSeparator();
    OBJ_63 = new JXTitledSeparator();
    NBBIB = new XRiCheckBox();
    OBJ_56 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_47 = new JLabel();
    UNITE = new XRiTextField();
    TGTRLS = new XRiTextField();
    DENS = new XRiTextField();
    NVOL = new XRiTextField();
    OBJ_69 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_70 = new JLabel();
    MODUL = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Sauvegarde de fichiers S\u00e9rie N");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_64 ----
          OBJ_64.setText("Vous venez de demander la Sauvegarde des Fichiers");
          OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
          OBJ_64.setName("OBJ_64");
          p_tete_gauche.add(OBJ_64);
          OBJ_64.setBounds(5, 0, 320, 29);

          //---- RFIC ----
          RFIC.setOpaque(false);
          RFIC.setText("@RFIC@");
          RFIC.setName("RFIC");
          p_tete_gauche.add(RFIC);
          RFIC.setBounds(320, 2, 50, RFIC.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(730, 475));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_58 ----
          OBJ_58.setText("Elle doit \u00eatre archiv\u00e9e dans un coffre ou \u00e0 l'ext\u00e9rieur de vos locaux et ne jamais \u00eatre r\u00e9utilis\u00e9e");
          OBJ_58.setName("OBJ_58");

          //---- OBJ_68 ----
          OBJ_68.setText("Ne vous servez pas de la derni\u00e8re sauvegarde, utilisez plusieurs Bandes par s\u00e9curit\u00e9");
          OBJ_68.setName("OBJ_68");

          //---- OBJ_53 ----
          OBJ_53.setTitle("SAUVEGARDE AVANT ARRETE");
          OBJ_53.setName("OBJ_53");

          //---- OBJ_59 ----
          OBJ_59.setTitle("SAUVEGARDE AVANT CLOTURE");
          OBJ_59.setName("OBJ_59");

          //---- OBJ_63 ----
          OBJ_63.setTitle("INFORMATIONS SUR LA SAUVEGARDE");
          OBJ_63.setName("OBJ_63");

          //---- NBBIB ----
          NBBIB.setText("Sauvegarde des Fichiers @RFIC2@ de vos autres biblioth\u00e8ques utilisateurs ?");
          NBBIB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NBBIB.setName("NBBIB");

          //---- OBJ_56 ----
          OBJ_56.setText("Inscrivez sur le catalogue : SAUVEGARDE AVANT ARRETE/MOIS/MODULE");
          OBJ_56.setName("OBJ_56");

          //---- OBJ_55 ----
          OBJ_55.setText("Elle doit \u00eatre conserv\u00e9e 3 mois minimum et si possible tout l'exercice");
          OBJ_55.setName("OBJ_55");

          //---- OBJ_67 ----
          OBJ_67.setText("Vous devez au pr\u00e9alable vous munir d'une Bande initialisable");
          OBJ_67.setName("OBJ_67");

          //---- OBJ_26 ----
          OBJ_26.setText("Celle-ci contiendra toutes les informations relatives au module");
          OBJ_26.setName("OBJ_26");

          //---- OBJ_61 ----
          OBJ_61.setText("Saisir le nom du volume et l'unit\u00e9");
          OBJ_61.setName("OBJ_61");

          //---- OBJ_47 ----
          OBJ_47.setText("pour toutes les soci\u00e9t\u00e9s du groupe");
          OBJ_47.setName("OBJ_47");

          //---- UNITE ----
          UNITE.setComponentPopupMenu(BTD);
          UNITE.setName("UNITE");

          //---- TGTRLS ----
          TGTRLS.setComponentPopupMenu(BTD);
          TGTRLS.setName("TGTRLS");

          //---- DENS ----
          DENS.setComponentPopupMenu(BTD);
          DENS.setName("DENS");

          //---- NVOL ----
          NVOL.setComponentPopupMenu(BTD);
          NVOL.setName("NVOL");

          //---- OBJ_69 ----
          OBJ_69.setText("Unit\u00e9/Bande");
          OBJ_69.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_69.setName("OBJ_69");

          //---- OBJ_60 ----
          OBJ_60.setText("Volume");
          OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_60.setName("OBJ_60");

          //---- OBJ_71 ----
          OBJ_71.setText("Densit\u00e9");
          OBJ_71.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_71.setName("OBJ_71");

          //---- OBJ_70 ----
          OBJ_70.setText("Version OS");
          OBJ_70.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_70.setName("OBJ_70");

          //---- MODUL ----
          MODUL.setText("@MODUL@");
          MODUL.setName("MODUL");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(355, 355, 355)
                    .addComponent(MODUL, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 472, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 543, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(NBBIB, GroupLayout.PREFERRED_SIZE, 498, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                  .addComponent(OBJ_60, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(NVOL, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                  .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(TGTRLS, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                  .addComponent(DENS, GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                  .addComponent(OBJ_71, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(MODUL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGap(20, 20, 20)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(NVOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TGTRLS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DENS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(NBBIB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_64;
  private RiZoneSortie RFIC;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_58;
  private JLabel OBJ_68;
  private JXTitledSeparator OBJ_53;
  private JXTitledSeparator OBJ_59;
  private JXTitledSeparator OBJ_63;
  private XRiCheckBox NBBIB;
  private JLabel OBJ_56;
  private JLabel OBJ_55;
  private JLabel OBJ_67;
  private JLabel OBJ_26;
  private JLabel OBJ_61;
  private JLabel OBJ_47;
  private XRiTextField UNITE;
  private XRiTextField TGTRLS;
  private XRiTextField DENS;
  private XRiTextField NVOL;
  private JLabel OBJ_69;
  private JLabel OBJ_60;
  private JLabel OBJ_71;
  private JLabel OBJ_70;
  private RiZoneSortie MODUL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
