
package ri.serien.libecranrpg.vexp.VEXP87FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VEXP87FM_B1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _T01_Top =
      { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "Bibliothèque", "Lettre", "Fichier", "Programme" };
  private String[][] _T01_Data = { { "B01", "L01", "F01", "V01" }, { "B02", "L02", "F02", "V02" }, { "B03", "L03", "F03", "V03" },
      { "B04", "L04", "F04", "V04" }, { "B05", "L05", "F05", "V05" }, { "B06", "L06", "F06", "V06" }, { "B07", "L07", "F07", "V07" },
      { "B08", "L08", "F08", "V08" }, { "B09", "L09", "F09", "V09" }, { "B10", "L10", "F10", "V10" }, { "B11", "L11", "F11", "V11" },
      { "B12", "L12", "F12", "V12" }, { "B13", "L13", "F13", "V13" }, { "B14", "L14", "F14", "V14" }, { "B15", "L15", "F15", "V15" }, };
  private int[] _T01_Width = { 124, 40, 84, 84, };
  
  /**
   * Constructeur.
   */
  public VEXP87FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
    
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion des numéros de versions"));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void miVersionActionPerformed(ActionEvent e) {
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void T01MouseClicked(MouseEvent e) {
    if (T01.doubleClicSelection(e)) {
      T01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void miBibliothequeTestActionPerformed(ActionEvent e) {
    T01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    pnlContenu = new JPanel();
    scpListe = new JScrollPane();
    T01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    miVersion = new JMenuItem();
    miBibliothequeTest = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(710, 310));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      pnlPrincipal.add(p_menus, BorderLayout.EAST);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);
        
        // ======== scpListe ========
        {
          scpListe.setName("scpListe");
          
          // ---- T01 ----
          T01.setComponentPopupMenu(BTD);
          T01.setName("T01");
          T01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              T01MouseClicked(e);
            }
          });
          scpListe.setViewportView(T01);
        }
        pnlContenu.add(scpListe);
        scpListe.setBounds(15, 15, scpListe.getPreferredSize().width, 270);
        
        // ---- BT_PGUP ----
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        pnlContenu.add(BT_PGUP);
        BT_PGUP.setBounds(485, 15, 25, 135);
        
        // ---- BT_PGDOWN ----
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlContenu.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(485, 155, 25, 135);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- miVersion ----
      miVersion.setText("Gestion de la version");
      miVersion.setName("miVersion");
      miVersion.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miVersionActionPerformed(e);
        }
      });
      BTD.add(miVersion);
      
      // ---- miBibliothequeTest ----
      miBibliothequeTest.setText("Biblioth\u00e8que de test");
      miBibliothequeTest.setName("miBibliothequeTest");
      miBibliothequeTest.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miBibliothequeTestActionPerformed(e);
        }
      });
      BTD.add(miBibliothequeTest);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel pnlContenu;
  private JScrollPane scpListe;
  private XRiTable T01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem miVersion;
  private JMenuItem miBibliothequeTest;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
