
package ri.serien.libecranrpg.vexp.VEXP14FM;
// Nom Fichier: b_VEXP14FM_FMTB2_FMTF1_37.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP14FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP14FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ARR.setValeursSelection("OUI", "NON");
    NBBIB.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    
    OBJ_72.setVisible(lexique.isPresent("HHD"));
    OBJ_67.setVisible(lexique.isPresent("SSD"));
    OBJ_64.setVisible(lexique.isPresent("AAD"));
    OBJ_62.setVisible(lexique.isPresent("JJD"));
    OBJ_65.setVisible(lexique.isPresent("HHD"));
    OBJ_66.setVisible(lexique.isPresent("MND"));
    OBJ_63.setVisible(lexique.isPresent("MMD"));
    SSD.setVisible(lexique.isPresent("SSD"));
    MND.setVisible(lexique.isPresent("MND"));
    HHD.setVisible(lexique.isPresent("HHD"));
    AAD.setVisible(lexique.isPresent("AAD"));
    MMD.setVisible(lexique.isPresent("MMD"));
    JJD.setVisible(lexique.isPresent("JJD"));
    NVOL.setEnabled(lexique.isPresent("NVOL"));
    DENS.setEnabled(lexique.isPresent("DENS"));
    TGTRLS.setEnabled(lexique.isPresent("TGTRLS"));
    UNITE.setEnabled(lexique.isPresent("UNITE"));
    // ARR.setVisible( lexique.isPresent("ARR"));
    // ARR.setSelected(lexique.HostFieldGetData("ARR").equalsIgnoreCase("OUI"));
    // NBBIB.setVisible( lexique.isPresent("NBBIB"));
    // NBBIB.setSelected(lexique.HostFieldGetData("NBBIB").equalsIgnoreCase("**"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (ARR.isSelected())
    // lexique.HostFieldPutData("ARR", 0, "OUI");
    // else
    // lexique.HostFieldPutData("ARR", 0, "NON");
    // if (NBBIB.isSelected())
    // lexique.HostFieldPutData("NBBIB", 0, "**");
    // else
    // lexique.HostFieldPutData("NBBIB", 0, " ");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_32 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_45 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_36 = new JXTitledSeparator();
    OBJ_43 = new JXTitledSeparator();
    OBJ_49 = new JXTitledSeparator();
    OBJ_41 = new JLabel();
    NBBIB = new XRiCheckBox();
    OBJ_37 = new JLabel();
    OBJ_46 = new JLabel();
    ARR = new XRiCheckBox();
    OBJ_56 = new JLabel();
    UNITE = new XRiTextField();
    TGTRLS = new XRiTextField();
    DENS = new XRiTextField();
    NVOL = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_54 = new JLabel();
    JJD = new XRiTextField();
    MMD = new XRiTextField();
    AAD = new XRiTextField();
    HHD = new XRiTextField();
    MND = new XRiTextField();
    SSD = new XRiTextField();
    OBJ_63 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_72 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Sauvegarde de fichiers S\u00e9rie N");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_32 ----
          OBJ_32.setText("Vous venez de demander la Sauvegarde Globale de vos Fichiers S\u00e9rie M");
          OBJ_32.setFont(OBJ_32.getFont().deriveFont(OBJ_32.getFont().getStyle() | Font.BOLD));
          OBJ_32.setName("OBJ_32");
          p_tete_gauche.add(OBJ_32);
          OBJ_32.setBounds(5, 0, 621, 30);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_45 ----
          OBJ_45.setText("Elle doit \u00eatre archiv\u00e9e dans un coffre ou \u00e0 l'ext\u00e9rieur de vos locaux et ne jamais \u00eatre r\u00e9utilis\u00e9e");
          OBJ_45.setName("OBJ_45");

          //---- OBJ_47 ----
          OBJ_47.setText("Ne vous servez jamais de la derni\u00e8re sauvegarde, utilisez plusieurs Cartouches par s\u00e9curit\u00e9");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_36 ----
          OBJ_36.setTitle("SAUVEGARDE AVANT ARRETE");
          OBJ_36.setName("OBJ_36");

          //---- OBJ_43 ----
          OBJ_43.setTitle("SAUVEGARDE AVANT CLOTURE");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_49 ----
          OBJ_49.setTitle("INFORMATIONS SUR LA SAUVEGARDE");
          OBJ_49.setName("OBJ_49");

          //---- OBJ_41 ----
          OBJ_41.setText("Inscrivez sur le catalogue : SAUVEGARDE AVANT ARRETE/MOIS/MODULE");
          OBJ_41.setName("OBJ_41");

          //---- NBBIB ----
          NBBIB.setText("Sauvegarde des Fichiers de vos autres biblioth\u00e8ques utilisateurs ?");
          NBBIB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NBBIB.setName("NBBIB");

          //---- OBJ_37 ----
          OBJ_37.setText("Elle doit \u00eatre conserv\u00e9e 3 mois minimum et si possible tout l'Exercice");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_46 ----
          OBJ_46.setText("Vous devez au pr\u00e9alable vous munir d'une Cartouche initialisable");
          OBJ_46.setName("OBJ_46");

          //---- ARR ----
          ARR.setText("Arr\u00eat de la machine en Fin de Sauvegarde ?");
          ARR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARR.setName("ARR");

          //---- OBJ_56 ----
          OBJ_56.setText("Saisir le NOM du VOLUME et l'UNITE");
          OBJ_56.setName("OBJ_56");

          //---- UNITE ----
          UNITE.setComponentPopupMenu(BTD);
          UNITE.setName("UNITE");

          //---- TGTRLS ----
          TGTRLS.setComponentPopupMenu(BTD);
          TGTRLS.setName("TGTRLS");

          //---- DENS ----
          DENS.setComponentPopupMenu(BTD);
          DENS.setName("DENS");

          //---- NVOL ----
          NVOL.setComponentPopupMenu(BTD);
          NVOL.setName("NVOL");

          //---- OBJ_53 ----
          OBJ_53.setText("Unit\u00e9/Bande");
          OBJ_53.setName("OBJ_53");

          //---- OBJ_52 ----
          OBJ_52.setText("Volume");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_55 ----
          OBJ_55.setText("Densit\u00e9");
          OBJ_55.setName("OBJ_55");

          //---- OBJ_54 ----
          OBJ_54.setText("Version OS");
          OBJ_54.setName("OBJ_54");

          //---- JJD ----
          JJD.setComponentPopupMenu(BTD);
          JJD.setName("JJD");

          //---- MMD ----
          MMD.setComponentPopupMenu(BTD);
          MMD.setName("MMD");

          //---- AAD ----
          AAD.setComponentPopupMenu(BTD);
          AAD.setName("AAD");

          //---- HHD ----
          HHD.setComponentPopupMenu(BTD);
          HHD.setName("HHD");

          //---- MND ----
          MND.setComponentPopupMenu(BTD);
          MND.setName("MND");

          //---- SSD ----
          SSD.setComponentPopupMenu(BTD);
          SSD.setName("SSD");

          //---- OBJ_63 ----
          OBJ_63.setText("MM");
          OBJ_63.setName("OBJ_63");

          //---- OBJ_66 ----
          OBJ_66.setText("MN");
          OBJ_66.setName("OBJ_66");

          //---- OBJ_65 ----
          OBJ_65.setText("HH");
          OBJ_65.setName("OBJ_65");

          //---- OBJ_62 ----
          OBJ_62.setText("JJ");
          OBJ_62.setName("OBJ_62");

          //---- OBJ_64 ----
          OBJ_64.setText("AA");
          OBJ_64.setName("OBJ_64");

          //---- OBJ_67 ----
          OBJ_67.setText("SS");
          OBJ_67.setName("OBJ_67");

          //---- OBJ_72 ----
          OBJ_72.setText("\u00e0");
          OBJ_72.setName("OBJ_72");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 472, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 412, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(304, 304, 304)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(NVOL, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(TGTRLS, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(DENS, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(374, 374, 374)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(ARR, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(JJD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(MMD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(AAD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(HHD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(MND, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(SSD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(NBBIB, GroupLayout.PREFERRED_SIZE, 423, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(NVOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TGTRLS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DENS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(JJD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MMD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AAD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(HHD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SSD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(ARR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGap(16, 16, 16)
                .addComponent(NBBIB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_32;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_45;
  private JLabel OBJ_47;
  private JXTitledSeparator OBJ_36;
  private JXTitledSeparator OBJ_43;
  private JXTitledSeparator OBJ_49;
  private JLabel OBJ_41;
  private XRiCheckBox NBBIB;
  private JLabel OBJ_37;
  private JLabel OBJ_46;
  private XRiCheckBox ARR;
  private JLabel OBJ_56;
  private XRiTextField UNITE;
  private XRiTextField TGTRLS;
  private XRiTextField DENS;
  private XRiTextField NVOL;
  private JLabel OBJ_53;
  private JLabel OBJ_52;
  private JLabel OBJ_55;
  private JLabel OBJ_54;
  private XRiTextField JJD;
  private XRiTextField MMD;
  private XRiTextField AAD;
  private XRiTextField HHD;
  private XRiTextField MND;
  private XRiTextField SSD;
  private JLabel OBJ_63;
  private JLabel OBJ_66;
  private JLabel OBJ_65;
  private JLabel OBJ_62;
  private JLabel OBJ_64;
  private JLabel OBJ_67;
  private JLabel OBJ_72;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
