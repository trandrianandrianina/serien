/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vexp.VEXPPLAF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXPPLAF_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] JRM05_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM04_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM03_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM02_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  private String[] JRM01_Value = { " ", "1", "2", "3", "4", "5", "*LAST", };
  
  public VEXPPLAF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    JRM05.setValeurs(JRM05_Value, null);
    JRM04.setValeurs(JRM04_Value, null);
    JRM03.setValeurs(JRM03_Value, null);
    JRM02.setValeurs(JRM02_Value, null);
    JRM01.setValeurs(JRM01_Value, null);
    JT04.setValeursSelection("X", " ");
    JT01.setValeursSelection("X", " ");
    JT02.setValeursSelection("X", " ");
    JT06.setValeursSelection("X", " ");
    JT03.setValeursSelection("X", " ");
    JT05.setValeursSelection("X", " ");
    JT07.setValeursSelection("X", " ");
    FREQ.setValeurs("J", FREQ_GRP);
    rbFrequenceSemaine.setValeurs("S");
    rbFrequenqueMensuelle.setValeurs("M");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    pnlSoumissionCodePlanning.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression(" Travail @TRAV@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    HEUTRT.setVisible(lexique.isPresent("HEUTRT"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Soumission d'un code au planning des travaux"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void DATTRTActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlSoumissionCodePlanning = new SNPanelTitre();
    pnlFrequence = new SNPanel();
    lbFrequence = new SNLabelChamp();
    FREQ = new XRiRadioButton();
    rbFrequenceSemaine = new XRiRadioButton();
    rbFrequenqueMensuelle = new XRiRadioButton();
    pnlDateOuJour = new SNPanel();
    lbDate = new SNLabelChamp();
    DATTRE = new XRiCalendrier();
    lbOu = new SNLabelTitre();
    lbJours = new SNLabelChamp();
    JT01 = new XRiCheckBox();
    JT03 = new XRiCheckBox();
    JT05 = new XRiCheckBox();
    JT07 = new XRiCheckBox();
    JT06 = new XRiCheckBox();
    JT04 = new XRiCheckBox();
    JT02 = new XRiCheckBox();
    pnlHeure = new SNPanel();
    lbHeure = new SNLabelChamp();
    HEUTRT = new XRiTextField();
    pnlJourRelatifMois = new SNPanel();
    lbJourRelatifMois = new SNLabelChamp();
    JRM01 = new XRiComboBox();
    JRM02 = new XRiComboBox();
    JRM03 = new XRiComboBox();
    JRM04 = new XRiComboBox();
    JRM05 = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    FREQ_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(750, 370));
    setPreferredSize(new Dimension(750, 370));
    setMaximumSize(new Dimension(750, 370));
    setName("this");
    setLayout(new BorderLayout());

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlSoumissionCodePlanning ========
      {
        pnlSoumissionCodePlanning.setOpaque(false);
        pnlSoumissionCodePlanning.setBackground(new Color(214, 217, 223));
        pnlSoumissionCodePlanning.setBorder(new TitledBorder(" Travail @TRAV@"));
        pnlSoumissionCodePlanning.setName("pnlSoumissionCodePlanning");
        pnlSoumissionCodePlanning.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlSoumissionCodePlanning.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlSoumissionCodePlanning.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlSoumissionCodePlanning.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlSoumissionCodePlanning.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlFrequence ========
        {
          pnlFrequence.setName("pnlFrequence");
          pnlFrequence.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlFrequence.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlFrequence.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlFrequence.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlFrequence.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbFrequence ----
          lbFrequence.setText("Fr\u00e9quence");
          lbFrequence.setName("lbFrequence");
          pnlFrequence.add(lbFrequence, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- FREQ ----
          FREQ.setText("Journali\u00e8re");
          FREQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FREQ.setMinimumSize(new Dimension(120, 30));
          FREQ.setPreferredSize(new Dimension(120, 30));
          FREQ.setFont(new Font("sansserif", Font.PLAIN, 14));
          FREQ.setForeground(Color.black);
          FREQ.setHorizontalAlignment(SwingConstants.CENTER);
          FREQ.setName("FREQ");
          pnlFrequence.add(FREQ, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- rbFrequenceSemaine ----
          rbFrequenceSemaine.setText("Hebdomadaire");
          rbFrequenceSemaine.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbFrequenceSemaine.setMinimumSize(new Dimension(120, 30));
          rbFrequenceSemaine.setPreferredSize(new Dimension(120, 30));
          rbFrequenceSemaine.setFont(new Font("sansserif", Font.PLAIN, 14));
          rbFrequenceSemaine.setName("rbFrequenceSemaine");
          pnlFrequence.add(rbFrequenceSemaine, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- rbFrequenqueMensuelle ----
          rbFrequenqueMensuelle.setText("Mensuelle");
          rbFrequenqueMensuelle.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbFrequenqueMensuelle.setMinimumSize(new Dimension(120, 30));
          rbFrequenqueMensuelle.setPreferredSize(new Dimension(120, 30));
          rbFrequenqueMensuelle.setFont(new Font("sansserif", Font.PLAIN, 14));
          rbFrequenqueMensuelle.setHorizontalAlignment(SwingConstants.CENTER);
          rbFrequenqueMensuelle.setName("rbFrequenqueMensuelle");
          pnlFrequence.add(rbFrequenqueMensuelle, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSoumissionCodePlanning.add(pnlFrequence, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlDateOuJour ========
        {
          pnlDateOuJour.setName("pnlDateOuJour");
          pnlDateOuJour.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDateOuJour.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDateOuJour.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDateOuJour.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlDateOuJour.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbDate ----
          lbDate.setText("Date");
          lbDate.setName("lbDate");
          pnlDateOuJour.add(lbDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- DATTRE ----
          DATTRE.setName("DATTRE");
          pnlDateOuJour.add(DATTRE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbOu ----
          lbOu.setText("ou");
          lbOu.setVerticalAlignment(SwingConstants.CENTER);
          lbOu.setMaximumSize(new Dimension(50, 30));
          lbOu.setMinimumSize(new Dimension(50, 30));
          lbOu.setPreferredSize(new Dimension(50, 30));
          lbOu.setName("lbOu");
          pnlDateOuJour.add(lbOu, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbJours ----
          lbJours.setText("Jour(s)");
          lbJours.setName("lbJours");
          pnlDateOuJour.add(lbJours, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- JT01 ----
          JT01.setText("Lundi");
          JT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT01.setFont(new Font("sansserif", Font.PLAIN, 14));
          JT01.setMinimumSize(new Dimension(120, 30));
          JT01.setPreferredSize(new Dimension(120, 30));
          JT01.setMaximumSize(new Dimension(90, 30));
          JT01.setName("JT01");
          pnlDateOuJour.add(JT01, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- JT03 ----
          JT03.setText("Mercredi");
          JT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT03.setFont(new Font("sansserif", Font.PLAIN, 14));
          JT03.setMaximumSize(new Dimension(90, 30));
          JT03.setMinimumSize(new Dimension(120, 30));
          JT03.setPreferredSize(new Dimension(120, 30));
          JT03.setName("JT03");
          pnlDateOuJour.add(JT03, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- JT05 ----
          JT05.setText("Vendredi");
          JT05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT05.setFont(new Font("sansserif", Font.PLAIN, 14));
          JT05.setMaximumSize(new Dimension(90, 30));
          JT05.setMinimumSize(new Dimension(120, 30));
          JT05.setPreferredSize(new Dimension(120, 30));
          JT05.setName("JT05");
          pnlDateOuJour.add(JT05, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- JT07 ----
          JT07.setText("Dimanche");
          JT07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT07.setFont(new Font("sansserif", Font.PLAIN, 14));
          JT07.setMaximumSize(new Dimension(90, 30));
          JT07.setMinimumSize(new Dimension(120, 30));
          JT07.setPreferredSize(new Dimension(120, 30));
          JT07.setName("JT07");
          pnlDateOuJour.add(JT07, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- JT06 ----
          JT06.setText("Samedi");
          JT06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT06.setFont(new Font("sansserif", Font.PLAIN, 14));
          JT06.setMaximumSize(new Dimension(90, 30));
          JT06.setMinimumSize(new Dimension(120, 30));
          JT06.setPreferredSize(new Dimension(120, 30));
          JT06.setName("JT06");
          pnlDateOuJour.add(JT06, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- JT04 ----
          JT04.setText("Jeudi");
          JT04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT04.setFont(new Font("sansserif", Font.PLAIN, 14));
          JT04.setMaximumSize(new Dimension(90, 30));
          JT04.setMinimumSize(new Dimension(120, 30));
          JT04.setPreferredSize(new Dimension(120, 30));
          JT04.setName("JT04");
          pnlDateOuJour.add(JT04, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- JT02 ----
          JT02.setText("Mardi");
          JT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JT02.setFont(new Font("sansserif", Font.PLAIN, 14));
          JT02.setMaximumSize(new Dimension(90, 30));
          JT02.setMinimumSize(new Dimension(120, 30));
          JT02.setPreferredSize(new Dimension(120, 30));
          JT02.setName("JT02");
          pnlDateOuJour.add(JT02, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlSoumissionCodePlanning.add(pnlDateOuJour, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlHeure ========
        {
          pnlHeure.setName("pnlHeure");
          pnlHeure.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlHeure.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlHeure.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlHeure.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlHeure.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbHeure ----
          lbHeure.setText("Heure");
          lbHeure.setName("lbHeure");
          pnlHeure.add(lbHeure, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- HEUTRT ----
          HEUTRT.setToolTipText("?V088692F_B1.HEUTRT.toolTipText?");
          HEUTRT.setComponentPopupMenu(BTD);
          HEUTRT.setFont(new Font("sansserif", Font.PLAIN, 14));
          HEUTRT.setMaximumSize(new Dimension(70, 30));
          HEUTRT.setMinimumSize(new Dimension(70, 30));
          HEUTRT.setPreferredSize(new Dimension(70, 30));
          HEUTRT.setName("HEUTRT");
          pnlHeure.add(HEUTRT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSoumissionCodePlanning.add(pnlHeure, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlJourRelatifMois ========
        {
          pnlJourRelatifMois.setName("pnlJourRelatifMois");
          pnlJourRelatifMois.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlJourRelatifMois.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlJourRelatifMois.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlJourRelatifMois.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlJourRelatifMois.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbJourRelatifMois ----
          lbJourRelatifMois.setText("Jour relatif du mois");
          lbJourRelatifMois.setName("lbJourRelatifMois");
          pnlJourRelatifMois.add(lbJourRelatifMois, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- JRM01 ----
          JRM01.setModel(new DefaultComboBoxModel(new String[] {
            "aucun",
            "premier",
            "deuxi\u00e8me",
            "troisi\u00e8me",
            "quatri\u00e8me",
            "cinqui\u00e8me",
            "dernier"
          }));
          JRM01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM01.setMaximumSize(new Dimension(100, 30));
          JRM01.setFont(new Font("sansserif", Font.PLAIN, 14));
          JRM01.setName("JRM01");
          pnlJourRelatifMois.add(JRM01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- JRM02 ----
          JRM02.setModel(new DefaultComboBoxModel(new String[] {
            "aucun",
            "premier",
            "deuxi\u00e8me",
            "troisi\u00e8me",
            "quatri\u00e8me",
            "cinqui\u00e8me",
            "dernier"
          }));
          JRM02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM02.setMaximumSize(new Dimension(100, 30));
          JRM02.setFont(new Font("sansserif", Font.PLAIN, 14));
          JRM02.setName("JRM02");
          pnlJourRelatifMois.add(JRM02, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- JRM03 ----
          JRM03.setModel(new DefaultComboBoxModel(new String[] {
            "aucun",
            "premier",
            "deuxi\u00e8me",
            "troisi\u00e8me",
            "quatri\u00e8me",
            "cinqui\u00e8me",
            "dernier"
          }));
          JRM03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM03.setMaximumSize(new Dimension(100, 30));
          JRM03.setFont(new Font("sansserif", Font.PLAIN, 14));
          JRM03.setName("JRM03");
          pnlJourRelatifMois.add(JRM03, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- JRM04 ----
          JRM04.setModel(new DefaultComboBoxModel(new String[] {
            "aucun",
            "premier",
            "deuxi\u00e8me",
            "troisi\u00e8me",
            "quatri\u00e8me",
            "cinqui\u00e8me",
            "dernier"
          }));
          JRM04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM04.setMaximumSize(new Dimension(100, 30));
          JRM04.setFont(new Font("sansserif", Font.PLAIN, 14));
          JRM04.setName("JRM04");
          pnlJourRelatifMois.add(JRM04, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- JRM05 ----
          JRM05.setModel(new DefaultComboBoxModel(new String[] {
            "aucun",
            "premier",
            "deuxi\u00e8me",
            "troisi\u00e8me",
            "quatri\u00e8me",
            "cinqui\u00e8me",
            "dernier"
          }));
          JRM05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          JRM05.setMaximumSize(new Dimension(100, 30));
          JRM05.setFont(new Font("sansserif", Font.PLAIN, 14));
          JRM05.setName("JRM05");
          pnlJourRelatifMois.add(JRM05, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSoumissionCodePlanning.add(pnlJourRelatifMois, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSoumissionCodePlanning);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("?V088692F_B1.OBJ_10.text?");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //---- FREQ_GRP ----
    FREQ_GRP.add(FREQ);
    FREQ_GRP.add(rbFrequenceSemaine);
    FREQ_GRP.add(rbFrequenqueMensuelle);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlSoumissionCodePlanning;
  private SNPanel pnlFrequence;
  private SNLabelChamp lbFrequence;
  private XRiRadioButton FREQ;
  private XRiRadioButton rbFrequenceSemaine;
  private XRiRadioButton rbFrequenqueMensuelle;
  private SNPanel pnlDateOuJour;
  private SNLabelChamp lbDate;
  private XRiCalendrier DATTRE;
  private SNLabelTitre lbOu;
  private SNLabelChamp lbJours;
  private XRiCheckBox JT01;
  private XRiCheckBox JT03;
  private XRiCheckBox JT05;
  private XRiCheckBox JT07;
  private XRiCheckBox JT06;
  private XRiCheckBox JT04;
  private XRiCheckBox JT02;
  private SNPanel pnlHeure;
  private SNLabelChamp lbHeure;
  private XRiTextField HEUTRT;
  private SNPanel pnlJourRelatifMois;
  private SNLabelChamp lbJourRelatifMois;
  private XRiComboBox JRM01;
  private XRiComboBox JRM02;
  private XRiComboBox JRM03;
  private XRiComboBox JRM04;
  private XRiComboBox JRM05;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private ButtonGroup FREQ_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
