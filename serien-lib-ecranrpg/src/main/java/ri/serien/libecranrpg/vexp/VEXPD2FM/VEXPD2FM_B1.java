
package ri.serien.libecranrpg.vexp.VEXPD2FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.GestionDocumentation;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;

/**
 * @author Stéphane Vénéri
 */
public class VEXPD2FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private GestionDocumentation[] gestionDoc = new GestionDocumentation[10]; // 10 étant le nombre de boutons possibles
  private SNPanelEcranRPG thisPanel = this;
  
  public VEXPD2FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC01@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC02@")).trim());
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC03@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC04@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC05@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC06@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC07@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC08@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC09@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DOC10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    OBJ_16.setVisible(!lexique.HostFieldGetData("DOC01").trim().equalsIgnoreCase(""));
    OBJ_17.setVisible(!lexique.HostFieldGetData("DOC02").trim().equalsIgnoreCase(""));
    OBJ_18.setVisible(!lexique.HostFieldGetData("DOC03").trim().equalsIgnoreCase(""));
    OBJ_19.setVisible(!lexique.HostFieldGetData("DOC04").trim().equalsIgnoreCase(""));
    OBJ_20.setVisible(!lexique.HostFieldGetData("DOC05").trim().equalsIgnoreCase(""));
    OBJ_21.setVisible(!lexique.HostFieldGetData("DOC06").trim().equalsIgnoreCase(""));
    OBJ_22.setVisible(!lexique.HostFieldGetData("DOC07").trim().equalsIgnoreCase(""));
    OBJ_23.setVisible(!lexique.HostFieldGetData("DOC08").trim().equalsIgnoreCase(""));
    OBJ_24.setVisible(!lexique.HostFieldGetData("DOC09").trim().equalsIgnoreCase(""));
    OBJ_25.setVisible(!lexique.HostFieldGetData("DOC10").trim().equalsIgnoreCase(""));
    
    if (OBJ_16.isVisible() && !OBJ_17.isVisible() && !OBJ_18.isVisible() && !OBJ_19.isVisible() && !OBJ_20.isVisible()
        && !OBJ_21.isVisible() && !OBJ_22.isVisible() && !OBJ_23.isVisible() && !OBJ_24.isVisible() && !OBJ_25.isVisible()) {
      construitChemin(0, "RAC01", "CHE01");
      new Thread() {
        @Override
        public void run() {
          try {
            Thread.sleep(1000);
          }
          catch (InterruptedException e) {
            // TODO Bloc catch généré automatiquement
            e.printStackTrace();
          }
          lexique.HostScreenSendKey(thisPanel, "F12");
        }
      }.start();
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion des documents"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Construit le chemin à partir des données envoyé par l'AS/400
   * @param hstfldrac
   * @param hstfldche
   */
  private void construitChemin(int indice, String hstfldrac, String hstfldche) {
    String cheminDossier = lexique.HostFieldGetData(hstfldrac).trim() + lexique.HostFieldGetData(hstfldche).trim();
    if ((gestionDoc[indice] == null) || (!gestionDoc[indice].open)) {
      gestionDoc[indice] = new GestionDocumentation(lexique, cheminDossier);
      gestionDoc[indice].setTitle(lexique.HostFieldGetData("DOC" + (indice >= 9 ? "" + (indice + 1) : "0" + (indice + 1))).trim());
    }
    
  }
  
  /**
   * Construit le chemin à partir des données envoyé par l'AS/400
   * @param hstfldrac
   * @param hstfldche
   *
   *          private void construitChemin(String hstfldrac, String hstfldche)
   *          {
   *          String chedoc = analyseRacine(lexique.HostFieldGetData(hstfldrac)) +
   *          (lexique.HostFieldGetData(hstfldche)).trim();
   * 
   *          lexique.ouvrirExplorer(chedoc, true);
   *          }
   */
  
  /**
   * Analyse la racine envoyé par l'AS/400
   * @param racine
   * @return
   *         
   *         private String analyseRacine(String racine)
   *         {
   *         if (racine == null) return "";
   *         racine = racine.trim();
   *         int pos = racine.indexOf(';');
   * 
   *         // On récupère la partie juste avant le point virgule sinon si pas de point virgule alors on renvoi la
   *         racine telle quelle
   *         if (pos != -1)
   *         racine = racine.substring(0, pos);
   * 
   *         // TODO à améliorer en fonction du poste client : WIN / UNIX
   *         if (racine.charAt(0) == '/')
   *         racine = "Z:\\";
   *         return racine;
   *         }
   */
  
  // -- Evènementiel ---
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    construitChemin(0, "RAC01", "CHE01");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    construitChemin(1, "RAC02", "CHE02");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    construitChemin(2, "RAC03", "CHE03");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    construitChemin(3, "RAC04", "CHE04");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    construitChemin(4, "RAC05", "CHE05");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    construitChemin(5, "RAC06", "CHE06");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    construitChemin(6, "RAC07", "CHE07");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    construitChemin(7, "RAC08", "CHE08");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    construitChemin(8, "RAC09", "CHE09");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    construitChemin(9, "RAC10", "CHE10");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
    // Permet de fermer les fenêtres de gestion de documents ouvertes
    for (int i = 0; i < gestionDoc.length; i++) {
      if (gestionDoc[i] != null) {
        gestionDoc[i].dispose();
      }
    }
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    OBJ_16 = new JButton();
    OBJ_17 = new JButton();
    OBJ_18 = new JButton();
    OBJ_19 = new JButton();
    OBJ_20 = new JButton();
    OBJ_21 = new JButton();
    OBJ_22 = new JButton();
    OBJ_23 = new JButton();
    OBJ_24 = new JButton();
    OBJ_25 = new JButton();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(655, 410));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 180));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setVerifyInputWhenFocusTarget(false);
              riMenu2.setVisible(false);
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Documents li\u00e9s"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setPreferredSize(new Dimension(450, 280));
            panel2.setName("panel2");
            panel2.setLayout(new VerticalLayout());

            //---- OBJ_16 ----
            OBJ_16.setText("@DOC01@");
            OBJ_16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_16.setPreferredSize(new Dimension(400, 32));
            OBJ_16.setName("OBJ_16");
            OBJ_16.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_16ActionPerformed(e);
              }
            });
            panel2.add(OBJ_16);

            //---- OBJ_17 ----
            OBJ_17.setText("@DOC02@");
            OBJ_17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_17.setPreferredSize(new Dimension(400, 32));
            OBJ_17.setName("OBJ_17");
            OBJ_17.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_17ActionPerformed(e);
              }
            });
            panel2.add(OBJ_17);

            //---- OBJ_18 ----
            OBJ_18.setText("@DOC03@");
            OBJ_18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_18.setPreferredSize(new Dimension(400, 32));
            OBJ_18.setName("OBJ_18");
            OBJ_18.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_18ActionPerformed(e);
              }
            });
            panel2.add(OBJ_18);

            //---- OBJ_19 ----
            OBJ_19.setText("@DOC04@");
            OBJ_19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_19.setPreferredSize(new Dimension(400, 32));
            OBJ_19.setName("OBJ_19");
            OBJ_19.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_19ActionPerformed(e);
              }
            });
            panel2.add(OBJ_19);

            //---- OBJ_20 ----
            OBJ_20.setText("@DOC05@");
            OBJ_20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_20.setPreferredSize(new Dimension(400, 32));
            OBJ_20.setName("OBJ_20");
            OBJ_20.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_20ActionPerformed(e);
              }
            });
            panel2.add(OBJ_20);

            //---- OBJ_21 ----
            OBJ_21.setText("@DOC06@");
            OBJ_21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_21.setPreferredSize(new Dimension(400, 32));
            OBJ_21.setName("OBJ_21");
            OBJ_21.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_21ActionPerformed(e);
              }
            });
            panel2.add(OBJ_21);

            //---- OBJ_22 ----
            OBJ_22.setText("@DOC07@");
            OBJ_22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_22.setPreferredSize(new Dimension(400, 32));
            OBJ_22.setName("OBJ_22");
            OBJ_22.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_22ActionPerformed(e);
              }
            });
            panel2.add(OBJ_22);

            //---- OBJ_23 ----
            OBJ_23.setText("@DOC08@");
            OBJ_23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_23.setPreferredSize(new Dimension(400, 32));
            OBJ_23.setName("OBJ_23");
            OBJ_23.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_23ActionPerformed(e);
              }
            });
            panel2.add(OBJ_23);

            //---- OBJ_24 ----
            OBJ_24.setText("@DOC09@");
            OBJ_24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_24.setPreferredSize(new Dimension(400, 32));
            OBJ_24.setName("OBJ_24");
            OBJ_24.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_24ActionPerformed(e);
              }
            });
            panel2.add(OBJ_24);

            //---- OBJ_25 ----
            OBJ_25.setText("@DOC10@");
            OBJ_25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_25.setPreferredSize(new Dimension(400, 32));
            OBJ_25.setName("OBJ_25");
            OBJ_25.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_25ActionPerformed(e);
              }
            });
            panel2.add(OBJ_25);
          }
          panel1.add(panel2);
          panel2.setBounds(6, 35, 443, 330);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 15, 455, 375);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      BTD.add(OBJ_9);
    }

    //======== riSousMenu6 ========
    {
      riSousMenu6.setName("riSousMenu6");

      //---- riSousMenu_bt6 ----
      riSousMenu_bt6.setText("Num\u00e9riser un document");
      riSousMenu_bt6.setName("riSousMenu_bt6");
      riSousMenu6.add(riSousMenu_bt6);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel2;
  private JButton OBJ_16;
  private JButton OBJ_17;
  private JButton OBJ_18;
  private JButton OBJ_19;
  private JButton OBJ_20;
  private JButton OBJ_21;
  private JButton OBJ_22;
  private JButton OBJ_23;
  private JButton OBJ_24;
  private JButton OBJ_25;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
