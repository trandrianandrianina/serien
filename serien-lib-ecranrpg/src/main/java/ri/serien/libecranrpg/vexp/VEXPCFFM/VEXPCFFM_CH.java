
package ri.serien.libecranrpg.vexp.VEXPCFFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VEXPCFFM_CH extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXPCFFM_CH(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(OBJ_8);
    
    // public boolean isDialog();
    // return isdialog;
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    this.setTitle("Confirmation");
    
    
    p_contenu.setBackground(Constantes.COULEUR_F1);
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void OBJ_6ActionPerformed() {
    lexique.HostFieldPutData("ACT", 0, "O");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_7ActionPerformed() {
    lexique.HostFieldPutData("ACT", 0, "R");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_8ActionPerformed() {
    lexique.HostFieldPutData("ACT", 0, "N");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    OBJ_5 = new JLabel();
    OBJ_9 = new JLabel();
    OBJ_8 = new JButton();
    OBJ_7 = new JButton();
    OBJ_6 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(285, 225));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setForeground(Color.black);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OBJ_5 ----
        OBJ_5.setText("Souhaitez vous enregistrer");
        OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_5.setFont(OBJ_5.getFont().deriveFont(OBJ_5.getFont().getStyle() | Font.BOLD, OBJ_5.getFont().getSize() + 4f));
        OBJ_5.setForeground(Color.white);
        OBJ_5.setName("OBJ_5");
        p_contenu.add(OBJ_5);
        OBJ_5.setBounds(0, 5, 285, 35);

        //---- OBJ_9 ----
        OBJ_9.setText("les modifications ?");
        OBJ_9.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_9.setFont(OBJ_9.getFont().deriveFont(OBJ_9.getFont().getStyle() | Font.BOLD, OBJ_9.getFont().getSize() + 4f));
        OBJ_9.setForeground(Color.white);
        OBJ_9.setName("OBJ_9");
        p_contenu.add(OBJ_9);
        OBJ_9.setBounds(0, 40, 285, 35);

        //---- OBJ_8 ----
        OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_8.setPreferredSize(new Dimension(120, 19));
        OBJ_8.setMinimumSize(new Dimension(120, 1));
        OBJ_8.setFont(new Font("sansserif", Font.BOLD, 14));
        OBJ_8.setIconTextGap(5);
        OBJ_8.setText("Sortir sans modifier");
        OBJ_8.setName("OBJ_8");
        OBJ_8.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_8ActionPerformed();
          }
        });
        p_contenu.add(OBJ_8);
        OBJ_8.setBounds(17, 170, 250, 40);

        //---- OBJ_7 ----
        OBJ_7.setText("Non, retour en modification");
        OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_7.setPreferredSize(new Dimension(120, 19));
        OBJ_7.setMinimumSize(new Dimension(120, 1));
        OBJ_7.setFont(new Font("sansserif", Font.BOLD, 14));
        OBJ_7.setIconTextGap(5);
        OBJ_7.setName("OBJ_7");
        OBJ_7.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_7ActionPerformed();
          }
        });
        p_contenu.add(OBJ_7);
        OBJ_7.setBounds(17, 130, 250, 40);

        //---- OBJ_6 ----
        OBJ_6.setText("Oui, enregistrer");
        OBJ_6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_6.setPreferredSize(new Dimension(110, 19));
        OBJ_6.setMinimumSize(new Dimension(110, 1));
        OBJ_6.setFont(OBJ_6.getFont().deriveFont(OBJ_6.getFont().getStyle() | Font.BOLD, OBJ_6.getFont().getSize() + 2f));
        OBJ_6.setIconTextGap(5);
        OBJ_6.setIcon(null);
        OBJ_6.setName("OBJ_6");
        OBJ_6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_6ActionPerformed();
          }
        });
        p_contenu.add(OBJ_6);
        OBJ_6.setBounds(17, 90, 250, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JLabel OBJ_5;
  private JLabel OBJ_9;
  private JButton OBJ_8;
  private JButton OBJ_7;
  private JButton OBJ_6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
