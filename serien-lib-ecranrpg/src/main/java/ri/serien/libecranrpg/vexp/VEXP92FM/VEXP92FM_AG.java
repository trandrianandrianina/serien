
package ri.serien.libecranrpg.vexp.VEXP92FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP92FM_AG extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP92FM_AG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNLI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_17.setVisible(lexique.isPresent("INDNLI"));
    NOMPGT.setVisible(lexique.isPresent("NOMPGT"));
    LIBPGT.setVisible(lexique.isPresent("LIBPGT"));
    PZ2.setVisible(lexique.isPresent("PZ2"));
    PZ1.setVisible(lexique.isPresent("PZ1"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    PZ1 = new XRiTextField();
    PZ2 = new XRiTextField();
    LIBPGT = new XRiTextField();
    OBJ_16 = new JLabel();
    NOMPGT = new XRiTextField();
    P_PnlOpts = new JPanel();
    OBJ_20 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_15 = new JLabel();
    OBJ_14 = new JLabel();
    OBJ_17 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_8 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(875, 220));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- PZ1 ----
          PZ1.setComponentPopupMenu(BTD);
          PZ1.setName("PZ1");
          p_recup.add(PZ1);
          PZ1.setBounds(20, 100, 650, PZ1.getPreferredSize().height);

          //---- PZ2 ----
          PZ2.setComponentPopupMenu(BTD);
          PZ2.setName("PZ2");
          p_recup.add(PZ2);
          PZ2.setBounds(20, 145, 650, PZ2.getPreferredSize().height);

          //---- LIBPGT ----
          LIBPGT.setComponentPopupMenu(BTD);
          LIBPGT.setName("LIBPGT");
          p_recup.add(LIBPGT);
          LIBPGT.setBounds(218, 54, 452, LIBPGT.getPreferredSize().height);

          //---- OBJ_16 ----
          OBJ_16.setText("Libell\u00e9 programme \u00e0 traiter");
          OBJ_16.setName("OBJ_16");
          p_recup.add(OBJ_16);
          OBJ_16.setBounds(225, 30, 182, 20);

          //---- NOMPGT ----
          NOMPGT.setComponentPopupMenu(BTD);
          NOMPGT.setName("NOMPGT");
          p_recup.add(NOMPGT);
          NOMPGT.setBounds(79, 54, 110, NOMPGT.getPreferredSize().height);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_20 ----
          OBJ_20.setText("Param\u00e8tre 1");
          OBJ_20.setName("OBJ_20");
          p_recup.add(OBJ_20);
          OBJ_20.setBounds(25, 80, 94, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("Param\u00e8tre 2");
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(25, 125, 94, 20);

          //---- OBJ_15 ----
          OBJ_15.setText("Programme");
          OBJ_15.setName("OBJ_15");
          p_recup.add(OBJ_15);
          OBJ_15.setBounds(85, 30, 92, 20);

          //---- OBJ_14 ----
          OBJ_14.setText("Ligne");
          OBJ_14.setName("OBJ_14");
          p_recup.add(OBJ_14);
          OBJ_14.setBounds(25, 30, 47, 20);

          //---- OBJ_17 ----
          OBJ_17.setText("@INDNLI@");
          OBJ_17.setName("OBJ_17");
          p_recup.add(OBJ_17);
          OBJ_17.setBounds(25, 58, 42, 21);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 5, 685, 185);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_8 ----
      OBJ_8.setText("Aide en ligne");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTD.add(OBJ_8);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField PZ1;
  private XRiTextField PZ2;
  private XRiTextField LIBPGT;
  private JLabel OBJ_16;
  private XRiTextField NOMPGT;
  private JPanel P_PnlOpts;
  private JLabel OBJ_20;
  private JLabel OBJ_22;
  private JLabel OBJ_15;
  private JLabel OBJ_14;
  private JLabel OBJ_17;
  private JPopupMenu BTD;
  private JMenuItem OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
