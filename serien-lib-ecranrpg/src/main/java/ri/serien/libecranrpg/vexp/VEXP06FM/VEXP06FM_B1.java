//$$david$$ ££04/01/11££ -> tests et modifs sur nouveau look

package ri.serien.libecranrpg.vexp.VEXP06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP06FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP06FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_46_OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEY@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    OBJ_46_OBJ_46.setVisible(lexique.isTrue("19"));
    OBJ_45_OBJ_45.setVisible(lexique.isTrue("19"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Domiciliation"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel3 = new JPanel();
    DODO2 = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    WIBAN1 = new XRiTextField();
    WIBAN2 = new XRiTextField();
    WIBAN3 = new XRiTextField();
    WIBAN4 = new XRiTextField();
    WIBAN5 = new XRiTextField();
    WIBAN6 = new XRiTextField();
    WIBAN7 = new XRiTextField();
    WIBAN8 = new XRiTextField();
    OBJ_46_OBJ_46 = new RiZoneSortie();
    OBJ_22_OBJ_23 = new JLabel();
    panel2 = new JPanel();
    DODO1 = new XRiTextField();
    OBJ_22_OBJ_22 = new JLabel();
    DOBQE = new XRiTextField();
    DOGUI = new XRiTextField();
    DOCPT = new XRiTextField();
    DORIB = new XRiTextField();
    OBJ_45_OBJ_45 = new RiZoneSortie();
    OBJ_22_OBJ_24 = new JLabel();
    OBJ_22_OBJ_25 = new JLabel();
    OBJ_22_OBJ_26 = new JLabel();
    OBJ_22_OBJ_27 = new JLabel();
    OBJ_22_OBJ_28 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1090, 195));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Domiciliation bancaire"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- DODO2 ----
            DODO2.setComponentPopupMenu(BTD);
            DODO2.setName("DODO2");
            panel3.add(DODO2);
            DODO2.setBounds(114, 21, 220, DODO2.getPreferredSize().height);

            //---- OBJ_40_OBJ_40 ----
            OBJ_40_OBJ_40.setText("IBAN");
            OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
            panel3.add(OBJ_40_OBJ_40);
            OBJ_40_OBJ_40.setBounds(355, 25, 56, 20);

            //---- WIBAN1 ----
            WIBAN1.setComponentPopupMenu(BTD);
            WIBAN1.setName("WIBAN1");
            panel3.add(WIBAN1);
            WIBAN1.setBounds(410, 21, 54, WIBAN1.getPreferredSize().height);

            //---- WIBAN2 ----
            WIBAN2.setComponentPopupMenu(BTD);
            WIBAN2.setName("WIBAN2");
            panel3.add(WIBAN2);
            WIBAN2.setBounds(462, 21, 54, WIBAN2.getPreferredSize().height);

            //---- WIBAN3 ----
            WIBAN3.setComponentPopupMenu(BTD);
            WIBAN3.setName("WIBAN3");
            panel3.add(WIBAN3);
            WIBAN3.setBounds(514, 21, 54, WIBAN3.getPreferredSize().height);

            //---- WIBAN4 ----
            WIBAN4.setComponentPopupMenu(BTD);
            WIBAN4.setName("WIBAN4");
            panel3.add(WIBAN4);
            WIBAN4.setBounds(566, 21, 54, WIBAN4.getPreferredSize().height);

            //---- WIBAN5 ----
            WIBAN5.setComponentPopupMenu(BTD);
            WIBAN5.setName("WIBAN5");
            panel3.add(WIBAN5);
            WIBAN5.setBounds(618, 21, 54, WIBAN5.getPreferredSize().height);

            //---- WIBAN6 ----
            WIBAN6.setComponentPopupMenu(BTD);
            WIBAN6.setName("WIBAN6");
            panel3.add(WIBAN6);
            WIBAN6.setBounds(670, 21, 54, WIBAN6.getPreferredSize().height);

            //---- WIBAN7 ----
            WIBAN7.setComponentPopupMenu(BTD);
            WIBAN7.setName("WIBAN7");
            panel3.add(WIBAN7);
            WIBAN7.setBounds(722, 21, 54, WIBAN7.getPreferredSize().height);

            //---- WIBAN8 ----
            WIBAN8.setComponentPopupMenu(BTD);
            WIBAN8.setName("WIBAN8");
            panel3.add(WIBAN8);
            WIBAN8.setBounds(775, 21, 28, WIBAN8.getPreferredSize().height);

            //---- OBJ_46_OBJ_46 ----
            OBJ_46_OBJ_46.setText("@UCLEY@");
            OBJ_46_OBJ_46.setForeground(Color.red);
            OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
            panel3.add(OBJ_46_OBJ_46);
            OBJ_46_OBJ_46.setBounds(820, 23, 25, OBJ_46_OBJ_46.getPreferredSize().height);

            //---- OBJ_22_OBJ_23 ----
            OBJ_22_OBJ_23.setText("Guichet ou SWIFT");
            OBJ_22_OBJ_23.setName("OBJ_22_OBJ_23");
            panel3.add(OBJ_22_OBJ_23);
            OBJ_22_OBJ_23.setBounds(5, 25, 110, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(15, 100, 875, 65);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- DODO1 ----
            DODO1.setComponentPopupMenu(BTD);
            DODO1.setName("DODO1");
            panel2.add(DODO1);
            DODO1.setBounds(114, 25, 220, DODO1.getPreferredSize().height);

            //---- OBJ_22_OBJ_22 ----
            OBJ_22_OBJ_22.setText("RIB");
            OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
            panel2.add(OBJ_22_OBJ_22);
            OBJ_22_OBJ_22.setBounds(355, 29, 55, 20);

            //---- DOBQE ----
            DOBQE.setComponentPopupMenu(BTD);
            DOBQE.setToolTipText("num\u00e9ro de banque");
            DOBQE.setName("DOBQE");
            panel2.add(DOBQE);
            DOBQE.setBounds(410, 25, 50, DOBQE.getPreferredSize().height);

            //---- DOGUI ----
            DOGUI.setComponentPopupMenu(BTD);
            DOGUI.setToolTipText("num\u00e9ro de guichet");
            DOGUI.setName("DOGUI");
            panel2.add(DOGUI);
            DOGUI.setBounds(462, 25, 50, DOGUI.getPreferredSize().height);

            //---- DOCPT ----
            DOCPT.setComponentPopupMenu(BTD);
            DOCPT.setToolTipText("num\u00e9ro de compte");
            DOCPT.setName("DOCPT");
            panel2.add(DOCPT);
            DOCPT.setBounds(514, 25, 121, DOCPT.getPreferredSize().height);

            //---- DORIB ----
            DORIB.setComponentPopupMenu(BTD);
            DORIB.setToolTipText("Cl\u00e9");
            DORIB.setName("DORIB");
            panel2.add(DORIB);
            DORIB.setBounds(670, 25, 28, DORIB.getPreferredSize().height);

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("@UCLEX@");
            OBJ_45_OBJ_45.setForeground(Color.red);
            OBJ_45_OBJ_45.setToolTipText("Cl\u00e9 r\u00e9elle");
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
            panel2.add(OBJ_45_OBJ_45);
            OBJ_45_OBJ_45.setBounds(820, 27, 30, OBJ_45_OBJ_45.getPreferredSize().height);

            //---- OBJ_22_OBJ_24 ----
            OBJ_22_OBJ_24.setText("Banque");
            OBJ_22_OBJ_24.setName("OBJ_22_OBJ_24");
            panel2.add(OBJ_22_OBJ_24);
            OBJ_22_OBJ_24.setBounds(5, 29, 110, 20);

            //---- OBJ_22_OBJ_25 ----
            OBJ_22_OBJ_25.setText("Banque");
            OBJ_22_OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_25.setFont(OBJ_22_OBJ_25.getFont().deriveFont(OBJ_22_OBJ_25.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_25.setName("OBJ_22_OBJ_25");
            panel2.add(OBJ_22_OBJ_25);
            OBJ_22_OBJ_25.setBounds(410, 5, 50, 20);

            //---- OBJ_22_OBJ_26 ----
            OBJ_22_OBJ_26.setText("Guichet");
            OBJ_22_OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_26.setFont(OBJ_22_OBJ_26.getFont().deriveFont(OBJ_22_OBJ_26.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_26.setName("OBJ_22_OBJ_26");
            panel2.add(OBJ_22_OBJ_26);
            OBJ_22_OBJ_26.setBounds(462, 5, 50, 20);

            //---- OBJ_22_OBJ_27 ----
            OBJ_22_OBJ_27.setText("Num\u00e9ro de compte");
            OBJ_22_OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_27.setFont(OBJ_22_OBJ_27.getFont().deriveFont(OBJ_22_OBJ_27.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_27.setName("OBJ_22_OBJ_27");
            panel2.add(OBJ_22_OBJ_27);
            OBJ_22_OBJ_27.setBounds(514, 5, 121, 20);

            //---- OBJ_22_OBJ_28 ----
            OBJ_22_OBJ_28.setText("Cl\u00e9");
            OBJ_22_OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_28.setFont(OBJ_22_OBJ_28.getFont().deriveFont(OBJ_22_OBJ_28.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_28.setName("OBJ_22_OBJ_28");
            panel2.add(OBJ_22_OBJ_28);
            OBJ_22_OBJ_28.setBounds(670, 5, 28, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(15, 30, 875, 65);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 904, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel3;
  private XRiTextField DODO2;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField WIBAN1;
  private XRiTextField WIBAN2;
  private XRiTextField WIBAN3;
  private XRiTextField WIBAN4;
  private XRiTextField WIBAN5;
  private XRiTextField WIBAN6;
  private XRiTextField WIBAN7;
  private XRiTextField WIBAN8;
  private RiZoneSortie OBJ_46_OBJ_46;
  private JLabel OBJ_22_OBJ_23;
  private JPanel panel2;
  private XRiTextField DODO1;
  private JLabel OBJ_22_OBJ_22;
  private XRiTextField DOBQE;
  private XRiTextField DOGUI;
  private XRiTextField DOCPT;
  private XRiTextField DORIB;
  private RiZoneSortie OBJ_45_OBJ_45;
  private JLabel OBJ_22_OBJ_24;
  private JLabel OBJ_22_OBJ_25;
  private JLabel OBJ_22_OBJ_26;
  private JLabel OBJ_22_OBJ_27;
  private JLabel OBJ_22_OBJ_28;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
