
package ri.serien.libecranrpg.vexp.VEXP45FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP45FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private URI uri = null;
  private Desktop desktop;
  
  public VEXP45FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    String mailerParametre = lexique.HostFieldGetData("OUTLOK");
    panel1.setVisible(mailerParametre.equalsIgnoreCase("0") || mailerParametre.trim().equalsIgnoreCase("")
        || mailerParametre.trim().equalsIgnoreCase("2"));
    panel2.setVisible(mailerParametre.equalsIgnoreCase("1"));
    panel3.setVisible(mailerParametre.equalsIgnoreCase("X"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void button_annOutlookActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void button_outlookActionPerformed(ActionEvent e) {
    ArrayList<String> dest = new ArrayList<String>(1);
    dest.add(lexique.HostFieldGetData("WTO"));
    ArrayList<String> destCC = new ArrayList<String>(4);
    destCC.add(lexique.HostFieldGetData("WCC1"));
    destCC.add(lexique.HostFieldGetData("WCC2"));
    destCC.add(lexique.HostFieldGetData("WCC3"));
    destCC.add(lexique.HostFieldGetData("WCC4"));
    String message = lexique.HostFieldGetData("WMES01") + lexique.HostFieldGetData("WMES02") + lexique.HostFieldGetData("WMES03")
        + lexique.HostFieldGetData("WMES04") + lexique.HostFieldGetData("WMES05") + lexique.HostFieldGetData("WMES06")
        + lexique.HostFieldGetData("WMES07");
    ArrayList<String> joint = new ArrayList<String>(5);
    if (!lexique.HostFieldGetData("WCH01").trim().equalsIgnoreCase("")) {
      joint.add(lexique.RecuperationFichier(lexique.HostFieldGetData("WCH01")));
    }
    if (!lexique.HostFieldGetData("WCH02").trim().equalsIgnoreCase("")) {
      joint.add(lexique.RecuperationFichier(lexique.HostFieldGetData("WCH02")));
    }
    if (!lexique.HostFieldGetData("WCH03").trim().equalsIgnoreCase("")) {
      joint.add(lexique.RecuperationFichier(lexique.HostFieldGetData("WCH03")));
    }
    if (!lexique.HostFieldGetData("WCH04").trim().equalsIgnoreCase("")) {
      joint.add(lexique.RecuperationFichier(lexique.HostFieldGetData("WCH04")));
    }
    if (!lexique.HostFieldGetData("WCH05").trim().equalsIgnoreCase("")) {
      joint.add(lexique.RecuperationFichier(lexique.HostFieldGetData("WCH05")));
    }
    
    // lexique.Mailto(1, new Mail(dest, destCC, lexique.HostFieldGetData("WSUJET"), message, joint, false), true);
    
    dest.clear();
    destCC.clear();
    joint.clear();
  }
  
  private void button_outlook2ActionPerformed(ActionEvent e) {
    if (Desktop.isDesktopSupported() && (desktop = Desktop.getDesktop()).isSupported(Desktop.Action.MAIL)) {
      String destinataire = lexique.HostFieldGetData("WTO").trim();
      String sujet = lexique.HostFieldGetData("WSUJET").trim();
      String joint = "";
      if (!lexique.HostFieldGetData("WCH01").trim().equalsIgnoreCase("")) {
        joint += (lexique.RecuperationFichier(lexique.HostFieldGetData("WCH01"))).trim();
      }
      if (!lexique.HostFieldGetData("WCH02").trim().equalsIgnoreCase("")) {
        joint += "," + (lexique.RecuperationFichier(lexique.HostFieldGetData("WCH02"))).trim();
      }
      if (!lexique.HostFieldGetData("WCH03").trim().equalsIgnoreCase("")) {
        joint += "," + (lexique.RecuperationFichier(lexique.HostFieldGetData("WCH03"))).trim();
      }
      if (!lexique.HostFieldGetData("WCH04").trim().equalsIgnoreCase("")) {
        joint += "," + (lexique.RecuperationFichier(lexique.HostFieldGetData("WCH04"))).trim();
      }
      if (!lexique.HostFieldGetData("WCH05").trim().equalsIgnoreCase("")) {
        joint += "," + (lexique.RecuperationFichier(lexique.HostFieldGetData("WCH05"))).trim();
      }
      String destCC = "";
      if (!lexique.HostFieldGetData("WCC1").trim().equalsIgnoreCase("")) {
        destCC += (lexique.HostFieldGetData("WCC1")).trim();
      }
      if (!lexique.HostFieldGetData("WCC2").trim().equalsIgnoreCase("")) {
        destCC += "," + (lexique.HostFieldGetData("WCC2")).trim();
      }
      if (!lexique.HostFieldGetData("WCC3").trim().equalsIgnoreCase("")) {
        destCC += "," + (lexique.HostFieldGetData("WCC3")).trim();
      }
      if (!lexique.HostFieldGetData("WCC4").trim().equalsIgnoreCase("")) {
        destCC += "," + (lexique.HostFieldGetData("WCC4")).trim();
      }
      String message = lexique.HostFieldGetData("WMES01") + lexique.HostFieldGetData("WMES02") + lexique.HostFieldGetData("WMES03")
          + lexique.HostFieldGetData("WMES04") + lexique.HostFieldGetData("WMES05") + lexique.HostFieldGetData("WMES06")
          + lexique.HostFieldGetData("WMES07");
      String mailURIStr = String.format("mailto:%s?subject=%s&cc=%s&body=%s&attachment=%s", filtreUri(destinataire), filtreUri(sujet),
          filtreUri(destCC), filtreUri(message), filtreUri(joint));
      // uri = new URI(mailURIStr);
    }
  }
  
  /*
   * filtre caractères blancs et caractères spéciaux de l'URI
   */
  public String filtreUri(String uriBrute) {
    String result;
    try {
      result = URLEncoder.encode(uriBrute, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!").replaceAll("\\%27", "'")
          .replaceAll("\\%28", "(").replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
    }
    catch (UnsupportedEncodingException e) {
      result = uriBrute;
    }
    
    return result;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_32_OBJ_32 = new JLabel();
    WPRF = new XRiTextField();
    OBJ_33_OBJ_33 = new JLabel();
    WETA = new XRiTextField();
    OBJ_33_OBJ_34 = new JLabel();
    OBJ_33_OBJ_35 = new JLabel();
    WDAT = new XRiCalendrier();
    WHR = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    button_outlook2 = new JButton();
    button_annOutlook2 = new JButton();
    panel2 = new JPanel();
    button_outlook = new JButton();
    button_annOutlook = new JButton();
    panel1 = new JPanel();
    WFROM = new XRiTextField();
    WTO = new XRiTextField();
    WCC1 = new XRiTextField();
    WCC2 = new XRiTextField();
    WCC3 = new XRiTextField();
    WCC4 = new XRiTextField();
    WSUJET = new XRiTextField();
    WMES01 = new XRiTextField();
    WMES02 = new XRiTextField();
    WMES03 = new XRiTextField();
    WMES04 = new XRiTextField();
    WMES05 = new XRiTextField();
    WMES06 = new XRiTextField();
    WMES07 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    WCH01 = new XRiTextField();
    WCH02 = new XRiTextField();
    WCH03 = new XRiTextField();
    WCH04 = new XRiTextField();
    WCH05 = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Email");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Alias");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          p_tete_gauche.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(5, 5, 85, 18);
          
          // ---- WPRF ----
          WPRF.setComponentPopupMenu(null);
          WPRF.setName("WPRF");
          p_tete_gauche.add(WPRF);
          WPRF.setBounds(85, 0, 164, WPRF.getPreferredSize().height);
          
          // ---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Etat");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          p_tete_gauche.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(295, 5, 95, 18);
          
          // ---- WETA ----
          WETA.setComponentPopupMenu(null);
          WETA.setName("WETA");
          p_tete_gauche.add(WETA);
          WETA.setBounds(390, 0, 54, WETA.getPreferredSize().height);
          
          // ---- OBJ_33_OBJ_34 ----
          OBJ_33_OBJ_34.setText("Date");
          OBJ_33_OBJ_34.setName("OBJ_33_OBJ_34");
          p_tete_gauche.add(OBJ_33_OBJ_34);
          OBJ_33_OBJ_34.setBounds(485, 5, 45, 18);
          
          // ---- OBJ_33_OBJ_35 ----
          OBJ_33_OBJ_35.setText("heure");
          OBJ_33_OBJ_35.setName("OBJ_33_OBJ_35");
          p_tete_gauche.add(OBJ_33_OBJ_35);
          OBJ_33_OBJ_35.setBounds(655, 5, 50, 18);
          
          // ---- WDAT ----
          WDAT.setName("WDAT");
          p_tete_gauche.add(WDAT);
          WDAT.setBounds(540, 0, 105, WDAT.getPreferredSize().height);
          
          // ---- WHR ----
          WHR.setName("WHR");
          p_tete_gauche.add(WHR);
          WHR.setBounds(710, 0, 65, WHR.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);
          
          // ======== panel3 ========
          {
            panel3.setBackground(new Color(102, 102, 102));
            panel3.setName("panel3");
            panel3.setLayout(null);
            
            // ---- button_outlook2 ----
            button_outlook2.setText("Envoyer vers votre logiciel de mails");
            button_outlook2.setFont(button_outlook2.getFont().deriveFont(button_outlook2.getFont().getStyle() | Font.BOLD,
                button_outlook2.getFont().getSize() + 3f));
            button_outlook2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            button_outlook2.setName("button_outlook2");
            button_outlook2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button_outlook2ActionPerformed(e);
              }
            });
            panel3.add(button_outlook2);
            button_outlook2.setBounds(25, 25, 330, 40);
            
            // ---- button_annOutlook2 ----
            button_annOutlook2.setText("Annuler");
            button_annOutlook2.setFont(button_annOutlook2.getFont().deriveFont(button_annOutlook2.getFont().getStyle() | Font.BOLD,
                button_annOutlook2.getFont().getSize() + 3f));
            button_annOutlook2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            button_annOutlook2.setName("button_annOutlook2");
            button_annOutlook2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button_annOutlookActionPerformed(e);
              }
            });
            panel3.add(button_annOutlook2);
            button_annOutlook2.setBounds(25, 70, 330, 40);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(260, 150, 385, 130);
          
          // ======== panel2 ========
          {
            panel2.setBackground(new Color(102, 102, 102));
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- button_outlook ----
            button_outlook.setText("Envoyer vers Outlook");
            button_outlook.setFont(button_outlook.getFont().deriveFont(button_outlook.getFont().getStyle() | Font.BOLD,
                button_outlook.getFont().getSize() + 3f));
            button_outlook.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            button_outlook.setName("button_outlook");
            button_outlook.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button_outlookActionPerformed(e);
              }
            });
            panel2.add(button_outlook);
            button_outlook.setBounds(25, 25, 330, 40);
            
            // ---- button_annOutlook ----
            button_annOutlook.setText("Annuler");
            button_annOutlook.setFont(button_annOutlook.getFont().deriveFont(button_annOutlook.getFont().getStyle() | Font.BOLD,
                button_annOutlook.getFont().getSize() + 3f));
            button_annOutlook.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            button_annOutlook.setName("button_annOutlook");
            button_annOutlook.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button_annOutlookActionPerformed(e);
              }
            });
            panel2.add(button_annOutlook);
            button_annOutlook.setBounds(25, 70, 330, 40);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(260, 200, 385, 130);
          
          // ======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- WFROM ----
            WFROM.setName("WFROM");
            panel1.add(WFROM);
            WFROM.setBounds(95, 5, 770, WFROM.getPreferredSize().height);
            
            // ---- WTO ----
            WTO.setName("WTO");
            panel1.add(WTO);
            WTO.setBounds(95, 35, 770, WTO.getPreferredSize().height);
            
            // ---- WCC1 ----
            WCC1.setName("WCC1");
            panel1.add(WCC1);
            WCC1.setBounds(95, 60, 770, WCC1.getPreferredSize().height);
            
            // ---- WCC2 ----
            WCC2.setName("WCC2");
            panel1.add(WCC2);
            WCC2.setBounds(95, 85, 770, WCC2.getPreferredSize().height);
            
            // ---- WCC3 ----
            WCC3.setName("WCC3");
            panel1.add(WCC3);
            WCC3.setBounds(95, 110, 770, WCC3.getPreferredSize().height);
            
            // ---- WCC4 ----
            WCC4.setName("WCC4");
            panel1.add(WCC4);
            WCC4.setBounds(95, 135, 770, WCC4.getPreferredSize().height);
            
            // ---- WSUJET ----
            WSUJET.setName("WSUJET");
            panel1.add(WSUJET);
            WSUJET.setBounds(95, 190, 770, WSUJET.getPreferredSize().height);
            
            // ---- WMES01 ----
            WMES01.setName("WMES01");
            panel1.add(WMES01);
            WMES01.setBounds(15, 225, 850, WMES01.getPreferredSize().height);
            
            // ---- WMES02 ----
            WMES02.setName("WMES02");
            panel1.add(WMES02);
            WMES02.setBounds(15, 250, 850, WMES02.getPreferredSize().height);
            
            // ---- WMES03 ----
            WMES03.setName("WMES03");
            panel1.add(WMES03);
            WMES03.setBounds(15, 275, 850, WMES03.getPreferredSize().height);
            
            // ---- WMES04 ----
            WMES04.setName("WMES04");
            panel1.add(WMES04);
            WMES04.setBounds(15, 300, 850, WMES04.getPreferredSize().height);
            
            // ---- WMES05 ----
            WMES05.setName("WMES05");
            panel1.add(WMES05);
            WMES05.setBounds(15, 325, 850, WMES05.getPreferredSize().height);
            
            // ---- WMES06 ----
            WMES06.setName("WMES06");
            panel1.add(WMES06);
            WMES06.setBounds(15, 350, 850, WMES06.getPreferredSize().height);
            
            // ---- WMES07 ----
            WMES07.setName("WMES07");
            panel1.add(WMES07);
            WMES07.setBounds(15, 375, 850, WMES07.getPreferredSize().height);
            
            // ---- label1 ----
            label1.setText("Exp\u00e9diteur");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(15, 5, 80, 28);
            
            // ---- label2 ----
            label2.setText("Destinataire");
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(15, 35, 80, 28);
            
            // ---- label3 ----
            label3.setText("Copie \u00e0");
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(15, 60, 80, 28);
            
            // ---- label4 ----
            label4.setText("Copie \u00e0");
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(15, 85, 80, 28);
            
            // ---- label5 ----
            label5.setText("Copie \u00e0");
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(15, 110, 80, 28);
            
            // ---- label6 ----
            label6.setText("Copie \u00e0");
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(15, 135, 80, 28);
            
            // ---- label7 ----
            label7.setText("Sujet");
            label7.setName("label7");
            panel1.add(label7);
            label7.setBounds(15, 190, 80, 28);
            
            // ---- label8 ----
            label8.setText("Pi\u00e8ces jointes");
            label8.setName("label8");
            panel1.add(label8);
            label8.setBounds(15, 415, 80, 28);
            
            // ---- WCH01 ----
            WCH01.setName("WCH01");
            panel1.add(WCH01);
            WCH01.setBounds(145, 415, 720, WCH01.getPreferredSize().height);
            
            // ---- WCH02 ----
            WCH02.setName("WCH02");
            panel1.add(WCH02);
            WCH02.setBounds(145, 440, 720, WCH02.getPreferredSize().height);
            
            // ---- WCH03 ----
            WCH03.setName("WCH03");
            panel1.add(WCH03);
            WCH03.setBounds(145, 465, 720, WCH03.getPreferredSize().height);
            
            // ---- WCH04 ----
            WCH04.setName("WCH04");
            panel1.add(WCH04);
            WCH04.setBounds(145, 490, 720, WCH04.getPreferredSize().height);
            
            // ---- WCH05 ----
            WCH05.setName("WCH05");
            panel1.add(WCH05);
            WCH05.setBounds(145, 515, 720, WCH05.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(7, 7, 886, 566);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField WPRF;
  private JLabel OBJ_33_OBJ_33;
  private XRiTextField WETA;
  private JLabel OBJ_33_OBJ_34;
  private JLabel OBJ_33_OBJ_35;
  private XRiCalendrier WDAT;
  private XRiTextField WHR;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private JButton button_outlook2;
  private JButton button_annOutlook2;
  private JPanel panel2;
  private JButton button_outlook;
  private JButton button_annOutlook;
  private JPanel panel1;
  private XRiTextField WFROM;
  private XRiTextField WTO;
  private XRiTextField WCC1;
  private XRiTextField WCC2;
  private XRiTextField WCC3;
  private XRiTextField WCC4;
  private XRiTextField WSUJET;
  private XRiTextField WMES01;
  private XRiTextField WMES02;
  private XRiTextField WMES03;
  private XRiTextField WMES04;
  private XRiTextField WMES05;
  private XRiTextField WMES06;
  private XRiTextField WMES07;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private XRiTextField WCH01;
  private XRiTextField WCH02;
  private XRiTextField WCH03;
  private XRiTextField WCH04;
  private XRiTextField WCH05;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
