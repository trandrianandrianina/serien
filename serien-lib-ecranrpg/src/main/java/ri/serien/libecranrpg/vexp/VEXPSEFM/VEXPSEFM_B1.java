
package ri.serien.libecranrpg.vexp.VEXPSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXPSEFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VEXPSEFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    
    V06F.setVisible(lexique.isPresent("V06F"));
    SET007.setVisible(lexique.isPresent("SET007"));
    SET006.setVisible(lexique.isPresent("SET006"));
    SET005.setVisible(lexique.isPresent("SET005"));
    SET004.setVisible(lexique.isPresent("SET004"));
    SET003.setEnabled(lexique.isPresent("SET003"));
    SET002.setEnabled(lexique.isPresent("SET002"));
    SET001.setEnabled(lexique.isPresent("SET001"));
    OBJ_81.setVisible(lexique.isPresent("WPAGE"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    INDUSR.setVisible(lexique.isPresent("INDUSR"));
    OBJ_51.setVisible(lexique.isPresent("DGNOM"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_51 = new RiZoneSortie();
    INDUSR = new RiZoneSortie();
    OBJ_38 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_39 = new JLabel();
    p_tete_droite = new JPanel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_61 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_62 = new JLabel();
    SET001 = new XRiTextField();
    SET002 = new XRiTextField();
    SET003 = new XRiTextField();
    SET004 = new XRiTextField();
    SET005 = new XRiTextField();
    SET006 = new XRiTextField();
    SET007 = new XRiTextField();
    OBJ_60 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_50 = new SNBoutonLeger();
    V06F = new XRiTextField();
    OBJ_80 = new JLabel();
    OBJ_81 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 E.X.P.");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_51 ----
          OBJ_51.setText("@DGNOM@");
          OBJ_51.setOpaque(false);
          OBJ_51.setName("OBJ_51");
          p_tete_gauche.add(OBJ_51);
          OBJ_51.setBounds(365, 0, 339, OBJ_51.getPreferredSize().height);

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setOpaque(false);
          INDUSR.setText("@INDUSR@");
          INDUSR.setName("INDUSR");
          p_tete_gauche.add(INDUSR);
          INDUSR.setBounds(80, 0, 110, INDUSR.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Utilisateur");
          OBJ_38.setName("OBJ_38");
          p_tete_gauche.add(OBJ_38);
          OBJ_38.setBounds(5, 2, 75, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(320, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Etablissement");
          OBJ_39.setName("OBJ_39");
          p_tete_gauche.add(OBJ_39);
          OBJ_39.setBounds(220, 2, 95, 20);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Tri");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Etablissement/utilisateur");
              riSousMenu_bt6.setToolTipText("Tri par \u00e9tablissement ou code utilisateur");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(480, 400));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (E.X.P)");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- OBJ_61 ----
          OBJ_61.setText("Sauvegarde et Restauration");
          OBJ_61.setName("OBJ_61");
          xTitledPanel1ContentContainer.add(OBJ_61);
          OBJ_61.setBounds(65, 209, 205, 20);

          //---- OBJ_71 ----
          OBJ_71.setText("Mise \u00e0 jour de S\u00e9rie M par Internet");
          OBJ_71.setName("OBJ_71");
          xTitledPanel1ContentContainer.add(OBJ_71);
          OBJ_71.setBounds(65, 179, 205, 20);

          //---- OBJ_65 ----
          OBJ_65.setText("Gestion des profils utilisateurs web");
          OBJ_65.setName("OBJ_65");
          xTitledPanel1ContentContainer.add(OBJ_65);
          OBJ_65.setBounds(65, 89, 205, 20);

          //---- OBJ_59 ----
          OBJ_59.setText("Gestion des devises");
          OBJ_59.setName("OBJ_59");
          xTitledPanel1ContentContainer.add(OBJ_59);
          OBJ_59.setBounds(65, 29, 205, 20);

          //---- OBJ_63 ----
          OBJ_63.setText("Historique des devises");
          OBJ_63.setName("OBJ_63");
          xTitledPanel1ContentContainer.add(OBJ_63);
          OBJ_63.setBounds(65, 59, 205, 20);

          //---- OBJ_67 ----
          OBJ_67.setText("Gestion des communes");
          OBJ_67.setName("OBJ_67");
          xTitledPanel1ContentContainer.add(OBJ_67);
          OBJ_67.setBounds(65, 119, 205, 20);

          //---- OBJ_69 ----
          OBJ_69.setText("Gestion des mails");
          OBJ_69.setName("OBJ_69");
          xTitledPanel1ContentContainer.add(OBJ_69);
          OBJ_69.setBounds(65, 149, 205, 20);

          //---- OBJ_62 ----
          OBJ_62.setText("07");
          OBJ_62.setName("OBJ_62");
          xTitledPanel1ContentContainer.add(OBJ_62);
          OBJ_62.setBounds(25, 209, 21, 20);

          //---- SET001 ----
          SET001.setComponentPopupMenu(BTD);
          SET001.setName("SET001");
          xTitledPanel1ContentContainer.add(SET001);
          SET001.setBounds(280, 25, 20, SET001.getPreferredSize().height);

          //---- SET002 ----
          SET002.setComponentPopupMenu(BTD);
          SET002.setName("SET002");
          xTitledPanel1ContentContainer.add(SET002);
          SET002.setBounds(280, 55, 20, SET002.getPreferredSize().height);

          //---- SET003 ----
          SET003.setComponentPopupMenu(BTD);
          SET003.setName("SET003");
          xTitledPanel1ContentContainer.add(SET003);
          SET003.setBounds(280, 85, 20, SET003.getPreferredSize().height);

          //---- SET004 ----
          SET004.setComponentPopupMenu(BTD);
          SET004.setName("SET004");
          xTitledPanel1ContentContainer.add(SET004);
          SET004.setBounds(280, 115, 20, SET004.getPreferredSize().height);

          //---- SET005 ----
          SET005.setComponentPopupMenu(BTD);
          SET005.setName("SET005");
          xTitledPanel1ContentContainer.add(SET005);
          SET005.setBounds(280, 145, 20, SET005.getPreferredSize().height);

          //---- SET006 ----
          SET006.setComponentPopupMenu(BTD);
          SET006.setName("SET006");
          xTitledPanel1ContentContainer.add(SET006);
          SET006.setBounds(280, 175, 20, SET006.getPreferredSize().height);

          //---- SET007 ----
          SET007.setComponentPopupMenu(BTD);
          SET007.setName("SET007");
          xTitledPanel1ContentContainer.add(SET007);
          SET007.setBounds(280, 205, 20, SET007.getPreferredSize().height);

          //---- OBJ_60 ----
          OBJ_60.setText("01");
          OBJ_60.setName("OBJ_60");
          xTitledPanel1ContentContainer.add(OBJ_60);
          OBJ_60.setBounds(25, 31, 21, 16);

          //---- OBJ_64 ----
          OBJ_64.setText("02");
          OBJ_64.setName("OBJ_64");
          xTitledPanel1ContentContainer.add(OBJ_64);
          OBJ_64.setBounds(25, 61, 21, 16);

          //---- OBJ_66 ----
          OBJ_66.setText("03");
          OBJ_66.setName("OBJ_66");
          xTitledPanel1ContentContainer.add(OBJ_66);
          OBJ_66.setBounds(25, 91, 21, 16);

          //---- OBJ_68 ----
          OBJ_68.setText("04");
          OBJ_68.setName("OBJ_68");
          xTitledPanel1ContentContainer.add(OBJ_68);
          OBJ_68.setBounds(25, 121, 21, 16);

          //---- OBJ_70 ----
          OBJ_70.setText("05");
          OBJ_70.setName("OBJ_70");
          xTitledPanel1ContentContainer.add(OBJ_70);
          OBJ_70.setBounds(25, 151, 21, 16);

          //---- OBJ_78 ----
          OBJ_78.setText("06");
          OBJ_78.setName("OBJ_78");
          xTitledPanel1ContentContainer.add(OBJ_78);
          OBJ_78.setBounds(25, 181, 21, 16);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(40, 30, 400, 290);

        //---- OBJ_50 ----
        OBJ_50.setText("Aller \u00e0 la page");
        OBJ_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_50.setName("OBJ_50");
        OBJ_50.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_50ActionPerformed(e);
          }
        });
        pnlContenu.add(OBJ_50);
        OBJ_50.setBounds(new Rectangle(new Point(220, 335), OBJ_50.getPreferredSize()));

        //---- V06F ----
        V06F.setComponentPopupMenu(BTD);
        V06F.setName("V06F");
        pnlContenu.add(V06F);
        V06F.setBounds(365, 335, 30, V06F.getPreferredSize().height);

        //---- OBJ_80 ----
        OBJ_80.setText("Page");
        OBJ_80.setName("OBJ_80");
        pnlContenu.add(OBJ_80);
        OBJ_80.setBounds(45, 337, 40, 20);

        //---- OBJ_81 ----
        OBJ_81.setText("@WPAGE@");
        OBJ_81.setName("OBJ_81");
        pnlContenu.add(OBJ_81);
        OBJ_81.setBounds(90, 335, 34, OBJ_81.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie OBJ_51;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_38;
  private RiZoneSortie INDETB;
  private JLabel OBJ_39;
  private JPanel p_tete_droite;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelContenu pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_61;
  private JLabel OBJ_71;
  private JLabel OBJ_65;
  private JLabel OBJ_59;
  private JLabel OBJ_63;
  private JLabel OBJ_67;
  private JLabel OBJ_69;
  private JLabel OBJ_62;
  private XRiTextField SET001;
  private XRiTextField SET002;
  private XRiTextField SET003;
  private XRiTextField SET004;
  private XRiTextField SET005;
  private XRiTextField SET006;
  private XRiTextField SET007;
  private JLabel OBJ_60;
  private JLabel OBJ_64;
  private JLabel OBJ_66;
  private JLabel OBJ_68;
  private JLabel OBJ_70;
  private JLabel OBJ_78;
  private SNBoutonLeger OBJ_50;
  private XRiTextField V06F;
  private JLabel OBJ_80;
  private RiZoneSortie OBJ_81;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
