//$$david$$ ££05/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vexp.VEXP04FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiMailto;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VEXP04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  // TODO declarations classe spécifiques...
  private String[] RLIN3_Value = { " ", "1", "2", };
  private String[] RLIN3_Title = { "Aucune", "Par mail", "par SMS" };
  
  // boutons
  private final static String CONSULTER = "Consulter";
  private final static String MODIFIER = "Modifier";
  private final static String BOUTON_EMAIL = "Composer E-Mail";
  private final static String BOUTON_ACCES_TIERS = "Accéder tiers";
  private final static String BOUTON_VUE_OPTIONS = "Voir options";
  private final static String BOUTON_EXTENTIONS = "Voir extentions";
  private final static String BOUTON_BLOC_NOTES = "Bloc-notes";
  private final static String BOUTON_BLOC_NOTES_COMPLET = "Bloc-notes complet";
  
  public VEXP04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    RLIN1.setValeursSelection("P", "");
    RLIN3.setValeurs(RLIN3_Value, RLIN3_Title);
    REAAPP.setValeursSelection("1", "");
    REASPA.setValeursSelection("1", "");
    REIN1.setValeursSelection("1", "");
    REIN4.setValeursSelection("1", "");
    RWACC.setValeursSelection("W", " ");
    RWIN1.setValeursSelection("1", " ");
    RWIN2.setValeursSelection("1", " ");
    RWIN3.setValeursSelection("1", " ");
    RWIN4.setValeursSelection("1", " ");
    RWIN5.setValeursSelection("1", " ");
    RLIN4.setValeursSelection("1", "");
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(CONSULTER, 'v', false);
    snBarreBouton.ajouterBouton(MODIFIER, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_EMAIL, 'e', true);
    snBarreBouton.ajouterBouton(BOUTON_ACCES_TIERS, 't', true);
    snBarreBouton.ajouterBouton(BOUTON_VUE_OPTIONS, 'o', true);
    snBarreBouton.ajouterBouton(BOUTON_EXTENTIONS, 'x', true);
    snBarreBouton.ajouterBouton(BOUTON_BLOC_NOTES, 'b', true);
    snBarreBouton.ajouterBouton(BOUTON_BLOC_NOTES_COMPLET, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_75_OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    OBJ_76_OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    OBJ_78_OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    OBJ_80_OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    OBJ_82_OBJ_82.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Visibilité des composants
    WTIE1.setVisible(lexique.isTrue("23"));
    pnlZP.setVisible(lexique.isPresent("RETOP1"));
    pnlWebshop.setVisible(lexique.isTrue("36"));
    
    // Boutons
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    snBarreBouton.activerBouton(CONSULTER, !isConsultation);
    snBarreBouton.activerBouton(MODIFIER, isConsultation);
    
    // Visibilité des boutons
    
    if (lexique.isTrue("52")) {
      RWIN1.setEnabled(RWACC.isSelected());
      RWIN2.setEnabled(RWACC.isSelected());
      RWIN3.setEnabled(RWACC.isSelected());
      RWIN4.setEnabled(RWACC.isSelected());
      RWIN5.setEnabled(RWACC.isSelected());
      RLIN4.setEnabled(RWACC.isSelected());
    }
    
    // Affichage suivant le contexte : client/fournisseur/prospect
    if (lexique.HostFieldGetData("RENOM").trim().equals("") && lexique.HostFieldGetData("REPRE").trim().equals("")
        && !lexique.isTrue("51") && !lexique.HostFieldGetData("REPAC").trim().equals("")) {
      RENOM.setText(lexique.HostFieldGetData("REPAC"));
    }
    
    if (lexique.isTrue("21")) {
      lbClient.setText("Client");
    }
    else if (lexique.isTrue("22")) {
      lbClient.setText("Prospec");
    }
    else if (lexique.isTrue("23")) {
      lbClient.setText("Fournisseur");
    }
    else {
      lbClient.setVisible(false);
    }
    
    setTitle("Contacts");
    
    repaint();
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void envoiMail() {
    if (Desktop.isDesktopSupported()) {
      if (Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
        try {
          Desktop.getDesktop().mail(new URI("mailto:" + lexique.HostFieldGetData("RENET1").trim()));
          
        }
        catch (IOException e1) {
          DialogueErreur.afficher(e1);
        }
        catch (URISyntaxException e2) {
          DialogueErreur.afficher(e2);
        }
      }
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDI.getInvoker().getName());
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTDI.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void RWACCActionPerformed(ActionEvent e) {
    RWIN1.setEnabled(RWACC.isSelected());
    RWIN2.setEnabled(RWACC.isSelected());
    RWIN3.setEnabled(RWACC.isSelected());
    RWIN4.setEnabled(RWACC.isSelected());
    RWIN5.setEnabled(RWACC.isSelected());
    RLIN4.setEnabled(RWACC.isSelected());
  }
  
  /**
   * Actions des boutons
   */
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(CONSULTER)) {
        lexique.HostScreenSendKey(this, "F15");
      }
      else if (pSNBouton.isBouton(MODIFIER)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_EMAIL)) {
        envoiMail();
      }
      else if (pSNBouton.isBouton(BOUTON_ACCES_TIERS)) {
        lexique.HostScreenSendKey(this, "F20");
      }
      else if (pSNBouton.isBouton(BOUTON_VUE_OPTIONS)) {
        lexique.HostScreenSendKey(this, "F19");
      }
      else if (pSNBouton.isBouton(BOUTON_EXTENTIONS)) {
        lexique.HostScreenSendKey(this, "F24");
      }
      else if (pSNBouton.isBouton(BOUTON_BLOC_NOTES)) {
        lexique.HostScreenSendKey(this, "F22");
      }
      else if (pSNBouton.isBouton(BOUTON_BLOC_NOTES_COMPLET)) {
        lexique.HostScreenSendKey(this, "F23");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlContact = new SNPanelTitre();
    RLIN1 = new XRiCheckBox();
    lbNom2 = new SNLabelChamp();
    RECIV = new XRiTextField();
    lbNom = new SNLabelChamp();
    RENOM = new XRiTextField();
    lbPrenom = new SNLabelChamp();
    REPRE = new XRiTextField();
    lbClassement1 = new SNLabelChamp();
    RECL1 = new XRiTextField();
    lbClassement2 = new SNLabelChamp();
    RECL2 = new XRiTextField();
    lbAlias = new SNLabelChamp();
    REPRF = new XRiTextField();
    lbObservation = new SNLabelChamp();
    REOBS = new XRiTextField();
    lbTelephone = new SNLabelChamp();
    RETEL = new XRiTextField();
    lbPoste = new SNLabelChamp();
    REPOS = new XRiTextField();
    lbTelephone2 = new SNLabelChamp();
    RETEL2 = new XRiTextField();
    lbFax = new SNLabelChamp();
    REFAX = new XRiTextField();
    lbFonction = new SNLabelChamp();
    RECAT = new XRiTextField();
    lbEmail = new SNLabelChamp();
    RENET1 = new XRiMailto();
    lbServeurMail = new SNLabelChamp();
    WZSRVM = new XRiTextField();
    lbGencod = new SNLabelChamp();
    WZGCD = new XRiTextField();
    lbExtention = new SNLabelChamp();
    RETYZP = new XRiTextField();
    pnlOptions = new SNPanel();
    REASPA = new XRiCheckBox();
    REAAPP = new XRiCheckBox();
    REIN1 = new XRiCheckBox();
    REIN4 = new XRiCheckBox();
    lbAlerteWorkflow = new SNLabelChamp();
    RLIN3 = new XRiComboBox();
    pnlDroite = new SNPanel();
    pnlIdentification = new SNPanelTitre();
    lbClient = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    WTIE1 = new XRiTextField();
    WTIE2 = new XRiTextField();
    WTIE3 = new XRiTextField();
    lbNom3 = new JLabel();
    WZNOM = new XRiTextField();
    lbComplementNom = new JLabel();
    WZCPL = new XRiTextField();
    lbLocalisation = new JLabel();
    WZRUE = new XRiTextField();
    lbRue = new JLabel();
    WZLOC = new XRiTextField();
    lbCodePostal = new JLabel();
    WZCDP = new XRiTextField();
    lbVille = new JLabel();
    WZVIL = new XRiTextField();
    lbPays = new JLabel();
    WZPAY = new XRiTextField();
    pnlWebshop = new SNPanelTitre();
    RWACC = new XRiCheckBox();
    RWIN1 = new XRiCheckBox();
    RWIN2 = new XRiCheckBox();
    RWIN5 = new XRiCheckBox();
    RWIN3 = new XRiCheckBox();
    RWIN4 = new XRiCheckBox();
    RLIN4 = new XRiCheckBox();
    pnlZP = new SNPanelTitre();
    OBJ_75_OBJ_75 = new JLabel();
    RETOP1 = new XRiTextField();
    OBJ_76_OBJ_76 = new JLabel();
    RETOP2 = new XRiTextField();
    OBJ_78_OBJ_78 = new JLabel();
    RETOP3 = new XRiTextField();
    OBJ_80_OBJ_80 = new JLabel();
    RETOP4 = new XRiTextField();
    OBJ_82_OBJ_82 = new JLabel();
    RETOP5 = new XRiTextField();
    BTDI = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1175, 755));
    setPreferredSize(new Dimension(1175, 755));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridLayout(1, 2));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlContact ========
          {
            pnlContact.setTitre("Contact");
            pnlContact.setName("pnlContact");
            pnlContact.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlContact.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlContact.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlContact.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlContact.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- RLIN1 ----
            RLIN1.setText("Contact principal");
            RLIN1.setComponentPopupMenu(BTD);
            RLIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RLIN1.setFont(new Font("sansserif", Font.PLAIN, 14));
            RLIN1.setName("RLIN1");
            pnlContact.add(RLIN1, new GridBagConstraints(0, 0, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNom2 ----
            lbNom2.setText("Civilit\u00e9");
            lbNom2.setName("lbNom2");
            pnlContact.add(lbNom2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RECIV ----
            RECIV.setComponentPopupMenu(BTDI);
            RECIV.setMinimumSize(new Dimension(50, 30));
            RECIV.setPreferredSize(new Dimension(50, 30));
            RECIV.setFont(new Font("sansserif", Font.PLAIN, 14));
            RECIV.setName("RECIV");
            pnlContact.add(RECIV, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNom ----
            lbNom.setText("Nom");
            lbNom.setName("lbNom");
            pnlContact.add(lbNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RENOM ----
            RENOM.setComponentPopupMenu(BTD);
            RENOM.setFont(new Font("sansserif", Font.BOLD, 14));
            RENOM.setPreferredSize(new Dimension(310, 30));
            RENOM.setMinimumSize(new Dimension(310, 30));
            RENOM.setName("RENOM");
            pnlContact.add(RENOM, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPrenom ----
            lbPrenom.setText("Pr\u00e9nom");
            lbPrenom.setName("lbPrenom");
            pnlContact.add(lbPrenom, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPRE ----
            REPRE.setName("REPRE");
            pnlContact.add(REPRE, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClassement1 ----
            lbClassement1.setText("Classement 1");
            lbClassement1.setName("lbClassement1");
            pnlContact.add(lbClassement1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RECL1 ----
            RECL1.setComponentPopupMenu(BTD);
            RECL1.setMinimumSize(new Dimension(160, 30));
            RECL1.setPreferredSize(new Dimension(160, 30));
            RECL1.setName("RECL1");
            pnlContact.add(RECL1, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClassement2 ----
            lbClassement2.setText("Classement 2");
            lbClassement2.setName("lbClassement2");
            pnlContact.add(lbClassement2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RECL2 ----
            RECL2.setComponentPopupMenu(BTD);
            RECL2.setMinimumSize(new Dimension(160, 30));
            RECL2.setPreferredSize(new Dimension(160, 30));
            RECL2.setName("RECL2");
            pnlContact.add(RECL2, new GridBagConstraints(1, 5, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAlias ----
            lbAlias.setText("Alias");
            lbAlias.setName("lbAlias");
            pnlContact.add(lbAlias, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPRF ----
            REPRF.setMinimumSize(new Dimension(160, 30));
            REPRF.setPreferredSize(new Dimension(160, 30));
            REPRF.setName("REPRF");
            pnlContact.add(REPRF, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbObservation ----
            lbObservation.setText("Observation");
            lbObservation.setName("lbObservation");
            pnlContact.add(lbObservation, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REOBS ----
            REOBS.setComponentPopupMenu(BTD);
            REOBS.setName("REOBS");
            pnlContact.add(REOBS, new GridBagConstraints(1, 7, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTelephone ----
            lbTelephone.setText("T\u00e9l\u00e9phone");
            lbTelephone.setName("lbTelephone");
            pnlContact.add(lbTelephone, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RETEL ----
            RETEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
            RETEL.setComponentPopupMenu(BTD);
            RETEL.setMinimumSize(new Dimension(170, 30));
            RETEL.setPreferredSize(new Dimension(170, 30));
            RETEL.setName("RETEL");
            pnlContact.add(RETEL, new GridBagConstraints(1, 8, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPoste ----
            lbPoste.setText("Poste");
            lbPoste.setName("lbPoste");
            pnlContact.add(lbPoste, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REPOS ----
            REPOS.setToolTipText("Num\u00e9ro de poste");
            REPOS.setComponentPopupMenu(BTD);
            REPOS.setPreferredSize(new Dimension(60, 30));
            REPOS.setMinimumSize(new Dimension(60, 30));
            REPOS.setName("REPOS");
            pnlContact.add(REPOS, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTelephone2 ----
            lbTelephone2.setText("T\u00e9l\u00e9phone 2");
            lbTelephone2.setName("lbTelephone2");
            pnlContact.add(lbTelephone2, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RETEL2 ----
            RETEL2.setToolTipText("Deuxi\u00e8me num\u00e9ro de t\u00e9l\u00e9phone");
            RETEL2.setComponentPopupMenu(BTD);
            RETEL2.setMinimumSize(new Dimension(170, 30));
            RETEL2.setPreferredSize(new Dimension(170, 30));
            RETEL2.setName("RETEL2");
            pnlContact.add(RETEL2, new GridBagConstraints(1, 9, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFax ----
            lbFax.setText("Fax");
            lbFax.setName("lbFax");
            pnlContact.add(lbFax, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- REFAX ----
            REFAX.setComponentPopupMenu(BTD);
            REFAX.setToolTipText("Num\u00e9ro de fax");
            REFAX.setMinimumSize(new Dimension(170, 30));
            REFAX.setPreferredSize(new Dimension(170, 30));
            REFAX.setName("REFAX");
            pnlContact.add(REFAX, new GridBagConstraints(1, 10, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFonction ----
            lbFonction.setText("Fonction");
            lbFonction.setName("lbFonction");
            pnlContact.add(lbFonction, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RECAT ----
            RECAT.setComponentPopupMenu(BTDI);
            RECAT.setMinimumSize(new Dimension(50, 30));
            RECAT.setPreferredSize(new Dimension(50, 30));
            RECAT.setName("RECAT");
            pnlContact.add(RECAT, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbEmail ----
            lbEmail.setText("E-mail");
            lbEmail.setName("lbEmail");
            pnlContact.add(lbEmail, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RENET1 ----
            RENET1.setToolTipText("Adresse Email");
            RENET1.setComponentPopupMenu(BTD);
            RENET1.setName("RENET1");
            pnlContact.add(RENET1, new GridBagConstraints(1, 12, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbServeurMail ----
            lbServeurMail.setText("Serveur mail");
            lbServeurMail.setName("lbServeurMail");
            pnlContact.add(lbServeurMail, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZSRVM ----
            WZSRVM.setComponentPopupMenu(BTDI);
            WZSRVM.setMinimumSize(new Dimension(160, 30));
            WZSRVM.setPreferredSize(new Dimension(160, 30));
            WZSRVM.setName("WZSRVM");
            pnlContact.add(WZSRVM, new GridBagConstraints(1, 13, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbGencod ----
            lbGencod.setText("Gencod");
            lbGencod.setName("lbGencod");
            pnlContact.add(lbGencod, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZGCD ----
            WZGCD.setComponentPopupMenu(BTDI);
            WZGCD.setMinimumSize(new Dimension(140, 30));
            WZGCD.setPreferredSize(new Dimension(140, 30));
            WZGCD.setName("WZGCD");
            pnlContact.add(WZGCD, new GridBagConstraints(1, 14, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbExtention ----
            lbExtention.setText("Type d'extention");
            lbExtention.setName("lbExtention");
            pnlContact.add(lbExtention, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RETYZP ----
            RETYZP.setComponentPopupMenu(BTD);
            RETYZP.setMinimumSize(new Dimension(25, 30));
            RETYZP.setPreferredSize(new Dimension(25, 30));
            RETYZP.setName("RETYZP");
            pnlContact.add(RETYZP, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlOptions ========
            {
              pnlOptions.setMinimumSize(new Dimension(307, 40));
              pnlOptions.setPreferredSize(new Dimension(307, 40));
              pnlOptions.setName("pnlOptions");
              pnlOptions.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 1.0, 1.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
              
              // ---- REASPA ----
              REASPA.setText("Anti-spam");
              REASPA.setComponentPopupMenu(BTD);
              REASPA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              REASPA.setFont(new Font("sansserif", Font.PLAIN, 14));
              REASPA.setName("REASPA");
              pnlOptions.add(REASPA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- REAAPP ----
              REAAPP.setText("Anti-appel");
              REAAPP.setComponentPopupMenu(BTD);
              REAAPP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              REAAPP.setFont(new Font("sansserif", Font.PLAIN, 14));
              REAAPP.setName("REAAPP");
              pnlOptions.add(REAAPP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- REIN1 ----
              REIN1.setText("Destinataire bon");
              REIN1.setComponentPopupMenu(BTD);
              REIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              REIN1.setFont(new Font("sansserif", Font.PLAIN, 14));
              REIN1.setName("REIN1");
              pnlOptions.add(REIN1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- REIN4 ----
              REIN4.setText("Destinataire facture");
              REIN4.setComponentPopupMenu(BTD);
              REIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              REIN4.setFont(new Font("sansserif", Font.PLAIN, 14));
              REIN4.setName("REIN4");
              pnlOptions.add(REIN4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlContact.add(pnlOptions, new GridBagConstraints(0, 16, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAlerteWorkflow ----
            lbAlerteWorkflow.setText("Alertes du workflow");
            lbAlerteWorkflow.setName("lbAlerteWorkflow");
            pnlContact.add(lbAlerteWorkflow, new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RLIN3 ----
            RLIN3.setModel(new DefaultComboBoxModel(new String[] { "aucune", "par mail", "par SMS" }));
            RLIN3.setFont(new Font("sansserif", Font.PLAIN, 14));
            RLIN3.setPreferredSize(new Dimension(100, 30));
            RLIN3.setMinimumSize(new Dimension(100, 30));
            RLIN3.setName("RLIN3");
            pnlContact.add(RLIN3, new GridBagConstraints(1, 17, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlIdentification ========
          {
            pnlIdentification.setTitre("Identification");
            pnlIdentification.setName("pnlIdentification");
            pnlIdentification.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlIdentification.getLayout()).columnWidths = new int[] { 0, 154, 0, 0, 0 };
            ((GridBagLayout) pnlIdentification.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlIdentification.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlIdentification.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbClient ----
            lbClient.setText("Client");
            lbClient.setName("lbClient");
            pnlIdentification.add(lbClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WTIE1 ----
              WTIE1.setComponentPopupMenu(BTD);
              WTIE1.setMinimumSize(new Dimension(25, 30));
              WTIE1.setPreferredSize(new Dimension(25, 30));
              WTIE1.setName("WTIE1");
              sNPanel1.add(WTIE1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WTIE2 ----
              WTIE2.setComponentPopupMenu(BTD);
              WTIE2.setMinimumSize(new Dimension(70, 30));
              WTIE2.setPreferredSize(new Dimension(70, 30));
              WTIE2.setName("WTIE2");
              sNPanel1.add(WTIE2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WTIE3 ----
              WTIE3.setComponentPopupMenu(BTD);
              WTIE3.setPreferredSize(new Dimension(40, 30));
              WTIE3.setMinimumSize(new Dimension(40, 30));
              WTIE3.setName("WTIE3");
              sNPanel1.add(WTIE3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlIdentification.add(sNPanel1, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNom3 ----
            lbNom3.setText("Raison sociale");
            lbNom3.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbNom3.setHorizontalAlignment(SwingConstants.RIGHT);
            lbNom3.setPreferredSize(new Dimension(100, 30));
            lbNom3.setMinimumSize(new Dimension(100, 30));
            lbNom3.setMaximumSize(new Dimension(100, 30));
            lbNom3.setName("lbNom3");
            pnlIdentification.add(lbNom3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZNOM ----
            WZNOM.setComponentPopupMenu(BTD);
            WZNOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZNOM.setMinimumSize(new Dimension(400, 30));
            WZNOM.setPreferredSize(new Dimension(400, 30));
            WZNOM.setName("WZNOM");
            pnlIdentification.add(WZNOM, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbComplementNom ----
            lbComplementNom.setText("Compl\u00e9ment");
            lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
            lbComplementNom.setPreferredSize(new Dimension(100, 30));
            lbComplementNom.setMinimumSize(new Dimension(100, 30));
            lbComplementNom.setMaximumSize(new Dimension(100, 30));
            lbComplementNom.setName("lbComplementNom");
            pnlIdentification.add(lbComplementNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZCPL ----
            WZCPL.setComponentPopupMenu(BTD);
            WZCPL.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZCPL.setName("WZCPL");
            pnlIdentification.add(WZCPL, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLocalisation ----
            lbLocalisation.setText("Adresse 1");
            lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLocalisation.setInheritsPopupMenu(false);
            lbLocalisation.setMaximumSize(new Dimension(100, 30));
            lbLocalisation.setMinimumSize(new Dimension(100, 30));
            lbLocalisation.setPreferredSize(new Dimension(100, 30));
            lbLocalisation.setName("lbLocalisation");
            pnlIdentification.add(lbLocalisation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZRUE ----
            WZRUE.setComponentPopupMenu(BTD);
            WZRUE.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZRUE.setName("WZRUE");
            pnlIdentification.add(WZRUE, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbRue ----
            lbRue.setText("Adresse 2");
            lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
            lbRue.setName("lbRue");
            pnlIdentification.add(lbRue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZLOC ----
            WZLOC.setComponentPopupMenu(BTD);
            WZLOC.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZLOC.setName("WZLOC");
            pnlIdentification.add(WZLOC, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodePostal ----
            lbCodePostal.setText("Code postal");
            lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbCodePostal.setHorizontalAlignment(SwingConstants.RIGHT);
            lbCodePostal.setName("lbCodePostal");
            pnlIdentification.add(lbCodePostal, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZCDP ----
            WZCDP.setComponentPopupMenu(BTD);
            WZCDP.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZCDP.setPreferredSize(new Dimension(60, 30));
            WZCDP.setMinimumSize(new Dimension(60, 30));
            WZCDP.setName("WZCDP");
            pnlIdentification.add(WZCDP, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbVille ----
            lbVille.setText("Ville");
            lbVille.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbVille.setHorizontalAlignment(SwingConstants.RIGHT);
            lbVille.setName("lbVille");
            pnlIdentification.add(lbVille, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WZVIL ----
            WZVIL.setComponentPopupMenu(BTD);
            WZVIL.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZVIL.setName("WZVIL");
            pnlIdentification.add(WZVIL, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPays ----
            lbPays.setText("Pays");
            lbPays.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbPays.setHorizontalAlignment(SwingConstants.RIGHT);
            lbPays.setName("lbPays");
            pnlIdentification.add(lbPays, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WZPAY ----
            WZPAY.setComponentPopupMenu(BTD);
            WZPAY.setFont(new Font("sansserif", Font.PLAIN, 14));
            WZPAY.setName("WZPAY");
            pnlIdentification.add(WZPAY, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlIdentification, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlWebshop ========
          {
            pnlWebshop.setTitre("Webshop");
            pnlWebshop.setName("pnlWebshop");
            pnlWebshop.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlWebshop.getLayout()).columnWidths = new int[] { 34, 0, 0 };
            ((GridBagLayout) pnlWebshop.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlWebshop.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlWebshop.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- RWACC ----
            RWACC.setText("Acc\u00e8s au WebShop");
            RWACC.setComponentPopupMenu(BTD);
            RWACC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RWACC.setToolTipText("Autorise ce contact \u00e0 acc\u00e9der au site internet commercial de votre entreprise");
            RWACC.setFont(new Font("sansserif", Font.PLAIN, 14));
            RWACC.setPreferredSize(new Dimension(200, 30));
            RWACC.setMinimumSize(new Dimension(200, 30));
            RWACC.setName("RWACC");
            RWACC.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                RWACCActionPerformed(e);
              }
            });
            pnlWebshop.add(RWACC, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RWIN1 ----
            RWIN1.setText("Visualisation des disponibilit\u00e9s en stock autoris\u00e9e");
            RWIN1.setComponentPopupMenu(BTD);
            RWIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RWIN1.setToolTipText("Autorise ce contact \u00e0 acc\u00e9der au site internet commercial de votre entreprise");
            RWIN1.setFont(new Font("sansserif", Font.PLAIN, 14));
            RWIN1.setPreferredSize(new Dimension(200, 30));
            RWIN1.setMinimumSize(new Dimension(200, 30));
            RWIN1.setName("RWIN1");
            pnlWebshop.add(RWIN1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RWIN2 ----
            RWIN2.setText("Visualisation des prix autoris\u00e9e");
            RWIN2.setComponentPopupMenu(BTD);
            RWIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RWIN2.setToolTipText("Autorise ce contact \u00e0 acc\u00e9der au site internet commercial de votre entreprise");
            RWIN2.setFont(new Font("sansserif", Font.PLAIN, 14));
            RWIN2.setPreferredSize(new Dimension(200, 30));
            RWIN2.setMinimumSize(new Dimension(200, 30));
            RWIN2.setName("RWIN2");
            pnlWebshop.add(RWIN2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RWIN5 ----
            RWIN5.setText("Saisie de devis autoris\u00e9e");
            RWIN5.setComponentPopupMenu(BTD);
            RWIN5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RWIN5.setToolTipText("Autorise ce contact \u00e0 acc\u00e9der au site internet commercial de votre entreprise");
            RWIN5.setFont(new Font("sansserif", Font.PLAIN, 14));
            RWIN5.setPreferredSize(new Dimension(200, 30));
            RWIN5.setMinimumSize(new Dimension(200, 30));
            RWIN5.setName("RWIN5");
            pnlWebshop.add(RWIN5, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RWIN3 ----
            RWIN3.setText("Saisie de commandes autoris\u00e9e");
            RWIN3.setComponentPopupMenu(BTD);
            RWIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RWIN3.setToolTipText("Autorise ce contact \u00e0 acc\u00e9der au site internet commercial de votre entreprise");
            RWIN3.setFont(new Font("sansserif", Font.PLAIN, 14));
            RWIN3.setPreferredSize(new Dimension(200, 30));
            RWIN3.setMinimumSize(new Dimension(200, 30));
            RWIN3.setName("RWIN3");
            pnlWebshop.add(RWIN3, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RWIN4 ----
            RWIN4.setText("Visualisation des documents de ventes de toute la soci\u00e9t\u00e9 autoris\u00e9e");
            RWIN4.setComponentPopupMenu(BTD);
            RWIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RWIN4.setToolTipText("Autorise ce contact \u00e0 acc\u00e9der au site internet commercial de votre entreprise");
            RWIN4.setFont(new Font("sansserif", Font.PLAIN, 14));
            RWIN4.setPreferredSize(new Dimension(200, 30));
            RWIN4.setMinimumSize(new Dimension(200, 30));
            RWIN4.setName("RWIN4");
            pnlWebshop.add(RWIN4, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- RLIN4 ----
            RLIN4.setText("Acc\u00e8s restreint");
            RLIN4.setComponentPopupMenu(BTD);
            RLIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RLIN4.setToolTipText("Autorise ce contact \u00e0 acc\u00e9der au site internet commercial de votre entreprise");
            RLIN4.setFont(new Font("sansserif", Font.PLAIN, 14));
            RLIN4.setPreferredSize(new Dimension(200, 30));
            RLIN4.setMinimumSize(new Dimension(200, 30));
            RLIN4.setName("RLIN4");
            pnlWebshop.add(RLIN4, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlWebshop, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlZP ========
          {
            pnlZP.setTitre("Zones personnalis\u00e9es");
            pnlZP.setName("pnlZP");
            pnlZP.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlZP.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlZP.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlZP.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlZP.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- OBJ_75_OBJ_75 ----
            OBJ_75_OBJ_75.setText("@WTI1@");
            OBJ_75_OBJ_75.setPreferredSize(new Dimension(52, 20));
            OBJ_75_OBJ_75.setMaximumSize(new Dimension(52, 20));
            OBJ_75_OBJ_75.setMinimumSize(new Dimension(52, 20));
            OBJ_75_OBJ_75.setHorizontalTextPosition(SwingConstants.RIGHT);
            OBJ_75_OBJ_75.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
            pnlZP.add(OBJ_75_OBJ_75, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RETOP1 ----
            RETOP1.setComponentPopupMenu(null);
            RETOP1.setMinimumSize(new Dimension(35, 30));
            RETOP1.setPreferredSize(new Dimension(35, 30));
            RETOP1.setName("RETOP1");
            pnlZP.add(RETOP1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("@WTI2@");
            OBJ_76_OBJ_76.setMaximumSize(new Dimension(52, 20));
            OBJ_76_OBJ_76.setMinimumSize(new Dimension(52, 20));
            OBJ_76_OBJ_76.setPreferredSize(new Dimension(52, 20));
            OBJ_76_OBJ_76.setHorizontalTextPosition(SwingConstants.RIGHT);
            OBJ_76_OBJ_76.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");
            pnlZP.add(OBJ_76_OBJ_76, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RETOP2 ----
            RETOP2.setComponentPopupMenu(null);
            RETOP2.setMinimumSize(new Dimension(35, 30));
            RETOP2.setPreferredSize(new Dimension(35, 30));
            RETOP2.setName("RETOP2");
            pnlZP.add(RETOP2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("@WTI3@");
            OBJ_78_OBJ_78.setMaximumSize(new Dimension(52, 20));
            OBJ_78_OBJ_78.setMinimumSize(new Dimension(52, 20));
            OBJ_78_OBJ_78.setPreferredSize(new Dimension(52, 20));
            OBJ_78_OBJ_78.setHorizontalTextPosition(SwingConstants.RIGHT);
            OBJ_78_OBJ_78.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
            pnlZP.add(OBJ_78_OBJ_78, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RETOP3 ----
            RETOP3.setComponentPopupMenu(null);
            RETOP3.setMinimumSize(new Dimension(35, 30));
            RETOP3.setPreferredSize(new Dimension(35, 30));
            RETOP3.setName("RETOP3");
            pnlZP.add(RETOP3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("@WTI4@");
            OBJ_80_OBJ_80.setMaximumSize(new Dimension(52, 20));
            OBJ_80_OBJ_80.setMinimumSize(new Dimension(52, 20));
            OBJ_80_OBJ_80.setPreferredSize(new Dimension(52, 20));
            OBJ_80_OBJ_80.setHorizontalTextPosition(SwingConstants.RIGHT);
            OBJ_80_OBJ_80.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
            pnlZP.add(OBJ_80_OBJ_80, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RETOP4 ----
            RETOP4.setComponentPopupMenu(null);
            RETOP4.setMinimumSize(new Dimension(35, 30));
            RETOP4.setPreferredSize(new Dimension(35, 30));
            RETOP4.setName("RETOP4");
            pnlZP.add(RETOP4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- OBJ_82_OBJ_82 ----
            OBJ_82_OBJ_82.setText("@WTI5@");
            OBJ_82_OBJ_82.setMaximumSize(new Dimension(52, 20));
            OBJ_82_OBJ_82.setMinimumSize(new Dimension(52, 20));
            OBJ_82_OBJ_82.setPreferredSize(new Dimension(52, 20));
            OBJ_82_OBJ_82.setHorizontalTextPosition(SwingConstants.RIGHT);
            OBJ_82_OBJ_82.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");
            pnlZP.add(OBJ_82_OBJ_82, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RETOP5 ----
            RETOP5.setComponentPopupMenu(null);
            RETOP5.setMinimumSize(new Dimension(35, 30));
            RETOP5.setPreferredSize(new Dimension(35, 30));
            RETOP5.setName("RETOP5");
            pnlZP.add(RETOP5, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlZP, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlDroite);
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ======== BTDI ========
    {
      BTDI.setName("BTDI");
      
      // ---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_6);
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_5);
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlContact;
  private XRiCheckBox RLIN1;
  private SNLabelChamp lbNom2;
  private XRiTextField RECIV;
  private SNLabelChamp lbNom;
  private XRiTextField RENOM;
  private SNLabelChamp lbPrenom;
  private XRiTextField REPRE;
  private SNLabelChamp lbClassement1;
  private XRiTextField RECL1;
  private SNLabelChamp lbClassement2;
  private XRiTextField RECL2;
  private SNLabelChamp lbAlias;
  private XRiTextField REPRF;
  private SNLabelChamp lbObservation;
  private XRiTextField REOBS;
  private SNLabelChamp lbTelephone;
  private XRiTextField RETEL;
  private SNLabelChamp lbPoste;
  private XRiTextField REPOS;
  private SNLabelChamp lbTelephone2;
  private XRiTextField RETEL2;
  private SNLabelChamp lbFax;
  private XRiTextField REFAX;
  private SNLabelChamp lbFonction;
  private XRiTextField RECAT;
  private SNLabelChamp lbEmail;
  private XRiMailto RENET1;
  private SNLabelChamp lbServeurMail;
  private XRiTextField WZSRVM;
  private SNLabelChamp lbGencod;
  private XRiTextField WZGCD;
  private SNLabelChamp lbExtention;
  private XRiTextField RETYZP;
  private SNPanel pnlOptions;
  private XRiCheckBox REASPA;
  private XRiCheckBox REAAPP;
  private XRiCheckBox REIN1;
  private XRiCheckBox REIN4;
  private SNLabelChamp lbAlerteWorkflow;
  private XRiComboBox RLIN3;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlIdentification;
  private SNLabelChamp lbClient;
  private SNPanel sNPanel1;
  private XRiTextField WTIE1;
  private XRiTextField WTIE2;
  private XRiTextField WTIE3;
  private JLabel lbNom3;
  private XRiTextField WZNOM;
  private JLabel lbComplementNom;
  private XRiTextField WZCPL;
  private JLabel lbLocalisation;
  private XRiTextField WZRUE;
  private JLabel lbRue;
  private XRiTextField WZLOC;
  private JLabel lbCodePostal;
  private XRiTextField WZCDP;
  private JLabel lbVille;
  private XRiTextField WZVIL;
  private JLabel lbPays;
  private XRiTextField WZPAY;
  private SNPanelTitre pnlWebshop;
  private XRiCheckBox RWACC;
  private XRiCheckBox RWIN1;
  private XRiCheckBox RWIN2;
  private XRiCheckBox RWIN5;
  private XRiCheckBox RWIN3;
  private XRiCheckBox RWIN4;
  private XRiCheckBox RLIN4;
  private SNPanelTitre pnlZP;
  private JLabel OBJ_75_OBJ_75;
  private XRiTextField RETOP1;
  private JLabel OBJ_76_OBJ_76;
  private XRiTextField RETOP2;
  private JLabel OBJ_78_OBJ_78;
  private XRiTextField RETOP3;
  private JLabel OBJ_80_OBJ_80;
  private XRiTextField RETOP4;
  private JLabel OBJ_82_OBJ_82;
  private XRiTextField RETOP5;
  private JPopupMenu BTDI;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
