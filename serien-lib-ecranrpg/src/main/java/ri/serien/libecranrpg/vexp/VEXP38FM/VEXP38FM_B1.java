
package ri.serien.libecranrpg.vexp.VEXP38FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VEXP38FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP38FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
    
    T01.setValeursSelection("X", "");
    T02.setValeursSelection("X", "");
    T03.setValeursSelection("X", "");
    T04.setValeursSelection("X", "");
    T05.setValeursSelection("X", "");
    T06.setValeursSelection("X", "");
    T07.setValeursSelection("X", "");
    T08.setValeursSelection("X", "");
    T09.setValeursSelection("X", "");
    T10.setValeursSelection("X", "");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    T01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L01@")).trim());
    T02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L02@")).trim());
    T03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L03@")).trim());
    T04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L04@")).trim());
    T05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L05@")).trim());
    T06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L06@")).trim());
    T07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L07@")).trim());
    T08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L08@")).trim());
    T09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L09@")).trim());
    T10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO Icones
    
    T01.setEnabled(!lexique.HostFieldGetData("L01").trim().equals(""));
    T02.setEnabled(!lexique.HostFieldGetData("L02").trim().equals(""));
    T03.setEnabled(!lexique.HostFieldGetData("L03").trim().equals(""));
    T04.setEnabled(!lexique.HostFieldGetData("L04").trim().equals(""));
    T05.setEnabled(!lexique.HostFieldGetData("L05").trim().equals(""));
    T06.setEnabled(!lexique.HostFieldGetData("L06").trim().equals(""));
    T07.setEnabled(!lexique.HostFieldGetData("L07").trim().equals(""));
    T08.setEnabled(!lexique.HostFieldGetData("L08").trim().equals(""));
    T09.setEnabled(!lexique.HostFieldGetData("L09").trim().equals(""));
    T10.setEnabled(!lexique.HostFieldGetData("L10").trim().equals(""));
    
    // titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    T01 = new XRiCheckBox();
    T02 = new XRiCheckBox();
    T03 = new XRiCheckBox();
    T04 = new XRiCheckBox();
    T05 = new XRiCheckBox();
    T06 = new XRiCheckBox();
    T07 = new XRiCheckBox();
    T08 = new XRiCheckBox();
    T09 = new XRiCheckBox();
    T10 = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1005, 325));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder("Cochez pour autoriser cet article sur un site"));
          panel4.setOpaque(false);
          panel4.setPreferredSize(new Dimension(840, 400));
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel4.add(BT_PGUP);
          BT_PGUP.setBounds(765, 30, 25, 105);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel4.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(765, 184, 25, 105);

          //---- T01 ----
          T01.setText("@L01@");
          T01.setName("T01");
          panel4.add(T01);
          T01.setBounds(20, 30, 720, 25);

          //---- T02 ----
          T02.setText("@L02@");
          T02.setName("T02");
          panel4.add(T02);
          T02.setBounds(20, 56, 720, 25);

          //---- T03 ----
          T03.setText("@L03@");
          T03.setName("T03");
          panel4.add(T03);
          T03.setBounds(20, 82, 720, 25);

          //---- T04 ----
          T04.setText("@L04@");
          T04.setName("T04");
          panel4.add(T04);
          T04.setBounds(20, 108, 720, 25);

          //---- T05 ----
          T05.setText("@L05@");
          T05.setName("T05");
          panel4.add(T05);
          T05.setBounds(20, 134, 720, 25);

          //---- T06 ----
          T06.setText("@L06@");
          T06.setName("T06");
          panel4.add(T06);
          T06.setBounds(20, 160, 720, 25);

          //---- T07 ----
          T07.setText("@L07@");
          T07.setName("T07");
          panel4.add(T07);
          T07.setBounds(20, 186, 720, 25);

          //---- T08 ----
          T08.setText("@L08@");
          T08.setName("T08");
          panel4.add(T08);
          T08.setBounds(20, 212, 720, 25);

          //---- T09 ----
          T09.setText("@L09@");
          T09.setName("T09");
          panel4.add(T09);
          T09.setBounds(20, 238, 720, 25);

          //---- T10 ----
          T10.setText("@L10@");
          T10.setName("T10");
          panel4.add(T10);
          T10.setBounds(20, 264, 720, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(15, 10, 804, 305);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel4;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiCheckBox T01;
  private XRiCheckBox T02;
  private XRiCheckBox T03;
  private XRiCheckBox T04;
  private XRiCheckBox T05;
  private XRiCheckBox T06;
  private XRiCheckBox T07;
  private XRiCheckBox T08;
  private XRiCheckBox T09;
  private XRiCheckBox T10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables



}
