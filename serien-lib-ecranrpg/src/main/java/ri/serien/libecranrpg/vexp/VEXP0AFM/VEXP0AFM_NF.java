/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vexp.VEXP0AFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * Boîte de dialogue pour saisir le nom du fichier à exporter.
 */
public class VEXP0AFM_NF extends SNPanelEcranRPG implements ioFrame {
   
  
  public VEXP0AFM_NF(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDialog(true);
    
    // Ajout
    initDiverses();
    SUFFI1.setListeCaracteresNonAutorises(Constantes.caracteresNonAutorisesPourFichier);
    SUFFI2.setListeCaracteresNonAutorises(Constantes.caracteresNonAutorisesPourFichier);
    SUFFI3.setListeCaracteresNonAutorises(Constantes.caracteresNonAutorisesPourFichier);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
        
    // Afficher le message d'erreur s'i ly en a un
    if (lexique.isTrue("19")) {
      String message = lexique.HostFieldGetData("V03F");
      if (message != null && !message.isEmpty()) {
        DialogueInformation.afficher(message);
      }      
    }
    
    // Récupération du nom du fichier
    String nomfic = lexique.getNomFichierTableur();
    if ((nomfic != null) && (SUFFI1.getText().trim().equals(""))) {
      if (nomfic.length() > 50) {
        SUFFI1.setText(nomfic.substring(0, 50));
        if (nomfic.length() > 100) {
          SUFFI2.setText(nomfic.substring(50, 100));
          SUFFI3.setText(nomfic.substring(100));
        }
        else {
          SUFFI2.setText(nomfic.substring(50));
        }
      }
      else {
        SUFFI1.setText(nomfic);
      }
    }
    lexique.setNomFichierTableur(null);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Exporter"));
  }
  
  @Override
  public void getData() {
    SUFFI1.setText(SUFFI1.getText().replace('"', ' ').replaceAll("\\&", "et"));
    SUFFI2.setText(SUFFI2.getText().replace('"', ' ').replaceAll("\\&", "et"));
    SUFFI3.setText(SUFFI3.getText().replace('"', ' ').replaceAll("\\&", "et"));
    super.getData();
    
    
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }  
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new JPanel();
    label1 = new JLabel();
    SUFFI1 = new XRiTextField();
    SUFFI2 = new XRiTextField();
    SUFFI3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(600, 300));
    setPreferredSize(new Dimension(600, 300));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(238, 238, 210));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

      //---- label1 ----
      label1.setText("<html>Saisir le nom du fichier \u00e0 exporter (sans l'extension). Le fichier sera automatiquement g\u00e9n\u00e9r\u00e9 dans le r\u00e9pertoire consacr\u00e9 aux documents li\u00e9s.</html>");
      label1.setMinimumSize(new Dimension(300, 50));
      label1.setPreferredSize(new Dimension(300, 50));
      label1.setMaximumSize(new Dimension(300, 50));
      label1.setFont(new Font("sansserif", Font.PLAIN, 14));
      label1.setVerticalTextPosition(SwingConstants.TOP);
      label1.setName("label1");
      pnlContenu.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- SUFFI1 ----
      SUFFI1.setPreferredSize(new Dimension(300, 30));
      SUFFI1.setMinimumSize(new Dimension(300, 30));
      SUFFI1.setFont(new Font("sansserif", Font.PLAIN, 14));
      SUFFI1.setName("SUFFI1");
      pnlContenu.add(SUFFI1, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- SUFFI2 ----
      SUFFI2.setMinimumSize(new Dimension(300, 30));
      SUFFI2.setPreferredSize(new Dimension(300, 30));
      SUFFI2.setFont(new Font("sansserif", Font.PLAIN, 14));
      SUFFI2.setName("SUFFI2");
      pnlContenu.add(SUFFI2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- SUFFI3 ----
      SUFFI3.setPreferredSize(new Dimension(300, 30));
      SUFFI3.setMinimumSize(new Dimension(300, 30));
      SUFFI3.setFont(new Font("sansserif", Font.PLAIN, 14));
      SUFFI3.setName("SUFFI3");
      pnlContenu.add(SUFFI3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private JPanel pnlContenu;
  private JLabel label1;
  private XRiTextField SUFFI1;
  private XRiTextField SUFFI2;
  private XRiTextField SUFFI3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
