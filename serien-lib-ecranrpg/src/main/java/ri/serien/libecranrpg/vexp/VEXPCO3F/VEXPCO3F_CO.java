
package ri.serien.libecranrpg.vexp.VEXPCO3F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * @author Stéphane Vénéri
 */
public class VEXPCO3F_CO extends SNPanelEcranRPG implements ioFrame {
  
  private Color defautFond;
  
  public VEXPCO3F_CO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Récupération de la couleur du fond par défaut
    defautFond = button1.getBackground();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    button1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO1@")).trim());
    button2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO2@")).trim());
    button3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO3@")).trim());
    button4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO4@")).trim());
    button5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO5@")).trim());
    button6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO6@")).trim());
    button7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO7@")).trim());
    button8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO8@")).trim());
    button9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO9@")).trim());
    button10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    button1.setVisible(!lexique.HostFieldGetData("TBO1").trim().equalsIgnoreCase(""));
    button2.setVisible(!lexique.HostFieldGetData("TBO2").trim().equalsIgnoreCase(""));
    button3.setVisible(!lexique.HostFieldGetData("TBO3").trim().equalsIgnoreCase(""));
    button4.setVisible(!lexique.HostFieldGetData("TBO4").trim().equalsIgnoreCase(""));
    button5.setVisible(!lexique.HostFieldGetData("TBO5").trim().equalsIgnoreCase(""));
    button6.setVisible(!lexique.HostFieldGetData("TBO6").trim().equalsIgnoreCase(""));
    button7.setVisible(!lexique.HostFieldGetData("TBO7").trim().equalsIgnoreCase(""));
    button8.setVisible(!lexique.HostFieldGetData("TBO8").trim().equalsIgnoreCase(""));
    button9.setVisible(!lexique.HostFieldGetData("TBO9").trim().equalsIgnoreCase(""));
    button10.setVisible(!lexique.HostFieldGetData("TBO10").trim().equalsIgnoreCase(""));
    
    // Mise en évidence du bouton qui a le "focus"
    button1.setBackground(lexique.isTrue("11") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button2.setBackground(lexique.isTrue("12") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button3.setBackground(lexique.isTrue("13") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button4.setBackground(lexique.isTrue("14") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button5.setBackground(lexique.isTrue("15") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button6.setBackground(lexique.isTrue("16") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button7.setBackground(lexique.isTrue("17") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button8.setBackground(lexique.isTrue("18") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button9.setBackground(lexique.isTrue("19") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    button10.setBackground(lexique.isTrue("20") ? SNCharteGraphique.COULEUR_FOND : defautFond);
    
    // Titre
    setTitle(interpreteurD.analyseExpression(lexique.HostFieldGetData("TBOT").trim()));
    if (getTitle().trim().equals("")) {
      setTitle(interpreteurD.analyseExpression("Sélection"));
    }
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N1"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N2"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N3"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N4"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N5"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N6"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N7"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N8"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N9"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("NA"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    button1 = new JButton();
    button2 = new JButton();
    button3 = new JButton();
    button4 = new JButton();
    button5 = new JButton();
    button6 = new JButton();
    button7 = new JButton();
    button8 = new JButton();
    button9 = new JButton();
    button10 = new JButton();
    bouton_retour = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(495, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setBackground(new Color(90, 90, 90));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setOpaque(false);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(430, 15, 25, 150);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(430, 165, 25, 150);

          //---- button1 ----
          button1.setText("@TBO1@");
          button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button1.setHorizontalAlignment(SwingConstants.LEFT);
          button1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button1.setName("button1");
          button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button1ActionPerformed(e);
            }
          });
          p_recup.add(button1);
          button1.setBounds(20, 15, 405, 30);

          //---- button2 ----
          button2.setText("@TBO2@");
          button2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button2.setHorizontalAlignment(SwingConstants.LEFT);
          button2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button2.setName("button2");
          button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button2ActionPerformed(e);
            }
          });
          p_recup.add(button2);
          button2.setBounds(20, 45, 405, 30);

          //---- button3 ----
          button3.setText("@TBO3@");
          button3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button3.setHorizontalAlignment(SwingConstants.LEFT);
          button3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button3.setName("button3");
          button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button3ActionPerformed(e);
            }
          });
          p_recup.add(button3);
          button3.setBounds(20, 75, 405, 30);

          //---- button4 ----
          button4.setText("@TBO4@");
          button4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button4.setHorizontalAlignment(SwingConstants.LEFT);
          button4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button4.setName("button4");
          button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button4ActionPerformed(e);
            }
          });
          p_recup.add(button4);
          button4.setBounds(20, 105, 405, 30);

          //---- button5 ----
          button5.setText("@TBO5@");
          button5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button5.setHorizontalAlignment(SwingConstants.LEFT);
          button5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button5.setName("button5");
          button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button5ActionPerformed(e);
            }
          });
          p_recup.add(button5);
          button5.setBounds(20, 135, 405, 30);

          //---- button6 ----
          button6.setText("@TBO6@");
          button6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button6.setHorizontalAlignment(SwingConstants.LEFT);
          button6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button6.setName("button6");
          button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button6ActionPerformed(e);
            }
          });
          p_recup.add(button6);
          button6.setBounds(20, 165, 405, 30);

          //---- button7 ----
          button7.setText("@TBO7@");
          button7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button7.setHorizontalAlignment(SwingConstants.LEFT);
          button7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button7.setName("button7");
          button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button7ActionPerformed(e);
            }
          });
          p_recup.add(button7);
          button7.setBounds(20, 195, 405, 30);

          //---- button8 ----
          button8.setText("@TBO8@");
          button8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button8.setHorizontalAlignment(SwingConstants.LEFT);
          button8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button8.setName("button8");
          button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button8ActionPerformed(e);
            }
          });
          p_recup.add(button8);
          button8.setBounds(20, 225, 405, 30);

          //---- button9 ----
          button9.setText("@TBO9@");
          button9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button9.setHorizontalAlignment(SwingConstants.LEFT);
          button9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button9.setName("button9");
          button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button9ActionPerformed(e);
            }
          });
          p_recup.add(button9);
          button9.setBounds(20, 255, 405, 30);

          //---- button10 ----
          button10.setText("@TBO10@");
          button10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button10.setHorizontalAlignment(SwingConstants.LEFT);
          button10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          button10.setName("button10");
          button10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button10ActionPerformed(e);
            }
          });
          p_recup.add(button10);
          button10.setBounds(20, 285, 405, 30);

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setFont(bouton_retour.getFont().deriveFont(bouton_retour.getFont().getStyle() | Font.BOLD));
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button11ActionPerformed(e);
            }
          });
          p_recup.add(bouton_retour);
          bouton_retour.setBounds(147, 330, 150, 40);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 475, 380);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton button1;
  private JButton button2;
  private JButton button3;
  private JButton button4;
  private JButton button5;
  private JButton button6;
  private JButton button7;
  private JButton button8;
  private JButton button9;
  private JButton button10;
  private JButton bouton_retour;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
