
package ri.serien.libecranrpg.vexp.VEXP01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP01FX_WS extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP01FX_WS(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    WSFIN8.setEnabled(lexique.isPresent("WSFIN8"));
    WSDEB8.setEnabled(lexique.isPresent("WSDEB8"));
    WSNUM8.setEnabled(lexique.isPresent("WSNUM8"));
    WSFIN7.setEnabled(lexique.isPresent("WSFIN7"));
    WSDEB7.setEnabled(lexique.isPresent("WSDEB7"));
    WSNUM7.setEnabled(lexique.isPresent("WSNUM7"));
    WSFIN6.setEnabled(lexique.isPresent("WSFIN6"));
    WSDEB6.setEnabled(lexique.isPresent("WSDEB6"));
    WSNUM6.setEnabled(lexique.isPresent("WSNUM6"));
    WSFIN5.setEnabled(lexique.isPresent("WSFIN5"));
    WSDEB5.setEnabled(lexique.isPresent("WSDEB5"));
    WSNUM5.setEnabled(lexique.isPresent("WSNUM5"));
    WSFIN4.setEnabled(lexique.isPresent("WSFIN4"));
    WSDEB4.setEnabled(lexique.isPresent("WSDEB4"));
    WSNUM4.setEnabled(lexique.isPresent("WSNUM4"));
    WSFIN3.setEnabled(lexique.isPresent("WSFIN3"));
    WSDEB3.setEnabled(lexique.isPresent("WSDEB3"));
    WSNUM3.setEnabled(lexique.isPresent("WSNUM3"));
    WSFIN2.setEnabled(lexique.isPresent("WSFIN2"));
    WSDEB2.setEnabled(lexique.isPresent("WSDEB2"));
    WSNUM2.setEnabled(lexique.isPresent("WSNUM2"));
    WSFIN1.setEnabled(lexique.isPresent("WSFIN1"));
    WSDEB1.setEnabled(lexique.isPresent("WSDEB1"));
    WSNUM1.setEnabled(lexique.isPresent("WSNUM1"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_52 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_53 = new JLabel();
    WSNUM1 = new XRiTextField();
    WSDEB1 = new XRiTextField();
    WSFIN1 = new XRiTextField();
    WSNUM2 = new XRiTextField();
    WSDEB2 = new XRiTextField();
    WSFIN2 = new XRiTextField();
    WSNUM3 = new XRiTextField();
    WSDEB3 = new XRiTextField();
    WSFIN3 = new XRiTextField();
    WSNUM4 = new XRiTextField();
    WSDEB4 = new XRiTextField();
    WSFIN4 = new XRiTextField();
    WSNUM5 = new XRiTextField();
    WSDEB5 = new XRiTextField();
    WSFIN5 = new XRiTextField();
    WSNUM6 = new XRiTextField();
    WSDEB6 = new XRiTextField();
    WSFIN6 = new XRiTextField();
    WSNUM7 = new XRiTextField();
    WSDEB7 = new XRiTextField();
    WSFIN7 = new XRiTextField();
    WSNUM8 = new XRiTextField();
    WSDEB8 = new XRiTextField();
    WSFIN8 = new XRiTextField();
    OBJ_54 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_82 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de l'exploitation");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(480, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Working type boite aux lettres");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_52 ----
            OBJ_52.setText("Num\u00e9ro d\u00e9but");
            OBJ_52.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_52.setName("OBJ_52");
            xTitledPanel1ContentContainer.add(OBJ_52);
            OBJ_52.setBounds(166, 15, 104, 20);

            //---- OBJ_51 ----
            OBJ_51.setText("Num\u00e9ro chrono");
            OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_51.setName("OBJ_51");
            xTitledPanel1ContentContainer.add(OBJ_51);
            OBJ_51.setBounds(53, 15, 101, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Num\u00e9ro fin");
            OBJ_53.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_53.setName("OBJ_53");
            xTitledPanel1ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(286, 15, 84, 20);

            //---- WSNUM1 ----
            WSNUM1.setComponentPopupMenu(BTD);
            WSNUM1.setName("WSNUM1");
            xTitledPanel1ContentContainer.add(WSNUM1);
            WSNUM1.setBounds(70, 50, 66, WSNUM1.getPreferredSize().height);

            //---- WSDEB1 ----
            WSDEB1.setComponentPopupMenu(BTD);
            WSDEB1.setName("WSDEB1");
            xTitledPanel1ContentContainer.add(WSDEB1);
            WSDEB1.setBounds(185, 50, 66, WSDEB1.getPreferredSize().height);

            //---- WSFIN1 ----
            WSFIN1.setComponentPopupMenu(BTD);
            WSFIN1.setName("WSFIN1");
            xTitledPanel1ContentContainer.add(WSFIN1);
            WSFIN1.setBounds(295, 50, 66, WSFIN1.getPreferredSize().height);

            //---- WSNUM2 ----
            WSNUM2.setComponentPopupMenu(BTD);
            WSNUM2.setName("WSNUM2");
            xTitledPanel1ContentContainer.add(WSNUM2);
            WSNUM2.setBounds(70, 81, 66, WSNUM2.getPreferredSize().height);

            //---- WSDEB2 ----
            WSDEB2.setComponentPopupMenu(BTD);
            WSDEB2.setName("WSDEB2");
            xTitledPanel1ContentContainer.add(WSDEB2);
            WSDEB2.setBounds(185, 81, 66, WSDEB2.getPreferredSize().height);

            //---- WSFIN2 ----
            WSFIN2.setComponentPopupMenu(BTD);
            WSFIN2.setName("WSFIN2");
            xTitledPanel1ContentContainer.add(WSFIN2);
            WSFIN2.setBounds(295, 81, 66, WSFIN2.getPreferredSize().height);

            //---- WSNUM3 ----
            WSNUM3.setComponentPopupMenu(BTD);
            WSNUM3.setName("WSNUM3");
            xTitledPanel1ContentContainer.add(WSNUM3);
            WSNUM3.setBounds(70, 112, 66, WSNUM3.getPreferredSize().height);

            //---- WSDEB3 ----
            WSDEB3.setComponentPopupMenu(BTD);
            WSDEB3.setName("WSDEB3");
            xTitledPanel1ContentContainer.add(WSDEB3);
            WSDEB3.setBounds(185, 112, 66, WSDEB3.getPreferredSize().height);

            //---- WSFIN3 ----
            WSFIN3.setComponentPopupMenu(BTD);
            WSFIN3.setName("WSFIN3");
            xTitledPanel1ContentContainer.add(WSFIN3);
            WSFIN3.setBounds(295, 112, 66, WSFIN3.getPreferredSize().height);

            //---- WSNUM4 ----
            WSNUM4.setComponentPopupMenu(BTD);
            WSNUM4.setName("WSNUM4");
            xTitledPanel1ContentContainer.add(WSNUM4);
            WSNUM4.setBounds(70, 143, 66, WSNUM4.getPreferredSize().height);

            //---- WSDEB4 ----
            WSDEB4.setComponentPopupMenu(BTD);
            WSDEB4.setName("WSDEB4");
            xTitledPanel1ContentContainer.add(WSDEB4);
            WSDEB4.setBounds(185, 143, 66, WSDEB4.getPreferredSize().height);

            //---- WSFIN4 ----
            WSFIN4.setComponentPopupMenu(BTD);
            WSFIN4.setName("WSFIN4");
            xTitledPanel1ContentContainer.add(WSFIN4);
            WSFIN4.setBounds(295, 143, 66, WSFIN4.getPreferredSize().height);

            //---- WSNUM5 ----
            WSNUM5.setComponentPopupMenu(BTD);
            WSNUM5.setName("WSNUM5");
            xTitledPanel1ContentContainer.add(WSNUM5);
            WSNUM5.setBounds(70, 174, 66, WSNUM5.getPreferredSize().height);

            //---- WSDEB5 ----
            WSDEB5.setComponentPopupMenu(BTD);
            WSDEB5.setName("WSDEB5");
            xTitledPanel1ContentContainer.add(WSDEB5);
            WSDEB5.setBounds(185, 174, 66, WSDEB5.getPreferredSize().height);

            //---- WSFIN5 ----
            WSFIN5.setComponentPopupMenu(BTD);
            WSFIN5.setName("WSFIN5");
            xTitledPanel1ContentContainer.add(WSFIN5);
            WSFIN5.setBounds(295, 174, 66, WSFIN5.getPreferredSize().height);

            //---- WSNUM6 ----
            WSNUM6.setComponentPopupMenu(BTD);
            WSNUM6.setName("WSNUM6");
            xTitledPanel1ContentContainer.add(WSNUM6);
            WSNUM6.setBounds(70, 205, 66, WSNUM6.getPreferredSize().height);

            //---- WSDEB6 ----
            WSDEB6.setComponentPopupMenu(BTD);
            WSDEB6.setName("WSDEB6");
            xTitledPanel1ContentContainer.add(WSDEB6);
            WSDEB6.setBounds(185, 205, 66, WSDEB6.getPreferredSize().height);

            //---- WSFIN6 ----
            WSFIN6.setComponentPopupMenu(BTD);
            WSFIN6.setName("WSFIN6");
            xTitledPanel1ContentContainer.add(WSFIN6);
            WSFIN6.setBounds(295, 205, 66, WSFIN6.getPreferredSize().height);

            //---- WSNUM7 ----
            WSNUM7.setComponentPopupMenu(BTD);
            WSNUM7.setName("WSNUM7");
            xTitledPanel1ContentContainer.add(WSNUM7);
            WSNUM7.setBounds(70, 236, 66, WSNUM7.getPreferredSize().height);

            //---- WSDEB7 ----
            WSDEB7.setComponentPopupMenu(BTD);
            WSDEB7.setName("WSDEB7");
            xTitledPanel1ContentContainer.add(WSDEB7);
            WSDEB7.setBounds(185, 236, 66, WSDEB7.getPreferredSize().height);

            //---- WSFIN7 ----
            WSFIN7.setComponentPopupMenu(BTD);
            WSFIN7.setName("WSFIN7");
            xTitledPanel1ContentContainer.add(WSFIN7);
            WSFIN7.setBounds(295, 236, 66, WSFIN7.getPreferredSize().height);

            //---- WSNUM8 ----
            WSNUM8.setComponentPopupMenu(BTD);
            WSNUM8.setName("WSNUM8");
            xTitledPanel1ContentContainer.add(WSNUM8);
            WSNUM8.setBounds(70, 267, 66, WSNUM8.getPreferredSize().height);

            //---- WSDEB8 ----
            WSDEB8.setComponentPopupMenu(BTD);
            WSDEB8.setName("WSDEB8");
            xTitledPanel1ContentContainer.add(WSDEB8);
            WSDEB8.setBounds(185, 267, 66, WSDEB8.getPreferredSize().height);

            //---- WSFIN8 ----
            WSFIN8.setComponentPopupMenu(BTD);
            WSFIN8.setName("WSFIN8");
            xTitledPanel1ContentContainer.add(WSFIN8);
            WSFIN8.setBounds(295, 267, 66, WSFIN8.getPreferredSize().height);

            //---- OBJ_54 ----
            OBJ_54.setText("01");
            OBJ_54.setName("OBJ_54");
            xTitledPanel1ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(20, 54, 18, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("02");
            OBJ_58.setName("OBJ_58");
            xTitledPanel1ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(20, 85, 18, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("03");
            OBJ_62.setName("OBJ_62");
            xTitledPanel1ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(20, 116, 18, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("04");
            OBJ_66.setName("OBJ_66");
            xTitledPanel1ContentContainer.add(OBJ_66);
            OBJ_66.setBounds(20, 147, 18, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("05");
            OBJ_70.setName("OBJ_70");
            xTitledPanel1ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(20, 178, 18, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("06");
            OBJ_74.setName("OBJ_74");
            xTitledPanel1ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(20, 209, 18, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("07");
            OBJ_78.setName("OBJ_78");
            xTitledPanel1ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(20, 240, 18, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("08");
            OBJ_82.setName("OBJ_82");
            xTitledPanel1ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(20, 271, 18, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 347, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_52;
  private JLabel OBJ_51;
  private JLabel OBJ_53;
  private XRiTextField WSNUM1;
  private XRiTextField WSDEB1;
  private XRiTextField WSFIN1;
  private XRiTextField WSNUM2;
  private XRiTextField WSDEB2;
  private XRiTextField WSFIN2;
  private XRiTextField WSNUM3;
  private XRiTextField WSDEB3;
  private XRiTextField WSFIN3;
  private XRiTextField WSNUM4;
  private XRiTextField WSDEB4;
  private XRiTextField WSFIN4;
  private XRiTextField WSNUM5;
  private XRiTextField WSDEB5;
  private XRiTextField WSFIN5;
  private XRiTextField WSNUM6;
  private XRiTextField WSDEB6;
  private XRiTextField WSFIN6;
  private XRiTextField WSNUM7;
  private XRiTextField WSDEB7;
  private XRiTextField WSFIN7;
  private XRiTextField WSNUM8;
  private XRiTextField WSDEB8;
  private XRiTextField WSFIN8;
  private JLabel OBJ_54;
  private JLabel OBJ_58;
  private JLabel OBJ_62;
  private JLabel OBJ_66;
  private JLabel OBJ_70;
  private JLabel OBJ_74;
  private JLabel OBJ_78;
  private JLabel OBJ_82;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
