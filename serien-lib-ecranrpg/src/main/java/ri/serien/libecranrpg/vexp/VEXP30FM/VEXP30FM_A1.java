
package ri.serien.libecranrpg.vexp.VEXP30FM;
// Nom Fichier: i_VEXP30FM_FMTA1_FMTF1_239.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP30FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP30FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX8.setValeurs("8", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX10.setValeurs("0", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX4.setValeurs("3", "RB");
    TIDX3.setValeurs("4", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX1.setValeurs("1", "RB");
    TSCAN.setValeursSelection("1", " ");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_Bib.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    ARG7.setVisible(lexique.isPresent("ARG7"));
    ARG10.setVisible(lexique.isPresent("ARG10"));
    ARG9.setVisible(lexique.isPresent("ARG9"));
    ARG5.setVisible(lexique.isPresent("ARG5"));
    ARG6X.setVisible(lexique.isPresent("ARG6X"));
    ARG1.setVisible(lexique.isPresent("ARG1"));
    INDNIN.setVisible(lexique.isPresent("INDNIN"));
    ARG2.setVisible(lexique.isPresent("ARG2"));
    ARG4.setVisible(lexique.isPresent("ARG4"));
    ARG3.setVisible(lexique.isPresent("ARG3"));
    SCAN.setVisible(lexique.isPresent("SCAN"));
    ARG8.setVisible(lexique.isPresent("ARG8"));
    // TIDX8.setVisible( lexique.isPresent("RB"));
    // TIDX8.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("8"));
    // TIDX7.setVisible( lexique.isPresent("RB"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // TIDX5.setVisible( lexique.isPresent("RB"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // TIDX10.setVisible( lexique.isPresent("RB"));
    // TIDX10.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("0"));
    // TIDX9.setVisible( lexique.isPresent("RB"));
    // TIDX9.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("9"));
    // TIDX4.setVisible( lexique.isPresent("RB"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX3.setVisible( lexique.isPresent("RB"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // TIDX2.setVisible( lexique.isPresent("RB"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // TIDX6.setVisible( lexique.isPresent("RB"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // TIDX1.setVisible( lexique.isPresent("RB"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TSCAN.setVisible( lexique.isPresent("TSCAN"));
    // TSCAN.setSelected(lexique.HostFieldGetData("TSCAN").equalsIgnoreCase("1"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    OBJ_77.setIcon(lexique.chargerImage("images/msgbox04.gif", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SERIE M - INTERVENTIONS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("RB", 0, "8");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TIDX10.isSelected())
    // lexique.HostFieldPutData("RB", 0, "0");
    // if (TIDX9.isSelected())
    // lexique.HostFieldPutData("RB", 0, "9");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TSCAN.isSelected())
    // lexique.HostFieldPutData("TSCAN", 0, "1");
    // else
    // lexique.HostFieldPutData("TSCAN", 0, " ");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_Bib = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    INDNIN = new XRiTextField();
    P_Centre = new JPanel();
    OBJ_22 = new JPanel();
    OBJ_74_OBJ_74 = new JLabel();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_77 = new JLabel();
    panel1 = new JPanel();
    TSCAN = new XRiCheckBox();
    TIDX1 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    ARG8 = new XRiTextField();
    SCAN = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG4 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG1 = new XRiTextField();
    ARG6X = new XRiTextField();
    ARG5 = new XRiTextField();
    ARG9 = new XRiTextField();
    ARG10 = new XRiTextField();
    ARG7 = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Recherche multi-crit\u00e8res");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
      CMD.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Cr\u00e9ation");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Modification");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Interrogation");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Annulation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Duplication");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("INTERVENTIONS");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- l_Bib ----
        l_Bib.setText("@V01F@");
        l_Bib.setFont(new Font("sansserif", Font.BOLD, 12));
        l_Bib.setName("l_Bib");

        //---- OBJ_66_OBJ_66 ----
        OBJ_66_OBJ_66.setText("Intervention");
        OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

        //---- INDNIN ----
        INDNIN.setComponentPopupMenu(BTD);
        INDNIN.setName("INDNIN");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
              .addGap(12, 12, 12)
              .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
              .addGap(1, 1, 1)
              .addComponent(INDNIN, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 593, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(7, 7, 7)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(9, 9, 9)
              .addComponent(OBJ_66_OBJ_66))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(INDNIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_Fonctions)))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //======== OBJ_22 ========
      {
        OBJ_22.setName("OBJ_22");
        OBJ_22.setLayout(null);

        //---- OBJ_74_OBJ_74 ----
        OBJ_74_OBJ_74.setText("Si aucune saisie, affichage des interventions selon leurs num\u00e9ros");
        OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");
        OBJ_22.add(OBJ_74_OBJ_74);
        OBJ_74_OBJ_74.setBounds(82, 14, 436, 20);

        //---- OBJ_75_OBJ_75 ----
        OBJ_75_OBJ_75.setText("Choisissez \u00e9ventuellement un ordre de tri.");
        OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
        OBJ_22.add(OBJ_75_OBJ_75);
        OBJ_75_OBJ_75.setBounds(82, 42, 249, 20);

        //---- OBJ_77 ----
        OBJ_77.setIcon(new ImageIcon("images/msgbox04.gif"));
        OBJ_77.setName("OBJ_77");
        OBJ_22.add(OBJ_77);
        OBJ_77.setBounds(6, 12, 48, 48);
      }

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- TSCAN ----
        TSCAN.setText("Portion de texte");
        TSCAN.setComponentPopupMenu(BTD);
        TSCAN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TSCAN.setName("TSCAN");
        panel1.add(TSCAN);
        TSCAN.setBounds(40, 344, 219, 20);

        //---- TIDX1 ----
        TIDX1.setText("Num\u00e9ro de fiche intervention");
        TIDX1.setToolTipText("Tri\u00e9 par");
        TIDX1.setComponentPopupMenu(BTD);
        TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX1.setName("TIDX1");
        panel1.add(TIDX1);
        TIDX1.setBounds(40, 44, 219, 20);

        //---- TIDX6 ----
        TIDX6.setText("Date de cr\u00e9ation");
        TIDX6.setToolTipText("Tri\u00e9 par");
        TIDX6.setComponentPopupMenu(BTD);
        TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX6.setName("TIDX6");
        panel1.add(TIDX6);
        TIDX6.setBounds(40, 74, 219, 20);

        //---- TIDX2 ----
        TIDX2.setText("Num\u00e9ro de client");
        TIDX2.setToolTipText("Tri\u00e9 par");
        TIDX2.setComponentPopupMenu(BTD);
        TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX2.setName("TIDX2");
        panel1.add(TIDX2);
        TIDX2.setBounds(40, 104, 219, 20);

        //---- TIDX3 ----
        TIDX3.setText("Programme concern\u00e9");
        TIDX3.setToolTipText("Tri\u00e9 par");
        TIDX3.setComponentPopupMenu(BTD);
        TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX3.setName("TIDX3");
        panel1.add(TIDX3);
        TIDX3.setBounds(40, 134, 219, 20);

        //---- TIDX4 ----
        TIDX4.setText("Point de menu");
        TIDX4.setToolTipText("Tri\u00e9 par");
        TIDX4.setComponentPopupMenu(BTD);
        TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX4.setName("TIDX4");
        panel1.add(TIDX4);
        TIDX4.setBounds(40, 164, 219, 20);

        //---- TIDX9 ----
        TIDX9.setText("Intervenant 1");
        TIDX9.setToolTipText("Tri\u00e9 par");
        TIDX9.setComponentPopupMenu(BTD);
        TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX9.setName("TIDX9");
        panel1.add(TIDX9);
        TIDX9.setBounds(40, 194, 219, 20);

        //---- TIDX10 ----
        TIDX10.setText("Intervenant 2");
        TIDX10.setToolTipText("Tri\u00e9 par");
        TIDX10.setComponentPopupMenu(BTD);
        TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX10.setName("TIDX10");
        panel1.add(TIDX10);
        TIDX10.setBounds(40, 224, 219, 20);

        //---- TIDX5 ----
        TIDX5.setText("Anosys");
        TIDX5.setToolTipText("Tri\u00e9 par");
        TIDX5.setComponentPopupMenu(BTD);
        TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX5.setName("TIDX5");
        panel1.add(TIDX5);
        TIDX5.setBounds(40, 254, 219, 20);

        //---- TIDX7 ----
        TIDX7.setText("Code \u00e9tat");
        TIDX7.setToolTipText("Tri\u00e9 par");
        TIDX7.setComponentPopupMenu(BTD);
        TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX7.setName("TIDX7");
        panel1.add(TIDX7);
        TIDX7.setBounds(40, 284, 219, 20);

        //---- TIDX8 ----
        TIDX8.setText("Mot de classement client");
        TIDX8.setToolTipText("Tri\u00e9 par");
        TIDX8.setComponentPopupMenu(BTD);
        TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX8.setName("TIDX8");
        panel1.add(TIDX8);
        TIDX8.setBounds(40, 314, 219, 20);

        //---- ARG8 ----
        ARG8.setComponentPopupMenu(BTD);
        ARG8.setName("ARG8");
        panel1.add(ARG8);
        ARG8.setBounds(270, 310, 155, ARG8.getPreferredSize().height);

        //---- SCAN ----
        SCAN.setComponentPopupMenu(BTD);
        SCAN.setName("SCAN");
        panel1.add(SCAN);
        SCAN.setBounds(270, 340, 155, SCAN.getPreferredSize().height);

        //---- ARG3 ----
        ARG3.setComponentPopupMenu(BTD);
        ARG3.setName("ARG3");
        panel1.add(ARG3);
        ARG3.setBounds(270, 130, 105, ARG3.getPreferredSize().height);

        //---- ARG4 ----
        ARG4.setComponentPopupMenu(BTD);
        ARG4.setName("ARG4");
        panel1.add(ARG4);
        ARG4.setBounds(270, 160, 85, ARG4.getPreferredSize().height);

        //---- ARG2 ----
        ARG2.setComponentPopupMenu(BTD);
        ARG2.setName("ARG2");
        panel1.add(ARG2);
        ARG2.setBounds(270, 100, 65, ARG2.getPreferredSize().height);

        //---- ARG1 ----
        ARG1.setComponentPopupMenu(BTD);
        ARG1.setName("ARG1");
        panel1.add(ARG1);
        ARG1.setBounds(270, 40, 60, ARG1.getPreferredSize().height);

        //---- ARG6X ----
        ARG6X.setComponentPopupMenu(BTD);
        ARG6X.setName("ARG6X");
        panel1.add(ARG6X);
        ARG6X.setBounds(270, 70, 50, ARG6X.getPreferredSize().height);

        //---- ARG5 ----
        ARG5.setComponentPopupMenu(BTD);
        ARG5.setName("ARG5");
        panel1.add(ARG5);
        ARG5.setBounds(270, 190, 35, ARG5.getPreferredSize().height);

        //---- ARG9 ----
        ARG9.setComponentPopupMenu(BTD);
        ARG9.setName("ARG9");
        panel1.add(ARG9);
        ARG9.setBounds(270, 220, 35, ARG9.getPreferredSize().height);

        //---- ARG10 ----
        ARG10.setComponentPopupMenu(BTD);
        ARG10.setName("ARG10");
        panel1.add(ARG10);
        ARG10.setBounds(270, 250, 35, ARG10.getPreferredSize().height);

        //---- ARG7 ----
        ARG7.setComponentPopupMenu(BTD);
        ARG7.setName("ARG7");
        panel1.add(ARG7);
        ARG7.setBounds(270, 280, 35, ARG7.getPreferredSize().height);
      }

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 607, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(25, 25, 25)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX10);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX8);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_Bib;
  private JLabel OBJ_66_OBJ_66;
  private XRiTextField INDNIN;
  private JPanel P_Centre;
  private JPanel OBJ_22;
  private JLabel OBJ_74_OBJ_74;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_77;
  private JPanel panel1;
  private XRiCheckBox TSCAN;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX8;
  private XRiTextField ARG8;
  private XRiTextField SCAN;
  private XRiTextField ARG3;
  private XRiTextField ARG4;
  private XRiTextField ARG2;
  private XRiTextField ARG1;
  private XRiTextField ARG6X;
  private XRiTextField ARG5;
  private XRiTextField ARG9;
  private XRiTextField ARG10;
  private XRiTextField ARG7;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
