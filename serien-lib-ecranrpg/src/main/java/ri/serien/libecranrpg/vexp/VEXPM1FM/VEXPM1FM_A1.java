
package ri.serien.libecranrpg.vexp.VEXPM1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXPM1FM_A1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  // 0=SMTP/Basic, 1=SMTP/TSL, 2=SMTP/SSL, 3=SMTP/STARTTLS (mettre à jour l'EnumProtocoleServeurMail)
  private String[] SMPROT_Value = { "0", "1", "2", "3" };
  
  public VEXPM1FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    SMPROT.setValeurs(SMPROT_Value, null);
    
    // Affichage du message de la note
    SNTitreNote.setMessage(Message.getMessageMoyen("Note"));
    // @formatter:off
    SNNote.setMessage(Message.getMessageNormal("Pour Microsoft 365, il est recommandé d'effectuer ce paramétrage :\n"
        + " - Sécurité de la connexion : SMTP avec chiffrement STARTTLS\n"
        + " - Nom du serveur : smtp-mail.outlook.com\n"
        + " - Port : 587\n"));
    // @formatter:on
    
    XRiBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    XRiBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    XRiBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion serveur"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    XRiBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlServeur = new SNPanelTitre();
    lbIdServeur = new SNLabelChamp();
    SMID = new XRiTextField();
    lbDescription = new SNLabelChamp();
    SMDESC = new XRiTextField();
    lbProtocole = new SNLabelChamp();
    SMPROT = new XRiComboBox();
    lbServeurHost = new SNLabelChamp();
    SMHOST = new XRiTextField();
    lbPortHost = new SNLabelChamp();
    SMPORT = new XRiTextField();
    lbUtilisateur = new SNLabelChamp();
    SMUSER = new XRiTextField();
    lbMotDePasse = new SNLabelChamp();
    SMPASS = new XRiTextField();
    lbUrlLogo = new SNLabelChamp();
    SMLOGO = new XRiTextField();
    SNTitreNote = new SNMessage();
    SNNote = new SNMessage();
    XRiBarreBouton = new XRiBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- bpPresentation ----
      bpPresentation.setText("Configuration d'un serveur de mail");
      bpPresentation.setName("bpPresentation");
      pnlNord.add(bpPresentation);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlServeur ========
      {
        pnlServeur.setTitre("D\u00e9tails du serveur");
        pnlServeur.setName("pnlServeur");
        pnlServeur.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlServeur.getLayout()).columnWidths = new int[] { 0, 734, 0 };
        ((GridBagLayout) pnlServeur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlServeur.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlServeur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ---- lbIdServeur ----
        lbIdServeur.setText("Identifiant du serveur");
        lbIdServeur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbIdServeur.setName("lbIdServeur");
        pnlServeur.add(lbIdServeur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMID ----
        SMID.setHorizontalAlignment(SwingConstants.RIGHT);
        SMID.setPreferredSize(new Dimension(100, 30));
        SMID.setFont(SMID.getFont().deriveFont(SMID.getFont().getSize() + 2f));
        SMID.setName("SMID");
        pnlServeur.add(SMID, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDescription ----
        lbDescription.setText("Description");
        lbDescription.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDescription.setName("lbDescription");
        pnlServeur.add(lbDescription, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMDESC ----
        SMDESC.setFont(new Font("sansserif", Font.PLAIN, 14));
        SMDESC.setBackground(Color.white);
        SMDESC.setName("SMDESC");
        pnlServeur.add(SMDESC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbProtocole ----
        lbProtocole.setText("S\u00e9curit\u00e9 de la connexion");
        lbProtocole.setHorizontalAlignment(SwingConstants.RIGHT);
        lbProtocole.setMaximumSize(new Dimension(200, 30));
        lbProtocole.setMinimumSize(new Dimension(200, 30));
        lbProtocole.setPreferredSize(new Dimension(200, 30));
        lbProtocole.setName("lbProtocole");
        pnlServeur.add(lbProtocole, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMPROT ----
        SMPROT.setModel(new DefaultComboBoxModel(new String[] { "SMTP sans chiffrement", "SMTP avec chiffrement TLS",
            "SMTP avec chiffrement SSL (exp\u00e9rimental)", "SMTP avec chiffrement STARTTLS" }));
        SMPROT.setPreferredSize(new Dimension(300, 30));
        SMPROT.setFont(SMPROT.getFont().deriveFont(SMPROT.getFont().getSize() + 2f));
        SMPROT.setName("SMPROT");
        pnlServeur.add(SMPROT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbServeurHost ----
        lbServeurHost.setText("Nom du serveur");
        lbServeurHost.setHorizontalAlignment(SwingConstants.RIGHT);
        lbServeurHost.setName("lbServeurHost");
        pnlServeur.add(lbServeurHost, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMHOST ----
        SMHOST.setFont(SMHOST.getFont().deriveFont(SMHOST.getFont().getSize() + 2f));
        SMHOST.setBackground(Color.white);
        SMHOST.setName("SMHOST");
        pnlServeur.add(SMHOST, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPortHost ----
        lbPortHost.setText("Port");
        lbPortHost.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPortHost.setName("lbPortHost");
        pnlServeur.add(lbPortHost, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMPORT ----
        SMPORT.setHorizontalAlignment(SwingConstants.RIGHT);
        SMPORT.setPreferredSize(new Dimension(100, 30));
        SMPORT.setFont(SMPORT.getFont().deriveFont(SMPORT.getFont().getSize() + 2f));
        SMPORT.setName("SMPORT");
        pnlServeur.add(SMPORT, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbUtilisateur ----
        lbUtilisateur.setText("Nom d'utilisateur");
        lbUtilisateur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbUtilisateur.setName("lbUtilisateur");
        pnlServeur.add(lbUtilisateur, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMUSER ----
        SMUSER.setFont(SMUSER.getFont().deriveFont(SMUSER.getFont().getSize() + 2f));
        SMUSER.setBackground(Color.white);
        SMUSER.setName("SMUSER");
        pnlServeur.add(SMUSER, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMotDePasse ----
        lbMotDePasse.setText("Mot de passe");
        lbMotDePasse.setHorizontalAlignment(SwingConstants.RIGHT);
        lbMotDePasse.setName("lbMotDePasse");
        pnlServeur.add(lbMotDePasse, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMPASS ----
        SMPASS.setPreferredSize(new Dimension(13, 30));
        SMPASS.setFont(SMPASS.getFont().deriveFont(SMPASS.getFont().getSize() + 2f));
        SMPASS.setName("SMPASS");
        pnlServeur.add(SMPASS, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbUrlLogo ----
        lbUrlLogo.setText("Url du logo");
        lbUrlLogo.setHorizontalAlignment(SwingConstants.RIGHT);
        lbUrlLogo.setName("lbUrlLogo");
        pnlServeur.add(lbUrlLogo, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- SMLOGO ----
        SMLOGO.setPreferredSize(new Dimension(13, 30));
        SMLOGO.setFont(SMLOGO.getFont().deriveFont(SMLOGO.getFont().getSize() + 2f));
        SMLOGO.setName("SMLOGO");
        pnlServeur.add(SMLOGO, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- SNTitreNote ----
        SNTitreNote.setName("SNTitreNote");
        pnlServeur.add(SNTitreNote, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- SNNote ----
        SNNote.setName("SNNote");
        pnlServeur.add(SNNote, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlServeur, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- XRiBarreBouton ----
    XRiBarreBouton.setName("XRiBarreBouton");
    add(XRiBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlServeur;
  private SNLabelChamp lbIdServeur;
  private XRiTextField SMID;
  private SNLabelChamp lbDescription;
  private XRiTextField SMDESC;
  private SNLabelChamp lbProtocole;
  private XRiComboBox SMPROT;
  private SNLabelChamp lbServeurHost;
  private XRiTextField SMHOST;
  private SNLabelChamp lbPortHost;
  private XRiTextField SMPORT;
  private SNLabelChamp lbUtilisateur;
  private XRiTextField SMUSER;
  private SNLabelChamp lbMotDePasse;
  private XRiTextField SMPASS;
  private SNLabelChamp lbUrlLogo;
  private XRiTextField SMLOGO;
  private SNMessage SNTitreNote;
  private SNMessage SNNote;
  private XRiBarreBouton XRiBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
