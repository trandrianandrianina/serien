
package ri.serien.libecranrpg.vexp.VEXP83FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiPasswordField;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VEXP83FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] USSTYP_Value = { "0", "1", };
  private String[] USSFON_Value = { " ", "3", };
  private String[] USSLAN_Value = { "2", "4", "5", "6", };
  private String[] USSTBL_Value = { ",", ";", };
  private String[] USSDSP_Value = { "N", "J", "S", "K", " ", };
  private String[] USSPOR_Value = { " ", "1", "2", "3" };
  
  public VEXP83FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    USSFON.setValeurs(USSFON_Value, null);
    USSLAN.setValeurs(USSLAN_Value, null);
    USSTBL.setValeurs(USSTBL_Value, null);
    USSDSP.setValeurs(USSDSP_Value, null);
    USSPOR.setValeurs(USSPOR_Value, null);
    USSTYP.setValeurs(USSTYP_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    USSPRF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@USSPRF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    USSMDP.setVisible(lexique.HostFieldGetData("USSDSP").equalsIgnoreCase("S") && USSMDP.isVisible());
    OBJ_82.setVisible(lexique.HostFieldGetData("USSDSP").equalsIgnoreCase("S"));
    OBJ_81.setVisible(lexique.HostFieldGetData("USSDSP").equalsIgnoreCase("S"));
    USSPRF.setVisible(lexique.isPresent("USSPRF"));
    USSPRT.setVisible(lexique.isPresent("USSPRT"));
    OBJ_68.setVisible(lexique.isPresent("USSTYP"));
    OBJ_80.setVisible(lexique.HostFieldGetData("USSDSP").equalsIgnoreCase("S"));
    USSMNP.setVisible(lexique.isPresent("USSMNP"));
    OBJ_66.setVisible(lexique.getMode() == Lexical.MODE_CREATION);
    CONPWD.setVisible(lexique.getMode() == Lexical.MODE_CREATION);
    // USSTYP.setVisible( lexique.isPresent("USSTYP"));
    xTitledPanel2.setVisible(lexique.HostFieldGetData("USSDSP").equalsIgnoreCase("S"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    USSPRF = new RiZoneSortie();
    OBJ_38 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    USSTYP = new XRiComboBox();
    OBJ_71 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_68 = new JLabel();
    USSPRT = new XRiTextField();
    USSBIB = new XRiTextField();
    USSPWD = new XRiPasswordField();
    CONPWD = new XRiPasswordField();
    USSPE = new XRiTextField();
    USSPOR = new XRiComboBox();
    USSDSP = new XRiComboBox();
    USSTBL = new XRiComboBox();
    OBJ_76 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_75 = new JLabel();
    USSMNP = new XRiTextField();
    OBJ_74 = new JLabel();
    USSMOD = new XRiTextField();
    label1 = new JLabel();
    USSNMC = new XRiTextField();
    OBJ_79 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    USSLAN = new XRiComboBox();
    USSFON = new XRiComboBox();
    OBJ_80 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    USSMDP = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion utilisateurs");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- USSPRF ----
          USSPRF.setComponentPopupMenu(BTD);
          USSPRF.setText("@USSPRF@");
          USSPRF.setOpaque(false);
          USSPRF.setName("USSPRF");

          //---- OBJ_38 ----
          OBJ_38.setText("Profil");
          OBJ_38.setName("OBJ_38");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(USSPRF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(USSPRF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(860, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMaximumSize(new Dimension(869, 550));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Informations principales");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- USSTYP ----
            USSTYP.setModel(new DefaultComboBoxModel(new String[] {
              "Utilisateur",
              "Officier"
            }));
            USSTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            USSTYP.setName("USSTYP");
            xTitledPanel1ContentContainer.add(USSTYP);
            USSTYP.setBounds(270, 105, 110, USSTYP.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("Biblioth\u00e8que de fichiers li\u00e9e au profil");
            OBJ_71.setName("OBJ_71");
            xTitledPanel1ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(15, 80, 230, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("Code personnel S\u00e9rie M du profil (PE)");
            OBJ_73.setName("OBJ_73");
            xTitledPanel1ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(15, 265, 230, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("Nom de l'imprimante li\u00e9e au profil");
            OBJ_72.setName("OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(15, 295, 230, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("Confirmation du mot de passe");
            OBJ_66.setName("OBJ_66");
            xTitledPanel1ContentContainer.add(OBJ_66);
            OBJ_66.setBounds(395, 15, 205, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("Mot de passe");
            OBJ_46.setName("OBJ_46");
            xTitledPanel1ContentContainer.add(OBJ_46);
            OBJ_46.setBounds(15, 15, 230, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Type du profil");
            OBJ_68.setName("OBJ_68");
            xTitledPanel1ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(15, 110, 230, 20);

            //---- USSPRT ----
            USSPRT.setComponentPopupMenu(BTD);
            USSPRT.setName("USSPRT");
            xTitledPanel1ContentContainer.add(USSPRT);
            USSPRT.setBounds(270, 290, 110, USSPRT.getPreferredSize().height);

            //---- USSBIB ----
            USSBIB.setComponentPopupMenu(BTD);
            USSBIB.setName("USSBIB");
            xTitledPanel1ContentContainer.add(USSBIB);
            USSBIB.setBounds(270, 75, 60, USSBIB.getPreferredSize().height);

            //---- USSPWD ----
            USSPWD.setComponentPopupMenu(BTD);
            USSPWD.setName("USSPWD");
            xTitledPanel1ContentContainer.add(USSPWD);
            USSPWD.setBounds(270, 11, 110, USSPWD.getPreferredSize().height);

            //---- CONPWD ----
            CONPWD.setComponentPopupMenu(BTD);
            CONPWD.setName("CONPWD");
            xTitledPanel1ContentContainer.add(CONPWD);
            CONPWD.setBounds(605, 11, 110, CONPWD.getPreferredSize().height);

            //---- USSPE ----
            USSPE.setComponentPopupMenu(BTD);
            USSPE.setName("USSPE");
            xTitledPanel1ContentContainer.add(USSPE);
            USSPE.setBounds(270, 260, 40, USSPE.getPreferredSize().height);

            //---- USSPOR ----
            USSPOR.setModel(new DefaultComboBoxModel(new String[] {
              "Toutes",
              "Restreintes",
              "Restreintes sauf s\u00e9lection biblioth\u00e8ques",
              "Restreintes sauf s\u00e9curit\u00e9/profils"
            }));
            USSPOR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            USSPOR.setName("USSPOR");
            xTitledPanel1ContentContainer.add(USSPOR);
            USSPOR.setBounds(270, 135, 265, USSPOR.getPreferredSize().height);

            //---- USSDSP ----
            USSDSP.setModel(new DefaultComboBoxModel(new String[] {
              "S\u00e9rie N",
              "Graphique",
              "SyVox",
              "Teklogix",
              "5250"
            }));
            USSDSP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            USSDSP.setName("USSDSP");
            xTitledPanel1ContentContainer.add(USSDSP);
            USSDSP.setBounds(270, 165, 110, USSDSP.getPreferredSize().height);

            //---- USSTBL ----
            USSTBL.setModel(new DefaultComboBoxModel(new String[] {
              "Virgule",
              "Point-virgule"
            }));
            USSTBL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            USSTBL.setName("USSTBL");
            xTitledPanel1ContentContainer.add(USSTBL);
            USSTBL.setBounds(605, 295, 120, USSTBL.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("Type d'\u00e9mulation");
            OBJ_76.setName("OBJ_76");
            xTitledPanel1ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(15, 170, 230, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("S\u00e9parateur de zones pour tableur");
            OBJ_67.setName("OBJ_67");
            xTitledPanel1ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(395, 295, 205, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("Module S\u00e9rie M \u00e0 l'ouverture");
            OBJ_75.setName("OBJ_75");
            xTitledPanel1ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(15, 200, 230, 20);

            //---- USSMNP ----
            USSMNP.setComponentPopupMenu(BTD);
            USSMNP.setName("USSMNP");
            xTitledPanel1ContentContainer.add(USSMNP);
            USSMNP.setBounds(270, 230, 210, USSMNP.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("Code menu personnalis\u00e9");
            OBJ_74.setName("OBJ_74");
            xTitledPanel1ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(15, 230, 230, 20);

            //---- USSMOD ----
            USSMOD.setComponentPopupMenu(BTD);
            USSMOD.setName("USSMOD");
            xTitledPanel1ContentContainer.add(USSMOD);
            USSMOD.setBounds(270, 195, 40, USSMOD.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Possibilit\u00e9s");
            label1.setName("label1");
            xTitledPanel1ContentContainer.add(label1);
            label1.setBounds(15, 140, 230, 20);

            //---- USSNMC ----
            USSNMC.setComponentPopupMenu(BTD);
            USSNMC.setName("USSNMC");
            xTitledPanel1ContentContainer.add(USSNMC);
            USSNMC.setBounds(270, 45, 310, USSNMC.getPreferredSize().height);

            //---- OBJ_79 ----
            OBJ_79.setText("Nom complet");
            OBJ_79.setName("OBJ_79");
            xTitledPanel1ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(15, 49, 220, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Informations compl\u00e9mentaires");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- USSLAN ----
            USSLAN.setModel(new DefaultComboBoxModel(new String[] {
              "Fran\u00e7ais",
              "Espagnol",
              "Anglais",
              "Allemand"
            }));
            USSLAN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            USSLAN.setName("USSLAN");
            xTitledPanel2ContentContainer.add(USSLAN);
            USSLAN.setBounds(270, 75, 110, USSLAN.getPreferredSize().height);

            //---- USSFON ----
            USSFON.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Picker"
            }));
            USSFON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            USSFON.setName("USSFON");
            xTitledPanel2ContentContainer.add(USSFON);
            USSFON.setBounds(270, 45, 110, USSFON.getPreferredSize().height);

            //---- OBJ_80 ----
            OBJ_80.setText("Mot de passe (Tiers)");
            OBJ_80.setName("OBJ_80");
            xTitledPanel2ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(15, 20, 145, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Fonction");
            OBJ_81.setName("OBJ_81");
            xTitledPanel2ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(15, 50, 73, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Langue");
            OBJ_82.setName("OBJ_82");
            xTitledPanel2ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(15, 80, 73, 20);

            //---- USSMDP ----
            USSMDP.setComponentPopupMenu(BTD);
            USSMDP.setName("USSMDP");
            xTitledPanel2ContentContainer.add(USSMDP);
            USSMDP.setBounds(270, 15, 70, USSMDP.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 808, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 806, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 362, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie USSPRF;
  private JLabel OBJ_38;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox USSTYP;
  private JLabel OBJ_71;
  private JLabel OBJ_73;
  private JLabel OBJ_72;
  private JLabel OBJ_66;
  private JLabel OBJ_46;
  private JLabel OBJ_68;
  private XRiTextField USSPRT;
  private XRiTextField USSBIB;
  private XRiPasswordField USSPWD;
  private XRiPasswordField CONPWD;
  private XRiTextField USSPE;
  private XRiComboBox USSPOR;
  private XRiComboBox USSDSP;
  private XRiComboBox USSTBL;
  private JLabel OBJ_76;
  private JLabel OBJ_67;
  private JLabel OBJ_75;
  private XRiTextField USSMNP;
  private JLabel OBJ_74;
  private XRiTextField USSMOD;
  private JLabel label1;
  private XRiTextField USSNMC;
  private JLabel OBJ_79;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox USSLAN;
  private XRiComboBox USSFON;
  private JLabel OBJ_80;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private XRiTextField USSMDP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
