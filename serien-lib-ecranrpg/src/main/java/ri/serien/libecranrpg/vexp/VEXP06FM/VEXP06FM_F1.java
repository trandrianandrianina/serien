
package ri.serien.libecranrpg.vexp.VEXP06FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VEXP06FM_F1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP06FM_F1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OK);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OK.setIcon(lexique.chargerImage("images/OK_p.png", true));
    ANN.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@V03F@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OKActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void ANNActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JLabel();
    OK = new JButton();
    ANN = new JButton();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setName("this");
    setLayout(null);

    //---- OBJ_5 ----
    OBJ_5.setText("Veullez confimer");
    OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_5.setFont(OBJ_5.getFont().deriveFont(OBJ_5.getFont().getStyle() | Font.BOLD, OBJ_5.getFont().getSize() + 2f));
    OBJ_5.setForeground(Color.white);
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(0, 5, 270, 30);

    //---- OK ----
    OK.setText("Valider");
    OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OK.setPreferredSize(new Dimension(110, 19));
    OK.setMinimumSize(new Dimension(110, 1));
    OK.setFont(OK.getFont().deriveFont(OK.getFont().getStyle() | Font.BOLD, OK.getFont().getSize() + 2f));
    OK.setIconTextGap(25);
    OK.setHorizontalAlignment(SwingConstants.LEADING);
    OK.setName("OK");
    OK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OKActionPerformed();
      }
    });
    add(OK);
    OK.setBounds(40, 40, 190, 40);

    //---- ANN ----
    ANN.setText(" Retour");
    ANN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ANN.setPreferredSize(new Dimension(120, 19));
    ANN.setMinimumSize(new Dimension(120, 1));
    ANN.setFont(new Font("sansserif", Font.BOLD, 14));
    ANN.setIconTextGap(25);
    ANN.setHorizontalAlignment(SwingConstants.LEADING);
    ANN.setName("ANN");
    ANN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ANNActionPerformed();
      }
    });
    add(ANN);
    ANN.setBounds(40, 80, 190, 40);

    setPreferredSize(new Dimension(270, 130));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_5;
  private JButton OK;
  private JButton ANN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
