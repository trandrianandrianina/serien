
package ri.serien.libecranrpg.vexp.VEXP01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP01FX_FX extends SNPanelEcranRPG implements ioFrame {
  
  /**
   * Constructeur
   */
  public VEXP01FX_FX(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    FXETA.setValeursSelection("1", "0");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // Gestion des champs en fonction du code du flux
    int codeFlux = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("INDIND"));
    switch (codeFlux) {
      // Flux suivi de commande
      case 3:
        initialiserEcranAvecHeure();
        lbIntervalle.setVisible(false);
        FXINT.setVisible(false);
        break;
      
      // Flux client
      case 4:
        initialiserEcranAvecHeure();
        lbIntervalle.setVisible(false);
        FXINT.setVisible(false);
        break;
      
      // Flux stock
      case 6:
        initialiserEcranPourFluxStock();
        lbIntervalle.setVisible(true);
        FXINT.setVisible(true);
        break;
      
      // Flux prix
      case 13:
        initialiserEcranAvecDateEtHeure();
        lbIntervalle.setVisible(false);
        FXINT.setVisible(false);
        break;
      
      // Les autres flux
      default:
        initialiserEcranSansDateEtHeure();
        lbIntervalle.setVisible(true);
        FXINT.setVisible(true);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // -- Méthodes privées
  
  /**
   * Affiche spécifiquement les champs pour le flux stock.
   */
  private void initialiserEcranPourFluxStock() {
    pnlDateDeclenchement.setVisible(true);
    lbDate1.setText("Heure / minute pour l'envoi du stock complet");
    lbDate2.setVisible(false);
    lbDate3.setVisible(false);
    lbDate4.setVisible(false);
    pnlDateHeure1.setVisible(true);
    pnlDateHeure2.setVisible(false);
    pnlDateHeure3.setVisible(false);
    pnlDateHeure4.setVisible(false);
    controlerPuce();
    pnlJourMois1.setVisible(false);
    lbNote.setVisible(true);
    lbNote.setText(
        "<html>L'heure d'envoi est facultative. Si une heure de déclenchement n'est pas défini alors seuls les mouvements de stock"
            + " seront envoyés à intervalle régulier.</html>");
  }
  
  /**
   * Affiche les champs avec la date et l'heure.
   */
  private void initialiserEcranAvecDateEtHeure() {
    pnlDateDeclenchement.setVisible(true);
    lbDate1.setVisible(true);
    lbDate2.setVisible(true);
    lbDate3.setVisible(true);
    lbDate4.setVisible(true);
    lbDate1.setText("1 - Jour / Mois / Heure / minute d'envoi");
    lbDate2.setText("2 - Jour / Mois / Heure / minute d'envoi");
    lbDate3.setText("3 - Jour / Mois / Heure / minute d'envoi");
    lbDate4.setText("4 - Jour / Mois / Heure / minute d'envoi");
    pnlDateHeure1.setVisible(true);
    pnlDateHeure2.setVisible(true);
    pnlDateHeure3.setVisible(true);
    pnlDateHeure4.setVisible(true);
    controlerPuce();
    pnlJourMois1.setVisible(false);
    pnlJourMois1.setVisible(true);
    pnlJourMois2.setVisible(true);
    pnlJourMois3.setVisible(true);
    pnlJourMois4.setVisible(true);
    lbNote.setVisible(true);
    lbNote.setText(
        "<html><u>Note pour les flux V3</u><br>Le flux à 2 modes de déclenchement :<br> - soit aucune heure n'est saisie alors le flux sera déclenché à"
            + " intervalle régulier comme cela a été paramétré dans le fichier msflux.ini avec le mot clé <b>delai</b>,<br> - soit le flux sera"
            + " déclenché uniquement aux dates et heures qui ont été saisie (de 1 à 4 possibles).</html>");
  }
  
  /**
   * Affiche les champs avec l'heure seulement.
   */
  private void initialiserEcranAvecHeure() {
    pnlDateDeclenchement.setVisible(true);
    lbDate1.setVisible(true);
    lbDate2.setVisible(true);
    lbDate3.setVisible(true);
    lbDate4.setVisible(true);
    lbDate1.setText("1 - Heure / minute d'envoi");
    lbDate2.setText("2 - Heure / minute d'envoi");
    lbDate3.setText("3 - Heure / minute d'envoi");
    lbDate4.setText("4 - Heure / minute d'envoi");
    pnlDateHeure1.setVisible(true);
    pnlDateHeure2.setVisible(true);
    pnlDateHeure3.setVisible(true);
    pnlDateHeure4.setVisible(true);
    controlerPuce();
    pnlJourMois1.setVisible(false);
    pnlJourMois2.setVisible(false);
    pnlJourMois3.setVisible(false);
    pnlJourMois4.setVisible(false);
    lbNote.setVisible(true);
    lbNote.setText(
        "<html><u>Note pour les flux V3</u><br>Le flux à 2 modes de déclenchement :<br> - soit aucune heure n'est saisie alors le flux sera déclenché à"
            + " intervalle régulier comme cela a été paramétré dans le fichier msflux.ini avec le mot clé <b>delai</b>,<br> - soit le flux sera"
            + " déclenché uniquement aux heures qui ont été saisie (de 1 à 4 possibles).</html>");
  }
  
  /**
   * Affiche les champs sans la date et l'heure.
   */
  private void initialiserEcranSansDateEtHeure() {
    pnlDateDeclenchement.setVisible(false);
    
  }
  
  /**
   * Contrôle l'apparitiond des puces.
   */
  private void controlerPuce() {
    if (Constantes.convertirTexteEnInteger(FXHH1.getText()) == 0 && Constantes.convertirTexteEnInteger(FXMN1.getText()) == 0) {
      lbPuce1.setVisible(false);
    }
    else {
      lbPuce1.setVisible(true);
    }
    if (Constantes.convertirTexteEnInteger(FXHH2.getText()) == 0 && Constantes.convertirTexteEnInteger(FXMN2.getText()) == 0) {
      lbPuce2.setVisible(false);
    }
    else {
      lbPuce2.setVisible(true);
    }
    if (Constantes.convertirTexteEnInteger(FXHH3.getText()) == 0 && Constantes.convertirTexteEnInteger(FXMN3.getText()) == 0) {
      lbPuce3.setVisible(false);
    }
    else {
      lbPuce3.setVisible(true);
    }
    if (Constantes.convertirTexteEnInteger(FXHH4.getText()) == 0 && Constantes.convertirTexteEnInteger(FXMN4.getText()) == 0) {
      lbPuce4.setVisible(false);
    }
    else {
      lbPuce4.setVisible(true);
    }
  }
  
  /**
   * Contrôle la valeur du jour.
   */
  private void controlerJour(String pValeur) {
    Integer entier = Constantes.convertirTexteEnInteger(pValeur);
    if (entier.intValue() < 0 || entier.intValue() > 31) {
      throw new MessageErreurException("La valeur du jour est invalide : elle doit être comprise entre 0 et 31 inclus.");
    }
    controlerPuce();
  }
  
  /**
   * Contrôle la valeur du mois.
   */
  private void controlerMois(String pValeur) {
    Integer entier = Constantes.convertirTexteEnInteger(pValeur);
    if (entier.intValue() < 0 || entier.intValue() > 12) {
      throw new MessageErreurException("La valeur du mois est invalide : elle doit être comprise entre 0 et 12 inclus.");
    }
    controlerPuce();
  }
  
  /**
   * Contrôle la valeur de l'heure.
   */
  private void controlerHeure(String pValeur) {
    Integer entier = Constantes.convertirTexteEnInteger(pValeur);
    if (entier.intValue() < 0 || entier.intValue() > 23) {
      throw new MessageErreurException("La valeur de l'heure est invalide : elle doit être comprise entre 0 et 23 inclus.");
    }
    controlerPuce();
  }
  
  /**
   * Contrôle la valeur des minutes.
   */
  private void controlerMinute(String pValeur) {
    Integer entier = Constantes.convertirTexteEnInteger(pValeur);
    if (entier.intValue() < 0 || entier.intValue() > 59) {
      throw new MessageErreurException("La valeur des minutes est invalide : elle doit être comprise entre 0 et 59 inclus.");
    }
    controlerPuce();
  }
  
  // -- Méthodes évènementielles
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void FXJJ1FocusLost(FocusEvent e) {
    try {
      controlerJour(FXJJ1.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXJJ2FocusLost(FocusEvent e) {
    try {
      controlerJour(FXJJ2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXJJ3FocusLost(FocusEvent e) {
    try {
      controlerJour(FXJJ3.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXJJ4FocusLost(FocusEvent e) {
    try {
      controlerJour(FXJJ4.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMM1FocusLost(FocusEvent e) {
    try {
      controlerMois(FXMM1.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMM2FocusLost(FocusEvent e) {
    try {
      controlerMois(FXMM2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMM3FocusLost(FocusEvent e) {
    try {
      controlerMois(FXMM3.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMM4FocusLost(FocusEvent e) {
    try {
      controlerMois(FXMM4.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXHH1FocusLost(FocusEvent e) {
    try {
      controlerHeure(FXHH1.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXHH2FocusLost(FocusEvent e) {
    try {
      controlerHeure(FXHH2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXHH3FocusLost(FocusEvent e) {
    try {
      controlerHeure(FXHH3.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXHH4FocusLost(FocusEvent e) {
    try {
      controlerHeure(FXHH4.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMN1FocusLost(FocusEvent e) {
    try {
      controlerMinute(FXMN1.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMN2FocusLost(FocusEvent e) {
    try {
      controlerMinute(FXMN2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMN3FocusLost(FocusEvent e) {
    try {
      controlerMinute(FXMN3.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void FXMN4FocusLost(FocusEvent e) {
    try {
      controlerMinute(FXMN4.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    pnlPersonnalisation = new SNPanelContenu();
    lbLibelle = new JLabel();
    FXLIB = new XRiTextField();
    FXETA = new XRiCheckBox();
    pnlDateDeclenchement = new SNPanel();
    lbDate1 = new JLabel();
    pnlDateHeure1 = new SNPanel();
    pnlJourMois1 = new SNPanel();
    FXJJ1 = new XRiTextField();
    lbSeparateurDM1 = new JLabel();
    FXMM1 = new XRiTextField();
    lbA1 = new JLabel();
    pnlHeureMinute1 = new SNPanel();
    FXHH1 = new XRiTextField();
    lbSeparateurHM1 = new JLabel();
    FXMN1 = new XRiTextField();
    lbPuce1 = new JLabel();
    lbNote = new JLabel();
    lbDate2 = new JLabel();
    pnlDateHeure2 = new SNPanel();
    pnlJourMois2 = new SNPanel();
    FXJJ2 = new XRiTextField();
    lbSeparateurDM2 = new JLabel();
    FXMM2 = new XRiTextField();
    lbA2 = new JLabel();
    pnlHeureMinute2 = new SNPanel();
    FXHH2 = new XRiTextField();
    lbSeparateurHM2 = new JLabel();
    FXMN2 = new XRiTextField();
    lbPuce2 = new JLabel();
    lbDate3 = new JLabel();
    pnlDateHeure3 = new SNPanel();
    pnlJourMois3 = new SNPanel();
    FXJJ3 = new XRiTextField();
    lbSeparateurDM3 = new JLabel();
    FXMM3 = new XRiTextField();
    lbA3 = new JLabel();
    pnlHeureMinute3 = new SNPanel();
    FXHH3 = new XRiTextField();
    lbSeparateurHM3 = new JLabel();
    FXMN3 = new XRiTextField();
    lbPuce3 = new JLabel();
    lbDate4 = new JLabel();
    pnlDateHeure4 = new SNPanel();
    pnlJourMois4 = new SNPanel();
    FXJJ4 = new XRiTextField();
    lbSeparateurDM4 = new JLabel();
    FXMM4 = new XRiTextField();
    lbA4 = new JLabel();
    pnlHeureMinute4 = new SNPanel();
    FXHH4 = new XRiTextField();
    lbSeparateurHM4 = new JLabel();
    FXMN4 = new XRiTextField();
    lbPuce4 = new JLabel();
    lbIntervalle = new JLabel();
    FXINT = new XRiTextField();
    lbCodeFtp = new JLabel();
    FXFTP = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de l'exploitation");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          
          // ---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          
          // ---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE).addGap(16, 16, 16)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(INDIND, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 350));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(700, 350));
          p_contenu.setName("p_contenu");
          
          // ======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Personnalisation de flux");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setPreferredSize(new Dimension(650, 187));
            xTitledPanel1.setMinimumSize(new Dimension(650, 187));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(new BorderLayout());
            
            // ======== pnlPersonnalisation ========
            {
              pnlPersonnalisation.setName("pnlPersonnalisation");
              pnlPersonnalisation.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlPersonnalisation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPersonnalisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbLibelle ----
              lbLibelle.setText("Libell\u00e9");
              lbLibelle.setMaximumSize(new Dimension(250, 30));
              lbLibelle.setMinimumSize(new Dimension(250, 30));
              lbLibelle.setPreferredSize(new Dimension(250, 30));
              lbLibelle.setName("lbLibelle");
              pnlPersonnalisation.add(lbLibelle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FXLIB ----
              FXLIB.setComponentPopupMenu(BTD);
              FXLIB.setPreferredSize(new Dimension(13, 30));
              FXLIB.setName("FXLIB");
              pnlPersonnalisation.add(FXLIB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FXETA ----
              FXETA.setText("activer (pour les flux automatiques uniquement)");
              FXETA.setPreferredSize(new Dimension(44, 30));
              FXETA.setName("FXETA");
              pnlPersonnalisation.add(FXETA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== pnlDateDeclenchement ========
              {
                pnlDateDeclenchement.setName("pnlDateDeclenchement");
                pnlDateDeclenchement.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlDateDeclenchement.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlDateDeclenchement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlDateDeclenchement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlDateDeclenchement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbDate1 ----
                lbDate1.setText("1 - Jour / Mois / Heure / minute d'envoi");
                lbDate1.setPreferredSize(new Dimension(250, 30));
                lbDate1.setMaximumSize(new Dimension(250, 30));
                lbDate1.setMinimumSize(new Dimension(250, 30));
                lbDate1.setName("lbDate1");
                pnlDateDeclenchement.add(lbDate1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlDateHeure1 ========
                {
                  pnlDateHeure1.setPreferredSize(new Dimension(200, 30));
                  pnlDateHeure1.setMinimumSize(new Dimension(200, 30));
                  pnlDateHeure1.setName("pnlDateHeure1");
                  pnlDateHeure1.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlDateHeure1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlDateHeure1.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlDateHeure1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlDateHeure1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ======== pnlJourMois1 ========
                  {
                    pnlJourMois1.setPreferredSize(new Dimension(90, 30));
                    pnlJourMois1.setMinimumSize(new Dimension(90, 30));
                    pnlJourMois1.setName("pnlJourMois1");
                    pnlJourMois1.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlJourMois1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlJourMois1.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlJourMois1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlJourMois1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXJJ1 ----
                    FXJJ1.setComponentPopupMenu(BTD);
                    FXJJ1.setPreferredSize(new Dimension(30, 30));
                    FXJJ1.setName("FXJJ1");
                    FXJJ1.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXJJ1FocusLost(e);
                      }
                    });
                    pnlJourMois1.add(FXJJ1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurDM1 ----
                    lbSeparateurDM1.setText("/");
                    lbSeparateurDM1.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurDM1.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurDM1.setMinimumSize(new Dimension(4, 30));
                    lbSeparateurDM1.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurDM1.setName("lbSeparateurDM1");
                    pnlJourMois1.add(lbSeparateurDM1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMM1 ----
                    FXMM1.setComponentPopupMenu(BTD);
                    FXMM1.setPreferredSize(new Dimension(30, 30));
                    FXMM1.setName("FXMM1");
                    FXMM1.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMM1FocusLost(e);
                      }
                    });
                    pnlJourMois1.add(FXMM1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                        GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbA1 ----
                    lbA1.setText("\u00e0");
                    lbA1.setHorizontalAlignment(SwingConstants.CENTER);
                    lbA1.setName("lbA1");
                    pnlJourMois1.add(lbA1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure1.add(pnlJourMois1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                      GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ======== pnlHeureMinute1 ========
                  {
                    pnlHeureMinute1.setPreferredSize(new Dimension(105, 30));
                    pnlHeureMinute1.setMinimumSize(new Dimension(105, 30));
                    pnlHeureMinute1.setName("pnlHeureMinute1");
                    pnlHeureMinute1.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlHeureMinute1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlHeureMinute1.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlHeureMinute1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlHeureMinute1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXHH1 ----
                    FXHH1.setComponentPopupMenu(BTD);
                    FXHH1.setPreferredSize(new Dimension(30, 30));
                    FXHH1.setMinimumSize(new Dimension(30, 30));
                    FXHH1.setName("FXHH1");
                    FXHH1.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXHH1FocusLost(e);
                      }
                    });
                    pnlHeureMinute1.add(FXHH1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurHM1 ----
                    lbSeparateurHM1.setText(":");
                    lbSeparateurHM1.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurHM1.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurHM1.setMinimumSize(new Dimension(5, 30));
                    lbSeparateurHM1.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurHM1.setName("lbSeparateurHM1");
                    pnlHeureMinute1.add(lbSeparateurHM1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMN1 ----
                    FXMN1.setComponentPopupMenu(BTD);
                    FXMN1.setPreferredSize(new Dimension(30, 30));
                    FXMN1.setMinimumSize(new Dimension(30, 30));
                    FXMN1.setName("FXMN1");
                    FXMN1.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMN1FocusLost(e);
                      }
                    });
                    pnlHeureMinute1.add(FXMN1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbPuce1 ----
                    lbPuce1.setIcon(new ImageIcon(getClass().getResource("/images/vert3.gif")));
                    lbPuce1.setMaximumSize(new Dimension(16, 30));
                    lbPuce1.setMinimumSize(new Dimension(16, 30));
                    lbPuce1.setPreferredSize(new Dimension(16, 30));
                    lbPuce1.setHorizontalTextPosition(SwingConstants.LEFT);
                    lbPuce1.setHorizontalAlignment(SwingConstants.CENTER);
                    lbPuce1.setName("lbPuce1");
                    pnlHeureMinute1.add(lbPuce1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                        GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure1.add(pnlHeureMinute1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlDateDeclenchement.add(pnlDateHeure1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbNote ----
                lbNote.setText("La note");
                lbNote.setName("lbNote");
                pnlDateDeclenchement.add(lbNote, new GridBagConstraints(2, 0, 1, 4, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                
                // ---- lbDate2 ----
                lbDate2.setText("2 - Jour / Mois / Heure / minute d'envoi");
                lbDate2.setMaximumSize(new Dimension(250, 30));
                lbDate2.setMinimumSize(new Dimension(250, 30));
                lbDate2.setPreferredSize(new Dimension(250, 30));
                lbDate2.setName("lbDate2");
                pnlDateDeclenchement.add(lbDate2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlDateHeure2 ========
                {
                  pnlDateHeure2.setPreferredSize(new Dimension(200, 30));
                  pnlDateHeure2.setMinimumSize(new Dimension(200, 30));
                  pnlDateHeure2.setName("pnlDateHeure2");
                  pnlDateHeure2.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlDateHeure2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlDateHeure2.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlDateHeure2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlDateHeure2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ======== pnlJourMois2 ========
                  {
                    pnlJourMois2.setPreferredSize(new Dimension(90, 30));
                    pnlJourMois2.setMinimumSize(new Dimension(90, 30));
                    pnlJourMois2.setName("pnlJourMois2");
                    pnlJourMois2.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlJourMois2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlJourMois2.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlJourMois2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlJourMois2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXJJ2 ----
                    FXJJ2.setComponentPopupMenu(BTD);
                    FXJJ2.setPreferredSize(new Dimension(30, 30));
                    FXJJ2.setName("FXJJ2");
                    FXJJ2.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXJJ2FocusLost(e);
                      }
                    });
                    pnlJourMois2.add(FXJJ2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurDM2 ----
                    lbSeparateurDM2.setText("/");
                    lbSeparateurDM2.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurDM2.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurDM2.setMinimumSize(new Dimension(4, 30));
                    lbSeparateurDM2.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurDM2.setName("lbSeparateurDM2");
                    pnlJourMois2.add(lbSeparateurDM2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMM2 ----
                    FXMM2.setComponentPopupMenu(BTD);
                    FXMM2.setPreferredSize(new Dimension(30, 30));
                    FXMM2.setName("FXMM2");
                    FXMM2.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMM2FocusLost(e);
                      }
                    });
                    pnlJourMois2.add(FXMM2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                        GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbA2 ----
                    lbA2.setText("\u00e0");
                    lbA2.setHorizontalAlignment(SwingConstants.CENTER);
                    lbA2.setName("lbA2");
                    pnlJourMois2.add(lbA2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure2.add(pnlJourMois2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                      GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ======== pnlHeureMinute2 ========
                  {
                    pnlHeureMinute2.setPreferredSize(new Dimension(105, 30));
                    pnlHeureMinute2.setMinimumSize(new Dimension(105, 30));
                    pnlHeureMinute2.setName("pnlHeureMinute2");
                    pnlHeureMinute2.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlHeureMinute2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlHeureMinute2.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlHeureMinute2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlHeureMinute2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXHH2 ----
                    FXHH2.setComponentPopupMenu(BTD);
                    FXHH2.setPreferredSize(new Dimension(30, 30));
                    FXHH2.setMinimumSize(new Dimension(30, 30));
                    FXHH2.setName("FXHH2");
                    FXHH2.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXHH2FocusLost(e);
                      }
                    });
                    pnlHeureMinute2.add(FXHH2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurHM2 ----
                    lbSeparateurHM2.setText(":");
                    lbSeparateurHM2.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurHM2.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurHM2.setMinimumSize(new Dimension(5, 30));
                    lbSeparateurHM2.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurHM2.setName("lbSeparateurHM2");
                    pnlHeureMinute2.add(lbSeparateurHM2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMN2 ----
                    FXMN2.setComponentPopupMenu(BTD);
                    FXMN2.setPreferredSize(new Dimension(30, 30));
                    FXMN2.setMinimumSize(new Dimension(30, 30));
                    FXMN2.setName("FXMN2");
                    FXMN2.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMN2FocusLost(e);
                      }
                    });
                    pnlHeureMinute2.add(FXMN2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbPuce2 ----
                    lbPuce2.setIcon(new ImageIcon(getClass().getResource("/images/vert3.gif")));
                    lbPuce2.setMaximumSize(new Dimension(16, 30));
                    lbPuce2.setMinimumSize(new Dimension(16, 30));
                    lbPuce2.setPreferredSize(new Dimension(16, 30));
                    lbPuce2.setHorizontalTextPosition(SwingConstants.LEFT);
                    lbPuce2.setHorizontalAlignment(SwingConstants.CENTER);
                    lbPuce2.setName("lbPuce2");
                    pnlHeureMinute2.add(lbPuce2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure2.add(pnlHeureMinute2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlDateDeclenchement.add(pnlDateHeure2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbDate3 ----
                lbDate3.setText("3 - Jour / Mois / Heure / minute d'envoi");
                lbDate3.setMaximumSize(new Dimension(250, 30));
                lbDate3.setMinimumSize(new Dimension(250, 30));
                lbDate3.setPreferredSize(new Dimension(250, 30));
                lbDate3.setName("lbDate3");
                pnlDateDeclenchement.add(lbDate3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlDateHeure3 ========
                {
                  pnlDateHeure3.setPreferredSize(new Dimension(200, 30));
                  pnlDateHeure3.setMinimumSize(new Dimension(200, 30));
                  pnlDateHeure3.setName("pnlDateHeure3");
                  pnlDateHeure3.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlDateHeure3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlDateHeure3.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlDateHeure3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlDateHeure3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ======== pnlJourMois3 ========
                  {
                    pnlJourMois3.setPreferredSize(new Dimension(90, 30));
                    pnlJourMois3.setMinimumSize(new Dimension(90, 30));
                    pnlJourMois3.setName("pnlJourMois3");
                    pnlJourMois3.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlJourMois3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlJourMois3.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlJourMois3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlJourMois3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXJJ3 ----
                    FXJJ3.setComponentPopupMenu(BTD);
                    FXJJ3.setPreferredSize(new Dimension(30, 30));
                    FXJJ3.setName("FXJJ3");
                    FXJJ3.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXJJ3FocusLost(e);
                      }
                    });
                    pnlJourMois3.add(FXJJ3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurDM3 ----
                    lbSeparateurDM3.setText("/");
                    lbSeparateurDM3.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurDM3.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurDM3.setMinimumSize(new Dimension(4, 30));
                    lbSeparateurDM3.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurDM3.setName("lbSeparateurDM3");
                    pnlJourMois3.add(lbSeparateurDM3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMM3 ----
                    FXMM3.setComponentPopupMenu(BTD);
                    FXMM3.setPreferredSize(new Dimension(30, 30));
                    FXMM3.setName("FXMM3");
                    FXMM3.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMM3FocusLost(e);
                      }
                    });
                    pnlJourMois3.add(FXMM3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                        GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbA3 ----
                    lbA3.setText("\u00e0");
                    lbA3.setHorizontalAlignment(SwingConstants.CENTER);
                    lbA3.setName("lbA3");
                    pnlJourMois3.add(lbA3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure3.add(pnlJourMois3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                      GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ======== pnlHeureMinute3 ========
                  {
                    pnlHeureMinute3.setPreferredSize(new Dimension(105, 30));
                    pnlHeureMinute3.setMinimumSize(new Dimension(105, 30));
                    pnlHeureMinute3.setName("pnlHeureMinute3");
                    pnlHeureMinute3.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlHeureMinute3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlHeureMinute3.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlHeureMinute3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlHeureMinute3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXHH3 ----
                    FXHH3.setComponentPopupMenu(BTD);
                    FXHH3.setPreferredSize(new Dimension(30, 30));
                    FXHH3.setMinimumSize(new Dimension(30, 30));
                    FXHH3.setName("FXHH3");
                    FXHH3.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXHH3FocusLost(e);
                      }
                    });
                    pnlHeureMinute3.add(FXHH3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurHM3 ----
                    lbSeparateurHM3.setText(":");
                    lbSeparateurHM3.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurHM3.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurHM3.setMinimumSize(new Dimension(5, 30));
                    lbSeparateurHM3.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurHM3.setName("lbSeparateurHM3");
                    pnlHeureMinute3.add(lbSeparateurHM3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMN3 ----
                    FXMN3.setComponentPopupMenu(BTD);
                    FXMN3.setPreferredSize(new Dimension(30, 30));
                    FXMN3.setMinimumSize(new Dimension(30, 30));
                    FXMN3.setName("FXMN3");
                    FXMN3.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMN3FocusLost(e);
                      }
                    });
                    pnlHeureMinute3.add(FXMN3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbPuce3 ----
                    lbPuce3.setIcon(new ImageIcon(getClass().getResource("/images/vert3.gif")));
                    lbPuce3.setMaximumSize(new Dimension(16, 30));
                    lbPuce3.setMinimumSize(new Dimension(16, 30));
                    lbPuce3.setPreferredSize(new Dimension(16, 30));
                    lbPuce3.setHorizontalTextPosition(SwingConstants.LEFT);
                    lbPuce3.setHorizontalAlignment(SwingConstants.CENTER);
                    lbPuce3.setName("lbPuce3");
                    pnlHeureMinute3.add(lbPuce3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure3.add(pnlHeureMinute3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlDateDeclenchement.add(pnlDateHeure3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbDate4 ----
                lbDate4.setText("4 - Jour / Mois / Heure / minute d'envoi");
                lbDate4.setMaximumSize(new Dimension(250, 30));
                lbDate4.setMinimumSize(new Dimension(250, 30));
                lbDate4.setPreferredSize(new Dimension(250, 30));
                lbDate4.setName("lbDate4");
                pnlDateDeclenchement.add(lbDate4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ======== pnlDateHeure4 ========
                {
                  pnlDateHeure4.setPreferredSize(new Dimension(200, 30));
                  pnlDateHeure4.setMinimumSize(new Dimension(200, 30));
                  pnlDateHeure4.setName("pnlDateHeure4");
                  pnlDateHeure4.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlDateHeure4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlDateHeure4.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlDateHeure4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlDateHeure4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ======== pnlJourMois4 ========
                  {
                    pnlJourMois4.setPreferredSize(new Dimension(90, 30));
                    pnlJourMois4.setMinimumSize(new Dimension(90, 30));
                    pnlJourMois4.setName("pnlJourMois4");
                    pnlJourMois4.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlJourMois4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlJourMois4.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlJourMois4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlJourMois4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXJJ4 ----
                    FXJJ4.setComponentPopupMenu(BTD);
                    FXJJ4.setPreferredSize(new Dimension(30, 30));
                    FXJJ4.setName("FXJJ4");
                    FXJJ4.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXJJ4FocusLost(e);
                      }
                    });
                    pnlJourMois4.add(FXJJ4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurDM4 ----
                    lbSeparateurDM4.setText("/");
                    lbSeparateurDM4.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurDM4.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurDM4.setMinimumSize(new Dimension(4, 30));
                    lbSeparateurDM4.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurDM4.setName("lbSeparateurDM4");
                    pnlJourMois4.add(lbSeparateurDM4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMM4 ----
                    FXMM4.setComponentPopupMenu(BTD);
                    FXMM4.setPreferredSize(new Dimension(30, 30));
                    FXMM4.setName("FXMM4");
                    FXMM4.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMM4FocusLost(e);
                      }
                    });
                    pnlJourMois4.add(FXMM4, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                        GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbA4 ----
                    lbA4.setText("\u00e0");
                    lbA4.setHorizontalAlignment(SwingConstants.CENTER);
                    lbA4.setName("lbA4");
                    pnlJourMois4.add(lbA4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure4.add(pnlJourMois4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                      GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ======== pnlHeureMinute4 ========
                  {
                    pnlHeureMinute4.setPreferredSize(new Dimension(105, 30));
                    pnlHeureMinute4.setMinimumSize(new Dimension(105, 30));
                    pnlHeureMinute4.setName("pnlHeureMinute4");
                    pnlHeureMinute4.setLayout(new GridBagLayout());
                    ((GridBagLayout) pnlHeureMinute4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                    ((GridBagLayout) pnlHeureMinute4.getLayout()).rowHeights = new int[] { 0, 0 };
                    ((GridBagLayout) pnlHeureMinute4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    ((GridBagLayout) pnlHeureMinute4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                    
                    // ---- FXHH4 ----
                    FXHH4.setComponentPopupMenu(BTD);
                    FXHH4.setPreferredSize(new Dimension(30, 30));
                    FXHH4.setMinimumSize(new Dimension(30, 30));
                    FXHH4.setName("FXHH4");
                    FXHH4.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXHH4FocusLost(e);
                      }
                    });
                    pnlHeureMinute4.add(FXHH4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbSeparateurHM4 ----
                    lbSeparateurHM4.setText(":");
                    lbSeparateurHM4.setHorizontalAlignment(SwingConstants.CENTER);
                    lbSeparateurHM4.setPreferredSize(new Dimension(5, 30));
                    lbSeparateurHM4.setMinimumSize(new Dimension(5, 30));
                    lbSeparateurHM4.setMaximumSize(new Dimension(5, 30));
                    lbSeparateurHM4.setName("lbSeparateurHM4");
                    pnlHeureMinute4.add(lbSeparateurHM4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- FXMN4 ----
                    FXMN4.setComponentPopupMenu(BTD);
                    FXMN4.setPreferredSize(new Dimension(30, 30));
                    FXMN4.setMinimumSize(new Dimension(30, 30));
                    FXMN4.setName("FXMN4");
                    FXMN4.addFocusListener(new FocusAdapter() {
                      @Override
                      public void focusLost(FocusEvent e) {
                        FXMN4FocusLost(e);
                      }
                    });
                    pnlHeureMinute4.add(FXMN4, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                    
                    // ---- lbPuce4 ----
                    lbPuce4.setIcon(new ImageIcon(getClass().getResource("/images/vert3.gif")));
                    lbPuce4.setMaximumSize(new Dimension(16, 30));
                    lbPuce4.setMinimumSize(new Dimension(16, 30));
                    lbPuce4.setPreferredSize(new Dimension(16, 30));
                    lbPuce4.setHorizontalTextPosition(SwingConstants.LEFT);
                    lbPuce4.setHorizontalAlignment(SwingConstants.CENTER);
                    lbPuce4.setName("lbPuce4");
                    pnlHeureMinute4.add(lbPuce4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  }
                  pnlDateHeure4.add(pnlHeureMinute4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlDateDeclenchement.add(pnlDateHeure4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlPersonnalisation.add(pnlDateDeclenchement, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbIntervalle ----
              lbIntervalle.setText("Intervalle (en secondes)");
              lbIntervalle.setPreferredSize(new Dimension(200, 30));
              lbIntervalle.setMaximumSize(new Dimension(200, 30));
              lbIntervalle.setMinimumSize(new Dimension(200, 30));
              lbIntervalle.setName("lbIntervalle");
              pnlPersonnalisation.add(lbIntervalle, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FXINT ----
              FXINT.setComponentPopupMenu(BTD);
              FXINT.setPreferredSize(new Dimension(50, 30));
              FXINT.setMinimumSize(new Dimension(50, 30));
              FXINT.setName("FXINT");
              pnlPersonnalisation.add(FXINT, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCodeFtp ----
              lbCodeFtp.setText("Code FTP");
              lbCodeFtp.setMaximumSize(new Dimension(200, 30));
              lbCodeFtp.setMinimumSize(new Dimension(200, 30));
              lbCodeFtp.setPreferredSize(new Dimension(200, 30));
              lbCodeFtp.setName("lbCodeFtp");
              pnlPersonnalisation.add(lbCodeFtp, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- FXFTP ----
              FXFTP.setComponentPopupMenu(BTD);
              FXFTP.setPreferredSize(new Dimension(75, 30));
              FXFTP.setMinimumSize(new Dimension(75, 30));
              FXFTP.setName("FXFTP");
              pnlPersonnalisation.add(FXFTP, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            xTitledPanel1ContentContainer.add(pnlPersonnalisation, BorderLayout.CENTER);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 736, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private SNPanelContenu pnlPersonnalisation;
  private JLabel lbLibelle;
  private XRiTextField FXLIB;
  private XRiCheckBox FXETA;
  private SNPanel pnlDateDeclenchement;
  private JLabel lbDate1;
  private SNPanel pnlDateHeure1;
  private SNPanel pnlJourMois1;
  private XRiTextField FXJJ1;
  private JLabel lbSeparateurDM1;
  private XRiTextField FXMM1;
  private JLabel lbA1;
  private SNPanel pnlHeureMinute1;
  private XRiTextField FXHH1;
  private JLabel lbSeparateurHM1;
  private XRiTextField FXMN1;
  private JLabel lbPuce1;
  private JLabel lbNote;
  private JLabel lbDate2;
  private SNPanel pnlDateHeure2;
  private SNPanel pnlJourMois2;
  private XRiTextField FXJJ2;
  private JLabel lbSeparateurDM2;
  private XRiTextField FXMM2;
  private JLabel lbA2;
  private SNPanel pnlHeureMinute2;
  private XRiTextField FXHH2;
  private JLabel lbSeparateurHM2;
  private XRiTextField FXMN2;
  private JLabel lbPuce2;
  private JLabel lbDate3;
  private SNPanel pnlDateHeure3;
  private SNPanel pnlJourMois3;
  private XRiTextField FXJJ3;
  private JLabel lbSeparateurDM3;
  private XRiTextField FXMM3;
  private JLabel lbA3;
  private SNPanel pnlHeureMinute3;
  private XRiTextField FXHH3;
  private JLabel lbSeparateurHM3;
  private XRiTextField FXMN3;
  private JLabel lbPuce3;
  private JLabel lbDate4;
  private SNPanel pnlDateHeure4;
  private SNPanel pnlJourMois4;
  private XRiTextField FXJJ4;
  private JLabel lbSeparateurDM4;
  private XRiTextField FXMM4;
  private JLabel lbA4;
  private SNPanel pnlHeureMinute4;
  private XRiTextField FXHH4;
  private JLabel lbSeparateurHM4;
  private XRiTextField FXMN4;
  private JLabel lbPuce4;
  private JLabel lbIntervalle;
  private XRiTextField FXINT;
  private JLabel lbCodeFtp;
  private XRiTextField FXFTP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
