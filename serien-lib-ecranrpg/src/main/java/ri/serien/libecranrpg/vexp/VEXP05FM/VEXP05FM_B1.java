
package ri.serien.libecranrpg.vexp.VEXP05FM;
// Nom Fichier: pop_VEXP05FM_FMTB1_FMTF1_264.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_recup.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@REPAC@")).trim()));
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP1@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP2@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP2@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP3@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP3@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP4@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP4@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP5@")).trim());
    riZoneSortie5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP5@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP6@")).trim());
    riZoneSortie7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP6@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP7@")).trim());
    riZoneSortie8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP7@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP8@")).trim());
    riZoneSortie9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP8@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP9@")).trim());
    riZoneSortie10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP9@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP10@")).trim());
    riZoneSortie11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP10@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP11@")).trim());
    riZoneSortie12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP11@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP12@")).trim());
    riZoneSortie13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP12@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP13@")).trim());
    riZoneSortie14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP13@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP14@")).trim());
    riZoneSortie15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP14@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP15@")).trim());
    riZoneSortie16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP15@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP16@")).trim());
    riZoneSortie17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP16@")).trim());
    label18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP17@")).trim());
    riZoneSortie18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP17@")).trim());
    label19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP18@")).trim());
    riZoneSortie19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP18@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Extension fiche contact"));
    
    panel1.setVisible(!lexique.HostFieldGetData("WZP1").trim().equals(""));
    panel2.setVisible(!lexique.HostFieldGetData("WZP2").trim().equals(""));
    panel3.setVisible(!lexique.HostFieldGetData("WZP3").trim().equals(""));
    panel4.setVisible(!lexique.HostFieldGetData("WZP4").trim().equals(""));
    panel5.setVisible(!lexique.HostFieldGetData("WZP5").trim().equals(""));
    panel6.setVisible(!lexique.HostFieldGetData("WZP6").trim().equals(""));
    panel7.setVisible(!lexique.HostFieldGetData("WZP7").trim().equals(""));
    panel8.setVisible(!lexique.HostFieldGetData("WZP8").trim().equals(""));
    panel9.setVisible(!lexique.HostFieldGetData("WZP9").trim().equals(""));
    panel10.setVisible(!lexique.HostFieldGetData("WZP10").trim().equals(""));
    panel11.setVisible(!lexique.HostFieldGetData("WZP11").trim().equals(""));
    panel12.setVisible(!lexique.HostFieldGetData("WZP12").trim().equals(""));
    panel13.setVisible(!lexique.HostFieldGetData("WZP13").trim().equals(""));
    panel14.setVisible(!lexique.HostFieldGetData("WZP14").trim().equals(""));
    panel15.setVisible(!lexique.HostFieldGetData("WZP15").trim().equals(""));
    panel16.setVisible(!lexique.HostFieldGetData("WZP16").trim().equals(""));
    panel17.setVisible(!lexique.HostFieldGetData("WZP17").trim().equals(""));
    panel18.setVisible(!lexique.HostFieldGetData("WZP18").trim().equals(""));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    ECZP1 = new XRiTextField();
    riZoneSortie1 = new RiZoneSortie();
    panel2 = new JPanel();
    label2 = new JLabel();
    ECZP2 = new XRiTextField();
    riZoneSortie2 = new RiZoneSortie();
    panel3 = new JPanel();
    label3 = new JLabel();
    ECZP3 = new XRiTextField();
    riZoneSortie3 = new RiZoneSortie();
    panel4 = new JPanel();
    label4 = new JLabel();
    ECZP4 = new XRiTextField();
    riZoneSortie4 = new RiZoneSortie();
    panel5 = new JPanel();
    label5 = new JLabel();
    ECZP5 = new XRiTextField();
    riZoneSortie5 = new RiZoneSortie();
    panel6 = new JPanel();
    label7 = new JLabel();
    ECZP6 = new XRiTextField();
    riZoneSortie7 = new RiZoneSortie();
    panel7 = new JPanel();
    label8 = new JLabel();
    ECZP7 = new XRiTextField();
    riZoneSortie8 = new RiZoneSortie();
    panel8 = new JPanel();
    label9 = new JLabel();
    ECZP8 = new XRiTextField();
    riZoneSortie9 = new RiZoneSortie();
    panel9 = new JPanel();
    label10 = new JLabel();
    ECZP9 = new XRiTextField();
    riZoneSortie10 = new RiZoneSortie();
    panel10 = new JPanel();
    label11 = new JLabel();
    ECZP10 = new XRiTextField();
    riZoneSortie11 = new RiZoneSortie();
    panel11 = new JPanel();
    label12 = new JLabel();
    ECZP11 = new XRiTextField();
    riZoneSortie12 = new RiZoneSortie();
    panel12 = new JPanel();
    label13 = new JLabel();
    ECZP12 = new XRiTextField();
    riZoneSortie13 = new RiZoneSortie();
    panel13 = new JPanel();
    label14 = new JLabel();
    ECZP13 = new XRiTextField();
    riZoneSortie14 = new RiZoneSortie();
    panel14 = new JPanel();
    label15 = new JLabel();
    ECZP14 = new XRiTextField();
    riZoneSortie15 = new RiZoneSortie();
    panel15 = new JPanel();
    label16 = new JLabel();
    ECZP15 = new XRiTextField();
    riZoneSortie16 = new RiZoneSortie();
    panel16 = new JPanel();
    label17 = new JLabel();
    ECZP16 = new XRiTextField();
    riZoneSortie17 = new RiZoneSortie();
    panel17 = new JPanel();
    label18 = new JLabel();
    ECZP17 = new XRiTextField();
    riZoneSortie18 = new RiZoneSortie();
    panel18 = new JPanel();
    label19 = new JLabel();
    ECZP18 = new XRiTextField();
    riZoneSortie19 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(835, 540));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(0, 0));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("@REPAC@"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- label1 ----
            label1.setText("@WZP1@");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() - 1f));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(5, 0, 180, 20);

            //---- ECZP1 ----
            ECZP1.setFont(ECZP1.getFont().deriveFont(ECZP1.getFont().getSize() - 1f));
            ECZP1.setName("ECZP1");
            panel1.add(ECZP1);
            ECZP1.setBounds(190, -2, 250, 24);

            //---- riZoneSortie1 ----
            riZoneSortie1.setText("@ERZP1@");
            riZoneSortie1.setOpaque(false);
            riZoneSortie1.setFont(riZoneSortie1.getFont().deriveFont(riZoneSortie1.getFont().getSize() - 1f));
            riZoneSortie1.setName("riZoneSortie1");
            panel1.add(riZoneSortie1);
            riZoneSortie1.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label2 ----
            label2.setText("@WZP2@");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() - 1f));
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(5, 0, 180, 20);

            //---- ECZP2 ----
            ECZP2.setFont(ECZP2.getFont().deriveFont(ECZP2.getFont().getSize() - 1f));
            ECZP2.setName("ECZP2");
            panel2.add(ECZP2);
            ECZP2.setBounds(190, -2, 250, 24);

            //---- riZoneSortie2 ----
            riZoneSortie2.setText("@ERZP2@");
            riZoneSortie2.setOpaque(false);
            riZoneSortie2.setFont(riZoneSortie2.getFont().deriveFont(riZoneSortie2.getFont().getSize() - 1f));
            riZoneSortie2.setName("riZoneSortie2");
            panel2.add(riZoneSortie2);
            riZoneSortie2.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label3 ----
            label3.setText("@WZP3@");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() - 1f));
            label3.setName("label3");
            panel3.add(label3);
            label3.setBounds(5, 0, 180, 20);

            //---- ECZP3 ----
            ECZP3.setFont(ECZP3.getFont().deriveFont(ECZP3.getFont().getSize() - 1f));
            ECZP3.setName("ECZP3");
            panel3.add(ECZP3);
            ECZP3.setBounds(190, -2, 250, 24);

            //---- riZoneSortie3 ----
            riZoneSortie3.setText("@ERZP3@");
            riZoneSortie3.setOpaque(false);
            riZoneSortie3.setFont(riZoneSortie3.getFont().deriveFont(riZoneSortie3.getFont().getSize() - 1f));
            riZoneSortie3.setName("riZoneSortie3");
            panel3.add(riZoneSortie3);
            riZoneSortie3.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- label4 ----
            label4.setText("@WZP4@");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getSize() - 1f));
            label4.setName("label4");
            panel4.add(label4);
            label4.setBounds(5, 0, 180, 20);

            //---- ECZP4 ----
            ECZP4.setFont(ECZP4.getFont().deriveFont(ECZP4.getFont().getSize() - 1f));
            ECZP4.setName("ECZP4");
            panel4.add(ECZP4);
            ECZP4.setBounds(190, -2, 250, 24);

            //---- riZoneSortie4 ----
            riZoneSortie4.setText("@ERZP4@");
            riZoneSortie4.setOpaque(false);
            riZoneSortie4.setFont(riZoneSortie4.getFont().deriveFont(riZoneSortie4.getFont().getSize() - 1f));
            riZoneSortie4.setName("riZoneSortie4");
            panel4.add(riZoneSortie4);
            riZoneSortie4.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- label5 ----
            label5.setText("@WZP5@");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getSize() - 1f));
            label5.setName("label5");
            panel5.add(label5);
            label5.setBounds(5, 0, 180, 20);

            //---- ECZP5 ----
            ECZP5.setFont(ECZP5.getFont().deriveFont(ECZP5.getFont().getSize() - 1f));
            ECZP5.setName("ECZP5");
            panel5.add(ECZP5);
            ECZP5.setBounds(190, -2, 250, 24);

            //---- riZoneSortie5 ----
            riZoneSortie5.setText("@ERZP5@");
            riZoneSortie5.setOpaque(false);
            riZoneSortie5.setFont(riZoneSortie5.getFont().deriveFont(riZoneSortie5.getFont().getSize() - 1f));
            riZoneSortie5.setName("riZoneSortie5");
            panel5.add(riZoneSortie5);
            riZoneSortie5.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- label7 ----
            label7.setText("@WZP6@");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getSize() - 1f));
            label7.setName("label7");
            panel6.add(label7);
            label7.setBounds(5, 0, 180, 20);

            //---- ECZP6 ----
            ECZP6.setFont(ECZP6.getFont().deriveFont(ECZP6.getFont().getSize() - 1f));
            ECZP6.setName("ECZP6");
            panel6.add(ECZP6);
            ECZP6.setBounds(190, -2, 250, 24);

            //---- riZoneSortie7 ----
            riZoneSortie7.setText("@ERZP6@");
            riZoneSortie7.setOpaque(false);
            riZoneSortie7.setFont(riZoneSortie7.getFont().deriveFont(riZoneSortie7.getFont().getSize() - 1f));
            riZoneSortie7.setName("riZoneSortie7");
            panel6.add(riZoneSortie7);
            riZoneSortie7.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- label8 ----
            label8.setText("@WZP7@");
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getSize() - 1f));
            label8.setName("label8");
            panel7.add(label8);
            label8.setBounds(5, 0, 180, 20);

            //---- ECZP7 ----
            ECZP7.setFont(ECZP7.getFont().deriveFont(ECZP7.getFont().getSize() - 1f));
            ECZP7.setName("ECZP7");
            panel7.add(ECZP7);
            ECZP7.setBounds(190, -2, 250, 24);

            //---- riZoneSortie8 ----
            riZoneSortie8.setText("@ERZP7@");
            riZoneSortie8.setOpaque(false);
            riZoneSortie8.setFont(riZoneSortie8.getFont().deriveFont(riZoneSortie8.getFont().getSize() - 1f));
            riZoneSortie8.setName("riZoneSortie8");
            panel7.add(riZoneSortie8);
            riZoneSortie8.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }

          //======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setName("panel8");
            panel8.setLayout(null);

            //---- label9 ----
            label9.setText("@WZP8@");
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getSize() - 1f));
            label9.setName("label9");
            panel8.add(label9);
            label9.setBounds(5, 0, 180, 20);

            //---- ECZP8 ----
            ECZP8.setFont(ECZP8.getFont().deriveFont(ECZP8.getFont().getSize() - 1f));
            ECZP8.setName("ECZP8");
            panel8.add(ECZP8);
            ECZP8.setBounds(190, -2, 250, 24);

            //---- riZoneSortie9 ----
            riZoneSortie9.setText("@ERZP8@");
            riZoneSortie9.setOpaque(false);
            riZoneSortie9.setFont(riZoneSortie9.getFont().deriveFont(riZoneSortie9.getFont().getSize() - 1f));
            riZoneSortie9.setName("riZoneSortie9");
            panel8.add(riZoneSortie9);
            riZoneSortie9.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel8.getComponentCount(); i++) {
                Rectangle bounds = panel8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8.setMinimumSize(preferredSize);
              panel8.setPreferredSize(preferredSize);
            }
          }

          //======== panel9 ========
          {
            panel9.setOpaque(false);
            panel9.setName("panel9");
            panel9.setLayout(null);

            //---- label10 ----
            label10.setText("@WZP9@");
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getSize() - 1f));
            label10.setName("label10");
            panel9.add(label10);
            label10.setBounds(5, 0, 180, 20);

            //---- ECZP9 ----
            ECZP9.setFont(ECZP9.getFont().deriveFont(ECZP9.getFont().getSize() - 1f));
            ECZP9.setName("ECZP9");
            panel9.add(ECZP9);
            ECZP9.setBounds(190, -2, 250, 24);

            //---- riZoneSortie10 ----
            riZoneSortie10.setText("@ERZP9@");
            riZoneSortie10.setOpaque(false);
            riZoneSortie10.setFont(riZoneSortie10.getFont().deriveFont(riZoneSortie10.getFont().getSize() - 1f));
            riZoneSortie10.setName("riZoneSortie10");
            panel9.add(riZoneSortie10);
            riZoneSortie10.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel9.getComponentCount(); i++) {
                Rectangle bounds = panel9.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel9.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel9.setMinimumSize(preferredSize);
              panel9.setPreferredSize(preferredSize);
            }
          }

          //======== panel10 ========
          {
            panel10.setOpaque(false);
            panel10.setName("panel10");
            panel10.setLayout(null);

            //---- label11 ----
            label11.setText("@WZP10@");
            label11.setFont(label11.getFont().deriveFont(label11.getFont().getSize() - 1f));
            label11.setName("label11");
            panel10.add(label11);
            label11.setBounds(5, 0, 180, 20);

            //---- ECZP10 ----
            ECZP10.setFont(ECZP10.getFont().deriveFont(ECZP10.getFont().getSize() - 1f));
            ECZP10.setName("ECZP10");
            panel10.add(ECZP10);
            ECZP10.setBounds(190, -2, 250, 24);

            //---- riZoneSortie11 ----
            riZoneSortie11.setText("@ERZP10@");
            riZoneSortie11.setOpaque(false);
            riZoneSortie11.setFont(riZoneSortie11.getFont().deriveFont(riZoneSortie11.getFont().getSize() - 1f));
            riZoneSortie11.setName("riZoneSortie11");
            panel10.add(riZoneSortie11);
            riZoneSortie11.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel10.getComponentCount(); i++) {
                Rectangle bounds = panel10.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel10.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel10.setMinimumSize(preferredSize);
              panel10.setPreferredSize(preferredSize);
            }
          }

          //======== panel11 ========
          {
            panel11.setOpaque(false);
            panel11.setName("panel11");
            panel11.setLayout(null);

            //---- label12 ----
            label12.setText("@WZP11@");
            label12.setFont(label12.getFont().deriveFont(label12.getFont().getSize() - 1f));
            label12.setName("label12");
            panel11.add(label12);
            label12.setBounds(5, 0, 180, 20);

            //---- ECZP11 ----
            ECZP11.setFont(ECZP11.getFont().deriveFont(ECZP11.getFont().getSize() - 1f));
            ECZP11.setName("ECZP11");
            panel11.add(ECZP11);
            ECZP11.setBounds(190, -2, 250, 24);

            //---- riZoneSortie12 ----
            riZoneSortie12.setText("@ERZP11@");
            riZoneSortie12.setOpaque(false);
            riZoneSortie12.setFont(riZoneSortie12.getFont().deriveFont(riZoneSortie12.getFont().getSize() - 1f));
            riZoneSortie12.setName("riZoneSortie12");
            panel11.add(riZoneSortie12);
            riZoneSortie12.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel11.getComponentCount(); i++) {
                Rectangle bounds = panel11.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel11.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel11.setMinimumSize(preferredSize);
              panel11.setPreferredSize(preferredSize);
            }
          }

          //======== panel12 ========
          {
            panel12.setOpaque(false);
            panel12.setName("panel12");
            panel12.setLayout(null);

            //---- label13 ----
            label13.setText("@WZP12@");
            label13.setFont(label13.getFont().deriveFont(label13.getFont().getSize() - 1f));
            label13.setName("label13");
            panel12.add(label13);
            label13.setBounds(5, 0, 180, 20);

            //---- ECZP12 ----
            ECZP12.setFont(ECZP12.getFont().deriveFont(ECZP12.getFont().getSize() - 1f));
            ECZP12.setName("ECZP12");
            panel12.add(ECZP12);
            ECZP12.setBounds(190, -2, 250, 24);

            //---- riZoneSortie13 ----
            riZoneSortie13.setText("@ERZP12@");
            riZoneSortie13.setOpaque(false);
            riZoneSortie13.setFont(riZoneSortie13.getFont().deriveFont(riZoneSortie13.getFont().getSize() - 1f));
            riZoneSortie13.setName("riZoneSortie13");
            panel12.add(riZoneSortie13);
            riZoneSortie13.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel12.getComponentCount(); i++) {
                Rectangle bounds = panel12.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel12.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel12.setMinimumSize(preferredSize);
              panel12.setPreferredSize(preferredSize);
            }
          }

          //======== panel13 ========
          {
            panel13.setOpaque(false);
            panel13.setName("panel13");
            panel13.setLayout(null);

            //---- label14 ----
            label14.setText("@WZP13@");
            label14.setFont(label14.getFont().deriveFont(label14.getFont().getSize() - 1f));
            label14.setName("label14");
            panel13.add(label14);
            label14.setBounds(5, 0, 180, 20);

            //---- ECZP13 ----
            ECZP13.setFont(ECZP13.getFont().deriveFont(ECZP13.getFont().getSize() - 1f));
            ECZP13.setName("ECZP13");
            panel13.add(ECZP13);
            ECZP13.setBounds(190, -2, 250, 24);

            //---- riZoneSortie14 ----
            riZoneSortie14.setText("@ERZP13@");
            riZoneSortie14.setOpaque(false);
            riZoneSortie14.setFont(riZoneSortie14.getFont().deriveFont(riZoneSortie14.getFont().getSize() - 1f));
            riZoneSortie14.setName("riZoneSortie14");
            panel13.add(riZoneSortie14);
            riZoneSortie14.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel13.getComponentCount(); i++) {
                Rectangle bounds = panel13.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel13.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel13.setMinimumSize(preferredSize);
              panel13.setPreferredSize(preferredSize);
            }
          }

          //======== panel14 ========
          {
            panel14.setOpaque(false);
            panel14.setName("panel14");
            panel14.setLayout(null);

            //---- label15 ----
            label15.setText("@WZP14@");
            label15.setFont(label15.getFont().deriveFont(label15.getFont().getSize() - 1f));
            label15.setName("label15");
            panel14.add(label15);
            label15.setBounds(5, 0, 180, 20);

            //---- ECZP14 ----
            ECZP14.setFont(ECZP14.getFont().deriveFont(ECZP14.getFont().getSize() - 1f));
            ECZP14.setName("ECZP14");
            panel14.add(ECZP14);
            ECZP14.setBounds(190, -2, 250, 24);

            //---- riZoneSortie15 ----
            riZoneSortie15.setText("@ERZP14@");
            riZoneSortie15.setOpaque(false);
            riZoneSortie15.setFont(riZoneSortie15.getFont().deriveFont(riZoneSortie15.getFont().getSize() - 1f));
            riZoneSortie15.setName("riZoneSortie15");
            panel14.add(riZoneSortie15);
            riZoneSortie15.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel14.getComponentCount(); i++) {
                Rectangle bounds = panel14.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel14.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel14.setMinimumSize(preferredSize);
              panel14.setPreferredSize(preferredSize);
            }
          }

          //======== panel15 ========
          {
            panel15.setOpaque(false);
            panel15.setName("panel15");
            panel15.setLayout(null);

            //---- label16 ----
            label16.setText("@WZP15@");
            label16.setFont(label16.getFont().deriveFont(label16.getFont().getSize() - 1f));
            label16.setName("label16");
            panel15.add(label16);
            label16.setBounds(5, 0, 180, 20);

            //---- ECZP15 ----
            ECZP15.setFont(ECZP15.getFont().deriveFont(ECZP15.getFont().getSize() - 1f));
            ECZP15.setName("ECZP15");
            panel15.add(ECZP15);
            ECZP15.setBounds(190, -2, 250, 24);

            //---- riZoneSortie16 ----
            riZoneSortie16.setText("@ERZP15@");
            riZoneSortie16.setOpaque(false);
            riZoneSortie16.setFont(riZoneSortie16.getFont().deriveFont(riZoneSortie16.getFont().getSize() - 1f));
            riZoneSortie16.setName("riZoneSortie16");
            panel15.add(riZoneSortie16);
            riZoneSortie16.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel15.getComponentCount(); i++) {
                Rectangle bounds = panel15.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel15.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel15.setMinimumSize(preferredSize);
              panel15.setPreferredSize(preferredSize);
            }
          }

          //======== panel16 ========
          {
            panel16.setOpaque(false);
            panel16.setName("panel16");
            panel16.setLayout(null);

            //---- label17 ----
            label17.setText("@WZP16@");
            label17.setFont(label17.getFont().deriveFont(label17.getFont().getSize() - 1f));
            label17.setName("label17");
            panel16.add(label17);
            label17.setBounds(5, 0, 180, 20);

            //---- ECZP16 ----
            ECZP16.setFont(ECZP16.getFont().deriveFont(ECZP16.getFont().getSize() - 1f));
            ECZP16.setName("ECZP16");
            panel16.add(ECZP16);
            ECZP16.setBounds(190, -2, 250, 24);

            //---- riZoneSortie17 ----
            riZoneSortie17.setText("@ERZP16@");
            riZoneSortie17.setOpaque(false);
            riZoneSortie17.setFont(riZoneSortie17.getFont().deriveFont(riZoneSortie17.getFont().getSize() - 1f));
            riZoneSortie17.setName("riZoneSortie17");
            panel16.add(riZoneSortie17);
            riZoneSortie17.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel16.getComponentCount(); i++) {
                Rectangle bounds = panel16.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel16.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel16.setMinimumSize(preferredSize);
              panel16.setPreferredSize(preferredSize);
            }
          }

          //======== panel17 ========
          {
            panel17.setOpaque(false);
            panel17.setName("panel17");
            panel17.setLayout(null);

            //---- label18 ----
            label18.setText("@WZP17@");
            label18.setFont(label18.getFont().deriveFont(label18.getFont().getSize() - 1f));
            label18.setName("label18");
            panel17.add(label18);
            label18.setBounds(5, 0, 180, 20);

            //---- ECZP17 ----
            ECZP17.setFont(ECZP17.getFont().deriveFont(ECZP17.getFont().getSize() - 1f));
            ECZP17.setName("ECZP17");
            panel17.add(ECZP17);
            ECZP17.setBounds(190, -2, 250, 24);

            //---- riZoneSortie18 ----
            riZoneSortie18.setText("@ERZP17@");
            riZoneSortie18.setOpaque(false);
            riZoneSortie18.setFont(riZoneSortie18.getFont().deriveFont(riZoneSortie18.getFont().getSize() - 1f));
            riZoneSortie18.setName("riZoneSortie18");
            panel17.add(riZoneSortie18);
            riZoneSortie18.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel17.getComponentCount(); i++) {
                Rectangle bounds = panel17.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel17.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel17.setMinimumSize(preferredSize);
              panel17.setPreferredSize(preferredSize);
            }
          }

          //======== panel18 ========
          {
            panel18.setOpaque(false);
            panel18.setName("panel18");
            panel18.setLayout(null);

            //---- label19 ----
            label19.setText("@WZP18@");
            label19.setFont(label19.getFont().deriveFont(label19.getFont().getSize() - 1f));
            label19.setName("label19");
            panel18.add(label19);
            label19.setBounds(5, 0, 180, 20);

            //---- ECZP18 ----
            ECZP18.setFont(ECZP18.getFont().deriveFont(ECZP18.getFont().getSize() - 1f));
            ECZP18.setName("ECZP18");
            panel18.add(ECZP18);
            ECZP18.setBounds(190, -2, 250, 24);

            //---- riZoneSortie19 ----
            riZoneSortie19.setText("@ERZP18@");
            riZoneSortie19.setOpaque(false);
            riZoneSortie19.setFont(riZoneSortie19.getFont().deriveFont(riZoneSortie19.getFont().getSize() - 1f));
            riZoneSortie19.setName("riZoneSortie19");
            panel18.add(riZoneSortie19);
            riZoneSortie19.setBounds(450, 0, 140, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel18.getComponentCount(); i++) {
                Rectangle bounds = panel18.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel18.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel18.setMinimumSize(preferredSize);
              panel18.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_recupLayout = new GroupLayout(p_recup);
          p_recup.setLayout(p_recupLayout);
          p_recupLayout.setHorizontalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_recupLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel9, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel10, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel11, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel12, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel13, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel14, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel15, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel16, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel17, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel18, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)))
          );
          p_recupLayout.setVerticalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel9, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel10, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel11, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel12, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel13, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel14, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel15, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel16, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel17, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel18, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
          );
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JPanel panel1;
  private JLabel label1;
  private XRiTextField ECZP1;
  private RiZoneSortie riZoneSortie1;
  private JPanel panel2;
  private JLabel label2;
  private XRiTextField ECZP2;
  private RiZoneSortie riZoneSortie2;
  private JPanel panel3;
  private JLabel label3;
  private XRiTextField ECZP3;
  private RiZoneSortie riZoneSortie3;
  private JPanel panel4;
  private JLabel label4;
  private XRiTextField ECZP4;
  private RiZoneSortie riZoneSortie4;
  private JPanel panel5;
  private JLabel label5;
  private XRiTextField ECZP5;
  private RiZoneSortie riZoneSortie5;
  private JPanel panel6;
  private JLabel label7;
  private XRiTextField ECZP6;
  private RiZoneSortie riZoneSortie7;
  private JPanel panel7;
  private JLabel label8;
  private XRiTextField ECZP7;
  private RiZoneSortie riZoneSortie8;
  private JPanel panel8;
  private JLabel label9;
  private XRiTextField ECZP8;
  private RiZoneSortie riZoneSortie9;
  private JPanel panel9;
  private JLabel label10;
  private XRiTextField ECZP9;
  private RiZoneSortie riZoneSortie10;
  private JPanel panel10;
  private JLabel label11;
  private XRiTextField ECZP10;
  private RiZoneSortie riZoneSortie11;
  private JPanel panel11;
  private JLabel label12;
  private XRiTextField ECZP11;
  private RiZoneSortie riZoneSortie12;
  private JPanel panel12;
  private JLabel label13;
  private XRiTextField ECZP12;
  private RiZoneSortie riZoneSortie13;
  private JPanel panel13;
  private JLabel label14;
  private XRiTextField ECZP13;
  private RiZoneSortie riZoneSortie14;
  private JPanel panel14;
  private JLabel label15;
  private XRiTextField ECZP14;
  private RiZoneSortie riZoneSortie15;
  private JPanel panel15;
  private JLabel label16;
  private XRiTextField ECZP15;
  private RiZoneSortie riZoneSortie16;
  private JPanel panel16;
  private JLabel label17;
  private XRiTextField ECZP16;
  private RiZoneSortie riZoneSortie17;
  private JPanel panel17;
  private JLabel label18;
  private XRiTextField ECZP17;
  private RiZoneSortie riZoneSortie18;
  private JPanel panel18;
  private JLabel label19;
  private XRiTextField ECZP18;
  private RiZoneSortie riZoneSortie19;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
