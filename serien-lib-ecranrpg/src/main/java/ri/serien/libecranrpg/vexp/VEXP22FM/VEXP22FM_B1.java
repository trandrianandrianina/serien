
package ri.serien.libecranrpg.vexp.VEXP22FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP22FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _TRCPA_Title = { "....+...10....+...20....+...30....+...40....+...50....+...60....+...70....+....", };
  private String[][] _TRCPA_Data = { { "TRCPA", }, };
  private int[] _TRCPA_Width = { 562, };
  
  public VEXP22FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WPCV.setValeursSelection("OUI", "NON");
    TRIN8.setValeursSelection("S", " ");
    TRIN9.setValeursSelection("1", " ");
    TRCPA.setAspectTable(null, _TRCPA_Title, _TRCPA_Data, _TRCPA_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TRLCO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLCO@")).trim());
    INDCTR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDCTR@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    BQLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BQLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(TRCPA, TRCPA.get_LIST_Title_Data_Brut(), null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    xTitledPanel2.setVisible(lexique.HostFieldGetData("TYPD").trim().equalsIgnoreCase(""));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    TRLCO = new RiZoneSortie();
    INDCTR = new RiZoneSortie();
    OBJ_39 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_48 = new JLabel();
    TRLTR = new XRiTextField();
    TRICL = new XRiTextField();
    OBJ_52 = new JLabel();
    OBJ_50 = new JLabel();
    TRCED = new XRiTextField();
    TRFIC = new XRiTextField();
    label1 = new JLabel();
    TRIN8 = new XRiCheckBox();
    TRIN9 = new XRiCheckBox();
    xTitledPanel2 = new JXTitledPanel();
    WSEQ = new XRiComboBox();
    OBJ_61 = new JLabel();
    WTEL1 = new XRiTextField();
    WTEL2 = new XRiTextField();
    WPCV = new XRiCheckBox();
    OBJ_63 = new JLabel();
    label2 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    BQLIB = new RiZoneSortie();
    OBJ_69 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_71 = new JLabel();
    TRCOP = new XRiTextField();
    TRSOC = new XRiTextField();
    TRBQE = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    TRCPA = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des transactions");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- TRLCO ----
          TRLCO.setText("@TRLCO@");
          TRLCO.setOpaque(false);
          TRLCO.setName("TRLCO");

          //---- INDCTR ----
          INDCTR.setComponentPopupMenu(BTD);
          INDCTR.setText("@INDCTR@");
          INDCTR.setOpaque(false);
          INDCTR.setName("INDCTR");

          //---- OBJ_39 ----
          OBJ_39.setText("Code");
          OBJ_39.setName("OBJ_39");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDCTR, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(TRLCO, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDCTR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(TRLCO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(740, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_48 ----
            OBJ_48.setText("Num\u00e9ro c\u00e9dant ou \u00e9metteur");
            OBJ_48.setName("OBJ_48");
            xTitledPanel1ContentContainer.add(OBJ_48);
            OBJ_48.setBounds(310, 19, 166, 20);

            //---- TRLTR ----
            TRLTR.setComponentPopupMenu(BTD);
            TRLTR.setName("TRLTR");
            xTitledPanel1ContentContainer.add(TRLTR);
            TRLTR.setBounds(125, 15, 160, TRLTR.getPreferredSize().height);

            //---- TRICL ----
            TRICL.setComponentPopupMenu(BTD);
            TRICL.setName("TRICL");
            xTitledPanel1ContentContainer.add(TRICL);
            TRICL.setBounds(485, 45, 170, TRICL.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("Identification client");
            OBJ_52.setName("OBJ_52");
            xTitledPanel1ContentContainer.add(OBJ_52);
            OBJ_52.setBounds(310, 49, 130, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("Nom du fichier");
            OBJ_50.setName("OBJ_50");
            xTitledPanel1ContentContainer.add(OBJ_50);
            OBJ_50.setBounds(20, 54, 95, 20);

            //---- TRCED ----
            TRCED.setComponentPopupMenu(BTD);
            TRCED.setName("TRCED");
            xTitledPanel1ContentContainer.add(TRCED);
            TRCED.setBounds(485, 15, 58, TRCED.getPreferredSize().height);

            //---- TRFIC ----
            TRFIC.setComponentPopupMenu(BTD);
            TRFIC.setName("TRFIC");
            xTitledPanel1ContentContainer.add(TRFIC);
            TRFIC.setBounds(125, 50, 60, TRFIC.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Libell\u00e9");
            label1.setName("label1");
            xTitledPanel1ContentContainer.add(label1);
            label1.setBounds(20, 19, 90, 20);

            //---- TRIN8 ----
            TRIN8.setText("Transformation SEPA");
            TRIN8.setName("TRIN8");
            xTitledPanel1ContentContainer.add(TRIN8);
            TRIN8.setBounds(310, 85, 170, TRIN8.getPreferredSize().height);

            //---- TRIN9 ----
            TRIN9.setText("Regroupement");
            TRIN9.setName("TRIN9");
            xTitledPanel1ContentContainer.add(TRIN9);
            TRIN9.setBounds(485, 85, 170, TRIN9.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel1);
          xTitledPanel1.setBounds(30, 20, 680, 140);

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Gestion des appels");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- WSEQ ----
            WSEQ.setModel(new DefaultComboBoxModel(new String[] {
              "OUI",
              "NON",
              "EMI",
              "REC",
              ""
            }));
            WSEQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSEQ.setName("WSEQ");
            xTitledPanel2ContentContainer.add(WSEQ);
            WSEQ.setBounds(125, 51, 76, WSEQ.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Num\u00e9ro compl\u00e9mentaire");
            OBJ_61.setName("OBJ_61");
            xTitledPanel2ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(310, 19, 153, 20);

            //---- WTEL1 ----
            WTEL1.setComponentPopupMenu(BTD);
            WTEL1.setName("WTEL1");
            xTitledPanel2ContentContainer.add(WTEL1);
            WTEL1.setBounds(125, 15, 160, WTEL1.getPreferredSize().height);

            //---- WTEL2 ----
            WTEL2.setComponentPopupMenu(BTD);
            WTEL2.setName("WTEL2");
            xTitledPanel2ContentContainer.add(WTEL2);
            WTEL2.setBounds(485, 15, 150, WTEL2.getPreferredSize().height);

            //---- WPCV ----
            WPCV.setText("Appel en PCV");
            WPCV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPCV.setName("WPCV");
            xTitledPanel2ContentContainer.add(WPCV);
            WPCV.setBounds(310, 54, 112, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("N\u00b0 de s\u00e9quence");
            OBJ_63.setName("OBJ_63");
            xTitledPanel2ContentContainer.add(OBJ_63);
            OBJ_63.setBounds(20, 54, 105, 20);

            //---- label2 ----
            label2.setText("T\u00e9l\u00e9phone");
            label2.setName("label2");
            xTitledPanel2ContentContainer.add(label2);
            label2.setBounds(20, 19, 99, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel2);
          xTitledPanel2.setBounds(30, 165, 680, 120);

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Relation S\u00e9rie M");
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- BQLIB ----
            BQLIB.setText("@BQLIB@");
            BQLIB.setName("BQLIB");
            xTitledPanel3ContentContainer.add(BQLIB);
            BQLIB.setBounds(350, 52, 280, BQLIB.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("Code op\u00e9ration");
            OBJ_69.setName("OBJ_69");
            xTitledPanel3ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(20, 19, 105, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Banque");
            OBJ_76.setName("OBJ_76");
            xTitledPanel3ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(245, 54, 60, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Soci\u00e9t\u00e9");
            OBJ_71.setName("OBJ_71");
            xTitledPanel3ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(245, 19, 60, 20);

            //---- TRCOP ----
            TRCOP.setComponentPopupMenu(BTD);
            TRCOP.setName("TRCOP");
            xTitledPanel3ContentContainer.add(TRCOP);
            TRCOP.setBounds(125, 15, 40, TRCOP.getPreferredSize().height);

            //---- TRSOC ----
            TRSOC.setComponentPopupMenu(BTD);
            TRSOC.setName("TRSOC");
            xTitledPanel3ContentContainer.add(TRSOC);
            TRSOC.setBounds(310, 15, 40, TRSOC.getPreferredSize().height);

            //---- TRBQE ----
            TRBQE.setComponentPopupMenu(BTD);
            TRBQE.setName("TRBQE");
            xTitledPanel3ContentContainer.add(TRBQE);
            TRBQE.setBounds(310, 50, 34, TRBQE.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel3);
          xTitledPanel3.setBounds(30, 290, 680, 120);

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Carte param\u00e8tre");
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- TRCPA ----
              TRCPA.setName("TRCPA");
              SCROLLPANE_LIST2.setViewportView(TRCPA);
            }
            xTitledPanel4ContentContainer.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(20, 25, 570, 45);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel4);
          xTitledPanel4.setBounds(30, 415, 680, 120);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie TRLCO;
  private RiZoneSortie INDCTR;
  private JLabel OBJ_39;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_48;
  private XRiTextField TRLTR;
  private XRiTextField TRICL;
  private JLabel OBJ_52;
  private JLabel OBJ_50;
  private XRiTextField TRCED;
  private XRiTextField TRFIC;
  private JLabel label1;
  private XRiCheckBox TRIN8;
  private XRiCheckBox TRIN9;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox WSEQ;
  private JLabel OBJ_61;
  private XRiTextField WTEL1;
  private XRiTextField WTEL2;
  private XRiCheckBox WPCV;
  private JLabel OBJ_63;
  private JLabel label2;
  private JXTitledPanel xTitledPanel3;
  private RiZoneSortie BQLIB;
  private JLabel OBJ_69;
  private JLabel OBJ_76;
  private JLabel OBJ_71;
  private XRiTextField TRCOP;
  private XRiTextField TRSOC;
  private XRiTextField TRBQE;
  private JXTitledPanel xTitledPanel4;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable TRCPA;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
