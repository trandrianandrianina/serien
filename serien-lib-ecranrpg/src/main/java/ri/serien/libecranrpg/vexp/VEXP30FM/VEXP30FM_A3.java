
package ri.serien.libecranrpg.vexp.VEXP30FM;
// Nom Fichier: pop_VEXP30FM_FMTA3_FMTF1_241.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP30FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP30FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX8.setValeurs("8", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX10.setValeurs("0", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX4.setValeurs("3", "RB");
    TIDX3.setValeurs("4", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX1.setValeurs("1", "RB");
    TSCAN.setValeursSelection("1", " ");
    
    // Bouton par défaut
    setDefaultButton(OBJ_26);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    ARG7.setVisible(lexique.isPresent("ARG7"));
    ARG5.setVisible(lexique.isPresent("ARG5"));
    ARG10.setVisible(lexique.isPresent("ARG10"));
    ARG9.setVisible(lexique.isPresent("ARG9"));
    ARG6X.setVisible(lexique.isPresent("ARG6X"));
    ARG1.setVisible(lexique.isPresent("ARG1"));
    ARG2.setVisible(lexique.isPresent("ARG2"));
    ARG4.setVisible(lexique.isPresent("ARG4"));
    ARG3.setVisible(lexique.isPresent("ARG3"));
    SCAN.setVisible(lexique.isPresent("SCAN"));
    ARG8.setVisible(lexique.isPresent("ARG8"));
    // TIDX10_OBJ_39.setVisible( lexique.isPresent("TIDX10"));
    // TIDX10_OBJ_39.setSelected(lexique.HostFieldGetData("TIDX10").equalsIgnoreCase("1"));
    // TIDX9_OBJ_38.setVisible( lexique.isPresent("TIDX9"));
    // TIDX9_OBJ_38.setSelected(lexique.HostFieldGetData("TIDX9").equalsIgnoreCase("1"));
    // TSCAN.setVisible( lexique.isPresent("TSCAN"));
    // TSCAN.setSelected(lexique.HostFieldGetData("TSCAN").equalsIgnoreCase("1"));
    // TIDX8_OBJ_35.setVisible( lexique.isPresent("TIDX8"));
    // TIDX8_OBJ_35.setSelected(lexique.HostFieldGetData("TIDX8").equalsIgnoreCase("1"));
    // TIDX7_OBJ_34.setVisible( lexique.isPresent("TIDX7"));
    // TIDX7_OBJ_34.setSelected(lexique.HostFieldGetData("TIDX7").equalsIgnoreCase("1"));
    // TIDX5_OBJ_33.setVisible( lexique.isPresent("TIDX5"));
    // TIDX5_OBJ_33.setSelected(lexique.HostFieldGetData("TIDX5").equalsIgnoreCase("1"));
    // TIDX4_OBJ_32.setVisible( lexique.isPresent("TIDX4"));
    // TIDX4_OBJ_32.setSelected(lexique.HostFieldGetData("TIDX4").equalsIgnoreCase("1"));
    // TIDX3_OBJ_31.setVisible( lexique.isPresent("TIDX3"));
    // TIDX3_OBJ_31.setSelected(lexique.HostFieldGetData("TIDX3").equalsIgnoreCase("1"));
    // TIDX2_OBJ_30.setVisible( lexique.isPresent("TIDX2"));
    // TIDX2_OBJ_30.setSelected(lexique.HostFieldGetData("TIDX2").equalsIgnoreCase("1"));
    // TIDX6_OBJ_29.setVisible( lexique.isPresent("TIDX6"));
    // TIDX6_OBJ_29.setSelected(lexique.HostFieldGetData("TIDX6").equalsIgnoreCase("1"));
    // TIDX1_OBJ_28.setVisible( lexique.isPresent("TIDX1"));
    // TIDX1_OBJ_28.setSelected(lexique.HostFieldGetData("TIDX1").equalsIgnoreCase("1"));
    
    // TODO Icones
    OBJ_26.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_27.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SERIE M - INTERVENTIONS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (TIDX10_OBJ_39.isSelected())
    // lexique.HostFieldPutData("TIDX10", 0, "1");
    // if (TIDX9_OBJ_38.isSelected())
    // lexique.HostFieldPutData("TIDX9", 0, "1");
    // if (TSCAN.isSelected())
    // lexique.HostFieldPutData("TSCAN", 0, "1");
    // else
    // lexique.HostFieldPutData("TSCAN", 0, " ");
    // if (TIDX8_OBJ_35.isSelected())
    // lexique.HostFieldPutData("TIDX8", 0, "1");
    // if (TIDX7_OBJ_34.isSelected())
    // lexique.HostFieldPutData("TIDX7", 0, "1");
    // if (TIDX5_OBJ_33.isSelected())
    // lexique.HostFieldPutData("TIDX5", 0, "1");
    // if (TIDX4_OBJ_32.isSelected())
    // lexique.HostFieldPutData("TIDX4", 0, "1");
    // if (TIDX3_OBJ_31.isSelected())
    // lexique.HostFieldPutData("TIDX3", 0, "1");
    // if (TIDX2_OBJ_30.isSelected())
    // lexique.HostFieldPutData("TIDX2", 0, "1");
    // if (TIDX6_OBJ_29.isSelected())
    // lexique.HostFieldPutData("TIDX6", 0, "1");
    // if (TIDX1_OBJ_28.isSelected())
    // lexique.HostFieldPutData("TIDX1", 0, "1");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter", false);
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    TIDX1 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TSCAN = new XRiCheckBox();
    TIDX9 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    ARG8 = new XRiTextField();
    SCAN = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG4 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG1 = new XRiTextField();
    ARG6X = new XRiTextField();
    ARG9 = new XRiTextField();
    ARG10 = new XRiTextField();
    ARG5 = new XRiTextField();
    ARG7 = new XRiTextField();
    panel1 = new JPanel();
    OBJ_26 = new JButton();
    OBJ_27 = new JButton();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- TIDX1 ----
      TIDX1.setText("Num\u00e9ro de fiche intervention");
      TIDX1.setToolTipText("Tri\u00e9 par");
      TIDX1.setComponentPopupMenu(BTD);
      TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX1.setName("TIDX1");
      panel2.add(TIDX1);
      TIDX1.setBounds(30, 44, 219, 20);

      //---- TIDX6 ----
      TIDX6.setText("Date de cr\u00e9ation");
      TIDX6.setToolTipText("Tri\u00e9 par");
      TIDX6.setComponentPopupMenu(BTD);
      TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX6.setName("TIDX6");
      panel2.add(TIDX6);
      TIDX6.setBounds(30, 74, 219, 20);

      //---- TIDX2 ----
      TIDX2.setText("Num\u00e9ro de client");
      TIDX2.setToolTipText("Tri\u00e9 par");
      TIDX2.setComponentPopupMenu(BTD);
      TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX2.setName("TIDX2");
      panel2.add(TIDX2);
      TIDX2.setBounds(30, 104, 219, 20);

      //---- TIDX3 ----
      TIDX3.setText("Programme concern\u00e9");
      TIDX3.setToolTipText("Tri\u00e9 par");
      TIDX3.setComponentPopupMenu(BTD);
      TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX3.setName("TIDX3");
      panel2.add(TIDX3);
      TIDX3.setBounds(30, 134, 219, 20);

      //---- TIDX4 ----
      TIDX4.setText("Point de menu");
      TIDX4.setToolTipText("Tri\u00e9 par");
      TIDX4.setComponentPopupMenu(BTD);
      TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX4.setName("TIDX4");
      panel2.add(TIDX4);
      TIDX4.setBounds(30, 164, 219, 20);

      //---- TIDX5 ----
      TIDX5.setText("Anosys");
      TIDX5.setToolTipText("Tri\u00e9 par");
      TIDX5.setComponentPopupMenu(BTD);
      TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX5.setName("TIDX5");
      panel2.add(TIDX5);
      TIDX5.setBounds(30, 254, 219, 20);

      //---- TIDX7 ----
      TIDX7.setText("Code \u00e9tat");
      TIDX7.setToolTipText("Tri\u00e9 par");
      TIDX7.setComponentPopupMenu(BTD);
      TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX7.setName("TIDX7");
      panel2.add(TIDX7);
      TIDX7.setBounds(30, 284, 219, 20);

      //---- TIDX8 ----
      TIDX8.setText("Mot de classement client");
      TIDX8.setToolTipText("Tri\u00e9 par");
      TIDX8.setComponentPopupMenu(BTD);
      TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX8.setName("TIDX8");
      panel2.add(TIDX8);
      TIDX8.setBounds(30, 314, 219, 20);

      //---- TSCAN ----
      TSCAN.setText("Portion de texte");
      TSCAN.setComponentPopupMenu(BTD);
      TSCAN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TSCAN.setName("TSCAN");
      panel2.add(TSCAN);
      TSCAN.setBounds(30, 344, 219, 20);

      //---- TIDX9 ----
      TIDX9.setText("Intervenant 1");
      TIDX9.setToolTipText("Tri\u00e9 par");
      TIDX9.setComponentPopupMenu(BTD);
      TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX9.setName("TIDX9");
      panel2.add(TIDX9);
      TIDX9.setBounds(30, 194, 219, 20);

      //---- TIDX10 ----
      TIDX10.setText("Intervenant 2");
      TIDX10.setToolTipText("Tri\u00e9 par");
      TIDX10.setComponentPopupMenu(BTD);
      TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX10.setName("TIDX10");
      panel2.add(TIDX10);
      TIDX10.setBounds(30, 224, 219, 20);

      //---- ARG8 ----
      ARG8.setComponentPopupMenu(BTD);
      ARG8.setName("ARG8");
      panel2.add(ARG8);
      ARG8.setBounds(275, 310, 155, ARG8.getPreferredSize().height);

      //---- SCAN ----
      SCAN.setComponentPopupMenu(BTD);
      SCAN.setName("SCAN");
      panel2.add(SCAN);
      SCAN.setBounds(275, 340, 155, SCAN.getPreferredSize().height);

      //---- ARG3 ----
      ARG3.setComponentPopupMenu(BTD);
      ARG3.setName("ARG3");
      panel2.add(ARG3);
      ARG3.setBounds(275, 130, 105, ARG3.getPreferredSize().height);

      //---- ARG4 ----
      ARG4.setComponentPopupMenu(BTD);
      ARG4.setName("ARG4");
      panel2.add(ARG4);
      ARG4.setBounds(275, 160, 85, ARG4.getPreferredSize().height);

      //---- ARG2 ----
      ARG2.setComponentPopupMenu(BTD);
      ARG2.setName("ARG2");
      panel2.add(ARG2);
      ARG2.setBounds(275, 100, 65, ARG2.getPreferredSize().height);

      //---- ARG1 ----
      ARG1.setComponentPopupMenu(BTD);
      ARG1.setName("ARG1");
      panel2.add(ARG1);
      ARG1.setBounds(275, 40, 60, ARG1.getPreferredSize().height);

      //---- ARG6X ----
      ARG6X.setComponentPopupMenu(BTD);
      ARG6X.setName("ARG6X");
      panel2.add(ARG6X);
      ARG6X.setBounds(275, 70, 50, ARG6X.getPreferredSize().height);

      //---- ARG9 ----
      ARG9.setComponentPopupMenu(BTD);
      ARG9.setName("ARG9");
      panel2.add(ARG9);
      ARG9.setBounds(275, 220, 35, ARG9.getPreferredSize().height);

      //---- ARG10 ----
      ARG10.setComponentPopupMenu(BTD);
      ARG10.setName("ARG10");
      panel2.add(ARG10);
      ARG10.setBounds(275, 250, 35, ARG10.getPreferredSize().height);

      //---- ARG5 ----
      ARG5.setComponentPopupMenu(BTD);
      ARG5.setName("ARG5");
      panel2.add(ARG5);
      ARG5.setBounds(275, 190, 35, ARG5.getPreferredSize().height);

      //---- ARG7 ----
      ARG7.setComponentPopupMenu(BTD);
      ARG7.setName("ARG7");
      panel2.add(ARG7);
      ARG7.setBounds(275, 280, 35, ARG7.getPreferredSize().height);
    }

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_26 ----
      OBJ_26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_26.setToolTipText("Ok");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      panel1.add(OBJ_26);
      OBJ_26.setBounds(10, 5, 56, 40);

      //---- OBJ_27 ----
      OBJ_27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_27.setToolTipText("Retour");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      panel1.add(OBJ_27);
      OBJ_27.setBounds(70, 5, 56, 40);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
          .addContainerGap())
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);
      OBJ_4.addSeparator();

      //---- OBJ_15 ----
      OBJ_15.setText("Exploitation");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_15);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX10);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel2;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX8;
  private XRiCheckBox TSCAN;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX10;
  private XRiTextField ARG8;
  private XRiTextField SCAN;
  private XRiTextField ARG3;
  private XRiTextField ARG4;
  private XRiTextField ARG2;
  private XRiTextField ARG1;
  private XRiTextField ARG6X;
  private XRiTextField ARG9;
  private XRiTextField ARG10;
  private XRiTextField ARG5;
  private XRiTextField ARG7;
  private JPanel panel1;
  private JButton OBJ_26;
  private JButton OBJ_27;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
