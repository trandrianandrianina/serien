
package ri.serien.libecranrpg.vexp.VEXP03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP03FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP03FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX6.setValeurs("6", "RB");
    // TIDX8.setValeurs("8", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX7.setValeurs("7", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    TIDX7 = new XRiRadioButton();
    ARG3 = new XRiTextField();
    ARG5 = new XRiTextField();
    ARG7 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG9 = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    ARG6 = new XRiTextField();
    ETBCLI = new XRiTextField();
    ETBFRS = new XRiTextField();
    ARG4 = new XRiTextField();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(625, 235));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- TIDX7 ----
        TIDX7.setText("Nom soci\u00e9t\u00e9 non tiers");
        TIDX7.setComponentPopupMenu(BTD);
        TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX7.setName("TIDX7");
        p_contenu.add(TIDX7);
        TIDX7.setBounds(45, 136, 160, 20);

        //---- ARG3 ----
        ARG3.setComponentPopupMenu(BTD);
        ARG3.setName("ARG3");
        p_contenu.add(ARG3);
        ARG3.setBounds(260, 74, 160, ARG3.getPreferredSize().height);

        //---- ARG5 ----
        ARG5.setComponentPopupMenu(BTD);
        ARG5.setName("ARG5");
        p_contenu.add(ARG5);
        ARG5.setBounds(260, 103, 160, ARG5.getPreferredSize().height);

        //---- ARG7 ----
        ARG7.setComponentPopupMenu(BTD);
        ARG7.setName("ARG7");
        p_contenu.add(ARG7);
        ARG7.setBounds(220, 132, 200, ARG7.getPreferredSize().height);

        //---- ARG2 ----
        ARG2.setComponentPopupMenu(BTD);
        ARG2.setName("ARG2");
        p_contenu.add(ARG2);
        ARG2.setBounds(220, 16, 200, ARG2.getPreferredSize().height);

        //---- ARG9 ----
        ARG9.setComponentPopupMenu(BTD);
        ARG9.setName("ARG9");
        p_contenu.add(ARG9);
        ARG9.setBounds(220, 45, 200, ARG9.getPreferredSize().height);

        //---- TIDX5 ----
        TIDX5.setText("Nom cournisseur");
        TIDX5.setComponentPopupMenu(BTD);
        TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX5.setName("TIDX5");
        p_contenu.add(TIDX5);
        TIDX5.setBounds(45, 107, 160, 20);

        //---- TIDX2 ----
        TIDX2.setText("Nom du contact");
        TIDX2.setComponentPopupMenu(BTD);
        TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX2.setName("TIDX2");
        p_contenu.add(TIDX2);
        TIDX2.setBounds(45, 20, 160, 20);

        //---- TIDX9 ----
        TIDX9.setText("Alias");
        TIDX9.setComponentPopupMenu(BTD);
        TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX9.setName("TIDX9");
        p_contenu.add(TIDX9);
        TIDX9.setBounds(45, 49, 160, 20);

        //---- TIDX6 ----
        TIDX6.setText("T\u00e9l\u00e9phone");
        TIDX6.setComponentPopupMenu(BTD);
        TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX6.setName("TIDX6");
        p_contenu.add(TIDX6);
        TIDX6.setBounds(45, 196, 160, 20);

        //---- TIDX3 ----
        TIDX3.setText("Nom client");
        TIDX3.setComponentPopupMenu(BTD);
        TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX3.setName("TIDX3");
        p_contenu.add(TIDX3);
        TIDX3.setBounds(45, 78, 160, 20);

        //---- TIDX4 ----
        TIDX4.setText("Fonction");
        TIDX4.setComponentPopupMenu(BTD);
        TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX4.setName("TIDX4");
        p_contenu.add(TIDX4);
        TIDX4.setBounds(45, 166, 160, 20);

        //---- ARG6 ----
        ARG6.setComponentPopupMenu(BTD);
        ARG6.setName("ARG6");
        p_contenu.add(ARG6);
        ARG6.setBounds(220, 192, 90, ARG6.getPreferredSize().height);

        //---- ETBCLI ----
        ETBCLI.setComponentPopupMenu(BTD);
        ETBCLI.setName("ETBCLI");
        p_contenu.add(ETBCLI);
        ETBCLI.setBounds(220, 74, 40, ETBCLI.getPreferredSize().height);

        //---- ETBFRS ----
        ETBFRS.setComponentPopupMenu(BTD);
        ETBFRS.setName("ETBFRS");
        p_contenu.add(ETBFRS);
        ETBFRS.setBounds(220, 103, 40, ETBFRS.getPreferredSize().height);

        //---- ARG4 ----
        ARG4.setComponentPopupMenu(BTD);
        ARG4.setName("ARG4");
        p_contenu.add(ARG4);
        ARG4.setBounds(220, 162, 40, ARG4.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);
      OBJ_4.addSeparator();

      //---- OBJ_15 ----
      OBJ_15.setText("Exploitation");
      OBJ_15.setName("OBJ_15");
      OBJ_4.add(OBJ_15);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG3;
  private XRiTextField ARG5;
  private XRiTextField ARG7;
  private XRiTextField ARG2;
  private XRiTextField ARG9;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG6;
  private XRiTextField ETBCLI;
  private XRiTextField ETBFRS;
  private XRiTextField ARG4;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
