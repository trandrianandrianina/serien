
package ri.serien.libecranrpg.vexp.VEXP91FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP91FM_CP extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] listeTop = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08" };
  
  public VEXP91FM_CP(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMOD@")).trim());
    L0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L01@")).trim());
    L1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L02@")).trim());
    L2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L03@")).trim());
    L3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L04@")).trim());
    L4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L05@")).trim());
    L5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L06@")).trim());
    L6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L07@")).trim());
    L7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L08@")).trim());
    D1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D02@")).trim());
    D2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D03@")).trim());
    D3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D04@")).trim());
    D4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D05@")).trim());
    D5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D06@")).trim());
    D6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D07@")).trim());
    D7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D08@")).trim());
    D0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D01@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_33.setVisible(lexique.isPresent("INDCOD"));
    OBJ_34.setVisible(lexique.isPresent("INDCOD"));
    OBJ_14.setVisible(!lexique.HostFieldGetData("CREA").trim().equalsIgnoreCase("CREATION"));
    OBJ_13.setVisible(!lexique.HostFieldGetData("CREA").trim().equalsIgnoreCase("CREATION"));
    OBJ_12.setVisible(!lexique.HostFieldGetData("CREA").trim().equalsIgnoreCase("CREATION"));
    CHOISIR.setVisible(!lexique.HostFieldGetData("CREA").trim().equalsIgnoreCase("CREATION"));
    
    // Titre
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(listeTop[Integer.parseInt(BTD.getInvoker().getName().substring(1, 2))], 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(listeTop[Integer.parseInt(BTD.getInvoker().getName().substring(1, 2))], 0, "4");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(listeTop[Integer.parseInt(BTD.getInvoker().getName().substring(1, 2))], 0, "S");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(listeTop[Integer.parseInt(BTD.getInvoker().getName().substring(1, 2))], 0, "G");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F13");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // Gestion de la surbrillance si zone sélectionnée
  
  private void L0MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L0.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L0.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[0], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L0.setBackground(Constantes.CL_ZONE_SORTIE);
        L0.setForeground(Color.black);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L0MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L0.setBackground(Constantes.CL_ZONE_SORTIE);
      L0.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L1MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L1.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L1.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[1], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L1.setBackground(Constantes.CL_ZONE_SORTIE);
        L1.setForeground(Color.black);
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L1MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L1.setBackground(Constantes.CL_ZONE_SORTIE);
      L1.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L2MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L2.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L2.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[2], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L2.setBackground(Constantes.CL_ZONE_SORTIE);
        L2.setForeground(Color.black);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L2MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L2.setBackground(Constantes.CL_ZONE_SORTIE);
      L2.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L3MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L3.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L3.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[3], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L3.setBackground(Constantes.CL_ZONE_SORTIE);
        L3.setForeground(Color.black);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L3MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L3.setBackground(Constantes.CL_ZONE_SORTIE);
      L3.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L4MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L4.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L4.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[4], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L4.setBackground(Constantes.CL_ZONE_SORTIE);
        L4.setForeground(Color.black);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L4MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L4.setBackground(Constantes.CL_ZONE_SORTIE);
      L4.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L5MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L5.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L5.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[5], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L5.setBackground(Constantes.CL_ZONE_SORTIE);
        L5.setForeground(Color.black);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L5MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L5.setBackground(Constantes.CL_ZONE_SORTIE);
      L5.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L6MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L6.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L6.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[6], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L6.setBackground(Constantes.CL_ZONE_SORTIE);
        L6.setForeground(Color.black);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L6MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L6.setBackground(Constantes.CL_ZONE_SORTIE);
      L6.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L7MouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L7.setBackground(Constantes.COULEUR_LISTE_FOND_BILAN);
      L7.setForeground(Color.white);
      if (e.getClickCount() == 2) {
        lexique.HostFieldPutData(listeTop[7], 0, "1");
        lexique.HostScreenSendKey(this, "ENTER");
        L7.setBackground(Constantes.CL_ZONE_SORTIE);
        L7.setForeground(Color.black);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void L7MouseExited(MouseEvent e) {
    try {
      // TODO Saisissez votre code
      L7.setBackground(Constantes.CL_ZONE_SORTIE);
      L7.setForeground(Color.black);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    INDLIB = new XRiTextField();
    INDCOD = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_33 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    L0 = new RiZoneSortie();
    L1 = new RiZoneSortie();
    L2 = new RiZoneSortie();
    L3 = new RiZoneSortie();
    L4 = new RiZoneSortie();
    L5 = new RiZoneSortie();
    L6 = new RiZoneSortie();
    L7 = new RiZoneSortie();
    D1 = new RiZoneSortie();
    D2 = new RiZoneSortie();
    D3 = new RiZoneSortie();
    D4 = new RiZoneSortie();
    D5 = new RiZoneSortie();
    D6 = new RiZoneSortie();
    D7 = new RiZoneSortie();
    D0 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@LIBMOD@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 862, Short.MAX_VALUE)
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 30, Short.MAX_VALUE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Cr\u00e9ation code planning");
              riSousMenu_bt6.setToolTipText("Cr\u00e9ation code planning");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {1019, 0};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {275, 0, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0E-4};

          //======== p_recup ========
          {
            p_recup.setBorder(new TitledBorder(""));
            p_recup.setOpaque(false);
            p_recup.setPreferredSize(new Dimension(720, 330));
            p_recup.setMinimumSize(new Dimension(720, 270));
            p_recup.setMaximumSize(new Dimension(720, 330));
            p_recup.setName("p_recup");
            p_recup.setLayout(null);

            //---- INDLIB ----
            INDLIB.setComponentPopupMenu(null);
            INDLIB.setName("INDLIB");
            p_recup.add(INDLIB);
            INDLIB.setBounds(145, 277, 310, INDLIB.getPreferredSize().height);

            //---- INDCOD ----
            INDCOD.setComponentPopupMenu(null);
            INDCOD.setName("INDCOD");
            p_recup.add(INDCOD);
            INDCOD.setBounds(25, 277, 110, INDCOD.getPreferredSize().height);

            //---- OBJ_34 ----
            OBJ_34.setText("Libell\u00e9");
            OBJ_34.setName("OBJ_34");
            p_recup.add(OBJ_34);
            OBJ_34.setBounds(145, 257, 81, 20);

            //---- OBJ_33 ----
            OBJ_33.setText("Code");
            OBJ_33.setName("OBJ_33");
            p_recup.add(OBJ_33);
            OBJ_33.setBounds(30, 257, 81, 20);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            p_recup.add(BT_PGUP);
            BT_PGUP.setBounds(660, 42, 25, 95);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            p_recup.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(660, 146, 25, 95);

            //---- L0 ----
            L0.setText("@L01@");
            L0.setComponentPopupMenu(BTD);
            L0.setName("L0");
            L0.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L0MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L0MouseExited(e);
              }
            });
            p_recup.add(L0);
            L0.setBounds(25, 42, 420, L0.getPreferredSize().height);

            //---- L1 ----
            L1.setText("@L02@");
            L1.setComponentPopupMenu(BTD);
            L1.setName("L1");
            L1.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L1MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L1MouseExited(e);
              }
            });
            p_recup.add(L1);
            L1.setBounds(25, 67, 420, 24);

            //---- L2 ----
            L2.setText("@L03@");
            L2.setComponentPopupMenu(BTD);
            L2.setName("L2");
            L2.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L2MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L2MouseExited(e);
              }
            });
            p_recup.add(L2);
            L2.setBounds(25, 92, 420, 24);

            //---- L3 ----
            L3.setText("@L04@");
            L3.setComponentPopupMenu(BTD);
            L3.setName("L3");
            L3.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L3MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L3MouseExited(e);
              }
            });
            p_recup.add(L3);
            L3.setBounds(25, 117, 420, 24);

            //---- L4 ----
            L4.setText("@L05@");
            L4.setComponentPopupMenu(BTD);
            L4.setName("L4");
            L4.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L4MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L4MouseExited(e);
              }
            });
            p_recup.add(L4);
            L4.setBounds(25, 142, 420, 24);

            //---- L5 ----
            L5.setText("@L06@");
            L5.setComponentPopupMenu(BTD);
            L5.setName("L5");
            L5.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L5MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L5MouseExited(e);
              }
            });
            p_recup.add(L5);
            L5.setBounds(25, 167, 420, 24);

            //---- L6 ----
            L6.setText("@L07@");
            L6.setComponentPopupMenu(BTD);
            L6.setName("L6");
            L6.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L6MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L6MouseExited(e);
              }
            });
            p_recup.add(L6);
            L6.setBounds(25, 192, 420, 24);

            //---- L7 ----
            L7.setText("@L08@");
            L7.setComponentPopupMenu(BTD);
            L7.setName("L7");
            L7.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                L7MouseClicked(e);
              }
              @Override
              public void mouseExited(MouseEvent e) {
                L7MouseExited(e);
              }
            });
            p_recup.add(L7);
            L7.setBounds(25, 217, 420, 24);

            //---- D1 ----
            D1.setText("@D02@");
            D1.setName("D1");
            p_recup.add(D1);
            D1.setBounds(450, 67, 200, D1.getPreferredSize().height);

            //---- D2 ----
            D2.setText("@D03@");
            D2.setName("D2");
            p_recup.add(D2);
            D2.setBounds(450, 92, 200, 24);

            //---- D3 ----
            D3.setText("@D04@");
            D3.setName("D3");
            p_recup.add(D3);
            D3.setBounds(450, 117, 200, D3.getPreferredSize().height);

            //---- D4 ----
            D4.setText("@D05@");
            D4.setName("D4");
            p_recup.add(D4);
            D4.setBounds(450, 142, 200, 24);

            //---- D5 ----
            D5.setText("@D06@");
            D5.setName("D5");
            p_recup.add(D5);
            D5.setBounds(450, 167, 200, D5.getPreferredSize().height);

            //---- D6 ----
            D6.setText("@D07@");
            D6.setName("D6");
            p_recup.add(D6);
            D6.setBounds(450, 192, 200, D6.getPreferredSize().height);

            //---- D7 ----
            D7.setText("@D08@");
            D7.setName("D7");
            p_recup.add(D7);
            D7.setBounds(450, 217, 200, D7.getPreferredSize().height);

            //---- D0 ----
            D0.setText("@D01@");
            D0.setName("D0");
            p_recup.add(D0);
            D0.setBounds(450, 42, 200, D0.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Code");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            p_recup.add(label1);
            label1.setBounds(28, 15, 100, 21);

            //---- label2 ----
            label2.setText("Libell\u00e9 code");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            p_recup.add(label2);
            label2.setBounds(138, 15, 300, 21);

            //---- label3 ----
            label3.setText("Date/heure soumission");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            p_recup.add(label3);
            label3.setBounds(453, 15, 195, 21);
          }
          p_contenu.add(p_recup, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(10, 10, 15, 10), 0, 0));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir un code planning");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_12 ----
      OBJ_12.setText("Supprimer un code planning");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Soumettre un code au planning des travaux AS400");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("G\u00e9rer le planning du code s\u00e9lectionn\u00e9");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField INDLIB;
  private XRiTextField INDCOD;
  private JLabel OBJ_34;
  private JLabel OBJ_33;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private RiZoneSortie L0;
  private RiZoneSortie L1;
  private RiZoneSortie L2;
  private RiZoneSortie L3;
  private RiZoneSortie L4;
  private RiZoneSortie L5;
  private RiZoneSortie L6;
  private RiZoneSortie L7;
  private RiZoneSortie D1;
  private RiZoneSortie D2;
  private RiZoneSortie D3;
  private RiZoneSortie D4;
  private RiZoneSortie D5;
  private RiZoneSortie D6;
  private RiZoneSortie D7;
  private RiZoneSortie D0;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
