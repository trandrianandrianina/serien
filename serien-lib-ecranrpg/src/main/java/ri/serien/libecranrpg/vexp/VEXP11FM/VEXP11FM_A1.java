
package ri.serien.libecranrpg.vexp.VEXP11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VEXP11FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] ARG14_Value = { "", "*", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  private String[] type_Value = { "", "C", "F", "R", "D", };
  private String[] type_lib = { "...", "Client", "Fournisseur", "Répertoire", "Autre contact", };
  
  public VEXP11FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    TIDX5.setValeurs("5", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX1.setValeurs("1", "RB");
    ARG14.setValeurs(ARG14_Value, null);
    ARG31.setValeurs(type_Value, type_lib);
    ARG41.setValeurs(type_Value, type_lib);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*DATE@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*TIME@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    Date d = new Date();
    SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
    riZoneSortie2.setText(f.format(d));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Appels téléphoniques"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_39 = new JLabel();
    OBJ_41 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ARG14 = new XRiComboBox();
    TIDX1 = new XRiRadioButton();
    OBJ_86 = new JLabel();
    TIDX2 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    ARG11 = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    OBJ_88 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_93 = new JLabel();
    ARG2 = new XRiTextField();
    OBJ_89 = new JLabel();
    DSTRP1 = new XRiTextField();
    DSTRP2 = new XRiTextField();
    DSTRP3 = new XRiTextField();
    ARG8 = new XRiTextField();
    ARG12 = new XRiTextField();
    ARG13 = new XRiTextField();
    HHFIN = new XRiTextField();
    MMFIN = new XRiTextField();
    panel2 = new JPanel();
    TVILLE = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG32 = new XRiTextField();
    ARG42 = new XRiTextField();
    OBJ_90 = new JLabel();
    OBJ_92 = new JLabel();
    TCDPOS = new XRiTextField();
    ARG31 = new XRiComboBox();
    ARG41 = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des appels t\u00e9l\u00e9phoniques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_39 ----
          OBJ_39.setText("Le");
          OBJ_39.setName("OBJ_39");
          p_tete_gauche.add(OBJ_39);
          OBJ_39.setBounds(5, 5, 21, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("\u00e0");
          OBJ_41.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_41.setName("OBJ_41");
          p_tete_gauche.add(OBJ_41);
          OBJ_41.setBounds(135, 5, 23, 20);

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@*DATE@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");
          p_tete_gauche.add(riZoneSortie1);
          riZoneSortie1.setBounds(45, 3, 80, riZoneSortie1.getPreferredSize().height);

          //---- riZoneSortie2 ----
          riZoneSortie2.setText("@*TIME@");
          riZoneSortie2.setOpaque(false);
          riZoneSortie2.setName("riZoneSortie2");
          p_tete_gauche.add(riZoneSortie2);
          riZoneSortie2.setBounds(175, 3, 65, riZoneSortie2.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Appels d'un tiers");
              riSousMenu_bt6.setToolTipText("Visualisation des appels d'un tiers");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(770, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- ARG14 ----
            ARG14.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Appels non r\u00e9gl\u00e9s",
              "En attente sur ligne",
              "A rappeler",
              "A traiter",
              "En cours",
              "Traitement interne",
              "Traitement sur site",
              "Traitement d\u00e9l\u00e9gu\u00e9",
              "Fin",
              "R\u00e9gl\u00e9"
            }));
            ARG14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG14.setName("ARG14");
            panel1.add(ARG14);
            ARG14.setBounds(540, 124, 150, ARG14.getPreferredSize().height);

            //---- TIDX1 ----
            TIDX1.setText("Date et heure de d\u00e9but");
            TIDX1.setToolTipText("Tri\u00e9 par");
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(30, 127, 162, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Date et heure de fin");
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(50, 166, 141, 20);

            //---- TIDX2 ----
            TIDX2.setText("Code destinataire");
            TIDX2.setToolTipText("Tri\u00e9 par");
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(30, 49, 131, 20);

            //---- TIDX5 ----
            TIDX5.setText("Type d'appel");
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(30, 88, 107, 20);

            //---- ARG11 ----
            ARG11.setToolTipText("jj.mm.aa");
            ARG11.setComponentPopupMenu(BTD);
            ARG11.setName("ARG11");
            panel1.add(ARG11);
            ARG11.setBounds(215, 123, 105, ARG11.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setComponentPopupMenu(BTD);
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(215, 162, 105, DATFIN.getPreferredSize().height);

            //---- OBJ_88 ----
            OBJ_88.setText("minutes");
            OBJ_88.setName("OBJ_88");
            panel1.add(OBJ_88);
            OBJ_88.setBounds(405, 127, 49, 20);

            //---- OBJ_94 ----
            OBJ_94.setText("minutes");
            OBJ_94.setName("OBJ_94");
            panel1.add(OBJ_94);
            OBJ_94.setBounds(405, 166, 49, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Rep 1");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(320, 49, 39, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Rep 2");
            OBJ_82.setName("OBJ_82");
            panel1.add(OBJ_82);
            OBJ_82.setBounds(455, 49, 39, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("Rep 3");
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(595, 49, 39, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("heure");
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(335, 127, 37, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("heure");
            OBJ_93.setName("OBJ_93");
            panel1.add(OBJ_93);
            OBJ_93.setBounds(335, 166, 37, 20);

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(BTD);
            ARG2.setName("ARG2");
            panel1.add(ARG2);
            ARG2.setBounds(215, 45, 40, ARG2.getPreferredSize().height);

            //---- OBJ_89 ----
            OBJ_89.setText("Etat");
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(500, 127, 27, 20);

            //---- DSTRP1 ----
            DSTRP1.setComponentPopupMenu(BTD);
            DSTRP1.setName("DSTRP1");
            panel1.add(DSTRP1);
            DSTRP1.setBounds(363, 45, 30, DSTRP1.getPreferredSize().height);

            //---- DSTRP2 ----
            DSTRP2.setComponentPopupMenu(BTD);
            DSTRP2.setName("DSTRP2");
            panel1.add(DSTRP2);
            DSTRP2.setBounds(501, 45, 30, DSTRP2.getPreferredSize().height);

            //---- DSTRP3 ----
            DSTRP3.setComponentPopupMenu(BTD);
            DSTRP3.setName("DSTRP3");
            panel1.add(DSTRP3);
            DSTRP3.setBounds(639, 45, 30, DSTRP3.getPreferredSize().height);

            //---- ARG8 ----
            ARG8.setComponentPopupMenu(BTD);
            ARG8.setName("ARG8");
            panel1.add(ARG8);
            ARG8.setBounds(215, 84, 30, ARG8.getPreferredSize().height);

            //---- ARG12 ----
            ARG12.setToolTipText("hh");
            ARG12.setComponentPopupMenu(BTD);
            ARG12.setName("ARG12");
            panel1.add(ARG12);
            ARG12.setBounds(375, 123, 26, ARG12.getPreferredSize().height);

            //---- ARG13 ----
            ARG13.setToolTipText("mm");
            ARG13.setComponentPopupMenu(BTD);
            ARG13.setName("ARG13");
            panel1.add(ARG13);
            ARG13.setBounds(460, 123, 26, ARG13.getPreferredSize().height);

            //---- HHFIN ----
            HHFIN.setComponentPopupMenu(BTD);
            HHFIN.setName("HHFIN");
            panel1.add(HHFIN);
            HHFIN.setBounds(375, 162, 26, HHFIN.getPreferredSize().height);

            //---- MMFIN ----
            MMFIN.setComponentPopupMenu(BTD);
            MMFIN.setName("MMFIN");
            panel1.add(MMFIN);
            MMFIN.setBounds(460, 162, 26, MMFIN.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Client, fournisseur, autres"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- TVILLE ----
              TVILLE.setComponentPopupMenu(BTD);
              TVILLE.setName("TVILLE");
              panel2.add(TVILLE);
              TVILLE.setBounds(345, 105, 210, TVILLE.getPreferredSize().height);

              //---- TIDX4 ----
              TIDX4.setText("Type et num\u00e9ro du tiers");
              TIDX4.setToolTipText("Tri\u00e9 par");
              TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX4.setName("TIDX4");
              panel2.add(TIDX4);
              TIDX4.setBounds(15, 70, 154, 20);

              //---- TIDX3 ----
              TIDX3.setText("Type et nom du tiers");
              TIDX3.setToolTipText("Tri\u00e9 par");
              TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX3.setName("TIDX3");
              panel2.add(TIDX3);
              TIDX3.setBounds(15, 35, 135, 20);

              //---- ARG32 ----
              ARG32.setComponentPopupMenu(BTD);
              ARG32.setName("ARG32");
              panel2.add(ARG32);
              ARG32.setBounds(345, 30, 150, ARG32.getPreferredSize().height);

              //---- ARG42 ----
              ARG42.setComponentPopupMenu(BTD);
              ARG42.setName("ARG42");
              panel2.add(ARG42);
              ARG42.setBounds(345, 65, 100, ARG42.getPreferredSize().height);

              //---- OBJ_90 ----
              OBJ_90.setText("Code postal");
              OBJ_90.setName("OBJ_90");
              panel2.add(OBJ_90);
              OBJ_90.setBounds(35, 109, 71, 20);

              //---- OBJ_92 ----
              OBJ_92.setText("Ville");
              OBJ_92.setName("OBJ_92");
              panel2.add(OBJ_92);
              OBJ_92.setBounds(295, 110, 40, 20);

              //---- TCDPOS ----
              TCDPOS.setComponentPopupMenu(BTD);
              TCDPOS.setName("TCDPOS");
              panel2.add(TCDPOS);
              TCDPOS.setBounds(200, 105, 60, TCDPOS.getPreferredSize().height);

              //---- ARG31 ----
              ARG31.setComponentPopupMenu(BTD);
              ARG31.setName("ARG31");
              panel2.add(ARG31);
              ARG31.setBounds(200, 31, 120, ARG31.getPreferredSize().height);

              //---- ARG41 ----
              ARG41.setComponentPopupMenu(BTD);
              ARG41.setName("ARG41");
              panel2.add(ARG41);
              ARG41.setBounds(200, 66, 120, ARG41.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(15, 200, 715, 160);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_39;
  private JLabel OBJ_41;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox ARG14;
  private XRiRadioButton TIDX1;
  private JLabel OBJ_86;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX5;
  private XRiCalendrier ARG11;
  private XRiCalendrier DATFIN;
  private JLabel OBJ_88;
  private JLabel OBJ_94;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JLabel OBJ_83;
  private JLabel OBJ_87;
  private JLabel OBJ_93;
  private XRiTextField ARG2;
  private JLabel OBJ_89;
  private XRiTextField DSTRP1;
  private XRiTextField DSTRP2;
  private XRiTextField DSTRP3;
  private XRiTextField ARG8;
  private XRiTextField ARG12;
  private XRiTextField ARG13;
  private XRiTextField HHFIN;
  private XRiTextField MMFIN;
  private JPanel panel2;
  private XRiTextField TVILLE;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG32;
  private XRiTextField ARG42;
  private JLabel OBJ_90;
  private JLabel OBJ_92;
  private XRiTextField TCDPOS;
  private XRiComboBox ARG31;
  private XRiComboBox ARG41;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
