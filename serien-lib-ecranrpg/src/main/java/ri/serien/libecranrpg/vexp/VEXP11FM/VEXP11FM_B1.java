
package ri.serien.libecranrpg.vexp.VEXP11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VEXP11FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] APURG_Value = { "", "1", "2", "3", "4", "5", "6", "7", "9", };
  private String[] APTYT_Value = { "", "C", "F", "P", "R", "D", };
  private String[] APETA_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  
  public VEXP11FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    APURG.setValeurs(APURG_Value, null);
    APTYT.setValeurs(APTYT_Value, null);
    APETA.setValeurs(APETA_Value, null);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    RPLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIBR@")).trim());
    WFAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFAX@")).trim());
    CLOBS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLOBS@")).trim());
    WEPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPOS@")).trim());
    CLPLF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLPLF@")).trim());
    WEDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDEP@")).trim());
    CLREP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLREP@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@APPAT@")).trim());
    WCZ3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCZ3@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCZ1@")).trim());
    WCZ6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCZ6@")).trim());
    INILIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INILIB@")).trim());
    DSTLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DSTLIB@")).trim());
    MOLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOLIB@")).trim());
    OBJ_142.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPSLIB@")).trim());
    OBJ_160.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL1@")).trim());
    OBJ_163.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL2@")).trim());
    OBJ_166.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL3@")).trim());
    OBJ_168.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL4@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCZ2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    WEPOS.setVisible(lexique.isTrue("(61) AND (N06)"));
    OBJ_101.setVisible(lexique.isPresent("WEPOS"));
    CLPLF.setVisible(lexique.isTrue("(61) AND (N07) AND (N06)"));
    OBJ_109.setVisible(lexique.isPresent("CLPLF"));
    WEDEP.setVisible(lexique.isTrue("(61) AND (N08) AND (N06)"));
    OBJ_103.setVisible(lexique.isPresent("WEDEP"));
    APCOT.setVisible(lexique.isTrue("(N61) AND (N62)"));
    APTCLI.setVisible(lexique.isTrue("61"));
    APTLIV.setVisible(lexique.isTrue("61"));
    APTCOL.setVisible(lexique.isTrue("62"));
    APTFRS.setVisible(lexique.isTrue("62"));
    OBJ_99.setVisible(lexique.isTrue("(41) AND (62)"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(APETT.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(APETT.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vexp11"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WF4EN");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_45 = new JLabel();
    APDTDX = new XRiCalendrier();
    OBJ_55 = new JLabel();
    APDTFX = new XRiCalendrier();
    APHRDH = new XRiTextField();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    APHRDM = new XRiTextField();
    OBJ_51 = new JLabel();
    APHRFH = new XRiTextField();
    OBJ_57 = new JLabel();
    APHRFM = new XRiTextField();
    OBJ_59 = new JLabel();
    OBJ_61 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    APETT = new XRiTextField();
    APCOT = new XRiTextField();
    APTCLI = new XRiTextField();
    APTFRS = new XRiTextField();
    APTCOL = new XRiTextField();
    APTLIV = new XRiTextField();
    APTYT = new XRiComboBox();
    OBJ_75 = new JLabel();
    APNOT = new XRiTextField();
    OBJ_87 = new JLabel();
    APPAT = new XRiTextField();
    RPLIBR = new RiZoneSortie();
    APTLT = new XRiTextField();
    WFAX = new RiZoneSortie();
    CLOBS = new RiZoneSortie();
    APPOT = new XRiTextField();
    OBJ_101 = new JLabel();
    WEPOS = new RiZoneSortie();
    CLPLF = new RiZoneSortie();
    WEDEP = new RiZoneSortie();
    OBJ_109 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_83 = new JLabel();
    CLREP = new RiZoneSortie();
    OBJ_93 = new JLabel();
    XRNOM = new XRiTextField();
    OBJ_76 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_89 = new JLabel();
    WCZ3 = new JLabel();
    XRPRE = new XRiTextField();
    OBJ_96 = new JLabel();
    CLTNS = new XRiTextField();
    CLATT = new XRiTextField();
    OBJ_84 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_115 = new JLabel();
    WCZ6 = new JLabel();
    OBJ_99 = new JLabel();
    panel3 = new JPanel();
    INILIB = new RiZoneSortie();
    DSTLIB = new RiZoneSortie();
    MOLIB = new RiZoneSortie();
    OBJ_121 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_143 = new JLabel();
    APNUD = new XRiTextField();
    APMOT = new XRiTextField();
    APINI = new XRiTextField();
    APDST = new XRiTextField();
    APETA = new XRiComboBox();
    APURG = new XRiComboBox();
    OBJ_142 = new RiZoneSortie();
    TALIBR = new XRiTextField();
    OBJ_133 = new JLabel();
    OBJ_140 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_125 = new JLabel();
    APTYP = new XRiTextField();
    APLIG = new XRiTextField();
    APTPS = new XRiTextField();
    OBJ_88 = new JLabel();
    APRYT = new XRiTextField();
    panel4 = new JPanel();
    APTXT1 = new XRiTextField();
    APTXT2 = new XRiTextField();
    APTXT3 = new XRiTextField();
    APZP1 = new XRiTextField();
    APZP2 = new XRiTextField();
    APZP3 = new XRiTextField();
    APZP4 = new XRiTextField();
    OBJ_160 = new JLabel();
    OBJ_163 = new JLabel();
    OBJ_166 = new JLabel();
    OBJ_168 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_24 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_114 = new JLabel();
    WFZ1 = new JTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des appels t\u00e9l\u00e9phoniques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_45 ----
          OBJ_45.setText("Appel du");
          OBJ_45.setName("OBJ_45");
          p_tete_gauche.add(OBJ_45);
          OBJ_45.setBounds(5, 5, 67, 18);

          //---- APDTDX ----
          APDTDX.setComponentPopupMenu(BTD);
          APDTDX.setName("APDTDX");
          p_tete_gauche.add(APDTDX);
          APDTDX.setBounds(70, 0, 105, APDTDX.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("Fin le");
          OBJ_55.setName("OBJ_55");
          p_tete_gauche.add(OBJ_55);
          OBJ_55.setBounds(420, 5, 67, 18);

          //---- APDTFX ----
          APDTFX.setComponentPopupMenu(BTD);
          APDTFX.setName("APDTFX");
          p_tete_gauche.add(APDTFX);
          APDTFX.setBounds(485, 0, 105, APDTFX.getPreferredSize().height);

          //---- APHRDH ----
          APHRDH.setComponentPopupMenu(BTD);
          APHRDH.setName("APHRDH");
          p_tete_gauche.add(APHRDH);
          APHRDH.setBounds(210, 0, 26, APHRDH.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("\u00e0");
          OBJ_47.setName("OBJ_47");
          p_tete_gauche.add(OBJ_47);
          OBJ_47.setBounds(185, 5, 13, 18);

          //---- OBJ_49 ----
          OBJ_49.setText("heure");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(240, 5, 39, 18);

          //---- APHRDM ----
          APHRDM.setComponentPopupMenu(BTD);
          APHRDM.setName("APHRDM");
          p_tete_gauche.add(APHRDM);
          APHRDM.setBounds(285, 0, 26, APHRDM.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("minutes");
          OBJ_51.setName("OBJ_51");
          p_tete_gauche.add(OBJ_51);
          OBJ_51.setBounds(315, 5, 55, 18);

          //---- APHRFH ----
          APHRFH.setComponentPopupMenu(BTD);
          APHRFH.setName("APHRFH");
          p_tete_gauche.add(APHRFH);
          APHRFH.setBounds(625, 0, 26, APHRFH.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("\u00e0");
          OBJ_57.setName("OBJ_57");
          p_tete_gauche.add(OBJ_57);
          OBJ_57.setBounds(600, 5, 13, 18);

          //---- APHRFM ----
          APHRFM.setComponentPopupMenu(BTD);
          APHRFM.setName("APHRFM");
          p_tete_gauche.add(APHRFM);
          APHRFM.setBounds(705, 0, 26, APHRFM.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setText("heure");
          OBJ_59.setName("OBJ_59");
          p_tete_gauche.add(OBJ_59);
          OBJ_59.setBounds(655, 5, 39, 18);

          //---- OBJ_61 ----
          OBJ_61.setText("minutes");
          OBJ_61.setName("OBJ_61");
          p_tete_gauche.add(OBJ_61);
          OBJ_61.setBounds(735, 5, 55, 18);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Ligne archive");
              riSousMenu_bt6.setToolTipText("G\u00e9n\u00e9ration d'une ligne archive personnalis\u00e9e");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Fiche sp\u00e9cifique client");
              riSousMenu_bt7.setToolTipText("Fiche sp\u00e9cifique client");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Appels d'un tiers");
              riSousMenu_bt8.setToolTipText("Visualisation des appels d'un tiers");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Fiche observation");
              riSousMenu_bt9.setToolTipText("Fiche observation");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Extension de texte");
              riSousMenu_bt10.setToolTipText("Extension texte sur un appel");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Fonctions");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Fonctions GMM \u00e9tendues");
              riSousMenu_bt18.setToolTipText("Affichage \u00e9tendu des fonctions GMM");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Encours");
              riSousMenu_bt19.setToolTipText("Encours comptable");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Tiers"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- APETT ----
              APETT.setComponentPopupMenu(BTD);
              APETT.setName("APETT");
              panel2.add(APETT);
              APETT.setBounds(120, 39, 40, APETT.getPreferredSize().height);

              //---- APCOT ----
              APCOT.setComponentPopupMenu(BTD);
              APCOT.setName("APCOT");
              panel2.add(APCOT);
              APCOT.setBounds(160, 39, 100, APCOT.getPreferredSize().height);

              //---- APTCLI ----
              APTCLI.setComponentPopupMenu(BTD);
              APTCLI.setName("APTCLI");
              panel2.add(APTCLI);
              APTCLI.setBounds(160, 39, 60, APTCLI.getPreferredSize().height);

              //---- APTFRS ----
              APTFRS.setComponentPopupMenu(BTD);
              APTFRS.setName("APTFRS");
              panel2.add(APTFRS);
              APTFRS.setBounds(190, 39, 70, APTFRS.getPreferredSize().height);

              //---- APTCOL ----
              APTCOL.setComponentPopupMenu(BTD);
              APTCOL.setName("APTCOL");
              panel2.add(APTCOL);
              APTCOL.setBounds(160, 39, 20, APTCOL.getPreferredSize().height);

              //---- APTLIV ----
              APTLIV.setComponentPopupMenu(BTD);
              APTLIV.setName("APTLIV");
              panel2.add(APTLIV);
              APTLIV.setBounds(226, 39, 34, APTLIV.getPreferredSize().height);

              //---- APTYT ----
              APTYT.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Client",
                "Fournisseur",
                "Prospect",
                "R\u00e9pertoire",
                "Divers"
              }));
              APTYT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              APTYT.setName("APTYT");
              panel2.add(APTYT);
              APTYT.setBounds(25, 40, 95, APTYT.getPreferredSize().height);

              //---- OBJ_75 ----
              OBJ_75.setText("Nom ou raison sociale");
              OBJ_75.setName("OBJ_75");
              panel2.add(OBJ_75);
              OBJ_75.setBounds(25, 70, 310, 16);

              //---- APNOT ----
              APNOT.setComponentPopupMenu(BTD);
              APNOT.setName("APNOT");
              panel2.add(APNOT);
              APNOT.setBounds(25, 85, 310, APNOT.getPreferredSize().height);

              //---- OBJ_87 ----
              OBJ_87.setText("Personne \u00e0 contacter");
              OBJ_87.setName("OBJ_87");
              panel2.add(OBJ_87);
              OBJ_87.setBounds(25, 115, 310, 16);

              //---- APPAT ----
              APPAT.setComponentPopupMenu(BTD);
              APPAT.setName("APPAT");
              panel2.add(APPAT);
              APPAT.setBounds(25, 129, 310, APPAT.getPreferredSize().height);

              //---- RPLIBR ----
              RPLIBR.setText("@RPLIBR@");
              RPLIBR.setName("RPLIBR");
              panel2.add(RPLIBR);
              RPLIBR.setBounds(525, 131, 240, RPLIBR.getPreferredSize().height);

              //---- APTLT ----
              APTLT.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
              APTLT.setComponentPopupMenu(BTD);
              APTLT.setName("APTLT");
              panel2.add(APTLT);
              APTLT.setBounds(490, 30, 210, APTLT.getPreferredSize().height);

              //---- WFAX ----
              WFAX.setToolTipText("Num\u00e9ro de fax");
              WFAX.setText("@WFAX@");
              WFAX.setName("WFAX");
              panel2.add(WFAX);
              WFAX.setBounds(490, 65, 210, WFAX.getPreferredSize().height);

              //---- CLOBS ----
              CLOBS.setText("@CLOBS@");
              CLOBS.setName("CLOBS");
              panel2.add(CLOBS);
              CLOBS.setBounds(490, 98, 210, CLOBS.getPreferredSize().height);

              //---- APPOT ----
              APPOT.setComponentPopupMenu(BTD);
              APPOT.setName("APPOT");
              panel2.add(APPOT);
              APPOT.setBounds(790, 30, 60, APPOT.getPreferredSize().height);

              //---- OBJ_101 ----
              OBJ_101.setText("Position");
              OBJ_101.setName("OBJ_101");
              panel2.add(OBJ_101);
              OBJ_101.setBounds(395, 170, 51, 20);

              //---- WEPOS ----
              WEPOS.setText("@WEPOS@");
              WEPOS.setHorizontalAlignment(SwingConstants.RIGHT);
              WEPOS.setHorizontalTextPosition(SwingConstants.RIGHT);
              WEPOS.setName("WEPOS");
              panel2.add(WEPOS);
              WEPOS.setBounds(490, 168, 66, WEPOS.getPreferredSize().height);

              //---- CLPLF ----
              CLPLF.setText("@CLPLF@");
              CLPLF.setHorizontalAlignment(SwingConstants.RIGHT);
              CLPLF.setHorizontalTextPosition(SwingConstants.RIGHT);
              CLPLF.setName("CLPLF");
              panel2.add(CLPLF);
              CLPLF.setBounds(625, 168, 66, CLPLF.getPreferredSize().height);

              //---- WEDEP ----
              WEDEP.setForeground(new Color(204, 0, 0));
              WEDEP.setText("@WEDEP@");
              WEDEP.setHorizontalAlignment(SwingConstants.RIGHT);
              WEDEP.setHorizontalTextPosition(SwingConstants.RIGHT);
              WEDEP.setName("WEDEP");
              panel2.add(WEDEP);
              WEDEP.setBounds(790, 168, 66, WEDEP.getPreferredSize().height);

              //---- OBJ_109 ----
              OBJ_109.setText("Plafond");
              OBJ_109.setName("OBJ_109");
              panel2.add(OBJ_109);
              OBJ_109.setBounds(565, 170, 49, 20);

              //---- OBJ_77 ----
              OBJ_77.setText("Poste");
              OBJ_77.setName("OBJ_77");
              panel2.add(OBJ_77);
              OBJ_77.setBounds(720, 36, 59, 16);

              //---- OBJ_83 ----
              OBJ_83.setText("Observation");
              OBJ_83.setName("OBJ_83");
              panel2.add(OBJ_83);
              OBJ_83.setBounds(395, 100, 85, 20);

              //---- CLREP ----
              CLREP.setText("@CLREP@");
              CLREP.setName("CLREP");
              panel2.add(CLREP);
              CLREP.setBounds(490, 131, 30, CLREP.getPreferredSize().height);

              //---- OBJ_93 ----
              OBJ_93.setText("Repr\u00e9sentant");
              OBJ_93.setName("OBJ_93");
              panel2.add(OBJ_93);
              OBJ_93.setBounds(395, 135, 85, 16);

              //---- XRNOM ----
              XRNOM.setComponentPopupMenu(BTD);
              XRNOM.setName("XRNOM");
              panel2.add(XRNOM);
              XRNOM.setBounds(25, 170, 310, XRNOM.getPreferredSize().height);

              //---- OBJ_76 ----
              OBJ_76.setText("Etb");
              OBJ_76.setName("OBJ_76");
              panel2.add(OBJ_76);
              OBJ_76.setBounds(120, 20, 40, 21);

              //---- OBJ_78 ----
              OBJ_78.setText("Code");
              OBJ_78.setName("OBJ_78");
              panel2.add(OBJ_78);
              OBJ_78.setBounds(160, 20, 100, 21);

              //---- OBJ_89 ----
              OBJ_89.setText("@APPAT@");
              OBJ_89.setName("OBJ_89");
              panel2.add(OBJ_89);
              OBJ_89.setBounds(25, 155, 310, OBJ_89.getPreferredSize().height);

              //---- WCZ3 ----
              WCZ3.setText("@WCZ3@");
              WCZ3.setName("WCZ3");
              panel2.add(WCZ3);
              WCZ3.setBounds(25, 220, 495, 20);

              //---- XRPRE ----
              XRPRE.setComponentPopupMenu(BTD);
              XRPRE.setName("XRPRE");
              panel2.add(XRPRE);
              XRPRE.setBounds(25, 195, 310, XRPRE.getPreferredSize().height);

              //---- OBJ_96 ----
              OBJ_96.setText("Attn");
              OBJ_96.setName("OBJ_96");
              panel2.add(OBJ_96);
              OBJ_96.setBounds(395, 199, 25, 20);

              //---- CLTNS ----
              CLTNS.setName("CLTNS");
              panel2.add(CLTNS);
              CLTNS.setBounds(490, 195, 25, CLTNS.getPreferredSize().height);

              //---- CLATT ----
              CLATT.setName("CLATT");
              panel2.add(CLATT);
              CLATT.setBounds(525, 195, 255, CLATT.getPreferredSize().height);

              //---- OBJ_84 ----
              OBJ_84.setText("T\u00e9l\u00e9phone");
              OBJ_84.setName("OBJ_84");
              panel2.add(OBJ_84);
              OBJ_84.setBounds(395, 34, 85, 20);

              //---- OBJ_85 ----
              OBJ_85.setText("Fax");
              OBJ_85.setName("OBJ_85");
              panel2.add(OBJ_85);
              OBJ_85.setBounds(395, 67, 85, 20);

              //---- OBJ_103 ----
              OBJ_103.setText("D\u00e9passement");
              OBJ_103.setForeground(new Color(204, 0, 0));
              OBJ_103.setName("OBJ_103");
              panel2.add(OBJ_103);
              OBJ_103.setBounds(700, 170, 90, 20);

              //---- OBJ_115 ----
              OBJ_115.setText("@WCZ1@");
              OBJ_115.setName("OBJ_115");
              panel2.add(OBJ_115);
              OBJ_115.setBounds(525, 220, 225, 20);

              //---- WCZ6 ----
              WCZ6.setText("@WCZ6@");
              WCZ6.setName("WCZ6");
              panel2.add(WCZ6);
              WCZ6.setBounds(755, 220, 105, 20);

              //---- OBJ_99 ----
              OBJ_99.setText("En litige");
              OBJ_99.setFont(OBJ_99.getFont().deriveFont(OBJ_99.getFont().getStyle() | Font.BOLD, OBJ_99.getFont().getSize() + 3f));
              OBJ_99.setForeground(new Color(204, 0, 51));
              OBJ_99.setName("OBJ_99");
              panel2.add(OBJ_99);
              OBJ_99.setBounds(new Rectangle(new Point(270, 43), OBJ_99.getPreferredSize()));

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(5, 0, 870, 250);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Divers"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- INILIB ----
              INILIB.setText("@INILIB@");
              INILIB.setName("INILIB");
              panel3.add(INILIB);
              INILIB.setBounds(630, 32, 210, INILIB.getPreferredSize().height);

              //---- DSTLIB ----
              DSTLIB.setText("@DSTLIB@");
              DSTLIB.setName("DSTLIB");
              panel3.add(DSTLIB);
              DSTLIB.setBounds(630, 59, 210, DSTLIB.getPreferredSize().height);

              //---- MOLIB ----
              MOLIB.setText("@MOLIB@");
              MOLIB.setName("MOLIB");
              panel3.add(MOLIB);
              MOLIB.setBounds(630, 86, 210, MOLIB.getPreferredSize().height);

              //---- OBJ_121 ----
              OBJ_121.setText("Interlocuteur");
              OBJ_121.setName("OBJ_121");
              panel3.add(OBJ_121);
              OBJ_121.setBounds(455, 34, 75, 20);

              //---- OBJ_128 ----
              OBJ_128.setText("Destinataire");
              OBJ_128.setName("OBJ_128");
              panel3.add(OBJ_128);
              OBJ_128.setBounds(455, 61, 75, 20);

              //---- OBJ_143 ----
              OBJ_143.setText("Intervention");
              OBJ_143.setName("OBJ_143");
              panel3.add(OBJ_143);
              OBJ_143.setBounds(455, 115, 72, 20);

              //---- APNUD ----
              APNUD.setComponentPopupMenu(BTD);
              APNUD.setName("APNUD");
              panel3.add(APNUD);
              APNUD.setBounds(545, 111, 58, APNUD.getPreferredSize().height);

              //---- APMOT ----
              APMOT.setComponentPopupMenu(BTD);
              APMOT.setName("APMOT");
              panel3.add(APMOT);
              APMOT.setBounds(545, 84, 60, APMOT.getPreferredSize().height);

              //---- APINI ----
              APINI.setComponentPopupMenu(BTD);
              APINI.setName("APINI");
              panel3.add(APINI);
              APINI.setBounds(545, 30, 40, APINI.getPreferredSize().height);

              //---- APDST ----
              APDST.setComponentPopupMenu(BTD);
              APDST.setName("APDST");
              panel3.add(APDST);
              APDST.setBounds(545, 57, 40, APDST.getPreferredSize().height);

              //---- APETA ----
              APETA.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "En attente sur ligne",
                "A rappeler",
                "A traiter",
                "En cours",
                "Traitement interne",
                "Traitement sur site",
                "Traitement d\u00e9l\u00e9gu\u00e9",
                "Fin",
                "R\u00e9gl\u00e9"
              }));
              APETA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              APETA.setName("APETA");
              panel3.add(APETA);
              APETA.setBounds(100, 58, 147, APETA.getPreferredSize().height);

              //---- APURG ----
              APURG.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Au plus t\u00f4t",
                "Voir texte",
                "Avant la fin de la journ\u00e9e",
                "Avant la fin de la semaine",
                "Avant la fin du mois",
                "Avant la fin de l'ann\u00e9e",
                "Attente de traitement",
                "A l'occasion"
              }));
              APURG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              APURG.setName("APURG");
              panel3.add(APURG);
              APURG.setBounds(100, 85, 147, APURG.getPreferredSize().height);

              //---- OBJ_142 ----
              OBJ_142.setText("@TPSLIB@");
              OBJ_142.setName("OBJ_142");
              panel3.add(OBJ_142);
              OBJ_142.setBounds(125, 113, 183, OBJ_142.getPreferredSize().height);

              //---- TALIBR ----
              TALIBR.setName("TALIBR");
              panel3.add(TALIBR);
              TALIBR.setBounds(130, 30, 210, TALIBR.getPreferredSize().height);

              //---- OBJ_133 ----
              OBJ_133.setText("Urgence");
              OBJ_133.setName("OBJ_133");
              panel3.add(OBJ_133);
              OBJ_133.setBounds(30, 88, 55, 20);

              //---- OBJ_140 ----
              OBJ_140.setText("Dur\u00e9e");
              OBJ_140.setName("OBJ_140");
              panel3.add(OBJ_140);
              OBJ_140.setBounds(30, 115, 40, 20);

              //---- OBJ_118 ----
              OBJ_118.setText("Type");
              OBJ_118.setName("OBJ_118");
              panel3.add(OBJ_118);
              OBJ_118.setBounds(30, 34, 36, 20);

              //---- OBJ_135 ----
              OBJ_135.setText("Motif");
              OBJ_135.setName("OBJ_135");
              panel3.add(OBJ_135);
              OBJ_135.setBounds(455, 88, 31, 20);

              //---- OBJ_125 ----
              OBJ_125.setText("Etat");
              OBJ_125.setName("OBJ_125");
              panel3.add(OBJ_125);
              OBJ_125.setBounds(30, 61, 27, 20);

              //---- APTYP ----
              APTYP.setComponentPopupMenu(BTD);
              APTYP.setName("APTYP");
              panel3.add(APTYP);
              APTYP.setBounds(100, 30, 30, APTYP.getPreferredSize().height);

              //---- APLIG ----
              APLIG.setComponentPopupMenu(BTD);
              APLIG.setName("APLIG");
              panel3.add(APLIG);
              APLIG.setBounds(265, 57, 30, APLIG.getPreferredSize().height);

              //---- APTPS ----
              APTPS.setComponentPopupMenu(BTD);
              APTPS.setName("APTPS");
              panel3.add(APTPS);
              APTPS.setBounds(100, 111, 18, APTPS.getPreferredSize().height);

              //---- OBJ_88 ----
              OBJ_88.setText("Rythme d'appel");
              OBJ_88.setName("OBJ_88");
              panel3.add(OBJ_88);
              OBJ_88.setBounds(630, 115, 117, 20);

              //---- APRYT ----
              APRYT.setComponentPopupMenu(BTD);
              APRYT.setName("APRYT");
              panel3.add(APRYT);
              APRYT.setBounds(814, 111, 26, APRYT.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(5, 250, 870, 150);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Contenu de l'appel"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- APTXT1 ----
              APTXT1.setComponentPopupMenu(BTD);
              APTXT1.setName("APTXT1");
              panel4.add(APTXT1);
              APTXT1.setBounds(105, 30, 610, APTXT1.getPreferredSize().height);

              //---- APTXT2 ----
              APTXT2.setComponentPopupMenu(BTD);
              APTXT2.setName("APTXT2");
              panel4.add(APTXT2);
              APTXT2.setBounds(105, 55, 610, APTXT2.getPreferredSize().height);

              //---- APTXT3 ----
              APTXT3.setComponentPopupMenu(BTD);
              APTXT3.setName("APTXT3");
              panel4.add(APTXT3);
              APTXT3.setBounds(105, 80, 610, APTXT3.getPreferredSize().height);

              //---- APZP1 ----
              APZP1.setComponentPopupMenu(BTD);
              APZP1.setName("APZP1");
              panel4.add(APZP1);
              APZP1.setBounds(235, 110, 160, APZP1.getPreferredSize().height);

              //---- APZP2 ----
              APZP2.setComponentPopupMenu(BTD);
              APZP2.setName("APZP2");
              panel4.add(APZP2);
              APZP2.setBounds(555, 110, 160, APZP2.getPreferredSize().height);

              //---- APZP3 ----
              APZP3.setComponentPopupMenu(BTD);
              APZP3.setName("APZP3");
              panel4.add(APZP3);
              APZP3.setBounds(235, 135, 160, APZP3.getPreferredSize().height);

              //---- APZP4 ----
              APZP4.setComponentPopupMenu(BTD);
              APZP4.setName("APZP4");
              panel4.add(APZP4);
              APZP4.setBounds(555, 135, 160, APZP4.getPreferredSize().height);

              //---- OBJ_160 ----
              OBJ_160.setText("@ZPL1@");
              OBJ_160.setName("OBJ_160");
              panel4.add(OBJ_160);
              OBJ_160.setBounds(105, 110, 106, 20);

              //---- OBJ_163 ----
              OBJ_163.setText("@ZPL2@");
              OBJ_163.setName("OBJ_163");
              panel4.add(OBJ_163);
              OBJ_163.setBounds(430, 110, 106, 20);

              //---- OBJ_166 ----
              OBJ_166.setText("@ZPL3@");
              OBJ_166.setName("OBJ_166");
              panel4.add(OBJ_166);
              OBJ_166.setBounds(105, 135, 106, 20);

              //---- OBJ_168 ----
              OBJ_168.setText("@ZPL4@");
              OBJ_168.setName("OBJ_168");
              panel4.add(OBJ_168);
              OBJ_168.setBounds(430, 135, 106, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel4);
            panel4.setBounds(5, 400, 870, 175);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_24 ----
      OBJ_24.setText("Choix possibles");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_23 ----
      OBJ_23.setText("Aide en ligne");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
    }

    //---- OBJ_114 ----
    OBJ_114.setText("@WCZ2@");
    OBJ_114.setName("OBJ_114");

    //---- WFZ1 ----
    WFZ1.setName("WFZ1");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_45;
  private XRiCalendrier APDTDX;
  private JLabel OBJ_55;
  private XRiCalendrier APDTFX;
  private XRiTextField APHRDH;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private XRiTextField APHRDM;
  private JLabel OBJ_51;
  private XRiTextField APHRFH;
  private JLabel OBJ_57;
  private XRiTextField APHRFM;
  private JLabel OBJ_59;
  private JLabel OBJ_61;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel2;
  private XRiTextField APETT;
  private XRiTextField APCOT;
  private XRiTextField APTCLI;
  private XRiTextField APTFRS;
  private XRiTextField APTCOL;
  private XRiTextField APTLIV;
  private XRiComboBox APTYT;
  private JLabel OBJ_75;
  private XRiTextField APNOT;
  private JLabel OBJ_87;
  private XRiTextField APPAT;
  private RiZoneSortie RPLIBR;
  private XRiTextField APTLT;
  private RiZoneSortie WFAX;
  private RiZoneSortie CLOBS;
  private XRiTextField APPOT;
  private JLabel OBJ_101;
  private RiZoneSortie WEPOS;
  private RiZoneSortie CLPLF;
  private RiZoneSortie WEDEP;
  private JLabel OBJ_109;
  private JLabel OBJ_77;
  private JLabel OBJ_83;
  private RiZoneSortie CLREP;
  private JLabel OBJ_93;
  private XRiTextField XRNOM;
  private JLabel OBJ_76;
  private JLabel OBJ_78;
  private JLabel OBJ_89;
  private JLabel WCZ3;
  private XRiTextField XRPRE;
  private JLabel OBJ_96;
  private XRiTextField CLTNS;
  private XRiTextField CLATT;
  private JLabel OBJ_84;
  private JLabel OBJ_85;
  private JLabel OBJ_103;
  private JLabel OBJ_115;
  private JLabel WCZ6;
  private JLabel OBJ_99;
  private JPanel panel3;
  private RiZoneSortie INILIB;
  private RiZoneSortie DSTLIB;
  private RiZoneSortie MOLIB;
  private JLabel OBJ_121;
  private JLabel OBJ_128;
  private JLabel OBJ_143;
  private XRiTextField APNUD;
  private XRiTextField APMOT;
  private XRiTextField APINI;
  private XRiTextField APDST;
  private XRiComboBox APETA;
  private XRiComboBox APURG;
  private RiZoneSortie OBJ_142;
  private XRiTextField TALIBR;
  private JLabel OBJ_133;
  private JLabel OBJ_140;
  private JLabel OBJ_118;
  private JLabel OBJ_135;
  private JLabel OBJ_125;
  private XRiTextField APTYP;
  private XRiTextField APLIG;
  private XRiTextField APTPS;
  private JLabel OBJ_88;
  private XRiTextField APRYT;
  private JPanel panel4;
  private XRiTextField APTXT1;
  private XRiTextField APTXT2;
  private XRiTextField APTXT3;
  private XRiTextField APZP1;
  private XRiTextField APZP2;
  private XRiTextField APZP3;
  private XRiTextField APZP4;
  private JLabel OBJ_160;
  private JLabel OBJ_163;
  private JLabel OBJ_166;
  private JLabel OBJ_168;
  private JPopupMenu BTD;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_23;
  private JLabel OBJ_114;
  private JTextField WFZ1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
