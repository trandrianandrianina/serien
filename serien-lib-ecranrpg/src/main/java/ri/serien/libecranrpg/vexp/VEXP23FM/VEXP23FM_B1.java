
package ri.serien.libecranrpg.vexp.VEXP23FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP23FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] PDFIC_Value = { "", "GVM", "ART", "LOT", "CLI", "REP", "AFF", "BON", "DEV", "GAM", "FRS", "ACT", "PRM", "GMM", "SAV",
      "INT", "TRM", "GPM", "OPE", "CGM", "NCA", "NCG", "ECC", "IMO", "TBM", "TDS", "WEB", "EXP", "SPM", };
  
  public VEXP23FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    PDFIC.setValeurs(PDFIC_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    PDDOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PDDOC@")).trim());
  }
  
  @Override
  @SuppressWarnings("unchecked")
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    PDFIC.removeAllItems();
    PDFIC.addItem("Choisir un type de fiche");
    PDFIC.addItem("Documents divers de gestion des ventes");
    PDFIC.addItem("Article");
    PDFIC.addItem("Lot d'articles");
    PDFIC.addItem("Client");
    PDFIC.addItem("Représentant");
    PDFIC.addItem("Affaires");
    PDFIC.addItem("Bon de vente");
    PDFIC.addItem("Devis");
    PDFIC.addItem("Documents divers de gestion des achats");
    PDFIC.addItem("Fournisseur");
    PDFIC.addItem("Actions commerciales");
    PDFIC.addItem("Prospection");
    PDFIC.addItem("Documents divers service aprés vente");
    PDFIC.addItem("Service après vente");
    PDFIC.addItem("Intervention");
    PDFIC.addItem("Documents divers de transport");
    PDFIC.addItem("Documents divers de gestion de production");
    PDFIC.addItem("Opérations de production");
    PDFIC.addItem("Documents divers de comptabilité");
    PDFIC.addItem("Plan comptable auxiliaire");
    PDFIC.addItem("Plan comptable général");
    PDFIC.addItem("Ecriture comptable");
    PDFIC.addItem("Immobilisation");
    PDFIC.addItem("Documents divers de tableau de bord comptable");
    PDFIC.addItem("Documents divers de TDS");
    PDFIC.addItem("Documents divers WEB");
    PDFIC.addItem("Documents divers d'exploitation");
    PDFIC.addItem("Documents divers spécifiques");
    
    PDFIC.setSelectedIndex(getIndice("PDFIC", PDFIC_Value));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Documents liés"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    PDIN1 = new XRiCheckBox();
    PDIN2 = new XRiCheckBox();
    PDIN4 = new XRiCheckBox();
    PDIN3 = new XRiCheckBox();
    PDLIB = new XRiTextField();
    PDCHM0 = new XRiTextField();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_41 = new JLabel();
    PDDOC = new RiZoneSortie();
    PDFIC = new XRiComboBox();
    panel2 = new JPanel();
    OBJ_50 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_69 = new JLabel();
    LCHM1 = new XRiTextField();
    LCHM5 = new XRiTextField();
    LCHM4 = new XRiTextField();
    LCHM3 = new XRiTextField();
    LCHM2 = new XRiTextField();
    PDCHM1 = new XRiTextField();
    PDCHM2 = new XRiTextField();
    PDCHM3 = new XRiTextField();
    PDCHM4 = new XRiTextField();
    PDCHM5 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("D\u00e9tail d'un type de document li\u00e9"));
          panel1.setOpaque(false);
          panel1.setPreferredSize(new Dimension(745, 500));
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- PDIN1 ----
          PDIN1.setText("Faire intervenir la biblioth\u00e8que dans la constitution du chemin");
          PDIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PDIN1.setName("PDIN1");
          panel1.add(PDIN1);
          PDIN1.setBounds(23, 130, 430, 20);

          //---- PDIN2 ----
          PDIN2.setText("Faire intervenir l'\u00e9tablissement dans la constitution du chemin");
          PDIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PDIN2.setName("PDIN2");
          panel1.add(PDIN2);
          PDIN2.setBounds(23, 155, 430, 20);

          //---- PDIN4 ----
          PDIN4.setText("Faire intervenir le code fiche dans la constitution du chemin");
          PDIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PDIN4.setName("PDIN4");
          panel1.add(PDIN4);
          PDIN4.setBounds(23, 180, 430, 20);

          //---- PDIN3 ----
          PDIN3.setText("Faire intervenir le code document dans la constitution du chemin");
          PDIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PDIN3.setName("PDIN3");
          panel1.add(PDIN3);
          PDIN3.setBounds(23, 205, 430, 20);

          //---- PDLIB ----
          PDLIB.setComponentPopupMenu(BTD);
          PDLIB.setName("PDLIB");
          panel1.add(PDLIB);
          PDLIB.setBounds(130, 65, 310, PDLIB.getPreferredSize().height);

          //---- PDCHM0 ----
          PDCHM0.setComponentPopupMenu(BTD);
          PDCHM0.setName("PDCHM0");
          panel1.add(PDCHM0);
          PDCHM0.setBounds(130, 95, 310, PDCHM0.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Code document");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(25, 39, 108, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("Type");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(190, 39, 45, 20);

          //---- OBJ_43 ----
          OBJ_43.setText("Racine");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(25, 99, 64, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("Libell\u00e9");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(25, 69, 61, 20);

          //---- PDDOC ----
          PDDOC.setText("@PDDOC@");
          PDDOC.setName("PDDOC");
          panel1.add(PDDOC);
          PDDOC.setBounds(130, 37, 42, PDDOC.getPreferredSize().height);

          //---- PDFIC ----
          PDFIC.setModel(new DefaultComboBoxModel(new String[] {
            "Choisir un type de fiche",
            "Documents divers de gestion des ventes",
            "Article",
            "Lot d'articles",
            "Client",
            "Repr\u00e9sentant",
            "Affaires",
            "Bon de vente",
            "Devis",
            "Documents divers de gestion des achats",
            "Fournisseur",
            "Actions commerciales",
            "Prospection",
            "Documents divers service apr\u00e9s vente",
            "Service apr\u00e8s vente",
            "Intervention",
            "Documents divers de transport",
            "Documents divers de gestion de production",
            "Op\u00e9rations de production",
            "Documents divers de comptabilit\u00e9",
            "Plan comptable auxiliaire",
            "Plan comptable g\u00e9n\u00e9ral",
            "Ecriture comptable",
            "Immobilisation",
            "Documents divers de tableau de bord comptable",
            "Documents divers de TDS",
            "Documents divers WEB",
            "Documents divers d'exploitation",
            "Documents divers sp\u00e9cifiques"
          }));
          PDFIC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PDFIC.setName("PDFIC");
          panel1.add(PDFIC);
          PDFIC.setBounds(240, 36, 280, PDFIC.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Constitution du chemin d'acc\u00e8s"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_50 ----
          OBJ_50.setText("Premier niveau de dossier");
          OBJ_50.setName("OBJ_50");
          panel2.add(OBJ_50);
          OBJ_50.setBounds(25, 38, 217, 20);

          //---- OBJ_51 ----
          OBJ_51.setText("Deuxi\u00e8me niveau de dossier");
          OBJ_51.setName("OBJ_51");
          panel2.add(OBJ_51);
          OBJ_51.setBounds(25, 65, 217, 20);

          //---- OBJ_57 ----
          OBJ_57.setText("Troisi\u00e8me niveau de dossier");
          OBJ_57.setName("OBJ_57");
          panel2.add(OBJ_57);
          OBJ_57.setBounds(25, 95, 217, 20);

          //---- OBJ_63 ----
          OBJ_63.setText("Quatri\u00e8me niveau de dossier");
          OBJ_63.setName("OBJ_63");
          panel2.add(OBJ_63);
          OBJ_63.setBounds(25, 125, 217, 20);

          //---- OBJ_69 ----
          OBJ_69.setText("Cinqui\u00e8me niveau de dossier");
          OBJ_69.setName("OBJ_69");
          panel2.add(OBJ_69);
          OBJ_69.setBounds(25, 155, 217, 20);

          //---- LCHM1 ----
          LCHM1.setName("LCHM1");
          panel2.add(LCHM1);
          LCHM1.setBounds(310, 34, 133, LCHM1.getPreferredSize().height);

          //---- LCHM5 ----
          LCHM5.setName("LCHM5");
          panel2.add(LCHM5);
          LCHM5.setBounds(310, 155, 133, LCHM5.getPreferredSize().height);

          //---- LCHM4 ----
          LCHM4.setName("LCHM4");
          panel2.add(LCHM4);
          LCHM4.setBounds(310, 125, 133, LCHM4.getPreferredSize().height);

          //---- LCHM3 ----
          LCHM3.setName("LCHM3");
          panel2.add(LCHM3);
          LCHM3.setBounds(310, 95, 133, LCHM3.getPreferredSize().height);

          //---- LCHM2 ----
          LCHM2.setName("LCHM2");
          panel2.add(LCHM2);
          LCHM2.setBounds(310, 65, 133, LCHM2.getPreferredSize().height);

          //---- PDCHM1 ----
          PDCHM1.setComponentPopupMenu(BTD);
          PDCHM1.setName("PDCHM1");
          panel2.add(PDCHM1);
          PDCHM1.setBounds(260, 34, 40, PDCHM1.getPreferredSize().height);

          //---- PDCHM2 ----
          PDCHM2.setComponentPopupMenu(BTD);
          PDCHM2.setName("PDCHM2");
          panel2.add(PDCHM2);
          PDCHM2.setBounds(260, 65, 40, PDCHM2.getPreferredSize().height);

          //---- PDCHM3 ----
          PDCHM3.setComponentPopupMenu(BTD);
          PDCHM3.setName("PDCHM3");
          panel2.add(PDCHM3);
          PDCHM3.setBounds(260, 95, 40, PDCHM3.getPreferredSize().height);

          //---- PDCHM4 ----
          PDCHM4.setComponentPopupMenu(BTD);
          PDCHM4.setName("PDCHM4");
          panel2.add(PDCHM4);
          PDCHM4.setBounds(260, 125, 40, PDCHM4.getPreferredSize().height);

          //---- PDCHM5 ----
          PDCHM5.setComponentPopupMenu(BTD);
          PDCHM5.setName("PDCHM5");
          panel2.add(PDCHM5);
          PDCHM5.setBounds(260, 155, 40, PDCHM5.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox PDIN1;
  private XRiCheckBox PDIN2;
  private XRiCheckBox PDIN4;
  private XRiCheckBox PDIN3;
  private XRiTextField PDLIB;
  private XRiTextField PDCHM0;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JLabel OBJ_43;
  private JLabel OBJ_41;
  private RiZoneSortie PDDOC;
  private XRiComboBox PDFIC;
  private JPanel panel2;
  private JLabel OBJ_50;
  private JLabel OBJ_51;
  private JLabel OBJ_57;
  private JLabel OBJ_63;
  private JLabel OBJ_69;
  private XRiTextField LCHM1;
  private XRiTextField LCHM5;
  private XRiTextField LCHM4;
  private XRiTextField LCHM3;
  private XRiTextField LCHM2;
  private XRiTextField PDCHM1;
  private XRiTextField PDCHM2;
  private XRiTextField PDCHM3;
  private XRiTextField PDCHM4;
  private XRiTextField PDCHM5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
