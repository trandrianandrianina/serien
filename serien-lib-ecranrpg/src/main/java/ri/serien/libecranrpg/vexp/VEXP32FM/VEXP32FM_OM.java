
package ri.serien.libecranrpg.vexp.VEXP32FM;
// Nom Fichier: pop_VEXP32FM_FMTOBM_244.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;

/**
 * @author Stéphane Vénéri
 */
public class VEXP32FM_OM extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] listeDonnees = { "OB1", "OB2", "OB3", "OB4", "OB5", "OB6", "OB7", "OB8", "OB9", "OB10", "OB11", "OB12", "OB13", "OB14",
      "OB15", "OB16", "OB17", "OB18", "OB19", "OB20", "OB21" };
  
  public VEXP32FM_OM(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_26);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    
    riTextArea1.setData(listeDonnees, lexique, true, true);
    riTextArea1.setEditable(!lexique.isTrue("20"));
    
    // TODO Icones
    OBJ_27.setIcon(lexique.chargerImage("images/retour.png", true));
    OBJ_26.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SERIE M - INTERVENTIONS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    riTextArea1.getData(60, lexique);
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    riTextArea1 = new RiTextArea();
    panel1 = new JPanel();
    OBJ_26 = new JButton();
    OBJ_27 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Texte d'intervention"));
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- riTextArea1 ----
      riTextArea1.setName("riTextArea1");
      panel2.add(riTextArea1);
      riTextArea1.setBounds(30, 45, 460, 355);
    }
    add(panel2);
    panel2.setBounds(10, 10, 520, 430);

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_26 ----
      OBJ_26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      panel1.add(OBJ_26);
      OBJ_26.setBounds(5, 5, 56, 40);

      //---- OBJ_27 ----
      OBJ_27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_27.setToolTipText("Retour");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      panel1.add(OBJ_27);
      OBJ_27.setBounds(65, 5, 56, 40);
    }
    add(panel1);
    panel1.setBounds(395, 440, 130, 50);

    setPreferredSize(new Dimension(540, 495));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel2;
  private RiTextArea riTextArea1;
  private JPanel panel1;
  private JButton OBJ_26;
  private JButton OBJ_27;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
