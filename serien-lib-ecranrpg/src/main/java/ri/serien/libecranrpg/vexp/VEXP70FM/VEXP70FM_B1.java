
package ri.serien.libecranrpg.vexp.VEXP70FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VEXP70FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP70FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    T01.setValeursSelection("X", "");
    T02.setValeursSelection("X", "");
    T03.setValeursSelection("X", "");
    T04.setValeursSelection("X", "");
    T05.setValeursSelection("X", "");
    T06.setValeursSelection("X", "");
    T07.setValeursSelection("X", "");
    T08.setValeursSelection("X", "");
    T09.setValeursSelection("X", "");
    T10.setValeursSelection("X", "");
    T11.setValeursSelection("X", "");
    T12.setValeursSelection("X", "");
    T13.setValeursSelection("X", "");
    T14.setValeursSelection("X", "");
    T15.setValeursSelection("X", "");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    B01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B01@")).trim());
    L01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L01@")).trim());
    B02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B02@")).trim());
    L02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L02@")).trim());
    B03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B03@")).trim());
    L03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L03@")).trim());
    B04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B04@")).trim());
    L04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L04@")).trim());
    B05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B05@")).trim());
    L05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L05@")).trim());
    B06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B06@")).trim());
    L06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L06@")).trim());
    B07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B07@")).trim());
    L07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L07@")).trim());
    B08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B08@")).trim());
    L08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L08@")).trim());
    B09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B09@")).trim());
    L09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L09@")).trim());
    B10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B10@")).trim());
    L10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L10@")).trim());
    B11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B11@")).trim());
    L11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L11@")).trim());
    B12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B12@")).trim());
    L12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L12@")).trim());
    B13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B13@")).trim());
    L13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L13@")).trim());
    B14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B14@")).trim());
    L14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L14@")).trim());
    B15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B15@")).trim());
    L15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(T01, T01.get_LIST_Title_Data_Brut(), _T01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Sauvegarde générale"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    B01 = new RiZoneSortie();
    L01 = new RiZoneSortie();
    T01 = new XRiCheckBox();
    B02 = new RiZoneSortie();
    L02 = new RiZoneSortie();
    T02 = new XRiCheckBox();
    B03 = new RiZoneSortie();
    L03 = new RiZoneSortie();
    T03 = new XRiCheckBox();
    B04 = new RiZoneSortie();
    L04 = new RiZoneSortie();
    T04 = new XRiCheckBox();
    B05 = new RiZoneSortie();
    L05 = new RiZoneSortie();
    T05 = new XRiCheckBox();
    B06 = new RiZoneSortie();
    L06 = new RiZoneSortie();
    T06 = new XRiCheckBox();
    B07 = new RiZoneSortie();
    L07 = new RiZoneSortie();
    T07 = new XRiCheckBox();
    B08 = new RiZoneSortie();
    L08 = new RiZoneSortie();
    T08 = new XRiCheckBox();
    B09 = new RiZoneSortie();
    L09 = new RiZoneSortie();
    T09 = new XRiCheckBox();
    B10 = new RiZoneSortie();
    L10 = new RiZoneSortie();
    T10 = new XRiCheckBox();
    B11 = new RiZoneSortie();
    L11 = new RiZoneSortie();
    T11 = new XRiCheckBox();
    B12 = new RiZoneSortie();
    L12 = new RiZoneSortie();
    T12 = new XRiCheckBox();
    B13 = new RiZoneSortie();
    L13 = new RiZoneSortie();
    T13 = new XRiCheckBox();
    B14 = new RiZoneSortie();
    L14 = new RiZoneSortie();
    T14 = new XRiCheckBox();
    B15 = new RiZoneSortie();
    L15 = new RiZoneSortie();
    T15 = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(925, 470));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setBorder(new TitledBorder("Choix des biblioth\u00e8ques"));
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(690, 45, 25, 165);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(690, 254, 25, 165);

          //---- B01 ----
          B01.setText("@B01@");
          B01.setName("B01");
          p_recup.add(B01);
          B01.setBounds(45, 45, 114, B01.getPreferredSize().height);

          //---- L01 ----
          L01.setText("@L01@");
          L01.setName("L01");
          p_recup.add(L01);
          L01.setBounds(165, 45, 514, L01.getPreferredSize().height);

          //---- T01 ----
          T01.setName("T01");
          p_recup.add(T01);
          T01.setBounds(new Rectangle(new Point(15, 48), T01.getPreferredSize()));

          //---- B02 ----
          B02.setText("@B02@");
          B02.setName("B02");
          p_recup.add(B02);
          B02.setBounds(45, 70, 114, B02.getPreferredSize().height);

          //---- L02 ----
          L02.setText("@L02@");
          L02.setName("L02");
          p_recup.add(L02);
          L02.setBounds(165, 70, 514, L02.getPreferredSize().height);

          //---- T02 ----
          T02.setName("T02");
          p_recup.add(T02);
          T02.setBounds(new Rectangle(new Point(15, 73), T02.getPreferredSize()));

          //---- B03 ----
          B03.setText("@B03@");
          B03.setName("B03");
          p_recup.add(B03);
          B03.setBounds(45, 95, 114, B03.getPreferredSize().height);

          //---- L03 ----
          L03.setText("@L03@");
          L03.setName("L03");
          p_recup.add(L03);
          L03.setBounds(165, 95, 514, L03.getPreferredSize().height);

          //---- T03 ----
          T03.setName("T03");
          p_recup.add(T03);
          T03.setBounds(new Rectangle(new Point(15, 98), T03.getPreferredSize()));

          //---- B04 ----
          B04.setText("@B04@");
          B04.setName("B04");
          p_recup.add(B04);
          B04.setBounds(45, 120, 114, B04.getPreferredSize().height);

          //---- L04 ----
          L04.setText("@L04@");
          L04.setName("L04");
          p_recup.add(L04);
          L04.setBounds(165, 120, 514, L04.getPreferredSize().height);

          //---- T04 ----
          T04.setName("T04");
          p_recup.add(T04);
          T04.setBounds(new Rectangle(new Point(15, 123), T04.getPreferredSize()));

          //---- B05 ----
          B05.setText("@B05@");
          B05.setName("B05");
          p_recup.add(B05);
          B05.setBounds(45, 145, 114, B05.getPreferredSize().height);

          //---- L05 ----
          L05.setText("@L05@");
          L05.setName("L05");
          p_recup.add(L05);
          L05.setBounds(165, 145, 514, L05.getPreferredSize().height);

          //---- T05 ----
          T05.setName("T05");
          p_recup.add(T05);
          T05.setBounds(new Rectangle(new Point(15, 148), T05.getPreferredSize()));

          //---- B06 ----
          B06.setText("@B06@");
          B06.setName("B06");
          p_recup.add(B06);
          B06.setBounds(45, 170, 114, B06.getPreferredSize().height);

          //---- L06 ----
          L06.setText("@L06@");
          L06.setName("L06");
          p_recup.add(L06);
          L06.setBounds(165, 170, 514, L06.getPreferredSize().height);

          //---- T06 ----
          T06.setName("T06");
          p_recup.add(T06);
          T06.setBounds(new Rectangle(new Point(15, 173), T06.getPreferredSize()));

          //---- B07 ----
          B07.setText("@B07@");
          B07.setName("B07");
          p_recup.add(B07);
          B07.setBounds(45, 195, 114, B07.getPreferredSize().height);

          //---- L07 ----
          L07.setText("@L07@");
          L07.setName("L07");
          p_recup.add(L07);
          L07.setBounds(165, 195, 514, L07.getPreferredSize().height);

          //---- T07 ----
          T07.setName("T07");
          p_recup.add(T07);
          T07.setBounds(new Rectangle(new Point(15, 198), T07.getPreferredSize()));

          //---- B08 ----
          B08.setText("@B08@");
          B08.setName("B08");
          p_recup.add(B08);
          B08.setBounds(45, 220, 114, B08.getPreferredSize().height);

          //---- L08 ----
          L08.setText("@L08@");
          L08.setName("L08");
          p_recup.add(L08);
          L08.setBounds(165, 220, 514, L08.getPreferredSize().height);

          //---- T08 ----
          T08.setName("T08");
          p_recup.add(T08);
          T08.setBounds(new Rectangle(new Point(15, 223), T08.getPreferredSize()));

          //---- B09 ----
          B09.setText("@B09@");
          B09.setName("B09");
          p_recup.add(B09);
          B09.setBounds(45, 245, 114, B09.getPreferredSize().height);

          //---- L09 ----
          L09.setText("@L09@");
          L09.setName("L09");
          p_recup.add(L09);
          L09.setBounds(165, 245, 514, L09.getPreferredSize().height);

          //---- T09 ----
          T09.setName("T09");
          p_recup.add(T09);
          T09.setBounds(new Rectangle(new Point(15, 248), T09.getPreferredSize()));

          //---- B10 ----
          B10.setText("@B10@");
          B10.setName("B10");
          p_recup.add(B10);
          B10.setBounds(45, 270, 114, B10.getPreferredSize().height);

          //---- L10 ----
          L10.setText("@L10@");
          L10.setName("L10");
          p_recup.add(L10);
          L10.setBounds(165, 270, 514, L10.getPreferredSize().height);

          //---- T10 ----
          T10.setName("T10");
          p_recup.add(T10);
          T10.setBounds(new Rectangle(new Point(15, 273), T10.getPreferredSize()));

          //---- B11 ----
          B11.setText("@B11@");
          B11.setName("B11");
          p_recup.add(B11);
          B11.setBounds(45, 295, 114, B11.getPreferredSize().height);

          //---- L11 ----
          L11.setText("@L11@");
          L11.setName("L11");
          p_recup.add(L11);
          L11.setBounds(165, 295, 514, L11.getPreferredSize().height);

          //---- T11 ----
          T11.setName("T11");
          p_recup.add(T11);
          T11.setBounds(new Rectangle(new Point(15, 298), T11.getPreferredSize()));

          //---- B12 ----
          B12.setText("@B12@");
          B12.setName("B12");
          p_recup.add(B12);
          B12.setBounds(45, 320, 114, B12.getPreferredSize().height);

          //---- L12 ----
          L12.setText("@L12@");
          L12.setName("L12");
          p_recup.add(L12);
          L12.setBounds(165, 320, 514, L12.getPreferredSize().height);

          //---- T12 ----
          T12.setName("T12");
          p_recup.add(T12);
          T12.setBounds(new Rectangle(new Point(15, 323), T12.getPreferredSize()));

          //---- B13 ----
          B13.setText("@B13@");
          B13.setName("B13");
          p_recup.add(B13);
          B13.setBounds(45, 345, 114, B13.getPreferredSize().height);

          //---- L13 ----
          L13.setText("@L13@");
          L13.setName("L13");
          p_recup.add(L13);
          L13.setBounds(165, 345, 514, L13.getPreferredSize().height);

          //---- T13 ----
          T13.setName("T13");
          p_recup.add(T13);
          T13.setBounds(new Rectangle(new Point(15, 348), T13.getPreferredSize()));

          //---- B14 ----
          B14.setText("@B14@");
          B14.setName("B14");
          p_recup.add(B14);
          B14.setBounds(45, 370, 114, B14.getPreferredSize().height);

          //---- L14 ----
          L14.setText("@L14@");
          L14.setName("L14");
          p_recup.add(L14);
          L14.setBounds(165, 370, 514, L14.getPreferredSize().height);

          //---- T14 ----
          T14.setName("T14");
          p_recup.add(T14);
          T14.setBounds(new Rectangle(new Point(15, 373), T14.getPreferredSize()));

          //---- B15 ----
          B15.setText("@B15@");
          B15.setName("B15");
          p_recup.add(B15);
          B15.setBounds(45, 395, 114, B15.getPreferredSize().height);

          //---- L15 ----
          L15.setText("@L15@");
          L15.setName("L15");
          p_recup.add(L15);
          L15.setBounds(165, 395, 514, L15.getPreferredSize().height);

          //---- T15 ----
          T15.setName("T15");
          p_recup.add(T15);
          T15.setBounds(new Rectangle(new Point(15, 398), T15.getPreferredSize()));
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 447, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private RiZoneSortie B01;
  private RiZoneSortie L01;
  private XRiCheckBox T01;
  private RiZoneSortie B02;
  private RiZoneSortie L02;
  private XRiCheckBox T02;
  private RiZoneSortie B03;
  private RiZoneSortie L03;
  private XRiCheckBox T03;
  private RiZoneSortie B04;
  private RiZoneSortie L04;
  private XRiCheckBox T04;
  private RiZoneSortie B05;
  private RiZoneSortie L05;
  private XRiCheckBox T05;
  private RiZoneSortie B06;
  private RiZoneSortie L06;
  private XRiCheckBox T06;
  private RiZoneSortie B07;
  private RiZoneSortie L07;
  private XRiCheckBox T07;
  private RiZoneSortie B08;
  private RiZoneSortie L08;
  private XRiCheckBox T08;
  private RiZoneSortie B09;
  private RiZoneSortie L09;
  private XRiCheckBox T09;
  private RiZoneSortie B10;
  private RiZoneSortie L10;
  private XRiCheckBox T10;
  private RiZoneSortie B11;
  private RiZoneSortie L11;
  private XRiCheckBox T11;
  private RiZoneSortie B12;
  private RiZoneSortie L12;
  private XRiCheckBox T12;
  private RiZoneSortie B13;
  private RiZoneSortie L13;
  private XRiCheckBox T13;
  private RiZoneSortie B14;
  private RiZoneSortie L14;
  private XRiCheckBox T14;
  private RiZoneSortie B15;
  private RiZoneSortie L15;
  private XRiCheckBox T15;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
