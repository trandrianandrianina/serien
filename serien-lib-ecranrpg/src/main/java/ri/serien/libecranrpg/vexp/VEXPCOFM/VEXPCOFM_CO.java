//$$david$$ ££03/01/11££ -> modifs et tests

package ri.serien.libecranrpg.vexp.VEXPCOFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VEXPCOFM_CO extends SNPanelEcranRPG implements ioFrame {
  
   
  private int hauteur = 395;
  
  public VEXPCOFM_CO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO1@")).trim());
    OBJ_01.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO1@")).trim());
    OBJ_02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO2@")).trim());
    OBJ_03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO3@")).trim());
    OBJ_04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO4@")).trim());
    OBJ_05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO5@")).trim());
    OBJ_06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO6@")).trim());
    OBJ_07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO7@")).trim());
    OBJ_08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO8@")).trim());
    OBJ_09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO9@")).trim());
    OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO10@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBOT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // FLD24079.setVisible( lexique.isPresent("FLD24079"));
    OBJ_01.setVisible(!interpreteurD.analyseExpression("@TBO1@").trim().equalsIgnoreCase("") || lexique.isTrue("N21"));
    OBJ_02.setVisible(!interpreteurD.analyseExpression("@TBO2@").trim().equalsIgnoreCase("") || lexique.isTrue("N22"));
    OBJ_03.setVisible(!interpreteurD.analyseExpression("@TBO3@").trim().equalsIgnoreCase("") || lexique.isTrue("N23"));
    OBJ_04.setVisible(!interpreteurD.analyseExpression("@TBO4@").trim().equalsIgnoreCase("") || lexique.isTrue("N24"));
    OBJ_05.setVisible(!interpreteurD.analyseExpression("@TBO5@").trim().equalsIgnoreCase("") || lexique.isTrue("N25"));
    OBJ_06.setVisible(!interpreteurD.analyseExpression("@TBO6@").trim().equalsIgnoreCase("") || lexique.isTrue("N26"));
    OBJ_07.setVisible(!interpreteurD.analyseExpression("@TBO7@").trim().equalsIgnoreCase("") || lexique.isTrue("N27"));
    OBJ_08.setVisible(!interpreteurD.analyseExpression("@TBO8@").trim().equalsIgnoreCase("") || lexique.isTrue("N28"));
    OBJ_09.setVisible(!interpreteurD.analyseExpression("@TBO9@").trim().equalsIgnoreCase("") || lexique.isTrue("N29"));
    OBJ_10.setVisible(!interpreteurD.analyseExpression("@TBO10@").trim().equalsIgnoreCase("") || lexique.isTrue("N30"));
    
    BT_PGUP.setVisible(OBJ_10.isVisible());
    BT_PGDOWN.setVisible(OBJ_10.isVisible());
    
    JButton[] boutons = { OBJ_01, OBJ_02, OBJ_03, OBJ_04, OBJ_05, OBJ_06, OBJ_07, OBJ_08, OBJ_09, OBJ_10 };
    
    hauteur = 395;
    
    for (int i = 0; i < boutons.length; i++) {
      if (!boutons[i].isVisible()) {
        hauteur -= 30;
      }
    }
    
    this.setPreferredSize(new Dimension(325, hauteur));
    
    // TODO Icones
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    
    setTitle(interpreteurD.analyseExpression("Options"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void OBJ_01ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N1").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_02ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N2").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_03ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N3").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_04ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N4").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_05ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N5").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_06ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N6").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_07ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N7").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_08ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N8").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_09ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N9").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("NA").trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    OBJ_01 = new JButton();
    OBJ_02 = new JButton();
    OBJ_03 = new JButton();
    OBJ_04 = new JButton();
    OBJ_05 = new JButton();
    OBJ_06 = new JButton();
    OBJ_07 = new JButton();
    OBJ_08 = new JButton();
    OBJ_09 = new JButton();
    OBJ_10 = new JButton();
    label1 = new JButton();
    bouton_retour = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(325, 395));
    setPreferredSize(new Dimension(325, 395));
    setForeground(Color.black);
    setBackground(new Color(90, 90, 90));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setOpaque(false);
      p_principal.setPreferredSize(new Dimension(295, 395));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setOpaque(false);
        p_contenu.setPreferredSize(new Dimension(295, 395));
        p_contenu.setName("p_contenu");

        //---- OBJ_01 ----
        OBJ_01.setText("@TBO1@");
        OBJ_01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_01.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_01.setForeground(Color.black);
        OBJ_01.setToolTipText("@TBO1@");
        OBJ_01.setName("OBJ_01");
        OBJ_01.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_01ActionPerformed(e);
          }
        });

        //---- OBJ_02 ----
        OBJ_02.setText("@TBO2@");
        OBJ_02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_02.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_02.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_02.setForeground(Color.black);
        OBJ_02.setName("OBJ_02");
        OBJ_02.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_02ActionPerformed(e);
          }
        });

        //---- OBJ_03 ----
        OBJ_03.setText("@TBO3@");
        OBJ_03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_03.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_03.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_03.setName("OBJ_03");
        OBJ_03.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_03ActionPerformed(e);
          }
        });

        //---- OBJ_04 ----
        OBJ_04.setText("@TBO4@");
        OBJ_04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_04.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_04.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_04.setName("OBJ_04");
        OBJ_04.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_04ActionPerformed(e);
          }
        });

        //---- OBJ_05 ----
        OBJ_05.setText("@TBO5@");
        OBJ_05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_05.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_05.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_05.setName("OBJ_05");
        OBJ_05.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_05ActionPerformed(e);
          }
        });

        //---- OBJ_06 ----
        OBJ_06.setText("@TBO6@");
        OBJ_06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_06.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_06.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_06.setName("OBJ_06");
        OBJ_06.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_06ActionPerformed(e);
          }
        });

        //---- OBJ_07 ----
        OBJ_07.setText("@TBO7@");
        OBJ_07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_07.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_07.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_07.setName("OBJ_07");
        OBJ_07.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_07ActionPerformed(e);
          }
        });

        //---- OBJ_08 ----
        OBJ_08.setText("@TBO8@");
        OBJ_08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_08.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_08.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_08.setName("OBJ_08");
        OBJ_08.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_08ActionPerformed(e);
          }
        });

        //---- OBJ_09 ----
        OBJ_09.setText("@TBO9@");
        OBJ_09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_09.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_09.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_09.setName("OBJ_09");
        OBJ_09.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_09ActionPerformed(e);
          }
        });

        //---- OBJ_10 ----
        OBJ_10.setText("@TBO10@");
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });

        //---- label1 ----
        label1.setText("@TBOT@");
        label1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        label1.setBorderPainted(false);
        label1.setContentAreaFilled(false);
        label1.setHorizontalAlignment(SwingConstants.LEFT);
        label1.setFocusable(false);
        label1.setName("label1");

        //---- bouton_retour ----
        bouton_retour.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bouton_retour.setText("Retour");
        bouton_retour.setFont(bouton_retour.getFont().deriveFont(bouton_retour.getFont().getStyle() | Font.BOLD, bouton_retour.getFont().getSize() + 3f));
        bouton_retour.setForeground(Color.black);
        bouton_retour.setHorizontalAlignment(SwingConstants.LEADING);
        bouton_retour.setIconTextGap(10);
        bouton_retour.setName("bouton_retour");
        bouton_retour.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bouton_retourActionPerformed(e);
          }
        });

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        BT_PGUP.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGUPActionPerformed(e);
          }
        });

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        BT_PGDOWN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGDOWNActionPerformed(e);
          }
        });

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(OBJ_01, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_02, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_03, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_04, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_05, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(OBJ_06, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_07, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_08, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_09, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
              .addGap(15, 15, 15))
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(bouton_retour, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(OBJ_01, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_02, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_03, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_04, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_05, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(OBJ_06, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_07, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_08, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_09, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
              .addComponent(bouton_retour, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JButton OBJ_01;
  private JButton OBJ_02;
  private JButton OBJ_03;
  private JButton OBJ_04;
  private JButton OBJ_05;
  private JButton OBJ_06;
  private JButton OBJ_07;
  private JButton OBJ_08;
  private JButton OBJ_09;
  private JButton OBJ_10;
  private JButton label1;
  private JButton bouton_retour;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
