
package ri.serien.libecranrpg.vexp.VEXP72FM;
// Nom Fichier: pop_VEXP72FM_FMTB1_298.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP72FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] S3_Value = { " ", "X", "1", "2", "3", "4" };
  private String[] S3_label =
      { "pas de sauvegarde", "vers une bande", "vers un fichier", "bande + fichier", "fichier + FTP", "bande + fichier + FTP" };
  
  public VEXP72FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    S3DIM.setValeurs(S3_Value, S3_label);
    S3SAM.setValeurs(S3_Value, S3_label);
    S3VEN.setValeurs(S3_Value, S3_label);
    S3JEU.setValeurs(S3_Value, S3_label);
    S3MER.setValeurs(S3_Value, S3_label);
    S3MAR.setValeurs(S3_Value, S3_label);
    S3LUN.setValeurs(S3_Value, S3_label);
    S3CONF.setValeursSelection("X", " ");
    S3SEC.setValeursSelection("X", " ");
    S3PGM.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19", lexique.HostFieldGetData("LIBERR"));
    
    
    S3STA.setVisible(!lexique.HostFieldGetData("S3STA").trim().equalsIgnoreCase("") & lexique.isPresent("S3STA"));
    // S3DIM.setVisible( lexique.isPresent("S3DIM"));
    // S3DIM.setSelected(lexique.HostFieldGetData("S3DIM").equalsIgnoreCase("X"));
    // S3SAM.setVisible( lexique.isPresent("S3SAM"));
    // S3SAM.setSelected(lexique.HostFieldGetData("S3SAM").equalsIgnoreCase("X"));
    // S3VEN.setVisible( lexique.isPresent("S3VEN"));
    // S3VEN.setSelected(lexique.HostFieldGetData("S3VEN").equalsIgnoreCase("X"));
    // S3JEU.setVisible( lexique.isPresent("S3JEU"));
    // S3JEU.setSelected(lexique.HostFieldGetData("S3JEU").equalsIgnoreCase("X"));
    // S3MER.setVisible( lexique.isPresent("S3MER"));
    // S3MER.setSelected(lexique.HostFieldGetData("S3MER").equalsIgnoreCase("X"));
    // S3MAR.setVisible( lexique.isPresent("S3MAR"));
    // S3MAR.setSelected(lexique.HostFieldGetData("S3MAR").equalsIgnoreCase("X"));
    // S3LUN.setVisible( lexique.isPresent("S3LUN"));
    // S3LUN.setSelected(lexique.HostFieldGetData("S3LUN").equalsIgnoreCase("X"));
    // S3CONF.setVisible( lexique.isPresent("S3CONF"));
    // S3CONF.setSelected(lexique.HostFieldGetData("S3CONF").equalsIgnoreCase("X"));
    // S3SEC.setVisible( lexique.isPresent("S3SEC"));
    // S3SEC.setSelected(lexique.HostFieldGetData("S3SEC").equalsIgnoreCase("X"));
    // S3PGM.setVisible( lexique.isPresent("S3PGM"));
    // S3PGM.setSelected(lexique.HostFieldGetData("S3PGM").equalsIgnoreCase("X"));
    S3HDIM.setVisible(lexique.isPresent("S3HDIM"));
    S3HSAM.setVisible(lexique.isPresent("S3HSAM"));
    S3HVEN.setVisible(lexique.isPresent("S3HVEN"));
    S3HJEU.setVisible(lexique.isPresent("S3HJEU"));
    S3HMER.setVisible(lexique.isPresent("S3HMER"));
    S3HMAR.setVisible(lexique.isPresent("S3HMAR"));
    S3HLUN.setVisible(lexique.isPresent("S3HLUN"));
    S3DEV.setVisible(lexique.isPresent("S3DEV"));
    S3ADM.setVisible(lexique.isPresent("S3ADM"));
    OBJ_70.setVisible(!lexique.HostFieldGetData("S3STA").trim().equalsIgnoreCase(""));
    OBJ_20.setVisible(!lexique.HostFieldGetData("S3STA").trim().equalsIgnoreCase(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Sauvegarde générale"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (S3DIM.isSelected())
    // lexique.HostFieldPutData("S3DIM", 0, "X");
    // else
    // lexique.HostFieldPutData("S3DIM", 0, " ");
    // if (S3SAM.isSelected())
    // lexique.HostFieldPutData("S3SAM", 0, "X");
    // else
    // lexique.HostFieldPutData("S3SAM", 0, " ");
    // if (S3VEN.isSelected())
    // lexique.HostFieldPutData("S3VEN", 0, "X");
    // else
    // lexique.HostFieldPutData("S3VEN", 0, " ");
    // if (S3JEU.isSelected())
    // lexique.HostFieldPutData("S3JEU", 0, "X");
    // else
    // lexique.HostFieldPutData("S3JEU", 0, " ");
    // if (S3MER.isSelected())
    // lexique.HostFieldPutData("S3MER", 0, "X");
    // else
    // lexique.HostFieldPutData("S3MER", 0, " ");
    // if (S3MAR.isSelected())
    // lexique.HostFieldPutData("S3MAR", 0, "X");
    // else
    // lexique.HostFieldPutData("S3MAR", 0, " ");
    // if (S3LUN.isSelected())
    // lexique.HostFieldPutData("S3LUN", 0, "X");
    // else
    // lexique.HostFieldPutData("S3LUN", 0, " ");
    // if (S3CONF.isSelected())
    // lexique.HostFieldPutData("S3CONF", 0, "X");
    // else
    // lexique.HostFieldPutData("S3CONF", 0, " ");
    // if (S3SEC.isSelected())
    // lexique.HostFieldPutData("S3SEC", 0, "X");
    // else
    // lexique.HostFieldPutData("S3SEC", 0, " ");
    // if (S3PGM.isSelected())
    // lexique.HostFieldPutData("S3PGM", 0, "X");
    // else
    // lexique.HostFieldPutData("S3PGM", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_24 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_29 = new JLabel();
    P_PnlOpts = new JPanel();
    S3ADM = new XRiTextField();
    S3DEV = new XRiTextField();
    OBJ_40 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_27 = new JLabel();
    S3HLUN = new XRiTextField();
    S3HMAR = new XRiTextField();
    S3HMER = new XRiTextField();
    S3HJEU = new XRiTextField();
    S3HVEN = new XRiTextField();
    S3HSAM = new XRiTextField();
    S3HDIM = new XRiTextField();
    S3PGM = new XRiCheckBox();
    S3SEC = new XRiCheckBox();
    S3CONF = new XRiCheckBox();
    S3STA = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_41 = new JLabel();
    S3LUN = new XRiComboBox();
    S3MAR = new XRiComboBox();
    S3MER = new XRiComboBox();
    S3JEU = new XRiComboBox();
    S3VEN = new XRiComboBox();
    S3SAM = new XRiComboBox();
    S3DIM = new XRiComboBox();
    separator1 = compFactory.createSeparator("P\u00e9riodicit\u00e9");

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(705, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Choix des biblioth\u00e8ques");
            riSousMenu_bt6.setToolTipText("Choix des biblioth\u00e8ques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Choix des r\u00e9pertoires");
            riSousMenu_bt7.setToolTipText("Choix des r\u00e9pertoires");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Arr\u00eat des sous-syst\u00e8mes");
            riSousMenu_bt8.setToolTipText("Arr\u00eat des sous-syst\u00e8mes");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Mise au planning");
            riSousMenu_bt9.setToolTipText("Mise au planning");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Param\u00e8tres g\u00e9n\u00e9raux"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_24 ----
          OBJ_24.setText("Sauvegarde donn\u00e9es de s\u00e9curit\u00e9");
          OBJ_24.setName("OBJ_24");
          p_recup.add(OBJ_24);
          OBJ_24.setBounds(26, 170, 217, 20);

          //---- OBJ_21 ----
          OBJ_21.setText("Profil administrateur sauvegarde");
          OBJ_21.setName("OBJ_21");
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(26, 79, 217, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("Sauvegarde du logiciel S\u00e9rie N");
          OBJ_23.setName("OBJ_23");
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(26, 140, 217, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("Unit\u00e9 de sauvegarde (bande)");
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(26, 109, 217, 20);

          //---- OBJ_20 ----
          OBJ_20.setText("Statut derni\u00e8re sauvegarde");
          OBJ_20.setName("OBJ_20");
          p_recup.add(OBJ_20);
          OBJ_20.setBounds(26, 49, 217, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Sauvegarde de la configuration");
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(26, 200, 217, 20);

          //---- OBJ_70 ----
          OBJ_70.setText("En erreur");
          OBJ_70.setName("OBJ_70");
          p_recup.add(OBJ_70);
          OBJ_70.setBounds(308, 48, 108, 23);

          //---- OBJ_29 ----
          OBJ_29.setText("(Heure minute)");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(400, 258, 90, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- S3ADM ----
          S3ADM.setName("S3ADM");
          p_recup.add(S3ADM);
          S3ADM.setBounds(280, 75, 110, S3ADM.getPreferredSize().height);

          //---- S3DEV ----
          S3DEV.setName("S3DEV");
          p_recup.add(S3DEV);
          S3DEV.setBounds(280, 105, 110, S3DEV.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setText("Dimanche");
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(35, 443, 79, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Vendredi");
          OBJ_36.setName("OBJ_36");
          p_recup.add(OBJ_36);
          OBJ_36.setBounds(35, 381, 79, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("Mercredi");
          OBJ_32.setName("OBJ_32");
          p_recup.add(OBJ_32);
          OBJ_32.setBounds(35, 320, 79, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("Samedi");
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(35, 412, 79, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("Mardi");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(35, 289, 79, 20);

          //---- OBJ_34 ----
          OBJ_34.setText("Jeudi");
          OBJ_34.setName("OBJ_34");
          p_recup.add(OBJ_34);
          OBJ_34.setBounds(35, 351, 79, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("Lundi");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(35, 258, 79, 20);

          //---- S3HLUN ----
          S3HLUN.setName("S3HLUN");
          p_recup.add(S3HLUN);
          S3HLUN.setBounds(350, 254, 42, S3HLUN.getPreferredSize().height);

          //---- S3HMAR ----
          S3HMAR.setName("S3HMAR");
          p_recup.add(S3HMAR);
          S3HMAR.setBounds(350, 285, 42, S3HMAR.getPreferredSize().height);

          //---- S3HMER ----
          S3HMER.setName("S3HMER");
          p_recup.add(S3HMER);
          S3HMER.setBounds(350, 316, 42, S3HMER.getPreferredSize().height);

          //---- S3HJEU ----
          S3HJEU.setName("S3HJEU");
          p_recup.add(S3HJEU);
          S3HJEU.setBounds(350, 347, 42, S3HJEU.getPreferredSize().height);

          //---- S3HVEN ----
          S3HVEN.setName("S3HVEN");
          p_recup.add(S3HVEN);
          S3HVEN.setBounds(350, 377, 42, S3HVEN.getPreferredSize().height);

          //---- S3HSAM ----
          S3HSAM.setName("S3HSAM");
          p_recup.add(S3HSAM);
          S3HSAM.setBounds(350, 408, 42, S3HSAM.getPreferredSize().height);

          //---- S3HDIM ----
          S3HDIM.setName("S3HDIM");
          p_recup.add(S3HDIM);
          S3HDIM.setBounds(350, 439, 42, S3HDIM.getPreferredSize().height);

          //---- S3PGM ----
          S3PGM.setText("");
          S3PGM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          S3PGM.setName("S3PGM");
          p_recup.add(S3PGM);
          S3PGM.setBounds(280, 140, 19, 20);

          //---- S3SEC ----
          S3SEC.setText("");
          S3SEC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          S3SEC.setName("S3SEC");
          p_recup.add(S3SEC);
          S3SEC.setBounds(280, 170, 19, 20);

          //---- S3CONF ----
          S3CONF.setText("");
          S3CONF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          S3CONF.setName("S3CONF");
          p_recup.add(S3CONF);
          S3CONF.setBounds(280, 200, 19, 20);

          //---- S3STA ----
          S3STA.setName("S3STA");
          p_recup.add(S3STA);
          S3STA.setBounds(280, 45, 20, S3STA.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("\u00e0");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(330, 258, 18, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("\u00e0");
          OBJ_31.setName("OBJ_31");
          p_recup.add(OBJ_31);
          OBJ_31.setBounds(330, 289, 11, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("\u00e0");
          OBJ_33.setName("OBJ_33");
          p_recup.add(OBJ_33);
          OBJ_33.setBounds(330, 320, 11, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("\u00e0");
          OBJ_35.setName("OBJ_35");
          p_recup.add(OBJ_35);
          OBJ_35.setBounds(330, 351, 11, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("\u00e0");
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(330, 381, 11, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("\u00e0");
          OBJ_39.setName("OBJ_39");
          p_recup.add(OBJ_39);
          OBJ_39.setBounds(330, 412, 11, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("\u00e0");
          OBJ_41.setName("OBJ_41");
          p_recup.add(OBJ_41);
          OBJ_41.setBounds(330, 443, 11, 20);

          //---- S3LUN ----
          S3LUN.setName("S3LUN");
          p_recup.add(S3LUN);
          S3LUN.setBounds(114, 255, 185, S3LUN.getPreferredSize().height);

          //---- S3MAR ----
          S3MAR.setName("S3MAR");
          p_recup.add(S3MAR);
          S3MAR.setBounds(114, 286, 185, S3MAR.getPreferredSize().height);

          //---- S3MER ----
          S3MER.setName("S3MER");
          p_recup.add(S3MER);
          S3MER.setBounds(114, 317, 185, S3MER.getPreferredSize().height);

          //---- S3JEU ----
          S3JEU.setName("S3JEU");
          p_recup.add(S3JEU);
          S3JEU.setBounds(114, 348, 185, S3JEU.getPreferredSize().height);

          //---- S3VEN ----
          S3VEN.setName("S3VEN");
          p_recup.add(S3VEN);
          S3VEN.setBounds(114, 379, 185, 25);

          //---- S3SAM ----
          S3SAM.setName("S3SAM");
          p_recup.add(S3SAM);
          S3SAM.setBounds(114, 409, 185, S3SAM.getPreferredSize().height);

          //---- S3DIM ----
          S3DIM.setName("S3DIM");
          p_recup.add(S3DIM);
          S3DIM.setBounds(114, 440, 185, S3DIM.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          p_recup.add(separator1);
          separator1.setBounds(26, 230, 464, separator1.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(18, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_24;
  private JLabel OBJ_21;
  private JLabel OBJ_23;
  private JLabel OBJ_22;
  private JLabel OBJ_20;
  private JLabel OBJ_25;
  private JLabel OBJ_70;
  private JLabel OBJ_29;
  private JPanel P_PnlOpts;
  private XRiTextField S3ADM;
  private XRiTextField S3DEV;
  private JLabel OBJ_40;
  private JLabel OBJ_36;
  private JLabel OBJ_32;
  private JLabel OBJ_38;
  private JLabel OBJ_30;
  private JLabel OBJ_34;
  private JLabel OBJ_27;
  private XRiTextField S3HLUN;
  private XRiTextField S3HMAR;
  private XRiTextField S3HMER;
  private XRiTextField S3HJEU;
  private XRiTextField S3HVEN;
  private XRiTextField S3HSAM;
  private XRiTextField S3HDIM;
  private XRiCheckBox S3PGM;
  private XRiCheckBox S3SEC;
  private XRiCheckBox S3CONF;
  private XRiTextField S3STA;
  private JLabel OBJ_28;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_35;
  private JLabel OBJ_37;
  private JLabel OBJ_39;
  private JLabel OBJ_41;
  private XRiComboBox S3LUN;
  private XRiComboBox S3MAR;
  private XRiComboBox S3MER;
  private XRiComboBox S3JEU;
  private XRiComboBox S3VEN;
  private XRiComboBox S3SAM;
  private XRiComboBox S3DIM;
  private JComponent separator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
