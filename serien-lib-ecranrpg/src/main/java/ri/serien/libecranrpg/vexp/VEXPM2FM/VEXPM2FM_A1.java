
package ri.serien.libecranrpg.vexp.VEXPM2FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import ri.serien.libcommun.exploitation.mail.CritereTypeMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeTypeMail;
import ri.serien.libcommun.exploitation.mail.TypeMail;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Html;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXPM2FM_A1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] TYACTI_Value = { "", "1" };
  private TypeMail typeMail = null;
  
  /**
   * Constructeur.
   */
  public VEXPM2FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    
    TYACTI.setValeurs(TYACTI_Value, null);
    
    // Initialiser les boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  // -- Méthodes publiques

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Mettre à jour l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "TYETB");
    snEtablissement.setEnabled(false);
    
    // Charger le type de mail via un appel direct pour récupérer le corps du mail
    typeMail = chargerTypeMail();
    // Mise en forme du corps s'il s'agit de html
    String corps = Html.mettreEnForme(typeMail.getCorpsMessage());
    taMessage.setText(corps);
    boolean modeModification = isModification();
    taMessage.setEditable(modeModification);
    
    // Affichage du bouton Valider
    snBarreBouton.activerBouton(EnumBouton.VALIDER, isModification());
    
    // remplissage de la combo Reply TO
    // Documents de vente (TYID >= 5 and TYID <= 8 (deevis, commande client, bons et factures): REPLY TO : "Nom présent", "Vendeur"
    // (adresse mail du vendeur)
    // Documents achat (TYID = 9 commande fournisseur) : REPLY TO : "Nom présent", "Acheteur" (adresse mail de l'acheteur)
    // Autres documents TYID <=4 ou TYID > 9) : REPLY TO : "Nom présent"
    TYREP.removeAllItems();
    if (lexique.isTrue("40")) {
      TYREP.addItem("Non présent");
      TYREP.addItem("Vendeur");
    }
    if (lexique.isTrue("41")) {
      TYREP.addItem("Non présent");
      TYREP.addItem("Acheteur");
    }
    if (lexique.isTrue("(N40) and (N41)")) {
      TYREP.addItem("Non présent");
    }
    if (lexique.HostFieldGetData("TYREP").equalsIgnoreCase("0")) {
      TYREP.setSelectedItem("Non présent");
    }
    if (lexique.HostFieldGetData("TYREP").equalsIgnoreCase("1")) {
      TYREP.setSelectedItem("Vendeur");
    }
    if (lexique.HostFieldGetData("TYREP").equalsIgnoreCase("2")) {
      TYREP.setSelectedItem("Acheteur");
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    if (((String) TYREP.getSelectedItem()).equalsIgnoreCase("Non présent")) {
      lexique.HostFieldPutData("TYREP", 0, "0");
    }
    if (((String) TYREP.getSelectedItem()).equalsIgnoreCase("Vendeur")) {
      lexique.HostFieldPutData("TYREP", 0, "1");
    }
    if (((String) TYREP.getSelectedItem()).equalsIgnoreCase("Acheteur")) {
      lexique.HostFieldPutData("TYREP", 0, "2");
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Charge le détail du type du mail affiché.
   * Le but est de récupérer le corps qui ne peut pas être transmit par la DataQueue.
   */
  private TypeMail chargerTypeMail() {
    IdEtablissement idEtablissement = snEtablissement.getIdSelection();
    if (idEtablissement == null) {
      throw new MessageErreurException("L'identifiant de l'établissement est invalide.");
    }
    Integer numeroType = Constantes.convertirTexteEnInteger(TYID.getText());
    if (numeroType == null || numeroType.intValue() == 0) {
      throw new MessageErreurException("Le numéro du type de mail est invalide.");
    }
    EnumTypeMail enumTypeMail = EnumTypeMail.valueOfByNumero(numeroType);
    
    CritereTypeMail critere = new CritereTypeMail();
    critere.setIdEtablissement(idEtablissement);
    critere.setIdTypeMail(IdTypeMail.getInstance(idEtablissement, enumTypeMail));
    ListeTypeMail listeTypeMail = ManagerServiceTechnique.chargerListeTypeMail(getIdSession(), critere);
    if (listeTypeMail == null) {
      throw new MessageErreurException("Le type de mail n'a pas été trouvé.");
    }
    if (listeTypeMail.size() > 1) {
      throw new MessageErreurException("Plusieurs type de mail ont été trouvés.");
    }
    return listeTypeMail.get(0);
  }
  
  /**
   * Sauve le corps du type du mail.
   */
  private void sauverCorpsTypeMail() {
    // Contrôle du mode
    if (!isModification()) {
      return;
    }
    // Vérification que le corps a été changé
    if (Constantes.equals(typeMail.getCorpsMessage(), taMessage.getText())) {
      return;
    }
    
    // Sauvegarde du corps du type de mail
    typeMail.setCorpsMessage(taMessage.getText());
    if (typeMail != null) {
      ManagerServiceTechnique.sauverTypeMail(getIdSession(), typeMail, true);
    }
  }
  
  /**
   * Retourne si le programme est en mode modification.
   */
  private boolean isModification() {
    boolean modeModification = false;
    if (lexique.HostFieldGetData("V01F").charAt(0) == 'M') {
      modeModification = true;
    }
    return modeModification;
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        sauverCorpsTypeMail();
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBpresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlIdentifiant = new SNPanel();
    lbEtablissement = new JLabel();
    snEtablissement = new SNEtablissement();
    lbNumeroType = new SNLabelChamp();
    TYID = new XRiTextField();
    pnlDetailTypeMail = new SNPanelTitre();
    lbDescription = new SNLabelChamp();
    TYLIBL = new XRiTextField();
    lbStatut = new SNLabelChamp();
    TYACTI = new XRiComboBox();
    lbDestinataire2 = new SNLabelChamp();
    TYREP = new XRiComboBox();
    lbDestinataire = new SNLabelChamp();
    TYDEST = new XRiTextField();
    lbSujet = new SNLabelChamp();
    TYSUJT = new XRiTextField();
    lbMessage = new SNLabelChamp();
    scrpMessage = new JScrollPane();
    taMessage = new JTextArea();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //---- pnlBpresentation ----
    pnlBpresentation.setText("Gestion des types de mail");
    pnlBpresentation.setName("pnlBpresentation");
    add(pnlBpresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(198, 198, 200));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());

      //======== pnlIdentifiant ========
      {
        pnlIdentifiant.setName("pnlIdentifiant");
        pnlIdentifiant.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlIdentifiant.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlIdentifiant.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlIdentifiant.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlIdentifiant.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEtablissement.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEtablissement.setMinimumSize(new Dimension(150, 30));
        lbEtablissement.setPreferredSize(new Dimension(150, 30));
        lbEtablissement.setMaximumSize(new Dimension(150, 30));
        lbEtablissement.setName("lbEtablissement");
        pnlIdentifiant.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setMinimumSize(new Dimension(320, 30));
        snEtablissement.setPreferredSize(new Dimension(320, 30));
        snEtablissement.setName("snEtablissement");
        snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snEtablissementValueChanged(e);
          }
        });
        pnlIdentifiant.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNumeroType ----
        lbNumeroType.setText("Num\u00e9ro du type");
        lbNumeroType.setName("lbNumeroType");
        pnlIdentifiant.add(lbNumeroType, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- TYID ----
        TYID.setHorizontalAlignment(SwingConstants.RIGHT);
        TYID.setPreferredSize(new Dimension(100, 30));
        TYID.setMinimumSize(new Dimension(100, 30));
        TYID.setMaximumSize(new Dimension(100, 30));
        TYID.setFont(TYID.getFont().deriveFont(TYID.getFont().getSize() + 2f));
        TYID.setName("TYID");
        pnlIdentifiant.add(TYID, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlIdentifiant, BorderLayout.NORTH);

      //======== pnlDetailTypeMail ========
      {
        pnlDetailTypeMail.setAutoscrolls(true);
        pnlDetailTypeMail.setOpaque(false);
        pnlDetailTypeMail.setTitre("D\u00e9tails du type");
        pnlDetailTypeMail.setName("pnlDetailTypeMail");
        pnlDetailTypeMail.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDetailTypeMail.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDetailTypeMail.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlDetailTypeMail.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlDetailTypeMail.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

        //---- lbDescription ----
        lbDescription.setText("Description");
        lbDescription.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDescription.setName("lbDescription");
        pnlDetailTypeMail.add(lbDescription, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- TYLIBL ----
        TYLIBL.setFont(TYLIBL.getFont().deriveFont(TYLIBL.getFont().getSize() + 2f));
        TYLIBL.setBackground(Color.white);
        TYLIBL.setName("TYLIBL");
        pnlDetailTypeMail.add(TYLIBL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbStatut ----
        lbStatut.setText("Statut");
        lbStatut.setHorizontalAlignment(SwingConstants.RIGHT);
        lbStatut.setName("lbStatut");
        pnlDetailTypeMail.add(lbStatut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- TYACTI ----
        TYACTI.setModel(new DefaultComboBoxModel(new String[] {
          "Inactif",
          "Actif"
        }));
        TYACTI.setFont(TYACTI.getFont().deriveFont(TYACTI.getFont().getSize() + 2f));
        TYACTI.setPreferredSize(new Dimension(140, 30));
        TYACTI.setMinimumSize(new Dimension(140, 30));
        TYACTI.setMaximumSize(new Dimension(140, 30));
        TYACTI.setName("TYACTI");
        pnlDetailTypeMail.add(TYACTI, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDestinataire2 ----
        lbDestinataire2.setText("R\u00e9pondre \u00e0");
        lbDestinataire2.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDestinataire2.setName("lbDestinataire2");
        pnlDetailTypeMail.add(lbDestinataire2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- TYREP ----
        TYREP.setFont(TYREP.getFont().deriveFont(TYREP.getFont().getSize() + 2f));
        TYREP.setPreferredSize(new Dimension(140, 30));
        TYREP.setMinimumSize(new Dimension(140, 30));
        TYREP.setMaximumSize(new Dimension(140, 30));
        TYREP.setName("TYREP");
        pnlDetailTypeMail.add(TYREP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDestinataire ----
        lbDestinataire.setText("Destinataire");
        lbDestinataire.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDestinataire.setName("lbDestinataire");
        pnlDetailTypeMail.add(lbDestinataire, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- TYDEST ----
        TYDEST.setFont(TYDEST.getFont().deriveFont(TYDEST.getFont().getSize() + 2f));
        TYDEST.setBackground(Color.white);
        TYDEST.setName("TYDEST");
        pnlDetailTypeMail.add(TYDEST, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbSujet ----
        lbSujet.setText("Sujet");
        lbSujet.setHorizontalAlignment(SwingConstants.RIGHT);
        lbSujet.setName("lbSujet");
        pnlDetailTypeMail.add(lbSujet, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- TYSUJT ----
        TYSUJT.setFont(TYSUJT.getFont().deriveFont(TYSUJT.getFont().getSize() + 2f));
        TYSUJT.setBackground(Color.white);
        TYSUJT.setPreferredSize(new Dimension(500, 30));
        TYSUJT.setName("TYSUJT");
        pnlDetailTypeMail.add(TYSUJT, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMessage ----
        lbMessage.setText("Message");
        lbMessage.setName("lbMessage");
        pnlDetailTypeMail.add(lbMessage, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== scrpMessage ========
        {
          scrpMessage.setOpaque(true);
          scrpMessage.setName("scrpMessage");

          //---- taMessage ----
          taMessage.setWrapStyleWord(true);
          taMessage.setFont(taMessage.getFont().deriveFont(taMessage.getFont().getSize() + 2f));
          taMessage.setName("taMessage");
          scrpMessage.setViewportView(taMessage);
        }
        pnlDetailTypeMail.add(scrpMessage, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDetailTypeMail, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre pnlBpresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlIdentifiant;
  private JLabel lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbNumeroType;
  private XRiTextField TYID;
  private SNPanelTitre pnlDetailTypeMail;
  private SNLabelChamp lbDescription;
  private XRiTextField TYLIBL;
  private SNLabelChamp lbStatut;
  private XRiComboBox TYACTI;
  private SNLabelChamp lbDestinataire2;
  private XRiComboBox TYREP;
  private SNLabelChamp lbDestinataire;
  private XRiTextField TYDEST;
  private SNLabelChamp lbSujet;
  private XRiTextField TYSUJT;
  private SNLabelChamp lbMessage;
  private JScrollPane scrpMessage;
  private JTextArea taMessage;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
