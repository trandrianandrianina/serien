
package ri.serien.libecranrpg.vexp.VEXP03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VEXP03FM_MD extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP03FM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(OBJ_7);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "R", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "C", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "M", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "I", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "A", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "D", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_11 = new JButton();
    OBJ_12 = new JButton();
    OBJ_8 = new JButton();
    OBJ_7 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(195, 265));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OBJ_9 ----
        OBJ_9.setText("Modification");
        OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_9.setFont(OBJ_9.getFont().deriveFont(OBJ_9.getFont().getSize() + 2f));
        OBJ_9.setForeground(Color.white);
        OBJ_9.setBackground(new Color(90, 90, 90));
        OBJ_9.setName("OBJ_9");
        OBJ_9.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_9ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_9);
        OBJ_9.setBounds(10, 90, 175, 40);

        //---- OBJ_10 ----
        OBJ_10.setText("Consultation");
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getSize() + 2f));
        OBJ_10.setForeground(Color.white);
        OBJ_10.setBackground(new Color(90, 90, 90));
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_10);
        OBJ_10.setBounds(10, 130, 175, 40);

        //---- OBJ_11 ----
        OBJ_11.setText("Annulation");
        OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_11.setFont(OBJ_11.getFont().deriveFont(OBJ_11.getFont().getSize() + 2f));
        OBJ_11.setForeground(Color.white);
        OBJ_11.setBackground(new Color(90, 90, 90));
        OBJ_11.setName("OBJ_11");
        OBJ_11.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_11ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_11);
        OBJ_11.setBounds(10, 170, 175, 40);

        //---- OBJ_12 ----
        OBJ_12.setText("Duplication");
        OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_12.setFont(OBJ_12.getFont().deriveFont(OBJ_12.getFont().getSize() + 2f));
        OBJ_12.setForeground(Color.white);
        OBJ_12.setBackground(new Color(90, 90, 90));
        OBJ_12.setName("OBJ_12");
        OBJ_12.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_12ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_12);
        OBJ_12.setBounds(10, 210, 175, 40);

        //---- OBJ_8 ----
        OBJ_8.setText("Cr\u00e9ation");
        OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_8.setFont(OBJ_8.getFont().deriveFont(OBJ_8.getFont().getSize() + 2f));
        OBJ_8.setForeground(Color.white);
        OBJ_8.setBackground(new Color(90, 90, 90));
        OBJ_8.setName("OBJ_8");
        OBJ_8.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_8ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_8);
        OBJ_8.setBounds(10, 50, 175, 40);

        //---- OBJ_7 ----
        OBJ_7.setText("Retour");
        OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_7.setFont(OBJ_7.getFont().deriveFont(OBJ_7.getFont().getSize() + 2f));
        OBJ_7.setForeground(Color.white);
        OBJ_7.setBackground(new Color(90, 90, 90));
        OBJ_7.setName("OBJ_7");
        OBJ_7.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_7ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_7);
        OBJ_7.setBounds(10, 10, 175, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_11;
  private JButton OBJ_12;
  private JButton OBJ_8;
  private JButton OBJ_7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
