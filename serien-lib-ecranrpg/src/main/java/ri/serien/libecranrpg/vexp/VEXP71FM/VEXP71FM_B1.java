
package ri.serien.libecranrpg.vexp.VEXP71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VEXP71FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP71FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    T01.setValeursSelection("X", "");
    T02.setValeursSelection("X", "");
    T03.setValeursSelection("X", "");
    T04.setValeursSelection("X", "");
    T05.setValeursSelection("X", "");
    T06.setValeursSelection("X", "");
    T07.setValeursSelection("X", "");
    T08.setValeursSelection("X", "");
    T09.setValeursSelection("X", "");
    T10.setValeursSelection("X", "");
    T11.setValeursSelection("X", "");
    T12.setValeursSelection("X", "");
    T13.setValeursSelection("X", "");
    T14.setValeursSelection("X", "");
    T15.setValeursSelection("X", "");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    L01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L01@")).trim());
    L02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L02@")).trim());
    L03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L03@")).trim());
    L04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L04@")).trim());
    L05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L05@")).trim());
    L06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L06@")).trim());
    L07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L07@")).trim());
    L08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L08@")).trim());
    L09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L09@")).trim());
    L10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L10@")).trim());
    L11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L11@")).trim());
    L12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L12@")).trim());
    L13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L13@")).trim());
    L14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L14@")).trim());
    L15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(T01, T01.get_LIST_Title_Data_Brut(), _T01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Sauvegarde générale"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    L01 = new RiZoneSortie();
    T01 = new XRiCheckBox();
    L02 = new RiZoneSortie();
    T02 = new XRiCheckBox();
    L03 = new RiZoneSortie();
    T03 = new XRiCheckBox();
    L04 = new RiZoneSortie();
    T04 = new XRiCheckBox();
    L05 = new RiZoneSortie();
    T05 = new XRiCheckBox();
    L06 = new RiZoneSortie();
    T06 = new XRiCheckBox();
    L07 = new RiZoneSortie();
    T07 = new XRiCheckBox();
    L08 = new RiZoneSortie();
    T08 = new XRiCheckBox();
    L09 = new RiZoneSortie();
    T09 = new XRiCheckBox();
    L10 = new RiZoneSortie();
    T10 = new XRiCheckBox();
    L11 = new RiZoneSortie();
    T11 = new XRiCheckBox();
    L12 = new RiZoneSortie();
    T12 = new XRiCheckBox();
    L13 = new RiZoneSortie();
    T13 = new XRiCheckBox();
    L14 = new RiZoneSortie();
    T14 = new XRiCheckBox();
    L15 = new RiZoneSortie();
    T15 = new XRiCheckBox();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(810, 450));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Choix des r\u00e9pertoires"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- L01 ----
          L01.setText("@L01@");
          L01.setName("L01");
          p_recup.add(L01);
          L01.setBounds(50, 35, 514, L01.getPreferredSize().height);

          //---- T01 ----
          T01.setName("T01");
          p_recup.add(T01);
          T01.setBounds(new Rectangle(new Point(15, 35), T01.getPreferredSize()));

          //---- L02 ----
          L02.setText("@L02@");
          L02.setName("L02");
          p_recup.add(L02);
          L02.setBounds(50, 60, 514, L02.getPreferredSize().height);

          //---- T02 ----
          T02.setName("T02");
          p_recup.add(T02);
          T02.setBounds(new Rectangle(new Point(15, 60), T02.getPreferredSize()));

          //---- L03 ----
          L03.setText("@L03@");
          L03.setName("L03");
          p_recup.add(L03);
          L03.setBounds(50, 85, 514, L03.getPreferredSize().height);

          //---- T03 ----
          T03.setName("T03");
          p_recup.add(T03);
          T03.setBounds(new Rectangle(new Point(15, 85), T03.getPreferredSize()));

          //---- L04 ----
          L04.setText("@L04@");
          L04.setName("L04");
          p_recup.add(L04);
          L04.setBounds(50, 110, 514, L04.getPreferredSize().height);

          //---- T04 ----
          T04.setName("T04");
          p_recup.add(T04);
          T04.setBounds(new Rectangle(new Point(15, 110), T04.getPreferredSize()));

          //---- L05 ----
          L05.setText("@L05@");
          L05.setName("L05");
          p_recup.add(L05);
          L05.setBounds(50, 135, 514, L05.getPreferredSize().height);

          //---- T05 ----
          T05.setName("T05");
          p_recup.add(T05);
          T05.setBounds(new Rectangle(new Point(15, 135), T05.getPreferredSize()));

          //---- L06 ----
          L06.setText("@L06@");
          L06.setName("L06");
          p_recup.add(L06);
          L06.setBounds(50, 160, 514, L06.getPreferredSize().height);

          //---- T06 ----
          T06.setName("T06");
          p_recup.add(T06);
          T06.setBounds(new Rectangle(new Point(15, 160), T06.getPreferredSize()));

          //---- L07 ----
          L07.setText("@L07@");
          L07.setName("L07");
          p_recup.add(L07);
          L07.setBounds(50, 185, 514, L07.getPreferredSize().height);

          //---- T07 ----
          T07.setName("T07");
          p_recup.add(T07);
          T07.setBounds(new Rectangle(new Point(15, 185), T07.getPreferredSize()));

          //---- L08 ----
          L08.setText("@L08@");
          L08.setName("L08");
          p_recup.add(L08);
          L08.setBounds(50, 210, 514, L08.getPreferredSize().height);

          //---- T08 ----
          T08.setName("T08");
          p_recup.add(T08);
          T08.setBounds(new Rectangle(new Point(15, 210), T08.getPreferredSize()));

          //---- L09 ----
          L09.setText("@L09@");
          L09.setName("L09");
          p_recup.add(L09);
          L09.setBounds(50, 235, 514, L09.getPreferredSize().height);

          //---- T09 ----
          T09.setName("T09");
          p_recup.add(T09);
          T09.setBounds(new Rectangle(new Point(15, 235), T09.getPreferredSize()));

          //---- L10 ----
          L10.setText("@L10@");
          L10.setName("L10");
          p_recup.add(L10);
          L10.setBounds(50, 260, 514, L10.getPreferredSize().height);

          //---- T10 ----
          T10.setName("T10");
          p_recup.add(T10);
          T10.setBounds(new Rectangle(new Point(15, 260), T10.getPreferredSize()));

          //---- L11 ----
          L11.setText("@L11@");
          L11.setName("L11");
          p_recup.add(L11);
          L11.setBounds(50, 285, 514, L11.getPreferredSize().height);

          //---- T11 ----
          T11.setName("T11");
          p_recup.add(T11);
          T11.setBounds(new Rectangle(new Point(15, 285), T11.getPreferredSize()));

          //---- L12 ----
          L12.setText("@L12@");
          L12.setName("L12");
          p_recup.add(L12);
          L12.setBounds(50, 310, 514, L12.getPreferredSize().height);

          //---- T12 ----
          T12.setName("T12");
          p_recup.add(T12);
          T12.setBounds(new Rectangle(new Point(15, 310), T12.getPreferredSize()));

          //---- L13 ----
          L13.setText("@L13@");
          L13.setName("L13");
          p_recup.add(L13);
          L13.setBounds(50, 335, 514, L13.getPreferredSize().height);

          //---- T13 ----
          T13.setName("T13");
          p_recup.add(T13);
          T13.setBounds(new Rectangle(new Point(15, 335), T13.getPreferredSize()));

          //---- L14 ----
          L14.setText("@L14@");
          L14.setName("L14");
          p_recup.add(L14);
          L14.setBounds(50, 360, 514, L14.getPreferredSize().height);

          //---- T14 ----
          T14.setName("T14");
          p_recup.add(T14);
          T14.setBounds(new Rectangle(new Point(15, 360), T14.getPreferredSize()));

          //---- L15 ----
          L15.setText("@L15@");
          L15.setName("L15");
          p_recup.add(L15);
          L15.setBounds(50, 385, 514, L15.getPreferredSize().height);

          //---- T15 ----
          T15.setName("T15");
          p_recup.add(T15);
          T15.setBounds(new Rectangle(new Point(15, 385), T15.getPreferredSize()));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(575, 35, 25, 165);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(575, 244, 25, 165);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie L01;
  private XRiCheckBox T01;
  private RiZoneSortie L02;
  private XRiCheckBox T02;
  private RiZoneSortie L03;
  private XRiCheckBox T03;
  private RiZoneSortie L04;
  private XRiCheckBox T04;
  private RiZoneSortie L05;
  private XRiCheckBox T05;
  private RiZoneSortie L06;
  private XRiCheckBox T06;
  private RiZoneSortie L07;
  private XRiCheckBox T07;
  private RiZoneSortie L08;
  private XRiCheckBox T08;
  private RiZoneSortie L09;
  private XRiCheckBox T09;
  private RiZoneSortie L10;
  private XRiCheckBox T10;
  private RiZoneSortie L11;
  private XRiCheckBox T11;
  private RiZoneSortie L12;
  private XRiCheckBox T12;
  private RiZoneSortie L13;
  private XRiCheckBox T13;
  private RiZoneSortie L14;
  private XRiCheckBox T14;
  private RiZoneSortie L15;
  private XRiCheckBox T15;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
