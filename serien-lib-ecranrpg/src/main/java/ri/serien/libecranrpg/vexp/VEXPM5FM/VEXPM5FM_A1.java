
package ri.serien.libecranrpg.vexp.VEXPM5FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXPM5FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  public VEXPM5FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion serveur"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    MAID = new XRiTextField();
    lbIDMail = new JLabel();
    lbDateCre = new JLabel();
    lbNumeroDestinataire = new JLabel();
    NUMDES = new XRiTextField();
    lbNomDestinataire = new JLabel();
    NOMDES = new XRiTextField();
    lbNumeroDocument = new JLabel();
    NUMDOC = new XRiTextField();
    lbStatut = new JLabel();
    LIBSTA = new XRiTextField();
    lbType = new JLabel();
    LIBTYP = new XRiTextField();
    lbDateExp = new JLabel();
    DATCRE = new XRiTextField();
    DATEXP = new XRiTextField();
    lbSujet = new JLabel();
    MASUJT = new XRiTextField();
    lbMailDestinataire = new JLabel();
    WMAMDST = new XRiTextField();
    scrollPane1 = new JScrollPane();
    MAMSG = new RiTextArea();
    lbStatut2 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Gestion des mails");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("?A1.bouton_erreurs.toolTipText?");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("?A1.bouton_valider.toolTipText?");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("?A1.bouton_retour.toolTipText?");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("?A1.riSousMenu_bt_consult.toolTipText?");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("?A1.riSousMenu_bt_modif.toolTipText?");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("?A1.riSousMenu_bt_crea.toolTipText?");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("?A1.riSousMenu_bt_suppr.toolTipText?");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("?A1.riSousMenu_bt_dupli.toolTipText?");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 440));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 440));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(null, "D\u00e9tails du mail", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
                new Font("Tahoma", Font.PLAIN, 11)));
            panel1.setAutoscrolls(true);
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- MAID ----
            MAID.setHorizontalAlignment(SwingConstants.RIGHT);
            MAID.setName("MAID");
            panel1.add(MAID);
            MAID.setBounds(245, 25, 92, MAID.getPreferredSize().height);
            
            // ---- lbIDMail ----
            lbIDMail.setText("Identifiant du mail");
            lbIDMail.setHorizontalAlignment(SwingConstants.RIGHT);
            lbIDMail.setName("lbIDMail");
            panel1.add(lbIDMail);
            lbIDMail.setBounds(20, 27, 215, 25);
            
            // ---- lbDateCre ----
            lbDateCre.setText("Date et heure de cr\u00e9ation du mail");
            lbDateCre.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateCre.setName("lbDateCre");
            panel1.add(lbDateCre);
            lbDateCre.setBounds(20, 90, 215, 18);
            
            // ---- lbNumeroDestinataire ----
            lbNumeroDestinataire.setText("Num\u00e9ro du destinataire");
            lbNumeroDestinataire.setHorizontalAlignment(SwingConstants.RIGHT);
            lbNumeroDestinataire.setName("lbNumeroDestinataire");
            panel1.add(lbNumeroDestinataire);
            lbNumeroDestinataire.setBounds(20, 150, 215, 18);
            
            // ---- NUMDES ----
            NUMDES.setComponentPopupMenu(null);
            NUMDES.setName("NUMDES");
            panel1.add(NUMDES);
            NUMDES.setBounds(245, 145, 210, NUMDES.getPreferredSize().height);
            
            // ---- lbNomDestinataire ----
            lbNomDestinataire.setText("Nom du destinataire");
            lbNomDestinataire.setHorizontalAlignment(SwingConstants.RIGHT);
            lbNomDestinataire.setName("lbNomDestinataire");
            panel1.add(lbNomDestinataire);
            lbNomDestinataire.setBounds(20, 180, 215, 18);
            
            // ---- NOMDES ----
            NOMDES.setComponentPopupMenu(null);
            NOMDES.setName("NOMDES");
            panel1.add(NOMDES);
            NOMDES.setBounds(245, 175, 510, NOMDES.getPreferredSize().height);
            
            // ---- lbNumeroDocument ----
            lbNumeroDocument.setText("Num\u00e9ro de documment");
            lbNumeroDocument.setHorizontalAlignment(SwingConstants.RIGHT);
            lbNumeroDocument.setName("lbNumeroDocument");
            panel1.add(lbNumeroDocument);
            lbNumeroDocument.setBounds(20, 240, 215, 18);
            
            // ---- NUMDOC ----
            NUMDOC.setComponentPopupMenu(null);
            NUMDOC.setName("NUMDOC");
            panel1.add(NUMDOC);
            NUMDOC.setBounds(245, 235, 210, NUMDOC.getPreferredSize().height);
            
            // ---- lbStatut ----
            lbStatut.setText("Statut du mail");
            lbStatut.setHorizontalAlignment(SwingConstants.RIGHT);
            lbStatut.setName("lbStatut");
            panel1.add(lbStatut);
            lbStatut.setBounds(20, 300, 215, 18);
            
            // ---- LIBSTA ----
            LIBSTA.setComponentPopupMenu(null);
            LIBSTA.setName("LIBSTA");
            panel1.add(LIBSTA);
            LIBSTA.setBounds(245, 295, 510, LIBSTA.getPreferredSize().height);
            
            // ---- lbType ----
            lbType.setText("Type de mail");
            lbType.setHorizontalAlignment(SwingConstants.RIGHT);
            lbType.setName("lbType");
            panel1.add(lbType);
            lbType.setBounds(20, 60, 215, 18);
            
            // ---- LIBTYP ----
            LIBTYP.setComponentPopupMenu(null);
            LIBTYP.setName("LIBTYP");
            panel1.add(LIBTYP);
            LIBTYP.setBounds(245, 55, 510, LIBTYP.getPreferredSize().height);
            
            // ---- lbDateExp ----
            lbDateExp.setText("Date et heure d'exp\u00e9dition du mail");
            lbDateExp.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateExp.setName("lbDateExp");
            panel1.add(lbDateExp);
            lbDateExp.setBounds(20, 120, 215, 18);
            
            // ---- DATCRE ----
            DATCRE.setComponentPopupMenu(null);
            DATCRE.setName("DATCRE");
            panel1.add(DATCRE);
            DATCRE.setBounds(245, 85, 150, DATCRE.getPreferredSize().height);
            
            // ---- DATEXP ----
            DATEXP.setComponentPopupMenu(null);
            DATEXP.setName("DATEXP");
            panel1.add(DATEXP);
            DATEXP.setBounds(245, 115, 150, DATEXP.getPreferredSize().height);
            
            // ---- lbSujet ----
            lbSujet.setText("Sujet du mail");
            lbSujet.setHorizontalAlignment(SwingConstants.RIGHT);
            lbSujet.setName("lbSujet");
            panel1.add(lbSujet);
            lbSujet.setBounds(20, 270, 215, 18);
            
            // ---- MASUJT ----
            MASUJT.setComponentPopupMenu(null);
            MASUJT.setName("MASUJT");
            panel1.add(MASUJT);
            MASUJT.setBounds(245, 265, 615, MASUJT.getPreferredSize().height);
            
            // ---- lbMailDestinataire ----
            lbMailDestinataire.setText("Adresse mail du destinataire");
            lbMailDestinataire.setHorizontalAlignment(SwingConstants.RIGHT);
            lbMailDestinataire.setName("lbMailDestinataire");
            panel1.add(lbMailDestinataire);
            lbMailDestinataire.setBounds(20, 210, 215, 18);
            
            // ---- WMAMDST ----
            WMAMDST.setComponentPopupMenu(null);
            WMAMDST.setName("WMAMDST");
            panel1.add(WMAMDST);
            WMAMDST.setBounds(245, 205, 510, WMAMDST.getPreferredSize().height);
            
            // ======== scrollPane1 ========
            {
              scrollPane1.setName("scrollPane1");
              
              // ---- MAMSG ----
              MAMSG.setComponentPopupMenu(null);
              MAMSG.setName("MAMSG");
              scrollPane1.setViewportView(MAMSG);
            }
            panel1.add(scrollPane1);
            scrollPane1.setBounds(245, 325, 615, 75);
            
            // ---- lbStatut2 ----
            lbStatut2.setText("Message");
            lbStatut2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbStatut2.setName("lbStatut2");
            panel1.add(lbStatut2);
            lbStatut2.setBounds(20, 330, 215, 18);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout
              .setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout
              .setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField MAID;
  private JLabel lbIDMail;
  private JLabel lbDateCre;
  private JLabel lbNumeroDestinataire;
  private XRiTextField NUMDES;
  private JLabel lbNomDestinataire;
  private XRiTextField NOMDES;
  private JLabel lbNumeroDocument;
  private XRiTextField NUMDOC;
  private JLabel lbStatut;
  private XRiTextField LIBSTA;
  private JLabel lbType;
  private XRiTextField LIBTYP;
  private JLabel lbDateExp;
  private XRiTextField DATCRE;
  private XRiTextField DATEXP;
  private JLabel lbSujet;
  private XRiTextField MASUJT;
  private JLabel lbMailDestinataire;
  private XRiTextField WMAMDST;
  private JScrollPane scrollPane1;
  private RiTextArea MAMSG;
  private JLabel lbStatut2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
