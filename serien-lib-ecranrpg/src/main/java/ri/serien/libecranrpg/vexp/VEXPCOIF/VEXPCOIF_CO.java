
package ri.serien.libecranrpg.vexp.VEXPCOIF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VEXPCOIF_CO extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXPCOIF_CO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    // setRefreshPanelUnderMe(false);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    button1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO1@")).trim());
    button2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO2@")).trim());
    button3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO3@")).trim());
    button4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO4@")).trim());
    button5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO5@")).trim());
    button6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO6@")).trim());
    button7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO7@")).trim());
    button8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO8@")).trim());
    button9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO9@")).trim());
    button10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    button1.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button1.setVisible(!lexique.HostFieldGetData("TBO1").trim().equalsIgnoreCase(""));
    button2.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button2.setVisible(!lexique.HostFieldGetData("TBO2").trim().equalsIgnoreCase(""));
    button3.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button3.setVisible(!lexique.HostFieldGetData("TBO3").trim().equalsIgnoreCase(""));
    button4.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button4.setVisible(!lexique.HostFieldGetData("TBO4").trim().equalsIgnoreCase(""));
    button5.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button5.setVisible(!lexique.HostFieldGetData("TBO5").trim().equalsIgnoreCase(""));
    button6.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button6.setVisible(!lexique.HostFieldGetData("TBO6").trim().equalsIgnoreCase(""));
    button7.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button7.setVisible(!lexique.HostFieldGetData("TBO7").trim().equalsIgnoreCase(""));
    button8.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button8.setVisible(!lexique.HostFieldGetData("TBO8").trim().equalsIgnoreCase(""));
    button9.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button9.setVisible(!lexique.HostFieldGetData("TBO9").trim().equalsIgnoreCase(""));
    button10.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    button10.setVisible(!lexique.HostFieldGetData("TBO10").trim().equalsIgnoreCase(""));
    
    p_contenu.setBackground(Constantes.COULEUR_F1);
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Options disponibles"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N1"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N2"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N3"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N4"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N5"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N6"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N7"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N8"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N9"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void button10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("NA"));
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    bouton_retour = new JButton();
    panel2 = new JPanel();
    button1 = new JButton();
    button2 = new JButton();
    button3 = new JButton();
    button4 = new JButton();
    button5 = new JButton();
    button6 = new JButton();
    button7 = new JButton();
    button8 = new JButton();
    button9 = new JButton();
    button10 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(435, 380));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new BorderLayout());

        //======== panel1 ========
        {
          panel1.setPreferredSize(new Dimension(435, 50));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setFont(bouton_retour.getFont().deriveFont(bouton_retour.getFont().getStyle() | Font.BOLD, bouton_retour.getFont().getSize() + 2f));
          bouton_retour.setMaximumSize(new Dimension(180, 40));
          bouton_retour.setMinimumSize(new Dimension(180, 40));
          bouton_retour.setPreferredSize(new Dimension(180, 40));
          bouton_retour.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addContainerGap(128, Short.MAX_VALUE)
                .addComponent(bouton_retour, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(127, 127, 127))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(bouton_retour, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_contenu.add(panel1, BorderLayout.SOUTH);

        //======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //---- button1 ----
          button1.setText("@TBO1@");
          button1.setHorizontalAlignment(SwingConstants.LEADING);
          button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button1.setName("button1");
          button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button1ActionPerformed(e);
            }
          });

          //---- button2 ----
          button2.setText("@TBO2@");
          button2.setHorizontalAlignment(SwingConstants.LEADING);
          button2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button2.setName("button2");
          button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button2ActionPerformed(e);
            }
          });

          //---- button3 ----
          button3.setText("@TBO3@");
          button3.setHorizontalAlignment(SwingConstants.LEADING);
          button3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button3.setName("button3");
          button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button3ActionPerformed(e);
            }
          });

          //---- button4 ----
          button4.setText("@TBO4@");
          button4.setHorizontalAlignment(SwingConstants.LEADING);
          button4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button4.setName("button4");
          button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button4ActionPerformed(e);
            }
          });

          //---- button5 ----
          button5.setText("@TBO5@");
          button5.setHorizontalAlignment(SwingConstants.LEADING);
          button5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button5.setName("button5");
          button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button5ActionPerformed(e);
            }
          });

          //---- button6 ----
          button6.setText("@TBO6@");
          button6.setHorizontalAlignment(SwingConstants.LEADING);
          button6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button6.setName("button6");
          button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button6ActionPerformed(e);
            }
          });

          //---- button7 ----
          button7.setText("@TBO7@");
          button7.setHorizontalAlignment(SwingConstants.LEADING);
          button7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button7.setName("button7");
          button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button7ActionPerformed(e);
            }
          });

          //---- button8 ----
          button8.setText("@TBO8@");
          button8.setHorizontalAlignment(SwingConstants.LEADING);
          button8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button8.setName("button8");
          button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button8ActionPerformed(e);
            }
          });

          //---- button9 ----
          button9.setText("@TBO9@");
          button9.setHorizontalAlignment(SwingConstants.LEADING);
          button9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button9.setName("button9");
          button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button9ActionPerformed(e);
            }
          });

          //---- button10 ----
          button10.setText("@TBO10@");
          button10.setHorizontalAlignment(SwingConstants.LEADING);
          button10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          button10.setName("button10");
          button10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button10ActionPerformed(e);
            }
          });

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(button1, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button2, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button3, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button4, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button5, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button6, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button7, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button8, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button9, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(button10, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(button1)
                .addGap(2, 2, 2)
                .addComponent(button2)
                .addGap(2, 2, 2)
                .addComponent(button3)
                .addGap(2, 2, 2)
                .addComponent(button4)
                .addGap(2, 2, 2)
                .addComponent(button5)
                .addGap(2, 2, 2)
                .addComponent(button6)
                .addGap(2, 2, 2)
                .addComponent(button7)
                .addGap(2, 2, 2)
                .addComponent(button8)
                .addGap(2, 2, 2)
                .addComponent(button9)
                .addGap(2, 2, 2)
                .addComponent(button10))
          );
        }
        p_contenu.add(panel2, BorderLayout.CENTER);
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton bouton_retour;
  private JPanel panel2;
  private JButton button1;
  private JButton button2;
  private JButton button3;
  private JButton button4;
  private JButton button5;
  private JButton button6;
  private JButton button7;
  private JButton button8;
  private JButton button9;
  private JButton button10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
