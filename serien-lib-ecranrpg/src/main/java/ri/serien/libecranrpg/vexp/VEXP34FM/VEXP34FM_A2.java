
package ri.serien.libecranrpg.vexp.VEXP34FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VEXP34FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  
  public VEXP34FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    INDCLC.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(WTP01, WTP01.get_WTP01_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    
    panel1.setVisible(lexique.getMode() == Lexical.MODE_CREATION || lexique.getMode() == Lexical.MODE_MODIFICATION);
    panel3.setVisible(lexique.getMode() != Lexical.MODE_CREATION && lexique.getMode() != Lexical.MODE_MODIFICATION);
    
    if (panel1.isVisible()) {
      TitledBorder t = (TitledBorder) panel1.getBorder();
      if (lexique.getMode() == Lexical.MODE_CREATION) {
        t.setTitle("Création");
      }
      else {
        t.setTitle("Modification");
      }
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    INDNOM = new XRiTextField();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_74_OBJ_74 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    INDINS = new XRiTextField();
    OBJ_75_OBJ_75 = new JLabel();
    INDCDP = new XRiTextField();
    OBJ_76_OBJ_76 = new JLabel();
    INDCOM = new XRiTextField();
    INDDEP = new XRiTextField();
    INDCAN = new XRiTextField();
    INDARR = new XRiTextField();
    INDCLC = new XRiCheckBox();
    OBJ_78_OBJ_81 = new JLabel();
    INDZGE = new XRiTextField();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel3 = new JPanel();
    OBJ_79_OBJ_80 = new JLabel();
    OBJ_74_OBJ_75 = new JLabel();
    OBJ_78_OBJ_79 = new JLabel();
    OBJ_77_OBJ_78 = new JLabel();
    OBJ_80_OBJ_81 = new JLabel();
    OBJ_75_OBJ_76 = new JLabel();
    OBJ_76_OBJ_77 = new JLabel();
    WZGE = new XRiTextField();
    OBJ_78_OBJ_80 = new JLabel();
    WDEP = new XRiTextField();
    WARR = new XRiTextField();
    WCAN = new XRiTextField();
    WCOM = new XRiTextField();
    WCDP = new XRiTextField();
    ARG2 = new XRiTextField();
    WINS = new XRiTextField();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_21 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1100, 660));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des communes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(1005, 32));
          p_tete_gauche.setMinimumSize(new Dimension(1005, 32));
          p_tete_gauche.setName("p_tete_gauche");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 1060, Short.MAX_VALUE)
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 30, Short.MAX_VALUE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(60, 0));
          p_tete_droite.setMinimumSize(new Dimension(60, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(710, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(669, 570));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Cr\u00e9ation"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- INDNOM ----
            INDNOM.setComponentPopupMenu(null);
            INDNOM.setName("INDNOM");
            panel1.add(INDNOM);
            INDNOM.setBounds(65, 65, 160, INDNOM.getPreferredSize().height);

            //---- OBJ_79_OBJ_79 ----
            OBJ_79_OBJ_79.setText("Arrondissement");
            OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");
            panel1.add(OBJ_79_OBJ_79);
            OBJ_79_OBJ_79.setBounds(240, 34, 97, 20);

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("D\u00e9partement");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");
            panel1.add(OBJ_74_OBJ_74);
            OBJ_74_OBJ_74.setBounds(25, 34, 84, 20);

            //---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("Code INSEE");
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
            panel1.add(OBJ_78_OBJ_78);
            OBJ_78_OBJ_78.setBounds(240, 70, 74, 20);

            //---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("Code postal");
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
            panel1.add(OBJ_77_OBJ_77);
            OBJ_77_OBJ_77.setBounds(520, 34, 76, 20);

            //---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("Commune");
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
            panel1.add(OBJ_80_OBJ_80);
            OBJ_80_OBJ_80.setBounds(390, 34, 64, 20);

            //---- INDINS ----
            INDINS.setComponentPopupMenu(null);
            INDINS.setName("INDINS");
            panel1.add(INDINS);
            INDINS.setBounds(315, 65, 60, INDINS.getPreferredSize().height);

            //---- OBJ_75_OBJ_75 ----
            OBJ_75_OBJ_75.setText("Canton");
            OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
            panel1.add(OBJ_75_OBJ_75);
            OBJ_75_OBJ_75.setBounds(145, 34, 45, 20);

            //---- INDCDP ----
            INDCDP.setComponentPopupMenu(null);
            INDCDP.setName("INDCDP");
            panel1.add(INDCDP);
            INDCDP.setBounds(600, 30, 60, INDCDP.getPreferredSize().height);

            //---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("Nom");
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");
            panel1.add(OBJ_76_OBJ_76);
            OBJ_76_OBJ_76.setBounds(25, 69, 33, 20);

            //---- INDCOM ----
            INDCOM.setComponentPopupMenu(null);
            INDCOM.setName("INDCOM");
            panel1.add(INDCOM);
            INDCOM.setBounds(455, 30, 40, INDCOM.getPreferredSize().height);

            //---- INDDEP ----
            INDDEP.setComponentPopupMenu(null);
            INDDEP.setName("INDDEP");
            panel1.add(INDDEP);
            INDDEP.setBounds(105, 30, 30, INDDEP.getPreferredSize().height);

            //---- INDCAN ----
            INDCAN.setComponentPopupMenu(null);
            INDCAN.setName("INDCAN");
            panel1.add(INDCAN);
            INDCAN.setBounds(195, 30, 30, INDCAN.getPreferredSize().height);

            //---- INDARR ----
            INDARR.setComponentPopupMenu(null);
            INDARR.setName("INDARR");
            panel1.add(INDARR);
            INDARR.setBounds(345, 30, 30, INDARR.getPreferredSize().height);

            //---- INDCLC ----
            INDCLC.setComponentPopupMenu(null);
            INDCLC.setText("Chef lieu");
            INDCLC.setName("INDCLC");
            panel1.add(INDCLC);
            INDCLC.setBounds(390, 70, 80, INDCLC.getPreferredSize().height);

            //---- OBJ_78_OBJ_81 ----
            OBJ_78_OBJ_81.setText("Zone g\u00e9ographique");
            OBJ_78_OBJ_81.setName("OBJ_78_OBJ_81");
            panel1.add(OBJ_78_OBJ_81);
            OBJ_78_OBJ_81.setBounds(476, 69, 120, 20);

            //---- INDZGE ----
            INDZGE.setComponentPopupMenu(null);
            INDZGE.setName("INDZGE");
            panel1.add(INDZGE);
            INDZGE.setBounds(600, 65, 45, INDZGE.getPreferredSize().height);
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 40, 600, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(635, 40, 25, 120);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(635, 190, 25, 120);
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Recherche"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_79_OBJ_80 ----
            OBJ_79_OBJ_80.setText("Arrondissement");
            OBJ_79_OBJ_80.setName("OBJ_79_OBJ_80");
            panel3.add(OBJ_79_OBJ_80);
            OBJ_79_OBJ_80.setBounds(240, 34, 97, 20);

            //---- OBJ_74_OBJ_75 ----
            OBJ_74_OBJ_75.setText("D\u00e9partement");
            OBJ_74_OBJ_75.setName("OBJ_74_OBJ_75");
            panel3.add(OBJ_74_OBJ_75);
            OBJ_74_OBJ_75.setBounds(25, 34, 84, 20);

            //---- OBJ_78_OBJ_79 ----
            OBJ_78_OBJ_79.setText("Code INSEE");
            OBJ_78_OBJ_79.setName("OBJ_78_OBJ_79");
            panel3.add(OBJ_78_OBJ_79);
            OBJ_78_OBJ_79.setBounds(240, 70, 79, 20);

            //---- OBJ_77_OBJ_78 ----
            OBJ_77_OBJ_78.setText("Code postal");
            OBJ_77_OBJ_78.setName("OBJ_77_OBJ_78");
            panel3.add(OBJ_77_OBJ_78);
            OBJ_77_OBJ_78.setBounds(520, 34, 76, 20);

            //---- OBJ_80_OBJ_81 ----
            OBJ_80_OBJ_81.setText("Commune");
            OBJ_80_OBJ_81.setName("OBJ_80_OBJ_81");
            panel3.add(OBJ_80_OBJ_81);
            OBJ_80_OBJ_81.setBounds(390, 34, 64, 20);

            //---- OBJ_75_OBJ_76 ----
            OBJ_75_OBJ_76.setText("Canton");
            OBJ_75_OBJ_76.setName("OBJ_75_OBJ_76");
            panel3.add(OBJ_75_OBJ_76);
            OBJ_75_OBJ_76.setBounds(145, 34, 45, 20);

            //---- OBJ_76_OBJ_77 ----
            OBJ_76_OBJ_77.setText("Nom");
            OBJ_76_OBJ_77.setName("OBJ_76_OBJ_77");
            panel3.add(OBJ_76_OBJ_77);
            OBJ_76_OBJ_77.setBounds(25, 69, 33, 20);

            //---- WZGE ----
            WZGE.setComponentPopupMenu(null);
            WZGE.setName("WZGE");
            panel3.add(WZGE);
            WZGE.setBounds(600, 65, 45, WZGE.getPreferredSize().height);

            //---- OBJ_78_OBJ_80 ----
            OBJ_78_OBJ_80.setText("Zone g\u00e9ographique");
            OBJ_78_OBJ_80.setName("OBJ_78_OBJ_80");
            panel3.add(OBJ_78_OBJ_80);
            OBJ_78_OBJ_80.setBounds(476, 69, 120, 20);

            //---- WDEP ----
            WDEP.setComponentPopupMenu(null);
            WDEP.setName("WDEP");
            panel3.add(WDEP);
            WDEP.setBounds(105, 30, 30, WDEP.getPreferredSize().height);

            //---- WARR ----
            WARR.setComponentPopupMenu(null);
            WARR.setName("WARR");
            panel3.add(WARR);
            WARR.setBounds(345, 30, 30, WARR.getPreferredSize().height);

            //---- WCAN ----
            WCAN.setComponentPopupMenu(null);
            WCAN.setName("WCAN");
            panel3.add(WCAN);
            WCAN.setBounds(195, 30, 30, WCAN.getPreferredSize().height);

            //---- WCOM ----
            WCOM.setComponentPopupMenu(null);
            WCOM.setName("WCOM");
            panel3.add(WCOM);
            WCOM.setBounds(455, 30, 40, WCOM.getPreferredSize().height);

            //---- WCDP ----
            WCDP.setComponentPopupMenu(null);
            WCDP.setName("WCDP");
            panel3.add(WCDP);
            WCDP.setBounds(600, 30, 60, WCDP.getPreferredSize().height);

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(null);
            ARG2.setName("ARG2");
            panel3.add(ARG2);
            ARG2.setBounds(65, 65, 160, ARG2.getPreferredSize().height);

            //---- WINS ----
            WINS.setComponentPopupMenu(null);
            WINS.setName("WINS");
            panel3.add(WINS);
            WINS.setBounds(315, 65, 60, WINS.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_14 ----
      OBJ_14.setText("Modifier");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Supprimer");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Repr\u00e9sentants");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
      BTD.addSeparator();

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField INDNOM;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_74_OBJ_74;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_80_OBJ_80;
  private XRiTextField INDINS;
  private JLabel OBJ_75_OBJ_75;
  private XRiTextField INDCDP;
  private JLabel OBJ_76_OBJ_76;
  private XRiTextField INDCOM;
  private XRiTextField INDDEP;
  private XRiTextField INDCAN;
  private XRiTextField INDARR;
  private XRiCheckBox INDCLC;
  private JLabel OBJ_78_OBJ_81;
  private XRiTextField INDZGE;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel3;
  private JLabel OBJ_79_OBJ_80;
  private JLabel OBJ_74_OBJ_75;
  private JLabel OBJ_78_OBJ_79;
  private JLabel OBJ_77_OBJ_78;
  private JLabel OBJ_80_OBJ_81;
  private JLabel OBJ_75_OBJ_76;
  private JLabel OBJ_76_OBJ_77;
  private XRiTextField WZGE;
  private JLabel OBJ_78_OBJ_80;
  private XRiTextField WDEP;
  private XRiTextField WARR;
  private XRiTextField WCAN;
  private XRiTextField WCOM;
  private XRiTextField WCDP;
  private XRiTextField ARG2;
  private XRiTextField WINS;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
