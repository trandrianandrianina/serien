
package ri.serien.libecranrpg.vexp.VEXPCOLF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VEXPCOLF_CO extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXPCOLF_CO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bt_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    B01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO1@")).trim());
    B02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO2@")).trim());
    B03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO3@")).trim());
    B04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO4@")).trim());
    B05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO5@")).trim());
    B06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO6@")).trim());
    B07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO7@")).trim());
    B08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO8@")).trim());
    B09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO9@")).trim());
    B10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO10@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @TBOT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    B10.setVisible(lexique.isTrue("N30") && !lexique.HostFieldGetData("TBO10").trim().equals(""));
    B09.setVisible(lexique.isTrue("N29") && !lexique.HostFieldGetData("TBO9").trim().equals(""));
    B08.setVisible(lexique.isTrue("N28") && !lexique.HostFieldGetData("TBO8").trim().equals(""));
    B07.setVisible(lexique.isTrue("N27") && !lexique.HostFieldGetData("TBO7").trim().equals(""));
    B06.setVisible(lexique.isTrue("N26") && !lexique.HostFieldGetData("TBO6").trim().equals(""));
    B05.setVisible(lexique.isTrue("N25") && !lexique.HostFieldGetData("TBO5").trim().equals(""));
    B04.setVisible(lexique.isTrue("N24") && !lexique.HostFieldGetData("TBO4").trim().equals(""));
    B03.setVisible(lexique.isTrue("N23") && !lexique.HostFieldGetData("TBO3").trim().equals(""));
    B02.setVisible(lexique.isTrue("N22") && !lexique.HostFieldGetData("TBO2").trim().equals(""));
    B01.setVisible(lexique.isTrue("N21") && !lexique.HostFieldGetData("TBO1").trim().equals(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix d'options"));
    
    bt_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void B01ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N1").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B02ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N2").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B03ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N3").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B04ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N4").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B05ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N5").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B06ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N6").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B07ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N7").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B08ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N8").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B09ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("N9").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, lexique.HostFieldGetData("NA").trim());
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    B01 = new JButton();
    B02 = new JButton();
    B03 = new JButton();
    B04 = new JButton();
    B05 = new JButton();
    B06 = new JButton();
    B07 = new JButton();
    B08 = new JButton();
    B09 = new JButton();
    B10 = new JButton();
    bt_retour = new JButton();
    label1 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(360, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setBackground(new Color(90, 90, 90));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setOpaque(false);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- B01 ----
        B01.setText("@TBO1@");
        B01.setHorizontalAlignment(SwingConstants.LEADING);
        B01.setPreferredSize(new Dimension(20, 25));
        B01.setMargin(new Insets(0, 0, 0, 0));
        B01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B01.setName("B01");
        B01.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B01ActionPerformed(e);
          }
        });
        p_contenu.add(B01);
        B01.setBounds(15, 35, 330, 30);

        //---- B02 ----
        B02.setText("@TBO2@");
        B02.setHorizontalAlignment(SwingConstants.LEFT);
        B02.setPreferredSize(new Dimension(20, 25));
        B02.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B02.setName("B02");
        B02.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B02ActionPerformed(e);
          }
        });
        p_contenu.add(B02);
        B02.setBounds(15, 65, 330, 30);

        //---- B03 ----
        B03.setText("@TBO3@");
        B03.setHorizontalAlignment(SwingConstants.LEFT);
        B03.setPreferredSize(new Dimension(20, 25));
        B03.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B03.setName("B03");
        B03.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B03ActionPerformed(e);
          }
        });
        p_contenu.add(B03);
        B03.setBounds(15, 95, 330, 30);

        //---- B04 ----
        B04.setText("@TBO4@");
        B04.setHorizontalAlignment(SwingConstants.LEFT);
        B04.setPreferredSize(new Dimension(20, 25));
        B04.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B04.setName("B04");
        B04.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B04ActionPerformed(e);
          }
        });
        p_contenu.add(B04);
        B04.setBounds(15, 125, 330, 30);

        //---- B05 ----
        B05.setText("@TBO5@");
        B05.setHorizontalAlignment(SwingConstants.LEFT);
        B05.setPreferredSize(new Dimension(20, 25));
        B05.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B05.setName("B05");
        B05.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B05ActionPerformed(e);
          }
        });
        p_contenu.add(B05);
        B05.setBounds(15, 155, 330, 30);

        //---- B06 ----
        B06.setText("@TBO6@");
        B06.setHorizontalAlignment(SwingConstants.LEFT);
        B06.setPreferredSize(new Dimension(20, 25));
        B06.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B06.setName("B06");
        B06.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B06ActionPerformed(e);
          }
        });
        p_contenu.add(B06);
        B06.setBounds(15, 185, 330, 30);

        //---- B07 ----
        B07.setText("@TBO7@");
        B07.setHorizontalAlignment(SwingConstants.LEFT);
        B07.setPreferredSize(new Dimension(20, 25));
        B07.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B07.setName("B07");
        B07.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B07ActionPerformed(e);
          }
        });
        p_contenu.add(B07);
        B07.setBounds(15, 215, 330, 30);

        //---- B08 ----
        B08.setText("@TBO8@");
        B08.setHorizontalAlignment(SwingConstants.LEFT);
        B08.setPreferredSize(new Dimension(20, 25));
        B08.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B08.setName("B08");
        B08.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B08ActionPerformed(e);
          }
        });
        p_contenu.add(B08);
        B08.setBounds(15, 245, 330, 30);

        //---- B09 ----
        B09.setText("@TBO9@");
        B09.setHorizontalAlignment(SwingConstants.LEFT);
        B09.setPreferredSize(new Dimension(20, 25));
        B09.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B09.setName("B09");
        B09.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B09ActionPerformed(e);
          }
        });
        p_contenu.add(B09);
        B09.setBounds(15, 275, 330, 30);

        //---- B10 ----
        B10.setText("@TBO10@");
        B10.setHorizontalAlignment(SwingConstants.LEFT);
        B10.setPreferredSize(new Dimension(20, 25));
        B10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B10.setName("B10");
        B10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B10ActionPerformed(e);
          }
        });
        p_contenu.add(B10);
        B10.setBounds(15, 305, 330, 30);

        //---- bt_retour ----
        bt_retour.setText("Retour");
        bt_retour.setHorizontalAlignment(SwingConstants.LEFT);
        bt_retour.setPreferredSize(new Dimension(20, 35));
        bt_retour.setFont(bt_retour.getFont().deriveFont(bt_retour.getFont().getStyle() | Font.BOLD));
        bt_retour.setMargin(new Insets(0, 0, 0, 0));
        bt_retour.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_retour.setName("bt_retour");
        bt_retour.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B12ActionPerformed(e);
          }
        });
        p_contenu.add(bt_retour);
        bt_retour.setBounds(210, 345, 135, 40);

        //---- label1 ----
        label1.setText("  @TBOT@");
        label1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        label1.setForeground(Color.white);
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(15, 10, 275, 25);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JButton B01;
  private JButton B02;
  private JButton B03;
  private JButton B04;
  private JButton B05;
  private JButton B06;
  private JButton B07;
  private JButton B08;
  private JButton B09;
  private JButton B10;
  private JButton bt_retour;
  private JLabel label1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
