
package ri.serien.libecranrpg.vexp.VEXP30FM;
// Nom Fichier: i_VEXP30FM_FMTC3_FMTF1_243.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP30FM_C3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP30FM_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    DMHTP.setValeursSelection("O", "N");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_Bib.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    DMTID3.setVisible(lexique.isPresent("DMTID3"));
    DMTID2.setVisible(lexique.isPresent("DMTID2"));
    DMTID1.setVisible(lexique.isPresent("DMTID1"));
    DMHRES.setEnabled(lexique.isPresent("DMHRES"));
    DMHREM.setEnabled(lexique.isPresent("DMHREM"));
    DMHREH.setEnabled(lexique.isPresent("DMHREH"));
    DMANO.setVisible(lexique.isPresent("DMANO"));
    DMETBC.setVisible(lexique.isPresent("DMETBC"));
    DMVJW.setVisible(lexique.isPresent("DMVJW"));
    DMBIB.setVisible(lexique.isPresent("DMBIB"));
    VS20.setVisible(lexique.isPresent("VS20"));
    VS19.setVisible(lexique.isPresent("VS19"));
    VS18.setVisible(lexique.isPresent("VS18"));
    VS17.setVisible(lexique.isPresent("VS17"));
    VS16.setVisible(lexique.isPresent("VS16"));
    VS15.setVisible(lexique.isPresent("VS15"));
    VS14.setVisible(lexique.isPresent("VS14"));
    VS13.setVisible(lexique.isPresent("VS13"));
    VS12.setVisible(lexique.isPresent("VS12"));
    VS11.setVisible(lexique.isPresent("VS11"));
    VS10.setVisible(lexique.isPresent("VS10"));
    VS09.setVisible(lexique.isPresent("VS09"));
    VS08.setVisible(lexique.isPresent("VS08"));
    VS07.setVisible(lexique.isPresent("VS07"));
    VS06.setVisible(lexique.isPresent("VS06"));
    VS05.setVisible(lexique.isPresent("VS05"));
    VS04.setVisible(lexique.isPresent("VS04"));
    VS03.setVisible(lexique.isPresent("VS03"));
    VS02.setVisible(lexique.isPresent("VS02"));
    VS01.setEnabled(lexique.isPresent("VS01"));
    DMPWJW.setVisible(lexique.isPresent("DMPWJW"));
    DMPLJW.setVisible(lexique.isPresent("DMPLJW"));
    DMMOD.setVisible(lexique.isPresent("DMMOD"));
    // DMDTJW.setVisible( lexique.isPresent("DMDTJW"));
    DMVOS.setVisible(lexique.isPresent("DMVOS"));
    DMIP.setVisible(lexique.isPresent("DMIP"));
    DMMNU.setVisible(lexique.isPresent("DMMNU"));
    DMECR.setVisible(lexique.isPresent("DMECR"));
    DMUSR.setVisible(lexique.isPresent("DMUSR"));
    DMPGM.setVisible(lexique.isPresent("DMPGM"));
    DMSER.setVisible(lexique.isPresent("DMSER"));
    DMID3.setVisible(lexique.isPresent("DMID3"));
    DMID2.setVisible(lexique.isPresent("DMID2"));
    DMID1.setVisible(lexique.isPresent("DMID1"));
    // DMHTP.setVisible( lexique.isPresent("DMHTP"));
    // DMHTP.setSelected(lexique.HostFieldGetData("DMHTP").equalsIgnoreCase("O"));
    DMOPT.setVisible(lexique.isPresent("DMOPT"));
    DMMIBM.setVisible(lexique.isPresent("DMMIBM"));
    DMJWP.setVisible(lexique.isPresent("DMJWP"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SERIE M - INTERVENTIONS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (DMHTP.isSelected())
    // lexique.HostFieldPutData("DMHTP", 0, "O");
    // else
    // lexique.HostFieldPutData("DMHTP", 0, "N");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_Bib = new JLabel();
    DMNOR = new XRiTextField();
    OBJ_162_OBJ_162 = new JLabel();
    INDNIN = new XRiTextField();
    DMDTEX = new XRiCalendrier();
    OBJ_161_OBJ_161 = new JLabel();
    OBJ_155_OBJ_155 = new JLabel();
    DMHREH = new XRiTextField();
    DMHREM = new XRiTextField();
    DMHRES = new XRiTextField();
    P_Centre = new JPanel();
    OBJ_24 = new JTabbedPane();
    OBJ_25 = new JPanel();
    OBJ_26 = new JPanel();
    OBJ_34_OBJ_34 = new JLabel();
    VS01 = new XRiTextField();
    VS02 = new XRiTextField();
    VS03 = new XRiTextField();
    VS04 = new XRiTextField();
    VS05 = new XRiTextField();
    VS06 = new XRiTextField();
    VS07 = new XRiTextField();
    VS08 = new XRiTextField();
    VS09 = new XRiTextField();
    VS10 = new XRiTextField();
    VS11 = new XRiTextField();
    VS12 = new XRiTextField();
    VS13 = new XRiTextField();
    VS14 = new XRiTextField();
    VS15 = new XRiTextField();
    VS16 = new XRiTextField();
    VS17 = new XRiTextField();
    VS18 = new XRiTextField();
    VS19 = new XRiTextField();
    VS20 = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_39_OBJ_39 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_69_OBJ_69 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_27 = new JPanel();
    DMSER = new XRiTextField();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_30_OBJ_30 = new JLabel();
    DMVOS = new XRiTextField();
    OBJ_75 = new JPanel();
    OBJ_82 = new JPanel();
    DMLDA = new XRiTextField();
    DMJWP = new XRiTextField();
    DMUSR = new XRiTextField();
    DMECR = new XRiTextField();
    OBJ_90_OBJ_90 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_88_OBJ_88 = new JLabel();
    OBJ_84_OBJ_84 = new JLabel();
    OBJ_80 = new JPanel();
    DMMIBM = new XRiTextField();
    DMOPT = new XRiTextField();
    DMPGM = new XRiTextField();
    DMMNU = new XRiTextField();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_113_OBJ_113 = new JLabel();
    OBJ_103_OBJ_103 = new JLabel();
    DMMOD = new XRiTextField();
    OBJ_115_OBJ_115 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    OBJ_108_OBJ_108 = new JLabel();
    DMANO = new XRiTextField();
    OBJ_78 = new JPanel();
    DMID1 = new XRiTextField();
    DMID2 = new XRiTextField();
    DMID3 = new XRiTextField();
    OBJ_96_OBJ_96 = new JLabel();
    OBJ_94_OBJ_94 = new JLabel();
    OBJ_100_OBJ_100 = new JLabel();
    OBJ_105_OBJ_105 = new JLabel();
    OBJ_110_OBJ_110 = new JLabel();
    DMBIB = new XRiTextField();
    DMETBC = new XRiTextField();
    DMTID1 = new XRiTextField();
    DMTID2 = new XRiTextField();
    DMTID3 = new XRiTextField();
    OBJ_76 = new JPanel();
    DMHTP = new XRiCheckBox();
    OBJ_120_OBJ_120 = new JLabel();
    OBJ_124_OBJ_124 = new JLabel();
    DMIP = new XRiTextField();
    OBJ_126_OBJ_126 = new JLabel();
    OBJ_118_OBJ_118 = new JLabel();
    DMDTJW = new XRiCalendrier();
    DMPLJW = new XRiTextField();
    DMPWJW = new XRiTextField();
    OBJ_122_OBJ_122 = new JLabel();
    DMVJW = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("INTERVENTIONS");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- l_Bib ----
        l_Bib.setText("@V01F@");
        l_Bib.setFont(new Font("sansserif", Font.BOLD, 12));
        l_Bib.setName("l_Bib");

        //---- DMNOR ----
        DMNOR.setName("DMNOR");

        //---- OBJ_162_OBJ_162 ----
        OBJ_162_OBJ_162.setText("Intervention");
        OBJ_162_OBJ_162.setName("OBJ_162_OBJ_162");

        //---- INDNIN ----
        INDNIN.setComponentPopupMenu(BTD);
        INDNIN.setName("INDNIN");

        //---- DMDTEX ----
        DMDTEX.setName("DMDTEX");

        //---- OBJ_161_OBJ_161 ----
        OBJ_161_OBJ_161.setText("Heure");
        OBJ_161_OBJ_161.setName("OBJ_161_OBJ_161");

        //---- OBJ_155_OBJ_155 ----
        OBJ_155_OBJ_155.setText("Date");
        OBJ_155_OBJ_155.setName("OBJ_155_OBJ_155");

        //---- DMHREH ----
        DMHREH.setName("DMHREH");

        //---- DMHREM ----
        DMHREM.setName("DMHREM");

        //---- DMHRES ----
        DMHRES.setName("DMHRES");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
              .addGap(20, 20, 20)
              .addComponent(OBJ_162_OBJ_162, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNIN, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
              .addGap(20, 20, 20)
              .addComponent(OBJ_155_OBJ_155, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
              .addGap(1, 1, 1)
              .addComponent(DMDTEX, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addGap(30, 30, 30)
              .addComponent(OBJ_161_OBJ_161, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
              .addGap(4, 4, 4)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(DMHREH, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addComponent(DMHRES, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(DMHREM, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
              .addGap(8, 8, 8)
              .addComponent(DMNOR, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 198, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(7, 7, 7)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(9, 9, 9)
              .addComponent(OBJ_162_OBJ_162))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(INDNIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(9, 9, 9)
              .addComponent(OBJ_155_OBJ_155))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(DMDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(9, 9, 9)
              .addComponent(OBJ_161_OBJ_161))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(DMHREH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(DMHRES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(DMHREM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(DMNOR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_Fonctions)))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //======== OBJ_24 ========
      {
        OBJ_24.setName("OBJ_24");

        //======== OBJ_25 ========
        {
          OBJ_25.setName("OBJ_25");
          OBJ_25.setLayout(null);

          //======== OBJ_26 ========
          {
            OBJ_26.setBorder(new TitledBorder("SERIE M"));
            OBJ_26.setName("OBJ_26");
            OBJ_26.setLayout(null);

            //---- OBJ_34_OBJ_34 ----
            OBJ_34_OBJ_34.setText("Num\u00e9ros de version:");
            OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
            OBJ_26.add(OBJ_34_OBJ_34);
            OBJ_34_OBJ_34.setBounds(30, 40, 129, 15);

            //---- VS01 ----
            VS01.setComponentPopupMenu(BTD);
            VS01.setName("VS01");
            OBJ_26.add(VS01);
            VS01.setBounds(95, 65, 50, VS01.getPreferredSize().height);

            //---- VS02 ----
            VS02.setComponentPopupMenu(BTD);
            VS02.setName("VS02");
            OBJ_26.add(VS02);
            VS02.setBounds(231, 65, 50, VS02.getPreferredSize().height);

            //---- VS03 ----
            VS03.setComponentPopupMenu(BTD);
            VS03.setName("VS03");
            OBJ_26.add(VS03);
            VS03.setBounds(366, 65, 50, VS03.getPreferredSize().height);

            //---- VS04 ----
            VS04.setComponentPopupMenu(BTD);
            VS04.setName("VS04");
            OBJ_26.add(VS04);
            VS04.setBounds(505, 65, 50, VS04.getPreferredSize().height);

            //---- VS05 ----
            VS05.setComponentPopupMenu(BTD);
            VS05.setName("VS05");
            OBJ_26.add(VS05);
            VS05.setBounds(95, 95, 50, VS05.getPreferredSize().height);

            //---- VS06 ----
            VS06.setComponentPopupMenu(BTD);
            VS06.setName("VS06");
            OBJ_26.add(VS06);
            VS06.setBounds(231, 95, 50, VS06.getPreferredSize().height);

            //---- VS07 ----
            VS07.setComponentPopupMenu(BTD);
            VS07.setName("VS07");
            OBJ_26.add(VS07);
            VS07.setBounds(366, 95, 50, VS07.getPreferredSize().height);

            //---- VS08 ----
            VS08.setComponentPopupMenu(BTD);
            VS08.setName("VS08");
            OBJ_26.add(VS08);
            VS08.setBounds(505, 95, 50, VS08.getPreferredSize().height);

            //---- VS09 ----
            VS09.setComponentPopupMenu(BTD);
            VS09.setName("VS09");
            OBJ_26.add(VS09);
            VS09.setBounds(95, 125, 50, VS09.getPreferredSize().height);

            //---- VS10 ----
            VS10.setComponentPopupMenu(BTD);
            VS10.setName("VS10");
            OBJ_26.add(VS10);
            VS10.setBounds(231, 125, 50, VS10.getPreferredSize().height);

            //---- VS11 ----
            VS11.setComponentPopupMenu(BTD);
            VS11.setName("VS11");
            OBJ_26.add(VS11);
            VS11.setBounds(366, 125, 50, VS11.getPreferredSize().height);

            //---- VS12 ----
            VS12.setComponentPopupMenu(BTD);
            VS12.setName("VS12");
            OBJ_26.add(VS12);
            VS12.setBounds(505, 125, 50, VS12.getPreferredSize().height);

            //---- VS13 ----
            VS13.setComponentPopupMenu(BTD);
            VS13.setName("VS13");
            OBJ_26.add(VS13);
            VS13.setBounds(95, 155, 50, VS13.getPreferredSize().height);

            //---- VS14 ----
            VS14.setComponentPopupMenu(BTD);
            VS14.setName("VS14");
            OBJ_26.add(VS14);
            VS14.setBounds(231, 155, 50, VS14.getPreferredSize().height);

            //---- VS15 ----
            VS15.setComponentPopupMenu(BTD);
            VS15.setName("VS15");
            OBJ_26.add(VS15);
            VS15.setBounds(366, 155, 50, VS15.getPreferredSize().height);

            //---- VS16 ----
            VS16.setComponentPopupMenu(BTD);
            VS16.setName("VS16");
            OBJ_26.add(VS16);
            VS16.setBounds(505, 155, 50, VS16.getPreferredSize().height);

            //---- VS17 ----
            VS17.setComponentPopupMenu(BTD);
            VS17.setName("VS17");
            OBJ_26.add(VS17);
            VS17.setBounds(95, 185, 50, VS17.getPreferredSize().height);

            //---- VS18 ----
            VS18.setComponentPopupMenu(BTD);
            VS18.setName("VS18");
            OBJ_26.add(VS18);
            VS18.setBounds(231, 185, 50, VS18.getPreferredSize().height);

            //---- VS19 ----
            VS19.setComponentPopupMenu(BTD);
            VS19.setName("VS19");
            OBJ_26.add(VS19);
            VS19.setBounds(366, 185, 50, VS19.getPreferredSize().height);

            //---- VS20 ----
            VS20.setComponentPopupMenu(BTD);
            VS20.setName("VS20");
            OBJ_26.add(VS20);
            VS20.setBounds(505, 185, 50, VS20.getPreferredSize().height);

            //---- OBJ_35_OBJ_35 ----
            OBJ_35_OBJ_35.setText("EXPL");
            OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
            OBJ_26.add(OBJ_35_OBJ_35);
            OBJ_35_OBJ_35.setBounds(44, 69, 41, 20);

            //---- OBJ_37_OBJ_37 ----
            OBJ_37_OBJ_37.setText("T.E.L");
            OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
            OBJ_26.add(OBJ_37_OBJ_37);
            OBJ_37_OBJ_37.setBounds(181, 69, 41, 20);

            //---- OBJ_39_OBJ_39 ----
            OBJ_39_OBJ_39.setText("C.G.M");
            OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
            OBJ_26.add(OBJ_39_OBJ_39);
            OBJ_39_OBJ_39.setBounds(316, 69, 41, 20);

            //---- OBJ_41_OBJ_41 ----
            OBJ_41_OBJ_41.setText("T.B.M");
            OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
            OBJ_26.add(OBJ_41_OBJ_41);
            OBJ_41_OBJ_41.setBounds(455, 69, 41, 20);

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("G.T.M");
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
            OBJ_26.add(OBJ_43_OBJ_43);
            OBJ_43_OBJ_43.setBounds(44, 99, 41, 20);

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("G.L.F");
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
            OBJ_26.add(OBJ_45_OBJ_45);
            OBJ_45_OBJ_45.setBounds(181, 99, 41, 20);

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("G.E.M");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
            OBJ_26.add(OBJ_47_OBJ_47);
            OBJ_47_OBJ_47.setBounds(316, 99, 41, 20);

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("T.I.M");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
            OBJ_26.add(OBJ_49_OBJ_49);
            OBJ_49_OBJ_49.setBounds(455, 99, 41, 20);

            //---- OBJ_51_OBJ_51 ----
            OBJ_51_OBJ_51.setText("G.I.M");
            OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
            OBJ_26.add(OBJ_51_OBJ_51);
            OBJ_51_OBJ_51.setBounds(44, 129, 41, 20);

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("P.A.M");
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
            OBJ_26.add(OBJ_53_OBJ_53);
            OBJ_53_OBJ_53.setBounds(181, 159, 41, 20);

            //---- OBJ_55_OBJ_55 ----
            OBJ_55_OBJ_55.setText("G.S.M");
            OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");
            OBJ_26.add(OBJ_55_OBJ_55);
            OBJ_55_OBJ_55.setBounds(316, 129, 41, 20);

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("T.D.S");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
            OBJ_26.add(OBJ_57_OBJ_57);
            OBJ_57_OBJ_57.setBounds(455, 129, 41, 20);

            //---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("G.V.M");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");
            OBJ_26.add(OBJ_59_OBJ_59);
            OBJ_59_OBJ_59.setBounds(44, 159, 41, 20);

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("GVMX");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
            OBJ_26.add(OBJ_61_OBJ_61);
            OBJ_61_OBJ_61.setBounds(181, 189, 41, 20);

            //---- OBJ_63_OBJ_63 ----
            OBJ_63_OBJ_63.setText("T.R.M");
            OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
            OBJ_26.add(OBJ_63_OBJ_63);
            OBJ_63_OBJ_63.setBounds(316, 159, 41, 20);

            //---- OBJ_65_OBJ_65 ----
            OBJ_65_OBJ_65.setText("G.A.M");
            OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");
            OBJ_26.add(OBJ_65_OBJ_65);
            OBJ_65_OBJ_65.setBounds(455, 159, 41, 20);

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("G.P.M");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
            OBJ_26.add(OBJ_67_OBJ_67);
            OBJ_67_OBJ_67.setBounds(44, 189, 41, 20);

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("G.M.M");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
            OBJ_26.add(OBJ_69_OBJ_69);
            OBJ_69_OBJ_69.setBounds(181, 129, 41, 20);

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("P.R.M");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
            OBJ_26.add(OBJ_71_OBJ_71);
            OBJ_71_OBJ_71.setBounds(316, 189, 41, 20);

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("L.T.M");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
            OBJ_26.add(OBJ_73_OBJ_73);
            OBJ_73_OBJ_73.setBounds(455, 189, 41, 20);
          }
          OBJ_25.add(OBJ_26);
          OBJ_26.setBounds(30, 110, 600, 250);

          //======== OBJ_27 ========
          {
            OBJ_27.setBorder(new TitledBorder("Syst\u00e8me AS400"));
            OBJ_27.setName("OBJ_27");
            OBJ_27.setLayout(null);

            //---- DMSER ----
            DMSER.setComponentPopupMenu(BTD);
            DMSER.setName("DMSER");
            OBJ_27.add(DMSER);
            DMSER.setBounds(145, 35, 124, DMSER.getPreferredSize().height);

            //---- OBJ_32_OBJ_32 ----
            OBJ_32_OBJ_32.setText("N\u00b0 de version OS400");
            OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
            OBJ_27.add(OBJ_32_OBJ_32);
            OBJ_32_OBJ_32.setBounds(316, 41, 126, 17);

            //---- OBJ_30_OBJ_30 ----
            OBJ_30_OBJ_30.setText("N\u00b0 de s\u00e9rie AS400");
            OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
            OBJ_27.add(OBJ_30_OBJ_30);
            OBJ_30_OBJ_30.setBounds(27, 41, 111, 17);

            //---- DMVOS ----
            DMVOS.setComponentPopupMenu(BTD);
            DMVOS.setName("DMVOS");
            OBJ_27.add(DMVOS);
            DMVOS.setBounds(450, 35, 78, DMVOS.getPreferredSize().height);
          }
          OBJ_25.add(OBJ_27);
          OBJ_27.setBounds(30, 20, 600, 85);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_25.getComponentCount(); i++) {
              Rectangle bounds = OBJ_25.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_25.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_25.setMinimumSize(preferredSize);
            OBJ_25.setPreferredSize(preferredSize);
          }
        }
        OBJ_24.addTab("Informations d'installation", OBJ_25);

        //======== OBJ_75 ========
        {
          OBJ_75.setName("OBJ_75");
          OBJ_75.setLayout(null);

          //======== OBJ_82 ========
          {
            OBJ_82.setBorder(new TitledBorder("Informations sur la session"));
            OBJ_82.setName("OBJ_82");
            OBJ_82.setLayout(null);

            //---- DMLDA ----
            DMLDA.setName("DMLDA");
            OBJ_82.add(DMLDA);
            DMLDA.setBounds(120, 60, 439, DMLDA.getPreferredSize().height);

            //---- DMJWP ----
            DMJWP.setComponentPopupMenu(BTD);
            DMJWP.setName("DMJWP");
            OBJ_82.add(DMJWP);
            DMJWP.setBounds(120, 90, 439, DMJWP.getPreferredSize().height);

            //---- DMUSR ----
            DMUSR.setComponentPopupMenu(BTD);
            DMUSR.setName("DMUSR");
            OBJ_82.add(DMUSR);
            DMUSR.setBounds(120, 30, 105, DMUSR.getPreferredSize().height);

            //---- DMECR ----
            DMECR.setComponentPopupMenu(BTD);
            DMECR.setName("DMECR");
            OBJ_82.add(DMECR);
            DMECR.setBounds(454, 30, 105, DMECR.getPreferredSize().height);

            //---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("Panel J-Walk");
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
            OBJ_82.add(OBJ_90_OBJ_90);
            OBJ_90_OBJ_90.setBounds(25, 94, 101, 20);

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("Ecran");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
            OBJ_82.add(OBJ_86_OBJ_86);
            OBJ_86_OBJ_86.setBounds(410, 34, 56, 20);

            //---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("*LDA");
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
            OBJ_82.add(OBJ_88_OBJ_88);
            OBJ_88_OBJ_88.setBounds(25, 64, 52, 20);

            //---- OBJ_84_OBJ_84 ----
            OBJ_84_OBJ_84.setText("Profil");
            OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");
            OBJ_82.add(OBJ_84_OBJ_84);
            OBJ_84_OBJ_84.setBounds(25, 34, 51, 20);
          }
          OBJ_75.add(OBJ_82);
          OBJ_82.setBounds(15, 15, 665, 140);

          //======== OBJ_80 ========
          {
            OBJ_80.setBorder(new TitledBorder("Localisation dans S\u00e9rie M"));
            OBJ_80.setName("OBJ_80");
            OBJ_80.setLayout(null);

            //---- DMMIBM ----
            DMMIBM.setComponentPopupMenu(BTD);
            DMMIBM.setName("DMMIBM");
            OBJ_80.add(DMMIBM);
            DMMIBM.setBounds(120, 120, 196, DMMIBM.getPreferredSize().height);

            //---- DMOPT ----
            DMOPT.setComponentPopupMenu(BTD);
            DMOPT.setName("DMOPT");
            OBJ_80.add(DMOPT);
            DMOPT.setBounds(120, 150, 196, DMOPT.getPreferredSize().height);

            //---- DMPGM ----
            DMPGM.setComponentPopupMenu(BTD);
            DMPGM.setName("DMPGM");
            OBJ_80.add(DMPGM);
            DMPGM.setBounds(120, 90, 121, DMPGM.getPreferredSize().height);

            //---- DMMNU ----
            DMMNU.setComponentPopupMenu(BTD);
            DMMNU.setName("DMMNU");
            OBJ_80.add(DMMNU);
            DMMNU.setBounds(120, 60, 103, DMMNU.getPreferredSize().height);

            //---- OBJ_98_OBJ_98 ----
            OBJ_98_OBJ_98.setText("Point de menu");
            OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
            OBJ_80.add(OBJ_98_OBJ_98);
            OBJ_98_OBJ_98.setBounds(25, 64, 88, 20);

            //---- OBJ_113_OBJ_113 ----
            OBJ_113_OBJ_113.setText("Message IBM");
            OBJ_113_OBJ_113.setName("OBJ_113_OBJ_113");
            OBJ_80.add(OBJ_113_OBJ_113);
            OBJ_113_OBJ_113.setBounds(25, 124, 86, 20);

            //---- OBJ_103_OBJ_103 ----
            OBJ_103_OBJ_103.setText("Programme");
            OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");
            OBJ_80.add(OBJ_103_OBJ_103);
            OBJ_103_OBJ_103.setBounds(25, 94, 74, 20);

            //---- DMMOD ----
            DMMOD.setComponentPopupMenu(BTD);
            DMMOD.setName("DMMOD");
            OBJ_80.add(DMMOD);
            DMMOD.setBounds(120, 30, 58, DMMOD.getPreferredSize().height);

            //---- OBJ_115_OBJ_115 ----
            OBJ_115_OBJ_115.setText("Options");
            OBJ_115_OBJ_115.setName("OBJ_115_OBJ_115");
            OBJ_80.add(OBJ_115_OBJ_115);
            OBJ_115_OBJ_115.setBounds(25, 154, 55, 20);

            //---- OBJ_92_OBJ_92 ----
            OBJ_92_OBJ_92.setText("Module");
            OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");
            OBJ_80.add(OBJ_92_OBJ_92);
            OBJ_92_OBJ_92.setBounds(25, 34, 48, 20);

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("Anosys");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");
            OBJ_80.add(OBJ_108_OBJ_108);
            OBJ_108_OBJ_108.setBounds(230, 34, 48, 20);

            //---- DMANO ----
            DMANO.setComponentPopupMenu(BTD);
            DMANO.setName("DMANO");
            OBJ_80.add(DMANO);
            DMANO.setBounds(281, 30, 35, DMANO.getPreferredSize().height);
          }
          OBJ_75.add(OBJ_80);
          OBJ_80.setBounds(15, 155, 345, 195);

          //======== OBJ_78 ========
          {
            OBJ_78.setBorder(new TitledBorder("Localisation du fichier"));
            OBJ_78.setName("OBJ_78");
            OBJ_78.setLayout(null);

            //---- DMID1 ----
            DMID1.setComponentPopupMenu(BTD);
            DMID1.setName("DMID1");
            OBJ_78.add(DMID1);
            DMID1.setBounds(125, 60, 160, DMID1.getPreferredSize().height);

            //---- DMID2 ----
            DMID2.setComponentPopupMenu(BTD);
            DMID2.setName("DMID2");
            OBJ_78.add(DMID2);
            DMID2.setBounds(125, 90, 160, DMID2.getPreferredSize().height);

            //---- DMID3 ----
            DMID3.setComponentPopupMenu(BTD);
            DMID3.setName("DMID3");
            OBJ_78.add(DMID3);
            DMID3.setBounds(125, 120, 160, DMID3.getPreferredSize().height);

            //---- OBJ_96_OBJ_96 ----
            OBJ_96_OBJ_96.setText("Etablissement");
            OBJ_96_OBJ_96.setName("OBJ_96_OBJ_96");
            OBJ_78.add(OBJ_96_OBJ_96);
            OBJ_96_OBJ_96.setBounds(157, 34, 88, 20);

            //---- OBJ_94_OBJ_94 ----
            OBJ_94_OBJ_94.setText("Biblioth\u00e8que");
            OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");
            OBJ_78.add(OBJ_94_OBJ_94);
            OBJ_94_OBJ_94.setBounds(25, 34, 78, 20);

            //---- OBJ_100_OBJ_100 ----
            OBJ_100_OBJ_100.setText("Indicatif 1");
            OBJ_100_OBJ_100.setName("OBJ_100_OBJ_100");
            OBJ_78.add(OBJ_100_OBJ_100);
            OBJ_100_OBJ_100.setBounds(25, 64, 76, 20);

            //---- OBJ_105_OBJ_105 ----
            OBJ_105_OBJ_105.setText("Indicatif 2");
            OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");
            OBJ_78.add(OBJ_105_OBJ_105);
            OBJ_105_OBJ_105.setBounds(25, 94, 76, 20);

            //---- OBJ_110_OBJ_110 ----
            OBJ_110_OBJ_110.setText("Indicatif 3");
            OBJ_110_OBJ_110.setName("OBJ_110_OBJ_110");
            OBJ_78.add(OBJ_110_OBJ_110);
            OBJ_110_OBJ_110.setBounds(25, 124, 76, 20);

            //---- DMBIB ----
            DMBIB.setComponentPopupMenu(BTD);
            DMBIB.setName("DMBIB");
            OBJ_78.add(DMBIB);
            DMBIB.setBounds(99, 30, 50, DMBIB.getPreferredSize().height);

            //---- DMETBC ----
            DMETBC.setComponentPopupMenu(BTD);
            DMETBC.setName("DMETBC");
            OBJ_78.add(DMETBC);
            DMETBC.setBounds(245, 30, 40, DMETBC.getPreferredSize().height);

            //---- DMTID1 ----
            DMTID1.setComponentPopupMenu(BTD);
            DMTID1.setName("DMTID1");
            OBJ_78.add(DMTID1);
            DMTID1.setBounds(99, 60, 20, DMTID1.getPreferredSize().height);

            //---- DMTID2 ----
            DMTID2.setComponentPopupMenu(BTD);
            DMTID2.setName("DMTID2");
            OBJ_78.add(DMTID2);
            DMTID2.setBounds(98, 90, 20, DMTID2.getPreferredSize().height);

            //---- DMTID3 ----
            DMTID3.setComponentPopupMenu(BTD);
            DMTID3.setName("DMTID3");
            OBJ_78.add(DMTID3);
            DMTID3.setBounds(98, 120, 20, DMTID3.getPreferredSize().height);
          }
          OBJ_75.add(OBJ_78);
          OBJ_78.setBounds(360, 155, 320, 195);

          //======== OBJ_76 ========
          {
            OBJ_76.setBorder(new TitledBorder("Version graphique"));
            OBJ_76.setName("OBJ_76");
            OBJ_76.setLayout(null);

            //---- DMHTP ----
            DMHTP.setText("QTMHHTTP actif");
            DMHTP.setComponentPopupMenu(BTD);
            DMHTP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DMHTP.setName("DMHTP");
            OBJ_76.add(DMHTP);
            DMHTP.setBounds(25, 34, 133, 20);

            //---- OBJ_120_OBJ_120 ----
            OBJ_120_OBJ_120.setText("Version de JWalk");
            OBJ_120_OBJ_120.setName("OBJ_120_OBJ_120");
            OBJ_76.add(OBJ_120_OBJ_120);
            OBJ_120_OBJ_120.setBounds(25, 64, 109, 20);

            //---- OBJ_124_OBJ_124 ----
            OBJ_124_OBJ_124.setText("N\u00b0 port de licence");
            OBJ_124_OBJ_124.setName("OBJ_124_OBJ_124");
            OBJ_76.add(OBJ_124_OBJ_124);
            OBJ_124_OBJ_124.setBounds(236, 34, 108, 20);

            //---- DMIP ----
            DMIP.setComponentPopupMenu(BTD);
            DMIP.setName("DMIP");
            OBJ_76.add(DMIP);
            DMIP.setBounds(510, 30, 100, DMIP.getPreferredSize().height);

            //---- OBJ_126_OBJ_126 ----
            OBJ_126_OBJ_126.setText("N\u00b0 port Web");
            OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");
            OBJ_76.add(OBJ_126_OBJ_126);
            OBJ_126_OBJ_126.setBounds(235, 64, 93, 20);

            //---- OBJ_118_OBJ_118 ----
            OBJ_118_OBJ_118.setText("Adresse IP");
            OBJ_118_OBJ_118.setName("OBJ_118_OBJ_118");
            OBJ_76.add(OBJ_118_OBJ_118);
            OBJ_118_OBJ_118.setBounds(435, 34, 69, 20);

            //---- DMDTJW ----
            DMDTJW.setComponentPopupMenu(BTD);
            DMDTJW.setName("DMDTJW");
            OBJ_76.add(DMDTJW);
            DMDTJW.setBounds(510, 60, 115, DMDTJW.getPreferredSize().height);

            //---- DMPLJW ----
            DMPLJW.setComponentPopupMenu(BTD);
            DMPLJW.setName("DMPLJW");
            OBJ_76.add(DMPLJW);
            DMPLJW.setBounds(350, 30, 60, DMPLJW.getPreferredSize().height);

            //---- DMPWJW ----
            DMPWJW.setComponentPopupMenu(BTD);
            DMPWJW.setName("DMPWJW");
            OBJ_76.add(DMPWJW);
            DMPWJW.setBounds(350, 60, 60, DMPWJW.getPreferredSize().height);

            //---- OBJ_122_OBJ_122 ----
            OBJ_122_OBJ_122.setText("Date");
            OBJ_122_OBJ_122.setName("OBJ_122_OBJ_122");
            OBJ_76.add(OBJ_122_OBJ_122);
            OBJ_122_OBJ_122.setBounds(435, 65, 50, 20);

            //---- DMVJW ----
            DMVJW.setComponentPopupMenu(BTD);
            DMVJW.setName("DMVJW");
            OBJ_76.add(DMVJW);
            DMVJW.setBounds(145, 60, 50, DMVJW.getPreferredSize().height);
          }
          OBJ_75.add(OBJ_76);
          OBJ_76.setBounds(15, 350, 665, 110);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_75.getComponentCount(); i++) {
              Rectangle bounds = OBJ_75.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_75.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_75.setMinimumSize(preferredSize);
            OBJ_75.setPreferredSize(preferredSize);
          }
        }
        OBJ_24.addTab("Localisation du probl\u00e8me", OBJ_75);
      }

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(32, 32, 32)
            .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE)
            .addContainerGap(273, Short.MAX_VALUE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(26, 26, 26)
            .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)
            .addContainerGap(30, Short.MAX_VALUE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_Bib;
  private XRiTextField DMNOR;
  private JLabel OBJ_162_OBJ_162;
  private XRiTextField INDNIN;
  private XRiCalendrier DMDTEX;
  private JLabel OBJ_161_OBJ_161;
  private JLabel OBJ_155_OBJ_155;
  private XRiTextField DMHREH;
  private XRiTextField DMHREM;
  private XRiTextField DMHRES;
  private JPanel P_Centre;
  private JTabbedPane OBJ_24;
  private JPanel OBJ_25;
  private JPanel OBJ_26;
  private JLabel OBJ_34_OBJ_34;
  private XRiTextField VS01;
  private XRiTextField VS02;
  private XRiTextField VS03;
  private XRiTextField VS04;
  private XRiTextField VS05;
  private XRiTextField VS06;
  private XRiTextField VS07;
  private XRiTextField VS08;
  private XRiTextField VS09;
  private XRiTextField VS10;
  private XRiTextField VS11;
  private XRiTextField VS12;
  private XRiTextField VS13;
  private XRiTextField VS14;
  private XRiTextField VS15;
  private XRiTextField VS16;
  private XRiTextField VS17;
  private XRiTextField VS18;
  private XRiTextField VS19;
  private XRiTextField VS20;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_37_OBJ_37;
  private JLabel OBJ_39_OBJ_39;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_55_OBJ_55;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_63_OBJ_63;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_69_OBJ_69;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_73_OBJ_73;
  private JPanel OBJ_27;
  private XRiTextField DMSER;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_30_OBJ_30;
  private XRiTextField DMVOS;
  private JPanel OBJ_75;
  private JPanel OBJ_82;
  private XRiTextField DMLDA;
  private XRiTextField DMJWP;
  private XRiTextField DMUSR;
  private XRiTextField DMECR;
  private JLabel OBJ_90_OBJ_90;
  private JLabel OBJ_86_OBJ_86;
  private JLabel OBJ_88_OBJ_88;
  private JLabel OBJ_84_OBJ_84;
  private JPanel OBJ_80;
  private XRiTextField DMMIBM;
  private XRiTextField DMOPT;
  private XRiTextField DMPGM;
  private XRiTextField DMMNU;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_113_OBJ_113;
  private JLabel OBJ_103_OBJ_103;
  private XRiTextField DMMOD;
  private JLabel OBJ_115_OBJ_115;
  private JLabel OBJ_92_OBJ_92;
  private JLabel OBJ_108_OBJ_108;
  private XRiTextField DMANO;
  private JPanel OBJ_78;
  private XRiTextField DMID1;
  private XRiTextField DMID2;
  private XRiTextField DMID3;
  private JLabel OBJ_96_OBJ_96;
  private JLabel OBJ_94_OBJ_94;
  private JLabel OBJ_100_OBJ_100;
  private JLabel OBJ_105_OBJ_105;
  private JLabel OBJ_110_OBJ_110;
  private XRiTextField DMBIB;
  private XRiTextField DMETBC;
  private XRiTextField DMTID1;
  private XRiTextField DMTID2;
  private XRiTextField DMTID3;
  private JPanel OBJ_76;
  private XRiCheckBox DMHTP;
  private JLabel OBJ_120_OBJ_120;
  private JLabel OBJ_124_OBJ_124;
  private XRiTextField DMIP;
  private JLabel OBJ_126_OBJ_126;
  private JLabel OBJ_118_OBJ_118;
  private XRiCalendrier DMDTJW;
  private XRiTextField DMPLJW;
  private XRiTextField DMPWJW;
  private JLabel OBJ_122_OBJ_122;
  private XRiTextField DMVJW;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
