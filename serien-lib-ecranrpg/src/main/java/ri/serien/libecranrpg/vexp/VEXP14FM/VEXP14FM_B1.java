
package ri.serien.libecranrpg.vexp.VEXP14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP14FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP14FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ARR.setValeursSelection("OUI", "NON");
    NBBIB.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    SSD.setEnabled(lexique.isPresent("SSD"));
    MND.setEnabled(lexique.isPresent("MND"));
    HHD.setEnabled(lexique.isPresent("HHD"));
    AAD.setEnabled(lexique.isPresent("AAD"));
    MMD.setEnabled(lexique.isPresent("MMD"));
    JJD.setEnabled(lexique.isPresent("JJD"));
    MM.setEnabled(lexique.isPresent("MM"));
    HH.setEnabled(lexique.isPresent("HH"));
    NVOL.setEnabled(lexique.isPresent("NVOL"));
    DENS.setEnabled(lexique.isPresent("DENS"));
    TGTRLS.setEnabled(lexique.isPresent("TGTRLS"));
    UNITE.setEnabled(lexique.isPresent("UNITE"));
    // ARR.setEnabled( lexique.isPresent("ARR"));
    // ARR.setSelected(lexique.HostFieldGetData("ARR").equalsIgnoreCase("OUI"));
    // NBBIB.setVisible( lexique.isPresent("NBBIB"));
    // NBBIB.setSelected(lexique.HostFieldGetData("NBBIB").equalsIgnoreCase("**"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (ARR.isSelected())
    // lexique.HostFieldPutData("ARR", 0, "OUI");
    // else
    // lexique.HostFieldPutData("ARR", 0, "NON");
    // if (NBBIB.isSelected())
    // lexique.HostFieldPutData("NBBIB", 0, "**");
    // else
    // lexique.HostFieldPutData("NBBIB", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vexp14"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_28 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_33 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_73 = new JXTitledSeparator();
    OBJ_75 = new JXTitledSeparator();
    OBJ_30 = new JLabel();
    OBJ_32 = new JLabel();
    NBBIB = new XRiCheckBox();
    OBJ_29 = new JLabel();
    ARR = new XRiCheckBox();
    OBJ_39 = new JLabel();
    OBJ_69 = new JLabel();
    UNITE = new XRiTextField();
    TGTRLS = new XRiTextField();
    NVOL = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_76 = new JLabel();
    HH = new XRiTextField();
    MM = new XRiTextField();
    JJD = new XRiTextField();
    MMD = new XRiTextField();
    AAD = new XRiTextField();
    HHD = new XRiTextField();
    MND = new XRiTextField();
    SSD = new XRiTextField();
    OBJ_79 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_38 = new JLabel();
    DENS = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Sauvegarde de nuit S\u00e9rie N");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_28 ----
          OBJ_28.setText("Vous venez de demander la Sauvegarde de Nuit de vos Fichiers S\u00e9rie M");
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28.setName("OBJ_28");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 621, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(710, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_33 ----
          OBJ_33.setText("Ne vous servez jamais de la derni\u00e8re sauvegarde, utilisez plusieurs Cartouches par s\u00e9curit\u00e9");
          OBJ_33.setName("OBJ_33");

          //---- OBJ_31 ----
          OBJ_31.setText("Elle doit \u00eatre archiv\u00e9e dans un coffre ou \u00e0 l'ext\u00e9rieur de vos locaux et ne jamais \u00eatre r\u00e9utilis\u00e9e");
          OBJ_31.setName("OBJ_31");

          //---- OBJ_73 ----
          OBJ_73.setTitle("SAUVEGARDE AVANT CLOTURE");
          OBJ_73.setName("OBJ_73");

          //---- OBJ_75 ----
          OBJ_75.setTitle("INFORMATIONS SUR LA SAUVEGARDE");
          OBJ_75.setName("OBJ_75");

          //---- OBJ_30 ----
          OBJ_30.setText("Inscrivez sur le catalogue : SAUVEGARDE AVANT ARRETE/MOIS/MODULE");
          OBJ_30.setName("OBJ_30");

          //---- OBJ_32 ----
          OBJ_32.setText("Vous devez au pr\u00e9alable vous munir d'une Cartouche initialisable");
          OBJ_32.setName("OBJ_32");

          //---- NBBIB ----
          NBBIB.setText("Sauvegarde des Fichiers de vos autres biblioth\u00e8ques utilisateurs ?");
          NBBIB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NBBIB.setName("NBBIB");

          //---- OBJ_29 ----
          OBJ_29.setText("Elle doit \u00eatre conserv\u00e9e 3 mois minimum et si possible tout l'Exercice");
          OBJ_29.setName("OBJ_29");

          //---- ARR ----
          ARR.setText("Arr\u00eat de la machine en Fin de Sauvegarde ?");
          ARR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARR.setName("ARR");

          //---- OBJ_39 ----
          OBJ_39.setText("Saisir le NOM du VOLUME et l'UNITE");
          OBJ_39.setName("OBJ_39");

          //---- OBJ_69 ----
          OBJ_69.setText("Heure de D\u00e9but de la Sauvegarde");
          OBJ_69.setName("OBJ_69");

          //---- UNITE ----
          UNITE.setComponentPopupMenu(BTD);
          UNITE.setName("UNITE");

          //---- TGTRLS ----
          TGTRLS.setComponentPopupMenu(BTD);
          TGTRLS.setName("TGTRLS");

          //---- NVOL ----
          NVOL.setComponentPopupMenu(BTD);
          NVOL.setName("NVOL");

          //---- OBJ_36 ----
          OBJ_36.setText("Unit\u00e9/Bande");
          OBJ_36.setName("OBJ_36");

          //---- OBJ_35 ----
          OBJ_35.setText("Volume");
          OBJ_35.setName("OBJ_35");

          //---- OBJ_37 ----
          OBJ_37.setText("Version OS");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_68 ----
          OBJ_68.setText("HH");
          OBJ_68.setName("OBJ_68");

          //---- OBJ_76 ----
          OBJ_76.setText("MM");
          OBJ_76.setName("OBJ_76");

          //---- HH ----
          HH.setComponentPopupMenu(BTD);
          HH.setName("HH");

          //---- MM ----
          MM.setComponentPopupMenu(BTD);
          MM.setName("MM");

          //---- JJD ----
          JJD.setComponentPopupMenu(BTD);
          JJD.setName("JJD");

          //---- MMD ----
          MMD.setComponentPopupMenu(BTD);
          MMD.setName("MMD");

          //---- AAD ----
          AAD.setComponentPopupMenu(BTD);
          AAD.setName("AAD");

          //---- HHD ----
          HHD.setComponentPopupMenu(BTD);
          HHD.setName("HHD");

          //---- MND ----
          MND.setComponentPopupMenu(BTD);
          MND.setName("MND");

          //---- SSD ----
          SSD.setComponentPopupMenu(BTD);
          SSD.setName("SSD");

          //---- OBJ_79 ----
          OBJ_79.setText("MM");
          OBJ_79.setName("OBJ_79");

          //---- OBJ_82 ----
          OBJ_82.setText("MN");
          OBJ_82.setName("OBJ_82");

          //---- OBJ_81 ----
          OBJ_81.setText("HH");
          OBJ_81.setName("OBJ_81");

          //---- OBJ_78 ----
          OBJ_78.setText("JJ");
          OBJ_78.setName("OBJ_78");

          //---- OBJ_80 ----
          OBJ_80.setText("AA");
          OBJ_80.setName("OBJ_80");

          //---- OBJ_83 ----
          OBJ_83.setText("SS");
          OBJ_83.setName("OBJ_83");

          //---- OBJ_77 ----
          OBJ_77.setText("\u00e0");
          OBJ_77.setName("OBJ_77");

          //---- OBJ_38 ----
          OBJ_38.setText("Densit\u00e9");
          OBJ_38.setName("OBJ_38");

          //---- DENS ----
          DENS.setComponentPopupMenu(BTD);
          DENS.setName("DENS");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 472, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 655, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 423, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 562, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 655, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(289, 289, 289)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(NVOL, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(TGTRLS, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(DENS, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(289, 289, 289)
                .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(HH, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(MM, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(ARR, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(JJD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                  .addComponent(MMD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(AAD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                  .addComponent(HHD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                  .addComponent(MND, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SSD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(NBBIB, GroupLayout.PREFERRED_SIZE, 423, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(NVOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(TGTRLS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DENS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(HH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(MM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(ARR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_78)
                    .addGap(4, 4, 4)
                    .addComponent(JJD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_79)
                    .addGap(4, 4, 4)
                    .addComponent(MMD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_80)
                    .addGap(4, 4, 4)
                    .addComponent(AAD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_81)
                    .addGap(4, 4, 4)
                    .addComponent(HHD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_82)
                    .addGap(4, 4, 4)
                    .addComponent(MND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_83)
                    .addGap(4, 4, 4)
                    .addComponent(SSD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addComponent(NBBIB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_28;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_33;
  private JLabel OBJ_31;
  private JXTitledSeparator OBJ_73;
  private JXTitledSeparator OBJ_75;
  private JLabel OBJ_30;
  private JLabel OBJ_32;
  private XRiCheckBox NBBIB;
  private JLabel OBJ_29;
  private XRiCheckBox ARR;
  private JLabel OBJ_39;
  private JLabel OBJ_69;
  private XRiTextField UNITE;
  private XRiTextField TGTRLS;
  private XRiTextField NVOL;
  private JLabel OBJ_36;
  private JLabel OBJ_35;
  private JLabel OBJ_37;
  private JLabel OBJ_68;
  private JLabel OBJ_76;
  private XRiTextField HH;
  private XRiTextField MM;
  private XRiTextField JJD;
  private XRiTextField MMD;
  private XRiTextField AAD;
  private XRiTextField HHD;
  private XRiTextField MND;
  private XRiTextField SSD;
  private JLabel OBJ_79;
  private JLabel OBJ_82;
  private JLabel OBJ_81;
  private JLabel OBJ_78;
  private JLabel OBJ_80;
  private JLabel OBJ_83;
  private JLabel OBJ_77;
  private JLabel OBJ_38;
  private XRiTextField DENS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
