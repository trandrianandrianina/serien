
package ri.serien.libecranrpg.vexp.VEXP25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP25FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXP25FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    OBJ_19.setIcon(lexique.chargerImage("images/internet.png", true));
    OBJ_20.setIcon(lexique.chargerImage("images/internet.png", true));
    OBJ_21.setIcon(lexique.chargerImage("images/internet.png", true));
    OBJ_22.setIcon(lexique.chargerImage("images/internet.png", true));
    OBJ_23.setIcon(lexique.chargerImage("images/internet.png", true));
    
    // Titre
    setTitle("Adresses des sites internet");
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(SUAD1.getText());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(SUAD2.getText());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(SUAD3.getText());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(SUAD4.getText());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(SUAD5.getText());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SUAD1 = new XRiTextField();
    SUAD2 = new XRiTextField();
    SUAD3 = new XRiTextField();
    OBJ_19 = new JButton();
    OBJ_20 = new JButton();
    OBJ_21 = new JButton();
    SUAD4 = new XRiTextField();
    SUAD5 = new XRiTextField();
    OBJ_22 = new JButton();
    OBJ_23 = new JButton();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(925, 225));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 90));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 90));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- SUAD1 ----
          SUAD1.setComponentPopupMenu(BTD);
          SUAD1.setName("SUAD1");
          p_recup.add(SUAD1);
          SUAD1.setBounds(50, 18, 675, SUAD1.getPreferredSize().height);

          //---- SUAD2 ----
          SUAD2.setComponentPopupMenu(BTD);
          SUAD2.setName("SUAD2");
          p_recup.add(SUAD2);
          SUAD2.setBounds(50, 53, 675, SUAD2.getPreferredSize().height);

          //---- SUAD3 ----
          SUAD3.setComponentPopupMenu(BTD);
          SUAD3.setName("SUAD3");
          p_recup.add(SUAD3);
          SUAD3.setBounds(50, 88, 675, SUAD3.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("");
          OBJ_19.setToolTipText("Appel du site");
          OBJ_19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_19.setName("OBJ_19");
          OBJ_19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_19ActionPerformed(e);
            }
          });
          p_recup.add(OBJ_19);
          OBJ_19.setBounds(9, 16, 32, 32);

          //---- OBJ_20 ----
          OBJ_20.setText("");
          OBJ_20.setToolTipText("Appel du site");
          OBJ_20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_20.setName("OBJ_20");
          OBJ_20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_20ActionPerformed(e);
            }
          });
          p_recup.add(OBJ_20);
          OBJ_20.setBounds(9, 51, 32, 32);

          //---- OBJ_21 ----
          OBJ_21.setText("");
          OBJ_21.setToolTipText("Appel du site");
          OBJ_21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_21.setName("OBJ_21");
          OBJ_21.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_21ActionPerformed(e);
            }
          });
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(9, 86, 32, 32);

          //---- SUAD4 ----
          SUAD4.setComponentPopupMenu(BTD);
          SUAD4.setName("SUAD4");
          p_recup.add(SUAD4);
          SUAD4.setBounds(50, 123, 675, SUAD4.getPreferredSize().height);

          //---- SUAD5 ----
          SUAD5.setComponentPopupMenu(BTD);
          SUAD5.setName("SUAD5");
          p_recup.add(SUAD5);
          SUAD5.setBounds(50, 158, 675, SUAD5.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("");
          OBJ_22.setToolTipText("Appel du site");
          OBJ_22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_22.setName("OBJ_22");
          OBJ_22.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_22ActionPerformed(e);
            }
          });
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(9, 121, 32, 32);

          //---- OBJ_23 ----
          OBJ_23.setText("");
          OBJ_23.setToolTipText("Appel du site");
          OBJ_23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_23.setName("OBJ_23");
          OBJ_23.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_23ActionPerformed(e);
            }
          });
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(9, 156, 32, 32);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField SUAD1;
  private XRiTextField SUAD2;
  private XRiTextField SUAD3;
  private JButton OBJ_19;
  private JButton OBJ_20;
  private JButton OBJ_21;
  private XRiTextField SUAD4;
  private XRiTextField SUAD5;
  private JButton OBJ_22;
  private JButton OBJ_23;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
