
package ri.serien.libecranrpg.vexp.VEXP01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP01FX_TG extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TGSNS_Value = { "D", "C", };
  
  public VEXP01FX_TG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TGSNS.setValeurs(TGSNS_Value, null);
    TGN12.setValeursSelection("1", " ");
    TGN11.setValeursSelection("1", " ");
    TGN10.setValeursSelection("1", " ");
    TGN9.setValeursSelection("1", " ");
    TGN8.setValeursSelection("1", " ");
    TGN7.setValeursSelection("1", " ");
    TGN6.setValeursSelection("1", " ");
    TGN5.setValeursSelection("1", " ");
    TGN4.setValeursSelection("1", " ");
    TGN3.setValeursSelection("1", " ");
    TGN2.setValeursSelection("1", " ");
    TGN1.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // TGSNS.setVisible( lexique.isPresent("TGSNS"));
    TGLIB.setVisible(lexique.isPresent("TGLIB"));
    // TGN1.setSelected(lexique.HostFieldGetData("TGN1").equals("1"));
    // TGN2.setSelected(lexique.HostFieldGetData("TGN2").equals("1"));
    // TGN3.setSelected(lexique.HostFieldGetData("TGN3").equals("1"));
    // TGN4.setSelected(lexique.HostFieldGetData("TGN4").equals("1"));
    // TGN5.setSelected(lexique.HostFieldGetData("TGN5").equals("1"));
    // TGN6.setSelected(lexique.HostFieldGetData("TGN6").equals("1"));
    // TGN7.setSelected(lexique.HostFieldGetData("TGN7").equals("1"));
    // TGN8.setSelected(lexique.HostFieldGetData("TGN8").equals("1"));
    TGN8.setSelected(lexique.HostFieldGetData("TGN9").equals("1"));
    // TGN10.setSelected(lexique.HostFieldGetData("TGN10").equals("1"));
    // TGN11.setSelected(lexique.HostFieldGetData("TGN11").equals("1"));
    // TGN12.setSelected(lexique.HostFieldGetData("TGN12").equals("1"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("TGSNS", 0, TGSNS_Value[TGSNS.getSelectedIndex()]);
    
    // if (TGN1.isSelected())
    // lexique.HostFieldPutData("TGN1", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN1", 0, " ");
    // if (TGN2.isSelected())
    // lexique.HostFieldPutData("TGN2", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN2", 0, " ");
    // if (TGN3.isSelected())
    // lexique.HostFieldPutData("TGN3", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN3", 0, " ");
    // if (TGN4.isSelected())
    // lexique.HostFieldPutData("TGN4", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN4", 0, " ");
    // if (TGN5.isSelected())
    // lexique.HostFieldPutData("TGN5", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN5", 0, " ");
    // if (TGN6.isSelected())
    // lexique.HostFieldPutData("TGN6", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN6", 0, " ");
    // if (TGN7.isSelected())
    // lexique.HostFieldPutData("TGN7", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN7", 0, " ");
    // if (TGN8.isSelected())
    // lexique.HostFieldPutData("TGN8", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN8", 0, " ");
    // if (TGN9.isSelected())
    // lexique.HostFieldPutData("TGN9", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN9", 0, " ");
    // if (TGN10.isSelected())
    // lexique.HostFieldPutData("TGN10", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN10", 0, " ");
    // if (TGN11.isSelected())
    // lexique.HostFieldPutData("TGN11", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN11", 0, " ");
    // if (TGN12.isSelected())
    // lexique.HostFieldPutData("TGN12", 0, "1");
    // else
    // lexique.HostFieldPutData("TGN12", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel3 = new JPanel();
    OBJ_55 = new JLabel();
    TGLIB = new XRiTextField();
    OBJ_68 = new JLabel();
    TGSNS = new XRiComboBox();
    OBJ_69 = new JXTitledSeparator();
    label1 = new JLabel();
    TGD1 = new XRiTextField();
    TGD2 = new XRiTextField();
    TGD3 = new XRiTextField();
    TGD4 = new XRiTextField();
    TGD5 = new XRiTextField();
    TGD6 = new XRiTextField();
    TGD7 = new XRiTextField();
    TGD8 = new XRiTextField();
    TGD9 = new XRiTextField();
    TGD10 = new XRiTextField();
    TGD11 = new XRiTextField();
    TGD12 = new XRiTextField();
    label2 = new JLabel();
    TGF1 = new XRiTextField();
    TGF2 = new XRiTextField();
    TGF3 = new XRiTextField();
    TGF4 = new XRiTextField();
    TGF5 = new XRiTextField();
    TGF6 = new XRiTextField();
    TGF7 = new XRiTextField();
    TGF8 = new XRiTextField();
    TGF9 = new XRiTextField();
    TGF10 = new XRiTextField();
    TGF11 = new XRiTextField();
    TGF12 = new XRiTextField();
    label3 = new JLabel();
    TGN1 = new XRiCheckBox();
    TGN2 = new XRiCheckBox();
    TGN3 = new XRiCheckBox();
    TGN4 = new XRiCheckBox();
    TGN5 = new XRiCheckBox();
    TGN6 = new XRiCheckBox();
    TGN7 = new XRiCheckBox();
    TGN8 = new XRiCheckBox();
    TGN9 = new XRiCheckBox();
    TGN10 = new XRiCheckBox();
    TGN11 = new XRiCheckBox();
    TGN12 = new XRiCheckBox();
    label4 = new JLabel();
    TGL1 = new XRiTextField();
    TGL2 = new XRiTextField();
    TGL3 = new XRiTextField();
    TGL4 = new XRiTextField();
    TGL5 = new XRiTextField();
    TGL6 = new XRiTextField();
    TGL7 = new XRiTextField();
    TGL8 = new XRiTextField();
    TGL9 = new XRiTextField();
    TGL10 = new XRiTextField();
    TGL11 = new XRiTextField();
    TGL12 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de l'exploitation");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Tableau bord G\u00e9n\u00e9ral");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel3 ========
            {
              panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel3.setOpaque(false);
              panel3.setName("panel3");

              //---- OBJ_55 ----
              OBJ_55.setText("Libell\u00e9");
              OBJ_55.setName("OBJ_55");

              //---- TGLIB ----
              TGLIB.setComponentPopupMenu(BTD);
              TGLIB.setName("TGLIB");

              GroupLayout panel3Layout = new GroupLayout(panel3);
              panel3.setLayout(panel3Layout);
              panel3Layout.setHorizontalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(96, 96, 96)
                    .addComponent(TGLIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
              );
              panel3Layout.setVerticalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(TGLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              );
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(12, 13, 500, 45);

            //---- OBJ_68 ----
            OBJ_68.setText("Sens normal");
            OBJ_68.setName("OBJ_68");
            xTitledPanel1ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(20, 65, 100, 20);

            //---- TGSNS ----
            TGSNS.setModel(new DefaultComboBoxModel(new String[] {
              "D\u00e9bit",
              "Cr\u00e9dit"
            }));
            TGSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TGSNS.setName("TGSNS");
            xTitledPanel1ContentContainer.add(TGSNS);
            TGSNS.setBounds(170, 60, 75, TGSNS.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setTitle("Plages Comptes G\u00e9n\u00e9raux ");
            OBJ_69.setName("OBJ_69");
            xTitledPanel1ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(12, 90, 500, 24);

            //---- label1 ----
            label1.setText("N\u00b0D\u00e9but");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            xTitledPanel1ContentContainer.add(label1);
            label1.setBounds(20, 120, 70, 20);

            //---- TGD1 ----
            TGD1.setName("TGD1");
            xTitledPanel1ContentContainer.add(TGD1);
            TGD1.setBounds(20, 140, 70, TGD1.getPreferredSize().height);

            //---- TGD2 ----
            TGD2.setName("TGD2");
            xTitledPanel1ContentContainer.add(TGD2);
            TGD2.setBounds(20, 170, 70, 28);

            //---- TGD3 ----
            TGD3.setName("TGD3");
            xTitledPanel1ContentContainer.add(TGD3);
            TGD3.setBounds(20, 200, 70, 28);

            //---- TGD4 ----
            TGD4.setName("TGD4");
            xTitledPanel1ContentContainer.add(TGD4);
            TGD4.setBounds(20, 230, 70, 28);

            //---- TGD5 ----
            TGD5.setName("TGD5");
            xTitledPanel1ContentContainer.add(TGD5);
            TGD5.setBounds(20, 260, 70, 28);

            //---- TGD6 ----
            TGD6.setName("TGD6");
            xTitledPanel1ContentContainer.add(TGD6);
            TGD6.setBounds(20, 290, 70, 28);

            //---- TGD7 ----
            TGD7.setName("TGD7");
            xTitledPanel1ContentContainer.add(TGD7);
            TGD7.setBounds(20, 320, 70, 28);

            //---- TGD8 ----
            TGD8.setName("TGD8");
            xTitledPanel1ContentContainer.add(TGD8);
            TGD8.setBounds(20, 350, 70, 28);

            //---- TGD9 ----
            TGD9.setName("TGD9");
            xTitledPanel1ContentContainer.add(TGD9);
            TGD9.setBounds(20, 380, 70, 28);

            //---- TGD10 ----
            TGD10.setName("TGD10");
            xTitledPanel1ContentContainer.add(TGD10);
            TGD10.setBounds(20, 410, 70, 28);

            //---- TGD11 ----
            TGD11.setName("TGD11");
            xTitledPanel1ContentContainer.add(TGD11);
            TGD11.setBounds(20, 440, 70, 28);

            //---- TGD12 ----
            TGD12.setName("TGD12");
            xTitledPanel1ContentContainer.add(TGD12);
            TGD12.setBounds(20, 470, 70, 28);

            //---- label2 ----
            label2.setText("N\u00b0Fin");
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            xTitledPanel1ContentContainer.add(label2);
            label2.setBounds(115, 120, 70, 20);

            //---- TGF1 ----
            TGF1.setName("TGF1");
            xTitledPanel1ContentContainer.add(TGF1);
            TGF1.setBounds(115, 140, 70, TGF1.getPreferredSize().height);

            //---- TGF2 ----
            TGF2.setName("TGF2");
            xTitledPanel1ContentContainer.add(TGF2);
            TGF2.setBounds(115, 170, 70, 28);

            //---- TGF3 ----
            TGF3.setName("TGF3");
            xTitledPanel1ContentContainer.add(TGF3);
            TGF3.setBounds(115, 200, 70, 28);

            //---- TGF4 ----
            TGF4.setName("TGF4");
            xTitledPanel1ContentContainer.add(TGF4);
            TGF4.setBounds(115, 230, 70, 28);

            //---- TGF5 ----
            TGF5.setName("TGF5");
            xTitledPanel1ContentContainer.add(TGF5);
            TGF5.setBounds(115, 260, 70, 28);

            //---- TGF6 ----
            TGF6.setName("TGF6");
            xTitledPanel1ContentContainer.add(TGF6);
            TGF6.setBounds(115, 290, 70, 28);

            //---- TGF7 ----
            TGF7.setName("TGF7");
            xTitledPanel1ContentContainer.add(TGF7);
            TGF7.setBounds(115, 320, 70, 28);

            //---- TGF8 ----
            TGF8.setName("TGF8");
            xTitledPanel1ContentContainer.add(TGF8);
            TGF8.setBounds(115, 350, 70, 28);

            //---- TGF9 ----
            TGF9.setName("TGF9");
            xTitledPanel1ContentContainer.add(TGF9);
            TGF9.setBounds(115, 380, 70, 28);

            //---- TGF10 ----
            TGF10.setName("TGF10");
            xTitledPanel1ContentContainer.add(TGF10);
            TGF10.setBounds(115, 410, 70, 28);

            //---- TGF11 ----
            TGF11.setName("TGF11");
            xTitledPanel1ContentContainer.add(TGF11);
            TGF11.setBounds(115, 440, 70, 28);

            //---- TGF12 ----
            TGF12.setName("TGF12");
            xTitledPanel1ContentContainer.add(TGF12);
            TGF12.setBounds(115, 470, 70, 28);

            //---- label3 ----
            label3.setText("Exclus");
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setName("label3");
            xTitledPanel1ContentContainer.add(label3);
            label3.setBounds(208, 120, 65, 20);

            //---- TGN1 ----
            TGN1.setName("TGN1");
            xTitledPanel1ContentContainer.add(TGN1);
            TGN1.setBounds(new Rectangle(new Point(231, 145), TGN1.getPreferredSize()));

            //---- TGN2 ----
            TGN2.setName("TGN2");
            xTitledPanel1ContentContainer.add(TGN2);
            TGN2.setBounds(231, 175, 18, 18);

            //---- TGN3 ----
            TGN3.setName("TGN3");
            xTitledPanel1ContentContainer.add(TGN3);
            TGN3.setBounds(231, 205, 18, 18);

            //---- TGN4 ----
            TGN4.setName("TGN4");
            xTitledPanel1ContentContainer.add(TGN4);
            TGN4.setBounds(231, 235, 18, 18);

            //---- TGN5 ----
            TGN5.setName("TGN5");
            xTitledPanel1ContentContainer.add(TGN5);
            TGN5.setBounds(231, 265, 18, 18);

            //---- TGN6 ----
            TGN6.setName("TGN6");
            xTitledPanel1ContentContainer.add(TGN6);
            TGN6.setBounds(231, 295, 18, 18);

            //---- TGN7 ----
            TGN7.setName("TGN7");
            xTitledPanel1ContentContainer.add(TGN7);
            TGN7.setBounds(231, 325, 18, 18);

            //---- TGN8 ----
            TGN8.setName("TGN8");
            xTitledPanel1ContentContainer.add(TGN8);
            TGN8.setBounds(231, 355, 18, 18);

            //---- TGN9 ----
            TGN9.setName("TGN9");
            xTitledPanel1ContentContainer.add(TGN9);
            TGN9.setBounds(231, 385, 18, 18);

            //---- TGN10 ----
            TGN10.setName("TGN10");
            xTitledPanel1ContentContainer.add(TGN10);
            TGN10.setBounds(231, 415, 18, 18);

            //---- TGN11 ----
            TGN11.setName("TGN11");
            xTitledPanel1ContentContainer.add(TGN11);
            TGN11.setBounds(231, 445, 18, 18);

            //---- TGN12 ----
            TGN12.setName("TGN12");
            xTitledPanel1ContentContainer.add(TGN12);
            TGN12.setBounds(231, 475, 18, 18);

            //---- label4 ----
            label4.setText("Libell\u00e9");
            label4.setHorizontalAlignment(SwingConstants.LEFT);
            label4.setName("label4");
            xTitledPanel1ContentContainer.add(label4);
            label4.setBounds(280, 120, 70, 20);

            //---- TGL1 ----
            TGL1.setName("TGL1");
            xTitledPanel1ContentContainer.add(TGL1);
            TGL1.setBounds(280, 140, 210, TGL1.getPreferredSize().height);

            //---- TGL2 ----
            TGL2.setName("TGL2");
            xTitledPanel1ContentContainer.add(TGL2);
            TGL2.setBounds(280, 170, 210, 28);

            //---- TGL3 ----
            TGL3.setName("TGL3");
            xTitledPanel1ContentContainer.add(TGL3);
            TGL3.setBounds(280, 200, 210, 28);

            //---- TGL4 ----
            TGL4.setName("TGL4");
            xTitledPanel1ContentContainer.add(TGL4);
            TGL4.setBounds(280, 230, 210, 28);

            //---- TGL5 ----
            TGL5.setName("TGL5");
            xTitledPanel1ContentContainer.add(TGL5);
            TGL5.setBounds(280, 260, 210, 28);

            //---- TGL6 ----
            TGL6.setName("TGL6");
            xTitledPanel1ContentContainer.add(TGL6);
            TGL6.setBounds(280, 290, 210, 28);

            //---- TGL7 ----
            TGL7.setName("TGL7");
            xTitledPanel1ContentContainer.add(TGL7);
            TGL7.setBounds(280, 320, 210, 28);

            //---- TGL8 ----
            TGL8.setName("TGL8");
            xTitledPanel1ContentContainer.add(TGL8);
            TGL8.setBounds(280, 350, 210, 28);

            //---- TGL9 ----
            TGL9.setName("TGL9");
            xTitledPanel1ContentContainer.add(TGL9);
            TGL9.setBounds(280, 380, 210, 28);

            //---- TGL10 ----
            TGL10.setName("TGL10");
            xTitledPanel1ContentContainer.add(TGL10);
            TGL10.setBounds(280, 410, 210, 28);

            //---- TGL11 ----
            TGL11.setName("TGL11");
            xTitledPanel1ContentContainer.add(TGL11);
            TGL11.setBounds(280, 440, 210, 28);

            //---- TGL12 ----
            TGL12.setName("TGL12");
            xTitledPanel1ContentContainer.add(TGL12);
            TGL12.setBounds(280, 470, 210, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 527, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 539, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel3;
  private JLabel OBJ_55;
  private XRiTextField TGLIB;
  private JLabel OBJ_68;
  private XRiComboBox TGSNS;
  private JXTitledSeparator OBJ_69;
  private JLabel label1;
  private XRiTextField TGD1;
  private XRiTextField TGD2;
  private XRiTextField TGD3;
  private XRiTextField TGD4;
  private XRiTextField TGD5;
  private XRiTextField TGD6;
  private XRiTextField TGD7;
  private XRiTextField TGD8;
  private XRiTextField TGD9;
  private XRiTextField TGD10;
  private XRiTextField TGD11;
  private XRiTextField TGD12;
  private JLabel label2;
  private XRiTextField TGF1;
  private XRiTextField TGF2;
  private XRiTextField TGF3;
  private XRiTextField TGF4;
  private XRiTextField TGF5;
  private XRiTextField TGF6;
  private XRiTextField TGF7;
  private XRiTextField TGF8;
  private XRiTextField TGF9;
  private XRiTextField TGF10;
  private XRiTextField TGF11;
  private XRiTextField TGF12;
  private JLabel label3;
  private XRiCheckBox TGN1;
  private XRiCheckBox TGN2;
  private XRiCheckBox TGN3;
  private XRiCheckBox TGN4;
  private XRiCheckBox TGN5;
  private XRiCheckBox TGN6;
  private XRiCheckBox TGN7;
  private XRiCheckBox TGN8;
  private XRiCheckBox TGN9;
  private XRiCheckBox TGN10;
  private XRiCheckBox TGN11;
  private XRiCheckBox TGN12;
  private JLabel label4;
  private XRiTextField TGL1;
  private XRiTextField TGL2;
  private XRiTextField TGL3;
  private XRiTextField TGL4;
  private XRiTextField TGL5;
  private XRiTextField TGL6;
  private XRiTextField TGL7;
  private XRiTextField TGL8;
  private XRiTextField TGL9;
  private XRiTextField TGL10;
  private XRiTextField TGL11;
  private XRiTextField TGL12;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
