
package ri.serien.libecranrpg.vexp.VEXP94FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP94FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] AEACT_Value = { "3", "4", };
  private String[] AEIN1_Value = { " ", "W", };
  
  public VEXP94FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    AEBRO.setValeursSelection("1", " ");
    AEDIR.setValeursSelection("1", " ");
    AEDUP.setValeursSelection("1", " ");
    AEFNB.setValeursSelection("1", " ");
    AEACT.setValeurs(AEACT_Value, null);
    AEIN1.setValeurs(AEIN1_Value, null);
    AEIN2.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    AEIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AEIMP@")).trim());
    AEECR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AEECR@")).trim());
    AEETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AEETB@")).trim());
    AEMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AEMAG@")).trim());
    AEREF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AEREF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    AEDEN.setVisible(lexique.HostFieldGetData("AEBRO").equalsIgnoreCase("1") & lexique.isPresent("AEDEN"));
    AEMAG.setVisible(lexique.isPresent("AEMAG"));
    AEETB.setVisible(lexique.isPresent("AEETB"));
    AEIMP.setVisible(lexique.isPresent("AEIMP"));
    AEECR.setVisible(lexique.isPresent("AEECR"));
    OBJ_46.setVisible(lexique.isPresent("AEIMU"));
    lbDensiteEncre.setVisible(lexique.HostFieldGetData("AEBRO").equalsIgnoreCase("1"));
    
    if (lexique.isTrue("60")) {
      panel3.setBorder(new TitledBorder("Chemin complet du script"));
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affectations d'impressions"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void AEIMUFocusLost(FocusEvent e) {
    AEIN2.setVisible(AEIMU.getText().trim().toUpperCase().equals("*NONE"));
  }
  
  private void boutonRechercheActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("AEIMU");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    AEACT = new XRiComboBox();
    AEDOC = new XRiComboBox();
    AEIMU = new XRiTextField();
    lbNomImprimante = new JLabel();
    AEFNB = new XRiCheckBox();
    AEDUP = new XRiCheckBox();
    lbNombreExemplaire = new JLabel();
    AEDIR = new XRiCheckBox();
    lbTypeDocument = new JLabel();
    lbChoixTiroirEntre = new JLabel();
    lbChoixTiroirSortie = new JLabel();
    lbActionSpool = new JLabel();
    AEBRO = new XRiCheckBox();
    lbDensiteEncre = new JLabel();
    AEEXE = new XRiTextField();
    AETIE = new XRiTextField();
    AETIS = new XRiTextField();
    AEDEN = new XRiTextField();
    OBJ_46 = new SNBoutonDetail();
    AEIN2 = new XRiCheckBox();
    panel3 = new JPanel();
    AECHE1 = new XRiTextField();
    AECHE2 = new XRiTextField();
    lbAdresseIp = new JLabel();
    ADRIP = new XRiTextField();
    lbTypeOS = new JLabel();
    AEIN1 = new XRiComboBox();
    barre_tete = new JMenuBar();
    panel1 = new JPanel();
    lbEtat = new JLabel();
    AEIMP = new RiZoneSortie();
    lbCodeEcran = new JLabel();
    AEECR = new RiZoneSortie();
    lbEtablissement = new JLabel();
    AEETB = new RiZoneSortie();
    AEMAG = new RiZoneSortie();
    lbMagasin = new JLabel();
    lbReference = new JLabel();
    AEREF = new RiZoneSortie();
    BTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    miAideEnLigne = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(770, 565));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Imprimante"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- AEACT ----
          AEACT.setModel(new DefaultComboBoxModel(new String[] {
            "Suspendre",
            "Supprimer"
          }));
          AEACT.setComponentPopupMenu(BTD);
          AEACT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AEACT.setName("AEACT");
          panel2.add(AEACT);
          AEACT.setBounds(205, 312, 124, AEACT.getPreferredSize().height);

          //---- AEDOC ----
          AEDOC.setModel(new DefaultComboBoxModel(new String[] {
            "PDF",
            "PS"
          }));
          AEDOC.setComponentPopupMenu(BTD);
          AEDOC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AEDOC.setName("AEDOC");
          panel2.add(AEDOC);
          AEDOC.setBounds(205, 100, 65, AEDOC.getPreferredSize().height);

          //---- AEIMU ----
          AEIMU.setComponentPopupMenu(BTD);
          AEIMU.setName("AEIMU");
          AEIMU.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              AEIMUFocusLost(e);
            }
          });
          panel2.add(AEIMU);
          AEIMU.setBounds(205, 40, 310, AEIMU.getPreferredSize().height);

          //---- lbNomImprimante ----
          lbNomImprimante.setText("Nom de l'imprimante utilis\u00e9e");
          lbNomImprimante.setName("lbNomImprimante");
          panel2.add(lbNomImprimante);
          lbNomImprimante.setBounds(30, 44, 175, 20);

          //---- AEFNB ----
          AEFNB.setText("Forcer le noir et blanc");
          AEFNB.setComponentPopupMenu(BTD);
          AEFNB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AEFNB.setName("AEFNB");
          panel2.add(AEFNB);
          AEFNB.setBounds(205, 286, 162, 20);

          //---- AEDUP ----
          AEDUP.setText("Duplex (Recto/verso)");
          AEDUP.setComponentPopupMenu(BTD);
          AEDUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AEDUP.setName("AEDUP");
          panel2.add(AEDUP);
          AEDUP.setBounds(205, 234, 152, 20);

          //---- lbNombreExemplaire ----
          lbNombreExemplaire.setText("Nombre exemplaires");
          lbNombreExemplaire.setName("lbNombreExemplaire");
          panel2.add(lbNombreExemplaire);
          lbNombreExemplaire.setBounds(30, 136, 175, 20);

          //---- AEDIR ----
          AEDIR.setText("Cacher la fen\u00eatre d'\u00e9dition");
          AEDIR.setComponentPopupMenu(BTD);
          AEDIR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AEDIR.setName("AEDIR");
          panel2.add(AEDIR);
          AEDIR.setBounds(205, 74, 260, 20);

          //---- lbTypeDocument ----
          lbTypeDocument.setText("Type de document");
          lbTypeDocument.setName("lbTypeDocument");
          panel2.add(lbTypeDocument);
          lbTypeDocument.setBounds(30, 103, 175, 20);

          //---- lbChoixTiroirEntre ----
          lbChoixTiroirEntre.setText("Choix tiroir d'entr\u00e9e");
          lbChoixTiroirEntre.setName("lbChoixTiroirEntre");
          panel2.add(lbChoixTiroirEntre);
          lbChoixTiroirEntre.setBounds(30, 170, 175, 20);

          //---- lbChoixTiroirSortie ----
          lbChoixTiroirSortie.setText("Choix tiroir de sortie");
          lbChoixTiroirSortie.setName("lbChoixTiroirSortie");
          panel2.add(lbChoixTiroirSortie);
          lbChoixTiroirSortie.setBounds(30, 204, 175, 20);

          //---- lbActionSpool ----
          lbActionSpool.setText("Action sur spool");
          lbActionSpool.setName("lbActionSpool");
          panel2.add(lbActionSpool);
          lbActionSpool.setBounds(30, 315, 175, 20);

          //---- AEBRO ----
          AEBRO.setText("Mode brouillon");
          AEBRO.setComponentPopupMenu(BTD);
          AEBRO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AEBRO.setName("AEBRO");
          panel2.add(AEBRO);
          AEBRO.setBounds(205, 260, 130, 20);

          //---- lbDensiteEncre ----
          lbDensiteEncre.setText("Densit\u00e9 encre");
          lbDensiteEncre.setName("lbDensiteEncre");
          panel2.add(lbDensiteEncre);
          lbDensiteEncre.setBounds(390, 234, 104, 20);

          //---- AEEXE ----
          AEEXE.setComponentPopupMenu(BTD);
          AEEXE.setName("AEEXE");
          panel2.add(AEEXE);
          AEEXE.setBounds(205, 132, 26, AEEXE.getPreferredSize().height);

          //---- AETIE ----
          AETIE.setComponentPopupMenu(BTD);
          AETIE.setName("AETIE");
          panel2.add(AETIE);
          AETIE.setBounds(205, 166, 26, AETIE.getPreferredSize().height);

          //---- AETIS ----
          AETIS.setComponentPopupMenu(BTD);
          AETIS.setName("AETIS");
          panel2.add(AETIS);
          AETIS.setBounds(205, 200, 26, AETIS.getPreferredSize().height);

          //---- AEDEN ----
          AEDEN.setComponentPopupMenu(BTD);
          AEDEN.setName("AEDEN");
          panel2.add(AEDEN);
          AEDEN.setBounds(495, 230, 20, AEDEN.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("");
          OBJ_46.setToolTipText("Choisir une imprimante");
          OBJ_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_46.setName("OBJ_46");
          OBJ_46.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              boutonRechercheActionPerformed(e);
            }
          });
          panel2.add(OBJ_46);
          OBJ_46.setBounds(515, 33, 46, 42);

          //---- AEIN2 ----
          AEIN2.setText("Activer la sauvegarde du fichier g\u00e9n\u00e9r\u00e9");
          AEIN2.setName("AEIN2");
          panel2.add(AEIN2);
          AEIN2.setBounds(280, 103, 235, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 575, 350);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Chemin Outq dans l'IFS"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- AECHE1 ----
          AECHE1.setComponentPopupMenu(BTD);
          AECHE1.setName("AECHE1");
          panel3.add(AECHE1);
          AECHE1.setBounds(30, 25, 510, AECHE1.getPreferredSize().height);

          //---- AECHE2 ----
          AECHE2.setComponentPopupMenu(BTD);
          AECHE2.setName("AECHE2");
          panel3.add(AECHE2);
          AECHE2.setBounds(30, 50, 510, AECHE2.getPreferredSize().height);

          //---- lbAdresseIp ----
          lbAdresseIp.setText("Adresse IP si le serveur NewSim n'est pas sur l'I5");
          lbAdresseIp.setName("lbAdresseIp");
          panel3.add(lbAdresseIp);
          lbAdresseIp.setBounds(30, 85, 320, 18);

          //---- ADRIP ----
          ADRIP.setName("ADRIP");
          panel3.add(ADRIP);
          ADRIP.setBounds(410, 80, 130, ADRIP.getPreferredSize().height);

          //---- lbTypeOS ----
          lbTypeOS.setText("Type d'OS sur lequel est install\u00e9 le serveur NewSim");
          lbTypeOS.setName("lbTypeOS");
          panel3.add(lbTypeOS);
          lbTypeOS.setBounds(30, 115, 320, 18);

          //---- AEIN1 ----
          AEIN1.setModel(new DefaultComboBoxModel(new String[] {
            "OS400 (localhost)",
            "Windows"
          }));
          AEIN1.setName("AEIN1");
          panel3.add(AEIN1);
          AEIN1.setBounds(390, 110, 150, AEIN1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 365, 575, 155);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setPreferredSize(new Dimension(750, 32));
        panel1.setMinimumSize(new Dimension(750, 32));
        panel1.setMaximumSize(new Dimension(800, 25));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- lbEtat ----
        lbEtat.setText("Etat");
        lbEtat.setName("lbEtat");
        panel1.add(lbEtat);
        lbEtat.setBounds(7, 0, 26, 24);

        //---- AEIMP ----
        AEIMP.setComponentPopupMenu(BTD);
        AEIMP.setText("@AEIMP@");
        AEIMP.setOpaque(false);
        AEIMP.setName("AEIMP");
        panel1.add(AEIMP);
        AEIMP.setBounds(40, 0, 56, AEIMP.getPreferredSize().height);

        //---- lbCodeEcran ----
        lbCodeEcran.setText("Code \u00e9cran");
        lbCodeEcran.setName("lbCodeEcran");
        panel1.add(lbCodeEcran);
        lbCodeEcran.setBounds(124, 0, 74, 24);

        //---- AEECR ----
        AEECR.setComponentPopupMenu(BTD);
        AEECR.setText("@AEECR@");
        AEECR.setOpaque(false);
        AEECR.setName("AEECR");
        panel1.add(AEECR);
        AEECR.setBounds(205, 0, 94, AEECR.getPreferredSize().height);

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        panel1.add(lbEtablissement);
        lbEtablissement.setBounds(323, 0, 95, 24);

        //---- AEETB ----
        AEETB.setComponentPopupMenu(BTD);
        AEETB.setText("@AEETB@");
        AEETB.setOpaque(false);
        AEETB.setName("AEETB");
        panel1.add(AEETB);
        AEETB.setBounds(425, 0, 40, AEETB.getPreferredSize().height);

        //---- AEMAG ----
        AEMAG.setComponentPopupMenu(BTD);
        AEMAG.setText("@AEMAG@");
        AEMAG.setOpaque(false);
        AEMAG.setName("AEMAG");
        panel1.add(AEMAG);
        AEMAG.setBounds(545, 0, 34, AEMAG.getPreferredSize().height);

        //---- lbMagasin ----
        lbMagasin.setText("Magasin");
        lbMagasin.setName("lbMagasin");
        panel1.add(lbMagasin);
        lbMagasin.setBounds(483, 0, 60, 24);

        //---- lbReference ----
        lbReference.setText("R\u00e9f\u00e9rence");
        lbReference.setName("lbReference");
        panel1.add(lbReference);
        lbReference.setBounds(597, 0, 63, 24);

        //---- AEREF ----
        AEREF.setComponentPopupMenu(BTD);
        AEREF.setText("@AEREF@");
        AEREF.setOpaque(false);
        AEREF.setName("AEREF");
        panel1.add(AEREF);
        AEREF.setBounds(661, 0, 86, AEREF.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      BTD.add(miChoixPossible);

      //---- miAideEnLigne ----
      miAideEnLigne.setText("Aide en ligne");
      miAideEnLigne.setName("miAideEnLigne");
      miAideEnLigne.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigneActionPerformed(e);
        }
      });
      BTD.add(miAideEnLigne);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiComboBox AEACT;
  private XRiComboBox AEDOC;
  private XRiTextField AEIMU;
  private JLabel lbNomImprimante;
  private XRiCheckBox AEFNB;
  private XRiCheckBox AEDUP;
  private JLabel lbNombreExemplaire;
  private XRiCheckBox AEDIR;
  private JLabel lbTypeDocument;
  private JLabel lbChoixTiroirEntre;
  private JLabel lbChoixTiroirSortie;
  private JLabel lbActionSpool;
  private XRiCheckBox AEBRO;
  private JLabel lbDensiteEncre;
  private XRiTextField AEEXE;
  private XRiTextField AETIE;
  private XRiTextField AETIS;
  private XRiTextField AEDEN;
  private SNBoutonDetail OBJ_46;
  private XRiCheckBox AEIN2;
  private JPanel panel3;
  private XRiTextField AECHE1;
  private XRiTextField AECHE2;
  private JLabel lbAdresseIp;
  private XRiTextField ADRIP;
  private JLabel lbTypeOS;
  private XRiComboBox AEIN1;
  private JMenuBar barre_tete;
  private JPanel panel1;
  private JLabel lbEtat;
  private RiZoneSortie AEIMP;
  private JLabel lbCodeEcran;
  private RiZoneSortie AEECR;
  private JLabel lbEtablissement;
  private RiZoneSortie AEETB;
  private RiZoneSortie AEMAG;
  private JLabel lbMagasin;
  private JLabel lbReference;
  private RiZoneSortie AEREF;
  private JPopupMenu BTD;
  private JMenuItem miChoixPossible;
  private JMenuItem miAideEnLigne;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
