
package ri.serien.libecranrpg.vexp.VEXP03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiMailto;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VEXP03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    RLIN1.setValeursSelection("P", " ");
    REAAPP.setValeursSelection("1", " ");
    REASPA.setValeursSelection("1", " ");
    REIN1.setValeursSelection("1", " ");
    REIN2.setValeursSelection("X", " ");
    REIN4.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_75_OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    OBJ_76_OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    OBJ_78_OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    OBJ_80_OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    OBJ_82_OBJ_82.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WTIE1.setVisible(lexique.isTrue("23"));
    
    if (lexique.HostFieldGetData("RENOM").trim().equals("") && lexique.HostFieldGetData("REPRE").trim().equals("")
        && !lexique.isTrue("51") && !lexique.HostFieldGetData("REPAC").trim().equals("")) {
      RENOM.setText(lexique.HostFieldGetData("REPAC"));
    }
    
    OBJ_74_OBJ_74.setVisible(lexique.isPresent("RETOP1"));
    
    if (lexique.isTrue("21")) {
      OBJ_42_OBJ_42.setText("Client");
    }
    else if (lexique.isTrue("22")) {
      OBJ_42_OBJ_42.setText("Prospec");
      
    }
    else if (lexique.isTrue("23")) {
      OBJ_42_OBJ_42.setText("Fournisseur");
    }
    else {
      OBJ_42_OBJ_42.setVisible(false);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Informations Contact @LIBPG@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void OBJ_54ActionPerformed(ActionEvent e) {
    // compose numéro de téléphone...
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDI.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_34_OBJ_34 = new JLabel();
    RECIV = new XRiTextField();
    RENOM = new XRiTextField();
    OBJ_34_OBJ_35 = new JLabel();
    REPRE = new XRiTextField();
    OBJ_38_OBJ_38 = new JLabel();
    RECL1 = new XRiTextField();
    l_Class2 = new JLabel();
    RECL2 = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    REPRF = new XRiTextField();
    OBJ_51_OBJ_51 = new JLabel();
    REOBS = new XRiTextField();
    OBJ_51_OBJ_52 = new JLabel();
    RETEL = new XRiTextField();
    OBJ_54 = new SNBoutonDetail();
    OBJ_56_OBJ_56 = new JLabel();
    REPOS = new XRiTextField();
    OBJ_51_OBJ_53 = new JLabel();
    RETEL2 = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    RECAT = new XRiTextField();
    OBJ_51_OBJ_54 = new JLabel();
    REFAX = new XRiTextField();
    OBJ_51_OBJ_55 = new JLabel();
    RENET1 = new XRiMailto();
    OBJ_38_OBJ_39 = new JLabel();
    WZSRVM = new XRiTextField();
    OBJ_95_OBJ_95 = new JLabel();
    RETYZP = new XRiTextField();
    REASPA = new XRiCheckBox();
    REAAPP = new XRiCheckBox();
    REIN1 = new XRiCheckBox();
    REIN2 = new XRiCheckBox();
    WZGCD = new XRiTextField();
    OBJ_38_OBJ_40 = new JLabel();
    REIN4 = new XRiCheckBox();
    xTitledPanel2 = new JXTitledPanel();
    WZNOM = new XRiTextField();
    WZCPL = new XRiTextField();
    WZRUE = new XRiTextField();
    WZLOC = new XRiTextField();
    WZPAY = new XRiTextField();
    WZVIL = new XRiTextField();
    RLIN1 = new XRiCheckBox();
    OBJ_74_OBJ_74 = new JXTitledSeparator();
    WTIE2 = new XRiTextField();
    WZCDP = new XRiTextField();
    WTIE3 = new XRiTextField();
    RETOP1 = new XRiTextField();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_76_OBJ_76 = new JLabel();
    RETOP2 = new XRiTextField();
    OBJ_78_OBJ_78 = new JLabel();
    RETOP3 = new XRiTextField();
    OBJ_80_OBJ_80 = new JLabel();
    RETOP4 = new XRiTextField();
    OBJ_82_OBJ_82 = new JLabel();
    RETOP5 = new XRiTextField();
    WTIE1 = new XRiTextField();
    OBJ_42_OBJ_42 = new JLabel();
    BTDI = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1080, 450));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Exportation");
              riSousMenu_bt7.setEnabled(false);
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Acc\u00e8s au tiers");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Options client");
              riSousMenu_bt16.setToolTipText("Options sur le client dont d\u00e9pend ce contact");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Zones personnalis\u00e9es");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Extension serveur mail");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc notes complet");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Contact");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);
          
          // ---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("Nom");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
          xTitledPanel1ContentContainer.add(OBJ_34_OBJ_34);
          OBJ_34_OBJ_34.setBounds(10, 14, 87, 20);
          
          // ---- RECIV ----
          RECIV.setComponentPopupMenu(BTD);
          RECIV.setName("RECIV");
          xTitledPanel1ContentContainer.add(RECIV);
          RECIV.setBounds(105, 10, 40, RECIV.getPreferredSize().height);
          
          // ---- RENOM ----
          RENOM.setComponentPopupMenu(BTDI);
          RENOM.setFont(RENOM.getFont().deriveFont(RENOM.getFont().getStyle() | Font.BOLD));
          RENOM.setName("RENOM");
          xTitledPanel1ContentContainer.add(RENOM);
          RENOM.setBounds(152, 10, 318, RENOM.getPreferredSize().height);
          
          // ---- OBJ_34_OBJ_35 ----
          OBJ_34_OBJ_35.setText("Pr\u00e9nom");
          OBJ_34_OBJ_35.setName("OBJ_34_OBJ_35");
          xTitledPanel1ContentContainer.add(OBJ_34_OBJ_35);
          OBJ_34_OBJ_35.setBounds(10, 44, 87, 20);
          
          // ---- REPRE ----
          REPRE.setName("REPRE");
          xTitledPanel1ContentContainer.add(REPRE);
          REPRE.setBounds(105, 40, 365, REPRE.getPreferredSize().height);
          
          // ---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Classement");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
          xTitledPanel1ContentContainer.add(OBJ_38_OBJ_38);
          OBJ_38_OBJ_38.setBounds(10, 74, 93, 20);
          
          // ---- RECL1 ----
          RECL1.setComponentPopupMenu(BTDI);
          RECL1.setName("RECL1");
          xTitledPanel1ContentContainer.add(RECL1);
          RECL1.setBounds(105, 70, 160, RECL1.getPreferredSize().height);
          
          // ---- l_Class2 ----
          l_Class2.setText("Cl.2");
          l_Class2.setName("l_Class2");
          xTitledPanel1ContentContainer.add(l_Class2);
          l_Class2.setBounds(270, 74, 35, 20);
          
          // ---- RECL2 ----
          RECL2.setComponentPopupMenu(BTDI);
          RECL2.setName("RECL2");
          xTitledPanel1ContentContainer.add(RECL2);
          RECL2.setBounds(310, 70, 160, RECL2.getPreferredSize().height);
          
          // ---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Alias");
          OBJ_48_OBJ_48.setHorizontalAlignment(SwingConstants.LEFT);
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          xTitledPanel1ContentContainer.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(10, 104, 93, 20);
          
          // ---- REPRF ----
          REPRF.setName("REPRF");
          xTitledPanel1ContentContainer.add(REPRF);
          REPRF.setBounds(105, 100, 160, REPRF.getPreferredSize().height);
          
          // ---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("Observation");
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          xTitledPanel1ContentContainer.add(OBJ_51_OBJ_51);
          OBJ_51_OBJ_51.setBounds(10, 134, 94, 20);
          
          // ---- REOBS ----
          REOBS.setComponentPopupMenu(BTDI);
          REOBS.setName("REOBS");
          xTitledPanel1ContentContainer.add(REOBS);
          REOBS.setBounds(105, 130, 365, REOBS.getPreferredSize().height);
          
          // ---- OBJ_51_OBJ_52 ----
          OBJ_51_OBJ_52.setText("T\u00e9l\u00e9phone");
          OBJ_51_OBJ_52.setName("OBJ_51_OBJ_52");
          xTitledPanel1ContentContainer.add(OBJ_51_OBJ_52);
          OBJ_51_OBJ_52.setBounds(10, 164, 94, 20);
          
          // ---- RETEL ----
          RETEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
          RETEL.setComponentPopupMenu(BTDI);
          RETEL.setName("RETEL");
          xTitledPanel1ContentContainer.add(RETEL);
          RETEL.setBounds(105, 160, 210, RETEL.getPreferredSize().height);
          
          // ---- OBJ_54 ----
          OBJ_54.setText("");
          OBJ_54.setToolTipText("Composer le num\u00e9ro de t\u00e9l\u00e9phone");
          OBJ_54.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_54.setName("OBJ_54");
          OBJ_54.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_54ActionPerformed(e);
            }
          });
          xTitledPanel1ContentContainer.add(OBJ_54);
          OBJ_54.setBounds(315, 160, 35, 28);
          
          // ---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("Poste");
          OBJ_56_OBJ_56.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
          xTitledPanel1ContentContainer.add(OBJ_56_OBJ_56);
          OBJ_56_OBJ_56.setBounds(375, 164, 46, 20);
          
          // ---- REPOS ----
          REPOS.setToolTipText("Num\u00e9ro de poste");
          REPOS.setComponentPopupMenu(BTDI);
          REPOS.setName("REPOS");
          xTitledPanel1ContentContainer.add(REPOS);
          REPOS.setBounds(433, 160, 37, REPOS.getPreferredSize().height);
          
          // ---- OBJ_51_OBJ_53 ----
          OBJ_51_OBJ_53.setText("T\u00e9l\u00e9phone 2");
          OBJ_51_OBJ_53.setName("OBJ_51_OBJ_53");
          xTitledPanel1ContentContainer.add(OBJ_51_OBJ_53);
          OBJ_51_OBJ_53.setBounds(10, 194, 94, 20);
          
          // ---- RETEL2 ----
          RETEL2.setToolTipText("Deuxi\u00e8me num\u00e9ro de t\u00e9l\u00e9phone");
          RETEL2.setComponentPopupMenu(BTDI);
          RETEL2.setName("RETEL2");
          xTitledPanel1ContentContainer.add(RETEL2);
          RETEL2.setBounds(105, 190, 210, RETEL2.getPreferredSize().height);
          
          // ---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Fonction");
          OBJ_40_OBJ_40.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
          xTitledPanel1ContentContainer.add(OBJ_40_OBJ_40);
          OBJ_40_OBJ_40.setBounds(360, 194, 61, 20);
          
          // ---- RECAT ----
          RECAT.setComponentPopupMenu(BTD);
          RECAT.setName("RECAT");
          xTitledPanel1ContentContainer.add(RECAT);
          RECAT.setBounds(430, 190, 40, RECAT.getPreferredSize().height);
          
          // ---- OBJ_51_OBJ_54 ----
          OBJ_51_OBJ_54.setText("Fax");
          OBJ_51_OBJ_54.setName("OBJ_51_OBJ_54");
          xTitledPanel1ContentContainer.add(OBJ_51_OBJ_54);
          OBJ_51_OBJ_54.setBounds(10, 224, 94, 20);
          
          // ---- REFAX ----
          REFAX.setComponentPopupMenu(BTDI);
          REFAX.setToolTipText("Num\u00e9ro de fax");
          REFAX.setName("REFAX");
          xTitledPanel1ContentContainer.add(REFAX);
          REFAX.setBounds(105, 220, 210, REFAX.getPreferredSize().height);
          
          // ---- OBJ_51_OBJ_55 ----
          OBJ_51_OBJ_55.setText("E-mail");
          OBJ_51_OBJ_55.setName("OBJ_51_OBJ_55");
          xTitledPanel1ContentContainer.add(OBJ_51_OBJ_55);
          OBJ_51_OBJ_55.setBounds(10, 254, 94, 20);
          
          // ---- RENET1 ----
          RENET1.setToolTipText("Adresse Email");
          RENET1.setComponentPopupMenu(BTDI);
          RENET1.setName("RENET1");
          xTitledPanel1ContentContainer.add(RENET1);
          RENET1.setBounds(105, 250, 365, RENET1.getPreferredSize().height);
          
          // ---- OBJ_38_OBJ_39 ----
          OBJ_38_OBJ_39.setText("Serveur mail");
          OBJ_38_OBJ_39.setName("OBJ_38_OBJ_39");
          xTitledPanel1ContentContainer.add(OBJ_38_OBJ_39);
          OBJ_38_OBJ_39.setBounds(10, 284, 93, 20);
          
          // ---- WZSRVM ----
          WZSRVM.setComponentPopupMenu(BTDI);
          WZSRVM.setName("WZSRVM");
          xTitledPanel1ContentContainer.add(WZSRVM);
          WZSRVM.setBounds(105, 280, 160, WZSRVM.getPreferredSize().height);
          
          // ---- OBJ_95_OBJ_95 ----
          OBJ_95_OBJ_95.setText("Type extension");
          OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");
          xTitledPanel1ContentContainer.add(OBJ_95_OBJ_95);
          OBJ_95_OBJ_95.setBounds(10, 314, 96, 20);
          
          // ---- RETYZP ----
          RETYZP.setComponentPopupMenu(BTDI);
          RETYZP.setName("RETYZP");
          xTitledPanel1ContentContainer.add(RETYZP);
          RETYZP.setBounds(105, 310, 20, RETYZP.getPreferredSize().height);
          
          // ---- REASPA ----
          REASPA.setText("Anti-spam");
          REASPA.setComponentPopupMenu(BTDI);
          REASPA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REASPA.setName("REASPA");
          xTitledPanel1ContentContainer.add(REASPA);
          REASPA.setBounds(165, 314, 95, 20);
          
          // ---- REAAPP ----
          REAAPP.setText("Anti-appel");
          REAAPP.setComponentPopupMenu(BTDI);
          REAAPP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REAAPP.setName("REAAPP");
          xTitledPanel1ContentContainer.add(REAAPP);
          REAAPP.setBounds(310, 314, 95, 20);
          
          // ---- REIN1 ----
          REIN1.setText("Destinataire bon");
          REIN1.setComponentPopupMenu(BTDI);
          REIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REIN1.setName("REIN1");
          xTitledPanel1ContentContainer.add(REIN1);
          REIN1.setBounds(165, 335, 124, 20);
          
          // ---- REIN2 ----
          REIN2.setText("En copie cach\u00e9e");
          REIN2.setComponentPopupMenu(BTDI);
          REIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REIN2.setName("REIN2");
          xTitledPanel1ContentContainer.add(REIN2);
          REIN2.setBounds(310, 335, 124, 20);
          
          // ---- WZGCD ----
          WZGCD.setComponentPopupMenu(BTDI);
          WZGCD.setName("WZGCD");
          xTitledPanel1ContentContainer.add(WZGCD);
          WZGCD.setBounds(354, 280, 116, WZGCD.getPreferredSize().height);
          
          // ---- OBJ_38_OBJ_40 ----
          OBJ_38_OBJ_40.setText("Gencod");
          OBJ_38_OBJ_40.setName("OBJ_38_OBJ_40");
          xTitledPanel1ContentContainer.add(OBJ_38_OBJ_40);
          OBJ_38_OBJ_40.setBounds(300, 284, 53, 20);
          
          // ---- REIN4 ----
          REIN4.setText("Destinataire facture");
          REIN4.setComponentPopupMenu(BTDI);
          REIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REIN4.setName("REIN4");
          xTitledPanel1ContentContainer.add(REIN4);
          REIN4.setBounds(165, 354, 140, 20);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 20, 505, 410);
        
        // ======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Identification");
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);
          
          // ---- WZNOM ----
          WZNOM.setComponentPopupMenu(BTDI);
          WZNOM.setName("WZNOM");
          xTitledPanel2ContentContainer.add(WZNOM);
          WZNOM.setBounds(17, 70, 310, WZNOM.getPreferredSize().height);
          
          // ---- WZCPL ----
          WZCPL.setComponentPopupMenu(BTDI);
          WZCPL.setName("WZCPL");
          xTitledPanel2ContentContainer.add(WZCPL);
          WZCPL.setBounds(17, 100, 310, WZCPL.getPreferredSize().height);
          
          // ---- WZRUE ----
          WZRUE.setComponentPopupMenu(BTDI);
          WZRUE.setName("WZRUE");
          xTitledPanel2ContentContainer.add(WZRUE);
          WZRUE.setBounds(17, 130, 310, WZRUE.getPreferredSize().height);
          
          // ---- WZLOC ----
          WZLOC.setComponentPopupMenu(BTDI);
          WZLOC.setName("WZLOC");
          xTitledPanel2ContentContainer.add(WZLOC);
          WZLOC.setBounds(17, 160, 310, WZLOC.getPreferredSize().height);
          
          // ---- WZPAY ----
          WZPAY.setComponentPopupMenu(BTDI);
          WZPAY.setName("WZPAY");
          xTitledPanel2ContentContainer.add(WZPAY);
          WZPAY.setBounds(17, 220, 310, WZPAY.getPreferredSize().height);
          
          // ---- WZVIL ----
          WZVIL.setComponentPopupMenu(BTDI);
          WZVIL.setName("WZVIL");
          xTitledPanel2ContentContainer.add(WZVIL);
          WZVIL.setBounds(75, 190, 252, WZVIL.getPreferredSize().height);
          
          // ---- RLIN1 ----
          RLIN1.setText("Contact principal");
          RLIN1.setComponentPopupMenu(BTDI);
          RLIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RLIN1.setName("RLIN1");
          xTitledPanel2ContentContainer.add(RLIN1);
          RLIN1.setBounds(17, 14, 128, 20);
          
          // ---- OBJ_74_OBJ_74 ----
          OBJ_74_OBJ_74.setTitle("Zones personnalis\u00e9es");
          OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");
          xTitledPanel2ContentContainer.add(OBJ_74_OBJ_74);
          OBJ_74_OBJ_74.setBounds(15, 255, 315, 20);
          
          // ---- WTIE2 ----
          WTIE2.setComponentPopupMenu(BTDI);
          WTIE2.setHorizontalAlignment(SwingConstants.RIGHT);
          WTIE2.setName("WTIE2");
          xTitledPanel2ContentContainer.add(WTIE2);
          WTIE2.setBounds(125, 40, 70, WTIE2.getPreferredSize().height);
          
          // ---- WZCDP ----
          WZCDP.setComponentPopupMenu(BTDI);
          WZCDP.setName("WZCDP");
          xTitledPanel2ContentContainer.add(WZCDP);
          WZCDP.setBounds(17, 190, 52, WZCDP.getPreferredSize().height);
          
          // ---- WTIE3 ----
          WTIE3.setComponentPopupMenu(BTDI);
          WTIE3.setHorizontalAlignment(SwingConstants.RIGHT);
          WTIE3.setName("WTIE3");
          xTitledPanel2ContentContainer.add(WTIE3);
          WTIE3.setBounds(200, 40, 40, WTIE3.getPreferredSize().height);
          
          // ---- RETOP1 ----
          RETOP1.setComponentPopupMenu(null);
          RETOP1.setName("RETOP1");
          xTitledPanel2ContentContainer.add(RETOP1);
          RETOP1.setBounds(40, 280, 30, RETOP1.getPreferredSize().height);
          
          // ---- OBJ_75_OBJ_75 ----
          OBJ_75_OBJ_75.setText("@WTI1@");
          OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
          xTitledPanel2ContentContainer.add(OBJ_75_OBJ_75);
          OBJ_75_OBJ_75.setBounds(17, 284, 22, 20);
          
          // ---- OBJ_76_OBJ_76 ----
          OBJ_76_OBJ_76.setText("@WTI2@");
          OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");
          xTitledPanel2ContentContainer.add(OBJ_76_OBJ_76);
          OBJ_76_OBJ_76.setBounds(85, 284, 22, 20);
          
          // ---- RETOP2 ----
          RETOP2.setComponentPopupMenu(null);
          RETOP2.setName("RETOP2");
          xTitledPanel2ContentContainer.add(RETOP2);
          RETOP2.setBounds(104, 280, 30, RETOP2.getPreferredSize().height);
          
          // ---- OBJ_78_OBJ_78 ----
          OBJ_78_OBJ_78.setText("@WTI3@");
          OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
          xTitledPanel2ContentContainer.add(OBJ_78_OBJ_78);
          OBJ_78_OBJ_78.setBounds(150, 284, 22, 20);
          
          // ---- RETOP3 ----
          RETOP3.setComponentPopupMenu(null);
          RETOP3.setName("RETOP3");
          xTitledPanel2ContentContainer.add(RETOP3);
          RETOP3.setBounds(168, 280, 30, RETOP3.getPreferredSize().height);
          
          // ---- OBJ_80_OBJ_80 ----
          OBJ_80_OBJ_80.setText("@WTI4@");
          OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
          xTitledPanel2ContentContainer.add(OBJ_80_OBJ_80);
          OBJ_80_OBJ_80.setBounds(210, 284, 22, 20);
          
          // ---- RETOP4 ----
          RETOP4.setComponentPopupMenu(null);
          RETOP4.setName("RETOP4");
          xTitledPanel2ContentContainer.add(RETOP4);
          RETOP4.setBounds(232, 280, 30, RETOP4.getPreferredSize().height);
          
          // ---- OBJ_82_OBJ_82 ----
          OBJ_82_OBJ_82.setText("@WTI5@");
          OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");
          xTitledPanel2ContentContainer.add(OBJ_82_OBJ_82);
          OBJ_82_OBJ_82.setBounds(275, 284, 22, 20);
          
          // ---- RETOP5 ----
          RETOP5.setComponentPopupMenu(null);
          RETOP5.setName("RETOP5");
          xTitledPanel2ContentContainer.add(RETOP5);
          RETOP5.setBounds(296, 280, 30, RETOP5.getPreferredSize().height);
          
          // ---- WTIE1 ----
          WTIE1.setComponentPopupMenu(BTDI);
          WTIE1.setHorizontalAlignment(SwingConstants.RIGHT);
          WTIE1.setName("WTIE1");
          xTitledPanel2ContentContainer.add(WTIE1);
          WTIE1.setBounds(100, 40, 20, WTIE1.getPreferredSize().height);
          
          // ---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("Client");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          xTitledPanel2ContentContainer.add(OBJ_42_OBJ_42);
          OBJ_42_OBJ_42.setBounds(17, 44, 83, 20);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(530, 20, 364, 410);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTDI ========
    {
      BTDI.setName("BTDI");
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_17);
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_34_OBJ_34;
  private XRiTextField RECIV;
  private XRiTextField RENOM;
  private JLabel OBJ_34_OBJ_35;
  private XRiTextField REPRE;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField RECL1;
  private JLabel l_Class2;
  private XRiTextField RECL2;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField REPRF;
  private JLabel OBJ_51_OBJ_51;
  private XRiTextField REOBS;
  private JLabel OBJ_51_OBJ_52;
  private XRiTextField RETEL;
  private SNBoutonDetail OBJ_54;
  private JLabel OBJ_56_OBJ_56;
  private XRiTextField REPOS;
  private JLabel OBJ_51_OBJ_53;
  private XRiTextField RETEL2;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField RECAT;
  private JLabel OBJ_51_OBJ_54;
  private XRiTextField REFAX;
  private JLabel OBJ_51_OBJ_55;
  private XRiMailto RENET1;
  private JLabel OBJ_38_OBJ_39;
  private XRiTextField WZSRVM;
  private JLabel OBJ_95_OBJ_95;
  private XRiTextField RETYZP;
  private XRiCheckBox REASPA;
  private XRiCheckBox REAAPP;
  private XRiCheckBox REIN1;
  private XRiCheckBox REIN2;
  private XRiTextField WZGCD;
  private JLabel OBJ_38_OBJ_40;
  private XRiCheckBox REIN4;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField WZNOM;
  private XRiTextField WZCPL;
  private XRiTextField WZRUE;
  private XRiTextField WZLOC;
  private XRiTextField WZPAY;
  private XRiTextField WZVIL;
  private XRiCheckBox RLIN1;
  private JXTitledSeparator OBJ_74_OBJ_74;
  private XRiTextField WTIE2;
  private XRiTextField WZCDP;
  private XRiTextField WTIE3;
  private XRiTextField RETOP1;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_76_OBJ_76;
  private XRiTextField RETOP2;
  private JLabel OBJ_78_OBJ_78;
  private XRiTextField RETOP3;
  private JLabel OBJ_80_OBJ_80;
  private XRiTextField RETOP4;
  private JLabel OBJ_82_OBJ_82;
  private XRiTextField RETOP5;
  private XRiTextField WTIE1;
  private JLabel OBJ_42_OBJ_42;
  private JPopupMenu BTDI;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
