
package ri.serien.libecranrpg.vexp.VEXPOBFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXPOBFM_O1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VEXPOBFM_O1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OB1.setVisible(lexique.isPresent("OB1"));
    
    // Titre
    setTitle("Bloc-notes");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OB1 = new XRiTextField();
    OBJ_11_OBJ_11 = new JLabel();
    OBJ_13_OBJ_13 = new JLabel();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    OB9 = new XRiTextField();
    OB10 = new XRiTextField();
    OB11 = new XRiTextField();
    OB12 = new XRiTextField();
    OB13 = new XRiTextField();
    OB14 = new XRiTextField();
    OB15 = new XRiTextField();
    OB16 = new XRiTextField();
    OB17 = new XRiTextField();
    OB18 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(650, 560));
    setPreferredSize(new Dimension(650, 560));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- OB1 ----
        OB1.setName("OB1");

        //---- OBJ_11_OBJ_11 ----
        OBJ_11_OBJ_11.setText("Sujet");
        OBJ_11_OBJ_11.setFont(OBJ_11_OBJ_11.getFont().deriveFont(OBJ_11_OBJ_11.getFont().getStyle() | Font.BOLD));
        OBJ_11_OBJ_11.setName("OBJ_11_OBJ_11");

        //---- OBJ_13_OBJ_13 ----
        OBJ_13_OBJ_13.setText("Texte");
        OBJ_13_OBJ_13.setFont(OBJ_13_OBJ_13.getFont().deriveFont(OBJ_13_OBJ_13.getFont().getStyle() | Font.BOLD));
        OBJ_13_OBJ_13.setName("OBJ_13_OBJ_13");

        //---- OB2 ----
        OB2.setName("OB2");

        //---- OB3 ----
        OB3.setName("OB3");

        //---- OB4 ----
        OB4.setName("OB4");

        //---- OB5 ----
        OB5.setName("OB5");

        //---- OB6 ----
        OB6.setName("OB6");

        //---- OB7 ----
        OB7.setName("OB7");

        //---- OB8 ----
        OB8.setName("OB8");

        //---- OB9 ----
        OB9.setName("OB9");

        //---- OB10 ----
        OB10.setName("OB10");

        //---- OB11 ----
        OB11.setName("OB11");

        //---- OB12 ----
        OB12.setName("OB12");

        //---- OB13 ----
        OB13.setName("OB13");

        //---- OB14 ----
        OB14.setName("OB14");

        //---- OB15 ----
        OB15.setName("OB15");

        //---- OB16 ----
        OB16.setName("OB16");

        //---- OB17 ----
        OB17.setName("OB17");

        //---- OB18 ----
        OB18.setName("OB18");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(OBJ_11_OBJ_11, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB1, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_13_OBJ_13, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB18, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB5, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB13, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB6, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB9, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB14, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB16, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB4, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB2, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB11, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB7, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB10, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB15, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB17, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB3, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB8, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addComponent(OB12, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(OBJ_11_OBJ_11)
              .addGap(4, 4, 4)
              .addComponent(OB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(17, 17, 17)
              .addComponent(OBJ_13_OBJ_13)
              .addGap(4, 4, 4)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(400, 400, 400)
                  .addComponent(OB18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(75, 75, 75)
                  .addComponent(OB5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(275, 275, 275)
                  .addComponent(OB13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(100, 100, 100)
                  .addComponent(OB6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(175, 175, 175)
                  .addComponent(OB9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(300, 300, 300)
                  .addComponent(OB14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(350, 350, 350)
                  .addComponent(OB16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addComponent(OB4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addComponent(OB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(225, 225, 225)
                  .addComponent(OB11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(125, 125, 125)
                  .addComponent(OB7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(200, 200, 200)
                  .addComponent(OB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(325, 325, 325)
                  .addComponent(OB15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(375, 375, 375)
                  .addComponent(OB17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OB3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(150, 150, 150)
                  .addComponent(OB8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(250, 250, 250)
                  .addComponent(OB12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private XRiTextField OB1;
  private JLabel OBJ_11_OBJ_11;
  private JLabel OBJ_13_OBJ_13;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private XRiTextField OB9;
  private XRiTextField OB10;
  private XRiTextField OB11;
  private XRiTextField OB12;
  private XRiTextField OB13;
  private XRiTextField OB14;
  private XRiTextField OB15;
  private XRiTextField OB16;
  private XRiTextField OB17;
  private XRiTextField OB18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
