//$$david$$ ££05/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vexp.VEXP02FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VEXP02FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15",
      "T16", "T17", "T18", "T19", "T20", "T21", "T22", };
  private String[] _T01_Title = { "", };
  private String[][] _T01_Data = { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", },
      { "L09", }, { "L10", }, { "L11", }, { "L12", }, { "L13", }, { "L14", }, { "L15", }, { "L16", }, { "L17", }, { "L18", }, { "L19", },
      { "L20", }, { "L21", }, { "L22", }, };
  private int[] _T01_Width = { 420, };
  
  public VEXP02FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(T01, T01.get_T01_Title_Data_Brut(), _T01_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    riMenu_bt1.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "Enter", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "Enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    WRECHA = new XRiTextField();
    WRECHI = new XRiTextField();
    OBJ_14_OBJ_14 = new JLabel();
    OBJ_16_OBJ_16 = new JLabel();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(615, 530));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("Options");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Gestion des param\u00e8tres");
              riSousMenu_bt1.setToolTipText("Gestion des param\u00e8tres");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Param\u00e8tres :"));
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- T01 ----
            T01.setComponentPopupMenu(BTD);
            T01.setName("T01");
            T01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                T01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(T01);
          }

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");

          //---- WRECHA ----
          WRECHA.setName("WRECHA");

          //---- WRECHI ----
          WRECHI.setName("WRECHI");

          //---- OBJ_14_OBJ_14 ----
          OBJ_14_OBJ_14.setText("Indicatif");
          OBJ_14_OBJ_14.setName("OBJ_14_OBJ_14");

          //---- OBJ_16_OBJ_16 ----
          OBJ_16_OBJ_16.setText("Cl\u00e9");
          OBJ_16_OBJ_16.setName("OBJ_16_OBJ_16");

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(OBJ_14_OBJ_14, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(WRECHI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(OBJ_16_OBJ_16, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                    .addGap(26, 26, 26)
                    .addComponent(WRECHA, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(16, Short.MAX_VALUE))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                    .addGap(25, 25, 25)
                    .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_14_OBJ_14))
                  .addComponent(WRECHI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_16_OBJ_16))
                  .addComponent(WRECHA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- menuItem1 ----
      menuItem1.setText("Choisir");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField WRECHA;
  private XRiTextField WRECHI;
  private JLabel OBJ_14_OBJ_14;
  private JLabel OBJ_16_OBJ_16;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
