
package ri.serien.libecranrpg.vexp.VEXP30FM;
// Nom Fichier: i_VEXP30FM_FMTB1_FMTF1_242.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VEXP30FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] DMTPJ_Value = { "", "1", "2", "3", };
  private String[] DMURG_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", };
  private String[] DMTYP3_Value = { "", "1", "2", "3", "4", "5", };
  private String[] DMTYP2_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", };
  private String[] DMTYP1_Value = { "", "1", "2", "3", };
  
  public VEXP30FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    DMTPJ.setValeurs(DMTPJ_Value, null);
    DMURG.setValeurs(DMURG_Value, null);
    DMTYP3.setValeurs(DMTYP3_Value, null);
    DMTYP2.setValeurs(DMTYP2_Value, null);
    DMTYP1.setValeurs(DMTYP1_Value, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // attribuer les boutons aux panels
    p_doc.setRightDecoration(OBJ_124);
    p_client.setRightDecoration(TCI1);
    p_contact.setRightDecoration(TCI2);
    p_version.setRightDecoration(TCI3);
    p_demande.setRightDecoration(TCI4);
    p_reponse.setRightDecoration(TCI5);
    p_technique.setRightDecoration(TCI6);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_Bib.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_86_OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SCTSM@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // les combobox
    // DMTPJ.setSelectedIndex(getIndice("DMTPJ", DMTPJ_Value));
    // DMURG.setSelectedIndex(getIndice("DMURG", DMURG_Value));
    // DMTYP3.setSelectedIndex(getIndice("DMTYP3", DMTYP3_Value));
    // DMTYP2.setSelectedIndex(getIndice("DMTYP2", DMTYP2_Value));
    // DMTYP1.setSelectedIndex(getIndice("DMTYP1", DMTYP1_Value));
    
    DMNPJ.setEnabled(lexique.isPresent("DMNPJ"));
    DMHRES.setEnabled(lexique.isPresent("DMHRES"));
    DMHREM.setEnabled(lexique.isPresent("DMHREM"));
    DMHREH.setEnabled(lexique.isPresent("DMHREH"));
    DMETA.setVisible(lexique.isPresent("DMETA"));
    DMCIN2.setVisible(lexique.isPresent("DMCIN2"));
    DMCIN1.setVisible(lexique.isPresent("DMCIN1"));
    WPLUST.setVisible(interpreteurD.analyseExpression("@WPLUST@").equalsIgnoreCase("+"));
    WPLUSR.setVisible(interpreteurD.analyseExpression("@WPLUSR@").equalsIgnoreCase("+"));
    WPLUSD.setVisible(interpreteurD.analyseExpression("@WPLUSD@").equalsIgnoreCase("+"));
    DMHRFS.setVisible(lexique.isPresent("DMHRFS"));
    WINTL.setVisible(lexique.isPresent("WINTL"));
    // DMDTFS.setVisible( lexique.isPresent("DMDTFS"));
    DMNOR.setVisible(lexique.isPresent("DMNOR"));
    TCI6.setEnabled(lexique.isPresent("TCI6"));
    TCI5.setEnabled(lexique.isPresent("TCI5"));
    TCI4.setEnabled(lexique.isPresent("TCI4"));
    TCI3.setEnabled(lexique.isPresent("TCI3"));
    TCI2.setEnabled(lexique.isPresent("TCI2"));
    TCI1.setEnabled(lexique.isPresent("TCI1"));
    OBJ_86_OBJ_86.setVisible(lexique.isPresent("SCTSM"));
    OBJ_124.setVisible(!interpreteurD.analyseExpression("@RACINE@").trim().equalsIgnoreCase(""));
    DMFAX.setVisible(lexique.isPresent("DMFAX"));
    DMTEL.setVisible(lexique.isPresent("DMTEL"));
    DMPAC.setVisible(lexique.isPresent("DMPAC"));
    DMNET.setVisible(lexique.isPresent("DMNET"));
    DMTT2.setVisible(lexique.isPresent("DMTT2"));
    DMTT1.setVisible(lexique.isPresent("DMTT1"));
    DMTR2.setVisible(lexique.isPresent("DMTR2"));
    DMTR1.setVisible(lexique.isPresent("DMTR1"));
    DMTD2.setVisible(lexique.isPresent("DMTD2"));
    DMTD1.setVisible(lexique.isPresent("DMTD1"));
    OBJ_87_OBJ_87.setVisible(interpreteurD.analyseExpression("@BLAC@").equalsIgnoreCase("*** Black Liste ***"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    OBJ_126.setIcon(lexique.chargerImage("images/avantbl.gif", true));
    OBJ_74.setIcon(lexique.chargerImage("images/tel.gif", true));
    WPLUST.setIcon(lexique.chargerImage("images/rouge2.gif", true));
    WPLUSR.setIcon(lexique.chargerImage("images/rouge2.gif", true));
    WPLUSD.setIcon(lexique.chargerImage("images/rouge2.gif", true));
    l_tel.setIcon(lexique.chargerImage("images/tel.png", true));
    l_fax.setIcon(lexique.chargerImage("images/fax.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    ImageIcon loupe = lexique.chargerImage("images/loupe.png", true);
    // OBJ_124.setIcon(lexique.getImage("images/lies3.gif", true));
    // TCI6.setIcon(lexique.getImage("images/travail.gif", true));
    // TCI5.setIcon(lexique.getImage("images/dupli.gif", true));
    // TCI4.setIcon(lexique.getImage("images/compb.gif", true));
    // TCI3.setIcon(lexique.getImage("images/trvetr.gif", true));
    // TCI2.setIcon(lexique.getImage("images/conts.gif", true));
    // TCI1.setIcon(lexique.getImage("images/client.gif", true));
    OBJ_124.setIcon(loupe);
    TCI1.setIcon(loupe);
    TCI2.setIcon(loupe);
    TCI3.setIcon(loupe);
    TCI4.setIcon(loupe);
    TCI5.setIcon(loupe);
    TCI6.setIcon(loupe);
    OBJ_47.setIcon(loupe);
    OBJ_52.setIcon(loupe);
    OBJ_56.setIcon(loupe);
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SERIE M - INTERVENTIONS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // lexique.HostFieldPutData("DMTPJ", 0, DMTPJ_Value[DMTPJ.getSelectedIndex()]);
    // lexique.HostFieldPutData("DMURG", 0, DMURG_Value[DMURG.getSelectedIndex()]);
    // lexique.HostFieldPutData("DMTYP3", 0, DMTYP3_Value[DMTYP3.getSelectedIndex()]);
    // lexique.HostFieldPutData("DMTYP2", 0, DMTYP2_Value[DMTYP2.getSelectedIndex()]);
    // lexique.HostFieldPutData("DMTYP1", 0, DMTYP1_Value[DMTYP1.getSelectedIndex()]);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_124ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@DMCLI@", false);
  }
  
  private void TCI1ActionPerformed(ActionEvent e) {
    // ScriptCall("G_FCB")
    if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "1");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    // ScriptCall("G_FCB")
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "2");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    // ScriptCall("G_FCB")
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "3");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    // ScriptCall("G_FCB")
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "4");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI5ActionPerformed(ActionEvent e) {
    // ScriptCall("G_FCB")
    if (lexique.HostFieldGetData("TCI5").trim().equals("")) {
      lexique.HostFieldPutData("TCI5", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "5");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI6ActionPerformed(ActionEvent e) {
    // ScriptCall("G_FCB")
    if (lexique.HostFieldGetData("TCI6").trim().equals("")) {
      lexique.HostFieldPutData("TCI6", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "6");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    // PcCommand(4, "Numeroteur.exe " +Chr(34)+@"@CLTEL@"+Chr(34)+ " "+Chr(34)+@"@CLNOM@"+Chr(34)+" "+"1 "+
    // @"@&PREFIXE@")
  }
  
  private void OBJ_126ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_Bib = new JLabel();
    INDNIN = new XRiTextField();
    OBJ_30_OBJ_30 = new JLabel();
    DMDTEX = new XRiCalendrier();
    OBJ_31_OBJ_31 = new JLabel();
    OBJ_32_OBJ_32 = new JLabel();
    DMNOR = new XRiTextField();
    DMHREH = new XRiTextField();
    DMHREM = new XRiTextField();
    DMHRES = new XRiTextField();
    P_Centre = new JPanel();
    panel3 = new JPanel();
    p_inter = new JXTitledPanel();
    DMTYP1 = new XRiComboBox();
    DMTYP2 = new XRiComboBox();
    DMTYP3 = new XRiComboBox();
    DMURG = new XRiComboBox();
    PELIB1 = new XRiTextField();
    PELIB2 = new XRiTextField();
    LIETA = new XRiTextField();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    DMCIN1 = new XRiTextField();
    DMCIN2 = new XRiTextField();
    DMETA = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    OBJ_47 = new JButton();
    OBJ_52 = new JButton();
    OBJ_56 = new JButton();
    DMDTFS = new XRiCalendrier();
    OBJ_59_OBJ_59 = new JLabel();
    DMHRFS = new XRiTextField();
    label4 = new JLabel();
    p_doc = new JXTitledPanel();
    OBJ_46_OBJ_46 = new JLabel();
    DMNPJ = new XRiTextField();
    OBJ_50_OBJ_50 = new JLabel();
    DMTPJ = new XRiComboBox();
    p_client = new JXTitledPanel();
    CLNOM = new XRiTextField();
    DMCLC = new XRiTextField();
    DMCLI = new XRiTextField();
    DMLIV = new XRiTextField();
    p_contact = new JXTitledPanel();
    DMNET = new XRiTextField();
    DMPAC = new XRiTextField();
    DMTEL = new XRiTextField();
    DMFAX = new XRiTextField();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    l_tel = new JLabel();
    l_fax = new JLabel();
    p_version = new JXTitledPanel();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    p_demande = new JXTitledPanel();
    DMTD1 = new XRiTextField();
    DMTD2 = new XRiTextField();
    WPLUSD = new JLabel();
    p_reponse = new JXTitledPanel();
    DMTR1 = new XRiTextField();
    DMTR2 = new XRiTextField();
    WPLUSR = new JLabel();
    p_technique = new JXTitledPanel();
    DMTT1 = new XRiTextField();
    DMTT2 = new XRiTextField();
    WPLUST = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    TCI1 = new JButton();
    TCI2 = new JButton();
    OBJ_74 = new JButton();
    TCI3 = new JButton();
    TCI4 = new JButton();
    TCI5 = new JButton();
    TCI6 = new JButton();
    OBJ_62_OBJ_62 = new JLabel();
    WINTL = new XRiTextField();
    OBJ_126 = new JButton();
    OBJ_124 = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- l_Bib ----
        l_Bib.setFont(new Font("sansserif", Font.BOLD, 12));
        l_Bib.setName("l_Bib");

        //---- INDNIN ----
        INDNIN.setComponentPopupMenu(BTD);
        INDNIN.setName("INDNIN");

        //---- OBJ_30_OBJ_30 ----
        OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");

        //---- DMDTEX ----
        DMDTEX.setName("DMDTEX");

        //---- OBJ_31_OBJ_31 ----
        OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");

        //---- OBJ_32_OBJ_32 ----
        OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

        //---- DMNOR ----
        DMNOR.setName("DMNOR");

        //---- DMHREH ----
        DMHREH.setName("DMHREH");

        //---- DMHREM ----
        DMHREM.setName("DMHREM");

        //---- DMHRES ----
        DMHRES.setName("DMHRES");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
              .addGap(15, 15, 15)
              .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(INDNIN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
              .addGap(40, 40, 40)
              .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addComponent(DMDTEX, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addGap(25, 25, 25)
              .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
              .addGap(4, 4, 4)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(DMHREH, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(DMHREM, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addComponent(DMHRES, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
              .addGap(18, 18, 18)
              .addComponent(DMNOR, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 168, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(6, 6, 6)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_30_OBJ_30))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(INDNIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_31_OBJ_31))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(DMDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_32_OBJ_32))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(DMHREH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(DMHREM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(DMHRES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DMNOR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(bt_Fonctions))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(new BorderLayout());

      //======== panel3 ========
      {
        panel3.setName("panel3");

        //======== p_inter ========
        {
          p_inter.setName("p_inter");
          Container p_interContentContainer = p_inter.getContentContainer();
          p_interContentContainer.setLayout(null);

          //---- DMTYP1 ----
          DMTYP1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Mat\u00e9riel",
            "Logiciel",
            "Autres"
          }));
          DMTYP1.setComponentPopupMenu(BTD);
          DMTYP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DMTYP1.setName("DMTYP1");
          p_interContentContainer.add(DMTYP1);
          DMTYP1.setBounds(165, 12, 120, DMTYP1.getPreferredSize().height);

          //---- DMTYP2 ----
          DMTYP2.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "AS/400",
            "Serveur",
            "PC",
            "Imprimante",
            "Modem",
            "Faisabilit\u00e9",
            "Intervention",
            "Remarque"
          }));
          DMTYP2.setComponentPopupMenu(BTD);
          DMTYP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DMTYP2.setName("DMTYP2");
          p_interContentContainer.add(DMTYP2);
          DMTYP2.setBounds(305, 12, 120, DMTYP2.getPreferredSize().height);

          //---- DMTYP3 ----
          DMTYP3.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Anosys",
            "Message syst\u00e8me",
            "Blocage",
            "Graphique",
            "Autre"
          }));
          DMTYP3.setComponentPopupMenu(BTD);
          DMTYP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DMTYP3.setName("DMTYP3");
          p_interContentContainer.add(DMTYP3);
          DMTYP3.setBounds(445, 12, 160, DMTYP3.getPreferredSize().height);

          //---- DMURG ----
          DMURG.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Au plus t\u00f4t",
            "Voir texte",
            "Avant la fin de la journ\u00e9e",
            "Avant la fin de la semaine",
            "Avant la fin du mois",
            "Avant la fin de l'ann\u00e9e",
            "En attente de traitement",
            "A l'occasion"
          }));
          DMURG.setComponentPopupMenu(BTD);
          DMURG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DMURG.setName("DMURG");
          p_interContentContainer.add(DMURG);
          DMURG.setBounds(165, 42, 260, DMURG.getPreferredSize().height);

          //---- PELIB1 ----
          PELIB1.setName("PELIB1");
          p_interContentContainer.add(PELIB1);
          PELIB1.setBounds(240, 70, 144, PELIB1.getPreferredSize().height);

          //---- PELIB2 ----
          PELIB2.setName("PELIB2");
          p_interContentContainer.add(PELIB2);
          PELIB2.setBounds(240, 100, 144, PELIB2.getPreferredSize().height);

          //---- LIETA ----
          LIETA.setName("LIETA");
          p_interContentContainer.add(LIETA);
          LIETA.setBounds(240, 131, 144, LIETA.getPreferredSize().height);

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
          p_interContentContainer.add(OBJ_36_OBJ_36);
          OBJ_36_OBJ_36.setBounds(15, 15, 117, 20);

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          p_interContentContainer.add(OBJ_42_OBJ_42);
          OBJ_42_OBJ_42.setBounds(15, 45, 55, 20);

          //---- DMCIN1 ----
          DMCIN1.setComponentPopupMenu(BTD);
          DMCIN1.setName("DMCIN1");
          p_interContentContainer.add(DMCIN1);
          DMCIN1.setBounds(196, 70, 40, DMCIN1.getPreferredSize().height);

          //---- DMCIN2 ----
          DMCIN2.setComponentPopupMenu(BTD);
          DMCIN2.setName("DMCIN2");
          p_interContentContainer.add(DMCIN2);
          DMCIN2.setBounds(196, 100, 40, DMCIN2.getPreferredSize().height);

          //---- DMETA ----
          DMETA.setComponentPopupMenu(BTD);
          DMETA.setName("DMETA");
          p_interContentContainer.add(DMETA);
          DMETA.setBounds(196, 131, 40, DMETA.getPreferredSize().height);

          //---- label1 ----
          label1.setName("label1");
          p_interContentContainer.add(label1);
          label1.setBounds(15, 75, 80, 20);

          //---- label2 ----
          label2.setName("label2");
          p_interContentContainer.add(label2);
          label2.setBounds(15, 105, 80, 20);

          //---- label3 ----
          label3.setName("label3");
          p_interContentContainer.add(label3);
          label3.setBounds(15, 135, 110, 20);

          //---- OBJ_47 ----
          OBJ_47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_47.setName("OBJ_47");
          OBJ_47.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_47ActionPerformed(e);
            }
          });
          p_interContentContainer.add(OBJ_47);
          OBJ_47.setBounds(165, 70, 28, 28);

          //---- OBJ_52 ----
          OBJ_52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_52.setName("OBJ_52");
          OBJ_52.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_52ActionPerformed(e);
            }
          });
          p_interContentContainer.add(OBJ_52);
          OBJ_52.setBounds(165, 100, 28, 28);

          //---- OBJ_56 ----
          OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_56.setName("OBJ_56");
          OBJ_56.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_56ActionPerformed(e);
            }
          });
          p_interContentContainer.add(OBJ_56);
          OBJ_56.setBounds(165, 130, 28, 28);

          //---- DMDTFS ----
          DMDTFS.setComponentPopupMenu(BTD);
          DMDTFS.setName("DMDTFS");
          p_interContentContainer.add(DMDTFS);
          DMDTFS.setBounds(490, 100, 115, DMDTFS.getPreferredSize().height);

          //---- OBJ_59_OBJ_59 ----
          OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");
          p_interContentContainer.add(OBJ_59_OBJ_59);
          OBJ_59_OBJ_59.setBounds(410, 104, 75, 20);

          //---- DMHRFS ----
          DMHRFS.setComponentPopupMenu(BTD);
          DMHRFS.setName("DMHRFS");
          p_interContentContainer.add(DMHRFS);
          DMHRFS.setBounds(490, 131, 45, DMHRFS.getPreferredSize().height);

          //---- label4 ----
          label4.setName("label4");
          p_interContentContainer.add(label4);
          label4.setBounds(410, 135, 80, 20);
        }

        //======== p_doc ========
        {
          p_doc.setName("p_doc");
          Container p_docContentContainer = p_doc.getContentContainer();
          p_docContentContainer.setLayout(null);

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          p_docContentContainer.add(OBJ_46_OBJ_46);
          OBJ_46_OBJ_46.setBounds(20, 15, 135, 20);

          //---- DMNPJ ----
          DMNPJ.setComponentPopupMenu(BTD);
          DMNPJ.setName("DMNPJ");
          p_docContentContainer.add(DMNPJ);
          DMNPJ.setBounds(165, 11, 20, DMNPJ.getPreferredSize().height);

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
          p_docContentContainer.add(OBJ_50_OBJ_50);
          OBJ_50_OBJ_50.setBounds(20, 45, 91, 20);

          //---- DMTPJ ----
          DMTPJ.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Mail",
            "Fax",
            "Autre"
          }));
          DMTPJ.setComponentPopupMenu(BTD);
          DMTPJ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DMTPJ.setName("DMTPJ");
          p_docContentContainer.add(DMTPJ);
          DMTPJ.setBounds(115, 42, 70, DMTPJ.getPreferredSize().height);
        }

        //======== p_client ========
        {
          p_client.setName("p_client");
          Container p_clientContentContainer = p_client.getContentContainer();
          p_clientContentContainer.setLayout(null);

          //---- CLNOM ----
          CLNOM.setName("CLNOM");
          p_clientContentContainer.add(CLNOM);
          CLNOM.setBounds(15, 40, 265, CLNOM.getPreferredSize().height);

          //---- DMCLC ----
          DMCLC.setComponentPopupMenu(BTD);
          DMCLC.setName("DMCLC");
          p_clientContentContainer.add(DMCLC);
          DMCLC.setBounds(119, 10, 160, DMCLC.getPreferredSize().height);

          //---- DMCLI ----
          DMCLI.setComponentPopupMenu(BTD);
          DMCLI.setName("DMCLI");
          p_clientContentContainer.add(DMCLI);
          DMCLI.setBounds(15, 10, 65, DMCLI.getPreferredSize().height);

          //---- DMLIV ----
          DMLIV.setComponentPopupMenu(BTD);
          DMLIV.setName("DMLIV");
          p_clientContentContainer.add(DMLIV);
          DMLIV.setBounds(82, 10, 35, DMLIV.getPreferredSize().height);
        }

        //======== p_contact ========
        {
          p_contact.setName("p_contact");
          Container p_contactContentContainer = p_contact.getContentContainer();
          p_contactContentContainer.setLayout(null);

          //---- DMNET ----
          DMNET.setComponentPopupMenu(BTD);
          DMNET.setName("DMNET");
          p_contactContentContainer.add(DMNET);
          DMNET.setBounds(80, 40, 267, DMNET.getPreferredSize().height);

          //---- DMPAC ----
          DMPAC.setComponentPopupMenu(BTD);
          DMPAC.setName("DMPAC");
          p_contactContentContainer.add(DMPAC);
          DMPAC.setBounds(80, 11, 211, DMPAC.getPreferredSize().height);

          //---- DMTEL ----
          DMTEL.setComponentPopupMenu(BTD);
          DMTEL.setName("DMTEL");
          p_contactContentContainer.add(DMTEL);
          DMTEL.setBounds(405, 11, 140, DMTEL.getPreferredSize().height);

          //---- DMFAX ----
          DMFAX.setComponentPopupMenu(BTD);
          DMFAX.setName("DMFAX");
          p_contactContentContainer.add(DMFAX);
          DMFAX.setBounds(405, 40, 140, DMFAX.getPreferredSize().height);

          //---- OBJ_75_OBJ_75 ----
          OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
          p_contactContentContainer.add(OBJ_75_OBJ_75);
          OBJ_75_OBJ_75.setBounds(20, 15, 32, 20);

          //---- OBJ_79_OBJ_79 ----
          OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");
          p_contactContentContainer.add(OBJ_79_OBJ_79);
          OBJ_79_OBJ_79.setBounds(20, 44, 28, 20);

          //---- l_tel ----
          l_tel.setName("l_tel");
          p_contactContentContainer.add(l_tel);
          l_tel.setBounds(375, 13, 24, 24);

          //---- l_fax ----
          l_fax.setName("l_fax");
          p_contactContentContainer.add(l_fax);
          l_fax.setBounds(375, 42, 24, 24);
        }

        //======== p_version ========
        {
          p_version.setName("p_version");
          Container p_versionContentContainer = p_version.getContentContainer();
          p_versionContentContainer.setLayout(null);

          //---- OBJ_85_OBJ_85 ----
          OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
          p_versionContentContainer.add(OBJ_85_OBJ_85);
          OBJ_85_OBJ_85.setBounds(20, 15, 60, 20);

          //---- OBJ_86_OBJ_86 ----
          OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
          p_versionContentContainer.add(OBJ_86_OBJ_86);
          OBJ_86_OBJ_86.setBounds(80, 15, 77, 20);

          //---- OBJ_87_OBJ_87 ----
          OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
          p_versionContentContainer.add(OBJ_87_OBJ_87);
          OBJ_87_OBJ_87.setBounds(20, 45, 190, 20);
        }

        //======== p_demande ========
        {
          p_demande.setName("p_demande");
          Container p_demandeContentContainer = p_demande.getContentContainer();
          p_demandeContentContainer.setLayout(null);

          //---- DMTD1 ----
          DMTD1.setComponentPopupMenu(BTD);
          DMTD1.setName("DMTD1");
          p_demandeContentContainer.add(DMTD1);
          DMTD1.setBounds(15, 10, 400, DMTD1.getPreferredSize().height);

          //---- DMTD2 ----
          DMTD2.setComponentPopupMenu(BTD);
          DMTD2.setName("DMTD2");
          p_demandeContentContainer.add(DMTD2);
          DMTD2.setBounds(15, 40, 400, DMTD2.getPreferredSize().height);

          //---- WPLUSD ----
          WPLUSD.setIcon(new ImageIcon("images/rouge2.gif"));
          WPLUSD.setName("WPLUSD");
          p_demandeContentContainer.add(WPLUSD);
          WPLUSD.setBounds(420, 30, 24, 20);
        }

        //======== p_reponse ========
        {
          p_reponse.setName("p_reponse");
          Container p_reponseContentContainer = p_reponse.getContentContainer();
          p_reponseContentContainer.setLayout(null);

          //---- DMTR1 ----
          DMTR1.setComponentPopupMenu(BTD);
          DMTR1.setName("DMTR1");
          p_reponseContentContainer.add(DMTR1);
          DMTR1.setBounds(15, 10, 400, DMTR1.getPreferredSize().height);

          //---- DMTR2 ----
          DMTR2.setComponentPopupMenu(BTD);
          DMTR2.setName("DMTR2");
          p_reponseContentContainer.add(DMTR2);
          DMTR2.setBounds(15, 40, 400, DMTR2.getPreferredSize().height);

          //---- WPLUSR ----
          WPLUSR.setIcon(new ImageIcon("images/rouge2.gif"));
          WPLUSR.setName("WPLUSR");
          p_reponseContentContainer.add(WPLUSR);
          WPLUSR.setBounds(420, 30, 24, 20);
        }

        //======== p_technique ========
        {
          p_technique.setName("p_technique");
          Container p_techniqueContentContainer = p_technique.getContentContainer();
          p_techniqueContentContainer.setLayout(null);

          //---- DMTT1 ----
          DMTT1.setComponentPopupMenu(BTD);
          DMTT1.setName("DMTT1");
          p_techniqueContentContainer.add(DMTT1);
          DMTT1.setBounds(15, 10, 400, DMTT1.getPreferredSize().height);

          //---- DMTT2 ----
          DMTT2.setComponentPopupMenu(BTD);
          DMTT2.setName("DMTT2");
          p_techniqueContentContainer.add(DMTT2);
          DMTT2.setBounds(480, 10, 400, DMTT2.getPreferredSize().height);

          //---- WPLUST ----
          WPLUST.setIcon(new ImageIcon("images/rouge2.gif"));
          WPLUST.setName("WPLUST");
          p_techniqueContentContainer.add(WPLUST);
          WPLUST.setBounds(885, 14, 24, 20);
        }

        GroupLayout panel3Layout = new GroupLayout(panel3);
        panel3.setLayout(panel3Layout);
        panel3Layout.setHorizontalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(p_inter, GroupLayout.PREFERRED_SIZE, 675, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(p_doc, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                    .addComponent(p_version, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(p_client, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(p_contact, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(p_demande, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(p_reponse, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE))
                .addComponent(p_technique, GroupLayout.PREFERRED_SIZE, 925, GroupLayout.PREFERRED_SIZE)))
        );
        panel3Layout.setVerticalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(panel3Layout.createParallelGroup()
                .addComponent(p_inter, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(p_doc, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                  .addComponent(p_version, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)))
              .addGap(10, 10, 10)
              .addGroup(panel3Layout.createParallelGroup()
                .addComponent(p_client, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addComponent(p_contact, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
              .addGap(10, 10, 10)
              .addGroup(panel3Layout.createParallelGroup()
                .addComponent(p_demande, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addComponent(p_reponse, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
              .addGap(10, 10, 10)
              .addComponent(p_technique, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
        );
      }
      P_Centre.add(panel3, BorderLayout.CENTER);
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //---- TCI1 ----
    TCI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI1.setName("TCI1");
    TCI1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI1ActionPerformed(e);
      }
    });

    //---- TCI2 ----
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- OBJ_74 ----
    OBJ_74.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_74.setName("OBJ_74");
    OBJ_74.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_74ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI4 ----
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });

    //---- TCI5 ----
    TCI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI5.setName("TCI5");
    TCI5.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI5ActionPerformed(e);
      }
    });

    //---- TCI6 ----
    TCI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI6.setName("TCI6");
    TCI6.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI6ActionPerformed(e);
      }
    });

    //---- OBJ_62_OBJ_62 ----
    OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");

    //---- WINTL ----
    WINTL.setComponentPopupMenu(BTD);
    WINTL.setName("WINTL");

    //---- OBJ_126 ----
    OBJ_126.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_126.setName("OBJ_126");
    OBJ_126.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_126ActionPerformed(e);
      }
    });

    //---- OBJ_124 ----
    OBJ_124.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_124.setName("OBJ_124");
    OBJ_124.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_124ActionPerformed(e);
      }
    });

    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_Bib;
  private XRiTextField INDNIN;
  private JLabel OBJ_30_OBJ_30;
  private XRiCalendrier DMDTEX;
  private JLabel OBJ_31_OBJ_31;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField DMNOR;
  private XRiTextField DMHREH;
  private XRiTextField DMHREM;
  private XRiTextField DMHRES;
  private JPanel P_Centre;
  private JPanel panel3;
  private JXTitledPanel p_inter;
  private XRiComboBox DMTYP1;
  private XRiComboBox DMTYP2;
  private XRiComboBox DMTYP3;
  private XRiComboBox DMURG;
  private XRiTextField PELIB1;
  private XRiTextField PELIB2;
  private XRiTextField LIETA;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_42_OBJ_42;
  private XRiTextField DMCIN1;
  private XRiTextField DMCIN2;
  private XRiTextField DMETA;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JButton OBJ_47;
  private JButton OBJ_52;
  private JButton OBJ_56;
  private XRiCalendrier DMDTFS;
  private JLabel OBJ_59_OBJ_59;
  private XRiTextField DMHRFS;
  private JLabel label4;
  private JXTitledPanel p_doc;
  private JLabel OBJ_46_OBJ_46;
  private XRiTextField DMNPJ;
  private JLabel OBJ_50_OBJ_50;
  private XRiComboBox DMTPJ;
  private JXTitledPanel p_client;
  private XRiTextField CLNOM;
  private XRiTextField DMCLC;
  private XRiTextField DMCLI;
  private XRiTextField DMLIV;
  private JXTitledPanel p_contact;
  private XRiTextField DMNET;
  private XRiTextField DMPAC;
  private XRiTextField DMTEL;
  private XRiTextField DMFAX;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_79_OBJ_79;
  private JLabel l_tel;
  private JLabel l_fax;
  private JXTitledPanel p_version;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_86_OBJ_86;
  private JLabel OBJ_87_OBJ_87;
  private JXTitledPanel p_demande;
  private XRiTextField DMTD1;
  private XRiTextField DMTD2;
  private JLabel WPLUSD;
  private JXTitledPanel p_reponse;
  private XRiTextField DMTR1;
  private XRiTextField DMTR2;
  private JLabel WPLUSR;
  private JXTitledPanel p_technique;
  private XRiTextField DMTT1;
  private XRiTextField DMTT2;
  private JLabel WPLUST;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JButton TCI1;
  private JButton TCI2;
  private JButton OBJ_74;
  private JButton TCI3;
  private JButton TCI4;
  private JButton TCI5;
  private JButton TCI6;
  private JLabel OBJ_62_OBJ_62;
  private XRiTextField WINTL;
  private JButton OBJ_126;
  private JButton OBJ_124;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
