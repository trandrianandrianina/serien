
package ri.serien.libecranrpg.vcgm.VCGM05FM;
// Nom Fichier: i_VCGM05FM_FMTA1_FMTF1_369.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM05FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] WSOLDE_Value = { "", "1", "2", "3", "4", };
  private String[] WSOLDE_Title = { "", "Tous", "Solde différent de zéro", "Débiteurs", "Créditeurs", };
  // private boolean constructionIsFinie = false;
  
  public VCGM05FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    
    // Ajout
    initDiverses();
    WSOLDE.setValeurs(WSOLDE_Value, WSOLDE_Title);
    SCAN2.setValeursSelection("X", " ");
    TIDX1.setValeurs("1", "RB");
    TIDX2.setValeurs("2", "RB");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    NCGX.requestFocus();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ @LIBPG@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA1@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA2@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA3@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA4@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA5@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // lexique.getConvertV01F(lexique.HostFieldGetData("V01F"),comboBox1);
    // WSOLDE.setSelectedIndex(getIndice("WSOLDE", WSOLDE_Value));
    
    // SCAN2.setVisible( lexique.isPresent("SCAN2"));
    // SCAN2.setSelected(lexique.HostFieldGetData("SCAN2").equalsIgnoreCase("X"));
    OBJ_91.setVisible(lexique.isPresent("LIBAA5"));
    OBJ_90.setVisible(lexique.isPresent("LIBAA4"));
    OBJ_89.setVisible(lexique.isPresent("LIBAA3"));
    OBJ_88.setVisible(lexique.isPresent("LIBAA2"));
    OBJ_87.setVisible(lexique.isPresent("LIBAA1"));
    // WCVAA1.setVisible( lexique.isPresent("WCVAA1"));
    // INDETB.setVisible( lexique.isPresent("INDETB"));
    OBJ_92.setVisible(lexique.isPresent("LIBAA6"));
    // WSANA1.setVisible( lexique.isPresent("WSANA1"));
    // ARG1.setVisible( lexique.isPresent("ARG1"));
    // WAXE6.setVisible( lexique.isPresent("WAXE6"));
    // WAXE5.setVisible( lexique.isPresent("WAXE5"));
    // WAXE4.setVisible( lexique.isPresent("WAXE4"));
    // WAXE3.setVisible( lexique.isPresent("WAXE3"));
    // WAXE2.setVisible( lexique.isPresent("WAXE2"));
    // WAXE1.setVisible( lexique.isPresent("WAXE1"));
    // NCGX.setVisible( lexique.isPresent("NCGX"));
    // WNATA1.setVisible( lexique.isPresent("WNATA1"));
    // WSFIN.setVisible( lexique.isPresent("WSFIN"));
    // WSDEB.setVisible( lexique.isPresent("WSDEB"));
    // ARG2.setVisible( lexique.isPresent("ARG2"));
    // OBJ_67.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // OBJ_62.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // WSOLDE.setVisible( lexique.isPresent("WSOLDE"));
    // WSOLDE.setSelectedIndex(lexique.getIndexTableau(WSOLDE_Value, lexique.HostFieldGetData("WSOLDE")));
    BT_RechEcr.setEnabled(!lexique.HostFieldGetData("ONGL3").trim().equalsIgnoreCase("X"));
    BT_RechTiers.setEnabled(!lexique.HostFieldGetData("ONGL1").trim().equalsIgnoreCase("X"));
    IN3ETB.setVisible(lexique.isTrue("56"));
    NC3X.setVisible(lexique.isTrue("56"));
    // OBJ_55.setEnabled(!interpreteurD.analyseExpression("@ONGL2@").trim().equalsIgnoreCase("X"));
    
    // TODO Icones
    BT_RechTiers.setIcon(lexique.chargerImage("images/rechercher48.png", true));
    BT_RechEcr.setIcon(lexique.chargerImage("images/rechercher48.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(INDETB.getText()));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Positionnement du curseur
    // NCGX.requestFocus();
    // NCGX.setRequestFocusEnabled(true);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (SCAN2.isSelected())
    // lexique.HostFieldPutData("SCAN2", 0, "X");
    // else
    // lexique.HostFieldPutData("SCAN2", 0, " ");
    // if (OBJ_67.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (OBJ_62.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // lexique.HostFieldPutData("WSOLDE", 0, WSOLDE_Value[WSOLDE.getSelectedIndex()]);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_RechTiersActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void BT_RechEcrActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    OBJ_6.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    IN3ETB = new XRiTextField();
    OBJ_45 = new JLabel();
    NCGX = new XRiTextField();
    NC3X = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    TIDX2 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    ARG1 = new XRiTextField();
    SCAN2 = new XRiCheckBox();
    SOLD1 = new JLabel();
    OBJ_72 = new JLabel();
    WSDEB = new XRiTextField();
    OBJ_74 = new JLabel();
    WSFIN = new XRiTextField();
    WSOLDE = new XRiComboBox();
    panel3 = new JPanel();
    OBJ_81 = new JLabel();
    OBJ_79 = new JLabel();
    WSANA1 = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_80 = new JLabel();
    WCVAA1 = new XRiTextField();
    OBJ_84 = new JLabel();
    WNATA1 = new XRiTextField();
    OBJ_98 = new JLabel();
    WAXE1 = new XRiTextField();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    WAXE2 = new XRiTextField();
    WAXE3 = new XRiTextField();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    WAXE4 = new XRiTextField();
    WAXE5 = new XRiTextField();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    WAXE6 = new XRiTextField();
    xHeader1 = new JXHeader();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JPopupMenu();
    OBJ_7 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    panel2 = new JPanel();
    BT_RechTiers = new JButton();
    BT_RechEcr = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("Recherche d'une fiche compte g\u00e9n\u00e9ral");
      xH_Titre.setDescription("FM@LOCGRP/+1/@ @LIBPG@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setMaximumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- OBJ_44 ----
        OBJ_44.setText("Soci\u00e9t\u00e9");
        OBJ_44.setName("OBJ_44");

        //---- INDETB ----
        INDETB.setComponentPopupMenu(BTD);
        INDETB.setName("INDETB");

        //---- IN3ETB ----
        IN3ETB.setComponentPopupMenu(BTD);
        IN3ETB.setToolTipText("Par duplication");
        IN3ETB.setName("IN3ETB");

        //---- OBJ_45 ----
        OBJ_45.setText("Num\u00e9ro de Compte");
        OBJ_45.setName("OBJ_45");

        //---- NCGX ----
        NCGX.setComponentPopupMenu(BTD);
        NCGX.setName("NCGX");

        //---- NC3X ----
        NC3X.setComponentPopupMenu(BTD);
        NC3X.setToolTipText("Par duplication");
        NC3X.setName("NC3X");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addGap(140, 140, 140)
              .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(NC3X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 309, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(bt_Fonctions)
                .addComponent(OBJ_44)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(NC3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(3, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Recherche des comptes g\u00e9n\u00e9raux"));
        panel1.setAutoscrolls(true);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- TIDX2 ----
        TIDX2.setText("Recherche Alphab\u00e9tique");
        TIDX2.setToolTipText("Tri\u00e9 par");
        TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX2.setName("TIDX2");
        panel1.add(TIDX2);
        TIDX2.setBounds(25, 35, 220, TIDX2.getPreferredSize().height);

        //---- TIDX1 ----
        TIDX1.setText("Num\u00e9ro de Compte");
        TIDX1.setToolTipText("Tri\u00e9 par");
        TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TIDX1.setName("TIDX1");
        panel1.add(TIDX1);
        TIDX1.setBounds(25, 65, 220, TIDX1.getPreferredSize().height);

        //---- ARG2 ----
        ARG2.setComponentPopupMenu(BTD);
        ARG2.setName("ARG2");
        panel1.add(ARG2);
        ARG2.setBounds(293, 30, 160, ARG2.getPreferredSize().height);

        //---- ARG1 ----
        ARG1.setComponentPopupMenu(BTD);
        ARG1.setName("ARG1");
        panel1.add(ARG1);
        ARG1.setBounds(293, 60, 60, ARG1.getPreferredSize().height);

        //---- SCAN2 ----
        SCAN2.setText("");
        SCAN2.setComponentPopupMenu(BTDA);
        SCAN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        SCAN2.setName("SCAN2");
        panel1.add(SCAN2);
        SCAN2.setBounds(460, 35, 31, SCAN2.getPreferredSize().height);

        //---- SOLD1 ----
        SOLD1.setText("Affichage du solde");
        SOLD1.setComponentPopupMenu(BTD);
        SOLD1.setName("SOLD1");
        panel1.add(SOLD1);
        SOLD1.setBounds(25, 95, 220, SOLD1.getPreferredSize().height);

        //---- OBJ_72 ----
        OBJ_72.setText("Solde compris entre");
        OBJ_72.setName("OBJ_72");
        panel1.add(OBJ_72);
        OBJ_72.setBounds(25, 125, 220, OBJ_72.getPreferredSize().height);

        //---- WSDEB ----
        WSDEB.setComponentPopupMenu(BTD);
        WSDEB.setName("WSDEB");
        panel1.add(WSDEB);
        WSDEB.setBounds(293, 120, 100, WSDEB.getPreferredSize().height);

        //---- OBJ_74 ----
        OBJ_74.setText("et");
        OBJ_74.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_74.setName("OBJ_74");
        panel1.add(OBJ_74);
        OBJ_74.setBounds(393, 125, 55, OBJ_74.getPreferredSize().height);

        //---- WSFIN ----
        WSFIN.setComponentPopupMenu(BTD);
        WSFIN.setName("WSFIN");
        panel1.add(WSFIN);
        WSFIN.setBounds(460, 120, 100, WSFIN.getPreferredSize().height);

        //---- WSOLDE ----
        WSOLDE.setComponentPopupMenu(BTD);
        WSOLDE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WSOLDE.setName("WSOLDE");
        panel1.add(WSOLDE);
        WSOLDE.setBounds(295, 90, 162, WSOLDE.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }

      //======== panel3 ========
      {
        panel3.setBorder(new TitledBorder("Filtres si analytique"));
        panel3.setForeground(Color.darkGray);
        panel3.setName("panel3");

        //---- OBJ_81 ----
        OBJ_81.setText("Section / Nature");
        OBJ_81.setName("OBJ_81");

        //---- OBJ_79 ----
        OBJ_79.setText("Section");
        OBJ_79.setName("OBJ_79");

        //---- WSANA1 ----
        WSANA1.setComponentPopupMenu(BTD);
        WSANA1.setName("WSANA1");

        //---- OBJ_86 ----
        OBJ_86.setText("ou");
        OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_86.setName("OBJ_86");

        //---- OBJ_80 ----
        OBJ_80.setText("Cl\u00e9");
        OBJ_80.setName("OBJ_80");

        //---- WCVAA1 ----
        WCVAA1.setComponentPopupMenu(BTD);
        WCVAA1.setName("WCVAA1");

        //---- OBJ_84 ----
        OBJ_84.setText("Nature");
        OBJ_84.setName("OBJ_84");

        //---- WNATA1 ----
        WNATA1.setComponentPopupMenu(BTD);
        WNATA1.setName("WNATA1");

        //---- OBJ_98 ----
        OBJ_98.setText("Axes");
        OBJ_98.setName("OBJ_98");

        //---- WAXE1 ----
        WAXE1.setComponentPopupMenu(BTD);
        WAXE1.setName("WAXE1");

        //---- OBJ_87 ----
        OBJ_87.setText("@LIBAA1@");
        OBJ_87.setForeground(Color.darkGray);
        OBJ_87.setName("OBJ_87");

        //---- OBJ_88 ----
        OBJ_88.setText("@LIBAA2@");
        OBJ_88.setForeground(Color.darkGray);
        OBJ_88.setName("OBJ_88");

        //---- WAXE2 ----
        WAXE2.setComponentPopupMenu(BTD);
        WAXE2.setName("WAXE2");

        //---- WAXE3 ----
        WAXE3.setComponentPopupMenu(BTD);
        WAXE3.setName("WAXE3");

        //---- OBJ_89 ----
        OBJ_89.setText("@LIBAA3@");
        OBJ_89.setForeground(Color.darkGray);
        OBJ_89.setName("OBJ_89");

        //---- OBJ_90 ----
        OBJ_90.setText("@LIBAA4@");
        OBJ_90.setForeground(Color.darkGray);
        OBJ_90.setName("OBJ_90");

        //---- WAXE4 ----
        WAXE4.setComponentPopupMenu(BTD);
        WAXE4.setName("WAXE4");

        //---- WAXE5 ----
        WAXE5.setComponentPopupMenu(BTD);
        WAXE5.setName("WAXE5");

        //---- OBJ_91 ----
        OBJ_91.setText("@LIBAA5@");
        OBJ_91.setForeground(Color.darkGray);
        OBJ_91.setName("OBJ_91");

        //---- OBJ_92 ----
        OBJ_92.setForeground(Color.darkGray);
        OBJ_92.setText("@LIBAA6@");
        OBJ_92.setName("OBJ_92");

        //---- WAXE6 ----
        WAXE6.setComponentPopupMenu(BTD);
        WAXE6.setName("WAXE6");

        GroupLayout panel3Layout = new GroupLayout(panel3);
        panel3.setLayout(panel3Layout);
        panel3Layout.setHorizontalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(281, 281, 281)
                  .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(55, 55, 55)
                  .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
                  .addGap(50, 50, 50)
                  .addComponent(WSANA1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(WCVAA1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(WNATA1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(281, 281, 281)
                  .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
                  .addGap(50, 50, 50)
                  .addComponent(WAXE1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(WAXE2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(WAXE3, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(WAXE4, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(WAXE5, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(WAXE6, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(716, 716, 716)
                  .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
              .addContainerGap(167, Short.MAX_VALUE))
        );
        panel3Layout.setVerticalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGroup(panel3Layout.createParallelGroup()
                .addComponent(OBJ_79)
                .addComponent(OBJ_80))
              .addGap(4, 4, 4)
              .addGroup(panel3Layout.createParallelGroup()
                .addComponent(WSANA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(WCVAA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(WNATA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_81)
                    .addComponent(OBJ_86)
                    .addComponent(OBJ_84))))
              .addGap(12, 12, 12)
              .addGroup(panel3Layout.createParallelGroup()
                .addComponent(OBJ_87)
                .addComponent(OBJ_88)
                .addComponent(OBJ_89)
                .addComponent(OBJ_90)
                .addComponent(OBJ_91)
                .addComponent(OBJ_92))
              .addGap(9, 9, 9)
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_98))
                .addComponent(WAXE1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(WAXE2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(WAXE3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(WAXE4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(WAXE5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(WAXE6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        );
      }

      //---- xHeader1 ----
      xHeader1.setName("xHeader1");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(xHeader1, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 973, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(15, Short.MAX_VALUE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addComponent(xHeader1, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE)
            .addGap(25, 25, 25)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(189, 189, 189))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== OBJ_6 ========
    {
      OBJ_6.setName("OBJ_6");

      //---- OBJ_7 ----
      OBJ_7.setText("Fin de Travail");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_6.add(OBJ_7);

      //---- OBJ_9 ----
      OBJ_9.setText("R\u00e9afficher");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_6.add(OBJ_9);

      //---- OBJ_11 ----
      OBJ_11.setText("Fonctions");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_6.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Annuler");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_6.add(OBJ_12);
      OBJ_6.addSeparator();
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Invite");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Autres recherches"));
      panel2.setName("panel2");

      //---- BT_RechTiers ----
      BT_RechTiers.setToolTipText("Recherche  sur tiers");
      BT_RechTiers.setName("BT_RechTiers");
      BT_RechTiers.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_RechTiersActionPerformed(e);
        }
      });

      //---- BT_RechEcr ----
      BT_RechEcr.setToolTipText("Recherche d'\u00e9critures");
      BT_RechEcr.setName("BT_RechEcr");
      BT_RechEcr.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_RechEcrActionPerformed(e);
        }
      });

      GroupLayout panel2Layout = new GroupLayout(panel2);
      panel2.setLayout(panel2Layout);
      panel2Layout.setHorizontalGroup(
        panel2Layout.createParallelGroup()
          .addGroup(panel2Layout.createSequentialGroup()
            .addGap(66, 66, 66)
            .addComponent(BT_RechTiers, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
            .addGap(90, 90, 90)
            .addComponent(BT_RechEcr, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
      );
      panel2Layout.setVerticalGroup(
        panel2Layout.createParallelGroup()
          .addGroup(panel2Layout.createSequentialGroup()
            .addGap(25, 25, 25)
            .addGroup(panel2Layout.createParallelGroup()
              .addComponent(BT_RechTiers, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
              .addComponent(BT_RechEcr, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
      );
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private XRiTextField IN3ETB;
  private JLabel OBJ_45;
  private XRiTextField NCGX;
  private XRiTextField NC3X;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel P_Centre;
  private JPanel panel1;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX1;
  private XRiTextField ARG2;
  private XRiTextField ARG1;
  private XRiCheckBox SCAN2;
  private JLabel SOLD1;
  private JLabel OBJ_72;
  private XRiTextField WSDEB;
  private JLabel OBJ_74;
  private XRiTextField WSFIN;
  private XRiComboBox WSOLDE;
  private JPanel panel3;
  private JLabel OBJ_81;
  private JLabel OBJ_79;
  private XRiTextField WSANA1;
  private JLabel OBJ_86;
  private JLabel OBJ_80;
  private XRiTextField WCVAA1;
  private JLabel OBJ_84;
  private XRiTextField WNATA1;
  private JLabel OBJ_98;
  private XRiTextField WAXE1;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private XRiTextField WAXE2;
  private XRiTextField WAXE3;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private XRiTextField WAXE4;
  private XRiTextField WAXE5;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private XRiTextField WAXE6;
  private JXHeader xHeader1;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JPanel panel2;
  private JButton BT_RechTiers;
  private JButton BT_RechEcr;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
