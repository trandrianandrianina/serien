
package ri.serien.libecranrpg.vcgm.VCGM30FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM30FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WTEX1_Top =
      { "WTEX1", "WTEX2", "WTEX3", "WTEX4", "WTEX5", "WTEX6", "WTEX7", "WTEX8", "WTEX9", "WTEX10", "WTEX11", "WTEX12", };
  private String[] _WTEX1_Title = { "Mois", "TIT1", "TIT2", "TIT3", "TIT4", "TIT5", };
  private String[][] _WTEX1_Data =
      { { "WTM01", "WT01", "WT13", "WT25", "WT37", "WT49", }, { "WTM02", "WT02", "WT14", "WT26", "WT38", "WT50", },
          { "WTM03", "WT03", "WT15", "WT27", "WT39", "WT51", }, { "WTM04", "WT04", "WT16", "WT28", "WT40", "WT52", },
          { "WTM05", "WT05", "WT17", "WT29", "WT41", "WT53", }, { "WTM06", "WT06", "WT18", "WT30", "WT42", "WT54", },
          { "WTM07", "WT07", "WT19", "WT31", "WT43", "WT55", }, { "WTM08", "WT08", "WT20", "WT32", "WT44", "WT56", },
          { "WTM09", "WT09", "WT21", "WT33", "WT45", "WT57", }, { "WTM10", "WT10", "WT22", "WT34", "WT46", "WT58", },
          { "WTM11", "WT11", "WT23", "WT35", "WT47", "WT59", }, { "WTM12", "WT12", "WT24", "WT36", "WT48", "WT60", }, };
  private int[] _WTEX1_Width = { 66, 90, 90, 90, 90, 90, };
  private boolean[][] _WTEX1_TranslationTable = { { false, false, false, false, false, false },
      { true, false, false, false, false, false }, { true, false, false, false, false, false },
      { true, false, false, false, false, false }, { true, false, false, false, false, false },
      { true, false, false, false, false, false }, { true, false, false, false, false, false },
      { true, false, false, false, false, false }, { true, false, false, false, false, false },
      { true, false, false, false, false, false }, { true, false, false, false, false, false },
      { true, false, false, false, false, false }, { true, false, false, false, false, false }, };
  private int[] _WTEX1_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT,
      SwingConstants.RIGHT, SwingConstants.RIGHT };
  private Color[][] _WTEX1_Text_Color = new Color[12][6];
  
  private Color[][] _WTEX1_Fond_Color = null;
  
  public VCGM30FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTEX1.setAspectTable(_WTEX1_Top, _WTEX1_Title, _WTEX1_Data, _WTEX1_Width, false, _WTEX1_Justification, _WTEX1_Text_Color,
        _WTEX1_Fond_Color, null);
    WTEX1.setUseTranslationTable(_WTEX1_TranslationTable);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBDV@")).trim());
    OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRAT58@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTEX1_Top, _LIST_Justification);
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_30.setVisible(lexique.isTrue("70"));
    INDNA2.setVisible(lexique.isTrue("70"));
    OBJ_29.setVisible(lexique.isTrue("99"));
    INDSAN.setVisible(lexique.isTrue("99"));
    INDNA1.setVisible(lexique.isTrue("70"));
    
    if (lexique.isTrue("51")) {
      label1.setText("Travail sur soldes");
    }
    if (lexique.isTrue("52")) {
      label1.setText("Travail sur débits");
    }
    if (lexique.isTrue("53")) {
      label1.setText("Travail sur crédits");
    }
    
    // ++++++++++++++++++++++++++ COULEURS LISTE ++++++++++++++++++++++++++++++++++++++++
    // Couleurs de la liste (suivant indicateurs de 01 à 65)
    for (int i = 0; i < _WTEX1_Text_Color.length; i++) {
      if (lexique.isTrue(String.valueOf((i + 1) < 10 ? "0" + (i + 1) : (i + 1)))) {
        _WTEX1_Text_Color[i][1] = Constantes.COULEUR_NEGATIF;
      }
      else {
        _WTEX1_Text_Color[i][1] = Color.BLACK;
      }
      
      if (lexique.isTrue(String.valueOf(i + 13))) {
        _WTEX1_Text_Color[i][2] = Color.RED;
      }
      else {
        _WTEX1_Text_Color[i][2] = Color.BLACK;
      }
      
      if (lexique.isTrue(String.valueOf(i + 25))) {
        _WTEX1_Text_Color[i][3] = Color.RED;
      }
      else {
        _WTEX1_Text_Color[i][3] = Color.BLACK;
      }
      
      if (lexique.isTrue(String.valueOf(i + 37))) {
        _WTEX1_Text_Color[i][4] = Color.RED;
      }
      else {
        _WTEX1_Text_Color[i][4] = Color.BLACK;
      }
      
      if (lexique.isTrue(String.valueOf(i + 49))) {
        _WTEX1_Text_Color[i][5] = Color.RED;
      }
      else {
        _WTEX1_Text_Color[i][5] = Color.BLACK;
      }
      
    }
    // ++++++++++++++++++++++++++ COULEURS LISTE ++++++++++++++++++++++++++++++++++++++++
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ @LIBPG@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/stats.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    String[] annee = new String[5];
    String[] donnee = new String[12 * 5];
    // Chargement des années
    for (int i = 0; i < annee.length; i++) {
      annee[i] = lexique.HostFieldGetData("TIT" + (i + 1)).trim();
    }
    
    // Chargement des données
    for (int i = 0; i < donnee.length; i++) {
      donnee[i] = lexique.HostFieldGetNumericData("WT" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
    }
    new VCGM30FM_GRAPHE1(annee, donnee);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    String[] annee = new String[5];
    String[] donnee = new String[12 * 5];
    // Chargement des années
    for (int i = 0; i < annee.length; i++) {
      annee[i] = lexique.HostFieldGetData("TIT" + (i + 1)).trim();
    }
    
    // Chargement des données
    for (int i = 0; i < donnee.length; i++) {
      donnee[i] = lexique.HostFieldGetNumericData("WT" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
    }
    new VCGM30FM_GRAPHE2(annee, donnee);
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    String[] annees = new String[5];
    String[][] donnees = new String[5][12];
    
    // Chargement des années
    annees[0] = lexique.HostFieldGetData("HLD01").substring(8, 20).trim();
    annees[1] = lexique.HostFieldGetData("HLD01").substring(23, 35).trim();
    annees[2] = lexique.HostFieldGetData("HLD01").substring(36, 48).trim();
    annees[3] = lexique.HostFieldGetData("HLD01").substring(49, 61).trim();
    annees[4] = lexique.HostFieldGetData("HLD01").substring(62, 74).trim();
    
    // Chargement des données
    for (int i = 0; i < 12; i++) {
      donnees[0][i] = lexique.HostFieldGetNumericData("WT" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
      donnees[1][i] = lexique.HostFieldGetNumericData("WT" + (i + 12));
      donnees[2][i] = lexique.HostFieldGetNumericData("WT" + (i + 24));
      donnees[3][i] = lexique.HostFieldGetNumericData("WT" + (i + 36));
      donnees[4][i] = lexique.HostFieldGetNumericData("WT" + (i + 48));
    }
    
    new VCGM30FM_GRAPHEA2(annees, donnees);
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    String[] annees = new String[5];
    String[] donnees = new String[5];
    String[] donnees1 = new String[5];
    
    // Chargement des années
    annees[0] = lexique.HostFieldGetData("HLD01").substring(8, 20).trim();
    annees[1] = lexique.HostFieldGetData("HLD01").substring(23, 35).trim();
    annees[2] = lexique.HostFieldGetData("HLD01").substring(36, 48).trim();
    annees[3] = lexique.HostFieldGetData("HLD01").substring(49, 61).trim();
    annees[4] = lexique.HostFieldGetData("HLD01").substring(62, 74).trim();
    
    // Chargement des données
    for (int i = 0; i < donnees.length; i++) {
      donnees[i] = lexique.HostFieldGetNumericData("TOT" + (i + 1));
    }
    for (int i = 0; i < donnees1.length; i++) {
      donnees1[i] = lexique.HostFieldGetNumericData("RAT" + (i + 1));
    }
    
    new VCGM30FM_GRAPHEA3(annees, donnees, donnees1);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    String annee = null;
    String annee1 = null;
    String[] donnee = new String[12];
    String[] donnee1 = new String[12];
    // Chargement des années
    annee = lexique.HostFieldGetData("HLD01").substring(8, 20).trim();
    annee1 = lexique.HostFieldGetData("HLD01").substring(23, 35).trim();
    
    // Chargement des données
    for (int i = 0; i < donnee.length; i++) {
      donnee[i] = lexique.HostFieldGetNumericData("WT" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
      donnee1[i] = lexique.HostFieldGetNumericData("WT" + (i + 12));
    }
    
    new VCGM30FM_GRAPHEA1(annee, annee1, donnee, donnee1);
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    String annee = null;
    String annee1 = null;
    String[] donnee = new String[12];
    String[] donnee1 = new String[12];
    // Chargement des années
    annee = lexique.HostFieldGetData("HLD01").substring(23, 35).trim();
    annee1 = lexique.HostFieldGetData("HLD01").substring(36, 48).trim();
    
    // Chargement des données
    for (int i = 0; i < donnee.length; i++) {
      donnee[i] = lexique.HostFieldGetNumericData("WT" + (i + 12));
      donnee1[i] = lexique.HostFieldGetNumericData("WT" + (i + 24));
    }
    
    new VCGM30FM_GRAPHEA1(annee, annee1, donnee, donnee1);
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    String annee = null;
    String annee1 = null;
    String[] donnee = new String[12];
    String[] donnee1 = new String[12];
    // Chargement des années
    annee = lexique.HostFieldGetData("HLD01").substring(36, 48).trim();
    annee1 = lexique.HostFieldGetData("HLD01").substring(49, 61).trim();
    
    // Chargement des données
    for (int i = 0; i < donnee.length; i++) {
      donnee[i] = lexique.HostFieldGetNumericData("WT" + (i + 24));
      donnee1[i] = lexique.HostFieldGetNumericData("WT" + (i + 36));
    }
    
    new VCGM30FM_GRAPHEA1(annee, annee1, donnee, donnee1);
  }
  
  private void button4ActionPerformed(ActionEvent e) {
    String annee = null;
    String annee1 = null;
    String[] donnee = new String[12];
    String[] donnee1 = new String[12];
    // Chargement des années
    annee = annee1 = lexique.HostFieldGetData("HLD01").substring(62, 74).trim();
    
    // Chargement des données
    for (int i = 0; i < donnee.length; i++) {
      donnee[i] = lexique.HostFieldGetNumericData("WT" + (i + 36));
      donnee1[i] = lexique.HostFieldGetNumericData("WT" + (i + 48));
    }
    
    new VCGM30FM_GRAPHEA1(annee, annee1, donnee, donnee1);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_soc = new JLabel();
    INDETB = new XRiTextField();
    OBJ_32 = new JLabel();
    INDNC1 = new XRiTextField();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    INDNC2 = new XRiTextField();
    INDNA1 = new XRiTextField();
    INDNA2 = new XRiTextField();
    INDSAN = new XRiTextField();
    OBJ_30 = new JLabel();
    p_tete_droite = new JPanel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTEX1 = new XRiTable();
    OBJ_76 = new JLabel();
    button1 = new SNBoutonDetail();
    button2 = new SNBoutonDetail();
    button3 = new SNBoutonDetail();
    button4 = new SNBoutonDetail();
    TOT1 = new XRiTextField();
    TOT2 = new XRiTextField();
    TOT3 = new XRiTextField();
    TOT4 = new XRiTextField();
    TOT5 = new XRiTextField();
    OBJ_102 = new JLabel();
    RAT1 = new XRiTextField();
    RAT2 = new XRiTextField();
    RAT3 = new XRiTextField();
    RAT4 = new XRiTextField();
    OBJ_100 = new JLabel();
    RAT5 = new XRiTextField();
    RAT6 = new XRiTextField();
    RAT7 = new XRiTextField();
    RAT8 = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Analyse de comptes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_soc ----
          OBJ_soc.setText("Soci\u00e9t\u00e9");
          OBJ_soc.setName("OBJ_soc");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_32 ----
          OBJ_32.setText("Comptes");
          OBJ_32.setComponentPopupMenu(null);
          OBJ_32.setName("OBJ_32");

          //---- INDNC1 ----
          INDNC1.setComponentPopupMenu(null);
          INDNC1.setName("INDNC1");

          //---- OBJ_27 ----
          OBJ_27.setText("D\u00e9but");
          OBJ_27.setComponentPopupMenu(null);
          OBJ_27.setName("OBJ_27");

          //---- OBJ_28 ----
          OBJ_28.setText("Fin");
          OBJ_28.setComponentPopupMenu(null);
          OBJ_28.setName("OBJ_28");

          //---- OBJ_29 ----
          OBJ_29.setText("Section");
          OBJ_29.setComponentPopupMenu(null);
          OBJ_29.setName("OBJ_29");

          //---- INDNC2 ----
          INDNC2.setComponentPopupMenu(null);
          INDNC2.setName("INDNC2");

          //---- INDNA1 ----
          INDNA1.setComponentPopupMenu(null);
          INDNA1.setName("INDNA1");

          //---- INDNA2 ----
          INDNA2.setComponentPopupMenu(null);
          INDNA2.setName("INDNA2");

          //---- INDSAN ----
          INDSAN.setComponentPopupMenu(null);
          INDSAN.setName("INDSAN");

          //---- OBJ_30 ----
          OBJ_30.setText("Tiers");
          OBJ_30.setComponentPopupMenu(null);
          OBJ_30.setName("OBJ_30");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_soc, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNC1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(INDNC2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(INDSAN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDNA1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(INDNA2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNC1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNC2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_soc)
                  .addComponent(OBJ_32)
                  .addComponent(OBJ_27)
                  .addComponent(OBJ_28)
                  .addComponent(OBJ_30)
                  .addComponent(OBJ_29)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label1 ----
          label1.setText("Travail sur");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Soldes");
              riSousMenu_bt6.setToolTipText("Soldes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("D\u00e9bits");
              riSousMenu_bt7.setToolTipText("D\u00e9bits");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Cr\u00e9dits");
              riSousMenu_bt8.setToolTipText("Cr\u00e9dits");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Statistiques");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Evolution sur 1 mois");
              riSousMenu_bt10.setToolTipText("Analyse de l'\u00e9volution des comptes pour un mois donn\u00e9");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Evolution sur 1 an");
              riSousMenu_bt11.setToolTipText("Analyse de l'\u00e9volution mensuelle sur une ann\u00e9e");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("5 derni\u00e8res ann\u00e9es");
              riSousMenu_bt12.setToolTipText("5 derni\u00e8res ann\u00e9es d'\u00e9volution mensuelle pour les comptes s\u00e9lectionn\u00e9s");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Totaux et ratios");
              riSousMenu_bt13.setToolTipText("Totaux et ratios");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("@WLIBDV@");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setPreferredSize(new Dimension(691, 450));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTEX1 ----
              WTEX1.setRowHeight(18);
              WTEX1.setName("WTEX1");
              SCROLLPANE_LIST.setViewportView(WTEX1);
            }

            //---- OBJ_76 ----
            OBJ_76.setText("Totaux");
            OBJ_76.setName("OBJ_76");

            //---- button1 ----
            button1.setName("button1");
            button1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button1ActionPerformed(e);
              }
            });

            //---- button2 ----
            button2.setName("button2");
            button2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button2ActionPerformed(e);
              }
            });

            //---- button3 ----
            button3.setName("button3");
            button3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button3ActionPerformed(e);
              }
            });

            //---- button4 ----
            button4.setName("button4");
            button4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button4ActionPerformed(e);
              }
            });

            //---- TOT1 ----
            TOT1.setName("TOT1");

            //---- TOT2 ----
            TOT2.setName("TOT2");

            //---- TOT3 ----
            TOT3.setName("TOT3");

            //---- TOT4 ----
            TOT4.setName("TOT4");

            //---- TOT5 ----
            TOT5.setName("TOT5");

            //---- OBJ_102 ----
            OBJ_102.setText("Exercices");
            OBJ_102.setName("OBJ_102");

            //---- RAT1 ----
            RAT1.setName("RAT1");

            //---- RAT2 ----
            RAT2.setName("RAT2");

            //---- RAT3 ----
            RAT3.setName("RAT3");

            //---- RAT4 ----
            RAT4.setName("RAT4");

            //---- OBJ_100 ----
            OBJ_100.setText("@LRAT58@");
            OBJ_100.setName("OBJ_100");

            //---- RAT5 ----
            RAT5.setName("RAT5");

            //---- RAT6 ----
            RAT6.setName("RAT6");

            //---- RAT7 ----
            RAT7.setName("RAT7");

            //---- RAT8 ----
            RAT8.setName("RAT8");

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("Ratios");
            xTitledSeparator1.setName("xTitledSeparator1");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(285, 285, 285)
                  .addComponent(button1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                  .addGap(16, 16, 16)
                  .addComponent(button2, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                  .addGap(16, 16, 16)
                  .addComponent(button3, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                  .addGap(16, 16, 16)
                  .addComponent(button4, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(75, 75, 75)
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 652, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(75, 75, 75)
                  .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                  .addGap(42, 42, 42)
                  .addComponent(TOT1, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(TOT2, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(TOT3, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(TOT4, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(TOT5, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(75, 75, 75)
                  .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(125, 125, 125)
                  .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(RAT1, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(34, 34, 34)
                  .addComponent(RAT2, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(39, 39, 39)
                  .addComponent(RAT3, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(34, 34, 34)
                  .addComponent(RAT4, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(125, 125, 125)
                  .addComponent(OBJ_100, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(RAT5, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(34, 34, 34)
                  .addComponent(RAT6, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(39, 39, 39)
                  .addComponent(RAT7, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(34, 34, 34)
                  .addComponent(RAT8, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(30, 30, 30)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(button1, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addComponent(button2, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addComponent(button3, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addComponent(button4, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(TOT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_102))
                    .addComponent(RAT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RAT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RAT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RAT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_100))
                    .addComponent(RAT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RAT6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RAT7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RAT8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 813, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== riSousMenu14 ========
    {
      riSousMenu14.setName("riSousMenu14");

      //---- riSousMenu_bt14 ----
      riSousMenu_bt14.setText("Evolution mois cumul\u00e9s");
      riSousMenu_bt14.setToolTipText("Analyse de l'\u00e9volution des comptes sur mois cumul\u00e9s");
      riSousMenu_bt14.setName("riSousMenu_bt14");
      riSousMenu_bt14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt14ActionPerformed(e);
        }
      });
      riSousMenu14.add(riSousMenu_bt14);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_soc;
  private XRiTextField INDETB;
  private JLabel OBJ_32;
  private XRiTextField INDNC1;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private XRiTextField INDNC2;
  private XRiTextField INDNA1;
  private XRiTextField INDNA2;
  private XRiTextField INDSAN;
  private JLabel OBJ_30;
  private JPanel p_tete_droite;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTEX1;
  private JLabel OBJ_76;
  private SNBoutonDetail button1;
  private SNBoutonDetail button2;
  private SNBoutonDetail button3;
  private SNBoutonDetail button4;
  private XRiTextField TOT1;
  private XRiTextField TOT2;
  private XRiTextField TOT3;
  private XRiTextField TOT4;
  private XRiTextField TOT5;
  private JLabel OBJ_102;
  private XRiTextField RAT1;
  private XRiTextField RAT2;
  private XRiTextField RAT3;
  private XRiTextField RAT4;
  private JLabel OBJ_100;
  private XRiTextField RAT5;
  private XRiTextField RAT6;
  private XRiTextField RAT7;
  private XRiTextField RAT8;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
