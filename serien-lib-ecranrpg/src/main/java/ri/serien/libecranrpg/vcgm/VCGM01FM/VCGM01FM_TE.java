
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_TE extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_TE(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_56_OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNTA@")).trim());
    OBJ_58_OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCC01@")).trim());
    OBJ_60_OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCC02@")).trim());
    OBJ_64_OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCC03@")).trim());
    OBJ_66_OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCC04@")).trim());
    OBJ_70_OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCC05@")).trim());
    OBJ_72_OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCC06@")).trim());
    OBJ_58_OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB01@")).trim());
    OBJ_60_OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB02@")).trim());
    OBJ_64_OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB03@")).trim());
    OBJ_66_OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB04@")).trim());
    OBJ_70_OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB05@")).trim());
    OBJ_72_OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB06@")).trim());
    OBJ_58_OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB07@")).trim());
    OBJ_60_OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB08@")).trim());
    OBJ_64_OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB09@")).trim());
    OBJ_66_OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB10@")).trim());
    OBJ_70_OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB11@")).trim());
    OBJ_72_OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLB12@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_72_OBJ_72.setVisible(lexique.isPresent("LCC06"));
    OBJ_70_OBJ_70.setVisible(lexique.isPresent("LCC05"));
    OBJ_66_OBJ_66.setVisible(lexique.isPresent("LCC04"));
    OBJ_64_OBJ_64.setVisible(lexique.isPresent("LCC03"));
    OBJ_60_OBJ_60.setVisible(lexique.isPresent("LCC02"));
    OBJ_58_OBJ_58.setVisible(lexique.isPresent("LCC01"));
    OBJ_56_OBJ_56.setVisible(lexique.isPresent("LIBNTA"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation de @LOCGRP/-1/@"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_56_OBJ_56 = new RiZoneSortie();
    OBJ_58_OBJ_58 = new RiZoneSortie();
    OBJ_60_OBJ_60 = new RiZoneSortie();
    OBJ_64_OBJ_64 = new RiZoneSortie();
    OBJ_66_OBJ_66 = new RiZoneSortie();
    OBJ_70_OBJ_70 = new RiZoneSortie();
    OBJ_72_OBJ_72 = new RiZoneSortie();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    TENTA = new XRiTextField();
    TECC01 = new XRiTextField();
    TECC02 = new XRiTextField();
    TECC03 = new XRiTextField();
    TECC04 = new XRiTextField();
    TECC05 = new XRiTextField();
    TECC06 = new XRiTextField();
    TETTA = new XRiTextField();
    TECJO = new XRiTextField();
    TECLA = new XRiTextField();
    separator1 = compFactory.createSeparator("Traitement des acomptes");
    OBJ_58_OBJ_59 = new RiZoneSortie();
    OBJ_60_OBJ_61 = new RiZoneSortie();
    OBJ_64_OBJ_65 = new RiZoneSortie();
    OBJ_66_OBJ_67 = new RiZoneSortie();
    OBJ_70_OBJ_71 = new RiZoneSortie();
    OBJ_72_OBJ_73 = new RiZoneSortie();
    TENG01 = new XRiTextField();
    TENG02 = new XRiTextField();
    TENG03 = new XRiTextField();
    TENG04 = new XRiTextField();
    TENG05 = new XRiTextField();
    TENG06 = new XRiTextField();
    OBJ_58_OBJ_60 = new RiZoneSortie();
    OBJ_60_OBJ_62 = new RiZoneSortie();
    OBJ_64_OBJ_66 = new RiZoneSortie();
    OBJ_66_OBJ_68 = new RiZoneSortie();
    OBJ_70_OBJ_72 = new RiZoneSortie();
    OBJ_72_OBJ_74 = new RiZoneSortie();
    TENG07 = new XRiTextField();
    TENG08 = new XRiTextField();
    TENG09 = new XRiTextField();
    TENG10 = new XRiTextField();
    TENG11 = new XRiTextField();
    TENG12 = new XRiTextField();
    TETT01 = new XRiTextField();
    TETT02 = new XRiTextField();
    TETT03 = new XRiTextField();
    TETT04 = new XRiTextField();
    TETT05 = new XRiTextField();
    TETT06 = new XRiTextField();
    separator2 = compFactory.createSeparator("N\u00b0Comptes TVA Collect\u00e9e/Prestations");
    separator3 = compFactory.createSeparator("N\u00b0Comptes TVA / Prestations r\u00e9gl\u00e9es");
    separator4 = compFactory.createSeparator("Taux");
    separator5 = compFactory.createSeparator("Compte de TVA \u00e0 r\u00e9gulariser");
    separator6 = compFactory.createSeparator("Collectifs clients \u00e0 traiter");
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("T.V.A. sur encaissements");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("@LIBNTA@");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("@LCC01@");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");

            //---- OBJ_60_OBJ_60 ----
            OBJ_60_OBJ_60.setText("@LCC02@");
            OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");

            //---- OBJ_64_OBJ_64 ----
            OBJ_64_OBJ_64.setText("@LCC03@");
            OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("@LCC04@");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("@LCC05@");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("@LCC06@");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");

            //---- OBJ_48_OBJ_48 ----
            OBJ_48_OBJ_48.setText("Code Journal TVA / Prestations");
            OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("Code libell\u00e9 des \u00e9critures");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("Taux de TVA");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");

            //---- TENTA ----
            TENTA.setName("TENTA");

            //---- TECC01 ----
            TECC01.setName("TECC01");

            //---- TECC02 ----
            TECC02.setName("TECC02");

            //---- TECC03 ----
            TECC03.setName("TECC03");

            //---- TECC04 ----
            TECC04.setName("TECC04");

            //---- TECC05 ----
            TECC05.setName("TECC05");

            //---- TECC06 ----
            TECC06.setName("TECC06");

            //---- TETTA ----
            TETTA.setName("TETTA");

            //---- TECJO ----
            TECJO.setName("TECJO");

            //---- TECLA ----
            TECLA.setName("TECLA");

            //---- separator1 ----
            separator1.setName("separator1");

            //---- OBJ_58_OBJ_59 ----
            OBJ_58_OBJ_59.setText("@TLB01@");
            OBJ_58_OBJ_59.setName("OBJ_58_OBJ_59");

            //---- OBJ_60_OBJ_61 ----
            OBJ_60_OBJ_61.setText("@TLB02@");
            OBJ_60_OBJ_61.setName("OBJ_60_OBJ_61");

            //---- OBJ_64_OBJ_65 ----
            OBJ_64_OBJ_65.setText("@TLB03@");
            OBJ_64_OBJ_65.setName("OBJ_64_OBJ_65");

            //---- OBJ_66_OBJ_67 ----
            OBJ_66_OBJ_67.setText("@TLB04@");
            OBJ_66_OBJ_67.setName("OBJ_66_OBJ_67");

            //---- OBJ_70_OBJ_71 ----
            OBJ_70_OBJ_71.setText("@TLB05@");
            OBJ_70_OBJ_71.setName("OBJ_70_OBJ_71");

            //---- OBJ_72_OBJ_73 ----
            OBJ_72_OBJ_73.setText("@TLB06@");
            OBJ_72_OBJ_73.setName("OBJ_72_OBJ_73");

            //---- TENG01 ----
            TENG01.setName("TENG01");

            //---- TENG02 ----
            TENG02.setName("TENG02");

            //---- TENG03 ----
            TENG03.setName("TENG03");

            //---- TENG04 ----
            TENG04.setName("TENG04");

            //---- TENG05 ----
            TENG05.setName("TENG05");

            //---- TENG06 ----
            TENG06.setName("TENG06");

            //---- OBJ_58_OBJ_60 ----
            OBJ_58_OBJ_60.setText("@TLB07@");
            OBJ_58_OBJ_60.setName("OBJ_58_OBJ_60");

            //---- OBJ_60_OBJ_62 ----
            OBJ_60_OBJ_62.setText("@TLB08@");
            OBJ_60_OBJ_62.setName("OBJ_60_OBJ_62");

            //---- OBJ_64_OBJ_66 ----
            OBJ_64_OBJ_66.setText("@TLB09@");
            OBJ_64_OBJ_66.setName("OBJ_64_OBJ_66");

            //---- OBJ_66_OBJ_68 ----
            OBJ_66_OBJ_68.setText("@TLB10@");
            OBJ_66_OBJ_68.setName("OBJ_66_OBJ_68");

            //---- OBJ_70_OBJ_72 ----
            OBJ_70_OBJ_72.setText("@TLB11@");
            OBJ_70_OBJ_72.setName("OBJ_70_OBJ_72");

            //---- OBJ_72_OBJ_74 ----
            OBJ_72_OBJ_74.setText("@TLB12@");
            OBJ_72_OBJ_74.setName("OBJ_72_OBJ_74");

            //---- TENG07 ----
            TENG07.setName("TENG07");

            //---- TENG08 ----
            TENG08.setName("TENG08");

            //---- TENG09 ----
            TENG09.setName("TENG09");

            //---- TENG10 ----
            TENG10.setName("TENG10");

            //---- TENG11 ----
            TENG11.setName("TENG11");

            //---- TENG12 ----
            TENG12.setName("TENG12");

            //---- TETT01 ----
            TETT01.setName("TETT01");

            //---- TETT02 ----
            TETT02.setName("TETT02");

            //---- TETT03 ----
            TETT03.setName("TETT03");

            //---- TETT04 ----
            TETT04.setName("TETT04");

            //---- TETT05 ----
            TETT05.setName("TETT05");

            //---- TETT06 ----
            TETT06.setName("TETT06");

            //---- separator2 ----
            separator2.setName("separator2");

            //---- separator3 ----
            separator3.setName("separator3");

            //---- separator4 ----
            separator4.setName("separator4");

            //---- separator5 ----
            separator5.setName("separator5");

            //---- separator6 ----
            separator6.setName("separator6");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                  .addGap(55, 55, 55)
                  .addComponent(separator3, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(separator4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(TENG01, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_58_OBJ_59, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(78, 78, 78)
                  .addComponent(TENG07, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_58_OBJ_60, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(TETT01, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(TENG02, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_60_OBJ_61, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(78, 78, 78)
                  .addComponent(TENG08, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_60_OBJ_62, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(TETT02, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(TENG03, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_64_OBJ_65, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(78, 78, 78)
                  .addComponent(TENG09, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_64_OBJ_66, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(TETT03, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(TENG04, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_66_OBJ_67, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(78, 78, 78)
                  .addComponent(TENG10, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_66_OBJ_68, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(TETT04, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(TENG05, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_70_OBJ_71, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(78, 78, 78)
                  .addComponent(TENG11, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_70_OBJ_72, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(TETT05, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(TENG06, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_72_OBJ_73, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(78, 78, 78)
                  .addComponent(TENG12, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_72_OBJ_74, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(TETT06, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(45, 45, 45)
                  .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(TECJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(separator5, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                  .addGap(55, 55, 55)
                  .addComponent(separator6, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(45, 45, 45)
                  .addComponent(TENTA, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                  .addGap(29, 29, 29)
                  .addComponent(TECC01, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(45, 45, 45)
                  .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGap(90, 90, 90)
                  .addComponent(TETTA, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(92, 92, 92)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TECC02, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TECC03, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_60_OBJ_60, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(385, 385, 385)
                  .addComponent(TECC04, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(45, 45, 45)
                  .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
                  .addGap(22, 22, 22)
                  .addComponent(TECLA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(130, 130, 130)
                  .addComponent(TECC05, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(385, 385, 385)
                  .addComponent(TECC06, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(separator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(separator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(separator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TENG01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TENG07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TETT01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_58_OBJ_59, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_58_OBJ_60, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TENG02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TENG08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TETT02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_60_OBJ_61, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_60_OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TENG03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TENG09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TETT03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_64_OBJ_65, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_64_OBJ_66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TENG04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TENG10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TETT04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_66_OBJ_67, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_66_OBJ_68, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TENG05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TENG11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TETT05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_70_OBJ_71, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_70_OBJ_72, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TENG06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TENG12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TETT06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_72_OBJ_73, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_72_OBJ_74, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(11, 11, 11)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TECJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(separator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(separator6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(6, 6, 6)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TENTA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TECC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(31, 31, 31)
                      .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(26, 26, 26)
                      .addComponent(TETTA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(TECC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(TECC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_60_OBJ_60, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TECC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(TECLA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(TECC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TECC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie OBJ_56_OBJ_56;
  private RiZoneSortie OBJ_58_OBJ_58;
  private RiZoneSortie OBJ_60_OBJ_60;
  private RiZoneSortie OBJ_64_OBJ_64;
  private RiZoneSortie OBJ_66_OBJ_66;
  private RiZoneSortie OBJ_70_OBJ_70;
  private RiZoneSortie OBJ_72_OBJ_72;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_61_OBJ_61;
  private XRiTextField TENTA;
  private XRiTextField TECC01;
  private XRiTextField TECC02;
  private XRiTextField TECC03;
  private XRiTextField TECC04;
  private XRiTextField TECC05;
  private XRiTextField TECC06;
  private XRiTextField TETTA;
  private XRiTextField TECJO;
  private XRiTextField TECLA;
  private JComponent separator1;
  private RiZoneSortie OBJ_58_OBJ_59;
  private RiZoneSortie OBJ_60_OBJ_61;
  private RiZoneSortie OBJ_64_OBJ_65;
  private RiZoneSortie OBJ_66_OBJ_67;
  private RiZoneSortie OBJ_70_OBJ_71;
  private RiZoneSortie OBJ_72_OBJ_73;
  private XRiTextField TENG01;
  private XRiTextField TENG02;
  private XRiTextField TENG03;
  private XRiTextField TENG04;
  private XRiTextField TENG05;
  private XRiTextField TENG06;
  private RiZoneSortie OBJ_58_OBJ_60;
  private RiZoneSortie OBJ_60_OBJ_62;
  private RiZoneSortie OBJ_64_OBJ_66;
  private RiZoneSortie OBJ_66_OBJ_68;
  private RiZoneSortie OBJ_70_OBJ_72;
  private RiZoneSortie OBJ_72_OBJ_74;
  private XRiTextField TENG07;
  private XRiTextField TENG08;
  private XRiTextField TENG09;
  private XRiTextField TENG10;
  private XRiTextField TENG11;
  private XRiTextField TENG12;
  private XRiTextField TETT01;
  private XRiTextField TETT02;
  private XRiTextField TETT03;
  private XRiTextField TETT04;
  private XRiTextField TETT05;
  private XRiTextField TETT06;
  private JComponent separator2;
  private JComponent separator3;
  private JComponent separator4;
  private JComponent separator5;
  private JComponent separator6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
