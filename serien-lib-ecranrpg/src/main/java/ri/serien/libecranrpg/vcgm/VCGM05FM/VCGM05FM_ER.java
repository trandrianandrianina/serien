
package ri.serien.libecranrpg.vcgm.VCGM05FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM05FM_ER extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[][] _LIST_Title_Data_Brut=null;
  private String[] _I5001_Title = { "Code", "Libellé", };
  private String[][] _I5001_Data = { { "I5001", "L5001", }, { "I5002", "L5002", }, { "I5003", "L5003", }, { "I5004", "L5004", },
      { "I5005", "L5005", }, { "I5006", "L5006", }, { "I5007", "L5007", }, { "I5008", "L5008", }, };
  private int[] _I5001_Width = { 33, 421, };
  
  public VCGM05FM_ER(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    // I5001.getTableHeader().setFont(new Font("Courier New", Font.BOLD, 12));
    // ((DefaultTableCellRenderer)I5001.getTableHeader().getDefaultRenderer()).setHorizontalAlignment( JLabel.LEFT );
    // _LIST_Title_Data_Brut = initTable(LIST);
    
    // Ajout
    initDiverses();
    I5001.setAspectTable(null, _I5001_Title, _I5001_Data, _I5001_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(OK);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liée directement à un composant graphique
    // majTable(LIST, _LIST_Title_Data_Brut, null);
    
    
    
    
    // TODO Icones
    OBJ_62.setIcon(lexique.chargerImage("images/pgup20.png", true));
    OBJ_68.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    OK.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("ERREURS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OKActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    SCROLLPANE_OBJ_7 = new JScrollPane();
    I5001 = new XRiTable();
    OBJ_5 = new JXTitledSeparator();
    OK = new JButton();
    OBJ_62 = new JButton();
    OBJ_68 = new JButton();

    //======== this ========
    setName("this");

    //======== SCROLLPANE_OBJ_7 ========
    {
      SCROLLPANE_OBJ_7.setName("SCROLLPANE_OBJ_7");

      //---- I5001 ----
      I5001.setRowSelectionAllowed(false);
      I5001.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      I5001.setName("I5001");
      SCROLLPANE_OBJ_7.setViewportView(I5001);
    }

    //---- OBJ_5 ----
    OBJ_5.setHorizontalAlignment(SwingConstants.LEFT);
    OBJ_5.setTitle("Signification des erreurs");
    OBJ_5.setName("OBJ_5");

    //---- OK ----
    OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OK.setName("OK");
    OK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OKActionPerformed(e);
      }
    });

    //---- OBJ_62 ----
    OBJ_62.setText("");
    OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_62.setName("OBJ_62");
    OBJ_62.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_62ActionPerformed(e);
      }
    });

    //---- OBJ_68 ----
    OBJ_68.setText("");
    OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_68.setName("OBJ_68");
    OBJ_68.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_68ActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 480, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(OK)
                .addComponent(SCROLLPANE_OBJ_7, GroupLayout.PREFERRED_SIZE, 480, GroupLayout.PREFERRED_SIZE))
              .addGap(5, 5, 5)
              .addGroup(layout.createParallelGroup()
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(9, 9, 9)
          .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(22, 22, 22)
          .addGroup(layout.createParallelGroup()
            .addComponent(SCROLLPANE_OBJ_7, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
              .addGap(3, 3, 3)
              .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)))
          .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
          .addComponent(OK)
          .addGap(28, 28, 28))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane SCROLLPANE_OBJ_7;
  private XRiTable I5001;
  private JXTitledSeparator OBJ_5;
  private JButton OK;
  private JButton OBJ_62;
  private JButton OBJ_68;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
