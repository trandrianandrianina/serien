
package ri.serien.libecranrpg.vcgm.VCGM16FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM16FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM16FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DT201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT201@")).trim());
    DT202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT202@")).trim());
    DT203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT203@")).trim());
    DT204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT204@")).trim());
    DT205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT205@")).trim());
    DT206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT206@")).trim());
    DT207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT207@")).trim());
    DT208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT208@")).trim());
    DT209.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT209@")).trim());
    DT210.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT210@")).trim());
    DT211.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT211@")).trim());
    DT212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT212@")).trim());
    POU201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU201@")).trim());
    POU202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU202@")).trim());
    POU203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU203@")).trim());
    POU204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU204@")).trim());
    POU205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU205@")).trim());
    POU206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU206@")).trim());
    POU207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU207@")).trim());
    POU208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU208@")).trim());
    POU209.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU209@")).trim());
    POU210.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU210@")).trim());
    POU211.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU211@")).trim());
    POU212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU212@")).trim());
    POR201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR201@")).trim());
    POR202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR202@")).trim());
    POR203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR203@")).trim());
    POR204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR204@")).trim());
    POR205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR205@")).trim());
    POR206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR206@")).trim());
    POR207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR207@")).trim());
    POR208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR208@")).trim());
    POR209.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR209@")).trim());
    POR210.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR210@")).trim());
    POR211.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR211@")).trim());
    POR212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR212@")).trim());
    WTOR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTOR2@")).trim());
    DT213.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT213@")).trim());
    POU213.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU213@")).trim());
    POR213.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR213@")).trim());
    DT214.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT214@")).trim());
    POU214.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU214@")).trim());
    POR214.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR214@")).trim());
    DT215.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT215@")).trim());
    POU215.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU215@")).trim());
    POR215.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR215@")).trim());
    DT216.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT216@")).trim());
    POU216.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU216@")).trim());
    POR216.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR216@")).trim());
    DT217.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT217@")).trim());
    POU217.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU217@")).trim());
    POR217.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR217@")).trim());
    DT218.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT218@")).trim());
    POU218.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU218@")).trim());
    POR218.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR218@")).trim());
    WTOT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTOT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WTOR2.setVisible(lexique.isPresent("WTOR2"));
    WTOT2.setVisible(lexique.isPresent("WTOT2"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    DT201 = new RiZoneSortie();
    DT202 = new RiZoneSortie();
    DT203 = new RiZoneSortie();
    DT204 = new RiZoneSortie();
    DT205 = new RiZoneSortie();
    DT206 = new RiZoneSortie();
    DT207 = new RiZoneSortie();
    DT208 = new RiZoneSortie();
    DT209 = new RiZoneSortie();
    DT210 = new RiZoneSortie();
    DT211 = new RiZoneSortie();
    DT212 = new RiZoneSortie();
    XBM201 = new XRiTextField();
    XBM202 = new XRiTextField();
    XBM203 = new XRiTextField();
    XBM204 = new XRiTextField();
    XBM205 = new XRiTextField();
    XBM206 = new XRiTextField();
    XBM207 = new XRiTextField();
    XBM208 = new XRiTextField();
    XBM209 = new XRiTextField();
    XBM210 = new XRiTextField();
    XBM211 = new XRiTextField();
    XBM212 = new XRiTextField();
    POU201 = new RiZoneSortie();
    POU202 = new RiZoneSortie();
    POU203 = new RiZoneSortie();
    POU204 = new RiZoneSortie();
    POU205 = new RiZoneSortie();
    POU206 = new RiZoneSortie();
    POU207 = new RiZoneSortie();
    POU208 = new RiZoneSortie();
    POU209 = new RiZoneSortie();
    POU210 = new RiZoneSortie();
    POU211 = new RiZoneSortie();
    POU212 = new RiZoneSortie();
    XBR201 = new XRiTextField();
    POR201 = new RiZoneSortie();
    XBR202 = new XRiTextField();
    POR202 = new RiZoneSortie();
    XBR203 = new XRiTextField();
    POR203 = new RiZoneSortie();
    XBR204 = new XRiTextField();
    POR204 = new RiZoneSortie();
    XBR205 = new XRiTextField();
    POR205 = new RiZoneSortie();
    XBR206 = new XRiTextField();
    POR206 = new RiZoneSortie();
    XBR207 = new XRiTextField();
    POR207 = new RiZoneSortie();
    XBR208 = new XRiTextField();
    POR208 = new RiZoneSortie();
    XBR209 = new XRiTextField();
    POR209 = new RiZoneSortie();
    XBR210 = new XRiTextField();
    POR210 = new RiZoneSortie();
    XBR211 = new XRiTextField();
    POR211 = new RiZoneSortie();
    XBR212 = new XRiTextField();
    POR212 = new RiZoneSortie();
    WTOR2 = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Projet");
    separator2 = compFactory.createSeparator("R\u00e9actualis\u00e9");
    DT213 = new RiZoneSortie();
    XBM213 = new XRiTextField();
    POU213 = new RiZoneSortie();
    XBR213 = new XRiTextField();
    POR213 = new RiZoneSortie();
    DT214 = new RiZoneSortie();
    XBM214 = new XRiTextField();
    POU214 = new RiZoneSortie();
    XBR214 = new XRiTextField();
    POR214 = new RiZoneSortie();
    DT215 = new RiZoneSortie();
    XBM215 = new XRiTextField();
    POU215 = new RiZoneSortie();
    XBR215 = new XRiTextField();
    POR215 = new RiZoneSortie();
    DT216 = new RiZoneSortie();
    XBM216 = new XRiTextField();
    POU216 = new RiZoneSortie();
    XBR216 = new XRiTextField();
    POR216 = new RiZoneSortie();
    DT217 = new RiZoneSortie();
    XBM217 = new XRiTextField();
    POU217 = new RiZoneSortie();
    XBR217 = new XRiTextField();
    POR217 = new RiZoneSortie();
    DT218 = new RiZoneSortie();
    XBM218 = new XRiTextField();
    POU218 = new RiZoneSortie();
    XBR218 = new XRiTextField();
    POR218 = new RiZoneSortie();
    WTOT2 = new RiZoneSortie();
    Total = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(740, 655));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- DT201 ----
          DT201.setText("@DT201@");
          DT201.setName("DT201");
          panel1.add(DT201);
          DT201.setBounds(new Rectangle(new Point(20, 50), DT201.getPreferredSize()));

          //---- DT202 ----
          DT202.setText("@DT202@");
          DT202.setName("DT202");
          panel1.add(DT202);
          DT202.setBounds(new Rectangle(new Point(20, 81), DT202.getPreferredSize()));

          //---- DT203 ----
          DT203.setText("@DT203@");
          DT203.setName("DT203");
          panel1.add(DT203);
          DT203.setBounds(new Rectangle(new Point(20, 112), DT203.getPreferredSize()));

          //---- DT204 ----
          DT204.setText("@DT204@");
          DT204.setName("DT204");
          panel1.add(DT204);
          DT204.setBounds(new Rectangle(new Point(20, 143), DT204.getPreferredSize()));

          //---- DT205 ----
          DT205.setText("@DT205@");
          DT205.setName("DT205");
          panel1.add(DT205);
          DT205.setBounds(new Rectangle(new Point(20, 174), DT205.getPreferredSize()));

          //---- DT206 ----
          DT206.setText("@DT206@");
          DT206.setName("DT206");
          panel1.add(DT206);
          DT206.setBounds(new Rectangle(new Point(20, 205), DT206.getPreferredSize()));

          //---- DT207 ----
          DT207.setText("@DT207@");
          DT207.setName("DT207");
          panel1.add(DT207);
          DT207.setBounds(new Rectangle(new Point(20, 236), DT207.getPreferredSize()));

          //---- DT208 ----
          DT208.setText("@DT208@");
          DT208.setName("DT208");
          panel1.add(DT208);
          DT208.setBounds(new Rectangle(new Point(20, 267), DT208.getPreferredSize()));

          //---- DT209 ----
          DT209.setText("@DT209@");
          DT209.setName("DT209");
          panel1.add(DT209);
          DT209.setBounds(new Rectangle(new Point(20, 298), DT209.getPreferredSize()));

          //---- DT210 ----
          DT210.setText("@DT210@");
          DT210.setName("DT210");
          panel1.add(DT210);
          DT210.setBounds(new Rectangle(new Point(20, 329), DT210.getPreferredSize()));

          //---- DT211 ----
          DT211.setText("@DT211@");
          DT211.setName("DT211");
          panel1.add(DT211);
          DT211.setBounds(new Rectangle(new Point(20, 360), DT211.getPreferredSize()));

          //---- DT212 ----
          DT212.setText("@DT212@");
          DT212.setName("DT212");
          panel1.add(DT212);
          DT212.setBounds(new Rectangle(new Point(20, 391), DT212.getPreferredSize()));

          //---- XBM201 ----
          XBM201.setName("XBM201");
          panel1.add(XBM201);
          XBM201.setBounds(130, 48, 110, XBM201.getPreferredSize().height);

          //---- XBM202 ----
          XBM202.setName("XBM202");
          panel1.add(XBM202);
          XBM202.setBounds(130, 79, 110, XBM202.getPreferredSize().height);

          //---- XBM203 ----
          XBM203.setName("XBM203");
          panel1.add(XBM203);
          XBM203.setBounds(130, 110, 110, XBM203.getPreferredSize().height);

          //---- XBM204 ----
          XBM204.setName("XBM204");
          panel1.add(XBM204);
          XBM204.setBounds(130, 141, 110, XBM204.getPreferredSize().height);

          //---- XBM205 ----
          XBM205.setName("XBM205");
          panel1.add(XBM205);
          XBM205.setBounds(130, 172, 110, XBM205.getPreferredSize().height);

          //---- XBM206 ----
          XBM206.setName("XBM206");
          panel1.add(XBM206);
          XBM206.setBounds(130, 203, 110, XBM206.getPreferredSize().height);

          //---- XBM207 ----
          XBM207.setName("XBM207");
          panel1.add(XBM207);
          XBM207.setBounds(130, 234, 110, XBM207.getPreferredSize().height);

          //---- XBM208 ----
          XBM208.setName("XBM208");
          panel1.add(XBM208);
          XBM208.setBounds(130, 265, 110, XBM208.getPreferredSize().height);

          //---- XBM209 ----
          XBM209.setName("XBM209");
          panel1.add(XBM209);
          XBM209.setBounds(130, 296, 110, XBM209.getPreferredSize().height);

          //---- XBM210 ----
          XBM210.setName("XBM210");
          panel1.add(XBM210);
          XBM210.setBounds(130, 327, 110, XBM210.getPreferredSize().height);

          //---- XBM211 ----
          XBM211.setName("XBM211");
          panel1.add(XBM211);
          XBM211.setBounds(130, 358, 110, XBM211.getPreferredSize().height);

          //---- XBM212 ----
          XBM212.setName("XBM212");
          panel1.add(XBM212);
          XBM212.setBounds(130, 389, 110, XBM212.getPreferredSize().height);

          //---- POU201 ----
          POU201.setText("@POU201@");
          POU201.setName("POU201");
          panel1.add(POU201);
          POU201.setBounds(245, 50, 50, POU201.getPreferredSize().height);

          //---- POU202 ----
          POU202.setText("@POU202@");
          POU202.setName("POU202");
          panel1.add(POU202);
          POU202.setBounds(245, 81, 50, POU202.getPreferredSize().height);

          //---- POU203 ----
          POU203.setText("@POU203@");
          POU203.setName("POU203");
          panel1.add(POU203);
          POU203.setBounds(245, 112, 50, POU203.getPreferredSize().height);

          //---- POU204 ----
          POU204.setText("@POU204@");
          POU204.setName("POU204");
          panel1.add(POU204);
          POU204.setBounds(245, 143, 50, POU204.getPreferredSize().height);

          //---- POU205 ----
          POU205.setText("@POU205@");
          POU205.setName("POU205");
          panel1.add(POU205);
          POU205.setBounds(245, 174, 50, POU205.getPreferredSize().height);

          //---- POU206 ----
          POU206.setText("@POU206@");
          POU206.setName("POU206");
          panel1.add(POU206);
          POU206.setBounds(245, 205, 50, POU206.getPreferredSize().height);

          //---- POU207 ----
          POU207.setText("@POU207@");
          POU207.setName("POU207");
          panel1.add(POU207);
          POU207.setBounds(245, 236, 50, POU207.getPreferredSize().height);

          //---- POU208 ----
          POU208.setText("@POU208@");
          POU208.setName("POU208");
          panel1.add(POU208);
          POU208.setBounds(245, 267, 50, POU208.getPreferredSize().height);

          //---- POU209 ----
          POU209.setText("@POU209@");
          POU209.setName("POU209");
          panel1.add(POU209);
          POU209.setBounds(245, 298, 50, POU209.getPreferredSize().height);

          //---- POU210 ----
          POU210.setText("@POU210@");
          POU210.setName("POU210");
          panel1.add(POU210);
          POU210.setBounds(245, 329, 50, POU210.getPreferredSize().height);

          //---- POU211 ----
          POU211.setText("@POU211@");
          POU211.setName("POU211");
          panel1.add(POU211);
          POU211.setBounds(245, 360, 50, POU211.getPreferredSize().height);

          //---- POU212 ----
          POU212.setText("@POU212@");
          POU212.setName("POU212");
          panel1.add(POU212);
          POU212.setBounds(245, 391, 50, POU212.getPreferredSize().height);

          //---- XBR201 ----
          XBR201.setName("XBR201");
          panel1.add(XBR201);
          XBR201.setBounds(360, 48, 110, XBR201.getPreferredSize().height);

          //---- POR201 ----
          POR201.setText("@POR201@");
          POR201.setName("POR201");
          panel1.add(POR201);
          POR201.setBounds(475, 50, 50, POR201.getPreferredSize().height);

          //---- XBR202 ----
          XBR202.setName("XBR202");
          panel1.add(XBR202);
          XBR202.setBounds(360, 79, 110, XBR202.getPreferredSize().height);

          //---- POR202 ----
          POR202.setText("@POR202@");
          POR202.setName("POR202");
          panel1.add(POR202);
          POR202.setBounds(475, 81, 50, POR202.getPreferredSize().height);

          //---- XBR203 ----
          XBR203.setName("XBR203");
          panel1.add(XBR203);
          XBR203.setBounds(360, 110, 110, XBR203.getPreferredSize().height);

          //---- POR203 ----
          POR203.setText("@POR203@");
          POR203.setName("POR203");
          panel1.add(POR203);
          POR203.setBounds(475, 112, 50, POR203.getPreferredSize().height);

          //---- XBR204 ----
          XBR204.setName("XBR204");
          panel1.add(XBR204);
          XBR204.setBounds(360, 141, 110, XBR204.getPreferredSize().height);

          //---- POR204 ----
          POR204.setText("@POR204@");
          POR204.setName("POR204");
          panel1.add(POR204);
          POR204.setBounds(475, 143, 50, POR204.getPreferredSize().height);

          //---- XBR205 ----
          XBR205.setName("XBR205");
          panel1.add(XBR205);
          XBR205.setBounds(360, 172, 110, XBR205.getPreferredSize().height);

          //---- POR205 ----
          POR205.setText("@POR205@");
          POR205.setName("POR205");
          panel1.add(POR205);
          POR205.setBounds(475, 174, 50, POR205.getPreferredSize().height);

          //---- XBR206 ----
          XBR206.setName("XBR206");
          panel1.add(XBR206);
          XBR206.setBounds(360, 203, 110, XBR206.getPreferredSize().height);

          //---- POR206 ----
          POR206.setText("@POR206@");
          POR206.setName("POR206");
          panel1.add(POR206);
          POR206.setBounds(475, 205, 50, POR206.getPreferredSize().height);

          //---- XBR207 ----
          XBR207.setName("XBR207");
          panel1.add(XBR207);
          XBR207.setBounds(360, 234, 110, XBR207.getPreferredSize().height);

          //---- POR207 ----
          POR207.setText("@POR207@");
          POR207.setName("POR207");
          panel1.add(POR207);
          POR207.setBounds(475, 236, 50, POR207.getPreferredSize().height);

          //---- XBR208 ----
          XBR208.setName("XBR208");
          panel1.add(XBR208);
          XBR208.setBounds(360, 265, 110, XBR208.getPreferredSize().height);

          //---- POR208 ----
          POR208.setText("@POR208@");
          POR208.setName("POR208");
          panel1.add(POR208);
          POR208.setBounds(475, 267, 50, POR208.getPreferredSize().height);

          //---- XBR209 ----
          XBR209.setName("XBR209");
          panel1.add(XBR209);
          XBR209.setBounds(360, 296, 110, XBR209.getPreferredSize().height);

          //---- POR209 ----
          POR209.setText("@POR209@");
          POR209.setName("POR209");
          panel1.add(POR209);
          POR209.setBounds(475, 298, 50, POR209.getPreferredSize().height);

          //---- XBR210 ----
          XBR210.setName("XBR210");
          panel1.add(XBR210);
          XBR210.setBounds(360, 327, 110, XBR210.getPreferredSize().height);

          //---- POR210 ----
          POR210.setText("@POR210@");
          POR210.setName("POR210");
          panel1.add(POR210);
          POR210.setBounds(475, 329, 50, POR210.getPreferredSize().height);

          //---- XBR211 ----
          XBR211.setName("XBR211");
          panel1.add(XBR211);
          XBR211.setBounds(360, 358, 110, XBR211.getPreferredSize().height);

          //---- POR211 ----
          POR211.setText("@POR211@");
          POR211.setName("POR211");
          panel1.add(POR211);
          POR211.setBounds(475, 360, 50, POR211.getPreferredSize().height);

          //---- XBR212 ----
          XBR212.setName("XBR212");
          panel1.add(XBR212);
          XBR212.setBounds(360, 389, 110, XBR212.getPreferredSize().height);

          //---- POR212 ----
          POR212.setText("@POR212@");
          POR212.setName("POR212");
          panel1.add(POR212);
          POR212.setBounds(475, 391, 50, POR212.getPreferredSize().height);

          //---- WTOR2 ----
          WTOR2.setText("@WTOR2@");
          WTOR2.setName("WTOR2");
          panel1.add(WTOR2);
          WTOR2.setBounds(360, 610, 110, WTOR2.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(130, 25, 175, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          panel1.add(separator2);
          separator2.setBounds(360, 25, 175, separator2.getPreferredSize().height);

          //---- DT213 ----
          DT213.setText("@DT213@");
          DT213.setName("DT213");
          panel1.add(DT213);
          DT213.setBounds(new Rectangle(new Point(20, 422), DT213.getPreferredSize()));

          //---- XBM213 ----
          XBM213.setName("XBM213");
          panel1.add(XBM213);
          XBM213.setBounds(130, 420, 110, XBM213.getPreferredSize().height);

          //---- POU213 ----
          POU213.setText("@POU213@");
          POU213.setName("POU213");
          panel1.add(POU213);
          POU213.setBounds(245, 422, 50, POU213.getPreferredSize().height);

          //---- XBR213 ----
          XBR213.setName("XBR213");
          panel1.add(XBR213);
          XBR213.setBounds(360, 420, 110, XBR213.getPreferredSize().height);

          //---- POR213 ----
          POR213.setText("@POR213@");
          POR213.setName("POR213");
          panel1.add(POR213);
          POR213.setBounds(475, 422, 50, POR213.getPreferredSize().height);

          //---- DT214 ----
          DT214.setText("@DT214@");
          DT214.setName("DT214");
          panel1.add(DT214);
          DT214.setBounds(new Rectangle(new Point(20, 453), DT214.getPreferredSize()));

          //---- XBM214 ----
          XBM214.setName("XBM214");
          panel1.add(XBM214);
          XBM214.setBounds(130, 451, 110, XBM214.getPreferredSize().height);

          //---- POU214 ----
          POU214.setText("@POU214@");
          POU214.setName("POU214");
          panel1.add(POU214);
          POU214.setBounds(245, 453, 50, POU214.getPreferredSize().height);

          //---- XBR214 ----
          XBR214.setName("XBR214");
          panel1.add(XBR214);
          XBR214.setBounds(360, 451, 110, XBR214.getPreferredSize().height);

          //---- POR214 ----
          POR214.setText("@POR214@");
          POR214.setName("POR214");
          panel1.add(POR214);
          POR214.setBounds(475, 453, 50, POR214.getPreferredSize().height);

          //---- DT215 ----
          DT215.setText("@DT215@");
          DT215.setName("DT215");
          panel1.add(DT215);
          DT215.setBounds(new Rectangle(new Point(20, 484), DT215.getPreferredSize()));

          //---- XBM215 ----
          XBM215.setName("XBM215");
          panel1.add(XBM215);
          XBM215.setBounds(130, 482, 110, XBM215.getPreferredSize().height);

          //---- POU215 ----
          POU215.setText("@POU215@");
          POU215.setName("POU215");
          panel1.add(POU215);
          POU215.setBounds(245, 484, 50, POU215.getPreferredSize().height);

          //---- XBR215 ----
          XBR215.setName("XBR215");
          panel1.add(XBR215);
          XBR215.setBounds(360, 482, 110, XBR215.getPreferredSize().height);

          //---- POR215 ----
          POR215.setText("@POR215@");
          POR215.setName("POR215");
          panel1.add(POR215);
          POR215.setBounds(475, 484, 50, POR215.getPreferredSize().height);

          //---- DT216 ----
          DT216.setText("@DT216@");
          DT216.setName("DT216");
          panel1.add(DT216);
          DT216.setBounds(new Rectangle(new Point(20, 515), DT216.getPreferredSize()));

          //---- XBM216 ----
          XBM216.setName("XBM216");
          panel1.add(XBM216);
          XBM216.setBounds(130, 513, 110, XBM216.getPreferredSize().height);

          //---- POU216 ----
          POU216.setText("@POU216@");
          POU216.setName("POU216");
          panel1.add(POU216);
          POU216.setBounds(245, 515, 50, POU216.getPreferredSize().height);

          //---- XBR216 ----
          XBR216.setName("XBR216");
          panel1.add(XBR216);
          XBR216.setBounds(360, 513, 110, XBR216.getPreferredSize().height);

          //---- POR216 ----
          POR216.setText("@POR216@");
          POR216.setName("POR216");
          panel1.add(POR216);
          POR216.setBounds(475, 515, 50, POR216.getPreferredSize().height);

          //---- DT217 ----
          DT217.setText("@DT217@");
          DT217.setName("DT217");
          panel1.add(DT217);
          DT217.setBounds(new Rectangle(new Point(20, 546), DT217.getPreferredSize()));

          //---- XBM217 ----
          XBM217.setName("XBM217");
          panel1.add(XBM217);
          XBM217.setBounds(130, 544, 110, XBM217.getPreferredSize().height);

          //---- POU217 ----
          POU217.setText("@POU217@");
          POU217.setName("POU217");
          panel1.add(POU217);
          POU217.setBounds(245, 546, 50, POU217.getPreferredSize().height);

          //---- XBR217 ----
          XBR217.setName("XBR217");
          panel1.add(XBR217);
          XBR217.setBounds(360, 544, 110, XBR217.getPreferredSize().height);

          //---- POR217 ----
          POR217.setText("@POR217@");
          POR217.setName("POR217");
          panel1.add(POR217);
          POR217.setBounds(475, 546, 50, POR217.getPreferredSize().height);

          //---- DT218 ----
          DT218.setText("@DT218@");
          DT218.setName("DT218");
          panel1.add(DT218);
          DT218.setBounds(new Rectangle(new Point(20, 577), DT218.getPreferredSize()));

          //---- XBM218 ----
          XBM218.setName("XBM218");
          panel1.add(XBM218);
          XBM218.setBounds(130, 575, 110, XBM218.getPreferredSize().height);

          //---- POU218 ----
          POU218.setText("@POU218@");
          POU218.setName("POU218");
          panel1.add(POU218);
          POU218.setBounds(245, 577, 50, POU218.getPreferredSize().height);

          //---- XBR218 ----
          XBR218.setName("XBR218");
          panel1.add(XBR218);
          XBR218.setBounds(360, 575, 110, XBR218.getPreferredSize().height);

          //---- POR218 ----
          POR218.setText("@POR218@");
          POR218.setName("POR218");
          panel1.add(POR218);
          POR218.setBounds(475, 577, 50, POR218.getPreferredSize().height);

          //---- WTOT2 ----
          WTOT2.setText("@WTOT2@");
          WTOT2.setName("WTOT2");
          panel1.add(WTOT2);
          WTOT2.setBounds(130, 610, 110, WTOT2.getPreferredSize().height);

          //---- Total ----
          Total.setText("TOTAL");
          Total.setName("Total");
          panel1.add(Total);
          Total.setBounds(new Rectangle(new Point(20, 610), Total.getPreferredSize()));

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 560, 645);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie DT201;
  private RiZoneSortie DT202;
  private RiZoneSortie DT203;
  private RiZoneSortie DT204;
  private RiZoneSortie DT205;
  private RiZoneSortie DT206;
  private RiZoneSortie DT207;
  private RiZoneSortie DT208;
  private RiZoneSortie DT209;
  private RiZoneSortie DT210;
  private RiZoneSortie DT211;
  private RiZoneSortie DT212;
  private XRiTextField XBM201;
  private XRiTextField XBM202;
  private XRiTextField XBM203;
  private XRiTextField XBM204;
  private XRiTextField XBM205;
  private XRiTextField XBM206;
  private XRiTextField XBM207;
  private XRiTextField XBM208;
  private XRiTextField XBM209;
  private XRiTextField XBM210;
  private XRiTextField XBM211;
  private XRiTextField XBM212;
  private RiZoneSortie POU201;
  private RiZoneSortie POU202;
  private RiZoneSortie POU203;
  private RiZoneSortie POU204;
  private RiZoneSortie POU205;
  private RiZoneSortie POU206;
  private RiZoneSortie POU207;
  private RiZoneSortie POU208;
  private RiZoneSortie POU209;
  private RiZoneSortie POU210;
  private RiZoneSortie POU211;
  private RiZoneSortie POU212;
  private XRiTextField XBR201;
  private RiZoneSortie POR201;
  private XRiTextField XBR202;
  private RiZoneSortie POR202;
  private XRiTextField XBR203;
  private RiZoneSortie POR203;
  private XRiTextField XBR204;
  private RiZoneSortie POR204;
  private XRiTextField XBR205;
  private RiZoneSortie POR205;
  private XRiTextField XBR206;
  private RiZoneSortie POR206;
  private XRiTextField XBR207;
  private RiZoneSortie POR207;
  private XRiTextField XBR208;
  private RiZoneSortie POR208;
  private XRiTextField XBR209;
  private RiZoneSortie POR209;
  private XRiTextField XBR210;
  private RiZoneSortie POR210;
  private XRiTextField XBR211;
  private RiZoneSortie POR211;
  private XRiTextField XBR212;
  private RiZoneSortie POR212;
  private RiZoneSortie WTOR2;
  private JComponent separator1;
  private JComponent separator2;
  private RiZoneSortie DT213;
  private XRiTextField XBM213;
  private RiZoneSortie POU213;
  private XRiTextField XBR213;
  private RiZoneSortie POR213;
  private RiZoneSortie DT214;
  private XRiTextField XBM214;
  private RiZoneSortie POU214;
  private XRiTextField XBR214;
  private RiZoneSortie POR214;
  private RiZoneSortie DT215;
  private XRiTextField XBM215;
  private RiZoneSortie POU215;
  private XRiTextField XBR215;
  private RiZoneSortie POR215;
  private RiZoneSortie DT216;
  private XRiTextField XBM216;
  private RiZoneSortie POU216;
  private XRiTextField XBR216;
  private RiZoneSortie POR216;
  private RiZoneSortie DT217;
  private XRiTextField XBM217;
  private RiZoneSortie POU217;
  private XRiTextField XBR217;
  private RiZoneSortie POR217;
  private RiZoneSortie DT218;
  private XRiTextField XBM218;
  private RiZoneSortie POU218;
  private XRiTextField XBR218;
  private RiZoneSortie POR218;
  private RiZoneSortie WTOT2;
  private RiZoneSortie Total;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
