
package ri.serien.libecranrpg.vcgm.VCGMSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMSEFM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGMSEFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET054.setValeursSelection("1", " ");
    SET053.setValeursSelection("1", " ");
    SET052.setValeursSelection("1", " ");
    SET051.setValeursSelection("1", " ");
    SET050.setValeursSelection("1", " ");
    SET049.setValeursSelection("1", " ");
    SET048.setValeursSelection("1", " ");
    SET047.setValeursSelection("1", " ");
    SET046.setValeursSelection("1", " ");
    SET045.setValeursSelection("1", " ");
    SET044.setValeursSelection("1", " ");
    SET043.setValeursSelection("1", " ");
    SET042.setValeursSelection("1", " ");
    SET041.setValeursSelection("1", " ");
    SET040.setValeursSelection("1", " ");
    SET039.setValeursSelection("1", " ");
    SET038.setValeursSelection("1", " ");
    SET037.setValeursSelection("1", " ");
    SET036.setValeursSelection("1", " ");
    SET035.setValeursSelection("1", " ");
    SET034.setValeursSelection("1", " ");
    SET033.setValeursSelection("1", " ");
    SET032.setValeursSelection("1", " ");
    SET031.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_160.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // CheckBox
    // SET031.setSelected(lexique.HostFieldGetData("SET031").equalsIgnoreCase("0"));
    // SET032.setSelected(lexique.HostFieldGetData("SET032").equalsIgnoreCase("0"));
    // SET033.setSelected(lexique.HostFieldGetData("SET033").equalsIgnoreCase("0"));
    // SET034.setSelected(lexique.HostFieldGetData("SET034").equalsIgnoreCase("0"));
    // SET035.setSelected(lexique.HostFieldGetData("SET035").equalsIgnoreCase("0"));
    // SET036.setSelected(lexique.HostFieldGetData("SET036").equalsIgnoreCase("0"));
    // SET037.setSelected(lexique.HostFieldGetData("SET037").equalsIgnoreCase("0"));
    // SET038.setSelected(lexique.HostFieldGetData("SET038").equalsIgnoreCase("0"));
    // SET039.setSelected(lexique.HostFieldGetData("SET039").equalsIgnoreCase("0"));
    // SET040.setSelected(lexique.HostFieldGetData("SET040").equalsIgnoreCase("0"));
    // SET041.setSelected(lexique.HostFieldGetData("SET041").equalsIgnoreCase("0"));
    // SET042.setSelected(lexique.HostFieldGetData("SET042").equalsIgnoreCase("0"));
    // SET043.setSelected(lexique.HostFieldGetData("SET043").equalsIgnoreCase("0"));
    // SET044.setSelected(lexique.HostFieldGetData("SET044").equalsIgnoreCase("0"));
    // SET045.setSelected(lexique.HostFieldGetData("SET045").equalsIgnoreCase("0"));
    // SET046.setSelected(lexique.HostFieldGetData("SET046").equalsIgnoreCase("0"));
    // SET047.setSelected(lexique.HostFieldGetData("SET047").equalsIgnoreCase("0"));
    // SET048.setSelected(lexique.HostFieldGetData("SET048").equalsIgnoreCase("0"));
    // SET049.setSelected(lexique.HostFieldGetData("SET049").equalsIgnoreCase("0"));
    // SET050.setSelected(lexique.HostFieldGetData("SET050").equalsIgnoreCase("0"));
    // SET051.setSelected(lexique.HostFieldGetData("SET051").equalsIgnoreCase("0"));
    // SET052.setSelected(lexique.HostFieldGetData("SET052").equalsIgnoreCase("0"));
    // SET053.setSelected(lexique.HostFieldGetData("SET053").equalsIgnoreCase("0"));
    // SET054.setSelected(lexique.HostFieldGetData("SET054").equalsIgnoreCase("0"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // CheckBox
    // if (SET031.isSelected())
    // lexique.HostFieldPutData("SET031", 0, "0");
    // else
    // lexique.HostFieldPutData("SET031", 0, "1");
    
    // if (SET032.isSelected())
    // lexique.HostFieldPutData("SET032", 0, "0");
    // else
    // lexique.HostFieldPutData("SET032", 0, "1");
    
    // if (SET033.isSelected())
    // lexique.HostFieldPutData("SET033", 0, "0");
    // else
    // lexique.HostFieldPutData("SET033", 0, "1");
    
    // if (SET034.isSelected())
    // lexique.HostFieldPutData("SET034", 0, "0");
    // else
    // lexique.HostFieldPutData("SET034", 0, "1");
    
    // if (SET035.isSelected())
    // lexique.HostFieldPutData("SET035", 0, "0");
    // else
    // lexique.HostFieldPutData("SET035", 0, "1");
    
    // if (SET036.isSelected())
    // lexique.HostFieldPutData("SET036", 0, "0");
    // else
    // lexique.HostFieldPutData("SET036", 0, "1");
    
    // if (SET037.isSelected())
    // lexique.HostFieldPutData("SET037", 0, "0");
    // else
    // lexique.HostFieldPutData("SET037", 0, "1");
    
    // if (SET038.isSelected())
    // lexique.HostFieldPutData("SET038", 0, "0");
    // else
    // lexique.HostFieldPutData("SET038", 0, "1");
    
    // if (SET039.isSelected())
    // lexique.HostFieldPutData("SET039", 0, "0");
    // else
    // lexique.HostFieldPutData("SET039", 0, "1");
    
    // if (SET040.isSelected())
    // lexique.HostFieldPutData("SET040", 0, "0");
    // else
    // lexique.HostFieldPutData("SET040", 0, "1");
    
    // if (SET041.isSelected())
    // lexique.HostFieldPutData("SET041", 0, "0");
    // else
    // lexique.HostFieldPutData("SET041", 0, "1");
    
    // if (SET042.isSelected())
    // lexique.HostFieldPutData("SET042", 0, "0");
    // else
    // lexique.HostFieldPutData("SET042", 0, "1");
    
    // if (SET043.isSelected())
    // lexique.HostFieldPutData("SET043", 0, "0");
    // else
    // lexique.HostFieldPutData("SET043", 0, "1");
    
    // if (SET044.isSelected())
    // lexique.HostFieldPutData("SET044", 0, "0");
    // else
    // lexique.HostFieldPutData("SET044", 0, "1");
    
    // if (SET045.isSelected())
    // lexique.HostFieldPutData("SET045", 0, "0");
    // else
    // lexique.HostFieldPutData("SET045", 0, "1");
    
    // if (SET046.isSelected())
    // lexique.HostFieldPutData("SET046", 0, "0");
    // else
    // lexique.HostFieldPutData("SET046", 0, "1");
    
    // if (SET047.isSelected())
    // lexique.HostFieldPutData("SET047", 0, "0");
    // else
    // lexique.HostFieldPutData("SET047", 0, "1");
    
    // if (SET048.isSelected())
    // lexique.HostFieldPutData("SET048", 0, "0");
    // else
    // lexique.HostFieldPutData("SET048", 0, "1");
    
    // if (SET049.isSelected())
    // lexique.HostFieldPutData("SET049", 0, "0");
    // else
    // lexique.HostFieldPutData("SET049", 0, "1");
    
    // if (SET050.isSelected())
    // lexique.HostFieldPutData("SET050", 0, "0");
    // else
    // lexique.HostFieldPutData("SET050", 0, "1");
    
    // if (SET051.isSelected())
    // lexique.HostFieldPutData("SET051", 0, "0");
    // else
    // lexique.HostFieldPutData("SET051", 0, "1");
    
    // if (SET052.isSelected())
    // lexique.HostFieldPutData("SET052", 0, "0");
    // else
    // lexique.HostFieldPutData("SET052", 0, "1");
    
    // if (SET053.isSelected())
    // lexique.HostFieldPutData("SET053", 0, "0");
    // else
    // lexique.HostFieldPutData("SET053", 0, "1");
    
    // if (SET054.isSelected())
    // lexique.HostFieldPutData("SET054", 0, "0");
    // else
    // lexique.HostFieldPutData("SET054", 0, "1");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_43 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_64 = new JLabel();
    INDETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_160 = new JLabel();
    OBJ_158 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    SET031 = new XRiCheckBox();
    SET032 = new XRiCheckBox();
    SET033 = new XRiCheckBox();
    SET034 = new XRiCheckBox();
    SET035 = new XRiCheckBox();
    SET036 = new XRiCheckBox();
    SET037 = new XRiCheckBox();
    SET038 = new XRiCheckBox();
    SET039 = new XRiCheckBox();
    SET040 = new XRiCheckBox();
    SET041 = new XRiCheckBox();
    SET042 = new XRiCheckBox();
    OBJ_87 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_129 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_141 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_153 = new JLabel();
    panel2 = new JPanel();
    SET043 = new XRiCheckBox();
    SET044 = new XRiCheckBox();
    SET045 = new XRiCheckBox();
    SET046 = new XRiCheckBox();
    SET047 = new XRiCheckBox();
    SET048 = new XRiCheckBox();
    SET049 = new XRiCheckBox();
    SET050 = new XRiCheckBox();
    SET051 = new XRiCheckBox();
    SET052 = new XRiCheckBox();
    SET053 = new XRiCheckBox();
    SET054 = new XRiCheckBox();
    OBJ_90 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_114 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_138 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_156 = new JLabel();
    pnlBas = new SNPanel();
    OBJ_68 = new SNBoutonLeger();
    V06F = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_43 ----
          OBJ_43.setText("Utilisateur");
          OBJ_43.setName("OBJ_43");

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setText("@INDUSR@");
          INDUSR.setOpaque(false);
          INDUSR.setName("INDUSR");

          //---- OBJ_64 ----
          OBJ_64.setText("Etablissement");
          OBJ_64.setName("OBJ_64");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_160 ----
          OBJ_160.setText("@WPAGE@");
          OBJ_160.setName("OBJ_160");
          p_tete_droite.add(OBJ_160);

          //---- OBJ_158 ----
          OBJ_158.setText("Page    ");
          OBJ_158.setName("OBJ_158");
          p_tete_droite.add(OBJ_158);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(950, 500));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (C.G.M)");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- SET031 ----
            SET031.setComponentPopupMenu(BTD);
            SET031.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET031.setText("Edition plan comptable g\u00e9n\u00e9ral");
            SET031.setName("SET031");

            //---- SET032 ----
            SET032.setComponentPopupMenu(BTD);
            SET032.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET032.setText("Edition plan comptable auxiliaire");
            SET032.setName("SET032");

            //---- SET033 ----
            SET033.setComponentPopupMenu(BTD);
            SET033.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET033.setText("Liste des budgets");
            SET033.setName("SET033");

            //---- SET034 ----
            SET034.setComponentPopupMenu(BTD);
            SET034.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET034.setText("Editions analyse budg\u00e9taire");
            SET034.setName("SET034");

            //---- SET035 ----
            SET035.setComponentPopupMenu(BTD);
            SET035.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET035.setText("Editions des journaux");
            SET035.setName("SET035");

            //---- SET036 ----
            SET036.setComponentPopupMenu(BTD);
            SET036.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET036.setText("Edition des comptes (en cours)");
            SET036.setName("SET036");

            //---- SET037 ----
            SET037.setComponentPopupMenu(BTD);
            SET037.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET037.setText("Edition des comptes (historique)");
            SET037.setName("SET037");

            //---- SET038 ----
            SET038.setComponentPopupMenu(BTD);
            SET038.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET038.setText("Balances g\u00e9n\u00e9rales");
            SET038.setName("SET038");

            //---- SET039 ----
            SET039.setComponentPopupMenu(BTD);
            SET039.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET039.setText("Balances auxiliaires");
            SET039.setName("SET039");

            //---- SET040 ----
            SET040.setComponentPopupMenu(BTD);
            SET040.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET040.setText("Balances sur plusieurs exercices");
            SET040.setName("SET040");

            //---- SET041 ----
            SET041.setComponentPopupMenu(BTD);
            SET041.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET041.setText("Balances reporting");
            SET041.setName("SET041");

            //---- SET042 ----
            SET042.setComponentPopupMenu(BTD);
            SET042.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET042.setText("Bilan, r\u00e9sultat");
            SET042.setName("SET042");

            //---- OBJ_87 ----
            OBJ_87.setText("31");
            OBJ_87.setName("OBJ_87");

            //---- OBJ_93 ----
            OBJ_93.setText("32");
            OBJ_93.setName("OBJ_93");

            //---- OBJ_99 ----
            OBJ_99.setText("33");
            OBJ_99.setName("OBJ_99");

            //---- OBJ_105 ----
            OBJ_105.setText("34");
            OBJ_105.setName("OBJ_105");

            //---- OBJ_111 ----
            OBJ_111.setText("35");
            OBJ_111.setName("OBJ_111");

            //---- OBJ_117 ----
            OBJ_117.setText("36");
            OBJ_117.setName("OBJ_117");

            //---- OBJ_123 ----
            OBJ_123.setText("37");
            OBJ_123.setName("OBJ_123");

            //---- OBJ_129 ----
            OBJ_129.setText("38");
            OBJ_129.setName("OBJ_129");

            //---- OBJ_135 ----
            OBJ_135.setText("39");
            OBJ_135.setName("OBJ_135");

            //---- OBJ_141 ----
            OBJ_141.setText("40");
            OBJ_141.setName("OBJ_141");

            //---- OBJ_147 ----
            OBJ_147.setText("41");
            OBJ_147.setName("OBJ_147");

            //---- OBJ_153 ----
            OBJ_153.setText("42");
            OBJ_153.setName("OBJ_153");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET031, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_93, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET032, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_99, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET033, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET034, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET035, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_117, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET036, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_123, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET037, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_129, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET038, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET039, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_141, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET040, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_147, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET041, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_153, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET042, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_87))
                    .addComponent(SET031, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_93))
                    .addComponent(SET032, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_99))
                    .addComponent(SET033, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_105))
                    .addComponent(SET034, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_111))
                    .addComponent(SET035, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_117))
                    .addComponent(SET036, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_123))
                    .addComponent(SET037, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_129))
                    .addComponent(SET038, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_135))
                    .addComponent(SET039, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_141))
                    .addComponent(SET040, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_147))
                    .addComponent(SET041, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_153))
                    .addComponent(SET042, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          xTitledPanel1ContentContainer.add(panel1);
          panel1.setBounds(10, 5, 445, 395);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- SET043 ----
            SET043.setComponentPopupMenu(BTD);
            SET043.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET043.setText("Edition de l'\u00e9ch\u00e9ancier");
            SET043.setName("SET043");

            //---- SET044 ----
            SET044.setComponentPopupMenu(BTD);
            SET044.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET044.setText("Editions de l'analytique");
            SET044.setName("SET044");

            //---- SET045 ----
            SET045.setComponentPopupMenu(BTD);
            SET045.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET045.setText("Editions de titres de paiement");
            SET045.setName("SET045");

            //---- SET046 ----
            SET046.setComponentPopupMenu(BTD);
            SET046.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET046.setText("Editions de la liasse fiscale");
            SET046.setName("SET046");

            //---- SET047 ----
            SET047.setComponentPopupMenu(BTD);
            SET047.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET047.setText("Edition des relev\u00e9s de compte");
            SET047.setName("SET047");

            //---- SET048 ----
            SET048.setComponentPopupMenu(BTD);
            SET048.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET048.setText("G\u00e9n\u00e9rateurs d'\u00e9tats");
            SET048.setName("SET048");

            //---- SET049 ----
            SET049.setComponentPopupMenu(BTD);
            SET049.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET049.setText("Gestion du tableau de tr\u00e9sorerie");
            SET049.setName("SET049");

            //---- SET050 ----
            SET050.setComponentPopupMenu(BTD);
            SET050.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET050.setText("Initialisation du rapprochement bancaire");
            SET050.setName("SET050");

            //---- SET051 ----
            SET051.setComponentPopupMenu(BTD);
            SET051.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET051.setText("Bon \u00e0 payer sur dettes");
            SET051.setName("SET051");

            //---- SET052 ----
            SET052.setComponentPopupMenu(BTD);
            SET052.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET052.setText("Modification dynamique imputation analytique");
            SET052.setName("SET052");

            //---- SET053 ----
            SET053.setComponentPopupMenu(BTD);
            SET053.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET053.setText("Transfert compte \u00e0 compte");
            SET053.setName("SET053");

            //---- SET054 ----
            SET054.setComponentPopupMenu(BTD);
            SET054.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET054.setText("Escompte en pointage manuel");
            SET054.setName("SET054");

            //---- OBJ_90 ----
            OBJ_90.setText("43");
            OBJ_90.setName("OBJ_90");

            //---- OBJ_96 ----
            OBJ_96.setText("44");
            OBJ_96.setName("OBJ_96");

            //---- OBJ_102 ----
            OBJ_102.setText("45");
            OBJ_102.setName("OBJ_102");

            //---- OBJ_108 ----
            OBJ_108.setText("46");
            OBJ_108.setName("OBJ_108");

            //---- OBJ_114 ----
            OBJ_114.setText("47");
            OBJ_114.setName("OBJ_114");

            //---- OBJ_120 ----
            OBJ_120.setText("48");
            OBJ_120.setName("OBJ_120");

            //---- OBJ_126 ----
            OBJ_126.setText("49");
            OBJ_126.setName("OBJ_126");

            //---- OBJ_132 ----
            OBJ_132.setText("50");
            OBJ_132.setName("OBJ_132");

            //---- OBJ_138 ----
            OBJ_138.setText("51");
            OBJ_138.setName("OBJ_138");

            //---- OBJ_144 ----
            OBJ_144.setText("52");
            OBJ_144.setName("OBJ_144");

            //---- OBJ_150 ----
            OBJ_150.setText("53");
            OBJ_150.setName("OBJ_150");

            //---- OBJ_156 ----
            OBJ_156.setText("54");
            OBJ_156.setName("OBJ_156");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET043, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET044, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET045, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_108, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET046, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET047, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET048, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_126, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET049, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_132, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET050, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_138, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET051, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_144, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET052, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_150, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET053, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_156, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(SET054, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_90))
                    .addComponent(SET043, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_96))
                    .addComponent(SET044, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_102))
                    .addComponent(SET045, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_108))
                    .addComponent(SET046, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_114))
                    .addComponent(SET047, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_120))
                    .addComponent(SET048, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_126))
                    .addComponent(SET049, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_132))
                    .addComponent(SET050, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_138))
                    .addComponent(SET051, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_144))
                    .addComponent(SET052, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_150))
                    .addComponent(SET053, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_156))
                    .addComponent(SET054, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          xTitledPanel1ContentContainer.add(panel2);
          panel2.setBounds(465, 5, 445, 395);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 924, 437);

        //======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);

          //---- OBJ_68 ----
          OBJ_68.setText("Aller \u00e0 la page");
          OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_68.setName("OBJ_68");
          OBJ_68.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_68ActionPerformed(e);
            }
          });
          pnlBas.add(OBJ_68);
          OBJ_68.setBounds(new Rectangle(new Point(75, 0), OBJ_68.getPreferredSize()));

          //---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(220, 0, 25, V06F.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(679, 458, 270, 34);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_43;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_64;
  private RiZoneSortie INDETB;
  private JPanel p_tete_droite;
  private JLabel OBJ_160;
  private JLabel OBJ_158;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private XRiCheckBox SET031;
  private XRiCheckBox SET032;
  private XRiCheckBox SET033;
  private XRiCheckBox SET034;
  private XRiCheckBox SET035;
  private XRiCheckBox SET036;
  private XRiCheckBox SET037;
  private XRiCheckBox SET038;
  private XRiCheckBox SET039;
  private XRiCheckBox SET040;
  private XRiCheckBox SET041;
  private XRiCheckBox SET042;
  private JLabel OBJ_87;
  private JLabel OBJ_93;
  private JLabel OBJ_99;
  private JLabel OBJ_105;
  private JLabel OBJ_111;
  private JLabel OBJ_117;
  private JLabel OBJ_123;
  private JLabel OBJ_129;
  private JLabel OBJ_135;
  private JLabel OBJ_141;
  private JLabel OBJ_147;
  private JLabel OBJ_153;
  private JPanel panel2;
  private XRiCheckBox SET043;
  private XRiCheckBox SET044;
  private XRiCheckBox SET045;
  private XRiCheckBox SET046;
  private XRiCheckBox SET047;
  private XRiCheckBox SET048;
  private XRiCheckBox SET049;
  private XRiCheckBox SET050;
  private XRiCheckBox SET051;
  private XRiCheckBox SET052;
  private XRiCheckBox SET053;
  private XRiCheckBox SET054;
  private JLabel OBJ_90;
  private JLabel OBJ_96;
  private JLabel OBJ_102;
  private JLabel OBJ_108;
  private JLabel OBJ_114;
  private JLabel OBJ_120;
  private JLabel OBJ_126;
  private JLabel OBJ_132;
  private JLabel OBJ_138;
  private JLabel OBJ_144;
  private JLabel OBJ_150;
  private JLabel OBJ_156;
  private SNPanel pnlBas;
  private SNBoutonLeger OBJ_68;
  private XRiTextField V06F;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
