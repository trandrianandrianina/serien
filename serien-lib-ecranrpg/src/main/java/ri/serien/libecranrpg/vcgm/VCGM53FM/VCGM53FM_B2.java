
package ri.serien.libecranrpg.vcgm.VCGM53FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM53FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM53FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIER@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    DATDEB.setEnabled(lexique.isPresent("DATDEB"));
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("WDIER"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Imputation différence de réglements"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_31_OBJ_31 = new JLabel();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_32_OBJ_32 = new RiZoneSortie();
    DATDEB = new XRiTextField();
    CGE01 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    MTE01 = new XRiTextField();
    LIE01 = new XRiTextField();
    WSAN01 = new XRiTextField();
    WAFF01 = new XRiTextField();
    WNAT01 = new XRiTextField();
    CGE02 = new XRiTextField();
    MTE02 = new XRiTextField();
    LIE02 = new XRiTextField();
    WSAN02 = new XRiTextField();
    WAFF02 = new XRiTextField();
    WNAT02 = new XRiTextField();
    CGE03 = new XRiTextField();
    MTE03 = new XRiTextField();
    LIE03 = new XRiTextField();
    WSAN03 = new XRiTextField();
    WAFF03 = new XRiTextField();
    WNAT03 = new XRiTextField();
    CGE04 = new XRiTextField();
    MTE04 = new XRiTextField();
    LIE04 = new XRiTextField();
    WSAN04 = new XRiTextField();
    WAFF04 = new XRiTextField();
    WNAT04 = new XRiTextField();
    CGE05 = new XRiTextField();
    MTE05 = new XRiTextField();
    LIE05 = new XRiTextField();
    WSAN05 = new XRiTextField();
    WAFF05 = new XRiTextField();
    WNAT05 = new XRiTextField();
    C6CGT = new XRiTextField();
    C6MTT = new XRiTextField();
    C6LIT = new XRiTextField();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 265));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Valeurs initiales");
            riSousMenu_bt6.setToolTipText("R\u00e9afficher les valeurs initiales");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Diff\u00e9rence");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
          panel1.add(OBJ_31_OBJ_31);
          OBJ_31_OBJ_31.setBounds(15, 207, 130, 20);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("G\u00e9n\u00e9ration au");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel1.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(350, 209, 115, OBJ_33_OBJ_33.getPreferredSize().height);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("@WDIER@");
          OBJ_32_OBJ_32.setForeground(Color.red);
          OBJ_32_OBJ_32.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel1.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(150, 205, 80, OBJ_32_OBJ_32.getPreferredSize().height);

          //---- DATDEB ----
          DATDEB.setName("DATDEB");
          panel1.add(DATDEB);
          DATDEB.setBounds(465, 203, 66, DATDEB.getPreferredSize().height);

          //---- CGE01 ----
          CGE01.setName("CGE01");
          panel1.add(CGE01);
          CGE01.setBounds(90, 30, 60, CGE01.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Charge");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(15, 36, 75, label1.getPreferredSize().height);

          //---- label2 ----
          label2.setText("Compte");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(new Rectangle(new Point(90, 10), label2.getPreferredSize()));

          //---- label3 ----
          label3.setText("Montant");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(new Rectangle(new Point(150, 10), label3.getPreferredSize()));

          //---- label4 ----
          label4.setText("Libell\u00e9 d'\u00e9criture");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(new Rectangle(new Point(230, 10), label4.getPreferredSize()));

          //---- label5 ----
          label5.setText("section");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(543, 10, 45, label5.getPreferredSize().height);

          //---- label6 ----
          label6.setText("affaire");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(590, 10, 50, label6.getPreferredSize().height);

          //---- label7 ----
          label7.setText("nature");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(645, 10, 50, label7.getPreferredSize().height);

          //---- MTE01 ----
          MTE01.setName("MTE01");
          panel1.add(MTE01);
          MTE01.setBounds(150, 30, 80, MTE01.getPreferredSize().height);

          //---- LIE01 ----
          LIE01.setName("LIE01");
          panel1.add(LIE01);
          LIE01.setBounds(230, 30, 300, LIE01.getPreferredSize().height);

          //---- WSAN01 ----
          WSAN01.setName("WSAN01");
          panel1.add(WSAN01);
          WSAN01.setBounds(545, 30, 40, WSAN01.getPreferredSize().height);

          //---- WAFF01 ----
          WAFF01.setName("WAFF01");
          panel1.add(WAFF01);
          WAFF01.setBounds(590, 30, 50, WAFF01.getPreferredSize().height);

          //---- WNAT01 ----
          WNAT01.setName("WNAT01");
          panel1.add(WNAT01);
          WNAT01.setBounds(645, 30, 50, WNAT01.getPreferredSize().height);

          //---- CGE02 ----
          CGE02.setName("CGE02");
          panel1.add(CGE02);
          CGE02.setBounds(90, 55, 60, CGE02.getPreferredSize().height);

          //---- MTE02 ----
          MTE02.setName("MTE02");
          panel1.add(MTE02);
          MTE02.setBounds(150, 55, 80, MTE02.getPreferredSize().height);

          //---- LIE02 ----
          LIE02.setName("LIE02");
          panel1.add(LIE02);
          LIE02.setBounds(230, 55, 300, LIE02.getPreferredSize().height);

          //---- WSAN02 ----
          WSAN02.setName("WSAN02");
          panel1.add(WSAN02);
          WSAN02.setBounds(545, 55, 40, WSAN02.getPreferredSize().height);

          //---- WAFF02 ----
          WAFF02.setName("WAFF02");
          panel1.add(WAFF02);
          WAFF02.setBounds(590, 55, 50, WAFF02.getPreferredSize().height);

          //---- WNAT02 ----
          WNAT02.setName("WNAT02");
          panel1.add(WNAT02);
          WNAT02.setBounds(645, 55, 50, WNAT02.getPreferredSize().height);

          //---- CGE03 ----
          CGE03.setName("CGE03");
          panel1.add(CGE03);
          CGE03.setBounds(90, 80, 60, CGE03.getPreferredSize().height);

          //---- MTE03 ----
          MTE03.setName("MTE03");
          panel1.add(MTE03);
          MTE03.setBounds(150, 80, 80, MTE03.getPreferredSize().height);

          //---- LIE03 ----
          LIE03.setName("LIE03");
          panel1.add(LIE03);
          LIE03.setBounds(230, 80, 300, LIE03.getPreferredSize().height);

          //---- WSAN03 ----
          WSAN03.setName("WSAN03");
          panel1.add(WSAN03);
          WSAN03.setBounds(545, 80, 40, WSAN03.getPreferredSize().height);

          //---- WAFF03 ----
          WAFF03.setName("WAFF03");
          panel1.add(WAFF03);
          WAFF03.setBounds(590, 80, 50, WAFF03.getPreferredSize().height);

          //---- WNAT03 ----
          WNAT03.setName("WNAT03");
          panel1.add(WNAT03);
          WNAT03.setBounds(645, 80, 50, WNAT03.getPreferredSize().height);

          //---- CGE04 ----
          CGE04.setName("CGE04");
          panel1.add(CGE04);
          CGE04.setBounds(90, 105, 60, CGE04.getPreferredSize().height);

          //---- MTE04 ----
          MTE04.setName("MTE04");
          panel1.add(MTE04);
          MTE04.setBounds(150, 105, 80, MTE04.getPreferredSize().height);

          //---- LIE04 ----
          LIE04.setName("LIE04");
          panel1.add(LIE04);
          LIE04.setBounds(230, 105, 300, LIE04.getPreferredSize().height);

          //---- WSAN04 ----
          WSAN04.setName("WSAN04");
          panel1.add(WSAN04);
          WSAN04.setBounds(545, 105, 40, WSAN04.getPreferredSize().height);

          //---- WAFF04 ----
          WAFF04.setName("WAFF04");
          panel1.add(WAFF04);
          WAFF04.setBounds(590, 105, 50, WAFF04.getPreferredSize().height);

          //---- WNAT04 ----
          WNAT04.setName("WNAT04");
          panel1.add(WNAT04);
          WNAT04.setBounds(645, 105, 50, WNAT04.getPreferredSize().height);

          //---- CGE05 ----
          CGE05.setName("CGE05");
          panel1.add(CGE05);
          CGE05.setBounds(90, 130, 60, CGE05.getPreferredSize().height);

          //---- MTE05 ----
          MTE05.setName("MTE05");
          panel1.add(MTE05);
          MTE05.setBounds(150, 130, 80, MTE05.getPreferredSize().height);

          //---- LIE05 ----
          LIE05.setName("LIE05");
          panel1.add(LIE05);
          LIE05.setBounds(230, 130, 300, LIE05.getPreferredSize().height);

          //---- WSAN05 ----
          WSAN05.setName("WSAN05");
          panel1.add(WSAN05);
          WSAN05.setBounds(545, 130, 40, WSAN05.getPreferredSize().height);

          //---- WAFF05 ----
          WAFF05.setName("WAFF05");
          panel1.add(WAFF05);
          WAFF05.setBounds(590, 130, 50, WAFF05.getPreferredSize().height);

          //---- WNAT05 ----
          WNAT05.setName("WNAT05");
          panel1.add(WNAT05);
          WNAT05.setBounds(645, 130, 50, WNAT05.getPreferredSize().height);

          //---- C6CGT ----
          C6CGT.setName("C6CGT");
          panel1.add(C6CGT);
          C6CGT.setBounds(90, 170, 60, C6CGT.getPreferredSize().height);

          //---- C6MTT ----
          C6MTT.setName("C6MTT");
          panel1.add(C6MTT);
          C6MTT.setBounds(150, 170, 80, C6MTT.getPreferredSize().height);

          //---- C6LIT ----
          C6LIT.setName("C6LIT");
          panel1.add(C6LIT);
          C6LIT.setBounds(230, 170, 300, C6LIT.getPreferredSize().height);

          //---- label8 ----
          label8.setText("Charge");
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(15, 61, 75, label8.getPreferredSize().height);

          //---- label9 ----
          label9.setText("Charge");
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(15, 86, 75, label9.getPreferredSize().height);

          //---- label10 ----
          label10.setText("Charge");
          label10.setName("label10");
          panel1.add(label10);
          label10.setBounds(15, 111, 75, label10.getPreferredSize().height);

          //---- label11 ----
          label11.setText("Charge");
          label11.setName("label11");
          panel1.add(label11);
          label11.setBounds(15, 136, 75, label11.getPreferredSize().height);

          //---- label12 ----
          label12.setText("TVA");
          label12.setName("label12");
          panel1.add(label12);
          label12.setBounds(15, 176, 75, label12.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 15, 715, 240);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_31_OBJ_31;
  private JLabel OBJ_33_OBJ_33;
  private RiZoneSortie OBJ_32_OBJ_32;
  private XRiTextField DATDEB;
  private XRiTextField CGE01;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private XRiTextField MTE01;
  private XRiTextField LIE01;
  private XRiTextField WSAN01;
  private XRiTextField WAFF01;
  private XRiTextField WNAT01;
  private XRiTextField CGE02;
  private XRiTextField MTE02;
  private XRiTextField LIE02;
  private XRiTextField WSAN02;
  private XRiTextField WAFF02;
  private XRiTextField WNAT02;
  private XRiTextField CGE03;
  private XRiTextField MTE03;
  private XRiTextField LIE03;
  private XRiTextField WSAN03;
  private XRiTextField WAFF03;
  private XRiTextField WNAT03;
  private XRiTextField CGE04;
  private XRiTextField MTE04;
  private XRiTextField LIE04;
  private XRiTextField WSAN04;
  private XRiTextField WAFF04;
  private XRiTextField WNAT04;
  private XRiTextField CGE05;
  private XRiTextField MTE05;
  private XRiTextField LIE05;
  private XRiTextField WSAN05;
  private XRiTextField WAFF05;
  private XRiTextField WNAT05;
  private XRiTextField C6CGT;
  private XRiTextField C6MTT;
  private XRiTextField C6LIT;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
